(declare-fun post!bb2.i.i26.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f5bc6401350_0 () Bool)
(declare-fun F0x7f5bc6402650 () Bool)
(declare-fun v0x7f5bc63fff90_0 () Real)
(declare-fun F0x7f5bc6402750 () Bool)
(declare-fun v0x7f5bc63ff910_0 () Bool)
(declare-fun F0x7f5bc6402790 () Bool)
(declare-fun v0x7f5bc63fe5d0_0 () Real)
(declare-fun v0x7f5bc63fdbd0_0 () Real)
(declare-fun v0x7f5bc63fda10_0 () Bool)
(declare-fun v0x7f5bc63fd510_0 () Real)
(declare-fun E0x7f5bc6400e10 () Bool)
(declare-fun E0x7f5bc6400b50 () Bool)
(declare-fun v0x7f5bc64007d0_0 () Real)
(declare-fun v0x7f5bc6400150_0 () Real)
(declare-fun E0x7f5bc6400890 () Bool)
(declare-fun v0x7f5bc64015d0_0 () Bool)
(declare-fun v0x7f5bc6400650_0 () Bool)
(declare-fun v0x7f5bc63fea90_0 () Bool)
(declare-fun v0x7f5bc6400290_0 () Bool)
(declare-fun v0x7f5bc6401710_0 () Bool)
(declare-fun v0x7f5bc64003d0_0 () Bool)
(declare-fun E0x7f5bc63ffc50 () Bool)
(declare-fun v0x7f5bc63ff550_0 () Bool)
(declare-fun v0x7f5bc6400710_0 () Real)
(declare-fun E0x7f5bc63ff050 () Bool)
(declare-fun v0x7f5bc63ffe50_0 () Bool)
(declare-fun v0x7f5bc63fcd90_0 () Real)
(declare-fun v0x7f5bc63febd0_0 () Real)
(declare-fun v0x7f5bc63ffb90_0 () Bool)
(declare-fun v0x7f5bc63fed10_0 () Bool)
(declare-fun E0x7f5bc6400490 () Bool)
(declare-fun v0x7f5bc63fe690_0 () Bool)
(declare-fun E0x7f5bc63fe890 () Bool)
(declare-fun v0x7f5bc63fe7d0_0 () Bool)
(declare-fun E0x7f5bc63fee90 () Bool)
(declare-fun v0x7f5bc63fdd10_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f5bc63fdf10_0 () Real)
(declare-fun v0x7f5bc63fedd0_0 () Real)
(declare-fun v0x7f5bc6401850_0 () Bool)
(declare-fun E0x7f5bc63fd7d0 () Bool)
(declare-fun v0x7f5bc6401490_0 () Bool)
(declare-fun E0x7f5bc63ff750 () Bool)
(declare-fun v0x7f5bc63fcc10_0 () Real)
(declare-fun v0x7f5bc63fd450_0 () Bool)
(declare-fun v0x7f5bc63fd5d0_0 () Bool)
(declare-fun F0x7f5bc6402590 () Bool)
(declare-fun v0x7f5bc63fd710_0 () Bool)
(declare-fun F0x7f5bc64024d0 () Bool)
(declare-fun v0x7f5bc63ff690_0 () Bool)
(declare-fun v0x7f5bc63ffa50_0 () Real)
(declare-fun F0x7f5bc6402390 () Bool)
(declare-fun E0x7f5bc63fe190 () Bool)
(declare-fun v0x7f5bc63fb010_0 () Real)
(declare-fun v0x7f5bc63fcf50_0 () Real)
(declare-fun E0x7f5bc63fdfd0 () Bool)
(declare-fun v0x7f5bc63fce90_0 () Real)
(declare-fun v0x7f5bc63fb110_0 () Bool)
(declare-fun v0x7f5bc63fde50_0 () Bool)
(declare-fun v0x7f5bc63fce50_0 () Real)
(declare-fun F0x7f5bc6402490 () Bool)

(assert (=> F0x7f5bc6402490
    (and v0x7f5bc63fb110_0
         (<= v0x7f5bc63fce50_0 0.0)
         (>= v0x7f5bc63fce50_0 0.0)
         (<= v0x7f5bc63fcf50_0 0.0)
         (>= v0x7f5bc63fcf50_0 0.0)
         (<= v0x7f5bc63fb010_0 1.0)
         (>= v0x7f5bc63fb010_0 1.0))))
(assert (=> F0x7f5bc6402490 F0x7f5bc6402390))
(assert (let ((a!1 (=> v0x7f5bc63fde50_0
               (or (and v0x7f5bc63fd710_0
                        E0x7f5bc63fdfd0
                        (<= v0x7f5bc63fdf10_0 v0x7f5bc63fdd10_0)
                        (>= v0x7f5bc63fdf10_0 v0x7f5bc63fdd10_0))
                   (and v0x7f5bc63fd450_0
                        E0x7f5bc63fe190
                        v0x7f5bc63fd5d0_0
                        (<= v0x7f5bc63fdf10_0 v0x7f5bc63fce90_0)
                        (>= v0x7f5bc63fdf10_0 v0x7f5bc63fce90_0)))))
      (a!2 (=> v0x7f5bc63fde50_0
               (or (and E0x7f5bc63fdfd0 (not E0x7f5bc63fe190))
                   (and E0x7f5bc63fe190 (not E0x7f5bc63fdfd0)))))
      (a!3 (=> v0x7f5bc63fed10_0
               (or (and v0x7f5bc63fe7d0_0
                        E0x7f5bc63fee90
                        (<= v0x7f5bc63fedd0_0 v0x7f5bc63febd0_0)
                        (>= v0x7f5bc63fedd0_0 v0x7f5bc63febd0_0))
                   (and v0x7f5bc63fde50_0
                        E0x7f5bc63ff050
                        v0x7f5bc63fe690_0
                        (<= v0x7f5bc63fedd0_0 v0x7f5bc63fcd90_0)
                        (>= v0x7f5bc63fedd0_0 v0x7f5bc63fcd90_0)))))
      (a!4 (=> v0x7f5bc63fed10_0
               (or (and E0x7f5bc63fee90 (not E0x7f5bc63ff050))
                   (and E0x7f5bc63ff050 (not E0x7f5bc63fee90)))))
      (a!5 (or (and v0x7f5bc63ff690_0
                    E0x7f5bc6400890
                    (<= v0x7f5bc6400710_0 v0x7f5bc63fdf10_0)
                    (>= v0x7f5bc6400710_0 v0x7f5bc63fdf10_0)
                    (<= v0x7f5bc64007d0_0 v0x7f5bc63ffa50_0)
                    (>= v0x7f5bc64007d0_0 v0x7f5bc63ffa50_0))
               (and v0x7f5bc64003d0_0
                    E0x7f5bc6400b50
                    (and (<= v0x7f5bc6400710_0 v0x7f5bc6400150_0)
                         (>= v0x7f5bc6400710_0 v0x7f5bc6400150_0))
                    (<= v0x7f5bc64007d0_0 v0x7f5bc63fcc10_0)
                    (>= v0x7f5bc64007d0_0 v0x7f5bc63fcc10_0))
               (and v0x7f5bc63ffb90_0
                    E0x7f5bc6400e10
                    (not v0x7f5bc6400290_0)
                    (and (<= v0x7f5bc6400710_0 v0x7f5bc6400150_0)
                         (>= v0x7f5bc6400710_0 v0x7f5bc6400150_0))
                    (<= v0x7f5bc64007d0_0 0.0)
                    (>= v0x7f5bc64007d0_0 0.0))))
      (a!6 (=> v0x7f5bc6400650_0
               (or (and E0x7f5bc6400890
                        (not E0x7f5bc6400b50)
                        (not E0x7f5bc6400e10))
                   (and E0x7f5bc6400b50
                        (not E0x7f5bc6400890)
                        (not E0x7f5bc6400e10))
                   (and E0x7f5bc6400e10
                        (not E0x7f5bc6400890)
                        (not E0x7f5bc6400b50))))))
(let ((a!7 (and (=> v0x7f5bc63fd710_0
                    (and v0x7f5bc63fd450_0
                         E0x7f5bc63fd7d0
                         (not v0x7f5bc63fd5d0_0)))
                (=> v0x7f5bc63fd710_0 E0x7f5bc63fd7d0)
                a!1
                a!2
                (=> v0x7f5bc63fe7d0_0
                    (and v0x7f5bc63fde50_0
                         E0x7f5bc63fe890
                         (not v0x7f5bc63fe690_0)))
                (=> v0x7f5bc63fe7d0_0 E0x7f5bc63fe890)
                a!3
                a!4
                (=> v0x7f5bc63ff690_0
                    (and v0x7f5bc63fed10_0 E0x7f5bc63ff750 v0x7f5bc63ff550_0))
                (=> v0x7f5bc63ff690_0 E0x7f5bc63ff750)
                (=> v0x7f5bc63ffb90_0
                    (and v0x7f5bc63fed10_0
                         E0x7f5bc63ffc50
                         (not v0x7f5bc63ff550_0)))
                (=> v0x7f5bc63ffb90_0 E0x7f5bc63ffc50)
                (=> v0x7f5bc64003d0_0
                    (and v0x7f5bc63ffb90_0 E0x7f5bc6400490 v0x7f5bc6400290_0))
                (=> v0x7f5bc64003d0_0 E0x7f5bc6400490)
                (=> v0x7f5bc6400650_0 a!5)
                a!6
                v0x7f5bc6400650_0
                (not v0x7f5bc6401850_0)
                (<= v0x7f5bc63fce50_0 v0x7f5bc64007d0_0)
                (>= v0x7f5bc63fce50_0 v0x7f5bc64007d0_0)
                (<= v0x7f5bc63fcf50_0 v0x7f5bc63fedd0_0)
                (>= v0x7f5bc63fcf50_0 v0x7f5bc63fedd0_0)
                (<= v0x7f5bc63fb010_0 v0x7f5bc6400710_0)
                (>= v0x7f5bc63fb010_0 v0x7f5bc6400710_0)
                (= v0x7f5bc63fd5d0_0 (= v0x7f5bc63fd510_0 0.0))
                (= v0x7f5bc63fda10_0 (< v0x7f5bc63fce90_0 2.0))
                (= v0x7f5bc63fdbd0_0 (ite v0x7f5bc63fda10_0 1.0 0.0))
                (= v0x7f5bc63fdd10_0 (+ v0x7f5bc63fdbd0_0 v0x7f5bc63fce90_0))
                (= v0x7f5bc63fe690_0 (= v0x7f5bc63fe5d0_0 0.0))
                (= v0x7f5bc63fea90_0 (= v0x7f5bc63fcd90_0 0.0))
                (= v0x7f5bc63febd0_0 (ite v0x7f5bc63fea90_0 1.0 0.0))
                (= v0x7f5bc63ff550_0 (= v0x7f5bc63fcc10_0 0.0))
                (= v0x7f5bc63ff910_0 (> v0x7f5bc63fdf10_0 1.0))
                (= v0x7f5bc63ffa50_0
                   (ite v0x7f5bc63ff910_0 1.0 v0x7f5bc63fcc10_0))
                (= v0x7f5bc63ffe50_0 (> v0x7f5bc63fdf10_0 0.0))
                (= v0x7f5bc63fff90_0 (+ v0x7f5bc63fdf10_0 (- 1.0)))
                (= v0x7f5bc6400150_0
                   (ite v0x7f5bc63ffe50_0 v0x7f5bc63fff90_0 v0x7f5bc63fdf10_0))
                (= v0x7f5bc6400290_0 (= v0x7f5bc6400150_0 0.0))
                (= v0x7f5bc6401350_0 (= v0x7f5bc63fedd0_0 0.0))
                (= v0x7f5bc6401490_0 (= v0x7f5bc6400710_0 2.0))
                (= v0x7f5bc64015d0_0 (= v0x7f5bc64007d0_0 0.0))
                (= v0x7f5bc6401710_0 (and v0x7f5bc6401490_0 v0x7f5bc6401350_0))
                (= v0x7f5bc6401850_0 (and v0x7f5bc6401710_0 v0x7f5bc64015d0_0)))))
  (=> F0x7f5bc64024d0 a!7))))
(assert (=> F0x7f5bc64024d0 F0x7f5bc6402590))
(assert (let ((a!1 (=> v0x7f5bc63fde50_0
               (or (and v0x7f5bc63fd710_0
                        E0x7f5bc63fdfd0
                        (<= v0x7f5bc63fdf10_0 v0x7f5bc63fdd10_0)
                        (>= v0x7f5bc63fdf10_0 v0x7f5bc63fdd10_0))
                   (and v0x7f5bc63fd450_0
                        E0x7f5bc63fe190
                        v0x7f5bc63fd5d0_0
                        (<= v0x7f5bc63fdf10_0 v0x7f5bc63fce90_0)
                        (>= v0x7f5bc63fdf10_0 v0x7f5bc63fce90_0)))))
      (a!2 (=> v0x7f5bc63fde50_0
               (or (and E0x7f5bc63fdfd0 (not E0x7f5bc63fe190))
                   (and E0x7f5bc63fe190 (not E0x7f5bc63fdfd0)))))
      (a!3 (=> v0x7f5bc63fed10_0
               (or (and v0x7f5bc63fe7d0_0
                        E0x7f5bc63fee90
                        (<= v0x7f5bc63fedd0_0 v0x7f5bc63febd0_0)
                        (>= v0x7f5bc63fedd0_0 v0x7f5bc63febd0_0))
                   (and v0x7f5bc63fde50_0
                        E0x7f5bc63ff050
                        v0x7f5bc63fe690_0
                        (<= v0x7f5bc63fedd0_0 v0x7f5bc63fcd90_0)
                        (>= v0x7f5bc63fedd0_0 v0x7f5bc63fcd90_0)))))
      (a!4 (=> v0x7f5bc63fed10_0
               (or (and E0x7f5bc63fee90 (not E0x7f5bc63ff050))
                   (and E0x7f5bc63ff050 (not E0x7f5bc63fee90)))))
      (a!5 (or (and v0x7f5bc63ff690_0
                    E0x7f5bc6400890
                    (<= v0x7f5bc6400710_0 v0x7f5bc63fdf10_0)
                    (>= v0x7f5bc6400710_0 v0x7f5bc63fdf10_0)
                    (<= v0x7f5bc64007d0_0 v0x7f5bc63ffa50_0)
                    (>= v0x7f5bc64007d0_0 v0x7f5bc63ffa50_0))
               (and v0x7f5bc64003d0_0
                    E0x7f5bc6400b50
                    (and (<= v0x7f5bc6400710_0 v0x7f5bc6400150_0)
                         (>= v0x7f5bc6400710_0 v0x7f5bc6400150_0))
                    (<= v0x7f5bc64007d0_0 v0x7f5bc63fcc10_0)
                    (>= v0x7f5bc64007d0_0 v0x7f5bc63fcc10_0))
               (and v0x7f5bc63ffb90_0
                    E0x7f5bc6400e10
                    (not v0x7f5bc6400290_0)
                    (and (<= v0x7f5bc6400710_0 v0x7f5bc6400150_0)
                         (>= v0x7f5bc6400710_0 v0x7f5bc6400150_0))
                    (<= v0x7f5bc64007d0_0 0.0)
                    (>= v0x7f5bc64007d0_0 0.0))))
      (a!6 (=> v0x7f5bc6400650_0
               (or (and E0x7f5bc6400890
                        (not E0x7f5bc6400b50)
                        (not E0x7f5bc6400e10))
                   (and E0x7f5bc6400b50
                        (not E0x7f5bc6400890)
                        (not E0x7f5bc6400e10))
                   (and E0x7f5bc6400e10
                        (not E0x7f5bc6400890)
                        (not E0x7f5bc6400b50))))))
(let ((a!7 (and (=> v0x7f5bc63fd710_0
                    (and v0x7f5bc63fd450_0
                         E0x7f5bc63fd7d0
                         (not v0x7f5bc63fd5d0_0)))
                (=> v0x7f5bc63fd710_0 E0x7f5bc63fd7d0)
                a!1
                a!2
                (=> v0x7f5bc63fe7d0_0
                    (and v0x7f5bc63fde50_0
                         E0x7f5bc63fe890
                         (not v0x7f5bc63fe690_0)))
                (=> v0x7f5bc63fe7d0_0 E0x7f5bc63fe890)
                a!3
                a!4
                (=> v0x7f5bc63ff690_0
                    (and v0x7f5bc63fed10_0 E0x7f5bc63ff750 v0x7f5bc63ff550_0))
                (=> v0x7f5bc63ff690_0 E0x7f5bc63ff750)
                (=> v0x7f5bc63ffb90_0
                    (and v0x7f5bc63fed10_0
                         E0x7f5bc63ffc50
                         (not v0x7f5bc63ff550_0)))
                (=> v0x7f5bc63ffb90_0 E0x7f5bc63ffc50)
                (=> v0x7f5bc64003d0_0
                    (and v0x7f5bc63ffb90_0 E0x7f5bc6400490 v0x7f5bc6400290_0))
                (=> v0x7f5bc64003d0_0 E0x7f5bc6400490)
                (=> v0x7f5bc6400650_0 a!5)
                a!6
                v0x7f5bc6400650_0
                v0x7f5bc6401850_0
                (= v0x7f5bc63fd5d0_0 (= v0x7f5bc63fd510_0 0.0))
                (= v0x7f5bc63fda10_0 (< v0x7f5bc63fce90_0 2.0))
                (= v0x7f5bc63fdbd0_0 (ite v0x7f5bc63fda10_0 1.0 0.0))
                (= v0x7f5bc63fdd10_0 (+ v0x7f5bc63fdbd0_0 v0x7f5bc63fce90_0))
                (= v0x7f5bc63fe690_0 (= v0x7f5bc63fe5d0_0 0.0))
                (= v0x7f5bc63fea90_0 (= v0x7f5bc63fcd90_0 0.0))
                (= v0x7f5bc63febd0_0 (ite v0x7f5bc63fea90_0 1.0 0.0))
                (= v0x7f5bc63ff550_0 (= v0x7f5bc63fcc10_0 0.0))
                (= v0x7f5bc63ff910_0 (> v0x7f5bc63fdf10_0 1.0))
                (= v0x7f5bc63ffa50_0
                   (ite v0x7f5bc63ff910_0 1.0 v0x7f5bc63fcc10_0))
                (= v0x7f5bc63ffe50_0 (> v0x7f5bc63fdf10_0 0.0))
                (= v0x7f5bc63fff90_0 (+ v0x7f5bc63fdf10_0 (- 1.0)))
                (= v0x7f5bc6400150_0
                   (ite v0x7f5bc63ffe50_0 v0x7f5bc63fff90_0 v0x7f5bc63fdf10_0))
                (= v0x7f5bc6400290_0 (= v0x7f5bc6400150_0 0.0))
                (= v0x7f5bc6401350_0 (= v0x7f5bc63fedd0_0 0.0))
                (= v0x7f5bc6401490_0 (= v0x7f5bc6400710_0 2.0))
                (= v0x7f5bc64015d0_0 (= v0x7f5bc64007d0_0 0.0))
                (= v0x7f5bc6401710_0 (and v0x7f5bc6401490_0 v0x7f5bc6401350_0))
                (= v0x7f5bc6401850_0 (and v0x7f5bc6401710_0 v0x7f5bc64015d0_0)))))
  (=> F0x7f5bc6402650 a!7))))
(assert (=> F0x7f5bc6402650 F0x7f5bc6402590))
(assert (=> F0x7f5bc6402790 (or F0x7f5bc6402490 F0x7f5bc64024d0)))
(assert (=> F0x7f5bc6402750 F0x7f5bc6402650))
(assert (=> pre!entry!0 (=> F0x7f5bc6402390 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f5bc6402590 (>= v0x7f5bc63fcc10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f5bc6402590 (not (<= 3.0 v0x7f5bc63fce90_0)))))
(assert (or (and (not post!bb1.i.i!0) F0x7f5bc6402790 (not (>= v0x7f5bc63fce50_0 0.0)))
    (and (not post!bb1.i.i!1) F0x7f5bc6402790 (<= 3.0 v0x7f5bc63fb010_0))
    (and (not post!bb2.i.i26.i.i!0) F0x7f5bc6402750 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i26.i.i!0)
