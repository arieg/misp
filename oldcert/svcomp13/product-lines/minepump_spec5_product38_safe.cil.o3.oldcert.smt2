(declare-fun post!bb2.i.i25.i.i!0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun F0x7f648cb89510 () Bool)
(declare-fun v0x7f648cb88310_0 () Bool)
(declare-fun v0x7f648cb881d0_0 () Bool)
(declare-fun v0x7f648cb86790_0 () Bool)
(declare-fun F0x7f648cb89610 () Bool)
(declare-fun v0x7f648cb85910_0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f648cb886d0_0 () Bool)
(declare-fun E0x7f648cb87c90 () Bool)
(declare-fun v0x7f648cb83a90_0 () Real)
(declare-fun v0x7f648cb86fd0_0 () Real)
(declare-fun v0x7f648cb87590_0 () Real)
(declare-fun v0x7f648cb84890_0 () Bool)
(declare-fun v0x7f648cb874d0_0 () Bool)
(declare-fun E0x7f648cb87710 () Bool)
(declare-fun v0x7f648cb87110_0 () Bool)
(declare-fun v0x7f648cb86e10_0 () Real)
(declare-fun E0x7f648cb87310 () Bool)
(declare-fun v0x7f648cb86a10_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f648cb83c10_0 () Real)
(declare-fun v0x7f648cb85a50_0 () Real)
(declare-fun E0x7f648cb85710 () Bool)
(declare-fun v0x7f648cb868d0_0 () Real)
(declare-fun v0x7f648cb88450_0 () Bool)
(declare-fun v0x7f648cb85650_0 () Bool)
(declare-fun v0x7f648cb83d10_0 () Real)
(declare-fun v0x7f648cb84b90_0 () Real)
(declare-fun E0x7f648cb84e50 () Bool)
(declare-fun v0x7f648cb863d0_0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun v0x7f648cb84cd0_0 () Bool)
(declare-fun v0x7f648cb85450_0 () Real)
(declare-fun v0x7f648cb88590_0 () Bool)
(declare-fun v0x7f648cb84450_0 () Bool)
(declare-fun E0x7f648cb84650 () Bool)
(declare-fun v0x7f648cb84a50_0 () Real)
(declare-fun v0x7f648cb842d0_0 () Bool)
(declare-fun v0x7f648cb87650_0 () Real)
(declare-fun E0x7f648cb85010 () Bool)
(declare-fun v0x7f648cb84d90_0 () Real)
(declare-fun v0x7f648cb85c50_0 () Real)
(declare-fun v0x7f648cb84590_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f648cb89390 () Bool)
(declare-fun E0x7f648cb879d0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f648cb895d0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f648cb87250_0 () Bool)
(declare-fun E0x7f648cb86ad0 () Bool)
(declare-fun v0x7f648cb84390_0 () Real)
(declare-fun v0x7f648cb86cd0_0 () Bool)
(declare-fun v0x7f648cb83dd0_0 () Real)
(declare-fun F0x7f648cb892d0 () Bool)
(declare-fun E0x7f648cb85ed0 () Bool)
(declare-fun F0x7f648cb89290 () Bool)
(declare-fun v0x7f648cb83cd0_0 () Real)
(declare-fun v0x7f648cb86510_0 () Bool)
(declare-fun v0x7f648cb85b90_0 () Bool)
(declare-fun v0x7f648cb82010_0 () Real)
(declare-fun E0x7f648cb865d0 () Bool)
(declare-fun v0x7f648cb85510_0 () Bool)
(declare-fun v0x7f648cb82110_0 () Bool)
(declare-fun E0x7f648cb85d10 () Bool)
(declare-fun F0x7f648cb894d0 () Bool)

(assert (=> F0x7f648cb894d0
    (and v0x7f648cb82110_0
         (<= v0x7f648cb83cd0_0 0.0)
         (>= v0x7f648cb83cd0_0 0.0)
         (<= v0x7f648cb83dd0_0 0.0)
         (>= v0x7f648cb83dd0_0 0.0)
         (<= v0x7f648cb82010_0 1.0)
         (>= v0x7f648cb82010_0 1.0))))
(assert (=> F0x7f648cb894d0 F0x7f648cb89290))
(assert (let ((a!1 (=> v0x7f648cb84cd0_0
               (or (and v0x7f648cb84590_0
                        E0x7f648cb84e50
                        (<= v0x7f648cb84d90_0 v0x7f648cb84b90_0)
                        (>= v0x7f648cb84d90_0 v0x7f648cb84b90_0))
                   (and v0x7f648cb842d0_0
                        E0x7f648cb85010
                        v0x7f648cb84450_0
                        (<= v0x7f648cb84d90_0 v0x7f648cb83d10_0)
                        (>= v0x7f648cb84d90_0 v0x7f648cb83d10_0)))))
      (a!2 (=> v0x7f648cb84cd0_0
               (or (and E0x7f648cb84e50 (not E0x7f648cb85010))
                   (and E0x7f648cb85010 (not E0x7f648cb84e50)))))
      (a!3 (=> v0x7f648cb85b90_0
               (or (and v0x7f648cb85650_0
                        E0x7f648cb85d10
                        (<= v0x7f648cb85c50_0 v0x7f648cb85a50_0)
                        (>= v0x7f648cb85c50_0 v0x7f648cb85a50_0))
                   (and v0x7f648cb84cd0_0
                        E0x7f648cb85ed0
                        v0x7f648cb85510_0
                        (<= v0x7f648cb85c50_0 v0x7f648cb83c10_0)
                        (>= v0x7f648cb85c50_0 v0x7f648cb83c10_0)))))
      (a!4 (=> v0x7f648cb85b90_0
               (or (and E0x7f648cb85d10 (not E0x7f648cb85ed0))
                   (and E0x7f648cb85ed0 (not E0x7f648cb85d10)))))
      (a!5 (or (and v0x7f648cb86510_0
                    E0x7f648cb87710
                    (<= v0x7f648cb87590_0 v0x7f648cb84d90_0)
                    (>= v0x7f648cb87590_0 v0x7f648cb84d90_0)
                    (<= v0x7f648cb87650_0 v0x7f648cb868d0_0)
                    (>= v0x7f648cb87650_0 v0x7f648cb868d0_0))
               (and v0x7f648cb87250_0
                    E0x7f648cb879d0
                    (and (<= v0x7f648cb87590_0 v0x7f648cb86fd0_0)
                         (>= v0x7f648cb87590_0 v0x7f648cb86fd0_0))
                    (<= v0x7f648cb87650_0 v0x7f648cb83a90_0)
                    (>= v0x7f648cb87650_0 v0x7f648cb83a90_0))
               (and v0x7f648cb86a10_0
                    E0x7f648cb87c90
                    (not v0x7f648cb87110_0)
                    (and (<= v0x7f648cb87590_0 v0x7f648cb86fd0_0)
                         (>= v0x7f648cb87590_0 v0x7f648cb86fd0_0))
                    (<= v0x7f648cb87650_0 0.0)
                    (>= v0x7f648cb87650_0 0.0))))
      (a!6 (=> v0x7f648cb874d0_0
               (or (and E0x7f648cb87710
                        (not E0x7f648cb879d0)
                        (not E0x7f648cb87c90))
                   (and E0x7f648cb879d0
                        (not E0x7f648cb87710)
                        (not E0x7f648cb87c90))
                   (and E0x7f648cb87c90
                        (not E0x7f648cb87710)
                        (not E0x7f648cb879d0))))))
(let ((a!7 (and (=> v0x7f648cb84590_0
                    (and v0x7f648cb842d0_0
                         E0x7f648cb84650
                         (not v0x7f648cb84450_0)))
                (=> v0x7f648cb84590_0 E0x7f648cb84650)
                a!1
                a!2
                (=> v0x7f648cb85650_0
                    (and v0x7f648cb84cd0_0
                         E0x7f648cb85710
                         (not v0x7f648cb85510_0)))
                (=> v0x7f648cb85650_0 E0x7f648cb85710)
                a!3
                a!4
                (=> v0x7f648cb86510_0
                    (and v0x7f648cb85b90_0 E0x7f648cb865d0 v0x7f648cb863d0_0))
                (=> v0x7f648cb86510_0 E0x7f648cb865d0)
                (=> v0x7f648cb86a10_0
                    (and v0x7f648cb85b90_0
                         E0x7f648cb86ad0
                         (not v0x7f648cb863d0_0)))
                (=> v0x7f648cb86a10_0 E0x7f648cb86ad0)
                (=> v0x7f648cb87250_0
                    (and v0x7f648cb86a10_0 E0x7f648cb87310 v0x7f648cb87110_0))
                (=> v0x7f648cb87250_0 E0x7f648cb87310)
                (=> v0x7f648cb874d0_0 a!5)
                a!6
                v0x7f648cb874d0_0
                (not v0x7f648cb886d0_0)
                (<= v0x7f648cb83cd0_0 v0x7f648cb87650_0)
                (>= v0x7f648cb83cd0_0 v0x7f648cb87650_0)
                (<= v0x7f648cb83dd0_0 v0x7f648cb85c50_0)
                (>= v0x7f648cb83dd0_0 v0x7f648cb85c50_0)
                (<= v0x7f648cb82010_0 v0x7f648cb87590_0)
                (>= v0x7f648cb82010_0 v0x7f648cb87590_0)
                (= v0x7f648cb84450_0 (= v0x7f648cb84390_0 0.0))
                (= v0x7f648cb84890_0 (< v0x7f648cb83d10_0 2.0))
                (= v0x7f648cb84a50_0 (ite v0x7f648cb84890_0 1.0 0.0))
                (= v0x7f648cb84b90_0 (+ v0x7f648cb84a50_0 v0x7f648cb83d10_0))
                (= v0x7f648cb85510_0 (= v0x7f648cb85450_0 0.0))
                (= v0x7f648cb85910_0 (= v0x7f648cb83c10_0 0.0))
                (= v0x7f648cb85a50_0 (ite v0x7f648cb85910_0 1.0 0.0))
                (= v0x7f648cb863d0_0 (= v0x7f648cb83a90_0 0.0))
                (= v0x7f648cb86790_0 (> v0x7f648cb84d90_0 1.0))
                (= v0x7f648cb868d0_0
                   (ite v0x7f648cb86790_0 1.0 v0x7f648cb83a90_0))
                (= v0x7f648cb86cd0_0 (> v0x7f648cb84d90_0 0.0))
                (= v0x7f648cb86e10_0 (+ v0x7f648cb84d90_0 (- 1.0)))
                (= v0x7f648cb86fd0_0
                   (ite v0x7f648cb86cd0_0 v0x7f648cb86e10_0 v0x7f648cb84d90_0))
                (= v0x7f648cb87110_0 (= v0x7f648cb85c50_0 0.0))
                (= v0x7f648cb881d0_0 (= v0x7f648cb87590_0 2.0))
                (= v0x7f648cb88310_0 (= v0x7f648cb87650_0 0.0))
                (= v0x7f648cb88450_0 (or v0x7f648cb88310_0 v0x7f648cb881d0_0))
                (= v0x7f648cb88590_0 (xor v0x7f648cb88450_0 true))
                (= v0x7f648cb886d0_0 (and v0x7f648cb863d0_0 v0x7f648cb88590_0)))))
  (=> F0x7f648cb89390 a!7))))
(assert (=> F0x7f648cb89390 F0x7f648cb892d0))
(assert (let ((a!1 (=> v0x7f648cb84cd0_0
               (or (and v0x7f648cb84590_0
                        E0x7f648cb84e50
                        (<= v0x7f648cb84d90_0 v0x7f648cb84b90_0)
                        (>= v0x7f648cb84d90_0 v0x7f648cb84b90_0))
                   (and v0x7f648cb842d0_0
                        E0x7f648cb85010
                        v0x7f648cb84450_0
                        (<= v0x7f648cb84d90_0 v0x7f648cb83d10_0)
                        (>= v0x7f648cb84d90_0 v0x7f648cb83d10_0)))))
      (a!2 (=> v0x7f648cb84cd0_0
               (or (and E0x7f648cb84e50 (not E0x7f648cb85010))
                   (and E0x7f648cb85010 (not E0x7f648cb84e50)))))
      (a!3 (=> v0x7f648cb85b90_0
               (or (and v0x7f648cb85650_0
                        E0x7f648cb85d10
                        (<= v0x7f648cb85c50_0 v0x7f648cb85a50_0)
                        (>= v0x7f648cb85c50_0 v0x7f648cb85a50_0))
                   (and v0x7f648cb84cd0_0
                        E0x7f648cb85ed0
                        v0x7f648cb85510_0
                        (<= v0x7f648cb85c50_0 v0x7f648cb83c10_0)
                        (>= v0x7f648cb85c50_0 v0x7f648cb83c10_0)))))
      (a!4 (=> v0x7f648cb85b90_0
               (or (and E0x7f648cb85d10 (not E0x7f648cb85ed0))
                   (and E0x7f648cb85ed0 (not E0x7f648cb85d10)))))
      (a!5 (or (and v0x7f648cb86510_0
                    E0x7f648cb87710
                    (<= v0x7f648cb87590_0 v0x7f648cb84d90_0)
                    (>= v0x7f648cb87590_0 v0x7f648cb84d90_0)
                    (<= v0x7f648cb87650_0 v0x7f648cb868d0_0)
                    (>= v0x7f648cb87650_0 v0x7f648cb868d0_0))
               (and v0x7f648cb87250_0
                    E0x7f648cb879d0
                    (and (<= v0x7f648cb87590_0 v0x7f648cb86fd0_0)
                         (>= v0x7f648cb87590_0 v0x7f648cb86fd0_0))
                    (<= v0x7f648cb87650_0 v0x7f648cb83a90_0)
                    (>= v0x7f648cb87650_0 v0x7f648cb83a90_0))
               (and v0x7f648cb86a10_0
                    E0x7f648cb87c90
                    (not v0x7f648cb87110_0)
                    (and (<= v0x7f648cb87590_0 v0x7f648cb86fd0_0)
                         (>= v0x7f648cb87590_0 v0x7f648cb86fd0_0))
                    (<= v0x7f648cb87650_0 0.0)
                    (>= v0x7f648cb87650_0 0.0))))
      (a!6 (=> v0x7f648cb874d0_0
               (or (and E0x7f648cb87710
                        (not E0x7f648cb879d0)
                        (not E0x7f648cb87c90))
                   (and E0x7f648cb879d0
                        (not E0x7f648cb87710)
                        (not E0x7f648cb87c90))
                   (and E0x7f648cb87c90
                        (not E0x7f648cb87710)
                        (not E0x7f648cb879d0))))))
(let ((a!7 (and (=> v0x7f648cb84590_0
                    (and v0x7f648cb842d0_0
                         E0x7f648cb84650
                         (not v0x7f648cb84450_0)))
                (=> v0x7f648cb84590_0 E0x7f648cb84650)
                a!1
                a!2
                (=> v0x7f648cb85650_0
                    (and v0x7f648cb84cd0_0
                         E0x7f648cb85710
                         (not v0x7f648cb85510_0)))
                (=> v0x7f648cb85650_0 E0x7f648cb85710)
                a!3
                a!4
                (=> v0x7f648cb86510_0
                    (and v0x7f648cb85b90_0 E0x7f648cb865d0 v0x7f648cb863d0_0))
                (=> v0x7f648cb86510_0 E0x7f648cb865d0)
                (=> v0x7f648cb86a10_0
                    (and v0x7f648cb85b90_0
                         E0x7f648cb86ad0
                         (not v0x7f648cb863d0_0)))
                (=> v0x7f648cb86a10_0 E0x7f648cb86ad0)
                (=> v0x7f648cb87250_0
                    (and v0x7f648cb86a10_0 E0x7f648cb87310 v0x7f648cb87110_0))
                (=> v0x7f648cb87250_0 E0x7f648cb87310)
                (=> v0x7f648cb874d0_0 a!5)
                a!6
                v0x7f648cb874d0_0
                v0x7f648cb886d0_0
                (= v0x7f648cb84450_0 (= v0x7f648cb84390_0 0.0))
                (= v0x7f648cb84890_0 (< v0x7f648cb83d10_0 2.0))
                (= v0x7f648cb84a50_0 (ite v0x7f648cb84890_0 1.0 0.0))
                (= v0x7f648cb84b90_0 (+ v0x7f648cb84a50_0 v0x7f648cb83d10_0))
                (= v0x7f648cb85510_0 (= v0x7f648cb85450_0 0.0))
                (= v0x7f648cb85910_0 (= v0x7f648cb83c10_0 0.0))
                (= v0x7f648cb85a50_0 (ite v0x7f648cb85910_0 1.0 0.0))
                (= v0x7f648cb863d0_0 (= v0x7f648cb83a90_0 0.0))
                (= v0x7f648cb86790_0 (> v0x7f648cb84d90_0 1.0))
                (= v0x7f648cb868d0_0
                   (ite v0x7f648cb86790_0 1.0 v0x7f648cb83a90_0))
                (= v0x7f648cb86cd0_0 (> v0x7f648cb84d90_0 0.0))
                (= v0x7f648cb86e10_0 (+ v0x7f648cb84d90_0 (- 1.0)))
                (= v0x7f648cb86fd0_0
                   (ite v0x7f648cb86cd0_0 v0x7f648cb86e10_0 v0x7f648cb84d90_0))
                (= v0x7f648cb87110_0 (= v0x7f648cb85c50_0 0.0))
                (= v0x7f648cb881d0_0 (= v0x7f648cb87590_0 2.0))
                (= v0x7f648cb88310_0 (= v0x7f648cb87650_0 0.0))
                (= v0x7f648cb88450_0 (or v0x7f648cb88310_0 v0x7f648cb881d0_0))
                (= v0x7f648cb88590_0 (xor v0x7f648cb88450_0 true))
                (= v0x7f648cb886d0_0 (and v0x7f648cb863d0_0 v0x7f648cb88590_0)))))
  (=> F0x7f648cb89510 a!7))))
(assert (=> F0x7f648cb89510 F0x7f648cb892d0))
(assert (=> F0x7f648cb89610 (or F0x7f648cb894d0 F0x7f648cb89390)))
(assert (=> F0x7f648cb895d0 F0x7f648cb89510))
(assert (=> pre!entry!0 (=> F0x7f648cb89290 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f648cb892d0 (>= v0x7f648cb83c10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f648cb892d0 (>= v0x7f648cb83d10_0 0.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f648cb892d0
        (or (<= v0x7f648cb83d10_0 1.0) (>= v0x7f648cb83d10_0 2.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f648cb892d0 (<= v0x7f648cb83d10_0 2.0))))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f648cb892d0
        (or (<= v0x7f648cb83d10_0 0.0) (>= v0x7f648cb83d10_0 1.0)))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7f648cb892d0
        (or (<= v0x7f648cb83d10_0 1.0) (>= v0x7f648cb83a90_0 1.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!2)
                F0x7f648cb89610
                (not (or (<= v0x7f648cb82010_0 1.0) (>= v0x7f648cb82010_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!4)
                F0x7f648cb89610
                (not (or (<= v0x7f648cb82010_0 0.0) (>= v0x7f648cb82010_0 1.0)))))
      (a!3 (and (not post!bb1.i.i!5)
                F0x7f648cb89610
                (not (or (<= v0x7f648cb82010_0 1.0) (>= v0x7f648cb83cd0_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f648cb89610
           (not (>= v0x7f648cb83dd0_0 0.0)))
      (and (not post!bb1.i.i!1)
           F0x7f648cb89610
           (not (>= v0x7f648cb82010_0 0.0)))
      a!1
      (and (not post!bb1.i.i!3)
           F0x7f648cb89610
           (not (<= v0x7f648cb82010_0 2.0)))
      a!2
      a!3
      (and (not post!bb2.i.i25.i.i!0) F0x7f648cb895d0 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i25.i.i!0)
