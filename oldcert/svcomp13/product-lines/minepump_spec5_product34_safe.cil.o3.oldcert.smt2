(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f68ba08e850 () Bool)
(declare-fun F0x7f68ba08e510 () Bool)
(declare-fun v0x7f68ba08c390_0 () Real)
(declare-fun v0x7f68ba08af10_0 () Real)
(declare-fun v0x7f68ba08ad50_0 () Bool)
(declare-fun v0x7f68ba08a850_0 () Real)
(declare-fun v0x7f68ba08da50_0 () Bool)
(declare-fun v0x7f68ba08c550_0 () Real)
(declare-fun E0x7f68ba08d090 () Bool)
(declare-fun v0x7f68ba08a190_0 () Real)
(declare-fun v0x7f68ba08ca10_0 () Real)
(declare-fun E0x7f68ba08cb90 () Bool)
(declare-fun v0x7f68ba08be50_0 () Bool)
(declare-fun v0x7f68ba08c690_0 () Bool)
(declare-fun F0x7f68ba08e790 () Bool)
(declare-fun E0x7f68ba08c050 () Bool)
(declare-fun v0x7f68ba08ba90_0 () Bool)
(declare-fun v0x7f68ba08a310_0 () Real)
(declare-fun v0x7f68ba08b250_0 () Real)
(declare-fun E0x7f68ba08b310 () Bool)
(declare-fun v0x7f68ba08b190_0 () Bool)
(declare-fun v0x7f68ba08cad0_0 () Real)
(declare-fun v0x7f68ba08a910_0 () Bool)
(declare-fun v0x7f68ba08c950_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f68ba08ab10 () Bool)
(declare-fun v0x7f68ba08a790_0 () Bool)
(declare-fun v0x7f68ba08d7d0_0 () Bool)
(declare-fun v0x7f68ba08aa50_0 () Bool)
(declare-fun v0x7f68ba08c250_0 () Bool)
(declare-fun E0x7f68ba08b4d0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f68ba08d910_0 () Bool)
(declare-fun F0x7f68ba08e690 () Bool)
(declare-fun v0x7f68ba089010_0 () Real)
(declare-fun v0x7f68ba08a3d0_0 () Real)
(declare-fun E0x7f68ba08c750 () Bool)
(declare-fun E0x7f68ba08ce50 () Bool)
(declare-fun v0x7f68ba08b050_0 () Real)
(declare-fun v0x7f68ba08d690_0 () Bool)
(declare-fun v0x7f68ba08bbd0_0 () Bool)
(declare-fun v0x7f68ba089110_0 () Bool)
(declare-fun F0x7f68ba08e5d0 () Bool)
(declare-fun v0x7f68ba08bf90_0 () Bool)
(declare-fun E0x7f68ba08bc90 () Bool)
(declare-fun F0x7f68ba08e890 () Bool)
(declare-fun v0x7f68ba08d550_0 () Bool)
(declare-fun F0x7f68ba08e750 () Bool)

(assert (=> F0x7f68ba08e750
    (and v0x7f68ba089110_0
         (<= v0x7f68ba08a3d0_0 0.0)
         (>= v0x7f68ba08a3d0_0 0.0)
         (<= v0x7f68ba089010_0 1.0)
         (>= v0x7f68ba089010_0 1.0))))
(assert (=> F0x7f68ba08e750 F0x7f68ba08e690))
(assert (let ((a!1 (=> v0x7f68ba08b190_0
               (or (and v0x7f68ba08aa50_0
                        E0x7f68ba08b310
                        (<= v0x7f68ba08b250_0 v0x7f68ba08b050_0)
                        (>= v0x7f68ba08b250_0 v0x7f68ba08b050_0))
                   (and v0x7f68ba08a790_0
                        E0x7f68ba08b4d0
                        v0x7f68ba08a910_0
                        (<= v0x7f68ba08b250_0 v0x7f68ba08a310_0)
                        (>= v0x7f68ba08b250_0 v0x7f68ba08a310_0)))))
      (a!2 (=> v0x7f68ba08b190_0
               (or (and E0x7f68ba08b310 (not E0x7f68ba08b4d0))
                   (and E0x7f68ba08b4d0 (not E0x7f68ba08b310)))))
      (a!3 (or (and v0x7f68ba08c690_0
                    E0x7f68ba08cb90
                    (and (<= v0x7f68ba08ca10_0 v0x7f68ba08b250_0)
                         (>= v0x7f68ba08ca10_0 v0x7f68ba08b250_0))
                    (and (<= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)
                         (>= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)))
               (and v0x7f68ba08bbd0_0
                    E0x7f68ba08ce50
                    v0x7f68ba08be50_0
                    (and (<= v0x7f68ba08ca10_0 v0x7f68ba08b250_0)
                         (>= v0x7f68ba08ca10_0 v0x7f68ba08b250_0))
                    (<= v0x7f68ba08cad0_0 1.0)
                    (>= v0x7f68ba08cad0_0 1.0))
               (and v0x7f68ba08bf90_0
                    E0x7f68ba08d090
                    (<= v0x7f68ba08ca10_0 v0x7f68ba08c550_0)
                    (>= v0x7f68ba08ca10_0 v0x7f68ba08c550_0)
                    (and (<= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)
                         (>= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)))))
      (a!4 (=> v0x7f68ba08c950_0
               (or (and E0x7f68ba08cb90
                        (not E0x7f68ba08ce50)
                        (not E0x7f68ba08d090))
                   (and E0x7f68ba08ce50
                        (not E0x7f68ba08cb90)
                        (not E0x7f68ba08d090))
                   (and E0x7f68ba08d090
                        (not E0x7f68ba08cb90)
                        (not E0x7f68ba08ce50))))))
(let ((a!5 (and (=> v0x7f68ba08aa50_0
                    (and v0x7f68ba08a790_0
                         E0x7f68ba08ab10
                         (not v0x7f68ba08a910_0)))
                (=> v0x7f68ba08aa50_0 E0x7f68ba08ab10)
                a!1
                a!2
                (=> v0x7f68ba08bbd0_0
                    (and v0x7f68ba08b190_0 E0x7f68ba08bc90 v0x7f68ba08ba90_0))
                (=> v0x7f68ba08bbd0_0 E0x7f68ba08bc90)
                (=> v0x7f68ba08bf90_0
                    (and v0x7f68ba08b190_0
                         E0x7f68ba08c050
                         (not v0x7f68ba08ba90_0)))
                (=> v0x7f68ba08bf90_0 E0x7f68ba08c050)
                (=> v0x7f68ba08c690_0
                    (and v0x7f68ba08bbd0_0
                         E0x7f68ba08c750
                         (not v0x7f68ba08be50_0)))
                (=> v0x7f68ba08c690_0 E0x7f68ba08c750)
                (=> v0x7f68ba08c950_0 a!3)
                a!4
                v0x7f68ba08c950_0
                (not v0x7f68ba08da50_0)
                (<= v0x7f68ba08a3d0_0 v0x7f68ba08cad0_0)
                (>= v0x7f68ba08a3d0_0 v0x7f68ba08cad0_0)
                (<= v0x7f68ba089010_0 v0x7f68ba08ca10_0)
                (>= v0x7f68ba089010_0 v0x7f68ba08ca10_0)
                (= v0x7f68ba08a910_0 (= v0x7f68ba08a850_0 0.0))
                (= v0x7f68ba08ad50_0 (< v0x7f68ba08a310_0 2.0))
                (= v0x7f68ba08af10_0 (ite v0x7f68ba08ad50_0 1.0 0.0))
                (= v0x7f68ba08b050_0 (+ v0x7f68ba08af10_0 v0x7f68ba08a310_0))
                (= v0x7f68ba08ba90_0 (= v0x7f68ba08a190_0 0.0))
                (= v0x7f68ba08be50_0 (> v0x7f68ba08b250_0 1.0))
                (= v0x7f68ba08c250_0 (> v0x7f68ba08b250_0 0.0))
                (= v0x7f68ba08c390_0 (+ v0x7f68ba08b250_0 (- 1.0)))
                (= v0x7f68ba08c550_0
                   (ite v0x7f68ba08c250_0 v0x7f68ba08c390_0 v0x7f68ba08b250_0))
                (= v0x7f68ba08d550_0 (= v0x7f68ba08ca10_0 2.0))
                (= v0x7f68ba08d690_0 (= v0x7f68ba08cad0_0 0.0))
                (= v0x7f68ba08d7d0_0 (or v0x7f68ba08d690_0 v0x7f68ba08d550_0))
                (= v0x7f68ba08d910_0 (xor v0x7f68ba08d7d0_0 true))
                (= v0x7f68ba08da50_0 (and v0x7f68ba08ba90_0 v0x7f68ba08d910_0)))))
  (=> F0x7f68ba08e5d0 a!5))))
(assert (=> F0x7f68ba08e5d0 F0x7f68ba08e510))
(assert (let ((a!1 (=> v0x7f68ba08b190_0
               (or (and v0x7f68ba08aa50_0
                        E0x7f68ba08b310
                        (<= v0x7f68ba08b250_0 v0x7f68ba08b050_0)
                        (>= v0x7f68ba08b250_0 v0x7f68ba08b050_0))
                   (and v0x7f68ba08a790_0
                        E0x7f68ba08b4d0
                        v0x7f68ba08a910_0
                        (<= v0x7f68ba08b250_0 v0x7f68ba08a310_0)
                        (>= v0x7f68ba08b250_0 v0x7f68ba08a310_0)))))
      (a!2 (=> v0x7f68ba08b190_0
               (or (and E0x7f68ba08b310 (not E0x7f68ba08b4d0))
                   (and E0x7f68ba08b4d0 (not E0x7f68ba08b310)))))
      (a!3 (or (and v0x7f68ba08c690_0
                    E0x7f68ba08cb90
                    (and (<= v0x7f68ba08ca10_0 v0x7f68ba08b250_0)
                         (>= v0x7f68ba08ca10_0 v0x7f68ba08b250_0))
                    (and (<= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)
                         (>= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)))
               (and v0x7f68ba08bbd0_0
                    E0x7f68ba08ce50
                    v0x7f68ba08be50_0
                    (and (<= v0x7f68ba08ca10_0 v0x7f68ba08b250_0)
                         (>= v0x7f68ba08ca10_0 v0x7f68ba08b250_0))
                    (<= v0x7f68ba08cad0_0 1.0)
                    (>= v0x7f68ba08cad0_0 1.0))
               (and v0x7f68ba08bf90_0
                    E0x7f68ba08d090
                    (<= v0x7f68ba08ca10_0 v0x7f68ba08c550_0)
                    (>= v0x7f68ba08ca10_0 v0x7f68ba08c550_0)
                    (and (<= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)
                         (>= v0x7f68ba08cad0_0 v0x7f68ba08a190_0)))))
      (a!4 (=> v0x7f68ba08c950_0
               (or (and E0x7f68ba08cb90
                        (not E0x7f68ba08ce50)
                        (not E0x7f68ba08d090))
                   (and E0x7f68ba08ce50
                        (not E0x7f68ba08cb90)
                        (not E0x7f68ba08d090))
                   (and E0x7f68ba08d090
                        (not E0x7f68ba08cb90)
                        (not E0x7f68ba08ce50))))))
(let ((a!5 (and (=> v0x7f68ba08aa50_0
                    (and v0x7f68ba08a790_0
                         E0x7f68ba08ab10
                         (not v0x7f68ba08a910_0)))
                (=> v0x7f68ba08aa50_0 E0x7f68ba08ab10)
                a!1
                a!2
                (=> v0x7f68ba08bbd0_0
                    (and v0x7f68ba08b190_0 E0x7f68ba08bc90 v0x7f68ba08ba90_0))
                (=> v0x7f68ba08bbd0_0 E0x7f68ba08bc90)
                (=> v0x7f68ba08bf90_0
                    (and v0x7f68ba08b190_0
                         E0x7f68ba08c050
                         (not v0x7f68ba08ba90_0)))
                (=> v0x7f68ba08bf90_0 E0x7f68ba08c050)
                (=> v0x7f68ba08c690_0
                    (and v0x7f68ba08bbd0_0
                         E0x7f68ba08c750
                         (not v0x7f68ba08be50_0)))
                (=> v0x7f68ba08c690_0 E0x7f68ba08c750)
                (=> v0x7f68ba08c950_0 a!3)
                a!4
                v0x7f68ba08c950_0
                v0x7f68ba08da50_0
                (= v0x7f68ba08a910_0 (= v0x7f68ba08a850_0 0.0))
                (= v0x7f68ba08ad50_0 (< v0x7f68ba08a310_0 2.0))
                (= v0x7f68ba08af10_0 (ite v0x7f68ba08ad50_0 1.0 0.0))
                (= v0x7f68ba08b050_0 (+ v0x7f68ba08af10_0 v0x7f68ba08a310_0))
                (= v0x7f68ba08ba90_0 (= v0x7f68ba08a190_0 0.0))
                (= v0x7f68ba08be50_0 (> v0x7f68ba08b250_0 1.0))
                (= v0x7f68ba08c250_0 (> v0x7f68ba08b250_0 0.0))
                (= v0x7f68ba08c390_0 (+ v0x7f68ba08b250_0 (- 1.0)))
                (= v0x7f68ba08c550_0
                   (ite v0x7f68ba08c250_0 v0x7f68ba08c390_0 v0x7f68ba08b250_0))
                (= v0x7f68ba08d550_0 (= v0x7f68ba08ca10_0 2.0))
                (= v0x7f68ba08d690_0 (= v0x7f68ba08cad0_0 0.0))
                (= v0x7f68ba08d7d0_0 (or v0x7f68ba08d690_0 v0x7f68ba08d550_0))
                (= v0x7f68ba08d910_0 (xor v0x7f68ba08d7d0_0 true))
                (= v0x7f68ba08da50_0 (and v0x7f68ba08ba90_0 v0x7f68ba08d910_0)))))
  (=> F0x7f68ba08e790 a!5))))
(assert (=> F0x7f68ba08e790 F0x7f68ba08e510))
(assert (=> F0x7f68ba08e890 (or F0x7f68ba08e750 F0x7f68ba08e5d0)))
(assert (=> F0x7f68ba08e850 F0x7f68ba08e790))
(assert (=> pre!entry!0 (=> F0x7f68ba08e690 true)))
(assert (let ((a!1 (not (or (not (>= v0x7f68ba08a310_0 1.0))
                    (not (<= v0x7f68ba08a190_0 0.0))
                    (not (<= v0x7f68ba08a310_0 1.0))
                    (not (>= v0x7f68ba08a190_0 0.0))))))
  (=> pre!bb1.i.i!0 (=> F0x7f68ba08e510 (or (>= v0x7f68ba08a190_0 1.0) a!1)))))
(assert (let ((a!1 (not (or (not (>= v0x7f68ba089010_0 1.0))
                    (not (<= v0x7f68ba08a3d0_0 0.0))
                    (not (<= v0x7f68ba089010_0 1.0))
                    (not (>= v0x7f68ba08a3d0_0 0.0))))))
(let ((a!2 (and (not post!bb1.i.i!0)
                F0x7f68ba08e890
                (not (or (>= v0x7f68ba08a3d0_0 1.0) a!1)))))
  (or a!2 (and (not post!bb2.i.i16.i.i!0) F0x7f68ba08e850 true)))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
;(post-assumptions: post!bb1.i.i!0 post!bb2.i.i16.i.i!0)
