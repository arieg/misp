(declare-fun post!bb1.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f1aef5f9c50 () Bool)
(declare-fun F0x7f1aef5f9b50 () Bool)
(declare-fun v0x7f1aef5f8c10_0 () Bool)
(declare-fun v0x7f1aef5f5750_0 () Real)
(declare-fun v0x7f1aef5f4d50_0 () Real)
(declare-fun v0x7f1aef5f4690_0 () Real)
(declare-fun v0x7f1aef5f8d50_0 () Bool)
(declare-fun E0x7f1aef5f8510 () Bool)
(declare-fun v0x7f1aef5f4b90_0 () Bool)
(declare-fun v0x7f1aef5f5c10_0 () Bool)
(declare-fun E0x7f1aef5f8310 () Bool)
(declare-fun v0x7f1aef5f3d90_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f1aef5f8090 () Bool)
(declare-fun v0x7f1aef5f7d10_0 () Real)
(declare-fun v0x7f1aef5f7c50_0 () Real)
(declare-fun v0x7f1aef5f6e90_0 () Bool)
(declare-fun E0x7f1aef5f7dd0 () Bool)
(declare-fun v0x7f1aef5f7b90_0 () Bool)
(declare-fun v0x7f1aef5f72d0_0 () Bool)
(declare-fun v0x7f1aef5f7190_0 () Real)
(declare-fun E0x7f1aef5f79d0 () Bool)
(declare-fun E0x7f1aef5f74d0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f1aef5f6bd0_0 () Bool)
(declare-fun v0x7f1aef5f6810_0 () Bool)
(declare-fun v0x7f1aef5f7910_0 () Bool)
(declare-fun v0x7f1aef5f5d50_0 () Real)
(declare-fun E0x7f1aef5f6c90 () Bool)
(declare-fun F0x7f1aef5f9a90 () Bool)
(declare-fun v0x7f1aef5f5f50_0 () Real)
(declare-fun E0x7f1aef5f5a10 () Bool)
(declare-fun E0x7f1aef5f68d0 () Bool)
(declare-fun v0x7f1aef5f4e90_0 () Real)
(declare-fun v0x7f1aef5f7690_0 () Bool)
(declare-fun v0x7f1aef5f66d0_0 () Bool)
(declare-fun v0x7f1aef5f5950_0 () Bool)
(declare-fun v0x7f1aef5f4fd0_0 () Bool)
(declare-fun E0x7f1aef5f6010 () Bool)
(declare-fun E0x7f1aef5f5150 () Bool)
(declare-fun v0x7f1aef5f4750_0 () Bool)
(declare-fun v0x7f1aef5f45d0_0 () Bool)
(declare-fun v0x7f1aef5f5e90_0 () Bool)
(declare-fun F0x7f1aef5f99d0 () Bool)
(declare-fun v0x7f1aef5f6fd0_0 () Real)
(declare-fun v0x7f1aef5f5090_0 () Real)
(declare-fun v0x7f1aef5f6a90_0 () Bool)
(declare-fun F0x7f1aef5f98d0 () Bool)
(declare-fun v0x7f1aef5f4890_0 () Bool)
(declare-fun E0x7f1aef5f5310 () Bool)
(declare-fun v0x7f1aef5f2010_0 () Real)
(declare-fun v0x7f1aef5f4010_0 () Real)
(declare-fun v0x7f1aef5f40d0_0 () Real)
(declare-fun v0x7f1aef5f3f10_0 () Real)
(declare-fun F0x7f1aef5f9c90 () Bool)
(declare-fun v0x7f1aef5f3fd0_0 () Real)
(declare-fun E0x7f1aef5f61d0 () Bool)
(declare-fun E0x7f1aef5f4950 () Bool)
(declare-fun v0x7f1aef5f7410_0 () Bool)
(declare-fun v0x7f1aef5f2110_0 () Bool)
(declare-fun v0x7f1aef5f5810_0 () Bool)
(declare-fun v0x7f1aef5f8ad0_0 () Bool)
(declare-fun F0x7f1aef5f9990 () Bool)
(declare-fun v0x7f1aef5f77d0_0 () Real)

(assert (=> F0x7f1aef5f9990
    (and v0x7f1aef5f2110_0
         (<= v0x7f1aef5f3fd0_0 0.0)
         (>= v0x7f1aef5f3fd0_0 0.0)
         (<= v0x7f1aef5f40d0_0 0.0)
         (>= v0x7f1aef5f40d0_0 0.0)
         (<= v0x7f1aef5f2010_0 1.0)
         (>= v0x7f1aef5f2010_0 1.0))))
(assert (=> F0x7f1aef5f9990 F0x7f1aef5f98d0))
(assert (let ((a!1 (=> v0x7f1aef5f4fd0_0
               (or (and v0x7f1aef5f4890_0
                        E0x7f1aef5f5150
                        (<= v0x7f1aef5f5090_0 v0x7f1aef5f4e90_0)
                        (>= v0x7f1aef5f5090_0 v0x7f1aef5f4e90_0))
                   (and v0x7f1aef5f45d0_0
                        E0x7f1aef5f5310
                        v0x7f1aef5f4750_0
                        (<= v0x7f1aef5f5090_0 v0x7f1aef5f4010_0)
                        (>= v0x7f1aef5f5090_0 v0x7f1aef5f4010_0)))))
      (a!2 (=> v0x7f1aef5f4fd0_0
               (or (and E0x7f1aef5f5150 (not E0x7f1aef5f5310))
                   (and E0x7f1aef5f5310 (not E0x7f1aef5f5150)))))
      (a!3 (=> v0x7f1aef5f5e90_0
               (or (and v0x7f1aef5f5950_0
                        E0x7f1aef5f6010
                        (<= v0x7f1aef5f5f50_0 v0x7f1aef5f5d50_0)
                        (>= v0x7f1aef5f5f50_0 v0x7f1aef5f5d50_0))
                   (and v0x7f1aef5f4fd0_0
                        E0x7f1aef5f61d0
                        v0x7f1aef5f5810_0
                        (<= v0x7f1aef5f5f50_0 v0x7f1aef5f3f10_0)
                        (>= v0x7f1aef5f5f50_0 v0x7f1aef5f3f10_0)))))
      (a!4 (=> v0x7f1aef5f5e90_0
               (or (and E0x7f1aef5f6010 (not E0x7f1aef5f61d0))
                   (and E0x7f1aef5f61d0 (not E0x7f1aef5f6010)))))
      (a!5 (or (and v0x7f1aef5f7410_0
                    E0x7f1aef5f7dd0
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0))
                    (<= v0x7f1aef5f7d10_0 v0x7f1aef5f77d0_0)
                    (>= v0x7f1aef5f7d10_0 v0x7f1aef5f77d0_0))
               (and v0x7f1aef5f6810_0
                    E0x7f1aef5f8090
                    (not v0x7f1aef5f6a90_0)
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0))
                    (and (<= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)
                         (>= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)))
               (and v0x7f1aef5f7910_0
                    E0x7f1aef5f8310
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0))
                    (and (<= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)
                         (>= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)))
               (and v0x7f1aef5f6bd0_0
                    E0x7f1aef5f8510
                    (not v0x7f1aef5f72d0_0)
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0))
                    (<= v0x7f1aef5f7d10_0 0.0)
                    (>= v0x7f1aef5f7d10_0 0.0))))
      (a!6 (=> v0x7f1aef5f7b90_0
               (or (and E0x7f1aef5f7dd0
                        (not E0x7f1aef5f8090)
                        (not E0x7f1aef5f8310)
                        (not E0x7f1aef5f8510))
                   (and E0x7f1aef5f8090
                        (not E0x7f1aef5f7dd0)
                        (not E0x7f1aef5f8310)
                        (not E0x7f1aef5f8510))
                   (and E0x7f1aef5f8310
                        (not E0x7f1aef5f7dd0)
                        (not E0x7f1aef5f8090)
                        (not E0x7f1aef5f8510))
                   (and E0x7f1aef5f8510
                        (not E0x7f1aef5f7dd0)
                        (not E0x7f1aef5f8090)
                        (not E0x7f1aef5f8310))))))
(let ((a!7 (and (=> v0x7f1aef5f4890_0
                    (and v0x7f1aef5f45d0_0
                         E0x7f1aef5f4950
                         (not v0x7f1aef5f4750_0)))
                (=> v0x7f1aef5f4890_0 E0x7f1aef5f4950)
                a!1
                a!2
                (=> v0x7f1aef5f5950_0
                    (and v0x7f1aef5f4fd0_0
                         E0x7f1aef5f5a10
                         (not v0x7f1aef5f5810_0)))
                (=> v0x7f1aef5f5950_0 E0x7f1aef5f5a10)
                a!3
                a!4
                (=> v0x7f1aef5f6810_0
                    (and v0x7f1aef5f5e90_0 E0x7f1aef5f68d0 v0x7f1aef5f66d0_0))
                (=> v0x7f1aef5f6810_0 E0x7f1aef5f68d0)
                (=> v0x7f1aef5f6bd0_0
                    (and v0x7f1aef5f5e90_0
                         E0x7f1aef5f6c90
                         (not v0x7f1aef5f66d0_0)))
                (=> v0x7f1aef5f6bd0_0 E0x7f1aef5f6c90)
                (=> v0x7f1aef5f7410_0
                    (and v0x7f1aef5f6810_0 E0x7f1aef5f74d0 v0x7f1aef5f6a90_0))
                (=> v0x7f1aef5f7410_0 E0x7f1aef5f74d0)
                (=> v0x7f1aef5f7910_0
                    (and v0x7f1aef5f6bd0_0 E0x7f1aef5f79d0 v0x7f1aef5f72d0_0))
                (=> v0x7f1aef5f7910_0 E0x7f1aef5f79d0)
                (=> v0x7f1aef5f7b90_0 a!5)
                a!6
                v0x7f1aef5f7b90_0
                v0x7f1aef5f8d50_0
                (<= v0x7f1aef5f3fd0_0 v0x7f1aef5f7d10_0)
                (>= v0x7f1aef5f3fd0_0 v0x7f1aef5f7d10_0)
                (<= v0x7f1aef5f40d0_0 v0x7f1aef5f5f50_0)
                (>= v0x7f1aef5f40d0_0 v0x7f1aef5f5f50_0)
                (<= v0x7f1aef5f2010_0 v0x7f1aef5f7c50_0)
                (>= v0x7f1aef5f2010_0 v0x7f1aef5f7c50_0)
                (= v0x7f1aef5f4750_0 (= v0x7f1aef5f4690_0 0.0))
                (= v0x7f1aef5f4b90_0 (< v0x7f1aef5f4010_0 2.0))
                (= v0x7f1aef5f4d50_0 (ite v0x7f1aef5f4b90_0 1.0 0.0))
                (= v0x7f1aef5f4e90_0 (+ v0x7f1aef5f4d50_0 v0x7f1aef5f4010_0))
                (= v0x7f1aef5f5810_0 (= v0x7f1aef5f5750_0 0.0))
                (= v0x7f1aef5f5c10_0 (= v0x7f1aef5f3f10_0 0.0))
                (= v0x7f1aef5f5d50_0 (ite v0x7f1aef5f5c10_0 1.0 0.0))
                (= v0x7f1aef5f66d0_0 (= v0x7f1aef5f3d90_0 0.0))
                (= v0x7f1aef5f6a90_0 (> v0x7f1aef5f5090_0 1.0))
                (= v0x7f1aef5f6e90_0 (> v0x7f1aef5f5090_0 0.0))
                (= v0x7f1aef5f6fd0_0 (+ v0x7f1aef5f5090_0 (- 1.0)))
                (= v0x7f1aef5f7190_0
                   (ite v0x7f1aef5f6e90_0 v0x7f1aef5f6fd0_0 v0x7f1aef5f5090_0))
                (= v0x7f1aef5f72d0_0 (= v0x7f1aef5f7190_0 0.0))
                (= v0x7f1aef5f7690_0 (= v0x7f1aef5f5f50_0 0.0))
                (= v0x7f1aef5f77d0_0
                   (ite v0x7f1aef5f7690_0 1.0 v0x7f1aef5f3d90_0))
                (= v0x7f1aef5f8ad0_0 (not (= v0x7f1aef5f7c50_0 0.0)))
                (= v0x7f1aef5f8c10_0 (= v0x7f1aef5f7d10_0 0.0))
                (= v0x7f1aef5f8d50_0 (or v0x7f1aef5f8c10_0 v0x7f1aef5f8ad0_0)))))
  (=> F0x7f1aef5f99d0 a!7))))
(assert (=> F0x7f1aef5f99d0 F0x7f1aef5f9a90))
(assert (let ((a!1 (=> v0x7f1aef5f4fd0_0
               (or (and v0x7f1aef5f4890_0
                        E0x7f1aef5f5150
                        (<= v0x7f1aef5f5090_0 v0x7f1aef5f4e90_0)
                        (>= v0x7f1aef5f5090_0 v0x7f1aef5f4e90_0))
                   (and v0x7f1aef5f45d0_0
                        E0x7f1aef5f5310
                        v0x7f1aef5f4750_0
                        (<= v0x7f1aef5f5090_0 v0x7f1aef5f4010_0)
                        (>= v0x7f1aef5f5090_0 v0x7f1aef5f4010_0)))))
      (a!2 (=> v0x7f1aef5f4fd0_0
               (or (and E0x7f1aef5f5150 (not E0x7f1aef5f5310))
                   (and E0x7f1aef5f5310 (not E0x7f1aef5f5150)))))
      (a!3 (=> v0x7f1aef5f5e90_0
               (or (and v0x7f1aef5f5950_0
                        E0x7f1aef5f6010
                        (<= v0x7f1aef5f5f50_0 v0x7f1aef5f5d50_0)
                        (>= v0x7f1aef5f5f50_0 v0x7f1aef5f5d50_0))
                   (and v0x7f1aef5f4fd0_0
                        E0x7f1aef5f61d0
                        v0x7f1aef5f5810_0
                        (<= v0x7f1aef5f5f50_0 v0x7f1aef5f3f10_0)
                        (>= v0x7f1aef5f5f50_0 v0x7f1aef5f3f10_0)))))
      (a!4 (=> v0x7f1aef5f5e90_0
               (or (and E0x7f1aef5f6010 (not E0x7f1aef5f61d0))
                   (and E0x7f1aef5f61d0 (not E0x7f1aef5f6010)))))
      (a!5 (or (and v0x7f1aef5f7410_0
                    E0x7f1aef5f7dd0
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0))
                    (<= v0x7f1aef5f7d10_0 v0x7f1aef5f77d0_0)
                    (>= v0x7f1aef5f7d10_0 v0x7f1aef5f77d0_0))
               (and v0x7f1aef5f6810_0
                    E0x7f1aef5f8090
                    (not v0x7f1aef5f6a90_0)
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f5090_0))
                    (and (<= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)
                         (>= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)))
               (and v0x7f1aef5f7910_0
                    E0x7f1aef5f8310
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0))
                    (and (<= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)
                         (>= v0x7f1aef5f7d10_0 v0x7f1aef5f3d90_0)))
               (and v0x7f1aef5f6bd0_0
                    E0x7f1aef5f8510
                    (not v0x7f1aef5f72d0_0)
                    (and (<= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0)
                         (>= v0x7f1aef5f7c50_0 v0x7f1aef5f7190_0))
                    (<= v0x7f1aef5f7d10_0 0.0)
                    (>= v0x7f1aef5f7d10_0 0.0))))
      (a!6 (=> v0x7f1aef5f7b90_0
               (or (and E0x7f1aef5f7dd0
                        (not E0x7f1aef5f8090)
                        (not E0x7f1aef5f8310)
                        (not E0x7f1aef5f8510))
                   (and E0x7f1aef5f8090
                        (not E0x7f1aef5f7dd0)
                        (not E0x7f1aef5f8310)
                        (not E0x7f1aef5f8510))
                   (and E0x7f1aef5f8310
                        (not E0x7f1aef5f7dd0)
                        (not E0x7f1aef5f8090)
                        (not E0x7f1aef5f8510))
                   (and E0x7f1aef5f8510
                        (not E0x7f1aef5f7dd0)
                        (not E0x7f1aef5f8090)
                        (not E0x7f1aef5f8310))))))
(let ((a!7 (and (=> v0x7f1aef5f4890_0
                    (and v0x7f1aef5f45d0_0
                         E0x7f1aef5f4950
                         (not v0x7f1aef5f4750_0)))
                (=> v0x7f1aef5f4890_0 E0x7f1aef5f4950)
                a!1
                a!2
                (=> v0x7f1aef5f5950_0
                    (and v0x7f1aef5f4fd0_0
                         E0x7f1aef5f5a10
                         (not v0x7f1aef5f5810_0)))
                (=> v0x7f1aef5f5950_0 E0x7f1aef5f5a10)
                a!3
                a!4
                (=> v0x7f1aef5f6810_0
                    (and v0x7f1aef5f5e90_0 E0x7f1aef5f68d0 v0x7f1aef5f66d0_0))
                (=> v0x7f1aef5f6810_0 E0x7f1aef5f68d0)
                (=> v0x7f1aef5f6bd0_0
                    (and v0x7f1aef5f5e90_0
                         E0x7f1aef5f6c90
                         (not v0x7f1aef5f66d0_0)))
                (=> v0x7f1aef5f6bd0_0 E0x7f1aef5f6c90)
                (=> v0x7f1aef5f7410_0
                    (and v0x7f1aef5f6810_0 E0x7f1aef5f74d0 v0x7f1aef5f6a90_0))
                (=> v0x7f1aef5f7410_0 E0x7f1aef5f74d0)
                (=> v0x7f1aef5f7910_0
                    (and v0x7f1aef5f6bd0_0 E0x7f1aef5f79d0 v0x7f1aef5f72d0_0))
                (=> v0x7f1aef5f7910_0 E0x7f1aef5f79d0)
                (=> v0x7f1aef5f7b90_0 a!5)
                a!6
                v0x7f1aef5f7b90_0
                (not v0x7f1aef5f8d50_0)
                (= v0x7f1aef5f4750_0 (= v0x7f1aef5f4690_0 0.0))
                (= v0x7f1aef5f4b90_0 (< v0x7f1aef5f4010_0 2.0))
                (= v0x7f1aef5f4d50_0 (ite v0x7f1aef5f4b90_0 1.0 0.0))
                (= v0x7f1aef5f4e90_0 (+ v0x7f1aef5f4d50_0 v0x7f1aef5f4010_0))
                (= v0x7f1aef5f5810_0 (= v0x7f1aef5f5750_0 0.0))
                (= v0x7f1aef5f5c10_0 (= v0x7f1aef5f3f10_0 0.0))
                (= v0x7f1aef5f5d50_0 (ite v0x7f1aef5f5c10_0 1.0 0.0))
                (= v0x7f1aef5f66d0_0 (= v0x7f1aef5f3d90_0 0.0))
                (= v0x7f1aef5f6a90_0 (> v0x7f1aef5f5090_0 1.0))
                (= v0x7f1aef5f6e90_0 (> v0x7f1aef5f5090_0 0.0))
                (= v0x7f1aef5f6fd0_0 (+ v0x7f1aef5f5090_0 (- 1.0)))
                (= v0x7f1aef5f7190_0
                   (ite v0x7f1aef5f6e90_0 v0x7f1aef5f6fd0_0 v0x7f1aef5f5090_0))
                (= v0x7f1aef5f72d0_0 (= v0x7f1aef5f7190_0 0.0))
                (= v0x7f1aef5f7690_0 (= v0x7f1aef5f5f50_0 0.0))
                (= v0x7f1aef5f77d0_0
                   (ite v0x7f1aef5f7690_0 1.0 v0x7f1aef5f3d90_0))
                (= v0x7f1aef5f8ad0_0 (not (= v0x7f1aef5f7c50_0 0.0)))
                (= v0x7f1aef5f8c10_0 (= v0x7f1aef5f7d10_0 0.0))
                (= v0x7f1aef5f8d50_0 (or v0x7f1aef5f8c10_0 v0x7f1aef5f8ad0_0)))))
  (=> F0x7f1aef5f9b50 a!7))))
(assert (=> F0x7f1aef5f9b50 F0x7f1aef5f9a90))
(assert (=> F0x7f1aef5f9c90 (or F0x7f1aef5f9990 F0x7f1aef5f99d0)))
(assert (=> F0x7f1aef5f9c50 F0x7f1aef5f9b50))
(assert (=> pre!entry!0 (=> F0x7f1aef5f98d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f1aef5f9a90 (>= v0x7f1aef5f3d90_0 0.0))))
(assert (let ((a!1 (=> F0x7f1aef5f9a90
               (or (not (<= v0x7f1aef5f4010_0 1.0)) (<= v0x7f1aef5f3d90_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (not (<= v0x7f1aef5f2010_0 1.0)) (<= v0x7f1aef5f3fd0_0 0.0)))))
  (or (and (not post!bb1.i.i!0)
           F0x7f1aef5f9c90
           (not (>= v0x7f1aef5f3fd0_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f1aef5f9c90 a!1)
      (and (not post!bb1.i.i34.i.i!0) F0x7f1aef5f9c50 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i34.i.i!0)
