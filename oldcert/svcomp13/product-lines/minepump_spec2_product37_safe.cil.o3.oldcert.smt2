(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f398b774bd0 () Bool)
(declare-fun F0x7f398b774c10 () Bool)
(declare-fun F0x7f398b774ad0 () Bool)
(declare-fun v0x7f398b773510_0 () Bool)
(declare-fun v0x7f398b773410_0 () Bool)
(declare-fun v0x7f398b772050_0 () Real)
(declare-fun v0x7f398b771f10_0 () Bool)
(declare-fun v0x7f398b770b50_0 () Bool)
(declare-fun v0x7f398b770690_0 () Real)
(declare-fun v0x7f398b76fc90_0 () Real)
(declare-fun v0x7f398b76ee10_0 () Real)
(declare-fun v0x7f398b773650_0 () Bool)
(declare-fun v0x7f398b772210_0 () Real)
(declare-fun E0x7f398b772c10 () Bool)
(declare-fun E0x7f398b772ed0 () Bool)
(declare-fun v0x7f398b771b10_0 () Real)
(declare-fun v0x7f398b772710_0 () Bool)
(declare-fun E0x7f398b772550 () Bool)
(declare-fun v0x7f398b76ed10_0 () Real)
(declare-fun E0x7f398b771d10 () Bool)
(declare-fun E0x7f398b771810 () Bool)
(declare-fun F0x7f398b774a10 () Bool)
(declare-fun v0x7f398b7719d0_0 () Bool)
(declare-fun v0x7f398b76ea90_0 () Real)
(declare-fun E0x7f398b773850 () Bool)
(declare-fun v0x7f398b771750_0 () Bool)
(declare-fun v0x7f398b770c90_0 () Real)
(declare-fun v0x7f398b773a50_0 () Bool)
(declare-fun v0x7f398b770e90_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f398b770dd0_0 () Bool)
(declare-fun E0x7f398b770950 () Bool)
(declare-fun E0x7f398b772950 () Bool)
(declare-fun v0x7f398b7727d0_0 () Real)
(declare-fun v0x7f398b773790_0 () Bool)
(declare-fun v0x7f398b770890_0 () Bool)
(declare-fun v0x7f398b772490_0 () Bool)
(declare-fun v0x7f398b76ec10_0 () Real)
(declare-fun v0x7f398b76ffd0_0 () Real)
(declare-fun E0x7f398b770250 () Bool)
(declare-fun v0x7f398b76ff10_0 () Bool)
(declare-fun E0x7f398b770090 () Bool)
(declare-fun F0x7f398b774950 () Bool)
(declare-fun E0x7f398b771110 () Bool)
(declare-fun F0x7f398b774850 () Bool)
(declare-fun v0x7f398b76d010_0 () Real)
(declare-fun E0x7f398b76f890 () Bool)
(declare-fun v0x7f398b770750_0 () Bool)
(declare-fun v0x7f398b772350_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f398b771610_0 () Bool)
(declare-fun v0x7f398b76f5d0_0 () Real)
(declare-fun v0x7f398b76f7d0_0 () Bool)
(declare-fun v0x7f398b772890_0 () Real)
(declare-fun v0x7f398b76eed0_0 () Real)
(declare-fun v0x7f398b76edd0_0 () Real)
(declare-fun v0x7f398b76f690_0 () Bool)
(declare-fun v0x7f398b76fad0_0 () Bool)
(declare-fun E0x7f398b770f50 () Bool)
(declare-fun v0x7f398b76f510_0 () Bool)
(declare-fun v0x7f398b76ecd0_0 () Real)
(declare-fun v0x7f398b771c50_0 () Bool)
(declare-fun v0x7f398b76fdd0_0 () Real)
(declare-fun v0x7f398b76d110_0 () Bool)
(declare-fun F0x7f398b774910 () Bool)

(assert (=> F0x7f398b774910
    (and v0x7f398b76d110_0
         (<= v0x7f398b76ecd0_0 0.0)
         (>= v0x7f398b76ecd0_0 0.0)
         (<= v0x7f398b76edd0_0 1.0)
         (>= v0x7f398b76edd0_0 1.0)
         (<= v0x7f398b76eed0_0 0.0)
         (>= v0x7f398b76eed0_0 0.0)
         (<= v0x7f398b76d010_0 0.0)
         (>= v0x7f398b76d010_0 0.0))))
(assert (=> F0x7f398b774910 F0x7f398b774850))
(assert (let ((a!1 (=> v0x7f398b76ff10_0
               (or (and v0x7f398b76f7d0_0
                        E0x7f398b770090
                        (<= v0x7f398b76ffd0_0 v0x7f398b76fdd0_0)
                        (>= v0x7f398b76ffd0_0 v0x7f398b76fdd0_0))
                   (and v0x7f398b76f510_0
                        E0x7f398b770250
                        v0x7f398b76f690_0
                        (<= v0x7f398b76ffd0_0 v0x7f398b76ec10_0)
                        (>= v0x7f398b76ffd0_0 v0x7f398b76ec10_0)))))
      (a!2 (=> v0x7f398b76ff10_0
               (or (and E0x7f398b770090 (not E0x7f398b770250))
                   (and E0x7f398b770250 (not E0x7f398b770090)))))
      (a!3 (=> v0x7f398b770dd0_0
               (or (and v0x7f398b770890_0
                        E0x7f398b770f50
                        (<= v0x7f398b770e90_0 v0x7f398b770c90_0)
                        (>= v0x7f398b770e90_0 v0x7f398b770c90_0))
                   (and v0x7f398b76ff10_0
                        E0x7f398b771110
                        v0x7f398b770750_0
                        (<= v0x7f398b770e90_0 v0x7f398b76ea90_0)
                        (>= v0x7f398b770e90_0 v0x7f398b76ea90_0)))))
      (a!4 (=> v0x7f398b770dd0_0
               (or (and E0x7f398b770f50 (not E0x7f398b771110))
                   (and E0x7f398b771110 (not E0x7f398b770f50)))))
      (a!5 (or (and v0x7f398b771750_0
                    E0x7f398b772950
                    (<= v0x7f398b7727d0_0 v0x7f398b76ffd0_0)
                    (>= v0x7f398b7727d0_0 v0x7f398b76ffd0_0)
                    (<= v0x7f398b772890_0 v0x7f398b771b10_0)
                    (>= v0x7f398b772890_0 v0x7f398b771b10_0))
               (and v0x7f398b772490_0
                    E0x7f398b772c10
                    (and (<= v0x7f398b7727d0_0 v0x7f398b772210_0)
                         (>= v0x7f398b7727d0_0 v0x7f398b772210_0))
                    (<= v0x7f398b772890_0 v0x7f398b76ee10_0)
                    (>= v0x7f398b772890_0 v0x7f398b76ee10_0))
               (and v0x7f398b771c50_0
                    E0x7f398b772ed0
                    (not v0x7f398b772350_0)
                    (and (<= v0x7f398b7727d0_0 v0x7f398b772210_0)
                         (>= v0x7f398b7727d0_0 v0x7f398b772210_0))
                    (<= v0x7f398b772890_0 0.0)
                    (>= v0x7f398b772890_0 0.0))))
      (a!6 (=> v0x7f398b772710_0
               (or (and E0x7f398b772950
                        (not E0x7f398b772c10)
                        (not E0x7f398b772ed0))
                   (and E0x7f398b772c10
                        (not E0x7f398b772950)
                        (not E0x7f398b772ed0))
                   (and E0x7f398b772ed0
                        (not E0x7f398b772950)
                        (not E0x7f398b772c10)))))
      (a!7 (or (and v0x7f398b773790_0
                    v0x7f398b773a50_0
                    (and (<= v0x7f398b76ecd0_0 v0x7f398b770e90_0)
                         (>= v0x7f398b76ecd0_0 v0x7f398b770e90_0))
                    (and (<= v0x7f398b76edd0_0 v0x7f398b7727d0_0)
                         (>= v0x7f398b76edd0_0 v0x7f398b7727d0_0))
                    (<= v0x7f398b76eed0_0 1.0)
                    (>= v0x7f398b76eed0_0 1.0)
                    (and (<= v0x7f398b76d010_0 v0x7f398b772890_0)
                         (>= v0x7f398b76d010_0 v0x7f398b772890_0)))
               (and v0x7f398b772710_0
                    v0x7f398b773650_0
                    (and (<= v0x7f398b76ecd0_0 v0x7f398b770e90_0)
                         (>= v0x7f398b76ecd0_0 v0x7f398b770e90_0))
                    (and (<= v0x7f398b76edd0_0 v0x7f398b7727d0_0)
                         (>= v0x7f398b76edd0_0 v0x7f398b7727d0_0))
                    (<= v0x7f398b76eed0_0 0.0)
                    (>= v0x7f398b76eed0_0 0.0)
                    (and (<= v0x7f398b76d010_0 v0x7f398b772890_0)
                         (>= v0x7f398b76d010_0 v0x7f398b772890_0))))))
(let ((a!8 (and (=> v0x7f398b76f7d0_0
                    (and v0x7f398b76f510_0
                         E0x7f398b76f890
                         (not v0x7f398b76f690_0)))
                (=> v0x7f398b76f7d0_0 E0x7f398b76f890)
                a!1
                a!2
                (=> v0x7f398b770890_0
                    (and v0x7f398b76ff10_0
                         E0x7f398b770950
                         (not v0x7f398b770750_0)))
                (=> v0x7f398b770890_0 E0x7f398b770950)
                a!3
                a!4
                (=> v0x7f398b771750_0
                    (and v0x7f398b770dd0_0 E0x7f398b771810 v0x7f398b771610_0))
                (=> v0x7f398b771750_0 E0x7f398b771810)
                (=> v0x7f398b771c50_0
                    (and v0x7f398b770dd0_0
                         E0x7f398b771d10
                         (not v0x7f398b771610_0)))
                (=> v0x7f398b771c50_0 E0x7f398b771d10)
                (=> v0x7f398b772490_0
                    (and v0x7f398b771c50_0 E0x7f398b772550 v0x7f398b772350_0))
                (=> v0x7f398b772490_0 E0x7f398b772550)
                (=> v0x7f398b772710_0 a!5)
                a!6
                (=> v0x7f398b773790_0
                    (and v0x7f398b772710_0
                         E0x7f398b773850
                         (not v0x7f398b773650_0)))
                (=> v0x7f398b773790_0 E0x7f398b773850)
                a!7
                (= v0x7f398b76f690_0 (= v0x7f398b76f5d0_0 0.0))
                (= v0x7f398b76fad0_0 (< v0x7f398b76ec10_0 2.0))
                (= v0x7f398b76fc90_0 (ite v0x7f398b76fad0_0 1.0 0.0))
                (= v0x7f398b76fdd0_0 (+ v0x7f398b76fc90_0 v0x7f398b76ec10_0))
                (= v0x7f398b770750_0 (= v0x7f398b770690_0 0.0))
                (= v0x7f398b770b50_0 (= v0x7f398b76ea90_0 0.0))
                (= v0x7f398b770c90_0 (ite v0x7f398b770b50_0 1.0 0.0))
                (= v0x7f398b771610_0 (= v0x7f398b76ee10_0 0.0))
                (= v0x7f398b7719d0_0 (> v0x7f398b76ffd0_0 1.0))
                (= v0x7f398b771b10_0
                   (ite v0x7f398b7719d0_0 1.0 v0x7f398b76ee10_0))
                (= v0x7f398b771f10_0 (> v0x7f398b76ffd0_0 0.0))
                (= v0x7f398b772050_0 (+ v0x7f398b76ffd0_0 (- 1.0)))
                (= v0x7f398b772210_0
                   (ite v0x7f398b771f10_0 v0x7f398b772050_0 v0x7f398b76ffd0_0))
                (= v0x7f398b772350_0 (= v0x7f398b770e90_0 0.0))
                (= v0x7f398b773410_0 (= v0x7f398b770e90_0 0.0))
                (= v0x7f398b773510_0 (= v0x7f398b772890_0 0.0))
                (= v0x7f398b773650_0 (or v0x7f398b773510_0 v0x7f398b773410_0))
                (= v0x7f398b773a50_0 (= v0x7f398b76ed10_0 0.0)))))
  (=> F0x7f398b774950 a!8))))
(assert (=> F0x7f398b774950 F0x7f398b774a10))
(assert (let ((a!1 (=> v0x7f398b76ff10_0
               (or (and v0x7f398b76f7d0_0
                        E0x7f398b770090
                        (<= v0x7f398b76ffd0_0 v0x7f398b76fdd0_0)
                        (>= v0x7f398b76ffd0_0 v0x7f398b76fdd0_0))
                   (and v0x7f398b76f510_0
                        E0x7f398b770250
                        v0x7f398b76f690_0
                        (<= v0x7f398b76ffd0_0 v0x7f398b76ec10_0)
                        (>= v0x7f398b76ffd0_0 v0x7f398b76ec10_0)))))
      (a!2 (=> v0x7f398b76ff10_0
               (or (and E0x7f398b770090 (not E0x7f398b770250))
                   (and E0x7f398b770250 (not E0x7f398b770090)))))
      (a!3 (=> v0x7f398b770dd0_0
               (or (and v0x7f398b770890_0
                        E0x7f398b770f50
                        (<= v0x7f398b770e90_0 v0x7f398b770c90_0)
                        (>= v0x7f398b770e90_0 v0x7f398b770c90_0))
                   (and v0x7f398b76ff10_0
                        E0x7f398b771110
                        v0x7f398b770750_0
                        (<= v0x7f398b770e90_0 v0x7f398b76ea90_0)
                        (>= v0x7f398b770e90_0 v0x7f398b76ea90_0)))))
      (a!4 (=> v0x7f398b770dd0_0
               (or (and E0x7f398b770f50 (not E0x7f398b771110))
                   (and E0x7f398b771110 (not E0x7f398b770f50)))))
      (a!5 (or (and v0x7f398b771750_0
                    E0x7f398b772950
                    (<= v0x7f398b7727d0_0 v0x7f398b76ffd0_0)
                    (>= v0x7f398b7727d0_0 v0x7f398b76ffd0_0)
                    (<= v0x7f398b772890_0 v0x7f398b771b10_0)
                    (>= v0x7f398b772890_0 v0x7f398b771b10_0))
               (and v0x7f398b772490_0
                    E0x7f398b772c10
                    (and (<= v0x7f398b7727d0_0 v0x7f398b772210_0)
                         (>= v0x7f398b7727d0_0 v0x7f398b772210_0))
                    (<= v0x7f398b772890_0 v0x7f398b76ee10_0)
                    (>= v0x7f398b772890_0 v0x7f398b76ee10_0))
               (and v0x7f398b771c50_0
                    E0x7f398b772ed0
                    (not v0x7f398b772350_0)
                    (and (<= v0x7f398b7727d0_0 v0x7f398b772210_0)
                         (>= v0x7f398b7727d0_0 v0x7f398b772210_0))
                    (<= v0x7f398b772890_0 0.0)
                    (>= v0x7f398b772890_0 0.0))))
      (a!6 (=> v0x7f398b772710_0
               (or (and E0x7f398b772950
                        (not E0x7f398b772c10)
                        (not E0x7f398b772ed0))
                   (and E0x7f398b772c10
                        (not E0x7f398b772950)
                        (not E0x7f398b772ed0))
                   (and E0x7f398b772ed0
                        (not E0x7f398b772950)
                        (not E0x7f398b772c10))))))
(let ((a!7 (and (=> v0x7f398b76f7d0_0
                    (and v0x7f398b76f510_0
                         E0x7f398b76f890
                         (not v0x7f398b76f690_0)))
                (=> v0x7f398b76f7d0_0 E0x7f398b76f890)
                a!1
                a!2
                (=> v0x7f398b770890_0
                    (and v0x7f398b76ff10_0
                         E0x7f398b770950
                         (not v0x7f398b770750_0)))
                (=> v0x7f398b770890_0 E0x7f398b770950)
                a!3
                a!4
                (=> v0x7f398b771750_0
                    (and v0x7f398b770dd0_0 E0x7f398b771810 v0x7f398b771610_0))
                (=> v0x7f398b771750_0 E0x7f398b771810)
                (=> v0x7f398b771c50_0
                    (and v0x7f398b770dd0_0
                         E0x7f398b771d10
                         (not v0x7f398b771610_0)))
                (=> v0x7f398b771c50_0 E0x7f398b771d10)
                (=> v0x7f398b772490_0
                    (and v0x7f398b771c50_0 E0x7f398b772550 v0x7f398b772350_0))
                (=> v0x7f398b772490_0 E0x7f398b772550)
                (=> v0x7f398b772710_0 a!5)
                a!6
                (=> v0x7f398b773790_0
                    (and v0x7f398b772710_0
                         E0x7f398b773850
                         (not v0x7f398b773650_0)))
                (=> v0x7f398b773790_0 E0x7f398b773850)
                v0x7f398b773790_0
                (not v0x7f398b773a50_0)
                (= v0x7f398b76f690_0 (= v0x7f398b76f5d0_0 0.0))
                (= v0x7f398b76fad0_0 (< v0x7f398b76ec10_0 2.0))
                (= v0x7f398b76fc90_0 (ite v0x7f398b76fad0_0 1.0 0.0))
                (= v0x7f398b76fdd0_0 (+ v0x7f398b76fc90_0 v0x7f398b76ec10_0))
                (= v0x7f398b770750_0 (= v0x7f398b770690_0 0.0))
                (= v0x7f398b770b50_0 (= v0x7f398b76ea90_0 0.0))
                (= v0x7f398b770c90_0 (ite v0x7f398b770b50_0 1.0 0.0))
                (= v0x7f398b771610_0 (= v0x7f398b76ee10_0 0.0))
                (= v0x7f398b7719d0_0 (> v0x7f398b76ffd0_0 1.0))
                (= v0x7f398b771b10_0
                   (ite v0x7f398b7719d0_0 1.0 v0x7f398b76ee10_0))
                (= v0x7f398b771f10_0 (> v0x7f398b76ffd0_0 0.0))
                (= v0x7f398b772050_0 (+ v0x7f398b76ffd0_0 (- 1.0)))
                (= v0x7f398b772210_0
                   (ite v0x7f398b771f10_0 v0x7f398b772050_0 v0x7f398b76ffd0_0))
                (= v0x7f398b772350_0 (= v0x7f398b770e90_0 0.0))
                (= v0x7f398b773410_0 (= v0x7f398b770e90_0 0.0))
                (= v0x7f398b773510_0 (= v0x7f398b772890_0 0.0))
                (= v0x7f398b773650_0 (or v0x7f398b773510_0 v0x7f398b773410_0))
                (= v0x7f398b773a50_0 (= v0x7f398b76ed10_0 0.0)))))
  (=> F0x7f398b774ad0 a!7))))
(assert (=> F0x7f398b774ad0 F0x7f398b774a10))
(assert (=> F0x7f398b774c10 (or F0x7f398b774910 F0x7f398b774950)))
(assert (=> F0x7f398b774bd0 F0x7f398b774ad0))
(assert (=> pre!entry!0 (=> F0x7f398b774850 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f398b774a10 (>= v0x7f398b76ed10_0 0.0))))
(assert (let ((a!1 (=> F0x7f398b774a10
               (or (<= v0x7f398b76ed10_0 0.0) (not (<= v0x7f398b76ee10_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7f398b76eed0_0 0.0) (not (<= v0x7f398b76d010_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f398b774c10
           (not (>= v0x7f398b76eed0_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f398b774c10 a!1)
      (and (not post!bb2.i.i23.i.i!0) F0x7f398b774bd0 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i23.i.i!0)
