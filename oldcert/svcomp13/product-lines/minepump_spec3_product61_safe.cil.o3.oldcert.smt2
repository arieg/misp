(declare-fun post!bb2.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f80ffbf87d0 () Bool)
(declare-fun F0x7f80ffbf8610 () Bool)
(declare-fun v0x7f80ffbf7890_0 () Bool)
(declare-fun v0x7f80ffbf7510_0 () Bool)
(declare-fun v0x7f80ffbf7610_0 () Bool)
(declare-fun v0x7f80ffbf5690_0 () Bool)
(declare-fun v0x7f80ffbf4410_0 () Bool)
(declare-fun v0x7f80ffbf3390_0 () Bool)
(declare-fun v0x7f80ffbf2e90_0 () Real)
(declare-fun E0x7f80ffbf6d50 () Bool)
(declare-fun E0x7f80ffbf6ad0 () Bool)
(declare-fun F0x7f80ffbf8910 () Bool)
(declare-fun v0x7f80ffbf6210_0 () Real)
(declare-fun v0x7f80ffbf5c10_0 () Bool)
(declare-fun E0x7f80ffbf6810 () Bool)
(declare-fun v0x7f80ffbf6690_0 () Real)
(declare-fun E0x7f80ffbf6410 () Bool)
(declare-fun v0x7f80ffbf5290_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f80ffbf5e90_0 () Bool)
(declare-fun E0x7f80ffbf5490 () Bool)
(declare-fun v0x7f80ffbf4ed0_0 () Bool)
(declare-fun E0x7f80ffbf4210 () Bool)
(declare-fun v0x7f80ffbf4550_0 () Real)
(declare-fun v0x7f80ffbf5d50_0 () Bool)
(declare-fun v0x7f80ffbf4150_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f80ffbf4010_0 () Bool)
(declare-fun v0x7f80ffbf6750_0 () Real)
(declare-fun v0x7f80ffbf6110_0 () Bool)
(declare-fun v0x7f80ffbf37d0_0 () Bool)
(declare-fun E0x7f80ffbf6f50 () Bool)
(declare-fun v0x7f80ffbf2710_0 () Real)
(declare-fun v0x7f80ffbf5010_0 () Bool)
(declare-fun E0x7f80ffbf3150 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f80ffbf3950 () Bool)
(declare-fun E0x7f80ffbf4810 () Bool)
(declare-fun v0x7f80ffbf3090_0 () Bool)
(declare-fun E0x7f80ffbf5f50 () Bool)
(declare-fun v0x7f80ffbf79d0_0 () Bool)
(declare-fun E0x7f80ffbf49d0 () Bool)
(declare-fun v0x7f80ffbf53d0_0 () Bool)
(declare-fun F0x7f80ffbf8650 () Bool)
(declare-fun v0x7f80ffbf65d0_0 () Bool)
(declare-fun v0x7f80ffbf3550_0 () Real)
(declare-fun v0x7f80ffbf6350_0 () Bool)
(declare-fun v0x7f80ffbf5ad0_0 () Bool)
(declare-fun v0x7f80ffbf2dd0_0 () Bool)
(declare-fun v0x7f80ffbf2590_0 () Real)
(declare-fun v0x7f80ffbf3f50_0 () Real)
(declare-fun v0x7f80ffbef010_0 () Real)
(declare-fun v0x7f80ffbf5990_0 () Real)
(declare-fun E0x7f80ffbf3b10 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun F0x7f80ffbf88d0 () Bool)
(declare-fun v0x7f80ffbf28d0_0 () Real)
(declare-fun E0x7f80ffbf50d0 () Bool)
(declare-fun v0x7f80ffbf3690_0 () Real)
(declare-fun v0x7f80ffbf7750_0 () Bool)
(declare-fun v0x7f80ffbf27d0_0 () Real)
(declare-fun v0x7f80ffbf4690_0 () Bool)
(declare-fun v0x7f80ffbef110_0 () Bool)
(declare-fun v0x7f80ffbf4750_0 () Real)
(declare-fun v0x7f80ffbf3890_0 () Real)
(declare-fun v0x7f80ffbf2810_0 () Real)
(declare-fun F0x7f80ffbf8550 () Bool)
(declare-fun v0x7f80ffbf57d0_0 () Real)
(declare-fun F0x7f80ffbf8710 () Bool)
(declare-fun v0x7f80ffbf2f50_0 () Bool)

(assert (=> F0x7f80ffbf8710
    (and v0x7f80ffbef110_0
         (<= v0x7f80ffbf27d0_0 0.0)
         (>= v0x7f80ffbf27d0_0 0.0)
         (<= v0x7f80ffbf28d0_0 1.0)
         (>= v0x7f80ffbf28d0_0 1.0)
         (<= v0x7f80ffbef010_0 0.0)
         (>= v0x7f80ffbef010_0 0.0))))
(assert (=> F0x7f80ffbf8710 F0x7f80ffbf8650))
(assert (let ((a!1 (=> v0x7f80ffbf37d0_0
               (or (and v0x7f80ffbf3090_0
                        E0x7f80ffbf3950
                        (<= v0x7f80ffbf3890_0 v0x7f80ffbf3690_0)
                        (>= v0x7f80ffbf3890_0 v0x7f80ffbf3690_0))
                   (and v0x7f80ffbf2dd0_0
                        E0x7f80ffbf3b10
                        v0x7f80ffbf2f50_0
                        (<= v0x7f80ffbf3890_0 v0x7f80ffbf2710_0)
                        (>= v0x7f80ffbf3890_0 v0x7f80ffbf2710_0)))))
      (a!2 (=> v0x7f80ffbf37d0_0
               (or (and E0x7f80ffbf3950 (not E0x7f80ffbf3b10))
                   (and E0x7f80ffbf3b10 (not E0x7f80ffbf3950)))))
      (a!3 (=> v0x7f80ffbf4690_0
               (or (and v0x7f80ffbf4150_0
                        E0x7f80ffbf4810
                        (<= v0x7f80ffbf4750_0 v0x7f80ffbf4550_0)
                        (>= v0x7f80ffbf4750_0 v0x7f80ffbf4550_0))
                   (and v0x7f80ffbf37d0_0
                        E0x7f80ffbf49d0
                        v0x7f80ffbf4010_0
                        (<= v0x7f80ffbf4750_0 v0x7f80ffbf2590_0)
                        (>= v0x7f80ffbf4750_0 v0x7f80ffbf2590_0)))))
      (a!4 (=> v0x7f80ffbf4690_0
               (or (and E0x7f80ffbf4810 (not E0x7f80ffbf49d0))
                   (and E0x7f80ffbf49d0 (not E0x7f80ffbf4810)))))
      (a!5 (or (and v0x7f80ffbf5e90_0
                    E0x7f80ffbf6810
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0))
                    (<= v0x7f80ffbf6750_0 v0x7f80ffbf6210_0)
                    (>= v0x7f80ffbf6750_0 v0x7f80ffbf6210_0))
               (and v0x7f80ffbf5010_0
                    E0x7f80ffbf6ad0
                    (not v0x7f80ffbf5290_0)
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0))
                    (and (<= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)
                         (>= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)))
               (and v0x7f80ffbf6350_0
                    E0x7f80ffbf6d50
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0))
                    (and (<= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)
                         (>= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)))
               (and v0x7f80ffbf53d0_0
                    E0x7f80ffbf6f50
                    (not v0x7f80ffbf5d50_0)
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0))
                    (<= v0x7f80ffbf6750_0 0.0)
                    (>= v0x7f80ffbf6750_0 0.0))))
      (a!6 (=> v0x7f80ffbf65d0_0
               (or (and E0x7f80ffbf6810
                        (not E0x7f80ffbf6ad0)
                        (not E0x7f80ffbf6d50)
                        (not E0x7f80ffbf6f50))
                   (and E0x7f80ffbf6ad0
                        (not E0x7f80ffbf6810)
                        (not E0x7f80ffbf6d50)
                        (not E0x7f80ffbf6f50))
                   (and E0x7f80ffbf6d50
                        (not E0x7f80ffbf6810)
                        (not E0x7f80ffbf6ad0)
                        (not E0x7f80ffbf6f50))
                   (and E0x7f80ffbf6f50
                        (not E0x7f80ffbf6810)
                        (not E0x7f80ffbf6ad0)
                        (not E0x7f80ffbf6d50))))))
(let ((a!7 (and (=> v0x7f80ffbf3090_0
                    (and v0x7f80ffbf2dd0_0
                         E0x7f80ffbf3150
                         (not v0x7f80ffbf2f50_0)))
                (=> v0x7f80ffbf3090_0 E0x7f80ffbf3150)
                a!1
                a!2
                (=> v0x7f80ffbf4150_0
                    (and v0x7f80ffbf37d0_0
                         E0x7f80ffbf4210
                         (not v0x7f80ffbf4010_0)))
                (=> v0x7f80ffbf4150_0 E0x7f80ffbf4210)
                a!3
                a!4
                (=> v0x7f80ffbf5010_0
                    (and v0x7f80ffbf4690_0 E0x7f80ffbf50d0 v0x7f80ffbf4ed0_0))
                (=> v0x7f80ffbf5010_0 E0x7f80ffbf50d0)
                (=> v0x7f80ffbf53d0_0
                    (and v0x7f80ffbf4690_0
                         E0x7f80ffbf5490
                         (not v0x7f80ffbf4ed0_0)))
                (=> v0x7f80ffbf53d0_0 E0x7f80ffbf5490)
                (=> v0x7f80ffbf5e90_0
                    (and v0x7f80ffbf5010_0 E0x7f80ffbf5f50 v0x7f80ffbf5290_0))
                (=> v0x7f80ffbf5e90_0 E0x7f80ffbf5f50)
                (=> v0x7f80ffbf6350_0
                    (and v0x7f80ffbf53d0_0 E0x7f80ffbf6410 v0x7f80ffbf5d50_0))
                (=> v0x7f80ffbf6350_0 E0x7f80ffbf6410)
                (=> v0x7f80ffbf65d0_0 a!5)
                a!6
                v0x7f80ffbf65d0_0
                (not v0x7f80ffbf79d0_0)
                (<= v0x7f80ffbf27d0_0 v0x7f80ffbf4750_0)
                (>= v0x7f80ffbf27d0_0 v0x7f80ffbf4750_0)
                (<= v0x7f80ffbf28d0_0 v0x7f80ffbf6690_0)
                (>= v0x7f80ffbf28d0_0 v0x7f80ffbf6690_0)
                (<= v0x7f80ffbef010_0 v0x7f80ffbf6750_0)
                (>= v0x7f80ffbef010_0 v0x7f80ffbf6750_0)
                (= v0x7f80ffbf2f50_0 (= v0x7f80ffbf2e90_0 0.0))
                (= v0x7f80ffbf3390_0 (< v0x7f80ffbf2710_0 2.0))
                (= v0x7f80ffbf3550_0 (ite v0x7f80ffbf3390_0 1.0 0.0))
                (= v0x7f80ffbf3690_0 (+ v0x7f80ffbf3550_0 v0x7f80ffbf2710_0))
                (= v0x7f80ffbf4010_0 (= v0x7f80ffbf3f50_0 0.0))
                (= v0x7f80ffbf4410_0 (= v0x7f80ffbf2590_0 0.0))
                (= v0x7f80ffbf4550_0 (ite v0x7f80ffbf4410_0 1.0 0.0))
                (= v0x7f80ffbf4ed0_0 (= v0x7f80ffbf2810_0 0.0))
                (= v0x7f80ffbf5290_0 (> v0x7f80ffbf3890_0 1.0))
                (= v0x7f80ffbf5690_0 (> v0x7f80ffbf3890_0 0.0))
                (= v0x7f80ffbf57d0_0 (+ v0x7f80ffbf3890_0 (- 1.0)))
                (= v0x7f80ffbf5990_0
                   (ite v0x7f80ffbf5690_0 v0x7f80ffbf57d0_0 v0x7f80ffbf3890_0))
                (= v0x7f80ffbf5ad0_0 (= v0x7f80ffbf4750_0 0.0))
                (= v0x7f80ffbf5c10_0 (= v0x7f80ffbf5990_0 0.0))
                (= v0x7f80ffbf5d50_0 (and v0x7f80ffbf5ad0_0 v0x7f80ffbf5c10_0))
                (= v0x7f80ffbf6110_0 (= v0x7f80ffbf4750_0 0.0))
                (= v0x7f80ffbf6210_0
                   (ite v0x7f80ffbf6110_0 1.0 v0x7f80ffbf2810_0))
                (= v0x7f80ffbf7510_0 (= v0x7f80ffbf4750_0 0.0))
                (= v0x7f80ffbf7610_0 (= v0x7f80ffbf6690_0 2.0))
                (= v0x7f80ffbf7750_0 (= v0x7f80ffbf6750_0 0.0))
                (= v0x7f80ffbf7890_0 (and v0x7f80ffbf7610_0 v0x7f80ffbf7510_0))
                (= v0x7f80ffbf79d0_0 (and v0x7f80ffbf7890_0 v0x7f80ffbf7750_0)))))
  (=> F0x7f80ffbf8550 a!7))))
(assert (=> F0x7f80ffbf8550 F0x7f80ffbf8610))
(assert (let ((a!1 (=> v0x7f80ffbf37d0_0
               (or (and v0x7f80ffbf3090_0
                        E0x7f80ffbf3950
                        (<= v0x7f80ffbf3890_0 v0x7f80ffbf3690_0)
                        (>= v0x7f80ffbf3890_0 v0x7f80ffbf3690_0))
                   (and v0x7f80ffbf2dd0_0
                        E0x7f80ffbf3b10
                        v0x7f80ffbf2f50_0
                        (<= v0x7f80ffbf3890_0 v0x7f80ffbf2710_0)
                        (>= v0x7f80ffbf3890_0 v0x7f80ffbf2710_0)))))
      (a!2 (=> v0x7f80ffbf37d0_0
               (or (and E0x7f80ffbf3950 (not E0x7f80ffbf3b10))
                   (and E0x7f80ffbf3b10 (not E0x7f80ffbf3950)))))
      (a!3 (=> v0x7f80ffbf4690_0
               (or (and v0x7f80ffbf4150_0
                        E0x7f80ffbf4810
                        (<= v0x7f80ffbf4750_0 v0x7f80ffbf4550_0)
                        (>= v0x7f80ffbf4750_0 v0x7f80ffbf4550_0))
                   (and v0x7f80ffbf37d0_0
                        E0x7f80ffbf49d0
                        v0x7f80ffbf4010_0
                        (<= v0x7f80ffbf4750_0 v0x7f80ffbf2590_0)
                        (>= v0x7f80ffbf4750_0 v0x7f80ffbf2590_0)))))
      (a!4 (=> v0x7f80ffbf4690_0
               (or (and E0x7f80ffbf4810 (not E0x7f80ffbf49d0))
                   (and E0x7f80ffbf49d0 (not E0x7f80ffbf4810)))))
      (a!5 (or (and v0x7f80ffbf5e90_0
                    E0x7f80ffbf6810
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0))
                    (<= v0x7f80ffbf6750_0 v0x7f80ffbf6210_0)
                    (>= v0x7f80ffbf6750_0 v0x7f80ffbf6210_0))
               (and v0x7f80ffbf5010_0
                    E0x7f80ffbf6ad0
                    (not v0x7f80ffbf5290_0)
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf3890_0))
                    (and (<= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)
                         (>= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)))
               (and v0x7f80ffbf6350_0
                    E0x7f80ffbf6d50
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0))
                    (and (<= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)
                         (>= v0x7f80ffbf6750_0 v0x7f80ffbf2810_0)))
               (and v0x7f80ffbf53d0_0
                    E0x7f80ffbf6f50
                    (not v0x7f80ffbf5d50_0)
                    (and (<= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0)
                         (>= v0x7f80ffbf6690_0 v0x7f80ffbf5990_0))
                    (<= v0x7f80ffbf6750_0 0.0)
                    (>= v0x7f80ffbf6750_0 0.0))))
      (a!6 (=> v0x7f80ffbf65d0_0
               (or (and E0x7f80ffbf6810
                        (not E0x7f80ffbf6ad0)
                        (not E0x7f80ffbf6d50)
                        (not E0x7f80ffbf6f50))
                   (and E0x7f80ffbf6ad0
                        (not E0x7f80ffbf6810)
                        (not E0x7f80ffbf6d50)
                        (not E0x7f80ffbf6f50))
                   (and E0x7f80ffbf6d50
                        (not E0x7f80ffbf6810)
                        (not E0x7f80ffbf6ad0)
                        (not E0x7f80ffbf6f50))
                   (and E0x7f80ffbf6f50
                        (not E0x7f80ffbf6810)
                        (not E0x7f80ffbf6ad0)
                        (not E0x7f80ffbf6d50))))))
(let ((a!7 (and (=> v0x7f80ffbf3090_0
                    (and v0x7f80ffbf2dd0_0
                         E0x7f80ffbf3150
                         (not v0x7f80ffbf2f50_0)))
                (=> v0x7f80ffbf3090_0 E0x7f80ffbf3150)
                a!1
                a!2
                (=> v0x7f80ffbf4150_0
                    (and v0x7f80ffbf37d0_0
                         E0x7f80ffbf4210
                         (not v0x7f80ffbf4010_0)))
                (=> v0x7f80ffbf4150_0 E0x7f80ffbf4210)
                a!3
                a!4
                (=> v0x7f80ffbf5010_0
                    (and v0x7f80ffbf4690_0 E0x7f80ffbf50d0 v0x7f80ffbf4ed0_0))
                (=> v0x7f80ffbf5010_0 E0x7f80ffbf50d0)
                (=> v0x7f80ffbf53d0_0
                    (and v0x7f80ffbf4690_0
                         E0x7f80ffbf5490
                         (not v0x7f80ffbf4ed0_0)))
                (=> v0x7f80ffbf53d0_0 E0x7f80ffbf5490)
                (=> v0x7f80ffbf5e90_0
                    (and v0x7f80ffbf5010_0 E0x7f80ffbf5f50 v0x7f80ffbf5290_0))
                (=> v0x7f80ffbf5e90_0 E0x7f80ffbf5f50)
                (=> v0x7f80ffbf6350_0
                    (and v0x7f80ffbf53d0_0 E0x7f80ffbf6410 v0x7f80ffbf5d50_0))
                (=> v0x7f80ffbf6350_0 E0x7f80ffbf6410)
                (=> v0x7f80ffbf65d0_0 a!5)
                a!6
                v0x7f80ffbf65d0_0
                v0x7f80ffbf79d0_0
                (= v0x7f80ffbf2f50_0 (= v0x7f80ffbf2e90_0 0.0))
                (= v0x7f80ffbf3390_0 (< v0x7f80ffbf2710_0 2.0))
                (= v0x7f80ffbf3550_0 (ite v0x7f80ffbf3390_0 1.0 0.0))
                (= v0x7f80ffbf3690_0 (+ v0x7f80ffbf3550_0 v0x7f80ffbf2710_0))
                (= v0x7f80ffbf4010_0 (= v0x7f80ffbf3f50_0 0.0))
                (= v0x7f80ffbf4410_0 (= v0x7f80ffbf2590_0 0.0))
                (= v0x7f80ffbf4550_0 (ite v0x7f80ffbf4410_0 1.0 0.0))
                (= v0x7f80ffbf4ed0_0 (= v0x7f80ffbf2810_0 0.0))
                (= v0x7f80ffbf5290_0 (> v0x7f80ffbf3890_0 1.0))
                (= v0x7f80ffbf5690_0 (> v0x7f80ffbf3890_0 0.0))
                (= v0x7f80ffbf57d0_0 (+ v0x7f80ffbf3890_0 (- 1.0)))
                (= v0x7f80ffbf5990_0
                   (ite v0x7f80ffbf5690_0 v0x7f80ffbf57d0_0 v0x7f80ffbf3890_0))
                (= v0x7f80ffbf5ad0_0 (= v0x7f80ffbf4750_0 0.0))
                (= v0x7f80ffbf5c10_0 (= v0x7f80ffbf5990_0 0.0))
                (= v0x7f80ffbf5d50_0 (and v0x7f80ffbf5ad0_0 v0x7f80ffbf5c10_0))
                (= v0x7f80ffbf6110_0 (= v0x7f80ffbf4750_0 0.0))
                (= v0x7f80ffbf6210_0
                   (ite v0x7f80ffbf6110_0 1.0 v0x7f80ffbf2810_0))
                (= v0x7f80ffbf7510_0 (= v0x7f80ffbf4750_0 0.0))
                (= v0x7f80ffbf7610_0 (= v0x7f80ffbf6690_0 2.0))
                (= v0x7f80ffbf7750_0 (= v0x7f80ffbf6750_0 0.0))
                (= v0x7f80ffbf7890_0 (and v0x7f80ffbf7610_0 v0x7f80ffbf7510_0))
                (= v0x7f80ffbf79d0_0 (and v0x7f80ffbf7890_0 v0x7f80ffbf7750_0)))))
  (=> F0x7f80ffbf87d0 a!7))))
(assert (=> F0x7f80ffbf87d0 F0x7f80ffbf8610))
(assert (=> F0x7f80ffbf8910 (or F0x7f80ffbf8710 F0x7f80ffbf8550)))
(assert (=> F0x7f80ffbf88d0 F0x7f80ffbf87d0))
(assert (=> pre!entry!0 (=> F0x7f80ffbf8650 true)))
(assert (=> pre!bb1.i.i!0
    (=> F0x7f80ffbf8610
        (or (<= v0x7f80ffbf2590_0 0.0) (<= v0x7f80ffbf2810_0 0.0)))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f80ffbf8610 (>= v0x7f80ffbf2810_0 0.0))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f80ffbf8610 (not (<= 3.0 v0x7f80ffbf2710_0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f80ffbf8610 (>= v0x7f80ffbf2590_0 0.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!0)
                F0x7f80ffbf8910
                (not (or (<= v0x7f80ffbf27d0_0 0.0) (<= v0x7f80ffbef010_0 0.0))))))
  (or a!1
      (and (not post!bb1.i.i!1)
           F0x7f80ffbf8910
           (not (>= v0x7f80ffbef010_0 0.0)))
      (and (not post!bb1.i.i!2) F0x7f80ffbf8910 (<= 3.0 v0x7f80ffbf28d0_0))
      (and (not post!bb1.i.i!3)
           F0x7f80ffbf8910
           (not (>= v0x7f80ffbf27d0_0 0.0)))
      (and (not post!bb2.i.i43.i.i!0) F0x7f80ffbf88d0 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i43.i.i!0)
