(declare-fun post!bb2.i.i21.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f798a15be10 () Bool)
(declare-fun v0x7f798a15ac50_0 () Bool)
(declare-fun v0x7f798a15ab50_0 () Bool)
(declare-fun v0x7f798a15a110_0 () Bool)
(declare-fun v0x7f798a159190_0 () Real)
(declare-fun v0x7f798a159050_0 () Bool)
(declare-fun v0x7f798a1572d0_0 () Real)
(declare-fun v0x7f798a15aed0_0 () Bool)
(declare-fun v0x7f798a157110_0 () Bool)
(declare-fun F0x7f798a15bf50 () Bool)
(declare-fun v0x7f798a156c10_0 () Real)
(declare-fun v0x7f798a15a250_0 () Real)
(declare-fun E0x7f798a15a6d0 () Bool)
(declare-fun v0x7f798a159d50_0 () Bool)
(declare-fun v0x7f798a157cd0_0 () Real)
(declare-fun E0x7f798a159f50 () Bool)
(declare-fun E0x7f798a1597d0 () Bool)
(declare-fun E0x7f798a159610 () Bool)
(declare-fun v0x7f798a156310_0 () Real)
(declare-fun v0x7f798a1582d0_0 () Real)
(declare-fun v0x7f798a159c10_0 () Bool)
(declare-fun E0x7f798a158590 () Bool)
(declare-fun F0x7f798a15bd50 () Bool)
(declare-fun v0x7f798a158410_0 () Bool)
(declare-fun v0x7f798a157d90_0 () Bool)
(declare-fun v0x7f798a156490_0 () Real)
(declare-fun E0x7f798a158750 () Bool)
(declare-fun E0x7f798a157890 () Bool)
(declare-fun v0x7f798a15a390_0 () Bool)
(declare-fun v0x7f798a157610_0 () Real)
(declare-fun v0x7f798a159490_0 () Bool)
(declare-fun v0x7f798a15a450_0 () Real)
(declare-fun v0x7f798a157550_0 () Bool)
(declare-fun v0x7f798a156cd0_0 () Bool)
(declare-fun E0x7f798a156ed0 () Bool)
(declare-fun v0x7f798a159e90_0 () Bool)
(declare-fun v0x7f798a156b50_0 () Bool)
(declare-fun v0x7f798a156e10_0 () Bool)
(declare-fun v0x7f798a156590_0 () Real)
(declare-fun F0x7f798a15bc90 () Bool)
(declare-fun E0x7f798a1576d0 () Bool)
(declare-fun v0x7f798a157ed0_0 () Bool)
(declare-fun F0x7f798a15bbd0 () Bool)
(declare-fun v0x7f798a15b010_0 () Bool)
(declare-fun v0x7f798a155010_0 () Real)
(declare-fun v0x7f798a159550_0 () Real)
(declare-fun v0x7f798a157410_0 () Real)
(declare-fun v0x7f798a15ad90_0 () Bool)
(declare-fun E0x7f798a157f90 () Bool)
(declare-fun v0x7f798a156650_0 () Real)
(declare-fun v0x7f798a159350_0 () Real)
(declare-fun F0x7f798a15bf10 () Bool)
(declare-fun v0x7f798a1584d0_0 () Real)
(declare-fun v0x7f798a156550_0 () Real)
(declare-fun v0x7f798a158190_0 () Bool)
(declare-fun v0x7f798a158c50_0 () Bool)
(declare-fun E0x7f798a158e50 () Bool)
(declare-fun E0x7f798a15a510 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f798a158d90_0 () Bool)
(declare-fun v0x7f798a155110_0 () Bool)
(declare-fun F0x7f798a15bb10 () Bool)

(assert (=> F0x7f798a15bb10
    (and v0x7f798a155110_0
         (<= v0x7f798a156550_0 0.0)
         (>= v0x7f798a156550_0 0.0)
         (<= v0x7f798a156650_0 1.0)
         (>= v0x7f798a156650_0 1.0)
         (<= v0x7f798a155010_0 0.0)
         (>= v0x7f798a155010_0 0.0))))
(assert (=> F0x7f798a15bb10 F0x7f798a15bbd0))
(assert (let ((a!1 (=> v0x7f798a157550_0
               (or (and v0x7f798a156e10_0
                        E0x7f798a1576d0
                        (<= v0x7f798a157610_0 v0x7f798a157410_0)
                        (>= v0x7f798a157610_0 v0x7f798a157410_0))
                   (and v0x7f798a156b50_0
                        E0x7f798a157890
                        v0x7f798a156cd0_0
                        (<= v0x7f798a157610_0 v0x7f798a156490_0)
                        (>= v0x7f798a157610_0 v0x7f798a156490_0)))))
      (a!2 (=> v0x7f798a157550_0
               (or (and E0x7f798a1576d0 (not E0x7f798a157890))
                   (and E0x7f798a157890 (not E0x7f798a1576d0)))))
      (a!3 (=> v0x7f798a158410_0
               (or (and v0x7f798a157ed0_0
                        E0x7f798a158590
                        (<= v0x7f798a1584d0_0 v0x7f798a1582d0_0)
                        (>= v0x7f798a1584d0_0 v0x7f798a1582d0_0))
                   (and v0x7f798a157550_0
                        E0x7f798a158750
                        v0x7f798a157d90_0
                        (<= v0x7f798a1584d0_0 v0x7f798a156310_0)
                        (>= v0x7f798a1584d0_0 v0x7f798a156310_0)))))
      (a!4 (=> v0x7f798a158410_0
               (or (and E0x7f798a158590 (not E0x7f798a158750))
                   (and E0x7f798a158750 (not E0x7f798a158590)))))
      (a!5 (=> v0x7f798a159490_0
               (or (and v0x7f798a158d90_0
                        E0x7f798a159610
                        (<= v0x7f798a159550_0 v0x7f798a159350_0)
                        (>= v0x7f798a159550_0 v0x7f798a159350_0))
                   (and v0x7f798a158410_0
                        E0x7f798a1597d0
                        v0x7f798a158c50_0
                        (<= v0x7f798a159550_0 v0x7f798a157610_0)
                        (>= v0x7f798a159550_0 v0x7f798a157610_0)))))
      (a!6 (=> v0x7f798a159490_0
               (or (and E0x7f798a159610 (not E0x7f798a1597d0))
                   (and E0x7f798a1597d0 (not E0x7f798a159610)))))
      (a!7 (=> v0x7f798a15a390_0
               (or (and v0x7f798a159e90_0
                        E0x7f798a15a510
                        (<= v0x7f798a15a450_0 v0x7f798a15a250_0)
                        (>= v0x7f798a15a450_0 v0x7f798a15a250_0))
                   (and v0x7f798a159490_0
                        E0x7f798a15a6d0
                        (not v0x7f798a159d50_0)
                        (<= v0x7f798a15a450_0 v0x7f798a156590_0)
                        (>= v0x7f798a15a450_0 v0x7f798a156590_0)))))
      (a!8 (=> v0x7f798a15a390_0
               (or (and E0x7f798a15a510 (not E0x7f798a15a6d0))
                   (and E0x7f798a15a6d0 (not E0x7f798a15a510))))))
(let ((a!9 (and (=> v0x7f798a156e10_0
                    (and v0x7f798a156b50_0
                         E0x7f798a156ed0
                         (not v0x7f798a156cd0_0)))
                (=> v0x7f798a156e10_0 E0x7f798a156ed0)
                a!1
                a!2
                (=> v0x7f798a157ed0_0
                    (and v0x7f798a157550_0
                         E0x7f798a157f90
                         (not v0x7f798a157d90_0)))
                (=> v0x7f798a157ed0_0 E0x7f798a157f90)
                a!3
                a!4
                (=> v0x7f798a158d90_0
                    (and v0x7f798a158410_0
                         E0x7f798a158e50
                         (not v0x7f798a158c50_0)))
                (=> v0x7f798a158d90_0 E0x7f798a158e50)
                a!5
                a!6
                (=> v0x7f798a159e90_0
                    (and v0x7f798a159490_0 E0x7f798a159f50 v0x7f798a159d50_0))
                (=> v0x7f798a159e90_0 E0x7f798a159f50)
                a!7
                a!8
                v0x7f798a15a390_0
                (not v0x7f798a15b010_0)
                (<= v0x7f798a156550_0 v0x7f798a1584d0_0)
                (>= v0x7f798a156550_0 v0x7f798a1584d0_0)
                (<= v0x7f798a156650_0 v0x7f798a159550_0)
                (>= v0x7f798a156650_0 v0x7f798a159550_0)
                (<= v0x7f798a155010_0 v0x7f798a15a450_0)
                (>= v0x7f798a155010_0 v0x7f798a15a450_0)
                (= v0x7f798a156cd0_0 (= v0x7f798a156c10_0 0.0))
                (= v0x7f798a157110_0 (< v0x7f798a156490_0 2.0))
                (= v0x7f798a1572d0_0 (ite v0x7f798a157110_0 1.0 0.0))
                (= v0x7f798a157410_0 (+ v0x7f798a1572d0_0 v0x7f798a156490_0))
                (= v0x7f798a157d90_0 (= v0x7f798a157cd0_0 0.0))
                (= v0x7f798a158190_0 (= v0x7f798a156310_0 0.0))
                (= v0x7f798a1582d0_0 (ite v0x7f798a158190_0 1.0 0.0))
                (= v0x7f798a158c50_0 (= v0x7f798a156590_0 0.0))
                (= v0x7f798a159050_0 (> v0x7f798a157610_0 0.0))
                (= v0x7f798a159190_0 (+ v0x7f798a157610_0 (- 1.0)))
                (= v0x7f798a159350_0
                   (ite v0x7f798a159050_0 v0x7f798a159190_0 v0x7f798a157610_0))
                (= v0x7f798a159c10_0 (> v0x7f798a159550_0 1.0))
                (= v0x7f798a159d50_0 (and v0x7f798a159c10_0 v0x7f798a158c50_0))
                (= v0x7f798a15a110_0 (= v0x7f798a1584d0_0 0.0))
                (= v0x7f798a15a250_0
                   (ite v0x7f798a15a110_0 1.0 v0x7f798a156590_0))
                (= v0x7f798a15ab50_0 (= v0x7f798a1584d0_0 0.0))
                (= v0x7f798a15ac50_0 (= v0x7f798a159550_0 2.0))
                (= v0x7f798a15ad90_0 (= v0x7f798a15a450_0 0.0))
                (= v0x7f798a15aed0_0 (and v0x7f798a15ac50_0 v0x7f798a15ab50_0))
                (= v0x7f798a15b010_0 (and v0x7f798a15aed0_0 v0x7f798a15ad90_0)))))
  (=> F0x7f798a15bc90 a!9))))
(assert (=> F0x7f798a15bc90 F0x7f798a15bd50))
(assert (let ((a!1 (=> v0x7f798a157550_0
               (or (and v0x7f798a156e10_0
                        E0x7f798a1576d0
                        (<= v0x7f798a157610_0 v0x7f798a157410_0)
                        (>= v0x7f798a157610_0 v0x7f798a157410_0))
                   (and v0x7f798a156b50_0
                        E0x7f798a157890
                        v0x7f798a156cd0_0
                        (<= v0x7f798a157610_0 v0x7f798a156490_0)
                        (>= v0x7f798a157610_0 v0x7f798a156490_0)))))
      (a!2 (=> v0x7f798a157550_0
               (or (and E0x7f798a1576d0 (not E0x7f798a157890))
                   (and E0x7f798a157890 (not E0x7f798a1576d0)))))
      (a!3 (=> v0x7f798a158410_0
               (or (and v0x7f798a157ed0_0
                        E0x7f798a158590
                        (<= v0x7f798a1584d0_0 v0x7f798a1582d0_0)
                        (>= v0x7f798a1584d0_0 v0x7f798a1582d0_0))
                   (and v0x7f798a157550_0
                        E0x7f798a158750
                        v0x7f798a157d90_0
                        (<= v0x7f798a1584d0_0 v0x7f798a156310_0)
                        (>= v0x7f798a1584d0_0 v0x7f798a156310_0)))))
      (a!4 (=> v0x7f798a158410_0
               (or (and E0x7f798a158590 (not E0x7f798a158750))
                   (and E0x7f798a158750 (not E0x7f798a158590)))))
      (a!5 (=> v0x7f798a159490_0
               (or (and v0x7f798a158d90_0
                        E0x7f798a159610
                        (<= v0x7f798a159550_0 v0x7f798a159350_0)
                        (>= v0x7f798a159550_0 v0x7f798a159350_0))
                   (and v0x7f798a158410_0
                        E0x7f798a1597d0
                        v0x7f798a158c50_0
                        (<= v0x7f798a159550_0 v0x7f798a157610_0)
                        (>= v0x7f798a159550_0 v0x7f798a157610_0)))))
      (a!6 (=> v0x7f798a159490_0
               (or (and E0x7f798a159610 (not E0x7f798a1597d0))
                   (and E0x7f798a1597d0 (not E0x7f798a159610)))))
      (a!7 (=> v0x7f798a15a390_0
               (or (and v0x7f798a159e90_0
                        E0x7f798a15a510
                        (<= v0x7f798a15a450_0 v0x7f798a15a250_0)
                        (>= v0x7f798a15a450_0 v0x7f798a15a250_0))
                   (and v0x7f798a159490_0
                        E0x7f798a15a6d0
                        (not v0x7f798a159d50_0)
                        (<= v0x7f798a15a450_0 v0x7f798a156590_0)
                        (>= v0x7f798a15a450_0 v0x7f798a156590_0)))))
      (a!8 (=> v0x7f798a15a390_0
               (or (and E0x7f798a15a510 (not E0x7f798a15a6d0))
                   (and E0x7f798a15a6d0 (not E0x7f798a15a510))))))
(let ((a!9 (and (=> v0x7f798a156e10_0
                    (and v0x7f798a156b50_0
                         E0x7f798a156ed0
                         (not v0x7f798a156cd0_0)))
                (=> v0x7f798a156e10_0 E0x7f798a156ed0)
                a!1
                a!2
                (=> v0x7f798a157ed0_0
                    (and v0x7f798a157550_0
                         E0x7f798a157f90
                         (not v0x7f798a157d90_0)))
                (=> v0x7f798a157ed0_0 E0x7f798a157f90)
                a!3
                a!4
                (=> v0x7f798a158d90_0
                    (and v0x7f798a158410_0
                         E0x7f798a158e50
                         (not v0x7f798a158c50_0)))
                (=> v0x7f798a158d90_0 E0x7f798a158e50)
                a!5
                a!6
                (=> v0x7f798a159e90_0
                    (and v0x7f798a159490_0 E0x7f798a159f50 v0x7f798a159d50_0))
                (=> v0x7f798a159e90_0 E0x7f798a159f50)
                a!7
                a!8
                v0x7f798a15a390_0
                v0x7f798a15b010_0
                (= v0x7f798a156cd0_0 (= v0x7f798a156c10_0 0.0))
                (= v0x7f798a157110_0 (< v0x7f798a156490_0 2.0))
                (= v0x7f798a1572d0_0 (ite v0x7f798a157110_0 1.0 0.0))
                (= v0x7f798a157410_0 (+ v0x7f798a1572d0_0 v0x7f798a156490_0))
                (= v0x7f798a157d90_0 (= v0x7f798a157cd0_0 0.0))
                (= v0x7f798a158190_0 (= v0x7f798a156310_0 0.0))
                (= v0x7f798a1582d0_0 (ite v0x7f798a158190_0 1.0 0.0))
                (= v0x7f798a158c50_0 (= v0x7f798a156590_0 0.0))
                (= v0x7f798a159050_0 (> v0x7f798a157610_0 0.0))
                (= v0x7f798a159190_0 (+ v0x7f798a157610_0 (- 1.0)))
                (= v0x7f798a159350_0
                   (ite v0x7f798a159050_0 v0x7f798a159190_0 v0x7f798a157610_0))
                (= v0x7f798a159c10_0 (> v0x7f798a159550_0 1.0))
                (= v0x7f798a159d50_0 (and v0x7f798a159c10_0 v0x7f798a158c50_0))
                (= v0x7f798a15a110_0 (= v0x7f798a1584d0_0 0.0))
                (= v0x7f798a15a250_0
                   (ite v0x7f798a15a110_0 1.0 v0x7f798a156590_0))
                (= v0x7f798a15ab50_0 (= v0x7f798a1584d0_0 0.0))
                (= v0x7f798a15ac50_0 (= v0x7f798a159550_0 2.0))
                (= v0x7f798a15ad90_0 (= v0x7f798a15a450_0 0.0))
                (= v0x7f798a15aed0_0 (and v0x7f798a15ac50_0 v0x7f798a15ab50_0))
                (= v0x7f798a15b010_0 (and v0x7f798a15aed0_0 v0x7f798a15ad90_0)))))
  (=> F0x7f798a15be10 a!9))))
(assert (=> F0x7f798a15be10 F0x7f798a15bd50))
(assert (=> F0x7f798a15bf50 (or F0x7f798a15bb10 F0x7f798a15bc90)))
(assert (=> F0x7f798a15bf10 F0x7f798a15be10))
(assert (=> pre!entry!0 (=> F0x7f798a15bbd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f798a15bd50 true)))
(assert (or (and (not post!bb1.i.i!0) F0x7f798a15bf50 false)
    (and (not post!bb2.i.i21.i.i!0) F0x7f798a15bf10 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0)
;(post-assumptions: post!bb1.i.i!0 post!bb2.i.i21.i.i!0)
