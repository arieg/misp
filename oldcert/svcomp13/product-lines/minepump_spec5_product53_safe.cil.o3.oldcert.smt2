(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7fd423119d50 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun F0x7fd423119a50 () Bool)
(declare-fun v0x7fd423118d10_0 () Bool)
(declare-fun v0x7fd423118bd0_0 () Bool)
(declare-fun v0x7fd423118a90_0 () Bool)
(declare-fun v0x7fd423117750_0 () Bool)
(declare-fun v0x7fd423117610_0 () Bool)
(declare-fun v0x7fd423114d90_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fd423115950_0 () Real)
(declare-fun v0x7fd423118e50_0 () Bool)
(declare-fun E0x7fd423118410 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7fd423118150 () Bool)
(declare-fun v0x7fd423116dd0_0 () Real)
(declare-fun v0x7fd423117dd0_0 () Real)
(declare-fun v0x7fd423114890_0 () Real)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun E0x7fd423117e90 () Bool)
(declare-fun v0x7fd423117890_0 () Bool)
(declare-fun E0x7fd423117a90 () Bool)
(declare-fun v0x7fd423117d10_0 () Real)
(declare-fun v0x7fd4231179d0_0 () Bool)
(declare-fun v0x7fd423116c90_0 () Bool)
(declare-fun v0x7fd4231168d0_0 () Bool)
(declare-fun E0x7fd423116fd0 () Bool)
(declare-fun v0x7fd4231174d0_0 () Real)
(declare-fun v0x7fd423116a10_0 () Bool)
(declare-fun v0x7fd423113f90_0 () Real)
(declare-fun v0x7fd423117310_0 () Real)
(declare-fun v0x7fd423115f50_0 () Real)
(declare-fun v0x7fd423116150_0 () Real)
(declare-fun E0x7fd4231163d0 () Bool)
(declare-fun v0x7fd423115a10_0 () Bool)
(declare-fun E0x7fd423115c10 () Bool)
(declare-fun v0x7fd423115b50_0 () Bool)
(declare-fun v0x7fd423115290_0 () Real)
(declare-fun v0x7fd423115090_0 () Real)
(declare-fun v0x7fd423116f10_0 () Bool)
(declare-fun F0x7fd423119990 () Bool)
(declare-fun E0x7fd423115510 () Bool)
(declare-fun E0x7fd423116210 () Bool)
(declare-fun v0x7fd423118950_0 () Bool)
(declare-fun v0x7fd423114950_0 () Bool)
(declare-fun v0x7fd423114a90_0 () Bool)
(declare-fun E0x7fd423114b50 () Bool)
(declare-fun F0x7fd423119b10 () Bool)
(declare-fun v0x7fd4231171d0_0 () Bool)
(declare-fun v0x7fd4231147d0_0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun E0x7fd423116ad0 () Bool)
(declare-fun v0x7fd423114210_0 () Real)
(declare-fun F0x7fd423119bd0 () Bool)
(declare-fun v0x7fd4231142d0_0 () Real)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun v0x7fd423116090_0 () Bool)
(declare-fun v0x7fd4231141d0_0 () Real)
(declare-fun v0x7fd423114f50_0 () Real)
(declare-fun v0x7fd423111110_0 () Bool)
(declare-fun v0x7fd423115e10_0 () Bool)
(declare-fun v0x7fd423114110_0 () Real)
(declare-fun E0x7fd423115350 () Bool)
(declare-fun F0x7fd423119d90 () Bool)
(declare-fun post!bb2.i.i37.i.i!0 () Bool)
(declare-fun v0x7fd423117c50_0 () Bool)
(declare-fun F0x7fd423119c90 () Bool)
(declare-fun v0x7fd423111010_0 () Real)
(declare-fun v0x7fd4231151d0_0 () Bool)

(assert (=> F0x7fd423119c90
    (and v0x7fd423111110_0
         (<= v0x7fd4231141d0_0 0.0)
         (>= v0x7fd4231141d0_0 0.0)
         (<= v0x7fd4231142d0_0 1.0)
         (>= v0x7fd4231142d0_0 1.0)
         (<= v0x7fd423111010_0 0.0)
         (>= v0x7fd423111010_0 0.0))))
(assert (=> F0x7fd423119c90 F0x7fd423119bd0))
(assert (let ((a!1 (=> v0x7fd4231151d0_0
               (or (and v0x7fd423114a90_0
                        E0x7fd423115350
                        (<= v0x7fd423115290_0 v0x7fd423115090_0)
                        (>= v0x7fd423115290_0 v0x7fd423115090_0))
                   (and v0x7fd4231147d0_0
                        E0x7fd423115510
                        v0x7fd423114950_0
                        (<= v0x7fd423115290_0 v0x7fd423114110_0)
                        (>= v0x7fd423115290_0 v0x7fd423114110_0)))))
      (a!2 (=> v0x7fd4231151d0_0
               (or (and E0x7fd423115350 (not E0x7fd423115510))
                   (and E0x7fd423115510 (not E0x7fd423115350)))))
      (a!3 (=> v0x7fd423116090_0
               (or (and v0x7fd423115b50_0
                        E0x7fd423116210
                        (<= v0x7fd423116150_0 v0x7fd423115f50_0)
                        (>= v0x7fd423116150_0 v0x7fd423115f50_0))
                   (and v0x7fd4231151d0_0
                        E0x7fd4231163d0
                        v0x7fd423115a10_0
                        (<= v0x7fd423116150_0 v0x7fd423113f90_0)
                        (>= v0x7fd423116150_0 v0x7fd423113f90_0)))))
      (a!4 (=> v0x7fd423116090_0
               (or (and E0x7fd423116210 (not E0x7fd4231163d0))
                   (and E0x7fd4231163d0 (not E0x7fd423116210)))))
      (a!5 (or (and v0x7fd423116a10_0
                    E0x7fd423117e90
                    (<= v0x7fd423117d10_0 v0x7fd423115290_0)
                    (>= v0x7fd423117d10_0 v0x7fd423115290_0)
                    (<= v0x7fd423117dd0_0 v0x7fd423116dd0_0)
                    (>= v0x7fd423117dd0_0 v0x7fd423116dd0_0))
               (and v0x7fd4231179d0_0
                    E0x7fd423118150
                    (and (<= v0x7fd423117d10_0 v0x7fd4231174d0_0)
                         (>= v0x7fd423117d10_0 v0x7fd4231174d0_0))
                    (<= v0x7fd423117dd0_0 v0x7fd423114210_0)
                    (>= v0x7fd423117dd0_0 v0x7fd423114210_0))
               (and v0x7fd423116f10_0
                    E0x7fd423118410
                    (not v0x7fd423117890_0)
                    (and (<= v0x7fd423117d10_0 v0x7fd4231174d0_0)
                         (>= v0x7fd423117d10_0 v0x7fd4231174d0_0))
                    (<= v0x7fd423117dd0_0 0.0)
                    (>= v0x7fd423117dd0_0 0.0))))
      (a!6 (=> v0x7fd423117c50_0
               (or (and E0x7fd423117e90
                        (not E0x7fd423118150)
                        (not E0x7fd423118410))
                   (and E0x7fd423118150
                        (not E0x7fd423117e90)
                        (not E0x7fd423118410))
                   (and E0x7fd423118410
                        (not E0x7fd423117e90)
                        (not E0x7fd423118150))))))
(let ((a!7 (and (=> v0x7fd423114a90_0
                    (and v0x7fd4231147d0_0
                         E0x7fd423114b50
                         (not v0x7fd423114950_0)))
                (=> v0x7fd423114a90_0 E0x7fd423114b50)
                a!1
                a!2
                (=> v0x7fd423115b50_0
                    (and v0x7fd4231151d0_0
                         E0x7fd423115c10
                         (not v0x7fd423115a10_0)))
                (=> v0x7fd423115b50_0 E0x7fd423115c10)
                a!3
                a!4
                (=> v0x7fd423116a10_0
                    (and v0x7fd423116090_0 E0x7fd423116ad0 v0x7fd4231168d0_0))
                (=> v0x7fd423116a10_0 E0x7fd423116ad0)
                (=> v0x7fd423116f10_0
                    (and v0x7fd423116090_0
                         E0x7fd423116fd0
                         (not v0x7fd4231168d0_0)))
                (=> v0x7fd423116f10_0 E0x7fd423116fd0)
                (=> v0x7fd4231179d0_0
                    (and v0x7fd423116f10_0 E0x7fd423117a90 v0x7fd423117890_0))
                (=> v0x7fd4231179d0_0 E0x7fd423117a90)
                (=> v0x7fd423117c50_0 a!5)
                a!6
                v0x7fd423117c50_0
                (not v0x7fd423118e50_0)
                (<= v0x7fd4231141d0_0 v0x7fd423116150_0)
                (>= v0x7fd4231141d0_0 v0x7fd423116150_0)
                (<= v0x7fd4231142d0_0 v0x7fd423117d10_0)
                (>= v0x7fd4231142d0_0 v0x7fd423117d10_0)
                (<= v0x7fd423111010_0 v0x7fd423117dd0_0)
                (>= v0x7fd423111010_0 v0x7fd423117dd0_0)
                (= v0x7fd423114950_0 (= v0x7fd423114890_0 0.0))
                (= v0x7fd423114d90_0 (< v0x7fd423114110_0 2.0))
                (= v0x7fd423114f50_0 (ite v0x7fd423114d90_0 1.0 0.0))
                (= v0x7fd423115090_0 (+ v0x7fd423114f50_0 v0x7fd423114110_0))
                (= v0x7fd423115a10_0 (= v0x7fd423115950_0 0.0))
                (= v0x7fd423115e10_0 (= v0x7fd423113f90_0 0.0))
                (= v0x7fd423115f50_0 (ite v0x7fd423115e10_0 1.0 0.0))
                (= v0x7fd4231168d0_0 (= v0x7fd423114210_0 0.0))
                (= v0x7fd423116c90_0 (> v0x7fd423115290_0 1.0))
                (= v0x7fd423116dd0_0
                   (ite v0x7fd423116c90_0 1.0 v0x7fd423114210_0))
                (= v0x7fd4231171d0_0 (> v0x7fd423115290_0 0.0))
                (= v0x7fd423117310_0 (+ v0x7fd423115290_0 (- 1.0)))
                (= v0x7fd4231174d0_0
                   (ite v0x7fd4231171d0_0 v0x7fd423117310_0 v0x7fd423115290_0))
                (= v0x7fd423117610_0 (= v0x7fd423116150_0 0.0))
                (= v0x7fd423117750_0 (= v0x7fd4231174d0_0 0.0))
                (= v0x7fd423117890_0 (and v0x7fd423117610_0 v0x7fd423117750_0))
                (= v0x7fd423118950_0 (= v0x7fd423117d10_0 2.0))
                (= v0x7fd423118a90_0 (= v0x7fd423117dd0_0 0.0))
                (= v0x7fd423118bd0_0 (or v0x7fd423118a90_0 v0x7fd423118950_0))
                (= v0x7fd423118d10_0 (xor v0x7fd423118bd0_0 true))
                (= v0x7fd423118e50_0 (and v0x7fd4231168d0_0 v0x7fd423118d10_0)))))
  (=> F0x7fd423119b10 a!7))))
(assert (=> F0x7fd423119b10 F0x7fd423119a50))
(assert (let ((a!1 (=> v0x7fd4231151d0_0
               (or (and v0x7fd423114a90_0
                        E0x7fd423115350
                        (<= v0x7fd423115290_0 v0x7fd423115090_0)
                        (>= v0x7fd423115290_0 v0x7fd423115090_0))
                   (and v0x7fd4231147d0_0
                        E0x7fd423115510
                        v0x7fd423114950_0
                        (<= v0x7fd423115290_0 v0x7fd423114110_0)
                        (>= v0x7fd423115290_0 v0x7fd423114110_0)))))
      (a!2 (=> v0x7fd4231151d0_0
               (or (and E0x7fd423115350 (not E0x7fd423115510))
                   (and E0x7fd423115510 (not E0x7fd423115350)))))
      (a!3 (=> v0x7fd423116090_0
               (or (and v0x7fd423115b50_0
                        E0x7fd423116210
                        (<= v0x7fd423116150_0 v0x7fd423115f50_0)
                        (>= v0x7fd423116150_0 v0x7fd423115f50_0))
                   (and v0x7fd4231151d0_0
                        E0x7fd4231163d0
                        v0x7fd423115a10_0
                        (<= v0x7fd423116150_0 v0x7fd423113f90_0)
                        (>= v0x7fd423116150_0 v0x7fd423113f90_0)))))
      (a!4 (=> v0x7fd423116090_0
               (or (and E0x7fd423116210 (not E0x7fd4231163d0))
                   (and E0x7fd4231163d0 (not E0x7fd423116210)))))
      (a!5 (or (and v0x7fd423116a10_0
                    E0x7fd423117e90
                    (<= v0x7fd423117d10_0 v0x7fd423115290_0)
                    (>= v0x7fd423117d10_0 v0x7fd423115290_0)
                    (<= v0x7fd423117dd0_0 v0x7fd423116dd0_0)
                    (>= v0x7fd423117dd0_0 v0x7fd423116dd0_0))
               (and v0x7fd4231179d0_0
                    E0x7fd423118150
                    (and (<= v0x7fd423117d10_0 v0x7fd4231174d0_0)
                         (>= v0x7fd423117d10_0 v0x7fd4231174d0_0))
                    (<= v0x7fd423117dd0_0 v0x7fd423114210_0)
                    (>= v0x7fd423117dd0_0 v0x7fd423114210_0))
               (and v0x7fd423116f10_0
                    E0x7fd423118410
                    (not v0x7fd423117890_0)
                    (and (<= v0x7fd423117d10_0 v0x7fd4231174d0_0)
                         (>= v0x7fd423117d10_0 v0x7fd4231174d0_0))
                    (<= v0x7fd423117dd0_0 0.0)
                    (>= v0x7fd423117dd0_0 0.0))))
      (a!6 (=> v0x7fd423117c50_0
               (or (and E0x7fd423117e90
                        (not E0x7fd423118150)
                        (not E0x7fd423118410))
                   (and E0x7fd423118150
                        (not E0x7fd423117e90)
                        (not E0x7fd423118410))
                   (and E0x7fd423118410
                        (not E0x7fd423117e90)
                        (not E0x7fd423118150))))))
(let ((a!7 (and (=> v0x7fd423114a90_0
                    (and v0x7fd4231147d0_0
                         E0x7fd423114b50
                         (not v0x7fd423114950_0)))
                (=> v0x7fd423114a90_0 E0x7fd423114b50)
                a!1
                a!2
                (=> v0x7fd423115b50_0
                    (and v0x7fd4231151d0_0
                         E0x7fd423115c10
                         (not v0x7fd423115a10_0)))
                (=> v0x7fd423115b50_0 E0x7fd423115c10)
                a!3
                a!4
                (=> v0x7fd423116a10_0
                    (and v0x7fd423116090_0 E0x7fd423116ad0 v0x7fd4231168d0_0))
                (=> v0x7fd423116a10_0 E0x7fd423116ad0)
                (=> v0x7fd423116f10_0
                    (and v0x7fd423116090_0
                         E0x7fd423116fd0
                         (not v0x7fd4231168d0_0)))
                (=> v0x7fd423116f10_0 E0x7fd423116fd0)
                (=> v0x7fd4231179d0_0
                    (and v0x7fd423116f10_0 E0x7fd423117a90 v0x7fd423117890_0))
                (=> v0x7fd4231179d0_0 E0x7fd423117a90)
                (=> v0x7fd423117c50_0 a!5)
                a!6
                v0x7fd423117c50_0
                v0x7fd423118e50_0
                (= v0x7fd423114950_0 (= v0x7fd423114890_0 0.0))
                (= v0x7fd423114d90_0 (< v0x7fd423114110_0 2.0))
                (= v0x7fd423114f50_0 (ite v0x7fd423114d90_0 1.0 0.0))
                (= v0x7fd423115090_0 (+ v0x7fd423114f50_0 v0x7fd423114110_0))
                (= v0x7fd423115a10_0 (= v0x7fd423115950_0 0.0))
                (= v0x7fd423115e10_0 (= v0x7fd423113f90_0 0.0))
                (= v0x7fd423115f50_0 (ite v0x7fd423115e10_0 1.0 0.0))
                (= v0x7fd4231168d0_0 (= v0x7fd423114210_0 0.0))
                (= v0x7fd423116c90_0 (> v0x7fd423115290_0 1.0))
                (= v0x7fd423116dd0_0
                   (ite v0x7fd423116c90_0 1.0 v0x7fd423114210_0))
                (= v0x7fd4231171d0_0 (> v0x7fd423115290_0 0.0))
                (= v0x7fd423117310_0 (+ v0x7fd423115290_0 (- 1.0)))
                (= v0x7fd4231174d0_0
                   (ite v0x7fd4231171d0_0 v0x7fd423117310_0 v0x7fd423115290_0))
                (= v0x7fd423117610_0 (= v0x7fd423116150_0 0.0))
                (= v0x7fd423117750_0 (= v0x7fd4231174d0_0 0.0))
                (= v0x7fd423117890_0 (and v0x7fd423117610_0 v0x7fd423117750_0))
                (= v0x7fd423118950_0 (= v0x7fd423117d10_0 2.0))
                (= v0x7fd423118a90_0 (= v0x7fd423117dd0_0 0.0))
                (= v0x7fd423118bd0_0 (or v0x7fd423118a90_0 v0x7fd423118950_0))
                (= v0x7fd423118d10_0 (xor v0x7fd423118bd0_0 true))
                (= v0x7fd423118e50_0 (and v0x7fd4231168d0_0 v0x7fd423118d10_0)))))
  (=> F0x7fd423119990 a!7))))
(assert (=> F0x7fd423119990 F0x7fd423119a50))
(assert (=> F0x7fd423119d90 (or F0x7fd423119c90 F0x7fd423119b10)))
(assert (=> F0x7fd423119d50 F0x7fd423119990))
(assert (=> pre!entry!0 (=> F0x7fd423119bd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd423119a50 (>= v0x7fd423113f90_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fd423119a50
        (or (>= v0x7fd423114110_0 2.0) (<= v0x7fd423114210_0 0.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fd423119a50 (>= v0x7fd423114110_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fd423119a50
        (or (<= v0x7fd423114110_0 1.0) (>= v0x7fd423114210_0 1.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7fd423119a50 (<= v0x7fd423114110_0 2.0))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7fd423119a50
        (or (>= v0x7fd423114110_0 1.0) (<= v0x7fd423114110_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7fd423119d90
                (not (or (>= v0x7fd4231142d0_0 2.0) (<= v0x7fd423111010_0 0.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7fd423119d90
                (not (or (<= v0x7fd4231142d0_0 1.0) (>= v0x7fd423111010_0 1.0)))))
      (a!3 (and (not post!bb1.i.i!5)
                F0x7fd423119d90
                (not (or (>= v0x7fd4231142d0_0 1.0) (<= v0x7fd4231142d0_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7fd423119d90
           (not (>= v0x7fd4231141d0_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7fd423119d90
           (not (>= v0x7fd4231142d0_0 0.0)))
      a!2
      (and (not post!bb1.i.i!4)
           F0x7fd423119d90
           (not (<= v0x7fd4231142d0_0 2.0)))
      a!3
      (and (not post!bb2.i.i37.i.i!0) F0x7fd423119d50 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i37.i.i!0)
