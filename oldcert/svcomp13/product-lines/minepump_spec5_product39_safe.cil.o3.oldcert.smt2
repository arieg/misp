(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7fe782db8e90 () Bool)
(declare-fun F0x7fe782db8ed0 () Bool)
(declare-fun v0x7fe782db7d50_0 () Bool)
(declare-fun v0x7fe782db7ad0_0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun v0x7fe782db4510_0 () Bool)
(declare-fun v0x7fe782db3550_0 () Real)
(declare-fun v0x7fe782db2b50_0 () Bool)
(declare-fun v0x7fe782db2690_0 () Real)
(declare-fun v0x7fe782db1c90_0 () Real)
(declare-fun v0x7fe782db1ad0_0 () Bool)
(declare-fun v0x7fe782db7e90_0 () Bool)
(declare-fun F0x7fe782db8d10 () Bool)
(declare-fun E0x7fe782db6e10 () Bool)
(declare-fun v0x7fe782db6190_0 () Real)
(declare-fun v0x7fe782db60d0_0 () Real)
(declare-fun v0x7fe782db5c50_0 () Bool)
(declare-fun v0x7fe782db6010_0 () Bool)
(declare-fun v0x7fe782db5d90_0 () Bool)
(declare-fun v0x7fe782db59d0_0 () Bool)
(declare-fun E0x7fe782db4c90 () Bool)
(declare-fun E0x7fe782db63d0 () Bool)
(declare-fun v0x7fe782db39d0_0 () Real)
(declare-fun v0x7fe782db4810_0 () Real)
(declare-fun v0x7fe782db54d0_0 () Bool)
(declare-fun v0x7fe782db7850_0 () Bool)
(declare-fun v0x7fe782db4a10_0 () Real)
(declare-fun v0x7fe782db4950_0 () Bool)
(declare-fun v0x7fe782db4110_0 () Bool)
(declare-fun v0x7fe782db0e10_0 () Real)
(declare-fun E0x7fe782db4310 () Bool)
(declare-fun E0x7fe782db3d90 () Bool)
(declare-fun v0x7fe782db7c10_0 () Bool)
(declare-fun E0x7fe782db5e50 () Bool)
(declare-fun v0x7fe782db3610_0 () Bool)
(declare-fun v0x7fe782db3a90_0 () Bool)
(declare-fun v0x7fe782db4650_0 () Real)
(declare-fun v0x7fe782db0a90_0 () Real)
(declare-fun v0x7fe782db15d0_0 () Real)
(declare-fun E0x7fe782db3110 () Bool)
(declare-fun v0x7fe782db3750_0 () Bool)
(declare-fun v0x7fe782db2c90_0 () Real)
(declare-fun v0x7fe782db3bd0_0 () Bool)
(declare-fun E0x7fe782db5a90 () Bool)
(declare-fun E0x7fe782db2f50 () Bool)
(declare-fun v0x7fe782db2890_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7fe782db3810 () Bool)
(declare-fun E0x7fe782db6b10 () Bool)
(declare-fun v0x7fe782db0c10_0 () Real)
(declare-fun v0x7fe782db1fd0_0 () Real)
(declare-fun F0x7fe782db8dd0 () Bool)
(declare-fun v0x7fe782db5750_0 () Bool)
(declare-fun v0x7fe782db2750_0 () Bool)
(declare-fun E0x7fe782db2090 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun E0x7fe782db1890 () Bool)
(declare-fun F0x7fe782db8b50 () Bool)
(declare-fun E0x7fe782db52d0 () Bool)
(declare-fun F0x7fe782db8c10 () Bool)
(declare-fun E0x7fe782db5590 () Bool)
(declare-fun v0x7fe782db5210_0 () Bool)
(declare-fun v0x7fe782daf010_0 () Real)
(declare-fun v0x7fe782db1510_0 () Bool)
(declare-fun v0x7fe782db2e90_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fe782db0ed0_0 () Real)
(declare-fun v0x7fe782db0d10_0 () Real)
(declare-fun E0x7fe782db2950 () Bool)
(declare-fun E0x7fe782db2250 () Bool)
(declare-fun v0x7fe782db0dd0_0 () Real)
(declare-fun v0x7fe782db2dd0_0 () Bool)
(declare-fun v0x7fe782db6250_0 () Real)
(declare-fun post!bb2.i.i25.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7fe782db1690_0 () Bool)
(declare-fun E0x7fe782db3c90 () Bool)
(declare-fun E0x7fe782db4ad0 () Bool)
(declare-fun E0x7fe782db6890 () Bool)
(declare-fun v0x7fe782db7990_0 () Bool)
(declare-fun v0x7fe782db0cd0_0 () Real)
(declare-fun v0x7fe782db50d0_0 () Bool)
(declare-fun E0x7fe782db7010 () Bool)
(declare-fun v0x7fe782db4250_0 () Bool)
(declare-fun v0x7fe782daf110_0 () Bool)
(declare-fun v0x7fe782db1dd0_0 () Real)
(declare-fun v0x7fe782db1f10_0 () Bool)
(declare-fun v0x7fe782db5890_0 () Real)
(declare-fun v0x7fe782db17d0_0 () Bool)
(declare-fun F0x7fe782db8cd0 () Bool)
(declare-fun v0x7fe782db6310_0 () Real)

(assert (=> F0x7fe782db8cd0
    (and v0x7fe782daf110_0
         (<= v0x7fe782db0cd0_0 0.0)
         (>= v0x7fe782db0cd0_0 0.0)
         (<= v0x7fe782db0dd0_0 1.0)
         (>= v0x7fe782db0dd0_0 1.0)
         (<= v0x7fe782db0ed0_0 1.0)
         (>= v0x7fe782db0ed0_0 1.0)
         (<= v0x7fe782daf010_0 0.0)
         (>= v0x7fe782daf010_0 0.0))))
(assert (=> F0x7fe782db8cd0 F0x7fe782db8c10))
(assert (let ((a!1 (=> v0x7fe782db1f10_0
               (or (and v0x7fe782db17d0_0
                        E0x7fe782db2090
                        (<= v0x7fe782db1fd0_0 v0x7fe782db1dd0_0)
                        (>= v0x7fe782db1fd0_0 v0x7fe782db1dd0_0))
                   (and v0x7fe782db1510_0
                        E0x7fe782db2250
                        v0x7fe782db1690_0
                        (<= v0x7fe782db1fd0_0 v0x7fe782db0c10_0)
                        (>= v0x7fe782db1fd0_0 v0x7fe782db0c10_0)))))
      (a!2 (=> v0x7fe782db1f10_0
               (or (and E0x7fe782db2090 (not E0x7fe782db2250))
                   (and E0x7fe782db2250 (not E0x7fe782db2090)))))
      (a!3 (=> v0x7fe782db2dd0_0
               (or (and v0x7fe782db2890_0
                        E0x7fe782db2f50
                        (<= v0x7fe782db2e90_0 v0x7fe782db2c90_0)
                        (>= v0x7fe782db2e90_0 v0x7fe782db2c90_0))
                   (and v0x7fe782db1f10_0
                        E0x7fe782db3110
                        v0x7fe782db2750_0
                        (<= v0x7fe782db2e90_0 v0x7fe782db0a90_0)
                        (>= v0x7fe782db2e90_0 v0x7fe782db0a90_0)))))
      (a!4 (=> v0x7fe782db2dd0_0
               (or (and E0x7fe782db2f50 (not E0x7fe782db3110))
                   (and E0x7fe782db3110 (not E0x7fe782db2f50)))))
      (a!5 (=> v0x7fe782db3bd0_0
               (or (and v0x7fe782db3750_0 E0x7fe782db3c90 v0x7fe782db3a90_0)
                   (and v0x7fe782db2dd0_0
                        E0x7fe782db3d90
                        (not v0x7fe782db3610_0)))))
      (a!6 (=> v0x7fe782db3bd0_0
               (or (and E0x7fe782db3c90 (not E0x7fe782db3d90))
                   (and E0x7fe782db3d90 (not E0x7fe782db3c90)))))
      (a!7 (=> v0x7fe782db4950_0
               (or (and v0x7fe782db4250_0
                        E0x7fe782db4ad0
                        (<= v0x7fe782db4a10_0 v0x7fe782db4810_0)
                        (>= v0x7fe782db4a10_0 v0x7fe782db4810_0))
                   (and v0x7fe782db3bd0_0
                        E0x7fe782db4c90
                        v0x7fe782db4110_0
                        (<= v0x7fe782db4a10_0 v0x7fe782db1fd0_0)
                        (>= v0x7fe782db4a10_0 v0x7fe782db1fd0_0)))))
      (a!8 (=> v0x7fe782db4950_0
               (or (and E0x7fe782db4ad0 (not E0x7fe782db4c90))
                   (and E0x7fe782db4c90 (not E0x7fe782db4ad0)))))
      (a!9 (or (and v0x7fe782db54d0_0
                    E0x7fe782db63d0
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (<= v0x7fe782db6310_0 v0x7fe782db5890_0)
                    (>= v0x7fe782db6310_0 v0x7fe782db5890_0))
               (and v0x7fe782db5d90_0
                    E0x7fe782db6890
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (and (<= v0x7fe782db6310_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db6310_0 v0x7fe782db0e10_0)))
               (and v0x7fe782db59d0_0
                    E0x7fe782db6b10
                    (not v0x7fe782db5c50_0)
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (and (<= v0x7fe782db6310_0 0.0) (>= v0x7fe782db6310_0 0.0)))
               (and v0x7fe782db4950_0
                    E0x7fe782db6e10
                    v0x7fe782db50d0_0
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (and (<= v0x7fe782db6310_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db6310_0 v0x7fe782db0e10_0)))
               (and v0x7fe782db3750_0
                    E0x7fe782db7010
                    (not v0x7fe782db3a90_0)
                    (<= v0x7fe782db60d0_0 0.0)
                    (>= v0x7fe782db60d0_0 0.0)
                    (<= v0x7fe782db6190_0 0.0)
                    (>= v0x7fe782db6190_0 0.0)
                    (<= v0x7fe782db6250_0 v0x7fe782db1fd0_0)
                    (>= v0x7fe782db6250_0 v0x7fe782db1fd0_0)
                    (and (<= v0x7fe782db6310_0 0.0) (>= v0x7fe782db6310_0 0.0)))))
      (a!10 (=> v0x7fe782db6010_0
                (or (and E0x7fe782db63d0
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db6e10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db6890
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db6e10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db6b10
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6e10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db6e10
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db7010
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db6e10))))))
(let ((a!11 (and (=> v0x7fe782db17d0_0
                     (and v0x7fe782db1510_0
                          E0x7fe782db1890
                          (not v0x7fe782db1690_0)))
                 (=> v0x7fe782db17d0_0 E0x7fe782db1890)
                 a!1
                 a!2
                 (=> v0x7fe782db2890_0
                     (and v0x7fe782db1f10_0
                          E0x7fe782db2950
                          (not v0x7fe782db2750_0)))
                 (=> v0x7fe782db2890_0 E0x7fe782db2950)
                 a!3
                 a!4
                 (=> v0x7fe782db3750_0
                     (and v0x7fe782db2dd0_0 E0x7fe782db3810 v0x7fe782db3610_0))
                 (=> v0x7fe782db3750_0 E0x7fe782db3810)
                 a!5
                 a!6
                 (=> v0x7fe782db4250_0
                     (and v0x7fe782db3bd0_0
                          E0x7fe782db4310
                          (not v0x7fe782db4110_0)))
                 (=> v0x7fe782db4250_0 E0x7fe782db4310)
                 a!7
                 a!8
                 (=> v0x7fe782db5210_0
                     (and v0x7fe782db4950_0
                          E0x7fe782db52d0
                          (not v0x7fe782db50d0_0)))
                 (=> v0x7fe782db5210_0 E0x7fe782db52d0)
                 (=> v0x7fe782db54d0_0
                     (and v0x7fe782db5210_0 E0x7fe782db5590 v0x7fe782db4110_0))
                 (=> v0x7fe782db54d0_0 E0x7fe782db5590)
                 (=> v0x7fe782db59d0_0
                     (and v0x7fe782db5210_0
                          E0x7fe782db5a90
                          (not v0x7fe782db4110_0)))
                 (=> v0x7fe782db59d0_0 E0x7fe782db5a90)
                 (=> v0x7fe782db5d90_0
                     (and v0x7fe782db59d0_0 E0x7fe782db5e50 v0x7fe782db5c50_0))
                 (=> v0x7fe782db5d90_0 E0x7fe782db5e50)
                 (=> v0x7fe782db6010_0 a!9)
                 a!10
                 v0x7fe782db6010_0
                 (not v0x7fe782db7e90_0)
                 (<= v0x7fe782db0cd0_0 v0x7fe782db2e90_0)
                 (>= v0x7fe782db0cd0_0 v0x7fe782db2e90_0)
                 (<= v0x7fe782db0dd0_0 v0x7fe782db6250_0)
                 (>= v0x7fe782db0dd0_0 v0x7fe782db6250_0)
                 (<= v0x7fe782db0ed0_0 v0x7fe782db6190_0)
                 (>= v0x7fe782db0ed0_0 v0x7fe782db6190_0)
                 (<= v0x7fe782daf010_0 v0x7fe782db6310_0)
                 (>= v0x7fe782daf010_0 v0x7fe782db6310_0)
                 (= v0x7fe782db1690_0 (= v0x7fe782db15d0_0 0.0))
                 (= v0x7fe782db1ad0_0 (< v0x7fe782db0c10_0 2.0))
                 (= v0x7fe782db1c90_0 (ite v0x7fe782db1ad0_0 1.0 0.0))
                 (= v0x7fe782db1dd0_0 (+ v0x7fe782db1c90_0 v0x7fe782db0c10_0))
                 (= v0x7fe782db2750_0 (= v0x7fe782db2690_0 0.0))
                 (= v0x7fe782db2b50_0 (= v0x7fe782db0a90_0 0.0))
                 (= v0x7fe782db2c90_0 (ite v0x7fe782db2b50_0 1.0 0.0))
                 (= v0x7fe782db3610_0 (= v0x7fe782db3550_0 0.0))
                 (= v0x7fe782db3a90_0 (= v0x7fe782db39d0_0 0.0))
                 (= v0x7fe782db4110_0 (= v0x7fe782db0e10_0 0.0))
                 (= v0x7fe782db4510_0 (> v0x7fe782db1fd0_0 0.0))
                 (= v0x7fe782db4650_0 (+ v0x7fe782db1fd0_0 (- 1.0)))
                 (= v0x7fe782db4810_0
                    (ite v0x7fe782db4510_0 v0x7fe782db4650_0 v0x7fe782db1fd0_0))
                 (= v0x7fe782db50d0_0 (= v0x7fe782db0d10_0 0.0))
                 (= v0x7fe782db5750_0 (> v0x7fe782db4a10_0 1.0))
                 (= v0x7fe782db5890_0
                    (ite v0x7fe782db5750_0 1.0 v0x7fe782db0e10_0))
                 (= v0x7fe782db5c50_0 (= v0x7fe782db2e90_0 0.0))
                 (= v0x7fe782db7850_0 (= v0x7fe782db6250_0 2.0))
                 (= v0x7fe782db7990_0 (= v0x7fe782db6310_0 0.0))
                 (= v0x7fe782db7ad0_0 (or v0x7fe782db7990_0 v0x7fe782db7850_0))
                 (= v0x7fe782db7c10_0 (xor v0x7fe782db7ad0_0 true))
                 (= v0x7fe782db7d50_0 (= v0x7fe782db60d0_0 0.0))
                 (= v0x7fe782db7e90_0 (and v0x7fe782db7d50_0 v0x7fe782db7c10_0)))))
  (=> F0x7fe782db8b50 a!11))))
(assert (=> F0x7fe782db8b50 F0x7fe782db8d10))
(assert (let ((a!1 (=> v0x7fe782db1f10_0
               (or (and v0x7fe782db17d0_0
                        E0x7fe782db2090
                        (<= v0x7fe782db1fd0_0 v0x7fe782db1dd0_0)
                        (>= v0x7fe782db1fd0_0 v0x7fe782db1dd0_0))
                   (and v0x7fe782db1510_0
                        E0x7fe782db2250
                        v0x7fe782db1690_0
                        (<= v0x7fe782db1fd0_0 v0x7fe782db0c10_0)
                        (>= v0x7fe782db1fd0_0 v0x7fe782db0c10_0)))))
      (a!2 (=> v0x7fe782db1f10_0
               (or (and E0x7fe782db2090 (not E0x7fe782db2250))
                   (and E0x7fe782db2250 (not E0x7fe782db2090)))))
      (a!3 (=> v0x7fe782db2dd0_0
               (or (and v0x7fe782db2890_0
                        E0x7fe782db2f50
                        (<= v0x7fe782db2e90_0 v0x7fe782db2c90_0)
                        (>= v0x7fe782db2e90_0 v0x7fe782db2c90_0))
                   (and v0x7fe782db1f10_0
                        E0x7fe782db3110
                        v0x7fe782db2750_0
                        (<= v0x7fe782db2e90_0 v0x7fe782db0a90_0)
                        (>= v0x7fe782db2e90_0 v0x7fe782db0a90_0)))))
      (a!4 (=> v0x7fe782db2dd0_0
               (or (and E0x7fe782db2f50 (not E0x7fe782db3110))
                   (and E0x7fe782db3110 (not E0x7fe782db2f50)))))
      (a!5 (=> v0x7fe782db3bd0_0
               (or (and v0x7fe782db3750_0 E0x7fe782db3c90 v0x7fe782db3a90_0)
                   (and v0x7fe782db2dd0_0
                        E0x7fe782db3d90
                        (not v0x7fe782db3610_0)))))
      (a!6 (=> v0x7fe782db3bd0_0
               (or (and E0x7fe782db3c90 (not E0x7fe782db3d90))
                   (and E0x7fe782db3d90 (not E0x7fe782db3c90)))))
      (a!7 (=> v0x7fe782db4950_0
               (or (and v0x7fe782db4250_0
                        E0x7fe782db4ad0
                        (<= v0x7fe782db4a10_0 v0x7fe782db4810_0)
                        (>= v0x7fe782db4a10_0 v0x7fe782db4810_0))
                   (and v0x7fe782db3bd0_0
                        E0x7fe782db4c90
                        v0x7fe782db4110_0
                        (<= v0x7fe782db4a10_0 v0x7fe782db1fd0_0)
                        (>= v0x7fe782db4a10_0 v0x7fe782db1fd0_0)))))
      (a!8 (=> v0x7fe782db4950_0
               (or (and E0x7fe782db4ad0 (not E0x7fe782db4c90))
                   (and E0x7fe782db4c90 (not E0x7fe782db4ad0)))))
      (a!9 (or (and v0x7fe782db54d0_0
                    E0x7fe782db63d0
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (<= v0x7fe782db6310_0 v0x7fe782db5890_0)
                    (>= v0x7fe782db6310_0 v0x7fe782db5890_0))
               (and v0x7fe782db5d90_0
                    E0x7fe782db6890
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (and (<= v0x7fe782db6310_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db6310_0 v0x7fe782db0e10_0)))
               (and v0x7fe782db59d0_0
                    E0x7fe782db6b10
                    (not v0x7fe782db5c50_0)
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (and (<= v0x7fe782db6310_0 0.0) (>= v0x7fe782db6310_0 0.0)))
               (and v0x7fe782db4950_0
                    E0x7fe782db6e10
                    v0x7fe782db50d0_0
                    (and (<= v0x7fe782db60d0_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db60d0_0 v0x7fe782db0e10_0))
                    (and (<= v0x7fe782db6190_0 v0x7fe782db0d10_0)
                         (>= v0x7fe782db6190_0 v0x7fe782db0d10_0))
                    (and (<= v0x7fe782db6250_0 v0x7fe782db4a10_0)
                         (>= v0x7fe782db6250_0 v0x7fe782db4a10_0))
                    (and (<= v0x7fe782db6310_0 v0x7fe782db0e10_0)
                         (>= v0x7fe782db6310_0 v0x7fe782db0e10_0)))
               (and v0x7fe782db3750_0
                    E0x7fe782db7010
                    (not v0x7fe782db3a90_0)
                    (<= v0x7fe782db60d0_0 0.0)
                    (>= v0x7fe782db60d0_0 0.0)
                    (<= v0x7fe782db6190_0 0.0)
                    (>= v0x7fe782db6190_0 0.0)
                    (<= v0x7fe782db6250_0 v0x7fe782db1fd0_0)
                    (>= v0x7fe782db6250_0 v0x7fe782db1fd0_0)
                    (and (<= v0x7fe782db6310_0 0.0) (>= v0x7fe782db6310_0 0.0)))))
      (a!10 (=> v0x7fe782db6010_0
                (or (and E0x7fe782db63d0
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db6e10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db6890
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db6e10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db6b10
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6e10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db6e10
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db7010))
                    (and E0x7fe782db7010
                         (not E0x7fe782db63d0)
                         (not E0x7fe782db6890)
                         (not E0x7fe782db6b10)
                         (not E0x7fe782db6e10))))))
(let ((a!11 (and (=> v0x7fe782db17d0_0
                     (and v0x7fe782db1510_0
                          E0x7fe782db1890
                          (not v0x7fe782db1690_0)))
                 (=> v0x7fe782db17d0_0 E0x7fe782db1890)
                 a!1
                 a!2
                 (=> v0x7fe782db2890_0
                     (and v0x7fe782db1f10_0
                          E0x7fe782db2950
                          (not v0x7fe782db2750_0)))
                 (=> v0x7fe782db2890_0 E0x7fe782db2950)
                 a!3
                 a!4
                 (=> v0x7fe782db3750_0
                     (and v0x7fe782db2dd0_0 E0x7fe782db3810 v0x7fe782db3610_0))
                 (=> v0x7fe782db3750_0 E0x7fe782db3810)
                 a!5
                 a!6
                 (=> v0x7fe782db4250_0
                     (and v0x7fe782db3bd0_0
                          E0x7fe782db4310
                          (not v0x7fe782db4110_0)))
                 (=> v0x7fe782db4250_0 E0x7fe782db4310)
                 a!7
                 a!8
                 (=> v0x7fe782db5210_0
                     (and v0x7fe782db4950_0
                          E0x7fe782db52d0
                          (not v0x7fe782db50d0_0)))
                 (=> v0x7fe782db5210_0 E0x7fe782db52d0)
                 (=> v0x7fe782db54d0_0
                     (and v0x7fe782db5210_0 E0x7fe782db5590 v0x7fe782db4110_0))
                 (=> v0x7fe782db54d0_0 E0x7fe782db5590)
                 (=> v0x7fe782db59d0_0
                     (and v0x7fe782db5210_0
                          E0x7fe782db5a90
                          (not v0x7fe782db4110_0)))
                 (=> v0x7fe782db59d0_0 E0x7fe782db5a90)
                 (=> v0x7fe782db5d90_0
                     (and v0x7fe782db59d0_0 E0x7fe782db5e50 v0x7fe782db5c50_0))
                 (=> v0x7fe782db5d90_0 E0x7fe782db5e50)
                 (=> v0x7fe782db6010_0 a!9)
                 a!10
                 v0x7fe782db6010_0
                 v0x7fe782db7e90_0
                 (= v0x7fe782db1690_0 (= v0x7fe782db15d0_0 0.0))
                 (= v0x7fe782db1ad0_0 (< v0x7fe782db0c10_0 2.0))
                 (= v0x7fe782db1c90_0 (ite v0x7fe782db1ad0_0 1.0 0.0))
                 (= v0x7fe782db1dd0_0 (+ v0x7fe782db1c90_0 v0x7fe782db0c10_0))
                 (= v0x7fe782db2750_0 (= v0x7fe782db2690_0 0.0))
                 (= v0x7fe782db2b50_0 (= v0x7fe782db0a90_0 0.0))
                 (= v0x7fe782db2c90_0 (ite v0x7fe782db2b50_0 1.0 0.0))
                 (= v0x7fe782db3610_0 (= v0x7fe782db3550_0 0.0))
                 (= v0x7fe782db3a90_0 (= v0x7fe782db39d0_0 0.0))
                 (= v0x7fe782db4110_0 (= v0x7fe782db0e10_0 0.0))
                 (= v0x7fe782db4510_0 (> v0x7fe782db1fd0_0 0.0))
                 (= v0x7fe782db4650_0 (+ v0x7fe782db1fd0_0 (- 1.0)))
                 (= v0x7fe782db4810_0
                    (ite v0x7fe782db4510_0 v0x7fe782db4650_0 v0x7fe782db1fd0_0))
                 (= v0x7fe782db50d0_0 (= v0x7fe782db0d10_0 0.0))
                 (= v0x7fe782db5750_0 (> v0x7fe782db4a10_0 1.0))
                 (= v0x7fe782db5890_0
                    (ite v0x7fe782db5750_0 1.0 v0x7fe782db0e10_0))
                 (= v0x7fe782db5c50_0 (= v0x7fe782db2e90_0 0.0))
                 (= v0x7fe782db7850_0 (= v0x7fe782db6250_0 2.0))
                 (= v0x7fe782db7990_0 (= v0x7fe782db6310_0 0.0))
                 (= v0x7fe782db7ad0_0 (or v0x7fe782db7990_0 v0x7fe782db7850_0))
                 (= v0x7fe782db7c10_0 (xor v0x7fe782db7ad0_0 true))
                 (= v0x7fe782db7d50_0 (= v0x7fe782db60d0_0 0.0))
                 (= v0x7fe782db7e90_0 (and v0x7fe782db7d50_0 v0x7fe782db7c10_0)))))
  (=> F0x7fe782db8dd0 a!11))))
(assert (=> F0x7fe782db8dd0 F0x7fe782db8d10))
(assert (=> F0x7fe782db8ed0 (or F0x7fe782db8cd0 F0x7fe782db8b50)))
(assert (=> F0x7fe782db8e90 F0x7fe782db8dd0))
(assert (=> pre!entry!0 (=> F0x7fe782db8c10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fe782db8d10 (<= v0x7fe782db0c10_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fe782db8d10
        (or (<= v0x7fe782db0c10_0 1.0) (>= v0x7fe782db0c10_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fe782db8d10 (>= v0x7fe782db0c10_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fe782db8d10
        (or (>= v0x7fe782db0c10_0 1.0) (<= v0x7fe782db0c10_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7fe782db8ed0
                (not (or (<= v0x7fe782db0dd0_0 1.0) (>= v0x7fe782db0dd0_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7fe782db8ed0
                (not (or (>= v0x7fe782db0dd0_0 1.0) (<= v0x7fe782db0dd0_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7fe782db8ed0
           (not (<= v0x7fe782db0dd0_0 2.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7fe782db8ed0
           (not (>= v0x7fe782db0dd0_0 0.0)))
      a!2
      (and (not post!bb2.i.i25.i.i!0) F0x7fe782db8e90 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i25.i.i!0)
