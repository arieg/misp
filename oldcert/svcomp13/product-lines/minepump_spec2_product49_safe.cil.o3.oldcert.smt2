(declare-fun post!bb1.i.i!7 () Bool)
(declare-fun post!bb1.i.i!6 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!6 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f86ccc7f9d0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun F0x7f86ccc7f990 () Bool)
(declare-fun v0x7f86ccc7e6d0_0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun v0x7f86ccc7e590_0 () Bool)
(declare-fun v0x7f86ccc7d090_0 () Bool)
(declare-fun v0x7f86ccc7bcd0_0 () Bool)
(declare-fun v0x7f86ccc7b810_0 () Real)
(declare-fun F0x7f86ccc7fbd0 () Bool)
(declare-fun v0x7f86ccc7e810_0 () Bool)
(declare-fun v0x7f86ccc7ac50_0 () Bool)
(declare-fun E0x7f86ccc7ea10 () Bool)
(declare-fun E0x7f86ccc7e050 () Bool)
(declare-fun v0x7f86ccc7d390_0 () Real)
(declare-fun v0x7f86ccc7cc90_0 () Real)
(declare-fun E0x7f86ccc7dad0 () Bool)
(declare-fun v0x7f86ccc7a750_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun E0x7f86ccc7d6d0 () Bool)
(declare-fun v0x7f86ccc7c790_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f86ccc7c990 () Bool)
(declare-fun v0x7f86ccc7d610_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f86ccc7c8d0_0 () Bool)
(declare-fun E0x7f86ccc7ce90 () Bool)
(declare-fun F0x7f86ccc7fb10 () Bool)
(declare-fun v0x7f86ccc7be10_0 () Real)
(declare-fun E0x7f86ccc7dd90 () Bool)
(declare-fun v0x7f86ccc7cb50_0 () Bool)
(declare-fun v0x7f86ccc7d1d0_0 () Real)
(declare-fun v0x7f86ccc7c010_0 () Real)
(declare-fun v0x7f86ccc7bf50_0 () Bool)
(declare-fun v0x7f86ccc7b8d0_0 () Bool)
(declare-fun E0x7f86ccc7bad0 () Bool)
(declare-fun v0x7f86ccc7d890_0 () Bool)
(declare-fun v0x7f86ccc7da10_0 () Real)
(declare-fun v0x7f86ccc79f90_0 () Real)
(declare-fun post!bb2.i.i26.i.i!0 () Bool)
(declare-fun v0x7f86ccc79c10_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f86ccc7b150_0 () Real)
(declare-fun v0x7f86ccc7a810_0 () Bool)
(declare-fun v0x7f86ccc7ae10_0 () Real)
(declare-fun v0x7f86ccc7b090_0 () Bool)
(declare-fun v0x7f86ccc7d4d0_0 () Bool)
(declare-fun E0x7f86ccc7aa10 () Bool)
(declare-fun v0x7f86ccc7ba10_0 () Bool)
(declare-fun v0x7f86ccc79e90_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7f86ccc7a690_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f86ccc7d950_0 () Real)
(declare-fun pre!bb1.i.i!7 () Bool)
(declare-fun v0x7f86ccc7a950_0 () Bool)
(declare-fun F0x7f86ccc7fc50 () Bool)
(declare-fun v0x7f86ccc7e950_0 () Bool)
(declare-fun v0x7f86ccc7cdd0_0 () Bool)
(declare-fun v0x7f86ccc7a050_0 () Real)
(declare-fun E0x7f86ccc7c290 () Bool)
(declare-fun E0x7f86ccc7b3d0 () Bool)
(declare-fun v0x7f86ccc7af50_0 () Real)
(declare-fun v0x7f86ccc79f50_0 () Real)
(declare-fun v0x7f86ccc79d90_0 () Real)
(declare-fun v0x7f86ccc7ec10_0 () Bool)
(declare-fun E0x7f86ccc7b210 () Bool)
(declare-fun v0x7f86ccc78010_0 () Real)
(declare-fun v0x7f86ccc79e50_0 () Real)
(declare-fun E0x7f86ccc7c0d0 () Bool)
(declare-fun F0x7f86ccc7fd10 () Bool)
(declare-fun v0x7f86ccc78110_0 () Bool)
(declare-fun F0x7f86ccc7fdd0 () Bool)

(assert (=> F0x7f86ccc7fdd0
    (and v0x7f86ccc78110_0
         (<= v0x7f86ccc79e50_0 0.0)
         (>= v0x7f86ccc79e50_0 0.0)
         (<= v0x7f86ccc79f50_0 0.0)
         (>= v0x7f86ccc79f50_0 0.0)
         (<= v0x7f86ccc7a050_0 0.0)
         (>= v0x7f86ccc7a050_0 0.0)
         (<= v0x7f86ccc78010_0 1.0)
         (>= v0x7f86ccc78010_0 1.0))))
(assert (=> F0x7f86ccc7fdd0 F0x7f86ccc7fd10))
(assert (let ((a!1 (=> v0x7f86ccc7b090_0
               (or (and v0x7f86ccc7a950_0
                        E0x7f86ccc7b210
                        (<= v0x7f86ccc7b150_0 v0x7f86ccc7af50_0)
                        (>= v0x7f86ccc7b150_0 v0x7f86ccc7af50_0))
                   (and v0x7f86ccc7a690_0
                        E0x7f86ccc7b3d0
                        v0x7f86ccc7a810_0
                        (<= v0x7f86ccc7b150_0 v0x7f86ccc79f90_0)
                        (>= v0x7f86ccc7b150_0 v0x7f86ccc79f90_0)))))
      (a!2 (=> v0x7f86ccc7b090_0
               (or (and E0x7f86ccc7b210 (not E0x7f86ccc7b3d0))
                   (and E0x7f86ccc7b3d0 (not E0x7f86ccc7b210)))))
      (a!3 (=> v0x7f86ccc7bf50_0
               (or (and v0x7f86ccc7ba10_0
                        E0x7f86ccc7c0d0
                        (<= v0x7f86ccc7c010_0 v0x7f86ccc7be10_0)
                        (>= v0x7f86ccc7c010_0 v0x7f86ccc7be10_0))
                   (and v0x7f86ccc7b090_0
                        E0x7f86ccc7c290
                        v0x7f86ccc7b8d0_0
                        (<= v0x7f86ccc7c010_0 v0x7f86ccc79e90_0)
                        (>= v0x7f86ccc7c010_0 v0x7f86ccc79e90_0)))))
      (a!4 (=> v0x7f86ccc7bf50_0
               (or (and E0x7f86ccc7c0d0 (not E0x7f86ccc7c290))
                   (and E0x7f86ccc7c290 (not E0x7f86ccc7c0d0)))))
      (a!5 (or (and v0x7f86ccc7c8d0_0
                    E0x7f86ccc7dad0
                    (<= v0x7f86ccc7d950_0 v0x7f86ccc7b150_0)
                    (>= v0x7f86ccc7d950_0 v0x7f86ccc7b150_0)
                    (<= v0x7f86ccc7da10_0 v0x7f86ccc7cc90_0)
                    (>= v0x7f86ccc7da10_0 v0x7f86ccc7cc90_0))
               (and v0x7f86ccc7d610_0
                    E0x7f86ccc7dd90
                    (and (<= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0)
                         (>= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0))
                    (<= v0x7f86ccc7da10_0 v0x7f86ccc79c10_0)
                    (>= v0x7f86ccc7da10_0 v0x7f86ccc79c10_0))
               (and v0x7f86ccc7cdd0_0
                    E0x7f86ccc7e050
                    (not v0x7f86ccc7d4d0_0)
                    (and (<= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0)
                         (>= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0))
                    (<= v0x7f86ccc7da10_0 0.0)
                    (>= v0x7f86ccc7da10_0 0.0))))
      (a!6 (=> v0x7f86ccc7d890_0
               (or (and E0x7f86ccc7dad0
                        (not E0x7f86ccc7dd90)
                        (not E0x7f86ccc7e050))
                   (and E0x7f86ccc7dd90
                        (not E0x7f86ccc7dad0)
                        (not E0x7f86ccc7e050))
                   (and E0x7f86ccc7e050
                        (not E0x7f86ccc7dad0)
                        (not E0x7f86ccc7dd90)))))
      (a!7 (or (and v0x7f86ccc7e950_0
                    v0x7f86ccc7ec10_0
                    (and (<= v0x7f86ccc79e50_0 v0x7f86ccc7da10_0)
                         (>= v0x7f86ccc79e50_0 v0x7f86ccc7da10_0))
                    (<= v0x7f86ccc79f50_0 1.0)
                    (>= v0x7f86ccc79f50_0 1.0)
                    (and (<= v0x7f86ccc7a050_0 v0x7f86ccc7c010_0)
                         (>= v0x7f86ccc7a050_0 v0x7f86ccc7c010_0))
                    (and (<= v0x7f86ccc78010_0 v0x7f86ccc7d950_0)
                         (>= v0x7f86ccc78010_0 v0x7f86ccc7d950_0)))
               (and v0x7f86ccc7d890_0
                    v0x7f86ccc7e810_0
                    (and (<= v0x7f86ccc79e50_0 v0x7f86ccc7da10_0)
                         (>= v0x7f86ccc79e50_0 v0x7f86ccc7da10_0))
                    (<= v0x7f86ccc79f50_0 0.0)
                    (>= v0x7f86ccc79f50_0 0.0)
                    (and (<= v0x7f86ccc7a050_0 v0x7f86ccc7c010_0)
                         (>= v0x7f86ccc7a050_0 v0x7f86ccc7c010_0))
                    (and (<= v0x7f86ccc78010_0 v0x7f86ccc7d950_0)
                         (>= v0x7f86ccc78010_0 v0x7f86ccc7d950_0))))))
(let ((a!8 (and (=> v0x7f86ccc7a950_0
                    (and v0x7f86ccc7a690_0
                         E0x7f86ccc7aa10
                         (not v0x7f86ccc7a810_0)))
                (=> v0x7f86ccc7a950_0 E0x7f86ccc7aa10)
                a!1
                a!2
                (=> v0x7f86ccc7ba10_0
                    (and v0x7f86ccc7b090_0
                         E0x7f86ccc7bad0
                         (not v0x7f86ccc7b8d0_0)))
                (=> v0x7f86ccc7ba10_0 E0x7f86ccc7bad0)
                a!3
                a!4
                (=> v0x7f86ccc7c8d0_0
                    (and v0x7f86ccc7bf50_0 E0x7f86ccc7c990 v0x7f86ccc7c790_0))
                (=> v0x7f86ccc7c8d0_0 E0x7f86ccc7c990)
                (=> v0x7f86ccc7cdd0_0
                    (and v0x7f86ccc7bf50_0
                         E0x7f86ccc7ce90
                         (not v0x7f86ccc7c790_0)))
                (=> v0x7f86ccc7cdd0_0 E0x7f86ccc7ce90)
                (=> v0x7f86ccc7d610_0
                    (and v0x7f86ccc7cdd0_0 E0x7f86ccc7d6d0 v0x7f86ccc7d4d0_0))
                (=> v0x7f86ccc7d610_0 E0x7f86ccc7d6d0)
                (=> v0x7f86ccc7d890_0 a!5)
                a!6
                (=> v0x7f86ccc7e950_0
                    (and v0x7f86ccc7d890_0
                         E0x7f86ccc7ea10
                         (not v0x7f86ccc7e810_0)))
                (=> v0x7f86ccc7e950_0 E0x7f86ccc7ea10)
                a!7
                (= v0x7f86ccc7a810_0 (= v0x7f86ccc7a750_0 0.0))
                (= v0x7f86ccc7ac50_0 (< v0x7f86ccc79f90_0 2.0))
                (= v0x7f86ccc7ae10_0 (ite v0x7f86ccc7ac50_0 1.0 0.0))
                (= v0x7f86ccc7af50_0 (+ v0x7f86ccc7ae10_0 v0x7f86ccc79f90_0))
                (= v0x7f86ccc7b8d0_0 (= v0x7f86ccc7b810_0 0.0))
                (= v0x7f86ccc7bcd0_0 (= v0x7f86ccc79e90_0 0.0))
                (= v0x7f86ccc7be10_0 (ite v0x7f86ccc7bcd0_0 1.0 0.0))
                (= v0x7f86ccc7c790_0 (= v0x7f86ccc79c10_0 0.0))
                (= v0x7f86ccc7cb50_0 (> v0x7f86ccc7b150_0 1.0))
                (= v0x7f86ccc7cc90_0
                   (ite v0x7f86ccc7cb50_0 1.0 v0x7f86ccc79c10_0))
                (= v0x7f86ccc7d090_0 (> v0x7f86ccc7b150_0 0.0))
                (= v0x7f86ccc7d1d0_0 (+ v0x7f86ccc7b150_0 (- 1.0)))
                (= v0x7f86ccc7d390_0
                   (ite v0x7f86ccc7d090_0 v0x7f86ccc7d1d0_0 v0x7f86ccc7b150_0))
                (= v0x7f86ccc7d4d0_0 (= v0x7f86ccc7d390_0 0.0))
                (= v0x7f86ccc7e590_0 (= v0x7f86ccc7c010_0 0.0))
                (= v0x7f86ccc7e6d0_0 (= v0x7f86ccc7da10_0 0.0))
                (= v0x7f86ccc7e810_0 (or v0x7f86ccc7e6d0_0 v0x7f86ccc7e590_0))
                (= v0x7f86ccc7ec10_0 (= v0x7f86ccc79d90_0 0.0)))))
  (=> F0x7f86ccc7fc50 a!8))))
(assert (=> F0x7f86ccc7fc50 F0x7f86ccc7fbd0))
(assert (let ((a!1 (=> v0x7f86ccc7b090_0
               (or (and v0x7f86ccc7a950_0
                        E0x7f86ccc7b210
                        (<= v0x7f86ccc7b150_0 v0x7f86ccc7af50_0)
                        (>= v0x7f86ccc7b150_0 v0x7f86ccc7af50_0))
                   (and v0x7f86ccc7a690_0
                        E0x7f86ccc7b3d0
                        v0x7f86ccc7a810_0
                        (<= v0x7f86ccc7b150_0 v0x7f86ccc79f90_0)
                        (>= v0x7f86ccc7b150_0 v0x7f86ccc79f90_0)))))
      (a!2 (=> v0x7f86ccc7b090_0
               (or (and E0x7f86ccc7b210 (not E0x7f86ccc7b3d0))
                   (and E0x7f86ccc7b3d0 (not E0x7f86ccc7b210)))))
      (a!3 (=> v0x7f86ccc7bf50_0
               (or (and v0x7f86ccc7ba10_0
                        E0x7f86ccc7c0d0
                        (<= v0x7f86ccc7c010_0 v0x7f86ccc7be10_0)
                        (>= v0x7f86ccc7c010_0 v0x7f86ccc7be10_0))
                   (and v0x7f86ccc7b090_0
                        E0x7f86ccc7c290
                        v0x7f86ccc7b8d0_0
                        (<= v0x7f86ccc7c010_0 v0x7f86ccc79e90_0)
                        (>= v0x7f86ccc7c010_0 v0x7f86ccc79e90_0)))))
      (a!4 (=> v0x7f86ccc7bf50_0
               (or (and E0x7f86ccc7c0d0 (not E0x7f86ccc7c290))
                   (and E0x7f86ccc7c290 (not E0x7f86ccc7c0d0)))))
      (a!5 (or (and v0x7f86ccc7c8d0_0
                    E0x7f86ccc7dad0
                    (<= v0x7f86ccc7d950_0 v0x7f86ccc7b150_0)
                    (>= v0x7f86ccc7d950_0 v0x7f86ccc7b150_0)
                    (<= v0x7f86ccc7da10_0 v0x7f86ccc7cc90_0)
                    (>= v0x7f86ccc7da10_0 v0x7f86ccc7cc90_0))
               (and v0x7f86ccc7d610_0
                    E0x7f86ccc7dd90
                    (and (<= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0)
                         (>= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0))
                    (<= v0x7f86ccc7da10_0 v0x7f86ccc79c10_0)
                    (>= v0x7f86ccc7da10_0 v0x7f86ccc79c10_0))
               (and v0x7f86ccc7cdd0_0
                    E0x7f86ccc7e050
                    (not v0x7f86ccc7d4d0_0)
                    (and (<= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0)
                         (>= v0x7f86ccc7d950_0 v0x7f86ccc7d390_0))
                    (<= v0x7f86ccc7da10_0 0.0)
                    (>= v0x7f86ccc7da10_0 0.0))))
      (a!6 (=> v0x7f86ccc7d890_0
               (or (and E0x7f86ccc7dad0
                        (not E0x7f86ccc7dd90)
                        (not E0x7f86ccc7e050))
                   (and E0x7f86ccc7dd90
                        (not E0x7f86ccc7dad0)
                        (not E0x7f86ccc7e050))
                   (and E0x7f86ccc7e050
                        (not E0x7f86ccc7dad0)
                        (not E0x7f86ccc7dd90))))))
(let ((a!7 (and (=> v0x7f86ccc7a950_0
                    (and v0x7f86ccc7a690_0
                         E0x7f86ccc7aa10
                         (not v0x7f86ccc7a810_0)))
                (=> v0x7f86ccc7a950_0 E0x7f86ccc7aa10)
                a!1
                a!2
                (=> v0x7f86ccc7ba10_0
                    (and v0x7f86ccc7b090_0
                         E0x7f86ccc7bad0
                         (not v0x7f86ccc7b8d0_0)))
                (=> v0x7f86ccc7ba10_0 E0x7f86ccc7bad0)
                a!3
                a!4
                (=> v0x7f86ccc7c8d0_0
                    (and v0x7f86ccc7bf50_0 E0x7f86ccc7c990 v0x7f86ccc7c790_0))
                (=> v0x7f86ccc7c8d0_0 E0x7f86ccc7c990)
                (=> v0x7f86ccc7cdd0_0
                    (and v0x7f86ccc7bf50_0
                         E0x7f86ccc7ce90
                         (not v0x7f86ccc7c790_0)))
                (=> v0x7f86ccc7cdd0_0 E0x7f86ccc7ce90)
                (=> v0x7f86ccc7d610_0
                    (and v0x7f86ccc7cdd0_0 E0x7f86ccc7d6d0 v0x7f86ccc7d4d0_0))
                (=> v0x7f86ccc7d610_0 E0x7f86ccc7d6d0)
                (=> v0x7f86ccc7d890_0 a!5)
                a!6
                (=> v0x7f86ccc7e950_0
                    (and v0x7f86ccc7d890_0
                         E0x7f86ccc7ea10
                         (not v0x7f86ccc7e810_0)))
                (=> v0x7f86ccc7e950_0 E0x7f86ccc7ea10)
                v0x7f86ccc7e950_0
                (not v0x7f86ccc7ec10_0)
                (= v0x7f86ccc7a810_0 (= v0x7f86ccc7a750_0 0.0))
                (= v0x7f86ccc7ac50_0 (< v0x7f86ccc79f90_0 2.0))
                (= v0x7f86ccc7ae10_0 (ite v0x7f86ccc7ac50_0 1.0 0.0))
                (= v0x7f86ccc7af50_0 (+ v0x7f86ccc7ae10_0 v0x7f86ccc79f90_0))
                (= v0x7f86ccc7b8d0_0 (= v0x7f86ccc7b810_0 0.0))
                (= v0x7f86ccc7bcd0_0 (= v0x7f86ccc79e90_0 0.0))
                (= v0x7f86ccc7be10_0 (ite v0x7f86ccc7bcd0_0 1.0 0.0))
                (= v0x7f86ccc7c790_0 (= v0x7f86ccc79c10_0 0.0))
                (= v0x7f86ccc7cb50_0 (> v0x7f86ccc7b150_0 1.0))
                (= v0x7f86ccc7cc90_0
                   (ite v0x7f86ccc7cb50_0 1.0 v0x7f86ccc79c10_0))
                (= v0x7f86ccc7d090_0 (> v0x7f86ccc7b150_0 0.0))
                (= v0x7f86ccc7d1d0_0 (+ v0x7f86ccc7b150_0 (- 1.0)))
                (= v0x7f86ccc7d390_0
                   (ite v0x7f86ccc7d090_0 v0x7f86ccc7d1d0_0 v0x7f86ccc7b150_0))
                (= v0x7f86ccc7d4d0_0 (= v0x7f86ccc7d390_0 0.0))
                (= v0x7f86ccc7e590_0 (= v0x7f86ccc7c010_0 0.0))
                (= v0x7f86ccc7e6d0_0 (= v0x7f86ccc7da10_0 0.0))
                (= v0x7f86ccc7e810_0 (or v0x7f86ccc7e6d0_0 v0x7f86ccc7e590_0))
                (= v0x7f86ccc7ec10_0 (= v0x7f86ccc79d90_0 0.0)))))
  (=> F0x7f86ccc7fb10 a!7))))
(assert (=> F0x7f86ccc7fb10 F0x7f86ccc7fbd0))
(assert (=> F0x7f86ccc7f990 (or F0x7f86ccc7fdd0 F0x7f86ccc7fc50)))
(assert (=> F0x7f86ccc7f9d0 F0x7f86ccc7fb10))
(assert (=> pre!entry!0 (=> F0x7f86ccc7fd10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f86ccc7fbd0 (>= v0x7f86ccc79d90_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f86ccc7fbd0
        (or (>= v0x7f86ccc79f90_0 0.0) (<= v0x7f86ccc79d90_0 0.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f86ccc7fbd0 (>= v0x7f86ccc79e90_0 0.0))))
(assert (let ((a!1 (=> F0x7f86ccc7fbd0
               (or (<= v0x7f86ccc79c10_0 0.0) (not (<= v0x7f86ccc79f90_0 1.0))))))
  (=> pre!bb1.i.i!3 a!1)))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f86ccc7fbd0
        (or (>= v0x7f86ccc79c10_0 1.0) (<= v0x7f86ccc79d90_0 0.0)))))
(assert (=> pre!bb1.i.i!5 (=> F0x7f86ccc7fbd0 (not (<= v0x7f86ccc79f90_0 0.0)))))
(assert (=> pre!bb1.i.i!6 (=> F0x7f86ccc7fbd0 (>= v0x7f86ccc79c10_0 0.0))))
(assert (=> pre!bb1.i.i!7
    (=> F0x7f86ccc7fbd0
        (or (>= v0x7f86ccc79e90_0 1.0) (<= v0x7f86ccc79e90_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f86ccc7f990
                (not (or (>= v0x7f86ccc78010_0 0.0) (<= v0x7f86ccc79f50_0 0.0)))))
      (a!2 (not (or (<= v0x7f86ccc79e50_0 0.0) (not (<= v0x7f86ccc78010_0 1.0)))))
      (a!3 (and (not post!bb1.i.i!4)
                F0x7f86ccc7f990
                (not (or (>= v0x7f86ccc79e50_0 1.0) (<= v0x7f86ccc79f50_0 0.0)))))
      (a!4 (and (not post!bb1.i.i!7)
                F0x7f86ccc7f990
                (not (or (>= v0x7f86ccc7a050_0 1.0) (<= v0x7f86ccc7a050_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f86ccc7f990
           (not (>= v0x7f86ccc79f50_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f86ccc7f990
           (not (>= v0x7f86ccc7a050_0 0.0)))
      (and (not post!bb1.i.i!3) F0x7f86ccc7f990 a!2)
      a!3
      (and (not post!bb1.i.i!5) F0x7f86ccc7f990 (<= v0x7f86ccc78010_0 0.0))
      (and (not post!bb1.i.i!6)
           F0x7f86ccc7f990
           (not (>= v0x7f86ccc79e50_0 0.0)))
      a!4
      (and (not post!bb2.i.i26.i.i!0) F0x7f86ccc7f9d0 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5 pre!bb1.i.i!6 pre!bb1.i.i!7)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb1.i.i!6 post!bb1.i.i!7 post!bb2.i.i26.i.i!0)
