(declare-fun post!bb2.i.i36.i.i!0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fd21a62c050 () Bool)
(declare-fun F0x7fd21a62c090 () Bool)
(declare-fun v0x7fd21a62ac50_0 () Bool)
(declare-fun v0x7fd21a629010_0 () Bool)
(declare-fun v0x7fd21a627d90_0 () Bool)
(declare-fun v0x7fd21a6278d0_0 () Real)
(declare-fun v0x7fd21a626810_0 () Real)
(declare-fun v0x7fd21a629310_0 () Real)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7fd21a62a490 () Bool)
(declare-fun v0x7fd21a625f10_0 () Real)
(declare-fun v0x7fd21a62aed0_0 () Bool)
(declare-fun v0x7fd21a629950_0 () Real)
(declare-fun v0x7fd21a629e90_0 () Real)
(declare-fun v0x7fd21a629d10_0 () Bool)
(declare-fun v0x7fd21a629150_0 () Real)
(declare-fun E0x7fd21a629b50 () Bool)
(declare-fun E0x7fd21a629f50 () Bool)
(declare-fun F0x7fd21a62bf90 () Bool)
(declare-fun v0x7fd21a629a90_0 () Bool)
(declare-fun E0x7fd21a629650 () Bool)
(declare-fun v0x7fd21a626d10_0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun v0x7fd21a626090_0 () Real)
(declare-fun v0x7fd21a628d50_0 () Bool)
(declare-fun E0x7fd21a62a210 () Bool)
(declare-fun v0x7fd21a627ed0_0 () Real)
(declare-fun v0x7fd21a6280d0_0 () Real)
(declare-fun F0x7fd21a62bc90 () Bool)
(declare-fun E0x7fd21a628190 () Bool)
(declare-fun v0x7fd21a62b150_0 () Bool)
(declare-fun v0x7fd21a628010_0 () Bool)
(declare-fun E0x7fd21a627b90 () Bool)
(declare-fun v0x7fd21a627ad0_0 () Bool)
(declare-fun v0x7fd21a627010_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7fd21a628350 () Bool)
(declare-fun v0x7fd21a627210_0 () Real)
(declare-fun v0x7fd21a628990_0 () Bool)
(declare-fun v0x7fd21a6268d0_0 () Bool)
(declare-fun v0x7fd21a629590_0 () Bool)
(declare-fun E0x7fd21a626ad0 () Bool)
(declare-fun E0x7fd21a628e10 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7fd21a626750_0 () Bool)
(declare-fun v0x7fd21a627150_0 () Bool)
(declare-fun E0x7fd21a628a50 () Bool)
(declare-fun v0x7fd21a626ed0_0 () Real)
(declare-fun v0x7fd21a626a10_0 () Bool)
(declare-fun v0x7fd21a62ad90_0 () Bool)
(declare-fun v0x7fd21a628850_0 () Bool)
(declare-fun v0x7fd21a62b010_0 () Bool)
(declare-fun F0x7fd21a62be10 () Bool)
(declare-fun v0x7fd21a624010_0 () Real)
(declare-fun v0x7fd21a627990_0 () Bool)
(declare-fun v0x7fd21a629dd0_0 () Real)
(declare-fun E0x7fd21a62a690 () Bool)
(declare-fun v0x7fd21a626250_0 () Real)
(declare-fun v0x7fd21a628c10_0 () Bool)
(declare-fun v0x7fd21a626150_0 () Real)
(declare-fun E0x7fd21a627490 () Bool)
(declare-fun v0x7fd21a626190_0 () Real)
(declare-fun v0x7fd21a629450_0 () Bool)
(declare-fun v0x7fd21a624110_0 () Bool)
(declare-fun E0x7fd21a6272d0 () Bool)
(declare-fun v0x7fd21a629810_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun F0x7fd21a62bd50 () Bool)
(declare-fun F0x7fd21a62bed0 () Bool)

(assert (=> F0x7fd21a62bed0
    (and v0x7fd21a624110_0
         (<= v0x7fd21a626150_0 0.0)
         (>= v0x7fd21a626150_0 0.0)
         (<= v0x7fd21a626250_0 0.0)
         (>= v0x7fd21a626250_0 0.0)
         (<= v0x7fd21a624010_0 1.0)
         (>= v0x7fd21a624010_0 1.0))))
(assert (=> F0x7fd21a62bed0 F0x7fd21a62be10))
(assert (let ((a!1 (=> v0x7fd21a627150_0
               (or (and v0x7fd21a626a10_0
                        E0x7fd21a6272d0
                        (<= v0x7fd21a627210_0 v0x7fd21a627010_0)
                        (>= v0x7fd21a627210_0 v0x7fd21a627010_0))
                   (and v0x7fd21a626750_0
                        E0x7fd21a627490
                        v0x7fd21a6268d0_0
                        (<= v0x7fd21a627210_0 v0x7fd21a626190_0)
                        (>= v0x7fd21a627210_0 v0x7fd21a626190_0)))))
      (a!2 (=> v0x7fd21a627150_0
               (or (and E0x7fd21a6272d0 (not E0x7fd21a627490))
                   (and E0x7fd21a627490 (not E0x7fd21a6272d0)))))
      (a!3 (=> v0x7fd21a628010_0
               (or (and v0x7fd21a627ad0_0
                        E0x7fd21a628190
                        (<= v0x7fd21a6280d0_0 v0x7fd21a627ed0_0)
                        (>= v0x7fd21a6280d0_0 v0x7fd21a627ed0_0))
                   (and v0x7fd21a627150_0
                        E0x7fd21a628350
                        v0x7fd21a627990_0
                        (<= v0x7fd21a6280d0_0 v0x7fd21a626090_0)
                        (>= v0x7fd21a6280d0_0 v0x7fd21a626090_0)))))
      (a!4 (=> v0x7fd21a628010_0
               (or (and E0x7fd21a628190 (not E0x7fd21a628350))
                   (and E0x7fd21a628350 (not E0x7fd21a628190)))))
      (a!5 (or (and v0x7fd21a629590_0
                    E0x7fd21a629f50
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a627210_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a627210_0))
                    (<= v0x7fd21a629e90_0 v0x7fd21a629950_0)
                    (>= v0x7fd21a629e90_0 v0x7fd21a629950_0))
               (and v0x7fd21a628990_0
                    E0x7fd21a62a210
                    (not v0x7fd21a628c10_0)
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a627210_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a627210_0))
                    (and (<= v0x7fd21a629e90_0 v0x7fd21a625f10_0)
                         (>= v0x7fd21a629e90_0 v0x7fd21a625f10_0)))
               (and v0x7fd21a629a90_0
                    E0x7fd21a62a490
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a629310_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a629310_0))
                    (and (<= v0x7fd21a629e90_0 v0x7fd21a625f10_0)
                         (>= v0x7fd21a629e90_0 v0x7fd21a625f10_0)))
               (and v0x7fd21a628d50_0
                    E0x7fd21a62a690
                    (not v0x7fd21a629450_0)
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a629310_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a629310_0))
                    (<= v0x7fd21a629e90_0 0.0)
                    (>= v0x7fd21a629e90_0 0.0))))
      (a!6 (=> v0x7fd21a629d10_0
               (or (and E0x7fd21a629f50
                        (not E0x7fd21a62a210)
                        (not E0x7fd21a62a490)
                        (not E0x7fd21a62a690))
                   (and E0x7fd21a62a210
                        (not E0x7fd21a629f50)
                        (not E0x7fd21a62a490)
                        (not E0x7fd21a62a690))
                   (and E0x7fd21a62a490
                        (not E0x7fd21a629f50)
                        (not E0x7fd21a62a210)
                        (not E0x7fd21a62a690))
                   (and E0x7fd21a62a690
                        (not E0x7fd21a629f50)
                        (not E0x7fd21a62a210)
                        (not E0x7fd21a62a490))))))
(let ((a!7 (and (=> v0x7fd21a626a10_0
                    (and v0x7fd21a626750_0
                         E0x7fd21a626ad0
                         (not v0x7fd21a6268d0_0)))
                (=> v0x7fd21a626a10_0 E0x7fd21a626ad0)
                a!1
                a!2
                (=> v0x7fd21a627ad0_0
                    (and v0x7fd21a627150_0
                         E0x7fd21a627b90
                         (not v0x7fd21a627990_0)))
                (=> v0x7fd21a627ad0_0 E0x7fd21a627b90)
                a!3
                a!4
                (=> v0x7fd21a628990_0
                    (and v0x7fd21a628010_0 E0x7fd21a628a50 v0x7fd21a628850_0))
                (=> v0x7fd21a628990_0 E0x7fd21a628a50)
                (=> v0x7fd21a628d50_0
                    (and v0x7fd21a628010_0
                         E0x7fd21a628e10
                         (not v0x7fd21a628850_0)))
                (=> v0x7fd21a628d50_0 E0x7fd21a628e10)
                (=> v0x7fd21a629590_0
                    (and v0x7fd21a628990_0 E0x7fd21a629650 v0x7fd21a628c10_0))
                (=> v0x7fd21a629590_0 E0x7fd21a629650)
                (=> v0x7fd21a629a90_0
                    (and v0x7fd21a628d50_0 E0x7fd21a629b50 v0x7fd21a629450_0))
                (=> v0x7fd21a629a90_0 E0x7fd21a629b50)
                (=> v0x7fd21a629d10_0 a!5)
                a!6
                v0x7fd21a629d10_0
                (not v0x7fd21a62b150_0)
                (<= v0x7fd21a626150_0 v0x7fd21a629e90_0)
                (>= v0x7fd21a626150_0 v0x7fd21a629e90_0)
                (<= v0x7fd21a626250_0 v0x7fd21a6280d0_0)
                (>= v0x7fd21a626250_0 v0x7fd21a6280d0_0)
                (<= v0x7fd21a624010_0 v0x7fd21a629dd0_0)
                (>= v0x7fd21a624010_0 v0x7fd21a629dd0_0)
                (= v0x7fd21a6268d0_0 (= v0x7fd21a626810_0 0.0))
                (= v0x7fd21a626d10_0 (< v0x7fd21a626190_0 2.0))
                (= v0x7fd21a626ed0_0 (ite v0x7fd21a626d10_0 1.0 0.0))
                (= v0x7fd21a627010_0 (+ v0x7fd21a626ed0_0 v0x7fd21a626190_0))
                (= v0x7fd21a627990_0 (= v0x7fd21a6278d0_0 0.0))
                (= v0x7fd21a627d90_0 (= v0x7fd21a626090_0 0.0))
                (= v0x7fd21a627ed0_0 (ite v0x7fd21a627d90_0 1.0 0.0))
                (= v0x7fd21a628850_0 (= v0x7fd21a625f10_0 0.0))
                (= v0x7fd21a628c10_0 (> v0x7fd21a627210_0 1.0))
                (= v0x7fd21a629010_0 (> v0x7fd21a627210_0 0.0))
                (= v0x7fd21a629150_0 (+ v0x7fd21a627210_0 (- 1.0)))
                (= v0x7fd21a629310_0
                   (ite v0x7fd21a629010_0 v0x7fd21a629150_0 v0x7fd21a627210_0))
                (= v0x7fd21a629450_0 (= v0x7fd21a629310_0 0.0))
                (= v0x7fd21a629810_0 (= v0x7fd21a6280d0_0 0.0))
                (= v0x7fd21a629950_0
                   (ite v0x7fd21a629810_0 1.0 v0x7fd21a625f10_0))
                (= v0x7fd21a62ac50_0 (= v0x7fd21a629dd0_0 2.0))
                (= v0x7fd21a62ad90_0 (= v0x7fd21a629e90_0 0.0))
                (= v0x7fd21a62aed0_0 (or v0x7fd21a62ad90_0 v0x7fd21a62ac50_0))
                (= v0x7fd21a62b010_0 (xor v0x7fd21a62aed0_0 true))
                (= v0x7fd21a62b150_0 (and v0x7fd21a628850_0 v0x7fd21a62b010_0)))))
  (=> F0x7fd21a62bd50 a!7))))
(assert (=> F0x7fd21a62bd50 F0x7fd21a62bc90))
(assert (let ((a!1 (=> v0x7fd21a627150_0
               (or (and v0x7fd21a626a10_0
                        E0x7fd21a6272d0
                        (<= v0x7fd21a627210_0 v0x7fd21a627010_0)
                        (>= v0x7fd21a627210_0 v0x7fd21a627010_0))
                   (and v0x7fd21a626750_0
                        E0x7fd21a627490
                        v0x7fd21a6268d0_0
                        (<= v0x7fd21a627210_0 v0x7fd21a626190_0)
                        (>= v0x7fd21a627210_0 v0x7fd21a626190_0)))))
      (a!2 (=> v0x7fd21a627150_0
               (or (and E0x7fd21a6272d0 (not E0x7fd21a627490))
                   (and E0x7fd21a627490 (not E0x7fd21a6272d0)))))
      (a!3 (=> v0x7fd21a628010_0
               (or (and v0x7fd21a627ad0_0
                        E0x7fd21a628190
                        (<= v0x7fd21a6280d0_0 v0x7fd21a627ed0_0)
                        (>= v0x7fd21a6280d0_0 v0x7fd21a627ed0_0))
                   (and v0x7fd21a627150_0
                        E0x7fd21a628350
                        v0x7fd21a627990_0
                        (<= v0x7fd21a6280d0_0 v0x7fd21a626090_0)
                        (>= v0x7fd21a6280d0_0 v0x7fd21a626090_0)))))
      (a!4 (=> v0x7fd21a628010_0
               (or (and E0x7fd21a628190 (not E0x7fd21a628350))
                   (and E0x7fd21a628350 (not E0x7fd21a628190)))))
      (a!5 (or (and v0x7fd21a629590_0
                    E0x7fd21a629f50
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a627210_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a627210_0))
                    (<= v0x7fd21a629e90_0 v0x7fd21a629950_0)
                    (>= v0x7fd21a629e90_0 v0x7fd21a629950_0))
               (and v0x7fd21a628990_0
                    E0x7fd21a62a210
                    (not v0x7fd21a628c10_0)
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a627210_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a627210_0))
                    (and (<= v0x7fd21a629e90_0 v0x7fd21a625f10_0)
                         (>= v0x7fd21a629e90_0 v0x7fd21a625f10_0)))
               (and v0x7fd21a629a90_0
                    E0x7fd21a62a490
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a629310_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a629310_0))
                    (and (<= v0x7fd21a629e90_0 v0x7fd21a625f10_0)
                         (>= v0x7fd21a629e90_0 v0x7fd21a625f10_0)))
               (and v0x7fd21a628d50_0
                    E0x7fd21a62a690
                    (not v0x7fd21a629450_0)
                    (and (<= v0x7fd21a629dd0_0 v0x7fd21a629310_0)
                         (>= v0x7fd21a629dd0_0 v0x7fd21a629310_0))
                    (<= v0x7fd21a629e90_0 0.0)
                    (>= v0x7fd21a629e90_0 0.0))))
      (a!6 (=> v0x7fd21a629d10_0
               (or (and E0x7fd21a629f50
                        (not E0x7fd21a62a210)
                        (not E0x7fd21a62a490)
                        (not E0x7fd21a62a690))
                   (and E0x7fd21a62a210
                        (not E0x7fd21a629f50)
                        (not E0x7fd21a62a490)
                        (not E0x7fd21a62a690))
                   (and E0x7fd21a62a490
                        (not E0x7fd21a629f50)
                        (not E0x7fd21a62a210)
                        (not E0x7fd21a62a690))
                   (and E0x7fd21a62a690
                        (not E0x7fd21a629f50)
                        (not E0x7fd21a62a210)
                        (not E0x7fd21a62a490))))))
(let ((a!7 (and (=> v0x7fd21a626a10_0
                    (and v0x7fd21a626750_0
                         E0x7fd21a626ad0
                         (not v0x7fd21a6268d0_0)))
                (=> v0x7fd21a626a10_0 E0x7fd21a626ad0)
                a!1
                a!2
                (=> v0x7fd21a627ad0_0
                    (and v0x7fd21a627150_0
                         E0x7fd21a627b90
                         (not v0x7fd21a627990_0)))
                (=> v0x7fd21a627ad0_0 E0x7fd21a627b90)
                a!3
                a!4
                (=> v0x7fd21a628990_0
                    (and v0x7fd21a628010_0 E0x7fd21a628a50 v0x7fd21a628850_0))
                (=> v0x7fd21a628990_0 E0x7fd21a628a50)
                (=> v0x7fd21a628d50_0
                    (and v0x7fd21a628010_0
                         E0x7fd21a628e10
                         (not v0x7fd21a628850_0)))
                (=> v0x7fd21a628d50_0 E0x7fd21a628e10)
                (=> v0x7fd21a629590_0
                    (and v0x7fd21a628990_0 E0x7fd21a629650 v0x7fd21a628c10_0))
                (=> v0x7fd21a629590_0 E0x7fd21a629650)
                (=> v0x7fd21a629a90_0
                    (and v0x7fd21a628d50_0 E0x7fd21a629b50 v0x7fd21a629450_0))
                (=> v0x7fd21a629a90_0 E0x7fd21a629b50)
                (=> v0x7fd21a629d10_0 a!5)
                a!6
                v0x7fd21a629d10_0
                v0x7fd21a62b150_0
                (= v0x7fd21a6268d0_0 (= v0x7fd21a626810_0 0.0))
                (= v0x7fd21a626d10_0 (< v0x7fd21a626190_0 2.0))
                (= v0x7fd21a626ed0_0 (ite v0x7fd21a626d10_0 1.0 0.0))
                (= v0x7fd21a627010_0 (+ v0x7fd21a626ed0_0 v0x7fd21a626190_0))
                (= v0x7fd21a627990_0 (= v0x7fd21a6278d0_0 0.0))
                (= v0x7fd21a627d90_0 (= v0x7fd21a626090_0 0.0))
                (= v0x7fd21a627ed0_0 (ite v0x7fd21a627d90_0 1.0 0.0))
                (= v0x7fd21a628850_0 (= v0x7fd21a625f10_0 0.0))
                (= v0x7fd21a628c10_0 (> v0x7fd21a627210_0 1.0))
                (= v0x7fd21a629010_0 (> v0x7fd21a627210_0 0.0))
                (= v0x7fd21a629150_0 (+ v0x7fd21a627210_0 (- 1.0)))
                (= v0x7fd21a629310_0
                   (ite v0x7fd21a629010_0 v0x7fd21a629150_0 v0x7fd21a627210_0))
                (= v0x7fd21a629450_0 (= v0x7fd21a629310_0 0.0))
                (= v0x7fd21a629810_0 (= v0x7fd21a6280d0_0 0.0))
                (= v0x7fd21a629950_0
                   (ite v0x7fd21a629810_0 1.0 v0x7fd21a625f10_0))
                (= v0x7fd21a62ac50_0 (= v0x7fd21a629dd0_0 2.0))
                (= v0x7fd21a62ad90_0 (= v0x7fd21a629e90_0 0.0))
                (= v0x7fd21a62aed0_0 (or v0x7fd21a62ad90_0 v0x7fd21a62ac50_0))
                (= v0x7fd21a62b010_0 (xor v0x7fd21a62aed0_0 true))
                (= v0x7fd21a62b150_0 (and v0x7fd21a628850_0 v0x7fd21a62b010_0)))))
  (=> F0x7fd21a62bf90 a!7))))
(assert (=> F0x7fd21a62bf90 F0x7fd21a62bc90))
(assert (=> F0x7fd21a62c090 (or F0x7fd21a62bed0 F0x7fd21a62bd50)))
(assert (=> F0x7fd21a62c050 F0x7fd21a62bf90))
(assert (=> pre!entry!0 (=> F0x7fd21a62be10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd21a62bc90 (>= v0x7fd21a626090_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fd21a62bc90
        (or (<= v0x7fd21a626190_0 1.0) (>= v0x7fd21a626190_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fd21a62bc90 (>= v0x7fd21a626190_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fd21a62bc90
        (or (>= v0x7fd21a626190_0 1.0) (<= v0x7fd21a626190_0 0.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7fd21a62bc90 (<= v0x7fd21a626190_0 2.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7fd21a62c090
                (not (or (<= v0x7fd21a624010_0 1.0) (>= v0x7fd21a624010_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7fd21a62c090
                (not (or (>= v0x7fd21a624010_0 1.0) (<= v0x7fd21a624010_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7fd21a62c090
           (not (>= v0x7fd21a626250_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7fd21a62c090
           (not (>= v0x7fd21a624010_0 0.0)))
      a!2
      (and (not post!bb1.i.i!4)
           F0x7fd21a62c090
           (not (<= v0x7fd21a624010_0 2.0)))
      (and (not post!bb2.i.i36.i.i!0) F0x7fd21a62c050 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i36.i.i!0)
