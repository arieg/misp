(declare-fun post!bb1.i.i35.i.i!0 () Bool)
(declare-fun F0x7f7ec7654990 () Bool)
(declare-fun F0x7f7ec7654850 () Bool)
(declare-fun F0x7f7ec7654790 () Bool)
(declare-fun v0x7f7ec76537d0_0 () Bool)
(declare-fun v0x7f7ec7652190_0 () Real)
(declare-fun v0x7f7ec7651b10_0 () Bool)
(declare-fun v0x7f7ec7650c90_0 () Bool)
(declare-fun v0x7f7ec764fc10_0 () Bool)
(declare-fun F0x7f7ec7654950 () Bool)
(declare-fun E0x7f7ec7653290 () Bool)
(declare-fun v0x7f7ec764f090_0 () Real)
(declare-fun E0x7f7ec7652fd0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f7ec7651c50_0 () Real)
(declare-fun v0x7f7ec7652c50_0 () Real)
(declare-fun v0x7f7ec764fdd0_0 () Real)
(declare-fun v0x7f7ec7652050_0 () Bool)
(declare-fun v0x7f7ec7652b90_0 () Real)
(declare-fun E0x7f7ec7652d10 () Bool)
(declare-fun v0x7f7ec7652710_0 () Bool)
(declare-fun v0x7f7ec7653a50_0 () Bool)
(declare-fun E0x7f7ec7652910 () Bool)
(declare-fun v0x7f7ec7651d90_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f7ec7651250 () Bool)
(declare-fun v0x7f7ec7650fd0_0 () Real)
(declare-fun v0x7f7ec7652ad0_0 () Bool)
(declare-fun E0x7f7ec7651090 () Bool)
(declare-fun v0x7f7ec7650890_0 () Bool)
(declare-fun v0x7f7ec7653910_0 () Bool)
(declare-fun v0x7f7ec76509d0_0 () Bool)
(declare-fun E0x7f7ec7651950 () Bool)
(declare-fun v0x7f7ec764ee10_0 () Real)
(declare-fun v0x7f7ec764ef90_0 () Real)
(declare-fun v0x7f7ec764ff10_0 () Real)
(declare-fun v0x7f7ec7651750_0 () Bool)
(declare-fun E0x7f7ec7650390 () Bool)
(declare-fun v0x7f7ec7650110_0 () Real)
(declare-fun v0x7f7ec7650dd0_0 () Real)
(declare-fun v0x7f7ec7651890_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f7ec76501d0 () Bool)
(declare-fun v0x7f7ec764f7d0_0 () Bool)
(declare-fun v0x7f7ec764f910_0 () Bool)
(declare-fun F0x7f7ec76546d0 () Bool)
(declare-fun v0x7f7ec76525d0_0 () Bool)
(declare-fun F0x7f7ec76545d0 () Bool)
(declare-fun E0x7f7ec7650a90 () Bool)
(declare-fun v0x7f7ec764f650_0 () Bool)
(declare-fun v0x7f7ec7652350_0 () Real)
(declare-fun v0x7f7ec764c010_0 () Real)
(declare-fun v0x7f7ec764f150_0 () Real)
(declare-fun v0x7f7ec7650f10_0 () Bool)
(declare-fun v0x7f7ec7652490_0 () Bool)
(declare-fun v0x7f7ec7652850_0 () Bool)
(declare-fun E0x7f7ec7651e50 () Bool)
(declare-fun v0x7f7ec764f050_0 () Real)
(declare-fun v0x7f7ec764c110_0 () Bool)
(declare-fun v0x7f7ec764f710_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f7ec7650050_0 () Bool)
(declare-fun v0x7f7ec76507d0_0 () Real)
(declare-fun F0x7f7ec7654690 () Bool)
(declare-fun E0x7f7ec764f9d0 () Bool)

(assert (=> F0x7f7ec7654690
    (and v0x7f7ec764c110_0
         (<= v0x7f7ec764f050_0 0.0)
         (>= v0x7f7ec764f050_0 0.0)
         (<= v0x7f7ec764f150_0 1.0)
         (>= v0x7f7ec764f150_0 1.0)
         (<= v0x7f7ec764c010_0 0.0)
         (>= v0x7f7ec764c010_0 0.0))))
(assert (=> F0x7f7ec7654690 F0x7f7ec76545d0))
(assert (let ((a!1 (=> v0x7f7ec7650050_0
               (or (and v0x7f7ec764f910_0
                        E0x7f7ec76501d0
                        (<= v0x7f7ec7650110_0 v0x7f7ec764ff10_0)
                        (>= v0x7f7ec7650110_0 v0x7f7ec764ff10_0))
                   (and v0x7f7ec764f650_0
                        E0x7f7ec7650390
                        v0x7f7ec764f7d0_0
                        (<= v0x7f7ec7650110_0 v0x7f7ec764ef90_0)
                        (>= v0x7f7ec7650110_0 v0x7f7ec764ef90_0)))))
      (a!2 (=> v0x7f7ec7650050_0
               (or (and E0x7f7ec76501d0 (not E0x7f7ec7650390))
                   (and E0x7f7ec7650390 (not E0x7f7ec76501d0)))))
      (a!3 (=> v0x7f7ec7650f10_0
               (or (and v0x7f7ec76509d0_0
                        E0x7f7ec7651090
                        (<= v0x7f7ec7650fd0_0 v0x7f7ec7650dd0_0)
                        (>= v0x7f7ec7650fd0_0 v0x7f7ec7650dd0_0))
                   (and v0x7f7ec7650050_0
                        E0x7f7ec7651250
                        v0x7f7ec7650890_0
                        (<= v0x7f7ec7650fd0_0 v0x7f7ec764ee10_0)
                        (>= v0x7f7ec7650fd0_0 v0x7f7ec764ee10_0)))))
      (a!4 (=> v0x7f7ec7650f10_0
               (or (and E0x7f7ec7651090 (not E0x7f7ec7651250))
                   (and E0x7f7ec7651250 (not E0x7f7ec7651090)))))
      (a!5 (or (and v0x7f7ec7651890_0
                    E0x7f7ec7652d10
                    (<= v0x7f7ec7652b90_0 v0x7f7ec7650110_0)
                    (>= v0x7f7ec7652b90_0 v0x7f7ec7650110_0)
                    (<= v0x7f7ec7652c50_0 v0x7f7ec7651c50_0)
                    (>= v0x7f7ec7652c50_0 v0x7f7ec7651c50_0))
               (and v0x7f7ec7652850_0
                    E0x7f7ec7652fd0
                    (and (<= v0x7f7ec7652b90_0 v0x7f7ec7652350_0)
                         (>= v0x7f7ec7652b90_0 v0x7f7ec7652350_0))
                    (<= v0x7f7ec7652c50_0 v0x7f7ec764f090_0)
                    (>= v0x7f7ec7652c50_0 v0x7f7ec764f090_0))
               (and v0x7f7ec7651d90_0
                    E0x7f7ec7653290
                    (not v0x7f7ec7652710_0)
                    (and (<= v0x7f7ec7652b90_0 v0x7f7ec7652350_0)
                         (>= v0x7f7ec7652b90_0 v0x7f7ec7652350_0))
                    (<= v0x7f7ec7652c50_0 0.0)
                    (>= v0x7f7ec7652c50_0 0.0))))
      (a!6 (=> v0x7f7ec7652ad0_0
               (or (and E0x7f7ec7652d10
                        (not E0x7f7ec7652fd0)
                        (not E0x7f7ec7653290))
                   (and E0x7f7ec7652fd0
                        (not E0x7f7ec7652d10)
                        (not E0x7f7ec7653290))
                   (and E0x7f7ec7653290
                        (not E0x7f7ec7652d10)
                        (not E0x7f7ec7652fd0))))))
(let ((a!7 (and (=> v0x7f7ec764f910_0
                    (and v0x7f7ec764f650_0
                         E0x7f7ec764f9d0
                         (not v0x7f7ec764f7d0_0)))
                (=> v0x7f7ec764f910_0 E0x7f7ec764f9d0)
                a!1
                a!2
                (=> v0x7f7ec76509d0_0
                    (and v0x7f7ec7650050_0
                         E0x7f7ec7650a90
                         (not v0x7f7ec7650890_0)))
                (=> v0x7f7ec76509d0_0 E0x7f7ec7650a90)
                a!3
                a!4
                (=> v0x7f7ec7651890_0
                    (and v0x7f7ec7650f10_0 E0x7f7ec7651950 v0x7f7ec7651750_0))
                (=> v0x7f7ec7651890_0 E0x7f7ec7651950)
                (=> v0x7f7ec7651d90_0
                    (and v0x7f7ec7650f10_0
                         E0x7f7ec7651e50
                         (not v0x7f7ec7651750_0)))
                (=> v0x7f7ec7651d90_0 E0x7f7ec7651e50)
                (=> v0x7f7ec7652850_0
                    (and v0x7f7ec7651d90_0 E0x7f7ec7652910 v0x7f7ec7652710_0))
                (=> v0x7f7ec7652850_0 E0x7f7ec7652910)
                (=> v0x7f7ec7652ad0_0 a!5)
                a!6
                v0x7f7ec7652ad0_0
                v0x7f7ec7653a50_0
                (<= v0x7f7ec764f050_0 v0x7f7ec7650fd0_0)
                (>= v0x7f7ec764f050_0 v0x7f7ec7650fd0_0)
                (<= v0x7f7ec764f150_0 v0x7f7ec7652b90_0)
                (>= v0x7f7ec764f150_0 v0x7f7ec7652b90_0)
                (<= v0x7f7ec764c010_0 v0x7f7ec7652c50_0)
                (>= v0x7f7ec764c010_0 v0x7f7ec7652c50_0)
                (= v0x7f7ec764f7d0_0 (= v0x7f7ec764f710_0 0.0))
                (= v0x7f7ec764fc10_0 (< v0x7f7ec764ef90_0 2.0))
                (= v0x7f7ec764fdd0_0 (ite v0x7f7ec764fc10_0 1.0 0.0))
                (= v0x7f7ec764ff10_0 (+ v0x7f7ec764fdd0_0 v0x7f7ec764ef90_0))
                (= v0x7f7ec7650890_0 (= v0x7f7ec76507d0_0 0.0))
                (= v0x7f7ec7650c90_0 (= v0x7f7ec764ee10_0 0.0))
                (= v0x7f7ec7650dd0_0 (ite v0x7f7ec7650c90_0 1.0 0.0))
                (= v0x7f7ec7651750_0 (= v0x7f7ec764f090_0 0.0))
                (= v0x7f7ec7651b10_0 (> v0x7f7ec7650110_0 1.0))
                (= v0x7f7ec7651c50_0
                   (ite v0x7f7ec7651b10_0 1.0 v0x7f7ec764f090_0))
                (= v0x7f7ec7652050_0 (> v0x7f7ec7650110_0 0.0))
                (= v0x7f7ec7652190_0 (+ v0x7f7ec7650110_0 (- 1.0)))
                (= v0x7f7ec7652350_0
                   (ite v0x7f7ec7652050_0 v0x7f7ec7652190_0 v0x7f7ec7650110_0))
                (= v0x7f7ec7652490_0 (= v0x7f7ec7650fd0_0 0.0))
                (= v0x7f7ec76525d0_0 (= v0x7f7ec7652350_0 0.0))
                (= v0x7f7ec7652710_0 (and v0x7f7ec7652490_0 v0x7f7ec76525d0_0))
                (= v0x7f7ec76537d0_0 (not (= v0x7f7ec7652b90_0 0.0)))
                (= v0x7f7ec7653910_0 (= v0x7f7ec7652c50_0 0.0))
                (= v0x7f7ec7653a50_0 (or v0x7f7ec7653910_0 v0x7f7ec76537d0_0)))))
  (=> F0x7f7ec76546d0 a!7))))
(assert (=> F0x7f7ec76546d0 F0x7f7ec7654790))
(assert (let ((a!1 (=> v0x7f7ec7650050_0
               (or (and v0x7f7ec764f910_0
                        E0x7f7ec76501d0
                        (<= v0x7f7ec7650110_0 v0x7f7ec764ff10_0)
                        (>= v0x7f7ec7650110_0 v0x7f7ec764ff10_0))
                   (and v0x7f7ec764f650_0
                        E0x7f7ec7650390
                        v0x7f7ec764f7d0_0
                        (<= v0x7f7ec7650110_0 v0x7f7ec764ef90_0)
                        (>= v0x7f7ec7650110_0 v0x7f7ec764ef90_0)))))
      (a!2 (=> v0x7f7ec7650050_0
               (or (and E0x7f7ec76501d0 (not E0x7f7ec7650390))
                   (and E0x7f7ec7650390 (not E0x7f7ec76501d0)))))
      (a!3 (=> v0x7f7ec7650f10_0
               (or (and v0x7f7ec76509d0_0
                        E0x7f7ec7651090
                        (<= v0x7f7ec7650fd0_0 v0x7f7ec7650dd0_0)
                        (>= v0x7f7ec7650fd0_0 v0x7f7ec7650dd0_0))
                   (and v0x7f7ec7650050_0
                        E0x7f7ec7651250
                        v0x7f7ec7650890_0
                        (<= v0x7f7ec7650fd0_0 v0x7f7ec764ee10_0)
                        (>= v0x7f7ec7650fd0_0 v0x7f7ec764ee10_0)))))
      (a!4 (=> v0x7f7ec7650f10_0
               (or (and E0x7f7ec7651090 (not E0x7f7ec7651250))
                   (and E0x7f7ec7651250 (not E0x7f7ec7651090)))))
      (a!5 (or (and v0x7f7ec7651890_0
                    E0x7f7ec7652d10
                    (<= v0x7f7ec7652b90_0 v0x7f7ec7650110_0)
                    (>= v0x7f7ec7652b90_0 v0x7f7ec7650110_0)
                    (<= v0x7f7ec7652c50_0 v0x7f7ec7651c50_0)
                    (>= v0x7f7ec7652c50_0 v0x7f7ec7651c50_0))
               (and v0x7f7ec7652850_0
                    E0x7f7ec7652fd0
                    (and (<= v0x7f7ec7652b90_0 v0x7f7ec7652350_0)
                         (>= v0x7f7ec7652b90_0 v0x7f7ec7652350_0))
                    (<= v0x7f7ec7652c50_0 v0x7f7ec764f090_0)
                    (>= v0x7f7ec7652c50_0 v0x7f7ec764f090_0))
               (and v0x7f7ec7651d90_0
                    E0x7f7ec7653290
                    (not v0x7f7ec7652710_0)
                    (and (<= v0x7f7ec7652b90_0 v0x7f7ec7652350_0)
                         (>= v0x7f7ec7652b90_0 v0x7f7ec7652350_0))
                    (<= v0x7f7ec7652c50_0 0.0)
                    (>= v0x7f7ec7652c50_0 0.0))))
      (a!6 (=> v0x7f7ec7652ad0_0
               (or (and E0x7f7ec7652d10
                        (not E0x7f7ec7652fd0)
                        (not E0x7f7ec7653290))
                   (and E0x7f7ec7652fd0
                        (not E0x7f7ec7652d10)
                        (not E0x7f7ec7653290))
                   (and E0x7f7ec7653290
                        (not E0x7f7ec7652d10)
                        (not E0x7f7ec7652fd0))))))
(let ((a!7 (and (=> v0x7f7ec764f910_0
                    (and v0x7f7ec764f650_0
                         E0x7f7ec764f9d0
                         (not v0x7f7ec764f7d0_0)))
                (=> v0x7f7ec764f910_0 E0x7f7ec764f9d0)
                a!1
                a!2
                (=> v0x7f7ec76509d0_0
                    (and v0x7f7ec7650050_0
                         E0x7f7ec7650a90
                         (not v0x7f7ec7650890_0)))
                (=> v0x7f7ec76509d0_0 E0x7f7ec7650a90)
                a!3
                a!4
                (=> v0x7f7ec7651890_0
                    (and v0x7f7ec7650f10_0 E0x7f7ec7651950 v0x7f7ec7651750_0))
                (=> v0x7f7ec7651890_0 E0x7f7ec7651950)
                (=> v0x7f7ec7651d90_0
                    (and v0x7f7ec7650f10_0
                         E0x7f7ec7651e50
                         (not v0x7f7ec7651750_0)))
                (=> v0x7f7ec7651d90_0 E0x7f7ec7651e50)
                (=> v0x7f7ec7652850_0
                    (and v0x7f7ec7651d90_0 E0x7f7ec7652910 v0x7f7ec7652710_0))
                (=> v0x7f7ec7652850_0 E0x7f7ec7652910)
                (=> v0x7f7ec7652ad0_0 a!5)
                a!6
                v0x7f7ec7652ad0_0
                (not v0x7f7ec7653a50_0)
                (= v0x7f7ec764f7d0_0 (= v0x7f7ec764f710_0 0.0))
                (= v0x7f7ec764fc10_0 (< v0x7f7ec764ef90_0 2.0))
                (= v0x7f7ec764fdd0_0 (ite v0x7f7ec764fc10_0 1.0 0.0))
                (= v0x7f7ec764ff10_0 (+ v0x7f7ec764fdd0_0 v0x7f7ec764ef90_0))
                (= v0x7f7ec7650890_0 (= v0x7f7ec76507d0_0 0.0))
                (= v0x7f7ec7650c90_0 (= v0x7f7ec764ee10_0 0.0))
                (= v0x7f7ec7650dd0_0 (ite v0x7f7ec7650c90_0 1.0 0.0))
                (= v0x7f7ec7651750_0 (= v0x7f7ec764f090_0 0.0))
                (= v0x7f7ec7651b10_0 (> v0x7f7ec7650110_0 1.0))
                (= v0x7f7ec7651c50_0
                   (ite v0x7f7ec7651b10_0 1.0 v0x7f7ec764f090_0))
                (= v0x7f7ec7652050_0 (> v0x7f7ec7650110_0 0.0))
                (= v0x7f7ec7652190_0 (+ v0x7f7ec7650110_0 (- 1.0)))
                (= v0x7f7ec7652350_0
                   (ite v0x7f7ec7652050_0 v0x7f7ec7652190_0 v0x7f7ec7650110_0))
                (= v0x7f7ec7652490_0 (= v0x7f7ec7650fd0_0 0.0))
                (= v0x7f7ec76525d0_0 (= v0x7f7ec7652350_0 0.0))
                (= v0x7f7ec7652710_0 (and v0x7f7ec7652490_0 v0x7f7ec76525d0_0))
                (= v0x7f7ec76537d0_0 (not (= v0x7f7ec7652b90_0 0.0)))
                (= v0x7f7ec7653910_0 (= v0x7f7ec7652c50_0 0.0))
                (= v0x7f7ec7653a50_0 (or v0x7f7ec7653910_0 v0x7f7ec76537d0_0)))))
  (=> F0x7f7ec7654850 a!7))))
(assert (=> F0x7f7ec7654850 F0x7f7ec7654790))
(assert (=> F0x7f7ec7654990 (or F0x7f7ec7654690 F0x7f7ec76546d0)))
(assert (=> F0x7f7ec7654950 F0x7f7ec7654850))
(assert (=> pre!entry!0 (=> F0x7f7ec76545d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f7ec7654790 (>= v0x7f7ec764f090_0 0.0))))
(assert (let ((a!1 (=> F0x7f7ec7654790
               (or (<= v0x7f7ec764f090_0 0.0) (not (<= v0x7f7ec764ef90_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7f7ec764c010_0 0.0) (not (<= v0x7f7ec764f150_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f7ec7654990
           (not (>= v0x7f7ec764c010_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f7ec7654990 a!1)
      (and (not post!bb1.i.i35.i.i!0) F0x7f7ec7654950 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i35.i.i!0)
