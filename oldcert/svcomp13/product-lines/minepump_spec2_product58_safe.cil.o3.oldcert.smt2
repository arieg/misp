(declare-fun post!bb2.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun F0x7ff445400650 () Bool)
(declare-fun F0x7ff445400690 () Bool)
(declare-fun v0x7ff4453fef90_0 () Bool)
(declare-fun v0x7ff4453fee90_0 () Bool)
(declare-fun v0x7ff4453fb110_0 () Real)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7ff4453faa50_0 () Real)
(declare-fun v0x7ff4453ff0d0_0 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun E0x7ff4453fe8d0 () Bool)
(declare-fun v0x7ff4453fd550_0 () Real)
(declare-fun F0x7ff445400410 () Bool)
(declare-fun v0x7ff4453f9f10_0 () Real)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun E0x7ff4453fe450 () Bool)
(declare-fun v0x7ff4453fe0d0_0 () Real)
(declare-fun F0x7ff445400310 () Bool)
(declare-fun v0x7ff4453fe010_0 () Real)
(declare-fun E0x7ff4453fe190 () Bool)
(declare-fun v0x7ff4453fdf50_0 () Bool)
(declare-fun E0x7ff4453ff2d0 () Bool)
(declare-fun v0x7ff4453fd690_0 () Bool)
(declare-fun v0x7ff4453fdcd0_0 () Bool)
(declare-fun v0x7ff4453fce50_0 () Bool)
(declare-fun v0x7ff4453faf50_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7ff4453fdb90_0 () Real)
(declare-fun E0x7ff4453fd890 () Bool)
(declare-fun v0x7ff4453fda50_0 () Bool)
(declare-fun v0x7ff4453fd7d0_0 () Bool)
(declare-fun v0x7ff4453fbb10_0 () Real)
(declare-fun E0x7ff4453fd050 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun v0x7ff4453fcf90_0 () Bool)
(declare-fun v0x7ff4453fa190_0 () Real)
(declare-fun v0x7ff4453ff4d0_0 () Bool)
(declare-fun v0x7ff4453fc250_0 () Bool)
(declare-fun E0x7ff4453fc590 () Bool)
(declare-fun E0x7ff4453fe6d0 () Bool)
(declare-fun E0x7ff4453fdd90 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7ff4453fd250_0 () Bool)
(declare-fun E0x7ff4453fbdd0 () Bool)
(declare-fun v0x7ff4453fc110_0 () Real)
(declare-fun v0x7ff4453fbd10_0 () Bool)
(declare-fun v0x7ff4453fa090_0 () Real)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7ff4453fb6d0 () Bool)
(declare-fun E0x7ff4453fcc90 () Bool)
(declare-fun v0x7ff4453fb250_0 () Real)
(declare-fun v0x7ff4453fb390_0 () Bool)
(declare-fun E0x7ff4453fb510 () Bool)
(declare-fun E0x7ff4453fad10 () Bool)
(declare-fun E0x7ff4453fc3d0 () Bool)
(declare-fun F0x7ff445400490 () Bool)
(declare-fun v0x7ff4453fca90_0 () Bool)
(declare-fun v0x7ff4453ff210_0 () Bool)
(declare-fun v0x7ff4453fa990_0 () Bool)
(declare-fun v0x7ff4453fc310_0 () Real)
(declare-fun v0x7ff4453fcbd0_0 () Bool)
(declare-fun v0x7ff4453fa290_0 () Real)
(declare-fun v0x7ff4453f8010_0 () Real)
(declare-fun v0x7ff4453fab10_0 () Bool)
(declare-fun v0x7ff4453fa350_0 () Real)
(declare-fun v0x7ff4453fa250_0 () Real)
(declare-fun v0x7ff4453fac50_0 () Bool)
(declare-fun v0x7ff4453fbfd0_0 () Bool)
(declare-fun v0x7ff4453fbbd0_0 () Bool)
(declare-fun v0x7ff4453fd390_0 () Real)
(declare-fun v0x7ff4453fa150_0 () Real)
(declare-fun v0x7ff4453fb450_0 () Real)
(declare-fun v0x7ff4453f8110_0 () Bool)
(declare-fun F0x7ff445400550 () Bool)
(declare-fun F0x7ff4454005d0 () Bool)

(assert (=> F0x7ff4454005d0
    (and v0x7ff4453f8110_0
         (<= v0x7ff4453fa150_0 0.0)
         (>= v0x7ff4453fa150_0 0.0)
         (<= v0x7ff4453fa250_0 0.0)
         (>= v0x7ff4453fa250_0 0.0)
         (<= v0x7ff4453fa350_0 0.0)
         (>= v0x7ff4453fa350_0 0.0)
         (<= v0x7ff4453f8010_0 1.0)
         (>= v0x7ff4453f8010_0 1.0))))
(assert (=> F0x7ff4454005d0 F0x7ff445400550))
(assert (let ((a!1 (=> v0x7ff4453fb390_0
               (or (and v0x7ff4453fac50_0
                        E0x7ff4453fb510
                        (<= v0x7ff4453fb450_0 v0x7ff4453fb250_0)
                        (>= v0x7ff4453fb450_0 v0x7ff4453fb250_0))
                   (and v0x7ff4453fa990_0
                        E0x7ff4453fb6d0
                        v0x7ff4453fab10_0
                        (<= v0x7ff4453fb450_0 v0x7ff4453fa290_0)
                        (>= v0x7ff4453fb450_0 v0x7ff4453fa290_0)))))
      (a!2 (=> v0x7ff4453fb390_0
               (or (and E0x7ff4453fb510 (not E0x7ff4453fb6d0))
                   (and E0x7ff4453fb6d0 (not E0x7ff4453fb510)))))
      (a!3 (=> v0x7ff4453fc250_0
               (or (and v0x7ff4453fbd10_0
                        E0x7ff4453fc3d0
                        (<= v0x7ff4453fc310_0 v0x7ff4453fc110_0)
                        (>= v0x7ff4453fc310_0 v0x7ff4453fc110_0))
                   (and v0x7ff4453fb390_0
                        E0x7ff4453fc590
                        v0x7ff4453fbbd0_0
                        (<= v0x7ff4453fc310_0 v0x7ff4453fa190_0)
                        (>= v0x7ff4453fc310_0 v0x7ff4453fa190_0)))))
      (a!4 (=> v0x7ff4453fc250_0
               (or (and E0x7ff4453fc3d0 (not E0x7ff4453fc590))
                   (and E0x7ff4453fc590 (not E0x7ff4453fc3d0)))))
      (a!5 (or (and v0x7ff4453fd7d0_0
                    E0x7ff4453fe190
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fb450_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fb450_0))
                    (<= v0x7ff4453fe0d0_0 v0x7ff4453fdb90_0)
                    (>= v0x7ff4453fe0d0_0 v0x7ff4453fdb90_0))
               (and v0x7ff4453fcbd0_0
                    E0x7ff4453fe450
                    (not v0x7ff4453fce50_0)
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fb450_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fb450_0))
                    (and (<= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)
                         (>= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)))
               (and v0x7ff4453fdcd0_0
                    E0x7ff4453fe6d0
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fd550_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fd550_0))
                    (and (<= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)
                         (>= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)))
               (and v0x7ff4453fcf90_0
                    E0x7ff4453fe8d0
                    (not v0x7ff4453fd690_0)
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fd550_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fd550_0))
                    (<= v0x7ff4453fe0d0_0 0.0)
                    (>= v0x7ff4453fe0d0_0 0.0))))
      (a!6 (=> v0x7ff4453fdf50_0
               (or (and E0x7ff4453fe190
                        (not E0x7ff4453fe450)
                        (not E0x7ff4453fe6d0)
                        (not E0x7ff4453fe8d0))
                   (and E0x7ff4453fe450
                        (not E0x7ff4453fe190)
                        (not E0x7ff4453fe6d0)
                        (not E0x7ff4453fe8d0))
                   (and E0x7ff4453fe6d0
                        (not E0x7ff4453fe190)
                        (not E0x7ff4453fe450)
                        (not E0x7ff4453fe8d0))
                   (and E0x7ff4453fe8d0
                        (not E0x7ff4453fe190)
                        (not E0x7ff4453fe450)
                        (not E0x7ff4453fe6d0)))))
      (a!7 (or (and v0x7ff4453ff210_0
                    v0x7ff4453ff4d0_0
                    (<= v0x7ff4453fa150_0 1.0)
                    (>= v0x7ff4453fa150_0 1.0)
                    (and (<= v0x7ff4453fa250_0 v0x7ff4453fe0d0_0)
                         (>= v0x7ff4453fa250_0 v0x7ff4453fe0d0_0))
                    (and (<= v0x7ff4453fa350_0 v0x7ff4453fc310_0)
                         (>= v0x7ff4453fa350_0 v0x7ff4453fc310_0))
                    (and (<= v0x7ff4453f8010_0 v0x7ff4453fe010_0)
                         (>= v0x7ff4453f8010_0 v0x7ff4453fe010_0)))
               (and v0x7ff4453fdf50_0
                    v0x7ff4453ff0d0_0
                    (<= v0x7ff4453fa150_0 0.0)
                    (>= v0x7ff4453fa150_0 0.0)
                    (and (<= v0x7ff4453fa250_0 v0x7ff4453fe0d0_0)
                         (>= v0x7ff4453fa250_0 v0x7ff4453fe0d0_0))
                    (and (<= v0x7ff4453fa350_0 v0x7ff4453fc310_0)
                         (>= v0x7ff4453fa350_0 v0x7ff4453fc310_0))
                    (and (<= v0x7ff4453f8010_0 v0x7ff4453fe010_0)
                         (>= v0x7ff4453f8010_0 v0x7ff4453fe010_0))))))
(let ((a!8 (and (=> v0x7ff4453fac50_0
                    (and v0x7ff4453fa990_0
                         E0x7ff4453fad10
                         (not v0x7ff4453fab10_0)))
                (=> v0x7ff4453fac50_0 E0x7ff4453fad10)
                a!1
                a!2
                (=> v0x7ff4453fbd10_0
                    (and v0x7ff4453fb390_0
                         E0x7ff4453fbdd0
                         (not v0x7ff4453fbbd0_0)))
                (=> v0x7ff4453fbd10_0 E0x7ff4453fbdd0)
                a!3
                a!4
                (=> v0x7ff4453fcbd0_0
                    (and v0x7ff4453fc250_0 E0x7ff4453fcc90 v0x7ff4453fca90_0))
                (=> v0x7ff4453fcbd0_0 E0x7ff4453fcc90)
                (=> v0x7ff4453fcf90_0
                    (and v0x7ff4453fc250_0
                         E0x7ff4453fd050
                         (not v0x7ff4453fca90_0)))
                (=> v0x7ff4453fcf90_0 E0x7ff4453fd050)
                (=> v0x7ff4453fd7d0_0
                    (and v0x7ff4453fcbd0_0 E0x7ff4453fd890 v0x7ff4453fce50_0))
                (=> v0x7ff4453fd7d0_0 E0x7ff4453fd890)
                (=> v0x7ff4453fdcd0_0
                    (and v0x7ff4453fcf90_0 E0x7ff4453fdd90 v0x7ff4453fd690_0))
                (=> v0x7ff4453fdcd0_0 E0x7ff4453fdd90)
                (=> v0x7ff4453fdf50_0 a!5)
                a!6
                (=> v0x7ff4453ff210_0
                    (and v0x7ff4453fdf50_0
                         E0x7ff4453ff2d0
                         (not v0x7ff4453ff0d0_0)))
                (=> v0x7ff4453ff210_0 E0x7ff4453ff2d0)
                a!7
                (= v0x7ff4453fab10_0 (= v0x7ff4453faa50_0 0.0))
                (= v0x7ff4453faf50_0 (< v0x7ff4453fa290_0 2.0))
                (= v0x7ff4453fb110_0 (ite v0x7ff4453faf50_0 1.0 0.0))
                (= v0x7ff4453fb250_0 (+ v0x7ff4453fb110_0 v0x7ff4453fa290_0))
                (= v0x7ff4453fbbd0_0 (= v0x7ff4453fbb10_0 0.0))
                (= v0x7ff4453fbfd0_0 (= v0x7ff4453fa190_0 0.0))
                (= v0x7ff4453fc110_0 (ite v0x7ff4453fbfd0_0 1.0 0.0))
                (= v0x7ff4453fca90_0 (= v0x7ff4453fa090_0 0.0))
                (= v0x7ff4453fce50_0 (> v0x7ff4453fb450_0 1.0))
                (= v0x7ff4453fd250_0 (> v0x7ff4453fb450_0 0.0))
                (= v0x7ff4453fd390_0 (+ v0x7ff4453fb450_0 (- 1.0)))
                (= v0x7ff4453fd550_0
                   (ite v0x7ff4453fd250_0 v0x7ff4453fd390_0 v0x7ff4453fb450_0))
                (= v0x7ff4453fd690_0 (= v0x7ff4453fd550_0 0.0))
                (= v0x7ff4453fda50_0 (= v0x7ff4453fc310_0 0.0))
                (= v0x7ff4453fdb90_0
                   (ite v0x7ff4453fda50_0 1.0 v0x7ff4453fa090_0))
                (= v0x7ff4453fee90_0 (= v0x7ff4453fc310_0 0.0))
                (= v0x7ff4453fef90_0 (= v0x7ff4453fe0d0_0 0.0))
                (= v0x7ff4453ff0d0_0 (or v0x7ff4453fef90_0 v0x7ff4453fee90_0))
                (= v0x7ff4453ff4d0_0 (= v0x7ff4453f9f10_0 0.0)))))
  (=> F0x7ff445400490 a!8))))
(assert (=> F0x7ff445400490 F0x7ff445400410))
(assert (let ((a!1 (=> v0x7ff4453fb390_0
               (or (and v0x7ff4453fac50_0
                        E0x7ff4453fb510
                        (<= v0x7ff4453fb450_0 v0x7ff4453fb250_0)
                        (>= v0x7ff4453fb450_0 v0x7ff4453fb250_0))
                   (and v0x7ff4453fa990_0
                        E0x7ff4453fb6d0
                        v0x7ff4453fab10_0
                        (<= v0x7ff4453fb450_0 v0x7ff4453fa290_0)
                        (>= v0x7ff4453fb450_0 v0x7ff4453fa290_0)))))
      (a!2 (=> v0x7ff4453fb390_0
               (or (and E0x7ff4453fb510 (not E0x7ff4453fb6d0))
                   (and E0x7ff4453fb6d0 (not E0x7ff4453fb510)))))
      (a!3 (=> v0x7ff4453fc250_0
               (or (and v0x7ff4453fbd10_0
                        E0x7ff4453fc3d0
                        (<= v0x7ff4453fc310_0 v0x7ff4453fc110_0)
                        (>= v0x7ff4453fc310_0 v0x7ff4453fc110_0))
                   (and v0x7ff4453fb390_0
                        E0x7ff4453fc590
                        v0x7ff4453fbbd0_0
                        (<= v0x7ff4453fc310_0 v0x7ff4453fa190_0)
                        (>= v0x7ff4453fc310_0 v0x7ff4453fa190_0)))))
      (a!4 (=> v0x7ff4453fc250_0
               (or (and E0x7ff4453fc3d0 (not E0x7ff4453fc590))
                   (and E0x7ff4453fc590 (not E0x7ff4453fc3d0)))))
      (a!5 (or (and v0x7ff4453fd7d0_0
                    E0x7ff4453fe190
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fb450_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fb450_0))
                    (<= v0x7ff4453fe0d0_0 v0x7ff4453fdb90_0)
                    (>= v0x7ff4453fe0d0_0 v0x7ff4453fdb90_0))
               (and v0x7ff4453fcbd0_0
                    E0x7ff4453fe450
                    (not v0x7ff4453fce50_0)
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fb450_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fb450_0))
                    (and (<= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)
                         (>= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)))
               (and v0x7ff4453fdcd0_0
                    E0x7ff4453fe6d0
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fd550_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fd550_0))
                    (and (<= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)
                         (>= v0x7ff4453fe0d0_0 v0x7ff4453fa090_0)))
               (and v0x7ff4453fcf90_0
                    E0x7ff4453fe8d0
                    (not v0x7ff4453fd690_0)
                    (and (<= v0x7ff4453fe010_0 v0x7ff4453fd550_0)
                         (>= v0x7ff4453fe010_0 v0x7ff4453fd550_0))
                    (<= v0x7ff4453fe0d0_0 0.0)
                    (>= v0x7ff4453fe0d0_0 0.0))))
      (a!6 (=> v0x7ff4453fdf50_0
               (or (and E0x7ff4453fe190
                        (not E0x7ff4453fe450)
                        (not E0x7ff4453fe6d0)
                        (not E0x7ff4453fe8d0))
                   (and E0x7ff4453fe450
                        (not E0x7ff4453fe190)
                        (not E0x7ff4453fe6d0)
                        (not E0x7ff4453fe8d0))
                   (and E0x7ff4453fe6d0
                        (not E0x7ff4453fe190)
                        (not E0x7ff4453fe450)
                        (not E0x7ff4453fe8d0))
                   (and E0x7ff4453fe8d0
                        (not E0x7ff4453fe190)
                        (not E0x7ff4453fe450)
                        (not E0x7ff4453fe6d0))))))
(let ((a!7 (and (=> v0x7ff4453fac50_0
                    (and v0x7ff4453fa990_0
                         E0x7ff4453fad10
                         (not v0x7ff4453fab10_0)))
                (=> v0x7ff4453fac50_0 E0x7ff4453fad10)
                a!1
                a!2
                (=> v0x7ff4453fbd10_0
                    (and v0x7ff4453fb390_0
                         E0x7ff4453fbdd0
                         (not v0x7ff4453fbbd0_0)))
                (=> v0x7ff4453fbd10_0 E0x7ff4453fbdd0)
                a!3
                a!4
                (=> v0x7ff4453fcbd0_0
                    (and v0x7ff4453fc250_0 E0x7ff4453fcc90 v0x7ff4453fca90_0))
                (=> v0x7ff4453fcbd0_0 E0x7ff4453fcc90)
                (=> v0x7ff4453fcf90_0
                    (and v0x7ff4453fc250_0
                         E0x7ff4453fd050
                         (not v0x7ff4453fca90_0)))
                (=> v0x7ff4453fcf90_0 E0x7ff4453fd050)
                (=> v0x7ff4453fd7d0_0
                    (and v0x7ff4453fcbd0_0 E0x7ff4453fd890 v0x7ff4453fce50_0))
                (=> v0x7ff4453fd7d0_0 E0x7ff4453fd890)
                (=> v0x7ff4453fdcd0_0
                    (and v0x7ff4453fcf90_0 E0x7ff4453fdd90 v0x7ff4453fd690_0))
                (=> v0x7ff4453fdcd0_0 E0x7ff4453fdd90)
                (=> v0x7ff4453fdf50_0 a!5)
                a!6
                (=> v0x7ff4453ff210_0
                    (and v0x7ff4453fdf50_0
                         E0x7ff4453ff2d0
                         (not v0x7ff4453ff0d0_0)))
                (=> v0x7ff4453ff210_0 E0x7ff4453ff2d0)
                v0x7ff4453ff210_0
                (not v0x7ff4453ff4d0_0)
                (= v0x7ff4453fab10_0 (= v0x7ff4453faa50_0 0.0))
                (= v0x7ff4453faf50_0 (< v0x7ff4453fa290_0 2.0))
                (= v0x7ff4453fb110_0 (ite v0x7ff4453faf50_0 1.0 0.0))
                (= v0x7ff4453fb250_0 (+ v0x7ff4453fb110_0 v0x7ff4453fa290_0))
                (= v0x7ff4453fbbd0_0 (= v0x7ff4453fbb10_0 0.0))
                (= v0x7ff4453fbfd0_0 (= v0x7ff4453fa190_0 0.0))
                (= v0x7ff4453fc110_0 (ite v0x7ff4453fbfd0_0 1.0 0.0))
                (= v0x7ff4453fca90_0 (= v0x7ff4453fa090_0 0.0))
                (= v0x7ff4453fce50_0 (> v0x7ff4453fb450_0 1.0))
                (= v0x7ff4453fd250_0 (> v0x7ff4453fb450_0 0.0))
                (= v0x7ff4453fd390_0 (+ v0x7ff4453fb450_0 (- 1.0)))
                (= v0x7ff4453fd550_0
                   (ite v0x7ff4453fd250_0 v0x7ff4453fd390_0 v0x7ff4453fb450_0))
                (= v0x7ff4453fd690_0 (= v0x7ff4453fd550_0 0.0))
                (= v0x7ff4453fda50_0 (= v0x7ff4453fc310_0 0.0))
                (= v0x7ff4453fdb90_0
                   (ite v0x7ff4453fda50_0 1.0 v0x7ff4453fa090_0))
                (= v0x7ff4453fee90_0 (= v0x7ff4453fc310_0 0.0))
                (= v0x7ff4453fef90_0 (= v0x7ff4453fe0d0_0 0.0))
                (= v0x7ff4453ff0d0_0 (or v0x7ff4453fef90_0 v0x7ff4453fee90_0))
                (= v0x7ff4453ff4d0_0 (= v0x7ff4453f9f10_0 0.0)))))
  (=> F0x7ff445400310 a!7))))
(assert (=> F0x7ff445400310 F0x7ff445400410))
(assert (=> F0x7ff445400690 (or F0x7ff4454005d0 F0x7ff445400490)))
(assert (=> F0x7ff445400650 F0x7ff445400310))
(assert (=> pre!entry!0 (=> F0x7ff445400550 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7ff445400410 (>= v0x7ff4453f9f10_0 0.0))))
(assert (let ((a!1 (=> F0x7ff445400410
               (or (<= v0x7ff4453f9f10_0 0.0)
                   (not (<= 0.0 v0x7ff4453fa190_0))
                   (not (<= v0x7ff4453fa190_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2
    (=> F0x7ff445400410
        (or (>= v0x7ff4453fa190_0 1.0) (<= v0x7ff4453fa190_0 0.0)))))
(assert (let ((a!1 (=> F0x7ff445400410
               (or (not (<= v0x7ff4453fa290_0 1.0)) (<= v0x7ff4453fa090_0 0.0)))))
  (=> pre!bb1.i.i!3 a!1)))
(assert (=> pre!bb1.i.i!4 (=> F0x7ff445400410 (<= v0x7ff4453f9f10_0 0.0))))
(assert (=> pre!bb1.i.i!5 (=> F0x7ff445400410 (>= v0x7ff4453fa090_0 0.0))))
(assert (let ((a!1 (not (or (<= v0x7ff4453fa150_0 0.0)
                    (not (<= 0.0 v0x7ff4453fa350_0))
                    (not (<= v0x7ff4453fa350_0 0.0)))))
      (a!2 (and (not post!bb1.i.i!2)
                F0x7ff445400690
                (not (or (>= v0x7ff4453fa350_0 1.0) (<= v0x7ff4453fa350_0 0.0)))))
      (a!3 (not (or (not (<= v0x7ff4453f8010_0 1.0)) (<= v0x7ff4453fa250_0 0.0)))))
  (or (and (not post!bb1.i.i!0)
           F0x7ff445400690
           (not (>= v0x7ff4453fa150_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7ff445400690 a!1)
      a!2
      (and (not post!bb1.i.i!3) F0x7ff445400690 a!3)
      (and (not post!bb1.i.i!4)
           F0x7ff445400690
           (not (<= v0x7ff4453fa150_0 0.0)))
      (and (not post!bb1.i.i!5)
           F0x7ff445400690
           (not (>= v0x7ff4453fa250_0 0.0)))
      (and (not post!bb2.i.i34.i.i!0) F0x7ff445400650 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i34.i.i!0)
