(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7fbb012c1490 () Bool)
(declare-fun F0x7fbb012c1390 () Bool)
(declare-fun v0x7fbb012be8d0_0 () Bool)
(declare-fun v0x7fbb012be0d0_0 () Bool)
(declare-fun v0x7fbb012bfcd0_0 () Bool)
(declare-fun v0x7fbb012bad90_0 () Real)
(declare-fun F0x7fbb012c12d0 () Bool)
(declare-fun v0x7fbb012bc990_0 () Real)
(declare-fun v0x7fbb012bbf90_0 () Real)
(declare-fun v0x7fbb012bb8d0_0 () Real)
(declare-fun v0x7fbb012bbdd0_0 () Bool)
(declare-fun v0x7fbb012bfdd0_0 () Bool)
(declare-fun F0x7fbb012c14d0 () Bool)
(declare-fun v0x7fbb012c0310_0 () Bool)
(declare-fun v0x7fbb012c0050_0 () Bool)
(declare-fun E0x7fbb012bf290 () Bool)
(declare-fun v0x7fbb012be9d0_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fbb012bee50_0 () Real)
(declare-fun v0x7fbb012bed90_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fbb012be510_0 () Bool)
(declare-fun v0x7fbb012bdcd0_0 () Bool)
(declare-fun E0x7fbb012be710 () Bool)
(declare-fun v0x7fbb012be650_0 () Bool)
(declare-fun v0x7fbb012bef10_0 () Real)
(declare-fun v0x7fbb012beb10_0 () Bool)
(declare-fun v0x7fbb012bd910_0 () Bool)
(declare-fun E0x7fbb012bdb10 () Bool)
(declare-fun v0x7fbb012bff10_0 () Bool)
(declare-fun v0x7fbb012bda50_0 () Bool)
(declare-fun v0x7fbb012bcf90_0 () Real)
(declare-fun v0x7fbb012baf10_0 () Real)
(declare-fun E0x7fbb012bf710 () Bool)
(declare-fun v0x7fbb012bd190_0 () Real)
(declare-fun E0x7fbb012bebd0 () Bool)
(declare-fun E0x7fbb012bd250 () Bool)
(declare-fun E0x7fbb012bf510 () Bool)
(declare-fun v0x7fbb012bd0d0_0 () Bool)
(declare-fun E0x7fbb012bcc50 () Bool)
(declare-fun E0x7fbb012befd0 () Bool)
(declare-fun E0x7fbb012bc550 () Bool)
(declare-fun v0x7fbb012bc210_0 () Bool)
(declare-fun v0x7fbb012be210_0 () Real)
(declare-fun E0x7fbb012bc390 () Bool)
(declare-fun v0x7fbb012bb990_0 () Bool)
(declare-fun v0x7fbb012bca50_0 () Bool)
(declare-fun v0x7fbb012bbad0_0 () Bool)
(declare-fun F0x7fbb012c1210 () Bool)
(declare-fun v0x7fbb012bb110_0 () Real)
(declare-fun v0x7fbb012bce50_0 () Bool)
(declare-fun F0x7fbb012c1150 () Bool)
(declare-fun E0x7fbb012bd410 () Bool)
(declare-fun v0x7fbb012bc0d0_0 () Real)
(declare-fun v0x7fbb012b9010_0 () Real)
(declare-fun v0x7fbb012bcb90_0 () Bool)
(declare-fun v0x7fbb012bb1d0_0 () Real)
(declare-fun post!bb2.i.i29.i.i!0 () Bool)
(declare-fun v0x7fbb012bb010_0 () Real)
(declare-fun E0x7fbb012bbb90 () Bool)
(declare-fun v0x7fbb012bb810_0 () Bool)
(declare-fun E0x7fbb012bded0 () Bool)
(declare-fun v0x7fbb012bb0d0_0 () Real)
(declare-fun v0x7fbb012be3d0_0 () Real)
(declare-fun E0x7fbb012c0110 () Bool)
(declare-fun v0x7fbb012bc2d0_0 () Real)
(declare-fun v0x7fbb012bafd0_0 () Real)
(declare-fun v0x7fbb012b9110_0 () Bool)
(declare-fun v0x7fbb012bde10_0 () Bool)
(declare-fun F0x7fbb012c1090 () Bool)

(assert (=> F0x7fbb012c1090
    (and v0x7fbb012b9110_0
         (<= v0x7fbb012bafd0_0 0.0)
         (>= v0x7fbb012bafd0_0 0.0)
         (<= v0x7fbb012bb0d0_0 0.0)
         (>= v0x7fbb012bb0d0_0 0.0)
         (<= v0x7fbb012bb1d0_0 0.0)
         (>= v0x7fbb012bb1d0_0 0.0)
         (<= v0x7fbb012b9010_0 1.0)
         (>= v0x7fbb012b9010_0 1.0))))
(assert (=> F0x7fbb012c1090 F0x7fbb012c1150))
(assert (let ((a!1 (=> v0x7fbb012bc210_0
               (or (and v0x7fbb012bbad0_0
                        E0x7fbb012bc390
                        (<= v0x7fbb012bc2d0_0 v0x7fbb012bc0d0_0)
                        (>= v0x7fbb012bc2d0_0 v0x7fbb012bc0d0_0))
                   (and v0x7fbb012bb810_0
                        E0x7fbb012bc550
                        v0x7fbb012bb990_0
                        (<= v0x7fbb012bc2d0_0 v0x7fbb012bb110_0)
                        (>= v0x7fbb012bc2d0_0 v0x7fbb012bb110_0)))))
      (a!2 (=> v0x7fbb012bc210_0
               (or (and E0x7fbb012bc390 (not E0x7fbb012bc550))
                   (and E0x7fbb012bc550 (not E0x7fbb012bc390)))))
      (a!3 (=> v0x7fbb012bd0d0_0
               (or (and v0x7fbb012bcb90_0
                        E0x7fbb012bd250
                        (<= v0x7fbb012bd190_0 v0x7fbb012bcf90_0)
                        (>= v0x7fbb012bd190_0 v0x7fbb012bcf90_0))
                   (and v0x7fbb012bc210_0
                        E0x7fbb012bd410
                        v0x7fbb012bca50_0
                        (<= v0x7fbb012bd190_0 v0x7fbb012bb010_0)
                        (>= v0x7fbb012bd190_0 v0x7fbb012bb010_0)))))
      (a!4 (=> v0x7fbb012bd0d0_0
               (or (and E0x7fbb012bd250 (not E0x7fbb012bd410))
                   (and E0x7fbb012bd410 (not E0x7fbb012bd250)))))
      (a!5 (or (and v0x7fbb012be650_0
                    E0x7fbb012befd0
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0))
                    (<= v0x7fbb012bef10_0 v0x7fbb012be9d0_0)
                    (>= v0x7fbb012bef10_0 v0x7fbb012be9d0_0))
               (and v0x7fbb012bda50_0
                    E0x7fbb012bf290
                    (not v0x7fbb012bdcd0_0)
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0))
                    (and (<= v0x7fbb012bef10_0 v0x7fbb012baf10_0)
                         (>= v0x7fbb012bef10_0 v0x7fbb012baf10_0)))
               (and v0x7fbb012beb10_0
                    E0x7fbb012bf510
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012be3d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012be3d0_0))
                    (and (<= v0x7fbb012bef10_0 v0x7fbb012baf10_0)
                         (>= v0x7fbb012bef10_0 v0x7fbb012baf10_0)))
               (and v0x7fbb012bde10_0
                    E0x7fbb012bf710
                    (not v0x7fbb012be510_0)
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012be3d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012be3d0_0))
                    (<= v0x7fbb012bef10_0 0.0)
                    (>= v0x7fbb012bef10_0 0.0))))
      (a!6 (=> v0x7fbb012bed90_0
               (or (and E0x7fbb012befd0
                        (not E0x7fbb012bf290)
                        (not E0x7fbb012bf510)
                        (not E0x7fbb012bf710))
                   (and E0x7fbb012bf290
                        (not E0x7fbb012befd0)
                        (not E0x7fbb012bf510)
                        (not E0x7fbb012bf710))
                   (and E0x7fbb012bf510
                        (not E0x7fbb012befd0)
                        (not E0x7fbb012bf290)
                        (not E0x7fbb012bf710))
                   (and E0x7fbb012bf710
                        (not E0x7fbb012befd0)
                        (not E0x7fbb012bf290)
                        (not E0x7fbb012bf510)))))
      (a!7 (or (and v0x7fbb012c0050_0
                    v0x7fbb012c0310_0
                    (<= v0x7fbb012bafd0_0 1.0)
                    (>= v0x7fbb012bafd0_0 1.0)
                    (and (<= v0x7fbb012bb0d0_0 v0x7fbb012bef10_0)
                         (>= v0x7fbb012bb0d0_0 v0x7fbb012bef10_0))
                    (and (<= v0x7fbb012bb1d0_0 v0x7fbb012bd190_0)
                         (>= v0x7fbb012bb1d0_0 v0x7fbb012bd190_0))
                    (and (<= v0x7fbb012b9010_0 v0x7fbb012bee50_0)
                         (>= v0x7fbb012b9010_0 v0x7fbb012bee50_0)))
               (and v0x7fbb012bed90_0
                    v0x7fbb012bff10_0
                    (<= v0x7fbb012bafd0_0 0.0)
                    (>= v0x7fbb012bafd0_0 0.0)
                    (and (<= v0x7fbb012bb0d0_0 v0x7fbb012bef10_0)
                         (>= v0x7fbb012bb0d0_0 v0x7fbb012bef10_0))
                    (and (<= v0x7fbb012bb1d0_0 v0x7fbb012bd190_0)
                         (>= v0x7fbb012bb1d0_0 v0x7fbb012bd190_0))
                    (and (<= v0x7fbb012b9010_0 v0x7fbb012bee50_0)
                         (>= v0x7fbb012b9010_0 v0x7fbb012bee50_0))))))
(let ((a!8 (and (=> v0x7fbb012bbad0_0
                    (and v0x7fbb012bb810_0
                         E0x7fbb012bbb90
                         (not v0x7fbb012bb990_0)))
                (=> v0x7fbb012bbad0_0 E0x7fbb012bbb90)
                a!1
                a!2
                (=> v0x7fbb012bcb90_0
                    (and v0x7fbb012bc210_0
                         E0x7fbb012bcc50
                         (not v0x7fbb012bca50_0)))
                (=> v0x7fbb012bcb90_0 E0x7fbb012bcc50)
                a!3
                a!4
                (=> v0x7fbb012bda50_0
                    (and v0x7fbb012bd0d0_0 E0x7fbb012bdb10 v0x7fbb012bd910_0))
                (=> v0x7fbb012bda50_0 E0x7fbb012bdb10)
                (=> v0x7fbb012bde10_0
                    (and v0x7fbb012bd0d0_0
                         E0x7fbb012bded0
                         (not v0x7fbb012bd910_0)))
                (=> v0x7fbb012bde10_0 E0x7fbb012bded0)
                (=> v0x7fbb012be650_0
                    (and v0x7fbb012bda50_0 E0x7fbb012be710 v0x7fbb012bdcd0_0))
                (=> v0x7fbb012be650_0 E0x7fbb012be710)
                (=> v0x7fbb012beb10_0
                    (and v0x7fbb012bde10_0 E0x7fbb012bebd0 v0x7fbb012be510_0))
                (=> v0x7fbb012beb10_0 E0x7fbb012bebd0)
                (=> v0x7fbb012bed90_0 a!5)
                a!6
                (=> v0x7fbb012c0050_0
                    (and v0x7fbb012bed90_0
                         E0x7fbb012c0110
                         (not v0x7fbb012bff10_0)))
                (=> v0x7fbb012c0050_0 E0x7fbb012c0110)
                a!7
                (= v0x7fbb012bb990_0 (= v0x7fbb012bb8d0_0 0.0))
                (= v0x7fbb012bbdd0_0 (< v0x7fbb012bb110_0 2.0))
                (= v0x7fbb012bbf90_0 (ite v0x7fbb012bbdd0_0 1.0 0.0))
                (= v0x7fbb012bc0d0_0 (+ v0x7fbb012bbf90_0 v0x7fbb012bb110_0))
                (= v0x7fbb012bca50_0 (= v0x7fbb012bc990_0 0.0))
                (= v0x7fbb012bce50_0 (= v0x7fbb012bb010_0 0.0))
                (= v0x7fbb012bcf90_0 (ite v0x7fbb012bce50_0 1.0 0.0))
                (= v0x7fbb012bd910_0 (= v0x7fbb012baf10_0 0.0))
                (= v0x7fbb012bdcd0_0 (> v0x7fbb012bc2d0_0 1.0))
                (= v0x7fbb012be0d0_0 (> v0x7fbb012bc2d0_0 0.0))
                (= v0x7fbb012be210_0 (+ v0x7fbb012bc2d0_0 (- 1.0)))
                (= v0x7fbb012be3d0_0
                   (ite v0x7fbb012be0d0_0 v0x7fbb012be210_0 v0x7fbb012bc2d0_0))
                (= v0x7fbb012be510_0 (= v0x7fbb012bd190_0 0.0))
                (= v0x7fbb012be8d0_0 (= v0x7fbb012bd190_0 0.0))
                (= v0x7fbb012be9d0_0
                   (ite v0x7fbb012be8d0_0 1.0 v0x7fbb012baf10_0))
                (= v0x7fbb012bfcd0_0 (= v0x7fbb012bd190_0 0.0))
                (= v0x7fbb012bfdd0_0 (= v0x7fbb012bef10_0 0.0))
                (= v0x7fbb012bff10_0 (or v0x7fbb012bfdd0_0 v0x7fbb012bfcd0_0))
                (= v0x7fbb012c0310_0 (= v0x7fbb012bad90_0 0.0)))))
  (=> F0x7fbb012c1210 a!8))))
(assert (=> F0x7fbb012c1210 F0x7fbb012c12d0))
(assert (let ((a!1 (=> v0x7fbb012bc210_0
               (or (and v0x7fbb012bbad0_0
                        E0x7fbb012bc390
                        (<= v0x7fbb012bc2d0_0 v0x7fbb012bc0d0_0)
                        (>= v0x7fbb012bc2d0_0 v0x7fbb012bc0d0_0))
                   (and v0x7fbb012bb810_0
                        E0x7fbb012bc550
                        v0x7fbb012bb990_0
                        (<= v0x7fbb012bc2d0_0 v0x7fbb012bb110_0)
                        (>= v0x7fbb012bc2d0_0 v0x7fbb012bb110_0)))))
      (a!2 (=> v0x7fbb012bc210_0
               (or (and E0x7fbb012bc390 (not E0x7fbb012bc550))
                   (and E0x7fbb012bc550 (not E0x7fbb012bc390)))))
      (a!3 (=> v0x7fbb012bd0d0_0
               (or (and v0x7fbb012bcb90_0
                        E0x7fbb012bd250
                        (<= v0x7fbb012bd190_0 v0x7fbb012bcf90_0)
                        (>= v0x7fbb012bd190_0 v0x7fbb012bcf90_0))
                   (and v0x7fbb012bc210_0
                        E0x7fbb012bd410
                        v0x7fbb012bca50_0
                        (<= v0x7fbb012bd190_0 v0x7fbb012bb010_0)
                        (>= v0x7fbb012bd190_0 v0x7fbb012bb010_0)))))
      (a!4 (=> v0x7fbb012bd0d0_0
               (or (and E0x7fbb012bd250 (not E0x7fbb012bd410))
                   (and E0x7fbb012bd410 (not E0x7fbb012bd250)))))
      (a!5 (or (and v0x7fbb012be650_0
                    E0x7fbb012befd0
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0))
                    (<= v0x7fbb012bef10_0 v0x7fbb012be9d0_0)
                    (>= v0x7fbb012bef10_0 v0x7fbb012be9d0_0))
               (and v0x7fbb012bda50_0
                    E0x7fbb012bf290
                    (not v0x7fbb012bdcd0_0)
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012bc2d0_0))
                    (and (<= v0x7fbb012bef10_0 v0x7fbb012baf10_0)
                         (>= v0x7fbb012bef10_0 v0x7fbb012baf10_0)))
               (and v0x7fbb012beb10_0
                    E0x7fbb012bf510
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012be3d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012be3d0_0))
                    (and (<= v0x7fbb012bef10_0 v0x7fbb012baf10_0)
                         (>= v0x7fbb012bef10_0 v0x7fbb012baf10_0)))
               (and v0x7fbb012bde10_0
                    E0x7fbb012bf710
                    (not v0x7fbb012be510_0)
                    (and (<= v0x7fbb012bee50_0 v0x7fbb012be3d0_0)
                         (>= v0x7fbb012bee50_0 v0x7fbb012be3d0_0))
                    (<= v0x7fbb012bef10_0 0.0)
                    (>= v0x7fbb012bef10_0 0.0))))
      (a!6 (=> v0x7fbb012bed90_0
               (or (and E0x7fbb012befd0
                        (not E0x7fbb012bf290)
                        (not E0x7fbb012bf510)
                        (not E0x7fbb012bf710))
                   (and E0x7fbb012bf290
                        (not E0x7fbb012befd0)
                        (not E0x7fbb012bf510)
                        (not E0x7fbb012bf710))
                   (and E0x7fbb012bf510
                        (not E0x7fbb012befd0)
                        (not E0x7fbb012bf290)
                        (not E0x7fbb012bf710))
                   (and E0x7fbb012bf710
                        (not E0x7fbb012befd0)
                        (not E0x7fbb012bf290)
                        (not E0x7fbb012bf510))))))
(let ((a!7 (and (=> v0x7fbb012bbad0_0
                    (and v0x7fbb012bb810_0
                         E0x7fbb012bbb90
                         (not v0x7fbb012bb990_0)))
                (=> v0x7fbb012bbad0_0 E0x7fbb012bbb90)
                a!1
                a!2
                (=> v0x7fbb012bcb90_0
                    (and v0x7fbb012bc210_0
                         E0x7fbb012bcc50
                         (not v0x7fbb012bca50_0)))
                (=> v0x7fbb012bcb90_0 E0x7fbb012bcc50)
                a!3
                a!4
                (=> v0x7fbb012bda50_0
                    (and v0x7fbb012bd0d0_0 E0x7fbb012bdb10 v0x7fbb012bd910_0))
                (=> v0x7fbb012bda50_0 E0x7fbb012bdb10)
                (=> v0x7fbb012bde10_0
                    (and v0x7fbb012bd0d0_0
                         E0x7fbb012bded0
                         (not v0x7fbb012bd910_0)))
                (=> v0x7fbb012bde10_0 E0x7fbb012bded0)
                (=> v0x7fbb012be650_0
                    (and v0x7fbb012bda50_0 E0x7fbb012be710 v0x7fbb012bdcd0_0))
                (=> v0x7fbb012be650_0 E0x7fbb012be710)
                (=> v0x7fbb012beb10_0
                    (and v0x7fbb012bde10_0 E0x7fbb012bebd0 v0x7fbb012be510_0))
                (=> v0x7fbb012beb10_0 E0x7fbb012bebd0)
                (=> v0x7fbb012bed90_0 a!5)
                a!6
                (=> v0x7fbb012c0050_0
                    (and v0x7fbb012bed90_0
                         E0x7fbb012c0110
                         (not v0x7fbb012bff10_0)))
                (=> v0x7fbb012c0050_0 E0x7fbb012c0110)
                v0x7fbb012c0050_0
                (not v0x7fbb012c0310_0)
                (= v0x7fbb012bb990_0 (= v0x7fbb012bb8d0_0 0.0))
                (= v0x7fbb012bbdd0_0 (< v0x7fbb012bb110_0 2.0))
                (= v0x7fbb012bbf90_0 (ite v0x7fbb012bbdd0_0 1.0 0.0))
                (= v0x7fbb012bc0d0_0 (+ v0x7fbb012bbf90_0 v0x7fbb012bb110_0))
                (= v0x7fbb012bca50_0 (= v0x7fbb012bc990_0 0.0))
                (= v0x7fbb012bce50_0 (= v0x7fbb012bb010_0 0.0))
                (= v0x7fbb012bcf90_0 (ite v0x7fbb012bce50_0 1.0 0.0))
                (= v0x7fbb012bd910_0 (= v0x7fbb012baf10_0 0.0))
                (= v0x7fbb012bdcd0_0 (> v0x7fbb012bc2d0_0 1.0))
                (= v0x7fbb012be0d0_0 (> v0x7fbb012bc2d0_0 0.0))
                (= v0x7fbb012be210_0 (+ v0x7fbb012bc2d0_0 (- 1.0)))
                (= v0x7fbb012be3d0_0
                   (ite v0x7fbb012be0d0_0 v0x7fbb012be210_0 v0x7fbb012bc2d0_0))
                (= v0x7fbb012be510_0 (= v0x7fbb012bd190_0 0.0))
                (= v0x7fbb012be8d0_0 (= v0x7fbb012bd190_0 0.0))
                (= v0x7fbb012be9d0_0
                   (ite v0x7fbb012be8d0_0 1.0 v0x7fbb012baf10_0))
                (= v0x7fbb012bfcd0_0 (= v0x7fbb012bd190_0 0.0))
                (= v0x7fbb012bfdd0_0 (= v0x7fbb012bef10_0 0.0))
                (= v0x7fbb012bff10_0 (or v0x7fbb012bfdd0_0 v0x7fbb012bfcd0_0))
                (= v0x7fbb012c0310_0 (= v0x7fbb012bad90_0 0.0)))))
  (=> F0x7fbb012c1390 a!7))))
(assert (=> F0x7fbb012c1390 F0x7fbb012c12d0))
(assert (=> F0x7fbb012c14d0 (or F0x7fbb012c1090 F0x7fbb012c1210)))
(assert (=> F0x7fbb012c1490 F0x7fbb012c1390))
(assert (=> pre!entry!0 (=> F0x7fbb012c1150 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fbb012c12d0 true)))
(assert (or (and (not post!bb1.i.i!0) F0x7fbb012c14d0 false)
    (and (not post!bb2.i.i29.i.i!0) F0x7fbb012c1490 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0)
;(post-assumptions: post!bb1.i.i!0 post!bb2.i.i29.i.i!0)
