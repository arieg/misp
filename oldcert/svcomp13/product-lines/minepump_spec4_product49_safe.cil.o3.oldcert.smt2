(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f49d4591210 () Bool)
(declare-fun v0x7f49d45902d0_0 () Bool)
(declare-fun v0x7f49d458ec90_0 () Bool)
(declare-fun F0x7f49d4591110 () Bool)
(declare-fun v0x7f49d458e750_0 () Bool)
(declare-fun v0x7f49d458d810_0 () Real)
(declare-fun v0x7f49d458d650_0 () Bool)
(declare-fun v0x7f49d458d150_0 () Real)
(declare-fun E0x7f49d458fc50 () Bool)
(declare-fun v0x7f49d458ca90_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f49d458ef90_0 () Real)
(declare-fun v0x7f49d458f550_0 () Real)
(declare-fun v0x7f49d458f490_0 () Bool)
(declare-fun E0x7f49d458f2d0 () Bool)
(declare-fun v0x7f49d458f210_0 () Bool)
(declare-fun E0x7f49d458f990 () Bool)
(declare-fun E0x7f49d458ea90 () Bool)
(declare-fun F0x7f49d4591250 () Bool)
(declare-fun v0x7f49d458e390_0 () Bool)
(declare-fun E0x7f49d458e590 () Bool)
(declare-fun v0x7f49d4590190_0 () Bool)
(declare-fun v0x7f49d458edd0_0 () Real)
(declare-fun E0x7f49d458f6d0 () Bool)
(declare-fun v0x7f49d458cc10_0 () Real)
(declare-fun post!bb1.i.i26.i.i!0 () Bool)
(declare-fun E0x7f49d458ddd0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f49d458e4d0_0 () Bool)
(declare-fun v0x7f49d458d950_0 () Real)
(declare-fun v0x7f49d458db50_0 () Real)
(declare-fun E0x7f49d458dc10 () Bool)
(declare-fun F0x7f49d4591050 () Bool)
(declare-fun v0x7f49d458da90_0 () Bool)
(declare-fun v0x7f49d458d210_0 () Bool)
(declare-fun E0x7f49d458d410 () Bool)
(declare-fun v0x7f49d458e9d0_0 () Bool)
(declare-fun v0x7f49d458f0d0_0 () Bool)
(declare-fun v0x7f49d458d090_0 () Bool)
(declare-fun v0x7f49d4590410_0 () Bool)
(declare-fun v0x7f49d458d350_0 () Bool)
(declare-fun v0x7f49d458f610_0 () Real)
(declare-fun F0x7f49d4590e90 () Bool)
(declare-fun v0x7f49d458ccd0_0 () Real)
(declare-fun F0x7f49d4590f90 () Bool)
(declare-fun v0x7f49d458b110_0 () Bool)
(declare-fun v0x7f49d458b010_0 () Real)
(declare-fun v0x7f49d458e890_0 () Real)
(declare-fun F0x7f49d4590f50 () Bool)

(assert (=> F0x7f49d4590f50
    (and v0x7f49d458b110_0
         (<= v0x7f49d458ccd0_0 0.0)
         (>= v0x7f49d458ccd0_0 0.0)
         (<= v0x7f49d458b010_0 1.0)
         (>= v0x7f49d458b010_0 1.0))))
(assert (=> F0x7f49d4590f50 F0x7f49d4590e90))
(assert (let ((a!1 (=> v0x7f49d458da90_0
               (or (and v0x7f49d458d350_0
                        E0x7f49d458dc10
                        (<= v0x7f49d458db50_0 v0x7f49d458d950_0)
                        (>= v0x7f49d458db50_0 v0x7f49d458d950_0))
                   (and v0x7f49d458d090_0
                        E0x7f49d458ddd0
                        v0x7f49d458d210_0
                        (<= v0x7f49d458db50_0 v0x7f49d458cc10_0)
                        (>= v0x7f49d458db50_0 v0x7f49d458cc10_0)))))
      (a!2 (=> v0x7f49d458da90_0
               (or (and E0x7f49d458dc10 (not E0x7f49d458ddd0))
                   (and E0x7f49d458ddd0 (not E0x7f49d458dc10)))))
      (a!3 (or (and v0x7f49d458e4d0_0
                    E0x7f49d458f6d0
                    (<= v0x7f49d458f550_0 v0x7f49d458db50_0)
                    (>= v0x7f49d458f550_0 v0x7f49d458db50_0)
                    (<= v0x7f49d458f610_0 v0x7f49d458e890_0)
                    (>= v0x7f49d458f610_0 v0x7f49d458e890_0))
               (and v0x7f49d458f210_0
                    E0x7f49d458f990
                    (and (<= v0x7f49d458f550_0 v0x7f49d458ef90_0)
                         (>= v0x7f49d458f550_0 v0x7f49d458ef90_0))
                    (<= v0x7f49d458f610_0 v0x7f49d458ca90_0)
                    (>= v0x7f49d458f610_0 v0x7f49d458ca90_0))
               (and v0x7f49d458e9d0_0
                    E0x7f49d458fc50
                    (not v0x7f49d458f0d0_0)
                    (and (<= v0x7f49d458f550_0 v0x7f49d458ef90_0)
                         (>= v0x7f49d458f550_0 v0x7f49d458ef90_0))
                    (<= v0x7f49d458f610_0 0.0)
                    (>= v0x7f49d458f610_0 0.0))))
      (a!4 (=> v0x7f49d458f490_0
               (or (and E0x7f49d458f6d0
                        (not E0x7f49d458f990)
                        (not E0x7f49d458fc50))
                   (and E0x7f49d458f990
                        (not E0x7f49d458f6d0)
                        (not E0x7f49d458fc50))
                   (and E0x7f49d458fc50
                        (not E0x7f49d458f6d0)
                        (not E0x7f49d458f990))))))
(let ((a!5 (and (=> v0x7f49d458d350_0
                    (and v0x7f49d458d090_0
                         E0x7f49d458d410
                         (not v0x7f49d458d210_0)))
                (=> v0x7f49d458d350_0 E0x7f49d458d410)
                a!1
                a!2
                (=> v0x7f49d458e4d0_0
                    (and v0x7f49d458da90_0 E0x7f49d458e590 v0x7f49d458e390_0))
                (=> v0x7f49d458e4d0_0 E0x7f49d458e590)
                (=> v0x7f49d458e9d0_0
                    (and v0x7f49d458da90_0
                         E0x7f49d458ea90
                         (not v0x7f49d458e390_0)))
                (=> v0x7f49d458e9d0_0 E0x7f49d458ea90)
                (=> v0x7f49d458f210_0
                    (and v0x7f49d458e9d0_0 E0x7f49d458f2d0 v0x7f49d458f0d0_0))
                (=> v0x7f49d458f210_0 E0x7f49d458f2d0)
                (=> v0x7f49d458f490_0 a!3)
                a!4
                v0x7f49d458f490_0
                v0x7f49d4590410_0
                (<= v0x7f49d458ccd0_0 v0x7f49d458f610_0)
                (>= v0x7f49d458ccd0_0 v0x7f49d458f610_0)
                (<= v0x7f49d458b010_0 v0x7f49d458f550_0)
                (>= v0x7f49d458b010_0 v0x7f49d458f550_0)
                (= v0x7f49d458d210_0 (= v0x7f49d458d150_0 0.0))
                (= v0x7f49d458d650_0 (< v0x7f49d458cc10_0 2.0))
                (= v0x7f49d458d810_0 (ite v0x7f49d458d650_0 1.0 0.0))
                (= v0x7f49d458d950_0 (+ v0x7f49d458d810_0 v0x7f49d458cc10_0))
                (= v0x7f49d458e390_0 (= v0x7f49d458ca90_0 0.0))
                (= v0x7f49d458e750_0 (> v0x7f49d458db50_0 1.0))
                (= v0x7f49d458e890_0
                   (ite v0x7f49d458e750_0 1.0 v0x7f49d458ca90_0))
                (= v0x7f49d458ec90_0 (> v0x7f49d458db50_0 0.0))
                (= v0x7f49d458edd0_0 (+ v0x7f49d458db50_0 (- 1.0)))
                (= v0x7f49d458ef90_0
                   (ite v0x7f49d458ec90_0 v0x7f49d458edd0_0 v0x7f49d458db50_0))
                (= v0x7f49d458f0d0_0 (= v0x7f49d458ef90_0 0.0))
                (= v0x7f49d4590190_0 (not (= v0x7f49d458f550_0 0.0)))
                (= v0x7f49d45902d0_0 (= v0x7f49d458f610_0 0.0))
                (= v0x7f49d4590410_0 (or v0x7f49d45902d0_0 v0x7f49d4590190_0)))))
  (=> F0x7f49d4590f90 a!5))))
(assert (=> F0x7f49d4590f90 F0x7f49d4591050))
(assert (let ((a!1 (=> v0x7f49d458da90_0
               (or (and v0x7f49d458d350_0
                        E0x7f49d458dc10
                        (<= v0x7f49d458db50_0 v0x7f49d458d950_0)
                        (>= v0x7f49d458db50_0 v0x7f49d458d950_0))
                   (and v0x7f49d458d090_0
                        E0x7f49d458ddd0
                        v0x7f49d458d210_0
                        (<= v0x7f49d458db50_0 v0x7f49d458cc10_0)
                        (>= v0x7f49d458db50_0 v0x7f49d458cc10_0)))))
      (a!2 (=> v0x7f49d458da90_0
               (or (and E0x7f49d458dc10 (not E0x7f49d458ddd0))
                   (and E0x7f49d458ddd0 (not E0x7f49d458dc10)))))
      (a!3 (or (and v0x7f49d458e4d0_0
                    E0x7f49d458f6d0
                    (<= v0x7f49d458f550_0 v0x7f49d458db50_0)
                    (>= v0x7f49d458f550_0 v0x7f49d458db50_0)
                    (<= v0x7f49d458f610_0 v0x7f49d458e890_0)
                    (>= v0x7f49d458f610_0 v0x7f49d458e890_0))
               (and v0x7f49d458f210_0
                    E0x7f49d458f990
                    (and (<= v0x7f49d458f550_0 v0x7f49d458ef90_0)
                         (>= v0x7f49d458f550_0 v0x7f49d458ef90_0))
                    (<= v0x7f49d458f610_0 v0x7f49d458ca90_0)
                    (>= v0x7f49d458f610_0 v0x7f49d458ca90_0))
               (and v0x7f49d458e9d0_0
                    E0x7f49d458fc50
                    (not v0x7f49d458f0d0_0)
                    (and (<= v0x7f49d458f550_0 v0x7f49d458ef90_0)
                         (>= v0x7f49d458f550_0 v0x7f49d458ef90_0))
                    (<= v0x7f49d458f610_0 0.0)
                    (>= v0x7f49d458f610_0 0.0))))
      (a!4 (=> v0x7f49d458f490_0
               (or (and E0x7f49d458f6d0
                        (not E0x7f49d458f990)
                        (not E0x7f49d458fc50))
                   (and E0x7f49d458f990
                        (not E0x7f49d458f6d0)
                        (not E0x7f49d458fc50))
                   (and E0x7f49d458fc50
                        (not E0x7f49d458f6d0)
                        (not E0x7f49d458f990))))))
(let ((a!5 (and (=> v0x7f49d458d350_0
                    (and v0x7f49d458d090_0
                         E0x7f49d458d410
                         (not v0x7f49d458d210_0)))
                (=> v0x7f49d458d350_0 E0x7f49d458d410)
                a!1
                a!2
                (=> v0x7f49d458e4d0_0
                    (and v0x7f49d458da90_0 E0x7f49d458e590 v0x7f49d458e390_0))
                (=> v0x7f49d458e4d0_0 E0x7f49d458e590)
                (=> v0x7f49d458e9d0_0
                    (and v0x7f49d458da90_0
                         E0x7f49d458ea90
                         (not v0x7f49d458e390_0)))
                (=> v0x7f49d458e9d0_0 E0x7f49d458ea90)
                (=> v0x7f49d458f210_0
                    (and v0x7f49d458e9d0_0 E0x7f49d458f2d0 v0x7f49d458f0d0_0))
                (=> v0x7f49d458f210_0 E0x7f49d458f2d0)
                (=> v0x7f49d458f490_0 a!3)
                a!4
                v0x7f49d458f490_0
                (not v0x7f49d4590410_0)
                (= v0x7f49d458d210_0 (= v0x7f49d458d150_0 0.0))
                (= v0x7f49d458d650_0 (< v0x7f49d458cc10_0 2.0))
                (= v0x7f49d458d810_0 (ite v0x7f49d458d650_0 1.0 0.0))
                (= v0x7f49d458d950_0 (+ v0x7f49d458d810_0 v0x7f49d458cc10_0))
                (= v0x7f49d458e390_0 (= v0x7f49d458ca90_0 0.0))
                (= v0x7f49d458e750_0 (> v0x7f49d458db50_0 1.0))
                (= v0x7f49d458e890_0
                   (ite v0x7f49d458e750_0 1.0 v0x7f49d458ca90_0))
                (= v0x7f49d458ec90_0 (> v0x7f49d458db50_0 0.0))
                (= v0x7f49d458edd0_0 (+ v0x7f49d458db50_0 (- 1.0)))
                (= v0x7f49d458ef90_0
                   (ite v0x7f49d458ec90_0 v0x7f49d458edd0_0 v0x7f49d458db50_0))
                (= v0x7f49d458f0d0_0 (= v0x7f49d458ef90_0 0.0))
                (= v0x7f49d4590190_0 (not (= v0x7f49d458f550_0 0.0)))
                (= v0x7f49d45902d0_0 (= v0x7f49d458f610_0 0.0))
                (= v0x7f49d4590410_0 (or v0x7f49d45902d0_0 v0x7f49d4590190_0)))))
  (=> F0x7f49d4591110 a!5))))
(assert (=> F0x7f49d4591110 F0x7f49d4591050))
(assert (=> F0x7f49d4591250 (or F0x7f49d4590f50 F0x7f49d4590f90)))
(assert (=> F0x7f49d4591210 F0x7f49d4591110))
(assert (=> pre!entry!0 (=> F0x7f49d4590e90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f49d4591050 (>= v0x7f49d458ca90_0 0.0))))
(assert (let ((a!1 (=> F0x7f49d4591050
               (or (<= v0x7f49d458ca90_0 0.0) (not (<= v0x7f49d458cc10_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7f49d458ccd0_0 0.0) (not (<= v0x7f49d458b010_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f49d4591250
           (not (>= v0x7f49d458ccd0_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f49d4591250 a!1)
      (and (not post!bb1.i.i26.i.i!0) F0x7f49d4591210 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i26.i.i!0)
