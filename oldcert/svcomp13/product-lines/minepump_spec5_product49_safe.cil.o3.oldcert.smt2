(declare-fun post!bb2.i.i28.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f26b34bd610 () Bool)
(declare-fun v0x7f26b34bc590_0 () Bool)
(declare-fun F0x7f26b34bd290 () Bool)
(declare-fun v0x7f26b34bc450_0 () Bool)
(declare-fun v0x7f26b34bc310_0 () Bool)
(declare-fun v0x7f26b34bae10_0 () Bool)
(declare-fun v0x7f26b34b9990_0 () Real)
(declare-fun v0x7f26b34b97d0_0 () Bool)
(declare-fun v0x7f26b34bc810_0 () Bool)
(declare-fun v0x7f26b34bb790_0 () Real)
(declare-fun E0x7f26b34bb850 () Bool)
(declare-fun v0x7f26b34bb610_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f26b34bb250_0 () Bool)
(declare-fun E0x7f26b34bb450 () Bool)
(declare-fun v0x7f26b34b8d90_0 () Real)
(declare-fun v0x7f26b34ba8d0_0 () Bool)
(declare-fun v0x7f26b34bb390_0 () Bool)
(declare-fun E0x7f26b34bac10 () Bool)
(declare-fun v0x7f26b34bab50_0 () Bool)
(declare-fun E0x7f26b34bbdd0 () Bool)
(declare-fun v0x7f26b34baa10_0 () Real)
(declare-fun v0x7f26b34ba510_0 () Bool)
(declare-fun F0x7f26b34bd350 () Bool)
(declare-fun v0x7f26b34bc6d0_0 () Bool)
(declare-fun v0x7f26b34ba650_0 () Bool)
(declare-fun E0x7f26b34b9f50 () Bool)
(declare-fun E0x7f26b34b9d90 () Bool)
(declare-fun v0x7f26b34b9c10_0 () Bool)
(declare-fun v0x7f26b34b9390_0 () Bool)
(declare-fun v0x7f26b34b9cd0_0 () Real)
(declare-fun v0x7f26b34b9210_0 () Bool)
(declare-fun F0x7f26b34bd650 () Bool)
(declare-fun v0x7f26b34b92d0_0 () Real)
(declare-fun E0x7f26b34bbb10 () Bool)
(declare-fun v0x7f26b34b94d0_0 () Bool)
(declare-fun F0x7f26b34bd410 () Bool)
(declare-fun E0x7f26b34ba710 () Bool)
(declare-fun v0x7f26b34b8c10_0 () Real)
(declare-fun E0x7f26b34b9590 () Bool)
(declare-fun F0x7f26b34bd4d0 () Bool)
(declare-fun v0x7f26b34b9ad0_0 () Real)
(declare-fun v0x7f26b34bb6d0_0 () Real)
(declare-fun v0x7f26b34bb110_0 () Real)
(declare-fun v0x7f26b34baf50_0 () Real)
(declare-fun v0x7f26b34b7010_0 () Real)
(declare-fun v0x7f26b34b8e50_0 () Real)
(declare-fun v0x7f26b34b7110_0 () Bool)
(declare-fun F0x7f26b34bd590 () Bool)

(assert (=> F0x7f26b34bd590
    (and v0x7f26b34b7110_0
         (<= v0x7f26b34b8e50_0 1.0)
         (>= v0x7f26b34b8e50_0 1.0)
         (<= v0x7f26b34b7010_0 0.0)
         (>= v0x7f26b34b7010_0 0.0))))
(assert (=> F0x7f26b34bd590 F0x7f26b34bd4d0))
(assert (let ((a!1 (=> v0x7f26b34b9c10_0
               (or (and v0x7f26b34b94d0_0
                        E0x7f26b34b9d90
                        (<= v0x7f26b34b9cd0_0 v0x7f26b34b9ad0_0)
                        (>= v0x7f26b34b9cd0_0 v0x7f26b34b9ad0_0))
                   (and v0x7f26b34b9210_0
                        E0x7f26b34b9f50
                        v0x7f26b34b9390_0
                        (<= v0x7f26b34b9cd0_0 v0x7f26b34b8c10_0)
                        (>= v0x7f26b34b9cd0_0 v0x7f26b34b8c10_0)))))
      (a!2 (=> v0x7f26b34b9c10_0
               (or (and E0x7f26b34b9d90 (not E0x7f26b34b9f50))
                   (and E0x7f26b34b9f50 (not E0x7f26b34b9d90)))))
      (a!3 (or (and v0x7f26b34ba650_0
                    E0x7f26b34bb850
                    (<= v0x7f26b34bb6d0_0 v0x7f26b34b9cd0_0)
                    (>= v0x7f26b34bb6d0_0 v0x7f26b34b9cd0_0)
                    (<= v0x7f26b34bb790_0 v0x7f26b34baa10_0)
                    (>= v0x7f26b34bb790_0 v0x7f26b34baa10_0))
               (and v0x7f26b34bb390_0
                    E0x7f26b34bbb10
                    (and (<= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0)
                         (>= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0))
                    (<= v0x7f26b34bb790_0 v0x7f26b34b8d90_0)
                    (>= v0x7f26b34bb790_0 v0x7f26b34b8d90_0))
               (and v0x7f26b34bab50_0
                    E0x7f26b34bbdd0
                    (not v0x7f26b34bb250_0)
                    (and (<= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0)
                         (>= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0))
                    (<= v0x7f26b34bb790_0 0.0)
                    (>= v0x7f26b34bb790_0 0.0))))
      (a!4 (=> v0x7f26b34bb610_0
               (or (and E0x7f26b34bb850
                        (not E0x7f26b34bbb10)
                        (not E0x7f26b34bbdd0))
                   (and E0x7f26b34bbb10
                        (not E0x7f26b34bb850)
                        (not E0x7f26b34bbdd0))
                   (and E0x7f26b34bbdd0
                        (not E0x7f26b34bb850)
                        (not E0x7f26b34bbb10))))))
(let ((a!5 (and (=> v0x7f26b34b94d0_0
                    (and v0x7f26b34b9210_0
                         E0x7f26b34b9590
                         (not v0x7f26b34b9390_0)))
                (=> v0x7f26b34b94d0_0 E0x7f26b34b9590)
                a!1
                a!2
                (=> v0x7f26b34ba650_0
                    (and v0x7f26b34b9c10_0 E0x7f26b34ba710 v0x7f26b34ba510_0))
                (=> v0x7f26b34ba650_0 E0x7f26b34ba710)
                (=> v0x7f26b34bab50_0
                    (and v0x7f26b34b9c10_0
                         E0x7f26b34bac10
                         (not v0x7f26b34ba510_0)))
                (=> v0x7f26b34bab50_0 E0x7f26b34bac10)
                (=> v0x7f26b34bb390_0
                    (and v0x7f26b34bab50_0 E0x7f26b34bb450 v0x7f26b34bb250_0))
                (=> v0x7f26b34bb390_0 E0x7f26b34bb450)
                (=> v0x7f26b34bb610_0 a!3)
                a!4
                v0x7f26b34bb610_0
                (not v0x7f26b34bc810_0)
                (<= v0x7f26b34b8e50_0 v0x7f26b34bb6d0_0)
                (>= v0x7f26b34b8e50_0 v0x7f26b34bb6d0_0)
                (<= v0x7f26b34b7010_0 v0x7f26b34bb790_0)
                (>= v0x7f26b34b7010_0 v0x7f26b34bb790_0)
                (= v0x7f26b34b9390_0 (= v0x7f26b34b92d0_0 0.0))
                (= v0x7f26b34b97d0_0 (< v0x7f26b34b8c10_0 2.0))
                (= v0x7f26b34b9990_0 (ite v0x7f26b34b97d0_0 1.0 0.0))
                (= v0x7f26b34b9ad0_0 (+ v0x7f26b34b9990_0 v0x7f26b34b8c10_0))
                (= v0x7f26b34ba510_0 (= v0x7f26b34b8d90_0 0.0))
                (= v0x7f26b34ba8d0_0 (> v0x7f26b34b9cd0_0 1.0))
                (= v0x7f26b34baa10_0
                   (ite v0x7f26b34ba8d0_0 1.0 v0x7f26b34b8d90_0))
                (= v0x7f26b34bae10_0 (> v0x7f26b34b9cd0_0 0.0))
                (= v0x7f26b34baf50_0 (+ v0x7f26b34b9cd0_0 (- 1.0)))
                (= v0x7f26b34bb110_0
                   (ite v0x7f26b34bae10_0 v0x7f26b34baf50_0 v0x7f26b34b9cd0_0))
                (= v0x7f26b34bb250_0 (= v0x7f26b34bb110_0 0.0))
                (= v0x7f26b34bc310_0 (= v0x7f26b34bb6d0_0 2.0))
                (= v0x7f26b34bc450_0 (= v0x7f26b34bb790_0 0.0))
                (= v0x7f26b34bc590_0 (or v0x7f26b34bc450_0 v0x7f26b34bc310_0))
                (= v0x7f26b34bc6d0_0 (xor v0x7f26b34bc590_0 true))
                (= v0x7f26b34bc810_0 (and v0x7f26b34ba510_0 v0x7f26b34bc6d0_0)))))
  (=> F0x7f26b34bd410 a!5))))
(assert (=> F0x7f26b34bd410 F0x7f26b34bd350))
(assert (let ((a!1 (=> v0x7f26b34b9c10_0
               (or (and v0x7f26b34b94d0_0
                        E0x7f26b34b9d90
                        (<= v0x7f26b34b9cd0_0 v0x7f26b34b9ad0_0)
                        (>= v0x7f26b34b9cd0_0 v0x7f26b34b9ad0_0))
                   (and v0x7f26b34b9210_0
                        E0x7f26b34b9f50
                        v0x7f26b34b9390_0
                        (<= v0x7f26b34b9cd0_0 v0x7f26b34b8c10_0)
                        (>= v0x7f26b34b9cd0_0 v0x7f26b34b8c10_0)))))
      (a!2 (=> v0x7f26b34b9c10_0
               (or (and E0x7f26b34b9d90 (not E0x7f26b34b9f50))
                   (and E0x7f26b34b9f50 (not E0x7f26b34b9d90)))))
      (a!3 (or (and v0x7f26b34ba650_0
                    E0x7f26b34bb850
                    (<= v0x7f26b34bb6d0_0 v0x7f26b34b9cd0_0)
                    (>= v0x7f26b34bb6d0_0 v0x7f26b34b9cd0_0)
                    (<= v0x7f26b34bb790_0 v0x7f26b34baa10_0)
                    (>= v0x7f26b34bb790_0 v0x7f26b34baa10_0))
               (and v0x7f26b34bb390_0
                    E0x7f26b34bbb10
                    (and (<= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0)
                         (>= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0))
                    (<= v0x7f26b34bb790_0 v0x7f26b34b8d90_0)
                    (>= v0x7f26b34bb790_0 v0x7f26b34b8d90_0))
               (and v0x7f26b34bab50_0
                    E0x7f26b34bbdd0
                    (not v0x7f26b34bb250_0)
                    (and (<= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0)
                         (>= v0x7f26b34bb6d0_0 v0x7f26b34bb110_0))
                    (<= v0x7f26b34bb790_0 0.0)
                    (>= v0x7f26b34bb790_0 0.0))))
      (a!4 (=> v0x7f26b34bb610_0
               (or (and E0x7f26b34bb850
                        (not E0x7f26b34bbb10)
                        (not E0x7f26b34bbdd0))
                   (and E0x7f26b34bbb10
                        (not E0x7f26b34bb850)
                        (not E0x7f26b34bbdd0))
                   (and E0x7f26b34bbdd0
                        (not E0x7f26b34bb850)
                        (not E0x7f26b34bbb10))))))
(let ((a!5 (and (=> v0x7f26b34b94d0_0
                    (and v0x7f26b34b9210_0
                         E0x7f26b34b9590
                         (not v0x7f26b34b9390_0)))
                (=> v0x7f26b34b94d0_0 E0x7f26b34b9590)
                a!1
                a!2
                (=> v0x7f26b34ba650_0
                    (and v0x7f26b34b9c10_0 E0x7f26b34ba710 v0x7f26b34ba510_0))
                (=> v0x7f26b34ba650_0 E0x7f26b34ba710)
                (=> v0x7f26b34bab50_0
                    (and v0x7f26b34b9c10_0
                         E0x7f26b34bac10
                         (not v0x7f26b34ba510_0)))
                (=> v0x7f26b34bab50_0 E0x7f26b34bac10)
                (=> v0x7f26b34bb390_0
                    (and v0x7f26b34bab50_0 E0x7f26b34bb450 v0x7f26b34bb250_0))
                (=> v0x7f26b34bb390_0 E0x7f26b34bb450)
                (=> v0x7f26b34bb610_0 a!3)
                a!4
                v0x7f26b34bb610_0
                v0x7f26b34bc810_0
                (= v0x7f26b34b9390_0 (= v0x7f26b34b92d0_0 0.0))
                (= v0x7f26b34b97d0_0 (< v0x7f26b34b8c10_0 2.0))
                (= v0x7f26b34b9990_0 (ite v0x7f26b34b97d0_0 1.0 0.0))
                (= v0x7f26b34b9ad0_0 (+ v0x7f26b34b9990_0 v0x7f26b34b8c10_0))
                (= v0x7f26b34ba510_0 (= v0x7f26b34b8d90_0 0.0))
                (= v0x7f26b34ba8d0_0 (> v0x7f26b34b9cd0_0 1.0))
                (= v0x7f26b34baa10_0
                   (ite v0x7f26b34ba8d0_0 1.0 v0x7f26b34b8d90_0))
                (= v0x7f26b34bae10_0 (> v0x7f26b34b9cd0_0 0.0))
                (= v0x7f26b34baf50_0 (+ v0x7f26b34b9cd0_0 (- 1.0)))
                (= v0x7f26b34bb110_0
                   (ite v0x7f26b34bae10_0 v0x7f26b34baf50_0 v0x7f26b34b9cd0_0))
                (= v0x7f26b34bb250_0 (= v0x7f26b34bb110_0 0.0))
                (= v0x7f26b34bc310_0 (= v0x7f26b34bb6d0_0 2.0))
                (= v0x7f26b34bc450_0 (= v0x7f26b34bb790_0 0.0))
                (= v0x7f26b34bc590_0 (or v0x7f26b34bc450_0 v0x7f26b34bc310_0))
                (= v0x7f26b34bc6d0_0 (xor v0x7f26b34bc590_0 true))
                (= v0x7f26b34bc810_0 (and v0x7f26b34ba510_0 v0x7f26b34bc6d0_0)))))
  (=> F0x7f26b34bd290 a!5))))
(assert (=> F0x7f26b34bd290 F0x7f26b34bd350))
(assert (=> F0x7f26b34bd650 (or F0x7f26b34bd590 F0x7f26b34bd410)))
(assert (=> F0x7f26b34bd610 F0x7f26b34bd290))
(assert (=> pre!entry!0 (=> F0x7f26b34bd4d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f26b34bd350 (<= v0x7f26b34b8c10_0 2.0))))
(assert (let ((a!1 (not (or (not (>= v0x7f26b34b8d90_0 0.0))
                    (not (<= v0x7f26b34b8c10_0 1.0))
                    (not (<= v0x7f26b34b8d90_0 0.0))
                    (not (>= v0x7f26b34b8c10_0 1.0))))))
  (=> pre!bb1.i.i!1 (=> F0x7f26b34bd350 (or (>= v0x7f26b34b8c10_0 2.0) a!1)))))
(assert (let ((a!1 (not (or (not (>= v0x7f26b34b7010_0 0.0))
                    (not (<= v0x7f26b34b8e50_0 1.0))
                    (not (<= v0x7f26b34b7010_0 0.0))
                    (not (>= v0x7f26b34b8e50_0 1.0))))))
(let ((a!2 (and (not post!bb1.i.i!1)
                F0x7f26b34bd650
                (not (or (>= v0x7f26b34b8e50_0 2.0) a!1)))))
  (or (and (not post!bb1.i.i!0)
           F0x7f26b34bd650
           (not (<= v0x7f26b34b8e50_0 2.0)))
      a!2
      (and (not post!bb2.i.i28.i.i!0) F0x7f26b34bd610 true)))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i28.i.i!0)
