(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f9092b23350 () Bool)
(declare-fun F0x7f9092b23390 () Bool)
(declare-fun F0x7f9092b23250 () Bool)
(declare-fun F0x7f9092b23190 () Bool)
(declare-fun v0x7f9092b20850_0 () Bool)
(declare-fun v0x7f9092b20550_0 () Real)
(declare-fun v0x7f9092b20410_0 () Bool)
(declare-fun v0x7f9092b1eb90_0 () Real)
(declare-fun v0x7f9092b1e190_0 () Real)
(declare-fun v0x7f9092b1dfd0_0 () Bool)
(declare-fun v0x7f9092b1dad0_0 () Real)
(declare-fun v0x7f9092b21dd0_0 () Bool)
(declare-fun E0x7f9092b21650 () Bool)
(declare-fun v0x7f9092b21010_0 () Real)
(declare-fun v0x7f9092b20f50_0 () Real)
(declare-fun E0x7f9092b210d0 () Bool)
(declare-fun E0x7f9092b20210 () Bool)
(declare-fun v0x7f9092b1fb10_0 () Bool)
(declare-fun v0x7f9092b1fc50_0 () Bool)
(declare-fun E0x7f9092b21390 () Bool)
(declare-fun v0x7f9092b21f10_0 () Bool)
(declare-fun v0x7f9092b1f050_0 () Bool)
(declare-fun E0x7f9092b1f610 () Bool)
(declare-fun v0x7f9092b1f190_0 () Real)
(declare-fun v0x7f9092b20150_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f9092b1f2d0_0 () Bool)
(declare-fun v0x7f9092b1f390_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f9092b1cf90_0 () Real)
(declare-fun v0x7f9092b1ec50_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7f9092b1ee50 () Bool)
(declare-fun E0x7f9092b20cd0 () Bool)
(declare-fun v0x7f9092b1ed90_0 () Bool)
(declare-fun v0x7f9092b1d110_0 () Real)
(declare-fun v0x7f9092b21c90_0 () Bool)
(declare-fun v0x7f9092b20e90_0 () Bool)
(declare-fun E0x7f9092b1e750 () Bool)
(declare-fun v0x7f9092b1fed0_0 () Bool)
(declare-fun v0x7f9092b1e4d0_0 () Real)
(declare-fun E0x7f9092b21fd0 () Bool)
(declare-fun E0x7f9092b1e590 () Bool)
(declare-fun v0x7f9092b21b90_0 () Bool)
(declare-fun v0x7f9092b1da10_0 () Bool)
(declare-fun v0x7f9092b1d210_0 () Real)
(declare-fun v0x7f9092b1dcd0_0 () Bool)
(declare-fun F0x7f9092b230d0 () Bool)
(declare-fun v0x7f9092b20710_0 () Real)
(declare-fun v0x7f9092b20c10_0 () Bool)
(declare-fun v0x7f9092b221d0_0 () Bool)
(declare-fun F0x7f9092b22fd0 () Bool)
(declare-fun v0x7f9092b1d3d0_0 () Real)
(declare-fun v0x7f9092b1db90_0 () Bool)
(declare-fun v0x7f9092b1d2d0_0 () Real)
(declare-fun v0x7f9092b1d1d0_0 () Real)
(declare-fun v0x7f9092b1d310_0 () Real)
(declare-fun v0x7f9092b1e2d0_0 () Real)
(declare-fun E0x7f9092b1dd90 () Bool)
(declare-fun v0x7f9092b1e410_0 () Bool)
(declare-fun v0x7f9092b1a110_0 () Bool)
(declare-fun v0x7f9092b20990_0 () Bool)
(declare-fun v0x7f9092b1a010_0 () Real)
(declare-fun v0x7f9092b20010_0 () Real)
(declare-fun E0x7f9092b1fd10 () Bool)
(declare-fun E0x7f9092b1f450 () Bool)
(declare-fun F0x7f9092b23090 () Bool)
(declare-fun v0x7f9092b20ad0_0 () Bool)

(assert (=> F0x7f9092b23090
    (and v0x7f9092b1a110_0
         (<= v0x7f9092b1d1d0_0 0.0)
         (>= v0x7f9092b1d1d0_0 0.0)
         (<= v0x7f9092b1d2d0_0 1.0)
         (>= v0x7f9092b1d2d0_0 1.0)
         (<= v0x7f9092b1d3d0_0 0.0)
         (>= v0x7f9092b1d3d0_0 0.0)
         (<= v0x7f9092b1a010_0 0.0)
         (>= v0x7f9092b1a010_0 0.0))))
(assert (=> F0x7f9092b23090 F0x7f9092b22fd0))
(assert (let ((a!1 (=> v0x7f9092b1e410_0
               (or (and v0x7f9092b1dcd0_0
                        E0x7f9092b1e590
                        (<= v0x7f9092b1e4d0_0 v0x7f9092b1e2d0_0)
                        (>= v0x7f9092b1e4d0_0 v0x7f9092b1e2d0_0))
                   (and v0x7f9092b1da10_0
                        E0x7f9092b1e750
                        v0x7f9092b1db90_0
                        (<= v0x7f9092b1e4d0_0 v0x7f9092b1d110_0)
                        (>= v0x7f9092b1e4d0_0 v0x7f9092b1d110_0)))))
      (a!2 (=> v0x7f9092b1e410_0
               (or (and E0x7f9092b1e590 (not E0x7f9092b1e750))
                   (and E0x7f9092b1e750 (not E0x7f9092b1e590)))))
      (a!3 (=> v0x7f9092b1f2d0_0
               (or (and v0x7f9092b1ed90_0
                        E0x7f9092b1f450
                        (<= v0x7f9092b1f390_0 v0x7f9092b1f190_0)
                        (>= v0x7f9092b1f390_0 v0x7f9092b1f190_0))
                   (and v0x7f9092b1e410_0
                        E0x7f9092b1f610
                        v0x7f9092b1ec50_0
                        (<= v0x7f9092b1f390_0 v0x7f9092b1cf90_0)
                        (>= v0x7f9092b1f390_0 v0x7f9092b1cf90_0)))))
      (a!4 (=> v0x7f9092b1f2d0_0
               (or (and E0x7f9092b1f450 (not E0x7f9092b1f610))
                   (and E0x7f9092b1f610 (not E0x7f9092b1f450)))))
      (a!5 (or (and v0x7f9092b1fc50_0
                    E0x7f9092b210d0
                    (<= v0x7f9092b20f50_0 v0x7f9092b1e4d0_0)
                    (>= v0x7f9092b20f50_0 v0x7f9092b1e4d0_0)
                    (<= v0x7f9092b21010_0 v0x7f9092b20010_0)
                    (>= v0x7f9092b21010_0 v0x7f9092b20010_0))
               (and v0x7f9092b20c10_0
                    E0x7f9092b21390
                    (and (<= v0x7f9092b20f50_0 v0x7f9092b20710_0)
                         (>= v0x7f9092b20f50_0 v0x7f9092b20710_0))
                    (<= v0x7f9092b21010_0 v0x7f9092b1d310_0)
                    (>= v0x7f9092b21010_0 v0x7f9092b1d310_0))
               (and v0x7f9092b20150_0
                    E0x7f9092b21650
                    (not v0x7f9092b20ad0_0)
                    (and (<= v0x7f9092b20f50_0 v0x7f9092b20710_0)
                         (>= v0x7f9092b20f50_0 v0x7f9092b20710_0))
                    (<= v0x7f9092b21010_0 0.0)
                    (>= v0x7f9092b21010_0 0.0))))
      (a!6 (=> v0x7f9092b20e90_0
               (or (and E0x7f9092b210d0
                        (not E0x7f9092b21390)
                        (not E0x7f9092b21650))
                   (and E0x7f9092b21390
                        (not E0x7f9092b210d0)
                        (not E0x7f9092b21650))
                   (and E0x7f9092b21650
                        (not E0x7f9092b210d0)
                        (not E0x7f9092b21390)))))
      (a!7 (or (and v0x7f9092b21f10_0
                    v0x7f9092b221d0_0
                    (and (<= v0x7f9092b1d1d0_0 v0x7f9092b1f390_0)
                         (>= v0x7f9092b1d1d0_0 v0x7f9092b1f390_0))
                    (and (<= v0x7f9092b1d2d0_0 v0x7f9092b20f50_0)
                         (>= v0x7f9092b1d2d0_0 v0x7f9092b20f50_0))
                    (<= v0x7f9092b1d3d0_0 1.0)
                    (>= v0x7f9092b1d3d0_0 1.0)
                    (and (<= v0x7f9092b1a010_0 v0x7f9092b21010_0)
                         (>= v0x7f9092b1a010_0 v0x7f9092b21010_0)))
               (and v0x7f9092b20e90_0
                    v0x7f9092b21dd0_0
                    (and (<= v0x7f9092b1d1d0_0 v0x7f9092b1f390_0)
                         (>= v0x7f9092b1d1d0_0 v0x7f9092b1f390_0))
                    (and (<= v0x7f9092b1d2d0_0 v0x7f9092b20f50_0)
                         (>= v0x7f9092b1d2d0_0 v0x7f9092b20f50_0))
                    (<= v0x7f9092b1d3d0_0 0.0)
                    (>= v0x7f9092b1d3d0_0 0.0)
                    (and (<= v0x7f9092b1a010_0 v0x7f9092b21010_0)
                         (>= v0x7f9092b1a010_0 v0x7f9092b21010_0))))))
(let ((a!8 (and (=> v0x7f9092b1dcd0_0
                    (and v0x7f9092b1da10_0
                         E0x7f9092b1dd90
                         (not v0x7f9092b1db90_0)))
                (=> v0x7f9092b1dcd0_0 E0x7f9092b1dd90)
                a!1
                a!2
                (=> v0x7f9092b1ed90_0
                    (and v0x7f9092b1e410_0
                         E0x7f9092b1ee50
                         (not v0x7f9092b1ec50_0)))
                (=> v0x7f9092b1ed90_0 E0x7f9092b1ee50)
                a!3
                a!4
                (=> v0x7f9092b1fc50_0
                    (and v0x7f9092b1f2d0_0 E0x7f9092b1fd10 v0x7f9092b1fb10_0))
                (=> v0x7f9092b1fc50_0 E0x7f9092b1fd10)
                (=> v0x7f9092b20150_0
                    (and v0x7f9092b1f2d0_0
                         E0x7f9092b20210
                         (not v0x7f9092b1fb10_0)))
                (=> v0x7f9092b20150_0 E0x7f9092b20210)
                (=> v0x7f9092b20c10_0
                    (and v0x7f9092b20150_0 E0x7f9092b20cd0 v0x7f9092b20ad0_0))
                (=> v0x7f9092b20c10_0 E0x7f9092b20cd0)
                (=> v0x7f9092b20e90_0 a!5)
                a!6
                (=> v0x7f9092b21f10_0
                    (and v0x7f9092b20e90_0
                         E0x7f9092b21fd0
                         (not v0x7f9092b21dd0_0)))
                (=> v0x7f9092b21f10_0 E0x7f9092b21fd0)
                a!7
                (= v0x7f9092b1db90_0 (= v0x7f9092b1dad0_0 0.0))
                (= v0x7f9092b1dfd0_0 (< v0x7f9092b1d110_0 2.0))
                (= v0x7f9092b1e190_0 (ite v0x7f9092b1dfd0_0 1.0 0.0))
                (= v0x7f9092b1e2d0_0 (+ v0x7f9092b1e190_0 v0x7f9092b1d110_0))
                (= v0x7f9092b1ec50_0 (= v0x7f9092b1eb90_0 0.0))
                (= v0x7f9092b1f050_0 (= v0x7f9092b1cf90_0 0.0))
                (= v0x7f9092b1f190_0 (ite v0x7f9092b1f050_0 1.0 0.0))
                (= v0x7f9092b1fb10_0 (= v0x7f9092b1d310_0 0.0))
                (= v0x7f9092b1fed0_0 (> v0x7f9092b1e4d0_0 1.0))
                (= v0x7f9092b20010_0
                   (ite v0x7f9092b1fed0_0 1.0 v0x7f9092b1d310_0))
                (= v0x7f9092b20410_0 (> v0x7f9092b1e4d0_0 0.0))
                (= v0x7f9092b20550_0 (+ v0x7f9092b1e4d0_0 (- 1.0)))
                (= v0x7f9092b20710_0
                   (ite v0x7f9092b20410_0 v0x7f9092b20550_0 v0x7f9092b1e4d0_0))
                (= v0x7f9092b20850_0 (= v0x7f9092b1f390_0 0.0))
                (= v0x7f9092b20990_0 (= v0x7f9092b20710_0 0.0))
                (= v0x7f9092b20ad0_0 (and v0x7f9092b20850_0 v0x7f9092b20990_0))
                (= v0x7f9092b21b90_0 (= v0x7f9092b1f390_0 0.0))
                (= v0x7f9092b21c90_0 (= v0x7f9092b21010_0 0.0))
                (= v0x7f9092b21dd0_0 (or v0x7f9092b21c90_0 v0x7f9092b21b90_0))
                (= v0x7f9092b221d0_0 (= v0x7f9092b1d210_0 0.0)))))
  (=> F0x7f9092b230d0 a!8))))
(assert (=> F0x7f9092b230d0 F0x7f9092b23190))
(assert (let ((a!1 (=> v0x7f9092b1e410_0
               (or (and v0x7f9092b1dcd0_0
                        E0x7f9092b1e590
                        (<= v0x7f9092b1e4d0_0 v0x7f9092b1e2d0_0)
                        (>= v0x7f9092b1e4d0_0 v0x7f9092b1e2d0_0))
                   (and v0x7f9092b1da10_0
                        E0x7f9092b1e750
                        v0x7f9092b1db90_0
                        (<= v0x7f9092b1e4d0_0 v0x7f9092b1d110_0)
                        (>= v0x7f9092b1e4d0_0 v0x7f9092b1d110_0)))))
      (a!2 (=> v0x7f9092b1e410_0
               (or (and E0x7f9092b1e590 (not E0x7f9092b1e750))
                   (and E0x7f9092b1e750 (not E0x7f9092b1e590)))))
      (a!3 (=> v0x7f9092b1f2d0_0
               (or (and v0x7f9092b1ed90_0
                        E0x7f9092b1f450
                        (<= v0x7f9092b1f390_0 v0x7f9092b1f190_0)
                        (>= v0x7f9092b1f390_0 v0x7f9092b1f190_0))
                   (and v0x7f9092b1e410_0
                        E0x7f9092b1f610
                        v0x7f9092b1ec50_0
                        (<= v0x7f9092b1f390_0 v0x7f9092b1cf90_0)
                        (>= v0x7f9092b1f390_0 v0x7f9092b1cf90_0)))))
      (a!4 (=> v0x7f9092b1f2d0_0
               (or (and E0x7f9092b1f450 (not E0x7f9092b1f610))
                   (and E0x7f9092b1f610 (not E0x7f9092b1f450)))))
      (a!5 (or (and v0x7f9092b1fc50_0
                    E0x7f9092b210d0
                    (<= v0x7f9092b20f50_0 v0x7f9092b1e4d0_0)
                    (>= v0x7f9092b20f50_0 v0x7f9092b1e4d0_0)
                    (<= v0x7f9092b21010_0 v0x7f9092b20010_0)
                    (>= v0x7f9092b21010_0 v0x7f9092b20010_0))
               (and v0x7f9092b20c10_0
                    E0x7f9092b21390
                    (and (<= v0x7f9092b20f50_0 v0x7f9092b20710_0)
                         (>= v0x7f9092b20f50_0 v0x7f9092b20710_0))
                    (<= v0x7f9092b21010_0 v0x7f9092b1d310_0)
                    (>= v0x7f9092b21010_0 v0x7f9092b1d310_0))
               (and v0x7f9092b20150_0
                    E0x7f9092b21650
                    (not v0x7f9092b20ad0_0)
                    (and (<= v0x7f9092b20f50_0 v0x7f9092b20710_0)
                         (>= v0x7f9092b20f50_0 v0x7f9092b20710_0))
                    (<= v0x7f9092b21010_0 0.0)
                    (>= v0x7f9092b21010_0 0.0))))
      (a!6 (=> v0x7f9092b20e90_0
               (or (and E0x7f9092b210d0
                        (not E0x7f9092b21390)
                        (not E0x7f9092b21650))
                   (and E0x7f9092b21390
                        (not E0x7f9092b210d0)
                        (not E0x7f9092b21650))
                   (and E0x7f9092b21650
                        (not E0x7f9092b210d0)
                        (not E0x7f9092b21390))))))
(let ((a!7 (and (=> v0x7f9092b1dcd0_0
                    (and v0x7f9092b1da10_0
                         E0x7f9092b1dd90
                         (not v0x7f9092b1db90_0)))
                (=> v0x7f9092b1dcd0_0 E0x7f9092b1dd90)
                a!1
                a!2
                (=> v0x7f9092b1ed90_0
                    (and v0x7f9092b1e410_0
                         E0x7f9092b1ee50
                         (not v0x7f9092b1ec50_0)))
                (=> v0x7f9092b1ed90_0 E0x7f9092b1ee50)
                a!3
                a!4
                (=> v0x7f9092b1fc50_0
                    (and v0x7f9092b1f2d0_0 E0x7f9092b1fd10 v0x7f9092b1fb10_0))
                (=> v0x7f9092b1fc50_0 E0x7f9092b1fd10)
                (=> v0x7f9092b20150_0
                    (and v0x7f9092b1f2d0_0
                         E0x7f9092b20210
                         (not v0x7f9092b1fb10_0)))
                (=> v0x7f9092b20150_0 E0x7f9092b20210)
                (=> v0x7f9092b20c10_0
                    (and v0x7f9092b20150_0 E0x7f9092b20cd0 v0x7f9092b20ad0_0))
                (=> v0x7f9092b20c10_0 E0x7f9092b20cd0)
                (=> v0x7f9092b20e90_0 a!5)
                a!6
                (=> v0x7f9092b21f10_0
                    (and v0x7f9092b20e90_0
                         E0x7f9092b21fd0
                         (not v0x7f9092b21dd0_0)))
                (=> v0x7f9092b21f10_0 E0x7f9092b21fd0)
                v0x7f9092b21f10_0
                (not v0x7f9092b221d0_0)
                (= v0x7f9092b1db90_0 (= v0x7f9092b1dad0_0 0.0))
                (= v0x7f9092b1dfd0_0 (< v0x7f9092b1d110_0 2.0))
                (= v0x7f9092b1e190_0 (ite v0x7f9092b1dfd0_0 1.0 0.0))
                (= v0x7f9092b1e2d0_0 (+ v0x7f9092b1e190_0 v0x7f9092b1d110_0))
                (= v0x7f9092b1ec50_0 (= v0x7f9092b1eb90_0 0.0))
                (= v0x7f9092b1f050_0 (= v0x7f9092b1cf90_0 0.0))
                (= v0x7f9092b1f190_0 (ite v0x7f9092b1f050_0 1.0 0.0))
                (= v0x7f9092b1fb10_0 (= v0x7f9092b1d310_0 0.0))
                (= v0x7f9092b1fed0_0 (> v0x7f9092b1e4d0_0 1.0))
                (= v0x7f9092b20010_0
                   (ite v0x7f9092b1fed0_0 1.0 v0x7f9092b1d310_0))
                (= v0x7f9092b20410_0 (> v0x7f9092b1e4d0_0 0.0))
                (= v0x7f9092b20550_0 (+ v0x7f9092b1e4d0_0 (- 1.0)))
                (= v0x7f9092b20710_0
                   (ite v0x7f9092b20410_0 v0x7f9092b20550_0 v0x7f9092b1e4d0_0))
                (= v0x7f9092b20850_0 (= v0x7f9092b1f390_0 0.0))
                (= v0x7f9092b20990_0 (= v0x7f9092b20710_0 0.0))
                (= v0x7f9092b20ad0_0 (and v0x7f9092b20850_0 v0x7f9092b20990_0))
                (= v0x7f9092b21b90_0 (= v0x7f9092b1f390_0 0.0))
                (= v0x7f9092b21c90_0 (= v0x7f9092b21010_0 0.0))
                (= v0x7f9092b21dd0_0 (or v0x7f9092b21c90_0 v0x7f9092b21b90_0))
                (= v0x7f9092b221d0_0 (= v0x7f9092b1d210_0 0.0)))))
  (=> F0x7f9092b23250 a!7))))
(assert (=> F0x7f9092b23250 F0x7f9092b23190))
(assert (=> F0x7f9092b23390 (or F0x7f9092b23090 F0x7f9092b230d0)))
(assert (=> F0x7f9092b23350 F0x7f9092b23250))
(assert (=> pre!entry!0 (=> F0x7f9092b22fd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f9092b23190 (>= v0x7f9092b1d210_0 0.0))))
(assert (let ((a!1 (=> F0x7f9092b23190
               (or (<= v0x7f9092b1d210_0 0.0) (not (<= v0x7f9092b1d310_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7f9092b1d3d0_0 0.0) (not (<= v0x7f9092b1a010_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f9092b23390
           (not (>= v0x7f9092b1d3d0_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f9092b23390 a!1)
      (and (not post!bb2.i.i35.i.i!0) F0x7f9092b23350 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
