(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f5e5e754d90 () Bool)
(declare-fun F0x7f5e5e754990 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f5e5e753950_0 () Bool)
(declare-fun v0x7f5e5e752610_0 () Bool)
(declare-fun v0x7f5e5e752310_0 () Real)
(declare-fun v0x7f5e5e74f890_0 () Real)
(declare-fun v0x7f5e5e753e50_0 () Bool)
(declare-fun E0x7f5e5e753410 () Bool)
(declare-fun v0x7f5e5e74ef90_0 () Real)
(declare-fun v0x7f5e5e7524d0_0 () Real)
(declare-fun v0x7f5e5e752dd0_0 () Real)
(declare-fun v0x7f5e5e752890_0 () Bool)
(declare-fun v0x7f5e5e7521d0_0 () Bool)
(declare-fun v0x7f5e5e7529d0_0 () Bool)
(declare-fun E0x7f5e5e751fd0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f5e5e753a90_0 () Bool)
(declare-fun v0x7f5e5e751f10_0 () Bool)
(declare-fun v0x7f5e5e751150_0 () Real)
(declare-fun v0x7f5e5e751c90_0 () Bool)
(declare-fun v0x7f5e5e751090_0 () Bool)
(declare-fun v0x7f5e5e750a10_0 () Bool)
(declare-fun v0x7f5e5e750e10_0 () Bool)
(declare-fun post!bb2.i.i37.i.i!0 () Bool)
(declare-fun E0x7f5e5e750c10 () Bool)
(declare-fun v0x7f5e5e74f110_0 () Real)
(declare-fun v0x7f5e5e750b50_0 () Bool)
(declare-fun E0x7f5e5e7513d0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f5e5e752d10_0 () Real)
(declare-fun F0x7f5e5e754c90 () Bool)
(declare-fun v0x7f5e5e750090_0 () Real)
(declare-fun v0x7f5e5e7518d0_0 () Bool)
(declare-fun v0x7f5e5e750290_0 () Real)
(declare-fun v0x7f5e5e751a10_0 () Bool)
(declare-fun v0x7f5e5e753d10_0 () Bool)
(declare-fun v0x7f5e5e74f210_0 () Real)
(declare-fun v0x7f5e5e7501d0_0 () Bool)
(declare-fun v0x7f5e5e74f950_0 () Bool)
(declare-fun v0x7f5e5e752750_0 () Bool)
(declare-fun E0x7f5e5e74fb50 () Bool)
(declare-fun v0x7f5e5e751dd0_0 () Real)
(declare-fun v0x7f5e5e74f7d0_0 () Bool)
(declare-fun E0x7f5e5e752e90 () Bool)
(declare-fun F0x7f5e5e754a50 () Bool)
(declare-fun v0x7f5e5e752c50_0 () Bool)
(declare-fun v0x7f5e5e750f50_0 () Real)
(declare-fun E0x7f5e5e750350 () Bool)
(declare-fun F0x7f5e5e754b10 () Bool)
(declare-fun v0x7f5e5e74fa90_0 () Bool)
(declare-fun E0x7f5e5e753150 () Bool)
(declare-fun v0x7f5e5e74c010_0 () Real)
(declare-fun E0x7f5e5e752a90 () Bool)
(declare-fun E0x7f5e5e751ad0 () Bool)
(declare-fun E0x7f5e5e751210 () Bool)
(declare-fun v0x7f5e5e74f2d0_0 () Real)
(declare-fun v0x7f5e5e74f1d0_0 () Real)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f5e5e750510 () Bool)
(declare-fun F0x7f5e5e754d50 () Bool)
(declare-fun v0x7f5e5e74c110_0 () Bool)
(declare-fun F0x7f5e5e754bd0 () Bool)
(declare-fun v0x7f5e5e74fd90_0 () Bool)
(declare-fun v0x7f5e5e74ff50_0 () Real)
(declare-fun v0x7f5e5e753bd0_0 () Bool)
(declare-fun v0x7f5e5e750950_0 () Real)

(assert (=> F0x7f5e5e754bd0
    (and v0x7f5e5e74c110_0
         (<= v0x7f5e5e74f1d0_0 0.0)
         (>= v0x7f5e5e74f1d0_0 0.0)
         (<= v0x7f5e5e74f2d0_0 0.0)
         (>= v0x7f5e5e74f2d0_0 0.0)
         (<= v0x7f5e5e74c010_0 1.0)
         (>= v0x7f5e5e74c010_0 1.0))))
(assert (=> F0x7f5e5e754bd0 F0x7f5e5e754b10))
(assert (let ((a!1 (=> v0x7f5e5e7501d0_0
               (or (and v0x7f5e5e74fa90_0
                        E0x7f5e5e750350
                        (<= v0x7f5e5e750290_0 v0x7f5e5e750090_0)
                        (>= v0x7f5e5e750290_0 v0x7f5e5e750090_0))
                   (and v0x7f5e5e74f7d0_0
                        E0x7f5e5e750510
                        v0x7f5e5e74f950_0
                        (<= v0x7f5e5e750290_0 v0x7f5e5e74f210_0)
                        (>= v0x7f5e5e750290_0 v0x7f5e5e74f210_0)))))
      (a!2 (=> v0x7f5e5e7501d0_0
               (or (and E0x7f5e5e750350 (not E0x7f5e5e750510))
                   (and E0x7f5e5e750510 (not E0x7f5e5e750350)))))
      (a!3 (=> v0x7f5e5e751090_0
               (or (and v0x7f5e5e750b50_0
                        E0x7f5e5e751210
                        (<= v0x7f5e5e751150_0 v0x7f5e5e750f50_0)
                        (>= v0x7f5e5e751150_0 v0x7f5e5e750f50_0))
                   (and v0x7f5e5e7501d0_0
                        E0x7f5e5e7513d0
                        v0x7f5e5e750a10_0
                        (<= v0x7f5e5e751150_0 v0x7f5e5e74f110_0)
                        (>= v0x7f5e5e751150_0 v0x7f5e5e74f110_0)))))
      (a!4 (=> v0x7f5e5e751090_0
               (or (and E0x7f5e5e751210 (not E0x7f5e5e7513d0))
                   (and E0x7f5e5e7513d0 (not E0x7f5e5e751210)))))
      (a!5 (or (and v0x7f5e5e751a10_0
                    E0x7f5e5e752e90
                    (<= v0x7f5e5e752d10_0 v0x7f5e5e750290_0)
                    (>= v0x7f5e5e752d10_0 v0x7f5e5e750290_0)
                    (<= v0x7f5e5e752dd0_0 v0x7f5e5e751dd0_0)
                    (>= v0x7f5e5e752dd0_0 v0x7f5e5e751dd0_0))
               (and v0x7f5e5e7529d0_0
                    E0x7f5e5e753150
                    (and (<= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0)
                         (>= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0))
                    (<= v0x7f5e5e752dd0_0 v0x7f5e5e74ef90_0)
                    (>= v0x7f5e5e752dd0_0 v0x7f5e5e74ef90_0))
               (and v0x7f5e5e751f10_0
                    E0x7f5e5e753410
                    (not v0x7f5e5e752890_0)
                    (and (<= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0)
                         (>= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0))
                    (<= v0x7f5e5e752dd0_0 0.0)
                    (>= v0x7f5e5e752dd0_0 0.0))))
      (a!6 (=> v0x7f5e5e752c50_0
               (or (and E0x7f5e5e752e90
                        (not E0x7f5e5e753150)
                        (not E0x7f5e5e753410))
                   (and E0x7f5e5e753150
                        (not E0x7f5e5e752e90)
                        (not E0x7f5e5e753410))
                   (and E0x7f5e5e753410
                        (not E0x7f5e5e752e90)
                        (not E0x7f5e5e753150))))))
(let ((a!7 (and (=> v0x7f5e5e74fa90_0
                    (and v0x7f5e5e74f7d0_0
                         E0x7f5e5e74fb50
                         (not v0x7f5e5e74f950_0)))
                (=> v0x7f5e5e74fa90_0 E0x7f5e5e74fb50)
                a!1
                a!2
                (=> v0x7f5e5e750b50_0
                    (and v0x7f5e5e7501d0_0
                         E0x7f5e5e750c10
                         (not v0x7f5e5e750a10_0)))
                (=> v0x7f5e5e750b50_0 E0x7f5e5e750c10)
                a!3
                a!4
                (=> v0x7f5e5e751a10_0
                    (and v0x7f5e5e751090_0 E0x7f5e5e751ad0 v0x7f5e5e7518d0_0))
                (=> v0x7f5e5e751a10_0 E0x7f5e5e751ad0)
                (=> v0x7f5e5e751f10_0
                    (and v0x7f5e5e751090_0
                         E0x7f5e5e751fd0
                         (not v0x7f5e5e7518d0_0)))
                (=> v0x7f5e5e751f10_0 E0x7f5e5e751fd0)
                (=> v0x7f5e5e7529d0_0
                    (and v0x7f5e5e751f10_0 E0x7f5e5e752a90 v0x7f5e5e752890_0))
                (=> v0x7f5e5e7529d0_0 E0x7f5e5e752a90)
                (=> v0x7f5e5e752c50_0 a!5)
                a!6
                v0x7f5e5e752c50_0
                (not v0x7f5e5e753e50_0)
                (<= v0x7f5e5e74f1d0_0 v0x7f5e5e752dd0_0)
                (>= v0x7f5e5e74f1d0_0 v0x7f5e5e752dd0_0)
                (<= v0x7f5e5e74f2d0_0 v0x7f5e5e751150_0)
                (>= v0x7f5e5e74f2d0_0 v0x7f5e5e751150_0)
                (<= v0x7f5e5e74c010_0 v0x7f5e5e752d10_0)
                (>= v0x7f5e5e74c010_0 v0x7f5e5e752d10_0)
                (= v0x7f5e5e74f950_0 (= v0x7f5e5e74f890_0 0.0))
                (= v0x7f5e5e74fd90_0 (< v0x7f5e5e74f210_0 2.0))
                (= v0x7f5e5e74ff50_0 (ite v0x7f5e5e74fd90_0 1.0 0.0))
                (= v0x7f5e5e750090_0 (+ v0x7f5e5e74ff50_0 v0x7f5e5e74f210_0))
                (= v0x7f5e5e750a10_0 (= v0x7f5e5e750950_0 0.0))
                (= v0x7f5e5e750e10_0 (= v0x7f5e5e74f110_0 0.0))
                (= v0x7f5e5e750f50_0 (ite v0x7f5e5e750e10_0 1.0 0.0))
                (= v0x7f5e5e7518d0_0 (= v0x7f5e5e74ef90_0 0.0))
                (= v0x7f5e5e751c90_0 (> v0x7f5e5e750290_0 1.0))
                (= v0x7f5e5e751dd0_0
                   (ite v0x7f5e5e751c90_0 1.0 v0x7f5e5e74ef90_0))
                (= v0x7f5e5e7521d0_0 (> v0x7f5e5e750290_0 0.0))
                (= v0x7f5e5e752310_0 (+ v0x7f5e5e750290_0 (- 1.0)))
                (= v0x7f5e5e7524d0_0
                   (ite v0x7f5e5e7521d0_0 v0x7f5e5e752310_0 v0x7f5e5e750290_0))
                (= v0x7f5e5e752610_0 (= v0x7f5e5e751150_0 0.0))
                (= v0x7f5e5e752750_0 (= v0x7f5e5e7524d0_0 0.0))
                (= v0x7f5e5e752890_0 (and v0x7f5e5e752610_0 v0x7f5e5e752750_0))
                (= v0x7f5e5e753950_0 (= v0x7f5e5e752d10_0 2.0))
                (= v0x7f5e5e753a90_0 (= v0x7f5e5e752dd0_0 0.0))
                (= v0x7f5e5e753bd0_0 (or v0x7f5e5e753a90_0 v0x7f5e5e753950_0))
                (= v0x7f5e5e753d10_0 (xor v0x7f5e5e753bd0_0 true))
                (= v0x7f5e5e753e50_0 (and v0x7f5e5e7518d0_0 v0x7f5e5e753d10_0)))))
  (=> F0x7f5e5e754a50 a!7))))
(assert (=> F0x7f5e5e754a50 F0x7f5e5e754990))
(assert (let ((a!1 (=> v0x7f5e5e7501d0_0
               (or (and v0x7f5e5e74fa90_0
                        E0x7f5e5e750350
                        (<= v0x7f5e5e750290_0 v0x7f5e5e750090_0)
                        (>= v0x7f5e5e750290_0 v0x7f5e5e750090_0))
                   (and v0x7f5e5e74f7d0_0
                        E0x7f5e5e750510
                        v0x7f5e5e74f950_0
                        (<= v0x7f5e5e750290_0 v0x7f5e5e74f210_0)
                        (>= v0x7f5e5e750290_0 v0x7f5e5e74f210_0)))))
      (a!2 (=> v0x7f5e5e7501d0_0
               (or (and E0x7f5e5e750350 (not E0x7f5e5e750510))
                   (and E0x7f5e5e750510 (not E0x7f5e5e750350)))))
      (a!3 (=> v0x7f5e5e751090_0
               (or (and v0x7f5e5e750b50_0
                        E0x7f5e5e751210
                        (<= v0x7f5e5e751150_0 v0x7f5e5e750f50_0)
                        (>= v0x7f5e5e751150_0 v0x7f5e5e750f50_0))
                   (and v0x7f5e5e7501d0_0
                        E0x7f5e5e7513d0
                        v0x7f5e5e750a10_0
                        (<= v0x7f5e5e751150_0 v0x7f5e5e74f110_0)
                        (>= v0x7f5e5e751150_0 v0x7f5e5e74f110_0)))))
      (a!4 (=> v0x7f5e5e751090_0
               (or (and E0x7f5e5e751210 (not E0x7f5e5e7513d0))
                   (and E0x7f5e5e7513d0 (not E0x7f5e5e751210)))))
      (a!5 (or (and v0x7f5e5e751a10_0
                    E0x7f5e5e752e90
                    (<= v0x7f5e5e752d10_0 v0x7f5e5e750290_0)
                    (>= v0x7f5e5e752d10_0 v0x7f5e5e750290_0)
                    (<= v0x7f5e5e752dd0_0 v0x7f5e5e751dd0_0)
                    (>= v0x7f5e5e752dd0_0 v0x7f5e5e751dd0_0))
               (and v0x7f5e5e7529d0_0
                    E0x7f5e5e753150
                    (and (<= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0)
                         (>= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0))
                    (<= v0x7f5e5e752dd0_0 v0x7f5e5e74ef90_0)
                    (>= v0x7f5e5e752dd0_0 v0x7f5e5e74ef90_0))
               (and v0x7f5e5e751f10_0
                    E0x7f5e5e753410
                    (not v0x7f5e5e752890_0)
                    (and (<= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0)
                         (>= v0x7f5e5e752d10_0 v0x7f5e5e7524d0_0))
                    (<= v0x7f5e5e752dd0_0 0.0)
                    (>= v0x7f5e5e752dd0_0 0.0))))
      (a!6 (=> v0x7f5e5e752c50_0
               (or (and E0x7f5e5e752e90
                        (not E0x7f5e5e753150)
                        (not E0x7f5e5e753410))
                   (and E0x7f5e5e753150
                        (not E0x7f5e5e752e90)
                        (not E0x7f5e5e753410))
                   (and E0x7f5e5e753410
                        (not E0x7f5e5e752e90)
                        (not E0x7f5e5e753150))))))
(let ((a!7 (and (=> v0x7f5e5e74fa90_0
                    (and v0x7f5e5e74f7d0_0
                         E0x7f5e5e74fb50
                         (not v0x7f5e5e74f950_0)))
                (=> v0x7f5e5e74fa90_0 E0x7f5e5e74fb50)
                a!1
                a!2
                (=> v0x7f5e5e750b50_0
                    (and v0x7f5e5e7501d0_0
                         E0x7f5e5e750c10
                         (not v0x7f5e5e750a10_0)))
                (=> v0x7f5e5e750b50_0 E0x7f5e5e750c10)
                a!3
                a!4
                (=> v0x7f5e5e751a10_0
                    (and v0x7f5e5e751090_0 E0x7f5e5e751ad0 v0x7f5e5e7518d0_0))
                (=> v0x7f5e5e751a10_0 E0x7f5e5e751ad0)
                (=> v0x7f5e5e751f10_0
                    (and v0x7f5e5e751090_0
                         E0x7f5e5e751fd0
                         (not v0x7f5e5e7518d0_0)))
                (=> v0x7f5e5e751f10_0 E0x7f5e5e751fd0)
                (=> v0x7f5e5e7529d0_0
                    (and v0x7f5e5e751f10_0 E0x7f5e5e752a90 v0x7f5e5e752890_0))
                (=> v0x7f5e5e7529d0_0 E0x7f5e5e752a90)
                (=> v0x7f5e5e752c50_0 a!5)
                a!6
                v0x7f5e5e752c50_0
                v0x7f5e5e753e50_0
                (= v0x7f5e5e74f950_0 (= v0x7f5e5e74f890_0 0.0))
                (= v0x7f5e5e74fd90_0 (< v0x7f5e5e74f210_0 2.0))
                (= v0x7f5e5e74ff50_0 (ite v0x7f5e5e74fd90_0 1.0 0.0))
                (= v0x7f5e5e750090_0 (+ v0x7f5e5e74ff50_0 v0x7f5e5e74f210_0))
                (= v0x7f5e5e750a10_0 (= v0x7f5e5e750950_0 0.0))
                (= v0x7f5e5e750e10_0 (= v0x7f5e5e74f110_0 0.0))
                (= v0x7f5e5e750f50_0 (ite v0x7f5e5e750e10_0 1.0 0.0))
                (= v0x7f5e5e7518d0_0 (= v0x7f5e5e74ef90_0 0.0))
                (= v0x7f5e5e751c90_0 (> v0x7f5e5e750290_0 1.0))
                (= v0x7f5e5e751dd0_0
                   (ite v0x7f5e5e751c90_0 1.0 v0x7f5e5e74ef90_0))
                (= v0x7f5e5e7521d0_0 (> v0x7f5e5e750290_0 0.0))
                (= v0x7f5e5e752310_0 (+ v0x7f5e5e750290_0 (- 1.0)))
                (= v0x7f5e5e7524d0_0
                   (ite v0x7f5e5e7521d0_0 v0x7f5e5e752310_0 v0x7f5e5e750290_0))
                (= v0x7f5e5e752610_0 (= v0x7f5e5e751150_0 0.0))
                (= v0x7f5e5e752750_0 (= v0x7f5e5e7524d0_0 0.0))
                (= v0x7f5e5e752890_0 (and v0x7f5e5e752610_0 v0x7f5e5e752750_0))
                (= v0x7f5e5e753950_0 (= v0x7f5e5e752d10_0 2.0))
                (= v0x7f5e5e753a90_0 (= v0x7f5e5e752dd0_0 0.0))
                (= v0x7f5e5e753bd0_0 (or v0x7f5e5e753a90_0 v0x7f5e5e753950_0))
                (= v0x7f5e5e753d10_0 (xor v0x7f5e5e753bd0_0 true))
                (= v0x7f5e5e753e50_0 (and v0x7f5e5e7518d0_0 v0x7f5e5e753d10_0)))))
  (=> F0x7f5e5e754c90 a!7))))
(assert (=> F0x7f5e5e754c90 F0x7f5e5e754990))
(assert (=> F0x7f5e5e754d90 (or F0x7f5e5e754bd0 F0x7f5e5e754a50)))
(assert (=> F0x7f5e5e754d50 F0x7f5e5e754c90))
(assert (=> pre!entry!0 (=> F0x7f5e5e754b10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f5e5e754990 (>= v0x7f5e5e74f110_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f5e5e754990 (>= v0x7f5e5e74f210_0 0.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f5e5e754990
        (or (>= v0x7f5e5e74f210_0 1.0) (<= v0x7f5e5e74f210_0 0.0)))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f5e5e754990
        (or (<= v0x7f5e5e74f210_0 1.0) (>= v0x7f5e5e74f210_0 2.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f5e5e754990 (<= v0x7f5e5e74f210_0 2.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!2)
                F0x7f5e5e754d90
                (not (or (>= v0x7f5e5e74c010_0 1.0) (<= v0x7f5e5e74c010_0 0.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7f5e5e754d90
                (not (or (<= v0x7f5e5e74c010_0 1.0) (>= v0x7f5e5e74c010_0 2.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f5e5e754d90
           (not (>= v0x7f5e5e74f2d0_0 0.0)))
      (and (not post!bb1.i.i!1)
           F0x7f5e5e754d90
           (not (>= v0x7f5e5e74c010_0 0.0)))
      a!1
      a!2
      (and (not post!bb1.i.i!4)
           F0x7f5e5e754d90
           (not (<= v0x7f5e5e74c010_0 2.0)))
      (and (not post!bb2.i.i37.i.i!0) F0x7f5e5e754d50 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i37.i.i!0)
