(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7ff003388910 () Bool)
(declare-fun F0x7ff003388950 () Bool)
(declare-fun v0x7ff003385c10_0 () Bool)
(declare-fun v0x7ff003384410_0 () Bool)
(declare-fun v0x7ff003383390_0 () Bool)
(declare-fun v0x7ff003387790_0 () Bool)
(declare-fun v0x7ff003382e90_0 () Real)
(declare-fun post!bb2.i.i45.i.i!0 () Bool)
(declare-fun E0x7ff003386f50 () Bool)
(declare-fun v0x7ff003382810_0 () Real)
(declare-fun v0x7ff003385ad0_0 () Bool)
(declare-fun v0x7ff003383550_0 () Real)
(declare-fun E0x7ff003386ad0 () Bool)
(declare-fun v0x7ff003387510_0 () Bool)
(declare-fun v0x7ff003386210_0 () Real)
(declare-fun F0x7ff003388550 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7ff0033857d0_0 () Real)
(declare-fun v0x7ff003386750_0 () Real)
(declare-fun v0x7ff003386690_0 () Real)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7ff003386350_0 () Bool)
(declare-fun v0x7ff003385690_0 () Bool)
(declare-fun v0x7ff003385290_0 () Bool)
(declare-fun v0x7ff003385e90_0 () Bool)
(declare-fun v0x7ff0033853d0_0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun E0x7ff003386d50 () Bool)
(declare-fun v0x7ff003385010_0 () Bool)
(declare-fun v0x7ff003384550_0 () Real)
(declare-fun E0x7ff0033850d0 () Bool)
(declare-fun v0x7ff003384ed0_0 () Bool)
(declare-fun v0x7ff003384750_0 () Real)
(declare-fun E0x7ff003386810 () Bool)
(declare-fun v0x7ff003385990_0 () Real)
(declare-fun v0x7ff003384690_0 () Bool)
(declare-fun v0x7ff003384010_0 () Bool)
(declare-fun E0x7ff003384810 () Bool)
(declare-fun v0x7ff0033878d0_0 () Bool)
(declare-fun F0x7ff003388850 () Bool)
(declare-fun E0x7ff003385f50 () Bool)
(declare-fun v0x7ff003384150_0 () Bool)
(declare-fun v0x7ff0033865d0_0 () Bool)
(declare-fun v0x7ff003382710_0 () Real)
(declare-fun E0x7ff003383b10 () Bool)
(declare-fun E0x7ff003386410 () Bool)
(declare-fun E0x7ff003383950 () Bool)
(declare-fun v0x7ff0033837d0_0 () Bool)
(declare-fun v0x7ff003387650_0 () Bool)
(declare-fun E0x7ff003383150 () Bool)
(declare-fun v0x7ff003382f50_0 () Bool)
(declare-fun v0x7ff003382590_0 () Real)
(declare-fun E0x7ff0033849d0 () Bool)
(declare-fun v0x7ff003382dd0_0 () Bool)
(declare-fun v0x7ff003383690_0 () Real)
(declare-fun v0x7ff003383090_0 () Bool)
(declare-fun E0x7ff003384210 () Bool)
(declare-fun F0x7ff0033886d0 () Bool)
(declare-fun v0x7ff0033827d0_0 () Real)
(declare-fun v0x7ff003387a10_0 () Bool)
(declare-fun v0x7ff003383f50_0 () Real)
(declare-fun E0x7ff003385490 () Bool)
(declare-fun v0x7ff003383890_0 () Real)
(declare-fun v0x7ff00337f110_0 () Bool)
(declare-fun v0x7ff003385d50_0 () Bool)
(declare-fun F0x7ff003388610 () Bool)
(declare-fun v0x7ff003386110_0 () Bool)
(declare-fun v0x7ff0033828d0_0 () Real)
(declare-fun F0x7ff003388790 () Bool)
(declare-fun v0x7ff00337f010_0 () Real)

(assert (=> F0x7ff003388790
    (and v0x7ff00337f110_0
         (<= v0x7ff0033827d0_0 0.0)
         (>= v0x7ff0033827d0_0 0.0)
         (<= v0x7ff0033828d0_0 1.0)
         (>= v0x7ff0033828d0_0 1.0)
         (<= v0x7ff00337f010_0 0.0)
         (>= v0x7ff00337f010_0 0.0))))
(assert (=> F0x7ff003388790 F0x7ff0033886d0))
(assert (let ((a!1 (=> v0x7ff0033837d0_0
               (or (and v0x7ff003383090_0
                        E0x7ff003383950
                        (<= v0x7ff003383890_0 v0x7ff003383690_0)
                        (>= v0x7ff003383890_0 v0x7ff003383690_0))
                   (and v0x7ff003382dd0_0
                        E0x7ff003383b10
                        v0x7ff003382f50_0
                        (<= v0x7ff003383890_0 v0x7ff003382710_0)
                        (>= v0x7ff003383890_0 v0x7ff003382710_0)))))
      (a!2 (=> v0x7ff0033837d0_0
               (or (and E0x7ff003383950 (not E0x7ff003383b10))
                   (and E0x7ff003383b10 (not E0x7ff003383950)))))
      (a!3 (=> v0x7ff003384690_0
               (or (and v0x7ff003384150_0
                        E0x7ff003384810
                        (<= v0x7ff003384750_0 v0x7ff003384550_0)
                        (>= v0x7ff003384750_0 v0x7ff003384550_0))
                   (and v0x7ff0033837d0_0
                        E0x7ff0033849d0
                        v0x7ff003384010_0
                        (<= v0x7ff003384750_0 v0x7ff003382590_0)
                        (>= v0x7ff003384750_0 v0x7ff003382590_0)))))
      (a!4 (=> v0x7ff003384690_0
               (or (and E0x7ff003384810 (not E0x7ff0033849d0))
                   (and E0x7ff0033849d0 (not E0x7ff003384810)))))
      (a!5 (or (and v0x7ff003385e90_0
                    E0x7ff003386810
                    (and (<= v0x7ff003386690_0 v0x7ff003383890_0)
                         (>= v0x7ff003386690_0 v0x7ff003383890_0))
                    (<= v0x7ff003386750_0 v0x7ff003386210_0)
                    (>= v0x7ff003386750_0 v0x7ff003386210_0))
               (and v0x7ff003385010_0
                    E0x7ff003386ad0
                    (not v0x7ff003385290_0)
                    (and (<= v0x7ff003386690_0 v0x7ff003383890_0)
                         (>= v0x7ff003386690_0 v0x7ff003383890_0))
                    (and (<= v0x7ff003386750_0 v0x7ff003382810_0)
                         (>= v0x7ff003386750_0 v0x7ff003382810_0)))
               (and v0x7ff003386350_0
                    E0x7ff003386d50
                    (and (<= v0x7ff003386690_0 v0x7ff003385990_0)
                         (>= v0x7ff003386690_0 v0x7ff003385990_0))
                    (and (<= v0x7ff003386750_0 v0x7ff003382810_0)
                         (>= v0x7ff003386750_0 v0x7ff003382810_0)))
               (and v0x7ff0033853d0_0
                    E0x7ff003386f50
                    (not v0x7ff003385d50_0)
                    (and (<= v0x7ff003386690_0 v0x7ff003385990_0)
                         (>= v0x7ff003386690_0 v0x7ff003385990_0))
                    (<= v0x7ff003386750_0 0.0)
                    (>= v0x7ff003386750_0 0.0))))
      (a!6 (=> v0x7ff0033865d0_0
               (or (and E0x7ff003386810
                        (not E0x7ff003386ad0)
                        (not E0x7ff003386d50)
                        (not E0x7ff003386f50))
                   (and E0x7ff003386ad0
                        (not E0x7ff003386810)
                        (not E0x7ff003386d50)
                        (not E0x7ff003386f50))
                   (and E0x7ff003386d50
                        (not E0x7ff003386810)
                        (not E0x7ff003386ad0)
                        (not E0x7ff003386f50))
                   (and E0x7ff003386f50
                        (not E0x7ff003386810)
                        (not E0x7ff003386ad0)
                        (not E0x7ff003386d50))))))
(let ((a!7 (and (=> v0x7ff003383090_0
                    (and v0x7ff003382dd0_0
                         E0x7ff003383150
                         (not v0x7ff003382f50_0)))
                (=> v0x7ff003383090_0 E0x7ff003383150)
                a!1
                a!2
                (=> v0x7ff003384150_0
                    (and v0x7ff0033837d0_0
                         E0x7ff003384210
                         (not v0x7ff003384010_0)))
                (=> v0x7ff003384150_0 E0x7ff003384210)
                a!3
                a!4
                (=> v0x7ff003385010_0
                    (and v0x7ff003384690_0 E0x7ff0033850d0 v0x7ff003384ed0_0))
                (=> v0x7ff003385010_0 E0x7ff0033850d0)
                (=> v0x7ff0033853d0_0
                    (and v0x7ff003384690_0
                         E0x7ff003385490
                         (not v0x7ff003384ed0_0)))
                (=> v0x7ff0033853d0_0 E0x7ff003385490)
                (=> v0x7ff003385e90_0
                    (and v0x7ff003385010_0 E0x7ff003385f50 v0x7ff003385290_0))
                (=> v0x7ff003385e90_0 E0x7ff003385f50)
                (=> v0x7ff003386350_0
                    (and v0x7ff0033853d0_0 E0x7ff003386410 v0x7ff003385d50_0))
                (=> v0x7ff003386350_0 E0x7ff003386410)
                (=> v0x7ff0033865d0_0 a!5)
                a!6
                v0x7ff0033865d0_0
                (not v0x7ff003387a10_0)
                (<= v0x7ff0033827d0_0 v0x7ff003384750_0)
                (>= v0x7ff0033827d0_0 v0x7ff003384750_0)
                (<= v0x7ff0033828d0_0 v0x7ff003386690_0)
                (>= v0x7ff0033828d0_0 v0x7ff003386690_0)
                (<= v0x7ff00337f010_0 v0x7ff003386750_0)
                (>= v0x7ff00337f010_0 v0x7ff003386750_0)
                (= v0x7ff003382f50_0 (= v0x7ff003382e90_0 0.0))
                (= v0x7ff003383390_0 (< v0x7ff003382710_0 2.0))
                (= v0x7ff003383550_0 (ite v0x7ff003383390_0 1.0 0.0))
                (= v0x7ff003383690_0 (+ v0x7ff003383550_0 v0x7ff003382710_0))
                (= v0x7ff003384010_0 (= v0x7ff003383f50_0 0.0))
                (= v0x7ff003384410_0 (= v0x7ff003382590_0 0.0))
                (= v0x7ff003384550_0 (ite v0x7ff003384410_0 1.0 0.0))
                (= v0x7ff003384ed0_0 (= v0x7ff003382810_0 0.0))
                (= v0x7ff003385290_0 (> v0x7ff003383890_0 1.0))
                (= v0x7ff003385690_0 (> v0x7ff003383890_0 0.0))
                (= v0x7ff0033857d0_0 (+ v0x7ff003383890_0 (- 1.0)))
                (= v0x7ff003385990_0
                   (ite v0x7ff003385690_0 v0x7ff0033857d0_0 v0x7ff003383890_0))
                (= v0x7ff003385ad0_0 (= v0x7ff003384750_0 0.0))
                (= v0x7ff003385c10_0 (= v0x7ff003385990_0 0.0))
                (= v0x7ff003385d50_0 (and v0x7ff003385ad0_0 v0x7ff003385c10_0))
                (= v0x7ff003386110_0 (= v0x7ff003384750_0 0.0))
                (= v0x7ff003386210_0
                   (ite v0x7ff003386110_0 1.0 v0x7ff003382810_0))
                (= v0x7ff003387510_0 (= v0x7ff003386690_0 2.0))
                (= v0x7ff003387650_0 (= v0x7ff003386750_0 0.0))
                (= v0x7ff003387790_0 (or v0x7ff003387650_0 v0x7ff003387510_0))
                (= v0x7ff0033878d0_0 (xor v0x7ff003387790_0 true))
                (= v0x7ff003387a10_0 (and v0x7ff003384ed0_0 v0x7ff0033878d0_0)))))
  (=> F0x7ff003388610 a!7))))
(assert (=> F0x7ff003388610 F0x7ff003388550))
(assert (let ((a!1 (=> v0x7ff0033837d0_0
               (or (and v0x7ff003383090_0
                        E0x7ff003383950
                        (<= v0x7ff003383890_0 v0x7ff003383690_0)
                        (>= v0x7ff003383890_0 v0x7ff003383690_0))
                   (and v0x7ff003382dd0_0
                        E0x7ff003383b10
                        v0x7ff003382f50_0
                        (<= v0x7ff003383890_0 v0x7ff003382710_0)
                        (>= v0x7ff003383890_0 v0x7ff003382710_0)))))
      (a!2 (=> v0x7ff0033837d0_0
               (or (and E0x7ff003383950 (not E0x7ff003383b10))
                   (and E0x7ff003383b10 (not E0x7ff003383950)))))
      (a!3 (=> v0x7ff003384690_0
               (or (and v0x7ff003384150_0
                        E0x7ff003384810
                        (<= v0x7ff003384750_0 v0x7ff003384550_0)
                        (>= v0x7ff003384750_0 v0x7ff003384550_0))
                   (and v0x7ff0033837d0_0
                        E0x7ff0033849d0
                        v0x7ff003384010_0
                        (<= v0x7ff003384750_0 v0x7ff003382590_0)
                        (>= v0x7ff003384750_0 v0x7ff003382590_0)))))
      (a!4 (=> v0x7ff003384690_0
               (or (and E0x7ff003384810 (not E0x7ff0033849d0))
                   (and E0x7ff0033849d0 (not E0x7ff003384810)))))
      (a!5 (or (and v0x7ff003385e90_0
                    E0x7ff003386810
                    (and (<= v0x7ff003386690_0 v0x7ff003383890_0)
                         (>= v0x7ff003386690_0 v0x7ff003383890_0))
                    (<= v0x7ff003386750_0 v0x7ff003386210_0)
                    (>= v0x7ff003386750_0 v0x7ff003386210_0))
               (and v0x7ff003385010_0
                    E0x7ff003386ad0
                    (not v0x7ff003385290_0)
                    (and (<= v0x7ff003386690_0 v0x7ff003383890_0)
                         (>= v0x7ff003386690_0 v0x7ff003383890_0))
                    (and (<= v0x7ff003386750_0 v0x7ff003382810_0)
                         (>= v0x7ff003386750_0 v0x7ff003382810_0)))
               (and v0x7ff003386350_0
                    E0x7ff003386d50
                    (and (<= v0x7ff003386690_0 v0x7ff003385990_0)
                         (>= v0x7ff003386690_0 v0x7ff003385990_0))
                    (and (<= v0x7ff003386750_0 v0x7ff003382810_0)
                         (>= v0x7ff003386750_0 v0x7ff003382810_0)))
               (and v0x7ff0033853d0_0
                    E0x7ff003386f50
                    (not v0x7ff003385d50_0)
                    (and (<= v0x7ff003386690_0 v0x7ff003385990_0)
                         (>= v0x7ff003386690_0 v0x7ff003385990_0))
                    (<= v0x7ff003386750_0 0.0)
                    (>= v0x7ff003386750_0 0.0))))
      (a!6 (=> v0x7ff0033865d0_0
               (or (and E0x7ff003386810
                        (not E0x7ff003386ad0)
                        (not E0x7ff003386d50)
                        (not E0x7ff003386f50))
                   (and E0x7ff003386ad0
                        (not E0x7ff003386810)
                        (not E0x7ff003386d50)
                        (not E0x7ff003386f50))
                   (and E0x7ff003386d50
                        (not E0x7ff003386810)
                        (not E0x7ff003386ad0)
                        (not E0x7ff003386f50))
                   (and E0x7ff003386f50
                        (not E0x7ff003386810)
                        (not E0x7ff003386ad0)
                        (not E0x7ff003386d50))))))
(let ((a!7 (and (=> v0x7ff003383090_0
                    (and v0x7ff003382dd0_0
                         E0x7ff003383150
                         (not v0x7ff003382f50_0)))
                (=> v0x7ff003383090_0 E0x7ff003383150)
                a!1
                a!2
                (=> v0x7ff003384150_0
                    (and v0x7ff0033837d0_0
                         E0x7ff003384210
                         (not v0x7ff003384010_0)))
                (=> v0x7ff003384150_0 E0x7ff003384210)
                a!3
                a!4
                (=> v0x7ff003385010_0
                    (and v0x7ff003384690_0 E0x7ff0033850d0 v0x7ff003384ed0_0))
                (=> v0x7ff003385010_0 E0x7ff0033850d0)
                (=> v0x7ff0033853d0_0
                    (and v0x7ff003384690_0
                         E0x7ff003385490
                         (not v0x7ff003384ed0_0)))
                (=> v0x7ff0033853d0_0 E0x7ff003385490)
                (=> v0x7ff003385e90_0
                    (and v0x7ff003385010_0 E0x7ff003385f50 v0x7ff003385290_0))
                (=> v0x7ff003385e90_0 E0x7ff003385f50)
                (=> v0x7ff003386350_0
                    (and v0x7ff0033853d0_0 E0x7ff003386410 v0x7ff003385d50_0))
                (=> v0x7ff003386350_0 E0x7ff003386410)
                (=> v0x7ff0033865d0_0 a!5)
                a!6
                v0x7ff0033865d0_0
                v0x7ff003387a10_0
                (= v0x7ff003382f50_0 (= v0x7ff003382e90_0 0.0))
                (= v0x7ff003383390_0 (< v0x7ff003382710_0 2.0))
                (= v0x7ff003383550_0 (ite v0x7ff003383390_0 1.0 0.0))
                (= v0x7ff003383690_0 (+ v0x7ff003383550_0 v0x7ff003382710_0))
                (= v0x7ff003384010_0 (= v0x7ff003383f50_0 0.0))
                (= v0x7ff003384410_0 (= v0x7ff003382590_0 0.0))
                (= v0x7ff003384550_0 (ite v0x7ff003384410_0 1.0 0.0))
                (= v0x7ff003384ed0_0 (= v0x7ff003382810_0 0.0))
                (= v0x7ff003385290_0 (> v0x7ff003383890_0 1.0))
                (= v0x7ff003385690_0 (> v0x7ff003383890_0 0.0))
                (= v0x7ff0033857d0_0 (+ v0x7ff003383890_0 (- 1.0)))
                (= v0x7ff003385990_0
                   (ite v0x7ff003385690_0 v0x7ff0033857d0_0 v0x7ff003383890_0))
                (= v0x7ff003385ad0_0 (= v0x7ff003384750_0 0.0))
                (= v0x7ff003385c10_0 (= v0x7ff003385990_0 0.0))
                (= v0x7ff003385d50_0 (and v0x7ff003385ad0_0 v0x7ff003385c10_0))
                (= v0x7ff003386110_0 (= v0x7ff003384750_0 0.0))
                (= v0x7ff003386210_0
                   (ite v0x7ff003386110_0 1.0 v0x7ff003382810_0))
                (= v0x7ff003387510_0 (= v0x7ff003386690_0 2.0))
                (= v0x7ff003387650_0 (= v0x7ff003386750_0 0.0))
                (= v0x7ff003387790_0 (or v0x7ff003387650_0 v0x7ff003387510_0))
                (= v0x7ff0033878d0_0 (xor v0x7ff003387790_0 true))
                (= v0x7ff003387a10_0 (and v0x7ff003384ed0_0 v0x7ff0033878d0_0)))))
  (=> F0x7ff003388850 a!7))))
(assert (=> F0x7ff003388850 F0x7ff003388550))
(assert (=> F0x7ff003388950 (or F0x7ff003388790 F0x7ff003388610)))
(assert (=> F0x7ff003388910 F0x7ff003388850))
(assert (=> pre!entry!0 (=> F0x7ff0033886d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7ff003388550 (>= v0x7ff003382590_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7ff003388550
        (or (>= v0x7ff003382710_0 2.0) (<= v0x7ff003382710_0 1.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7ff003388550 (>= v0x7ff003382710_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7ff003388550
        (or (>= v0x7ff003382710_0 1.0) (<= v0x7ff003382710_0 0.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7ff003388550 (<= v0x7ff003382710_0 2.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7ff003388950
                (not (or (>= v0x7ff0033828d0_0 2.0) (<= v0x7ff0033828d0_0 1.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7ff003388950
                (not (or (>= v0x7ff0033828d0_0 1.0) (<= v0x7ff0033828d0_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7ff003388950
           (not (>= v0x7ff0033827d0_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7ff003388950
           (not (>= v0x7ff0033828d0_0 0.0)))
      a!2
      (and (not post!bb1.i.i!4)
           F0x7ff003388950
           (not (<= v0x7ff0033828d0_0 2.0)))
      (and (not post!bb2.i.i45.i.i!0) F0x7ff003388910 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i45.i.i!0)
