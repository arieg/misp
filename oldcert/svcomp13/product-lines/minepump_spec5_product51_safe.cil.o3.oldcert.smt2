(declare-fun post!bb2.i.i28.i.i!0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fef091a8b10 () Bool)
(declare-fun F0x7fef091a8c90 () Bool)
(declare-fun v0x7fef091a7e90_0 () Bool)
(declare-fun v0x7fef091a7d50_0 () Bool)
(declare-fun v0x7fef091a7c10_0 () Bool)
(declare-fun v0x7fef091a7ad0_0 () Bool)
(declare-fun v0x7fef091a3b10_0 () Real)
(declare-fun v0x7fef091a2a10_0 () Bool)
(declare-fun v0x7fef091a2510_0 () Real)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun v0x7fef091a7fd0_0 () Bool)
(declare-fun E0x7fef091a6c50 () Bool)
(declare-fun v0x7fef091a6450_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun E0x7fef091a7150 () Bool)
(declare-fun v0x7fef091a6390_0 () Real)
(declare-fun v0x7fef091a4650_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fef091a1e90_0 () Real)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun E0x7fef091a6510 () Bool)
(declare-fun v0x7fef091a5d90_0 () Bool)
(declare-fun v0x7fef091a6150_0 () Bool)
(declare-fun E0x7fef091a69d0 () Bool)
(declare-fun E0x7fef091a56d0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fef091a5210_0 () Bool)
(declare-fun E0x7fef091a5410 () Bool)
(declare-fun v0x7fef091a5890_0 () Bool)
(declare-fun v0x7fef091a5350_0 () Bool)
(declare-fun F0x7fef091a8b50 () Bool)
(declare-fun v0x7fef091a4a90_0 () Bool)
(declare-fun E0x7fef091a4450 () Bool)
(declare-fun E0x7fef091a6f50 () Bool)
(declare-fun v0x7fef091a59d0_0 () Real)
(declare-fun E0x7fef091a3ed0 () Bool)
(declare-fun v0x7fef091a3bd0_0 () Bool)
(declare-fun v0x7fef091a3d10_0 () Bool)
(declare-fun E0x7fef091a3950 () Bool)
(declare-fun v0x7fef091a4390_0 () Bool)
(declare-fun v0x7fef091a1c10_0 () Real)
(declare-fun v0x7fef091a6210_0 () Real)
(declare-fun v0x7fef091a7990_0 () Bool)
(declare-fun E0x7fef091a3190 () Bool)
(declare-fun E0x7fef091a4dd0 () Bool)
(declare-fun v0x7fef091a3750_0 () Bool)
(declare-fun v0x7fef091a2f10_0 () Real)
(declare-fun v0x7fef091a5ed0_0 () Bool)
(declare-fun v0x7fef091a3690_0 () Real)
(declare-fun E0x7fef091a2fd0 () Bool)
(declare-fun v0x7fef091a4b50_0 () Real)
(declare-fun v0x7fef091a4250_0 () Bool)
(declare-fun v0x7fef091a5b10_0 () Bool)
(declare-fun v0x7fef091a2e50_0 () Bool)
(declare-fun v0x7fef091a2bd0_0 () Real)
(declare-fun v0x7fef091a1d90_0 () Real)
(declare-fun v0x7fef091a25d0_0 () Bool)
(declare-fun v0x7fef091a2710_0 () Bool)
(declare-fun v0x7fef091a4790_0 () Real)
(declare-fun E0x7fef091a5f90 () Bool)
(declare-fun v0x7fef091a2d10_0 () Real)
(declare-fun F0x7fef091a8bd0 () Bool)
(declare-fun F0x7fef091a8dd0 () Bool)
(declare-fun v0x7fef091a5610_0 () Bool)
(declare-fun F0x7fef091a8e90 () Bool)
(declare-fun v0x7fef091a1f50_0 () Real)
(declare-fun v0x7fef091a0010_0 () Real)
(declare-fun v0x7fef091a3890_0 () Bool)
(declare-fun v0x7fef091a62d0_0 () Real)
(declare-fun v0x7fef091a1e50_0 () Real)
(declare-fun v0x7fef091a2450_0 () Bool)
(declare-fun v0x7fef091a4950_0 () Real)
(declare-fun v0x7fef091a0110_0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun E0x7fef091a4c10 () Bool)
(declare-fun E0x7fef091a27d0 () Bool)
(declare-fun F0x7fef091a8f10 () Bool)
(declare-fun E0x7fef091a5bd0 () Bool)
(declare-fun E0x7fef091a3dd0 () Bool)

(assert (=> F0x7fef091a8f10
    (and v0x7fef091a0110_0
         (<= v0x7fef091a1e50_0 1.0)
         (>= v0x7fef091a1e50_0 1.0)
         (<= v0x7fef091a1f50_0 1.0)
         (>= v0x7fef091a1f50_0 1.0)
         (<= v0x7fef091a0010_0 0.0)
         (>= v0x7fef091a0010_0 0.0))))
(assert (=> F0x7fef091a8f10 F0x7fef091a8e90))
(assert (let ((a!1 (=> v0x7fef091a2e50_0
               (or (and v0x7fef091a2710_0
                        E0x7fef091a2fd0
                        (<= v0x7fef091a2f10_0 v0x7fef091a2d10_0)
                        (>= v0x7fef091a2f10_0 v0x7fef091a2d10_0))
                   (and v0x7fef091a2450_0
                        E0x7fef091a3190
                        v0x7fef091a25d0_0
                        (<= v0x7fef091a2f10_0 v0x7fef091a1c10_0)
                        (>= v0x7fef091a2f10_0 v0x7fef091a1c10_0)))))
      (a!2 (=> v0x7fef091a2e50_0
               (or (and E0x7fef091a2fd0 (not E0x7fef091a3190))
                   (and E0x7fef091a3190 (not E0x7fef091a2fd0)))))
      (a!3 (=> v0x7fef091a3d10_0
               (or (and v0x7fef091a3890_0 E0x7fef091a3dd0 v0x7fef091a3bd0_0)
                   (and v0x7fef091a2e50_0
                        E0x7fef091a3ed0
                        (not v0x7fef091a3750_0)))))
      (a!4 (=> v0x7fef091a3d10_0
               (or (and E0x7fef091a3dd0 (not E0x7fef091a3ed0))
                   (and E0x7fef091a3ed0 (not E0x7fef091a3dd0)))))
      (a!5 (=> v0x7fef091a4a90_0
               (or (and v0x7fef091a4390_0
                        E0x7fef091a4c10
                        (<= v0x7fef091a4b50_0 v0x7fef091a4950_0)
                        (>= v0x7fef091a4b50_0 v0x7fef091a4950_0))
                   (and v0x7fef091a3d10_0
                        E0x7fef091a4dd0
                        v0x7fef091a4250_0
                        (<= v0x7fef091a4b50_0 v0x7fef091a2f10_0)
                        (>= v0x7fef091a4b50_0 v0x7fef091a2f10_0)))))
      (a!6 (=> v0x7fef091a4a90_0
               (or (and E0x7fef091a4c10 (not E0x7fef091a4dd0))
                   (and E0x7fef091a4dd0 (not E0x7fef091a4c10)))))
      (a!7 (or (and v0x7fef091a5610_0
                    E0x7fef091a6510
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (<= v0x7fef091a6450_0 v0x7fef091a59d0_0)
                    (>= v0x7fef091a6450_0 v0x7fef091a59d0_0))
               (and v0x7fef091a5ed0_0
                    E0x7fef091a69d0
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (and (<= v0x7fef091a6450_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6450_0 v0x7fef091a1e90_0)))
               (and v0x7fef091a5b10_0
                    E0x7fef091a6c50
                    (not v0x7fef091a5d90_0)
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (and (<= v0x7fef091a6450_0 0.0) (>= v0x7fef091a6450_0 0.0)))
               (and v0x7fef091a4a90_0
                    E0x7fef091a6f50
                    v0x7fef091a5210_0
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (and (<= v0x7fef091a6450_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6450_0 v0x7fef091a1e90_0)))
               (and v0x7fef091a3890_0
                    E0x7fef091a7150
                    (not v0x7fef091a3bd0_0)
                    (<= v0x7fef091a6210_0 0.0)
                    (>= v0x7fef091a6210_0 0.0)
                    (<= v0x7fef091a62d0_0 0.0)
                    (>= v0x7fef091a62d0_0 0.0)
                    (<= v0x7fef091a6390_0 v0x7fef091a2f10_0)
                    (>= v0x7fef091a6390_0 v0x7fef091a2f10_0)
                    (and (<= v0x7fef091a6450_0 0.0) (>= v0x7fef091a6450_0 0.0)))))
      (a!8 (=> v0x7fef091a6150_0
               (or (and E0x7fef091a6510
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a6f50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a69d0
                        (not E0x7fef091a6510)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a6f50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a6c50
                        (not E0x7fef091a6510)
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6f50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a6f50
                        (not E0x7fef091a6510)
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a7150
                        (not E0x7fef091a6510)
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a6f50))))))
(let ((a!9 (and (=> v0x7fef091a2710_0
                    (and v0x7fef091a2450_0
                         E0x7fef091a27d0
                         (not v0x7fef091a25d0_0)))
                (=> v0x7fef091a2710_0 E0x7fef091a27d0)
                a!1
                a!2
                (=> v0x7fef091a3890_0
                    (and v0x7fef091a2e50_0 E0x7fef091a3950 v0x7fef091a3750_0))
                (=> v0x7fef091a3890_0 E0x7fef091a3950)
                a!3
                a!4
                (=> v0x7fef091a4390_0
                    (and v0x7fef091a3d10_0
                         E0x7fef091a4450
                         (not v0x7fef091a4250_0)))
                (=> v0x7fef091a4390_0 E0x7fef091a4450)
                a!5
                a!6
                (=> v0x7fef091a5350_0
                    (and v0x7fef091a4a90_0
                         E0x7fef091a5410
                         (not v0x7fef091a5210_0)))
                (=> v0x7fef091a5350_0 E0x7fef091a5410)
                (=> v0x7fef091a5610_0
                    (and v0x7fef091a5350_0 E0x7fef091a56d0 v0x7fef091a4250_0))
                (=> v0x7fef091a5610_0 E0x7fef091a56d0)
                (=> v0x7fef091a5b10_0
                    (and v0x7fef091a5350_0
                         E0x7fef091a5bd0
                         (not v0x7fef091a4250_0)))
                (=> v0x7fef091a5b10_0 E0x7fef091a5bd0)
                (=> v0x7fef091a5ed0_0
                    (and v0x7fef091a5b10_0 E0x7fef091a5f90 v0x7fef091a5d90_0))
                (=> v0x7fef091a5ed0_0 E0x7fef091a5f90)
                (=> v0x7fef091a6150_0 a!7)
                a!8
                v0x7fef091a6150_0
                (not v0x7fef091a7fd0_0)
                (<= v0x7fef091a1e50_0 v0x7fef091a6390_0)
                (>= v0x7fef091a1e50_0 v0x7fef091a6390_0)
                (<= v0x7fef091a1f50_0 v0x7fef091a62d0_0)
                (>= v0x7fef091a1f50_0 v0x7fef091a62d0_0)
                (<= v0x7fef091a0010_0 v0x7fef091a6450_0)
                (>= v0x7fef091a0010_0 v0x7fef091a6450_0)
                (= v0x7fef091a25d0_0 (= v0x7fef091a2510_0 0.0))
                (= v0x7fef091a2a10_0 (< v0x7fef091a1c10_0 2.0))
                (= v0x7fef091a2bd0_0 (ite v0x7fef091a2a10_0 1.0 0.0))
                (= v0x7fef091a2d10_0 (+ v0x7fef091a2bd0_0 v0x7fef091a1c10_0))
                (= v0x7fef091a3750_0 (= v0x7fef091a3690_0 0.0))
                (= v0x7fef091a3bd0_0 (= v0x7fef091a3b10_0 0.0))
                (= v0x7fef091a4250_0 (= v0x7fef091a1e90_0 0.0))
                (= v0x7fef091a4650_0 (> v0x7fef091a2f10_0 0.0))
                (= v0x7fef091a4790_0 (+ v0x7fef091a2f10_0 (- 1.0)))
                (= v0x7fef091a4950_0
                   (ite v0x7fef091a4650_0 v0x7fef091a4790_0 v0x7fef091a2f10_0))
                (= v0x7fef091a5210_0 (= v0x7fef091a1d90_0 0.0))
                (= v0x7fef091a5890_0 (> v0x7fef091a4b50_0 1.0))
                (= v0x7fef091a59d0_0
                   (ite v0x7fef091a5890_0 1.0 v0x7fef091a1e90_0))
                (= v0x7fef091a5d90_0 (= v0x7fef091a4b50_0 0.0))
                (= v0x7fef091a7990_0 (= v0x7fef091a6390_0 2.0))
                (= v0x7fef091a7ad0_0 (= v0x7fef091a6450_0 0.0))
                (= v0x7fef091a7c10_0 (or v0x7fef091a7ad0_0 v0x7fef091a7990_0))
                (= v0x7fef091a7d50_0 (xor v0x7fef091a7c10_0 true))
                (= v0x7fef091a7e90_0 (= v0x7fef091a6210_0 0.0))
                (= v0x7fef091a7fd0_0 (and v0x7fef091a7e90_0 v0x7fef091a7d50_0)))))
  (=> F0x7fef091a8dd0 a!9))))
(assert (=> F0x7fef091a8dd0 F0x7fef091a8bd0))
(assert (let ((a!1 (=> v0x7fef091a2e50_0
               (or (and v0x7fef091a2710_0
                        E0x7fef091a2fd0
                        (<= v0x7fef091a2f10_0 v0x7fef091a2d10_0)
                        (>= v0x7fef091a2f10_0 v0x7fef091a2d10_0))
                   (and v0x7fef091a2450_0
                        E0x7fef091a3190
                        v0x7fef091a25d0_0
                        (<= v0x7fef091a2f10_0 v0x7fef091a1c10_0)
                        (>= v0x7fef091a2f10_0 v0x7fef091a1c10_0)))))
      (a!2 (=> v0x7fef091a2e50_0
               (or (and E0x7fef091a2fd0 (not E0x7fef091a3190))
                   (and E0x7fef091a3190 (not E0x7fef091a2fd0)))))
      (a!3 (=> v0x7fef091a3d10_0
               (or (and v0x7fef091a3890_0 E0x7fef091a3dd0 v0x7fef091a3bd0_0)
                   (and v0x7fef091a2e50_0
                        E0x7fef091a3ed0
                        (not v0x7fef091a3750_0)))))
      (a!4 (=> v0x7fef091a3d10_0
               (or (and E0x7fef091a3dd0 (not E0x7fef091a3ed0))
                   (and E0x7fef091a3ed0 (not E0x7fef091a3dd0)))))
      (a!5 (=> v0x7fef091a4a90_0
               (or (and v0x7fef091a4390_0
                        E0x7fef091a4c10
                        (<= v0x7fef091a4b50_0 v0x7fef091a4950_0)
                        (>= v0x7fef091a4b50_0 v0x7fef091a4950_0))
                   (and v0x7fef091a3d10_0
                        E0x7fef091a4dd0
                        v0x7fef091a4250_0
                        (<= v0x7fef091a4b50_0 v0x7fef091a2f10_0)
                        (>= v0x7fef091a4b50_0 v0x7fef091a2f10_0)))))
      (a!6 (=> v0x7fef091a4a90_0
               (or (and E0x7fef091a4c10 (not E0x7fef091a4dd0))
                   (and E0x7fef091a4dd0 (not E0x7fef091a4c10)))))
      (a!7 (or (and v0x7fef091a5610_0
                    E0x7fef091a6510
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (<= v0x7fef091a6450_0 v0x7fef091a59d0_0)
                    (>= v0x7fef091a6450_0 v0x7fef091a59d0_0))
               (and v0x7fef091a5ed0_0
                    E0x7fef091a69d0
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (and (<= v0x7fef091a6450_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6450_0 v0x7fef091a1e90_0)))
               (and v0x7fef091a5b10_0
                    E0x7fef091a6c50
                    (not v0x7fef091a5d90_0)
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (and (<= v0x7fef091a6450_0 0.0) (>= v0x7fef091a6450_0 0.0)))
               (and v0x7fef091a4a90_0
                    E0x7fef091a6f50
                    v0x7fef091a5210_0
                    (and (<= v0x7fef091a6210_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6210_0 v0x7fef091a1e90_0))
                    (and (<= v0x7fef091a62d0_0 v0x7fef091a1d90_0)
                         (>= v0x7fef091a62d0_0 v0x7fef091a1d90_0))
                    (and (<= v0x7fef091a6390_0 v0x7fef091a4b50_0)
                         (>= v0x7fef091a6390_0 v0x7fef091a4b50_0))
                    (and (<= v0x7fef091a6450_0 v0x7fef091a1e90_0)
                         (>= v0x7fef091a6450_0 v0x7fef091a1e90_0)))
               (and v0x7fef091a3890_0
                    E0x7fef091a7150
                    (not v0x7fef091a3bd0_0)
                    (<= v0x7fef091a6210_0 0.0)
                    (>= v0x7fef091a6210_0 0.0)
                    (<= v0x7fef091a62d0_0 0.0)
                    (>= v0x7fef091a62d0_0 0.0)
                    (<= v0x7fef091a6390_0 v0x7fef091a2f10_0)
                    (>= v0x7fef091a6390_0 v0x7fef091a2f10_0)
                    (and (<= v0x7fef091a6450_0 0.0) (>= v0x7fef091a6450_0 0.0)))))
      (a!8 (=> v0x7fef091a6150_0
               (or (and E0x7fef091a6510
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a6f50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a69d0
                        (not E0x7fef091a6510)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a6f50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a6c50
                        (not E0x7fef091a6510)
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6f50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a6f50
                        (not E0x7fef091a6510)
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a7150))
                   (and E0x7fef091a7150
                        (not E0x7fef091a6510)
                        (not E0x7fef091a69d0)
                        (not E0x7fef091a6c50)
                        (not E0x7fef091a6f50))))))
(let ((a!9 (and (=> v0x7fef091a2710_0
                    (and v0x7fef091a2450_0
                         E0x7fef091a27d0
                         (not v0x7fef091a25d0_0)))
                (=> v0x7fef091a2710_0 E0x7fef091a27d0)
                a!1
                a!2
                (=> v0x7fef091a3890_0
                    (and v0x7fef091a2e50_0 E0x7fef091a3950 v0x7fef091a3750_0))
                (=> v0x7fef091a3890_0 E0x7fef091a3950)
                a!3
                a!4
                (=> v0x7fef091a4390_0
                    (and v0x7fef091a3d10_0
                         E0x7fef091a4450
                         (not v0x7fef091a4250_0)))
                (=> v0x7fef091a4390_0 E0x7fef091a4450)
                a!5
                a!6
                (=> v0x7fef091a5350_0
                    (and v0x7fef091a4a90_0
                         E0x7fef091a5410
                         (not v0x7fef091a5210_0)))
                (=> v0x7fef091a5350_0 E0x7fef091a5410)
                (=> v0x7fef091a5610_0
                    (and v0x7fef091a5350_0 E0x7fef091a56d0 v0x7fef091a4250_0))
                (=> v0x7fef091a5610_0 E0x7fef091a56d0)
                (=> v0x7fef091a5b10_0
                    (and v0x7fef091a5350_0
                         E0x7fef091a5bd0
                         (not v0x7fef091a4250_0)))
                (=> v0x7fef091a5b10_0 E0x7fef091a5bd0)
                (=> v0x7fef091a5ed0_0
                    (and v0x7fef091a5b10_0 E0x7fef091a5f90 v0x7fef091a5d90_0))
                (=> v0x7fef091a5ed0_0 E0x7fef091a5f90)
                (=> v0x7fef091a6150_0 a!7)
                a!8
                v0x7fef091a6150_0
                v0x7fef091a7fd0_0
                (= v0x7fef091a25d0_0 (= v0x7fef091a2510_0 0.0))
                (= v0x7fef091a2a10_0 (< v0x7fef091a1c10_0 2.0))
                (= v0x7fef091a2bd0_0 (ite v0x7fef091a2a10_0 1.0 0.0))
                (= v0x7fef091a2d10_0 (+ v0x7fef091a2bd0_0 v0x7fef091a1c10_0))
                (= v0x7fef091a3750_0 (= v0x7fef091a3690_0 0.0))
                (= v0x7fef091a3bd0_0 (= v0x7fef091a3b10_0 0.0))
                (= v0x7fef091a4250_0 (= v0x7fef091a1e90_0 0.0))
                (= v0x7fef091a4650_0 (> v0x7fef091a2f10_0 0.0))
                (= v0x7fef091a4790_0 (+ v0x7fef091a2f10_0 (- 1.0)))
                (= v0x7fef091a4950_0
                   (ite v0x7fef091a4650_0 v0x7fef091a4790_0 v0x7fef091a2f10_0))
                (= v0x7fef091a5210_0 (= v0x7fef091a1d90_0 0.0))
                (= v0x7fef091a5890_0 (> v0x7fef091a4b50_0 1.0))
                (= v0x7fef091a59d0_0
                   (ite v0x7fef091a5890_0 1.0 v0x7fef091a1e90_0))
                (= v0x7fef091a5d90_0 (= v0x7fef091a4b50_0 0.0))
                (= v0x7fef091a7990_0 (= v0x7fef091a6390_0 2.0))
                (= v0x7fef091a7ad0_0 (= v0x7fef091a6450_0 0.0))
                (= v0x7fef091a7c10_0 (or v0x7fef091a7ad0_0 v0x7fef091a7990_0))
                (= v0x7fef091a7d50_0 (xor v0x7fef091a7c10_0 true))
                (= v0x7fef091a7e90_0 (= v0x7fef091a6210_0 0.0))
                (= v0x7fef091a7fd0_0 (and v0x7fef091a7e90_0 v0x7fef091a7d50_0)))))
  (=> F0x7fef091a8c90 a!9))))
(assert (=> F0x7fef091a8c90 F0x7fef091a8bd0))
(assert (=> F0x7fef091a8b10 (or F0x7fef091a8f10 F0x7fef091a8dd0)))
(assert (=> F0x7fef091a8b50 F0x7fef091a8c90))
(assert (=> pre!entry!0 (=> F0x7fef091a8e90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fef091a8bd0 (>= v0x7fef091a1d90_0 0.0))))
(assert (let ((a!1 (=> F0x7fef091a8bd0
               (or (>= v0x7fef091a1c10_0 0.0) (not (<= 1.0 v0x7fef091a1d90_0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2
    (=> F0x7fef091a8bd0
        (or (>= v0x7fef091a1c10_0 1.0)
            (<= v0x7fef091a1c10_0 0.0)
            (<= v0x7fef091a1d90_0 0.0)))))
(assert (let ((a!1 (=> F0x7fef091a8bd0
               (or (not (<= 1.0 v0x7fef091a1d90_0)) (<= v0x7fef091a1c10_0 2.0)))))
  (=> pre!bb1.i.i!3 a!1)))
(assert (=> pre!bb1.i.i!4
    (=> F0x7fef091a8bd0
        (or (<= v0x7fef091a1c10_0 1.0)
            (<= v0x7fef091a1d90_0 0.0)
            (>= v0x7fef091a1c10_0 2.0)))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7fef091a8bd0
        (or (>= v0x7fef091a1d90_0 1.0) (<= v0x7fef091a1d90_0 0.0)))))
(assert (let ((a!1 (not (or (>= v0x7fef091a1e50_0 0.0) (not (<= 1.0 v0x7fef091a1f50_0)))))
      (a!2 (and (not post!bb1.i.i!2)
                F0x7fef091a8b10
                (not (or (>= v0x7fef091a1e50_0 1.0)
                         (<= v0x7fef091a1e50_0 0.0)
                         (<= v0x7fef091a1f50_0 0.0)))))
      (a!3 (not (or (not (<= 1.0 v0x7fef091a1f50_0)) (<= v0x7fef091a1e50_0 2.0))))
      (a!4 (and (not post!bb1.i.i!4)
                F0x7fef091a8b10
                (not (or (<= v0x7fef091a1e50_0 1.0)
                         (<= v0x7fef091a1f50_0 0.0)
                         (>= v0x7fef091a1e50_0 2.0)))))
      (a!5 (and (not post!bb1.i.i!5)
                F0x7fef091a8b10
                (not (or (>= v0x7fef091a1f50_0 1.0) (<= v0x7fef091a1f50_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7fef091a8b10
           (not (>= v0x7fef091a1f50_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7fef091a8b10 a!1)
      a!2
      (and (not post!bb1.i.i!3) F0x7fef091a8b10 a!3)
      a!4
      a!5
      (and (not post!bb2.i.i28.i.i!0) F0x7fef091a8b50 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i28.i.i!0)
