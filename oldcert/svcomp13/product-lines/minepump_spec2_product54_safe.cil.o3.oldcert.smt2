(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f0e60491390 () Bool)
(declare-fun v0x7f0e6048b310_0 () Real)
(declare-fun v0x7f0e6048fc90_0 () Bool)
(declare-fun v0x7f0e6048e990_0 () Bool)
(declare-fun v0x7f0e6048e550_0 () Real)
(declare-fun v0x7f0e6048cb90_0 () Real)
(declare-fun v0x7f0e6048c190_0 () Real)
(declare-fun E0x7f0e6048ffd0 () Bool)
(declare-fun v0x7f0e6048ff10_0 () Bool)
(declare-fun E0x7f0e6048f650 () Bool)
(declare-fun E0x7f0e6048f390 () Bool)
(declare-fun v0x7f0e6048e710_0 () Real)
(declare-fun v0x7f0e6048e010_0 () Real)
(declare-fun v0x7f0e604901d0_0 () Bool)
(declare-fun v0x7f0e6048ef50_0 () Real)
(declare-fun v0x7f0e6048bfd0_0 () Bool)
(declare-fun v0x7f0e6048ead0_0 () Bool)
(declare-fun v0x7f0e6048ded0_0 () Bool)
(declare-fun E0x7f0e6048ecd0 () Bool)
(declare-fun F0x7f0e60491250 () Bool)
(declare-fun v0x7f0e6048ec10_0 () Bool)
(declare-fun E0x7f0e6048e210 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f0e6048dd10 () Bool)
(declare-fun v0x7f0e6048db10_0 () Bool)
(declare-fun v0x7f0e6048dc50_0 () Bool)
(declare-fun v0x7f0e6048bad0_0 () Real)
(declare-fun F0x7f0e60491190 () Bool)
(declare-fun v0x7f0e6048e150_0 () Bool)
(declare-fun v0x7f0e6048af90_0 () Real)
(declare-fun E0x7f0e6048d610 () Bool)
(declare-fun v0x7f0e6048d390_0 () Real)
(declare-fun E0x7f0e6048d450 () Bool)
(declare-fun v0x7f0e6048d2d0_0 () Bool)
(declare-fun v0x7f0e6048cc50_0 () Bool)
(declare-fun E0x7f0e6048ce50 () Bool)
(declare-fun v0x7f0e6048cd90_0 () Bool)
(declare-fun v0x7f0e6048d050_0 () Bool)
(declare-fun v0x7f0e6048b110_0 () Real)
(declare-fun v0x7f0e6048d190_0 () Real)
(declare-fun E0x7f0e6048c750 () Bool)
(declare-fun v0x7f0e6048fb90_0 () Bool)
(declare-fun v0x7f0e6048c2d0_0 () Real)
(declare-fun v0x7f0e6048c410_0 () Bool)
(declare-fun v0x7f0e6048b210_0 () Real)
(declare-fun v0x7f0e6048bb90_0 () Bool)
(declare-fun E0x7f0e6048bd90 () Bool)
(declare-fun v0x7f0e6048bcd0_0 () Bool)
(declare-fun v0x7f0e6048ee90_0 () Bool)
(declare-fun F0x7f0e604910d0 () Bool)
(declare-fun v0x7f0e6048c4d0_0 () Real)
(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun F0x7f0e60490fd0 () Bool)
(declare-fun E0x7f0e6048f0d0 () Bool)
(declare-fun v0x7f0e6048f010_0 () Real)
(declare-fun v0x7f0e6048b2d0_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f0e6048fdd0_0 () Bool)
(declare-fun F0x7f0e60491350 () Bool)
(declare-fun v0x7f0e6048ba10_0 () Bool)
(declare-fun v0x7f0e6048b3d0_0 () Real)
(declare-fun v0x7f0e6048b1d0_0 () Real)
(declare-fun v0x7f0e6048e850_0 () Bool)
(declare-fun v0x7f0e60488110_0 () Bool)
(declare-fun v0x7f0e60488010_0 () Real)
(declare-fun v0x7f0e6048e410_0 () Bool)
(declare-fun F0x7f0e60491090 () Bool)
(declare-fun E0x7f0e6048c590 () Bool)

(assert (=> F0x7f0e60491090
    (and v0x7f0e60488110_0
         (<= v0x7f0e6048b1d0_0 0.0)
         (>= v0x7f0e6048b1d0_0 0.0)
         (<= v0x7f0e6048b2d0_0 1.0)
         (>= v0x7f0e6048b2d0_0 1.0)
         (<= v0x7f0e6048b3d0_0 0.0)
         (>= v0x7f0e6048b3d0_0 0.0)
         (<= v0x7f0e60488010_0 0.0)
         (>= v0x7f0e60488010_0 0.0))))
(assert (=> F0x7f0e60491090 F0x7f0e60490fd0))
(assert (let ((a!1 (=> v0x7f0e6048c410_0
               (or (and v0x7f0e6048bcd0_0
                        E0x7f0e6048c590
                        (<= v0x7f0e6048c4d0_0 v0x7f0e6048c2d0_0)
                        (>= v0x7f0e6048c4d0_0 v0x7f0e6048c2d0_0))
                   (and v0x7f0e6048ba10_0
                        E0x7f0e6048c750
                        v0x7f0e6048bb90_0
                        (<= v0x7f0e6048c4d0_0 v0x7f0e6048b110_0)
                        (>= v0x7f0e6048c4d0_0 v0x7f0e6048b110_0)))))
      (a!2 (=> v0x7f0e6048c410_0
               (or (and E0x7f0e6048c590 (not E0x7f0e6048c750))
                   (and E0x7f0e6048c750 (not E0x7f0e6048c590)))))
      (a!3 (=> v0x7f0e6048d2d0_0
               (or (and v0x7f0e6048cd90_0
                        E0x7f0e6048d450
                        (<= v0x7f0e6048d390_0 v0x7f0e6048d190_0)
                        (>= v0x7f0e6048d390_0 v0x7f0e6048d190_0))
                   (and v0x7f0e6048c410_0
                        E0x7f0e6048d610
                        v0x7f0e6048cc50_0
                        (<= v0x7f0e6048d390_0 v0x7f0e6048af90_0)
                        (>= v0x7f0e6048d390_0 v0x7f0e6048af90_0)))))
      (a!4 (=> v0x7f0e6048d2d0_0
               (or (and E0x7f0e6048d450 (not E0x7f0e6048d610))
                   (and E0x7f0e6048d610 (not E0x7f0e6048d450)))))
      (a!5 (or (and v0x7f0e6048dc50_0
                    E0x7f0e6048f0d0
                    (<= v0x7f0e6048ef50_0 v0x7f0e6048c4d0_0)
                    (>= v0x7f0e6048ef50_0 v0x7f0e6048c4d0_0)
                    (<= v0x7f0e6048f010_0 v0x7f0e6048e010_0)
                    (>= v0x7f0e6048f010_0 v0x7f0e6048e010_0))
               (and v0x7f0e6048ec10_0
                    E0x7f0e6048f390
                    (and (<= v0x7f0e6048ef50_0 v0x7f0e6048e710_0)
                         (>= v0x7f0e6048ef50_0 v0x7f0e6048e710_0))
                    (<= v0x7f0e6048f010_0 v0x7f0e6048b210_0)
                    (>= v0x7f0e6048f010_0 v0x7f0e6048b210_0))
               (and v0x7f0e6048e150_0
                    E0x7f0e6048f650
                    (not v0x7f0e6048ead0_0)
                    (and (<= v0x7f0e6048ef50_0 v0x7f0e6048e710_0)
                         (>= v0x7f0e6048ef50_0 v0x7f0e6048e710_0))
                    (<= v0x7f0e6048f010_0 0.0)
                    (>= v0x7f0e6048f010_0 0.0))))
      (a!6 (=> v0x7f0e6048ee90_0
               (or (and E0x7f0e6048f0d0
                        (not E0x7f0e6048f390)
                        (not E0x7f0e6048f650))
                   (and E0x7f0e6048f390
                        (not E0x7f0e6048f0d0)
                        (not E0x7f0e6048f650))
                   (and E0x7f0e6048f650
                        (not E0x7f0e6048f0d0)
                        (not E0x7f0e6048f390)))))
      (a!7 (or (and v0x7f0e6048ff10_0
                    v0x7f0e604901d0_0
                    (and (<= v0x7f0e6048b1d0_0 v0x7f0e6048d390_0)
                         (>= v0x7f0e6048b1d0_0 v0x7f0e6048d390_0))
                    (and (<= v0x7f0e6048b2d0_0 v0x7f0e6048ef50_0)
                         (>= v0x7f0e6048b2d0_0 v0x7f0e6048ef50_0))
                    (and (<= v0x7f0e6048b3d0_0 v0x7f0e6048f010_0)
                         (>= v0x7f0e6048b3d0_0 v0x7f0e6048f010_0))
                    (<= v0x7f0e60488010_0 1.0)
                    (>= v0x7f0e60488010_0 1.0))
               (and v0x7f0e6048ee90_0
                    v0x7f0e6048fdd0_0
                    (and (<= v0x7f0e6048b1d0_0 v0x7f0e6048d390_0)
                         (>= v0x7f0e6048b1d0_0 v0x7f0e6048d390_0))
                    (and (<= v0x7f0e6048b2d0_0 v0x7f0e6048ef50_0)
                         (>= v0x7f0e6048b2d0_0 v0x7f0e6048ef50_0))
                    (and (<= v0x7f0e6048b3d0_0 v0x7f0e6048f010_0)
                         (>= v0x7f0e6048b3d0_0 v0x7f0e6048f010_0))
                    (<= v0x7f0e60488010_0 0.0)
                    (>= v0x7f0e60488010_0 0.0)))))
(let ((a!8 (and (=> v0x7f0e6048bcd0_0
                    (and v0x7f0e6048ba10_0
                         E0x7f0e6048bd90
                         (not v0x7f0e6048bb90_0)))
                (=> v0x7f0e6048bcd0_0 E0x7f0e6048bd90)
                a!1
                a!2
                (=> v0x7f0e6048cd90_0
                    (and v0x7f0e6048c410_0
                         E0x7f0e6048ce50
                         (not v0x7f0e6048cc50_0)))
                (=> v0x7f0e6048cd90_0 E0x7f0e6048ce50)
                a!3
                a!4
                (=> v0x7f0e6048dc50_0
                    (and v0x7f0e6048d2d0_0 E0x7f0e6048dd10 v0x7f0e6048db10_0))
                (=> v0x7f0e6048dc50_0 E0x7f0e6048dd10)
                (=> v0x7f0e6048e150_0
                    (and v0x7f0e6048d2d0_0
                         E0x7f0e6048e210
                         (not v0x7f0e6048db10_0)))
                (=> v0x7f0e6048e150_0 E0x7f0e6048e210)
                (=> v0x7f0e6048ec10_0
                    (and v0x7f0e6048e150_0 E0x7f0e6048ecd0 v0x7f0e6048ead0_0))
                (=> v0x7f0e6048ec10_0 E0x7f0e6048ecd0)
                (=> v0x7f0e6048ee90_0 a!5)
                a!6
                (=> v0x7f0e6048ff10_0
                    (and v0x7f0e6048ee90_0
                         E0x7f0e6048ffd0
                         (not v0x7f0e6048fdd0_0)))
                (=> v0x7f0e6048ff10_0 E0x7f0e6048ffd0)
                a!7
                (= v0x7f0e6048bb90_0 (= v0x7f0e6048bad0_0 0.0))
                (= v0x7f0e6048bfd0_0 (< v0x7f0e6048b110_0 2.0))
                (= v0x7f0e6048c190_0 (ite v0x7f0e6048bfd0_0 1.0 0.0))
                (= v0x7f0e6048c2d0_0 (+ v0x7f0e6048c190_0 v0x7f0e6048b110_0))
                (= v0x7f0e6048cc50_0 (= v0x7f0e6048cb90_0 0.0))
                (= v0x7f0e6048d050_0 (= v0x7f0e6048af90_0 0.0))
                (= v0x7f0e6048d190_0 (ite v0x7f0e6048d050_0 1.0 0.0))
                (= v0x7f0e6048db10_0 (= v0x7f0e6048b210_0 0.0))
                (= v0x7f0e6048ded0_0 (> v0x7f0e6048c4d0_0 1.0))
                (= v0x7f0e6048e010_0
                   (ite v0x7f0e6048ded0_0 1.0 v0x7f0e6048b210_0))
                (= v0x7f0e6048e410_0 (> v0x7f0e6048c4d0_0 0.0))
                (= v0x7f0e6048e550_0 (+ v0x7f0e6048c4d0_0 (- 1.0)))
                (= v0x7f0e6048e710_0
                   (ite v0x7f0e6048e410_0 v0x7f0e6048e550_0 v0x7f0e6048c4d0_0))
                (= v0x7f0e6048e850_0 (= v0x7f0e6048d390_0 0.0))
                (= v0x7f0e6048e990_0 (= v0x7f0e6048e710_0 0.0))
                (= v0x7f0e6048ead0_0 (and v0x7f0e6048e850_0 v0x7f0e6048e990_0))
                (= v0x7f0e6048fb90_0 (= v0x7f0e6048d390_0 0.0))
                (= v0x7f0e6048fc90_0 (= v0x7f0e6048f010_0 0.0))
                (= v0x7f0e6048fdd0_0 (or v0x7f0e6048fc90_0 v0x7f0e6048fb90_0))
                (= v0x7f0e604901d0_0 (= v0x7f0e6048b310_0 0.0)))))
  (=> F0x7f0e604910d0 a!8))))
(assert (=> F0x7f0e604910d0 F0x7f0e60491190))
(assert (let ((a!1 (=> v0x7f0e6048c410_0
               (or (and v0x7f0e6048bcd0_0
                        E0x7f0e6048c590
                        (<= v0x7f0e6048c4d0_0 v0x7f0e6048c2d0_0)
                        (>= v0x7f0e6048c4d0_0 v0x7f0e6048c2d0_0))
                   (and v0x7f0e6048ba10_0
                        E0x7f0e6048c750
                        v0x7f0e6048bb90_0
                        (<= v0x7f0e6048c4d0_0 v0x7f0e6048b110_0)
                        (>= v0x7f0e6048c4d0_0 v0x7f0e6048b110_0)))))
      (a!2 (=> v0x7f0e6048c410_0
               (or (and E0x7f0e6048c590 (not E0x7f0e6048c750))
                   (and E0x7f0e6048c750 (not E0x7f0e6048c590)))))
      (a!3 (=> v0x7f0e6048d2d0_0
               (or (and v0x7f0e6048cd90_0
                        E0x7f0e6048d450
                        (<= v0x7f0e6048d390_0 v0x7f0e6048d190_0)
                        (>= v0x7f0e6048d390_0 v0x7f0e6048d190_0))
                   (and v0x7f0e6048c410_0
                        E0x7f0e6048d610
                        v0x7f0e6048cc50_0
                        (<= v0x7f0e6048d390_0 v0x7f0e6048af90_0)
                        (>= v0x7f0e6048d390_0 v0x7f0e6048af90_0)))))
      (a!4 (=> v0x7f0e6048d2d0_0
               (or (and E0x7f0e6048d450 (not E0x7f0e6048d610))
                   (and E0x7f0e6048d610 (not E0x7f0e6048d450)))))
      (a!5 (or (and v0x7f0e6048dc50_0
                    E0x7f0e6048f0d0
                    (<= v0x7f0e6048ef50_0 v0x7f0e6048c4d0_0)
                    (>= v0x7f0e6048ef50_0 v0x7f0e6048c4d0_0)
                    (<= v0x7f0e6048f010_0 v0x7f0e6048e010_0)
                    (>= v0x7f0e6048f010_0 v0x7f0e6048e010_0))
               (and v0x7f0e6048ec10_0
                    E0x7f0e6048f390
                    (and (<= v0x7f0e6048ef50_0 v0x7f0e6048e710_0)
                         (>= v0x7f0e6048ef50_0 v0x7f0e6048e710_0))
                    (<= v0x7f0e6048f010_0 v0x7f0e6048b210_0)
                    (>= v0x7f0e6048f010_0 v0x7f0e6048b210_0))
               (and v0x7f0e6048e150_0
                    E0x7f0e6048f650
                    (not v0x7f0e6048ead0_0)
                    (and (<= v0x7f0e6048ef50_0 v0x7f0e6048e710_0)
                         (>= v0x7f0e6048ef50_0 v0x7f0e6048e710_0))
                    (<= v0x7f0e6048f010_0 0.0)
                    (>= v0x7f0e6048f010_0 0.0))))
      (a!6 (=> v0x7f0e6048ee90_0
               (or (and E0x7f0e6048f0d0
                        (not E0x7f0e6048f390)
                        (not E0x7f0e6048f650))
                   (and E0x7f0e6048f390
                        (not E0x7f0e6048f0d0)
                        (not E0x7f0e6048f650))
                   (and E0x7f0e6048f650
                        (not E0x7f0e6048f0d0)
                        (not E0x7f0e6048f390))))))
(let ((a!7 (and (=> v0x7f0e6048bcd0_0
                    (and v0x7f0e6048ba10_0
                         E0x7f0e6048bd90
                         (not v0x7f0e6048bb90_0)))
                (=> v0x7f0e6048bcd0_0 E0x7f0e6048bd90)
                a!1
                a!2
                (=> v0x7f0e6048cd90_0
                    (and v0x7f0e6048c410_0
                         E0x7f0e6048ce50
                         (not v0x7f0e6048cc50_0)))
                (=> v0x7f0e6048cd90_0 E0x7f0e6048ce50)
                a!3
                a!4
                (=> v0x7f0e6048dc50_0
                    (and v0x7f0e6048d2d0_0 E0x7f0e6048dd10 v0x7f0e6048db10_0))
                (=> v0x7f0e6048dc50_0 E0x7f0e6048dd10)
                (=> v0x7f0e6048e150_0
                    (and v0x7f0e6048d2d0_0
                         E0x7f0e6048e210
                         (not v0x7f0e6048db10_0)))
                (=> v0x7f0e6048e150_0 E0x7f0e6048e210)
                (=> v0x7f0e6048ec10_0
                    (and v0x7f0e6048e150_0 E0x7f0e6048ecd0 v0x7f0e6048ead0_0))
                (=> v0x7f0e6048ec10_0 E0x7f0e6048ecd0)
                (=> v0x7f0e6048ee90_0 a!5)
                a!6
                (=> v0x7f0e6048ff10_0
                    (and v0x7f0e6048ee90_0
                         E0x7f0e6048ffd0
                         (not v0x7f0e6048fdd0_0)))
                (=> v0x7f0e6048ff10_0 E0x7f0e6048ffd0)
                v0x7f0e6048ff10_0
                (not v0x7f0e604901d0_0)
                (= v0x7f0e6048bb90_0 (= v0x7f0e6048bad0_0 0.0))
                (= v0x7f0e6048bfd0_0 (< v0x7f0e6048b110_0 2.0))
                (= v0x7f0e6048c190_0 (ite v0x7f0e6048bfd0_0 1.0 0.0))
                (= v0x7f0e6048c2d0_0 (+ v0x7f0e6048c190_0 v0x7f0e6048b110_0))
                (= v0x7f0e6048cc50_0 (= v0x7f0e6048cb90_0 0.0))
                (= v0x7f0e6048d050_0 (= v0x7f0e6048af90_0 0.0))
                (= v0x7f0e6048d190_0 (ite v0x7f0e6048d050_0 1.0 0.0))
                (= v0x7f0e6048db10_0 (= v0x7f0e6048b210_0 0.0))
                (= v0x7f0e6048ded0_0 (> v0x7f0e6048c4d0_0 1.0))
                (= v0x7f0e6048e010_0
                   (ite v0x7f0e6048ded0_0 1.0 v0x7f0e6048b210_0))
                (= v0x7f0e6048e410_0 (> v0x7f0e6048c4d0_0 0.0))
                (= v0x7f0e6048e550_0 (+ v0x7f0e6048c4d0_0 (- 1.0)))
                (= v0x7f0e6048e710_0
                   (ite v0x7f0e6048e410_0 v0x7f0e6048e550_0 v0x7f0e6048c4d0_0))
                (= v0x7f0e6048e850_0 (= v0x7f0e6048d390_0 0.0))
                (= v0x7f0e6048e990_0 (= v0x7f0e6048e710_0 0.0))
                (= v0x7f0e6048ead0_0 (and v0x7f0e6048e850_0 v0x7f0e6048e990_0))
                (= v0x7f0e6048fb90_0 (= v0x7f0e6048d390_0 0.0))
                (= v0x7f0e6048fc90_0 (= v0x7f0e6048f010_0 0.0))
                (= v0x7f0e6048fdd0_0 (or v0x7f0e6048fc90_0 v0x7f0e6048fb90_0))
                (= v0x7f0e604901d0_0 (= v0x7f0e6048b310_0 0.0)))))
  (=> F0x7f0e60491250 a!7))))
(assert (=> F0x7f0e60491250 F0x7f0e60491190))
(assert (=> F0x7f0e60491390 (or F0x7f0e60491090 F0x7f0e604910d0)))
(assert (=> F0x7f0e60491350 F0x7f0e60491250))
(assert (=> pre!entry!0 (=> F0x7f0e60490fd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f0e60491190 (>= v0x7f0e6048b310_0 0.0))))
(assert (let ((a!1 (=> F0x7f0e60491190
               (or (<= v0x7f0e6048b310_0 0.0) (not (<= v0x7f0e6048b210_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7f0e60488010_0 0.0) (not (<= v0x7f0e6048b3d0_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f0e60491390
           (not (>= v0x7f0e60488010_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f0e60491390 a!1)
      (and (not post!bb2.i.i35.i.i!0) F0x7f0e60491350 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
