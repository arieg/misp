(declare-fun post!bb2.i.i31.i.i!0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f9945a0bd10_0 () Bool)
(declare-fun v0x7f9945a0ba90_0 () Bool)
(declare-fun F0x7f9945a0ce90 () Bool)
(declare-fun v0x7f9945a0a690_0 () Bool)
(declare-fun v0x7f9945a09fd0_0 () Real)
(declare-fun v0x7f9945a08c10_0 () Bool)
(declare-fun v0x7f9945a07b90_0 () Bool)
(declare-fun v0x7f9945a07690_0 () Real)
(declare-fun v0x7f9945a08750_0 () Real)
(declare-fun v0x7f9945a0bf90_0 () Bool)
(declare-fun E0x7f9945a0b050 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7f9945a0a790_0 () Real)
(declare-fun v0x7f9945a0acd0_0 () Real)
(declare-fun E0x7f9945a0ad90 () Bool)
(declare-fun v0x7f9945a0bbd0_0 () Bool)
(declare-fun F0x7f9945a0ced0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun v0x7f9945a0a2d0_0 () Bool)
(declare-fun v0x7f9945a0a8d0_0 () Bool)
(declare-fun v0x7f9945a09a90_0 () Bool)
(declare-fun F0x7f9945a0cad0 () Bool)
(declare-fun v0x7f9945a07d50_0 () Real)
(declare-fun E0x7f9945a0a4d0 () Bool)
(declare-fun v0x7f9945a0a410_0 () Bool)
(declare-fun v0x7f9945a096d0_0 () Bool)
(declare-fun E0x7f9945a098d0 () Bool)
(declare-fun v0x7f9945a06f10_0 () Real)
(declare-fun v0x7f9945a08f50_0 () Real)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun E0x7f9945a091d0 () Bool)
(declare-fun v0x7f9945a08e90_0 () Bool)
(declare-fun v0x7f9945a08d50_0 () Real)
(declare-fun E0x7f9945a08310 () Bool)
(declare-fun E0x7f9945a09c90 () Bool)
(declare-fun v0x7f9945a07e90_0 () Real)
(declare-fun v0x7f9945a08810_0 () Bool)
(declare-fun v0x7f9945a09e90_0 () Bool)
(declare-fun v0x7f9945a08090_0 () Real)
(declare-fun E0x7f9945a0b2d0 () Bool)
(declare-fun v0x7f9945a06d90_0 () Real)
(declare-fun v0x7f9945a0a190_0 () Real)
(declare-fun F0x7f9945a0cdd0 () Bool)
(declare-fun v0x7f9945a07750_0 () Bool)
(declare-fun v0x7f9945a0be50_0 () Bool)
(declare-fun v0x7f9945a08950_0 () Bool)
(declare-fun E0x7f9945a07950 () Bool)
(declare-fun v0x7f9945a075d0_0 () Bool)
(declare-fun v0x7f9945a0ab50_0 () Bool)
(declare-fun v0x7f9945a07890_0 () Bool)
(declare-fun v0x7f9945a09810_0 () Bool)
(declare-fun F0x7f9945a0cb90 () Bool)
(declare-fun E0x7f9945a0b4d0 () Bool)
(declare-fun v0x7f9945a0ac10_0 () Real)
(declare-fun E0x7f9945a08a10 () Bool)
(declare-fun E0x7f9945a0a990 () Bool)
(declare-fun F0x7f9945a0cc50 () Bool)
(declare-fun v0x7f9945a07010_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f9945a070d0_0 () Real)
(declare-fun E0x7f9945a08150 () Bool)
(declare-fun v0x7f9945a07fd0_0 () Bool)
(declare-fun v0x7f9945a09bd0_0 () Bool)
(declare-fun v0x7f9945a06fd0_0 () Real)
(declare-fun v0x7f9945a05110_0 () Bool)
(declare-fun v0x7f9945a05010_0 () Real)
(declare-fun E0x7f9945a09010 () Bool)
(declare-fun F0x7f9945a0cd10 () Bool)

(assert (=> F0x7f9945a0cd10
    (and v0x7f9945a05110_0
         (<= v0x7f9945a06fd0_0 0.0)
         (>= v0x7f9945a06fd0_0 0.0)
         (<= v0x7f9945a070d0_0 0.0)
         (>= v0x7f9945a070d0_0 0.0)
         (<= v0x7f9945a05010_0 1.0)
         (>= v0x7f9945a05010_0 1.0))))
(assert (=> F0x7f9945a0cd10 F0x7f9945a0cc50))
(assert (let ((a!1 (=> v0x7f9945a07fd0_0
               (or (and v0x7f9945a07890_0
                        E0x7f9945a08150
                        (<= v0x7f9945a08090_0 v0x7f9945a07e90_0)
                        (>= v0x7f9945a08090_0 v0x7f9945a07e90_0))
                   (and v0x7f9945a075d0_0
                        E0x7f9945a08310
                        v0x7f9945a07750_0
                        (<= v0x7f9945a08090_0 v0x7f9945a07010_0)
                        (>= v0x7f9945a08090_0 v0x7f9945a07010_0)))))
      (a!2 (=> v0x7f9945a07fd0_0
               (or (and E0x7f9945a08150 (not E0x7f9945a08310))
                   (and E0x7f9945a08310 (not E0x7f9945a08150)))))
      (a!3 (=> v0x7f9945a08e90_0
               (or (and v0x7f9945a08950_0
                        E0x7f9945a09010
                        (<= v0x7f9945a08f50_0 v0x7f9945a08d50_0)
                        (>= v0x7f9945a08f50_0 v0x7f9945a08d50_0))
                   (and v0x7f9945a07fd0_0
                        E0x7f9945a091d0
                        v0x7f9945a08810_0
                        (<= v0x7f9945a08f50_0 v0x7f9945a06f10_0)
                        (>= v0x7f9945a08f50_0 v0x7f9945a06f10_0)))))
      (a!4 (=> v0x7f9945a08e90_0
               (or (and E0x7f9945a09010 (not E0x7f9945a091d0))
                   (and E0x7f9945a091d0 (not E0x7f9945a09010)))))
      (a!5 (or (and v0x7f9945a0a410_0
                    E0x7f9945a0ad90
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a08090_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a08090_0))
                    (<= v0x7f9945a0acd0_0 v0x7f9945a0a790_0)
                    (>= v0x7f9945a0acd0_0 v0x7f9945a0a790_0))
               (and v0x7f9945a09810_0
                    E0x7f9945a0b050
                    (not v0x7f9945a09a90_0)
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a08090_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a08090_0))
                    (and (<= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)
                         (>= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)))
               (and v0x7f9945a0a8d0_0
                    E0x7f9945a0b2d0
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a0a190_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a0a190_0))
                    (and (<= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)
                         (>= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)))
               (and v0x7f9945a09bd0_0
                    E0x7f9945a0b4d0
                    (not v0x7f9945a0a2d0_0)
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a0a190_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a0a190_0))
                    (<= v0x7f9945a0acd0_0 0.0)
                    (>= v0x7f9945a0acd0_0 0.0))))
      (a!6 (=> v0x7f9945a0ab50_0
               (or (and E0x7f9945a0ad90
                        (not E0x7f9945a0b050)
                        (not E0x7f9945a0b2d0)
                        (not E0x7f9945a0b4d0))
                   (and E0x7f9945a0b050
                        (not E0x7f9945a0ad90)
                        (not E0x7f9945a0b2d0)
                        (not E0x7f9945a0b4d0))
                   (and E0x7f9945a0b2d0
                        (not E0x7f9945a0ad90)
                        (not E0x7f9945a0b050)
                        (not E0x7f9945a0b4d0))
                   (and E0x7f9945a0b4d0
                        (not E0x7f9945a0ad90)
                        (not E0x7f9945a0b050)
                        (not E0x7f9945a0b2d0))))))
(let ((a!7 (and (=> v0x7f9945a07890_0
                    (and v0x7f9945a075d0_0
                         E0x7f9945a07950
                         (not v0x7f9945a07750_0)))
                (=> v0x7f9945a07890_0 E0x7f9945a07950)
                a!1
                a!2
                (=> v0x7f9945a08950_0
                    (and v0x7f9945a07fd0_0
                         E0x7f9945a08a10
                         (not v0x7f9945a08810_0)))
                (=> v0x7f9945a08950_0 E0x7f9945a08a10)
                a!3
                a!4
                (=> v0x7f9945a09810_0
                    (and v0x7f9945a08e90_0 E0x7f9945a098d0 v0x7f9945a096d0_0))
                (=> v0x7f9945a09810_0 E0x7f9945a098d0)
                (=> v0x7f9945a09bd0_0
                    (and v0x7f9945a08e90_0
                         E0x7f9945a09c90
                         (not v0x7f9945a096d0_0)))
                (=> v0x7f9945a09bd0_0 E0x7f9945a09c90)
                (=> v0x7f9945a0a410_0
                    (and v0x7f9945a09810_0 E0x7f9945a0a4d0 v0x7f9945a09a90_0))
                (=> v0x7f9945a0a410_0 E0x7f9945a0a4d0)
                (=> v0x7f9945a0a8d0_0
                    (and v0x7f9945a09bd0_0 E0x7f9945a0a990 v0x7f9945a0a2d0_0))
                (=> v0x7f9945a0a8d0_0 E0x7f9945a0a990)
                (=> v0x7f9945a0ab50_0 a!5)
                a!6
                v0x7f9945a0ab50_0
                (not v0x7f9945a0bf90_0)
                (<= v0x7f9945a06fd0_0 v0x7f9945a0acd0_0)
                (>= v0x7f9945a06fd0_0 v0x7f9945a0acd0_0)
                (<= v0x7f9945a070d0_0 v0x7f9945a08f50_0)
                (>= v0x7f9945a070d0_0 v0x7f9945a08f50_0)
                (<= v0x7f9945a05010_0 v0x7f9945a0ac10_0)
                (>= v0x7f9945a05010_0 v0x7f9945a0ac10_0)
                (= v0x7f9945a07750_0 (= v0x7f9945a07690_0 0.0))
                (= v0x7f9945a07b90_0 (< v0x7f9945a07010_0 2.0))
                (= v0x7f9945a07d50_0 (ite v0x7f9945a07b90_0 1.0 0.0))
                (= v0x7f9945a07e90_0 (+ v0x7f9945a07d50_0 v0x7f9945a07010_0))
                (= v0x7f9945a08810_0 (= v0x7f9945a08750_0 0.0))
                (= v0x7f9945a08c10_0 (= v0x7f9945a06f10_0 0.0))
                (= v0x7f9945a08d50_0 (ite v0x7f9945a08c10_0 1.0 0.0))
                (= v0x7f9945a096d0_0 (= v0x7f9945a06d90_0 0.0))
                (= v0x7f9945a09a90_0 (> v0x7f9945a08090_0 1.0))
                (= v0x7f9945a09e90_0 (> v0x7f9945a08090_0 0.0))
                (= v0x7f9945a09fd0_0 (+ v0x7f9945a08090_0 (- 1.0)))
                (= v0x7f9945a0a190_0
                   (ite v0x7f9945a09e90_0 v0x7f9945a09fd0_0 v0x7f9945a08090_0))
                (= v0x7f9945a0a2d0_0 (= v0x7f9945a08f50_0 0.0))
                (= v0x7f9945a0a690_0 (= v0x7f9945a08f50_0 0.0))
                (= v0x7f9945a0a790_0
                   (ite v0x7f9945a0a690_0 1.0 v0x7f9945a06d90_0))
                (= v0x7f9945a0ba90_0 (= v0x7f9945a0ac10_0 2.0))
                (= v0x7f9945a0bbd0_0 (= v0x7f9945a0acd0_0 0.0))
                (= v0x7f9945a0bd10_0 (or v0x7f9945a0bbd0_0 v0x7f9945a0ba90_0))
                (= v0x7f9945a0be50_0 (xor v0x7f9945a0bd10_0 true))
                (= v0x7f9945a0bf90_0 (and v0x7f9945a096d0_0 v0x7f9945a0be50_0)))))
  (=> F0x7f9945a0cb90 a!7))))
(assert (=> F0x7f9945a0cb90 F0x7f9945a0cad0))
(assert (let ((a!1 (=> v0x7f9945a07fd0_0
               (or (and v0x7f9945a07890_0
                        E0x7f9945a08150
                        (<= v0x7f9945a08090_0 v0x7f9945a07e90_0)
                        (>= v0x7f9945a08090_0 v0x7f9945a07e90_0))
                   (and v0x7f9945a075d0_0
                        E0x7f9945a08310
                        v0x7f9945a07750_0
                        (<= v0x7f9945a08090_0 v0x7f9945a07010_0)
                        (>= v0x7f9945a08090_0 v0x7f9945a07010_0)))))
      (a!2 (=> v0x7f9945a07fd0_0
               (or (and E0x7f9945a08150 (not E0x7f9945a08310))
                   (and E0x7f9945a08310 (not E0x7f9945a08150)))))
      (a!3 (=> v0x7f9945a08e90_0
               (or (and v0x7f9945a08950_0
                        E0x7f9945a09010
                        (<= v0x7f9945a08f50_0 v0x7f9945a08d50_0)
                        (>= v0x7f9945a08f50_0 v0x7f9945a08d50_0))
                   (and v0x7f9945a07fd0_0
                        E0x7f9945a091d0
                        v0x7f9945a08810_0
                        (<= v0x7f9945a08f50_0 v0x7f9945a06f10_0)
                        (>= v0x7f9945a08f50_0 v0x7f9945a06f10_0)))))
      (a!4 (=> v0x7f9945a08e90_0
               (or (and E0x7f9945a09010 (not E0x7f9945a091d0))
                   (and E0x7f9945a091d0 (not E0x7f9945a09010)))))
      (a!5 (or (and v0x7f9945a0a410_0
                    E0x7f9945a0ad90
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a08090_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a08090_0))
                    (<= v0x7f9945a0acd0_0 v0x7f9945a0a790_0)
                    (>= v0x7f9945a0acd0_0 v0x7f9945a0a790_0))
               (and v0x7f9945a09810_0
                    E0x7f9945a0b050
                    (not v0x7f9945a09a90_0)
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a08090_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a08090_0))
                    (and (<= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)
                         (>= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)))
               (and v0x7f9945a0a8d0_0
                    E0x7f9945a0b2d0
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a0a190_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a0a190_0))
                    (and (<= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)
                         (>= v0x7f9945a0acd0_0 v0x7f9945a06d90_0)))
               (and v0x7f9945a09bd0_0
                    E0x7f9945a0b4d0
                    (not v0x7f9945a0a2d0_0)
                    (and (<= v0x7f9945a0ac10_0 v0x7f9945a0a190_0)
                         (>= v0x7f9945a0ac10_0 v0x7f9945a0a190_0))
                    (<= v0x7f9945a0acd0_0 0.0)
                    (>= v0x7f9945a0acd0_0 0.0))))
      (a!6 (=> v0x7f9945a0ab50_0
               (or (and E0x7f9945a0ad90
                        (not E0x7f9945a0b050)
                        (not E0x7f9945a0b2d0)
                        (not E0x7f9945a0b4d0))
                   (and E0x7f9945a0b050
                        (not E0x7f9945a0ad90)
                        (not E0x7f9945a0b2d0)
                        (not E0x7f9945a0b4d0))
                   (and E0x7f9945a0b2d0
                        (not E0x7f9945a0ad90)
                        (not E0x7f9945a0b050)
                        (not E0x7f9945a0b4d0))
                   (and E0x7f9945a0b4d0
                        (not E0x7f9945a0ad90)
                        (not E0x7f9945a0b050)
                        (not E0x7f9945a0b2d0))))))
(let ((a!7 (and (=> v0x7f9945a07890_0
                    (and v0x7f9945a075d0_0
                         E0x7f9945a07950
                         (not v0x7f9945a07750_0)))
                (=> v0x7f9945a07890_0 E0x7f9945a07950)
                a!1
                a!2
                (=> v0x7f9945a08950_0
                    (and v0x7f9945a07fd0_0
                         E0x7f9945a08a10
                         (not v0x7f9945a08810_0)))
                (=> v0x7f9945a08950_0 E0x7f9945a08a10)
                a!3
                a!4
                (=> v0x7f9945a09810_0
                    (and v0x7f9945a08e90_0 E0x7f9945a098d0 v0x7f9945a096d0_0))
                (=> v0x7f9945a09810_0 E0x7f9945a098d0)
                (=> v0x7f9945a09bd0_0
                    (and v0x7f9945a08e90_0
                         E0x7f9945a09c90
                         (not v0x7f9945a096d0_0)))
                (=> v0x7f9945a09bd0_0 E0x7f9945a09c90)
                (=> v0x7f9945a0a410_0
                    (and v0x7f9945a09810_0 E0x7f9945a0a4d0 v0x7f9945a09a90_0))
                (=> v0x7f9945a0a410_0 E0x7f9945a0a4d0)
                (=> v0x7f9945a0a8d0_0
                    (and v0x7f9945a09bd0_0 E0x7f9945a0a990 v0x7f9945a0a2d0_0))
                (=> v0x7f9945a0a8d0_0 E0x7f9945a0a990)
                (=> v0x7f9945a0ab50_0 a!5)
                a!6
                v0x7f9945a0ab50_0
                v0x7f9945a0bf90_0
                (= v0x7f9945a07750_0 (= v0x7f9945a07690_0 0.0))
                (= v0x7f9945a07b90_0 (< v0x7f9945a07010_0 2.0))
                (= v0x7f9945a07d50_0 (ite v0x7f9945a07b90_0 1.0 0.0))
                (= v0x7f9945a07e90_0 (+ v0x7f9945a07d50_0 v0x7f9945a07010_0))
                (= v0x7f9945a08810_0 (= v0x7f9945a08750_0 0.0))
                (= v0x7f9945a08c10_0 (= v0x7f9945a06f10_0 0.0))
                (= v0x7f9945a08d50_0 (ite v0x7f9945a08c10_0 1.0 0.0))
                (= v0x7f9945a096d0_0 (= v0x7f9945a06d90_0 0.0))
                (= v0x7f9945a09a90_0 (> v0x7f9945a08090_0 1.0))
                (= v0x7f9945a09e90_0 (> v0x7f9945a08090_0 0.0))
                (= v0x7f9945a09fd0_0 (+ v0x7f9945a08090_0 (- 1.0)))
                (= v0x7f9945a0a190_0
                   (ite v0x7f9945a09e90_0 v0x7f9945a09fd0_0 v0x7f9945a08090_0))
                (= v0x7f9945a0a2d0_0 (= v0x7f9945a08f50_0 0.0))
                (= v0x7f9945a0a690_0 (= v0x7f9945a08f50_0 0.0))
                (= v0x7f9945a0a790_0
                   (ite v0x7f9945a0a690_0 1.0 v0x7f9945a06d90_0))
                (= v0x7f9945a0ba90_0 (= v0x7f9945a0ac10_0 2.0))
                (= v0x7f9945a0bbd0_0 (= v0x7f9945a0acd0_0 0.0))
                (= v0x7f9945a0bd10_0 (or v0x7f9945a0bbd0_0 v0x7f9945a0ba90_0))
                (= v0x7f9945a0be50_0 (xor v0x7f9945a0bd10_0 true))
                (= v0x7f9945a0bf90_0 (and v0x7f9945a096d0_0 v0x7f9945a0be50_0)))))
  (=> F0x7f9945a0cdd0 a!7))))
(assert (=> F0x7f9945a0cdd0 F0x7f9945a0cad0))
(assert (=> F0x7f9945a0ced0 (or F0x7f9945a0cd10 F0x7f9945a0cb90)))
(assert (=> F0x7f9945a0ce90 F0x7f9945a0cdd0))
(assert (=> pre!entry!0 (=> F0x7f9945a0cc50 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f9945a0cad0 (>= v0x7f9945a06f10_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f9945a0cad0
        (or (>= v0x7f9945a07010_0 2.0) (<= v0x7f9945a07010_0 1.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f9945a0cad0 (>= v0x7f9945a07010_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f9945a0cad0
        (or (<= v0x7f9945a07010_0 0.0) (>= v0x7f9945a07010_0 1.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f9945a0cad0 (<= v0x7f9945a07010_0 2.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f9945a0ced0
                (not (or (>= v0x7f9945a05010_0 2.0) (<= v0x7f9945a05010_0 1.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7f9945a0ced0
                (not (or (<= v0x7f9945a05010_0 0.0) (>= v0x7f9945a05010_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f9945a0ced0
           (not (>= v0x7f9945a070d0_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f9945a0ced0
           (not (>= v0x7f9945a05010_0 0.0)))
      a!2
      (and (not post!bb1.i.i!4)
           F0x7f9945a0ced0
           (not (<= v0x7f9945a05010_0 2.0)))
      (and (not post!bb2.i.i31.i.i!0) F0x7f9945a0ce90 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i31.i.i!0)
