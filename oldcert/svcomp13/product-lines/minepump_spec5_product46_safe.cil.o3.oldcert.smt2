(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f2fb43f2e90 () Bool)
(declare-fun F0x7f2fb43f2dd0 () Bool)
(declare-fun v0x7f2fb43f1a90_0 () Bool)
(declare-fun v0x7f2fb43f0690_0 () Bool)
(declare-fun v0x7f2fb43eec10_0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f2fb43edd50_0 () Real)
(declare-fun v0x7f2fb43f1bd0_0 () Bool)
(declare-fun E0x7f2fb43f14d0 () Bool)
(declare-fun v0x7f2fb43ecd90_0 () Real)
(declare-fun v0x7f2fb43f0cd0_0 () Real)
(declare-fun E0x7f2fb43f0d90 () Bool)
(declare-fun v0x7f2fb43f0b50_0 () Bool)
(declare-fun E0x7f2fb43f0990 () Bool)
(declare-fun v0x7f2fb43f08d0_0 () Bool)
(declare-fun v0x7f2fb43efa90_0 () Bool)
(declare-fun v0x7f2fb43ef6d0_0 () Bool)
(declare-fun v0x7f2fb43f1d10_0 () Bool)
(declare-fun E0x7f2fb43ef8d0 () Bool)
(declare-fun E0x7f2fb43ef1d0 () Bool)
(declare-fun v0x7f2fb43eed50_0 () Real)
(declare-fun post!bb2.i.i31.i.i!0 () Bool)
(declare-fun v0x7f2fb43eee90_0 () Bool)
(declare-fun v0x7f2fb43f0790_0 () Real)
(declare-fun v0x7f2fb43efbd0_0 () Bool)
(declare-fun v0x7f2fb43f0c10_0 () Real)
(declare-fun E0x7f2fb43f04d0 () Bool)
(declare-fun v0x7f2fb43ee810_0 () Bool)
(declare-fun F0x7f2fb43f2ad0 () Bool)
(declare-fun v0x7f2fb43ee950_0 () Bool)
(declare-fun v0x7f2fb43edb90_0 () Bool)
(declare-fun E0x7f2fb43eea10 () Bool)
(declare-fun v0x7f2fb43ed010_0 () Real)
(declare-fun v0x7f2fb43ed690_0 () Real)
(declare-fun v0x7f2fb43ee090_0 () Real)
(declare-fun v0x7f2fb43ede90_0 () Real)
(declare-fun v0x7f2fb43effd0_0 () Real)
(declare-fun v0x7f2fb43ef810_0 () Bool)
(declare-fun E0x7f2fb43ee150 () Bool)
(declare-fun v0x7f2fb43f1f90_0 () Bool)
(declare-fun v0x7f2fb43edfd0_0 () Bool)
(declare-fun F0x7f2fb43f2b90 () Bool)
(declare-fun v0x7f2fb43efe90_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f2fb43ed5d0_0 () Bool)
(declare-fun v0x7f2fb43ee750_0 () Real)
(declare-fun v0x7f2fb43ed890_0 () Bool)
(declare-fun F0x7f2fb43f2c50 () Bool)
(declare-fun v0x7f2fb43ecf10_0 () Real)
(declare-fun v0x7f2fb43eb010_0 () Real)
(declare-fun v0x7f2fb43ed0d0_0 () Real)
(declare-fun v0x7f2fb43f0410_0 () Bool)
(declare-fun v0x7f2fb43eef50_0 () Real)
(declare-fun v0x7f2fb43ecfd0_0 () Real)
(declare-fun E0x7f2fb43ee310 () Bool)
(declare-fun E0x7f2fb43efc90 () Bool)
(declare-fun E0x7f2fb43f12d0 () Bool)
(declare-fun v0x7f2fb43ed750_0 () Bool)
(declare-fun E0x7f2fb43ed950 () Bool)
(declare-fun v0x7f2fb43f0190_0 () Real)
(declare-fun E0x7f2fb43f1050 () Bool)
(declare-fun F0x7f2fb43f2ed0 () Bool)
(declare-fun E0x7f2fb43ef010 () Bool)
(declare-fun v0x7f2fb43eb110_0 () Bool)
(declare-fun v0x7f2fb43f1e50_0 () Bool)
(declare-fun F0x7f2fb43f2d10 () Bool)
(declare-fun v0x7f2fb43f02d0_0 () Bool)

(assert (=> F0x7f2fb43f2d10
    (and v0x7f2fb43eb110_0
         (<= v0x7f2fb43ecfd0_0 0.0)
         (>= v0x7f2fb43ecfd0_0 0.0)
         (<= v0x7f2fb43ed0d0_0 0.0)
         (>= v0x7f2fb43ed0d0_0 0.0)
         (<= v0x7f2fb43eb010_0 1.0)
         (>= v0x7f2fb43eb010_0 1.0))))
(assert (=> F0x7f2fb43f2d10 F0x7f2fb43f2c50))
(assert (let ((a!1 (=> v0x7f2fb43edfd0_0
               (or (and v0x7f2fb43ed890_0
                        E0x7f2fb43ee150
                        (<= v0x7f2fb43ee090_0 v0x7f2fb43ede90_0)
                        (>= v0x7f2fb43ee090_0 v0x7f2fb43ede90_0))
                   (and v0x7f2fb43ed5d0_0
                        E0x7f2fb43ee310
                        v0x7f2fb43ed750_0
                        (<= v0x7f2fb43ee090_0 v0x7f2fb43ed010_0)
                        (>= v0x7f2fb43ee090_0 v0x7f2fb43ed010_0)))))
      (a!2 (=> v0x7f2fb43edfd0_0
               (or (and E0x7f2fb43ee150 (not E0x7f2fb43ee310))
                   (and E0x7f2fb43ee310 (not E0x7f2fb43ee150)))))
      (a!3 (=> v0x7f2fb43eee90_0
               (or (and v0x7f2fb43ee950_0
                        E0x7f2fb43ef010
                        (<= v0x7f2fb43eef50_0 v0x7f2fb43eed50_0)
                        (>= v0x7f2fb43eef50_0 v0x7f2fb43eed50_0))
                   (and v0x7f2fb43edfd0_0
                        E0x7f2fb43ef1d0
                        v0x7f2fb43ee810_0
                        (<= v0x7f2fb43eef50_0 v0x7f2fb43ecf10_0)
                        (>= v0x7f2fb43eef50_0 v0x7f2fb43ecf10_0)))))
      (a!4 (=> v0x7f2fb43eee90_0
               (or (and E0x7f2fb43ef010 (not E0x7f2fb43ef1d0))
                   (and E0x7f2fb43ef1d0 (not E0x7f2fb43ef010)))))
      (a!5 (or (and v0x7f2fb43f0410_0
                    E0x7f2fb43f0d90
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0))
                    (<= v0x7f2fb43f0cd0_0 v0x7f2fb43f0790_0)
                    (>= v0x7f2fb43f0cd0_0 v0x7f2fb43f0790_0))
               (and v0x7f2fb43ef810_0
                    E0x7f2fb43f1050
                    (not v0x7f2fb43efa90_0)
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0))
                    (and (<= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)
                         (>= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)))
               (and v0x7f2fb43f08d0_0
                    E0x7f2fb43f12d0
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0))
                    (and (<= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)
                         (>= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)))
               (and v0x7f2fb43efbd0_0
                    E0x7f2fb43f14d0
                    (not v0x7f2fb43f02d0_0)
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0))
                    (<= v0x7f2fb43f0cd0_0 0.0)
                    (>= v0x7f2fb43f0cd0_0 0.0))))
      (a!6 (=> v0x7f2fb43f0b50_0
               (or (and E0x7f2fb43f0d90
                        (not E0x7f2fb43f1050)
                        (not E0x7f2fb43f12d0)
                        (not E0x7f2fb43f14d0))
                   (and E0x7f2fb43f1050
                        (not E0x7f2fb43f0d90)
                        (not E0x7f2fb43f12d0)
                        (not E0x7f2fb43f14d0))
                   (and E0x7f2fb43f12d0
                        (not E0x7f2fb43f0d90)
                        (not E0x7f2fb43f1050)
                        (not E0x7f2fb43f14d0))
                   (and E0x7f2fb43f14d0
                        (not E0x7f2fb43f0d90)
                        (not E0x7f2fb43f1050)
                        (not E0x7f2fb43f12d0))))))
(let ((a!7 (and (=> v0x7f2fb43ed890_0
                    (and v0x7f2fb43ed5d0_0
                         E0x7f2fb43ed950
                         (not v0x7f2fb43ed750_0)))
                (=> v0x7f2fb43ed890_0 E0x7f2fb43ed950)
                a!1
                a!2
                (=> v0x7f2fb43ee950_0
                    (and v0x7f2fb43edfd0_0
                         E0x7f2fb43eea10
                         (not v0x7f2fb43ee810_0)))
                (=> v0x7f2fb43ee950_0 E0x7f2fb43eea10)
                a!3
                a!4
                (=> v0x7f2fb43ef810_0
                    (and v0x7f2fb43eee90_0 E0x7f2fb43ef8d0 v0x7f2fb43ef6d0_0))
                (=> v0x7f2fb43ef810_0 E0x7f2fb43ef8d0)
                (=> v0x7f2fb43efbd0_0
                    (and v0x7f2fb43eee90_0
                         E0x7f2fb43efc90
                         (not v0x7f2fb43ef6d0_0)))
                (=> v0x7f2fb43efbd0_0 E0x7f2fb43efc90)
                (=> v0x7f2fb43f0410_0
                    (and v0x7f2fb43ef810_0 E0x7f2fb43f04d0 v0x7f2fb43efa90_0))
                (=> v0x7f2fb43f0410_0 E0x7f2fb43f04d0)
                (=> v0x7f2fb43f08d0_0
                    (and v0x7f2fb43efbd0_0 E0x7f2fb43f0990 v0x7f2fb43f02d0_0))
                (=> v0x7f2fb43f08d0_0 E0x7f2fb43f0990)
                (=> v0x7f2fb43f0b50_0 a!5)
                a!6
                v0x7f2fb43f0b50_0
                (not v0x7f2fb43f1f90_0)
                (<= v0x7f2fb43ecfd0_0 v0x7f2fb43f0cd0_0)
                (>= v0x7f2fb43ecfd0_0 v0x7f2fb43f0cd0_0)
                (<= v0x7f2fb43ed0d0_0 v0x7f2fb43eef50_0)
                (>= v0x7f2fb43ed0d0_0 v0x7f2fb43eef50_0)
                (<= v0x7f2fb43eb010_0 v0x7f2fb43f0c10_0)
                (>= v0x7f2fb43eb010_0 v0x7f2fb43f0c10_0)
                (= v0x7f2fb43ed750_0 (= v0x7f2fb43ed690_0 0.0))
                (= v0x7f2fb43edb90_0 (< v0x7f2fb43ed010_0 2.0))
                (= v0x7f2fb43edd50_0 (ite v0x7f2fb43edb90_0 1.0 0.0))
                (= v0x7f2fb43ede90_0 (+ v0x7f2fb43edd50_0 v0x7f2fb43ed010_0))
                (= v0x7f2fb43ee810_0 (= v0x7f2fb43ee750_0 0.0))
                (= v0x7f2fb43eec10_0 (= v0x7f2fb43ecf10_0 0.0))
                (= v0x7f2fb43eed50_0 (ite v0x7f2fb43eec10_0 1.0 0.0))
                (= v0x7f2fb43ef6d0_0 (= v0x7f2fb43ecd90_0 0.0))
                (= v0x7f2fb43efa90_0 (> v0x7f2fb43ee090_0 1.0))
                (= v0x7f2fb43efe90_0 (> v0x7f2fb43ee090_0 0.0))
                (= v0x7f2fb43effd0_0 (+ v0x7f2fb43ee090_0 (- 1.0)))
                (= v0x7f2fb43f0190_0
                   (ite v0x7f2fb43efe90_0 v0x7f2fb43effd0_0 v0x7f2fb43ee090_0))
                (= v0x7f2fb43f02d0_0 (= v0x7f2fb43eef50_0 0.0))
                (= v0x7f2fb43f0690_0 (= v0x7f2fb43eef50_0 0.0))
                (= v0x7f2fb43f0790_0
                   (ite v0x7f2fb43f0690_0 1.0 v0x7f2fb43ecd90_0))
                (= v0x7f2fb43f1a90_0 (= v0x7f2fb43f0c10_0 2.0))
                (= v0x7f2fb43f1bd0_0 (= v0x7f2fb43f0cd0_0 0.0))
                (= v0x7f2fb43f1d10_0 (or v0x7f2fb43f1bd0_0 v0x7f2fb43f1a90_0))
                (= v0x7f2fb43f1e50_0 (xor v0x7f2fb43f1d10_0 true))
                (= v0x7f2fb43f1f90_0 (and v0x7f2fb43ef6d0_0 v0x7f2fb43f1e50_0)))))
  (=> F0x7f2fb43f2b90 a!7))))
(assert (=> F0x7f2fb43f2b90 F0x7f2fb43f2ad0))
(assert (let ((a!1 (=> v0x7f2fb43edfd0_0
               (or (and v0x7f2fb43ed890_0
                        E0x7f2fb43ee150
                        (<= v0x7f2fb43ee090_0 v0x7f2fb43ede90_0)
                        (>= v0x7f2fb43ee090_0 v0x7f2fb43ede90_0))
                   (and v0x7f2fb43ed5d0_0
                        E0x7f2fb43ee310
                        v0x7f2fb43ed750_0
                        (<= v0x7f2fb43ee090_0 v0x7f2fb43ed010_0)
                        (>= v0x7f2fb43ee090_0 v0x7f2fb43ed010_0)))))
      (a!2 (=> v0x7f2fb43edfd0_0
               (or (and E0x7f2fb43ee150 (not E0x7f2fb43ee310))
                   (and E0x7f2fb43ee310 (not E0x7f2fb43ee150)))))
      (a!3 (=> v0x7f2fb43eee90_0
               (or (and v0x7f2fb43ee950_0
                        E0x7f2fb43ef010
                        (<= v0x7f2fb43eef50_0 v0x7f2fb43eed50_0)
                        (>= v0x7f2fb43eef50_0 v0x7f2fb43eed50_0))
                   (and v0x7f2fb43edfd0_0
                        E0x7f2fb43ef1d0
                        v0x7f2fb43ee810_0
                        (<= v0x7f2fb43eef50_0 v0x7f2fb43ecf10_0)
                        (>= v0x7f2fb43eef50_0 v0x7f2fb43ecf10_0)))))
      (a!4 (=> v0x7f2fb43eee90_0
               (or (and E0x7f2fb43ef010 (not E0x7f2fb43ef1d0))
                   (and E0x7f2fb43ef1d0 (not E0x7f2fb43ef010)))))
      (a!5 (or (and v0x7f2fb43f0410_0
                    E0x7f2fb43f0d90
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0))
                    (<= v0x7f2fb43f0cd0_0 v0x7f2fb43f0790_0)
                    (>= v0x7f2fb43f0cd0_0 v0x7f2fb43f0790_0))
               (and v0x7f2fb43ef810_0
                    E0x7f2fb43f1050
                    (not v0x7f2fb43efa90_0)
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43ee090_0))
                    (and (<= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)
                         (>= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)))
               (and v0x7f2fb43f08d0_0
                    E0x7f2fb43f12d0
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0))
                    (and (<= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)
                         (>= v0x7f2fb43f0cd0_0 v0x7f2fb43ecd90_0)))
               (and v0x7f2fb43efbd0_0
                    E0x7f2fb43f14d0
                    (not v0x7f2fb43f02d0_0)
                    (and (<= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0)
                         (>= v0x7f2fb43f0c10_0 v0x7f2fb43f0190_0))
                    (<= v0x7f2fb43f0cd0_0 0.0)
                    (>= v0x7f2fb43f0cd0_0 0.0))))
      (a!6 (=> v0x7f2fb43f0b50_0
               (or (and E0x7f2fb43f0d90
                        (not E0x7f2fb43f1050)
                        (not E0x7f2fb43f12d0)
                        (not E0x7f2fb43f14d0))
                   (and E0x7f2fb43f1050
                        (not E0x7f2fb43f0d90)
                        (not E0x7f2fb43f12d0)
                        (not E0x7f2fb43f14d0))
                   (and E0x7f2fb43f12d0
                        (not E0x7f2fb43f0d90)
                        (not E0x7f2fb43f1050)
                        (not E0x7f2fb43f14d0))
                   (and E0x7f2fb43f14d0
                        (not E0x7f2fb43f0d90)
                        (not E0x7f2fb43f1050)
                        (not E0x7f2fb43f12d0))))))
(let ((a!7 (and (=> v0x7f2fb43ed890_0
                    (and v0x7f2fb43ed5d0_0
                         E0x7f2fb43ed950
                         (not v0x7f2fb43ed750_0)))
                (=> v0x7f2fb43ed890_0 E0x7f2fb43ed950)
                a!1
                a!2
                (=> v0x7f2fb43ee950_0
                    (and v0x7f2fb43edfd0_0
                         E0x7f2fb43eea10
                         (not v0x7f2fb43ee810_0)))
                (=> v0x7f2fb43ee950_0 E0x7f2fb43eea10)
                a!3
                a!4
                (=> v0x7f2fb43ef810_0
                    (and v0x7f2fb43eee90_0 E0x7f2fb43ef8d0 v0x7f2fb43ef6d0_0))
                (=> v0x7f2fb43ef810_0 E0x7f2fb43ef8d0)
                (=> v0x7f2fb43efbd0_0
                    (and v0x7f2fb43eee90_0
                         E0x7f2fb43efc90
                         (not v0x7f2fb43ef6d0_0)))
                (=> v0x7f2fb43efbd0_0 E0x7f2fb43efc90)
                (=> v0x7f2fb43f0410_0
                    (and v0x7f2fb43ef810_0 E0x7f2fb43f04d0 v0x7f2fb43efa90_0))
                (=> v0x7f2fb43f0410_0 E0x7f2fb43f04d0)
                (=> v0x7f2fb43f08d0_0
                    (and v0x7f2fb43efbd0_0 E0x7f2fb43f0990 v0x7f2fb43f02d0_0))
                (=> v0x7f2fb43f08d0_0 E0x7f2fb43f0990)
                (=> v0x7f2fb43f0b50_0 a!5)
                a!6
                v0x7f2fb43f0b50_0
                v0x7f2fb43f1f90_0
                (= v0x7f2fb43ed750_0 (= v0x7f2fb43ed690_0 0.0))
                (= v0x7f2fb43edb90_0 (< v0x7f2fb43ed010_0 2.0))
                (= v0x7f2fb43edd50_0 (ite v0x7f2fb43edb90_0 1.0 0.0))
                (= v0x7f2fb43ede90_0 (+ v0x7f2fb43edd50_0 v0x7f2fb43ed010_0))
                (= v0x7f2fb43ee810_0 (= v0x7f2fb43ee750_0 0.0))
                (= v0x7f2fb43eec10_0 (= v0x7f2fb43ecf10_0 0.0))
                (= v0x7f2fb43eed50_0 (ite v0x7f2fb43eec10_0 1.0 0.0))
                (= v0x7f2fb43ef6d0_0 (= v0x7f2fb43ecd90_0 0.0))
                (= v0x7f2fb43efa90_0 (> v0x7f2fb43ee090_0 1.0))
                (= v0x7f2fb43efe90_0 (> v0x7f2fb43ee090_0 0.0))
                (= v0x7f2fb43effd0_0 (+ v0x7f2fb43ee090_0 (- 1.0)))
                (= v0x7f2fb43f0190_0
                   (ite v0x7f2fb43efe90_0 v0x7f2fb43effd0_0 v0x7f2fb43ee090_0))
                (= v0x7f2fb43f02d0_0 (= v0x7f2fb43eef50_0 0.0))
                (= v0x7f2fb43f0690_0 (= v0x7f2fb43eef50_0 0.0))
                (= v0x7f2fb43f0790_0
                   (ite v0x7f2fb43f0690_0 1.0 v0x7f2fb43ecd90_0))
                (= v0x7f2fb43f1a90_0 (= v0x7f2fb43f0c10_0 2.0))
                (= v0x7f2fb43f1bd0_0 (= v0x7f2fb43f0cd0_0 0.0))
                (= v0x7f2fb43f1d10_0 (or v0x7f2fb43f1bd0_0 v0x7f2fb43f1a90_0))
                (= v0x7f2fb43f1e50_0 (xor v0x7f2fb43f1d10_0 true))
                (= v0x7f2fb43f1f90_0 (and v0x7f2fb43ef6d0_0 v0x7f2fb43f1e50_0)))))
  (=> F0x7f2fb43f2dd0 a!7))))
(assert (=> F0x7f2fb43f2dd0 F0x7f2fb43f2ad0))
(assert (=> F0x7f2fb43f2ed0 (or F0x7f2fb43f2d10 F0x7f2fb43f2b90)))
(assert (=> F0x7f2fb43f2e90 F0x7f2fb43f2dd0))
(assert (=> pre!entry!0 (=> F0x7f2fb43f2c50 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f2fb43f2ad0 (>= v0x7f2fb43ecf10_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f2fb43f2ad0
        (or (>= v0x7f2fb43ed010_0 2.0) (<= v0x7f2fb43ed010_0 1.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f2fb43f2ad0 (>= v0x7f2fb43ed010_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f2fb43f2ad0
        (or (<= v0x7f2fb43ed010_0 0.0) (>= v0x7f2fb43ed010_0 1.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f2fb43f2ad0 (<= v0x7f2fb43ed010_0 2.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f2fb43f2ed0
                (not (or (>= v0x7f2fb43eb010_0 2.0) (<= v0x7f2fb43eb010_0 1.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7f2fb43f2ed0
                (not (or (<= v0x7f2fb43eb010_0 0.0) (>= v0x7f2fb43eb010_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f2fb43f2ed0
           (not (>= v0x7f2fb43ed0d0_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f2fb43f2ed0
           (not (>= v0x7f2fb43eb010_0 0.0)))
      a!2
      (and (not post!bb1.i.i!4)
           F0x7f2fb43f2ed0
           (not (<= v0x7f2fb43eb010_0 2.0)))
      (and (not post!bb2.i.i31.i.i!0) F0x7f2fb43f2e90 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i31.i.i!0)
