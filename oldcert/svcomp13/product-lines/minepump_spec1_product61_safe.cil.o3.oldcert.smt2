(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f16e4bc84d0 () Bool)
(declare-fun F0x7f16e4bc8310 () Bool)
(declare-fun v0x7f16e4bc5a90_0 () Bool)
(declare-fun v0x7f16e4bc5950_0 () Bool)
(declare-fun v0x7f16e4bc5650_0 () Real)
(declare-fun F0x7f16e4bc83d0 () Bool)
(declare-fun v0x7f16e4bc33d0_0 () Real)
(declare-fun v0x7f16e4bc7390_0 () Bool)
(declare-fun v0x7f16e4bc3210_0 () Bool)
(declare-fun E0x7f16e4bc6dd0 () Bool)
(declare-fun v0x7f16e4bc5810_0 () Real)
(declare-fun E0x7f16e4bc6bd0 () Bool)
(declare-fun v0x7f16e4bc3dd0_0 () Real)
(declare-fun v0x7f16e4bc2410_0 () Real)
(declare-fun E0x7f16e4bc6950 () Bool)
(declare-fun v0x7f16e4bc65d0_0 () Real)
(declare-fun v0x7f16e4bc6510_0 () Real)
(declare-fun v0x7f16e4bc61d0_0 () Bool)
(declare-fun v0x7f16e4bc5bd0_0 () Bool)
(declare-fun v0x7f16e4bc5110_0 () Bool)
(declare-fun v0x7f16e4bc5d10_0 () Bool)
(declare-fun E0x7f16e4bc4f50 () Bool)
(declare-fun F0x7f16e4bc8510 () Bool)
(declare-fun E0x7f16e4bc4850 () Bool)
(declare-fun v0x7f16e4bc43d0_0 () Real)
(declare-fun v0x7f16e4bc6090_0 () Real)
(declare-fun v0x7f16e4bc5250_0 () Bool)
(declare-fun v0x7f16e4bc3e90_0 () Bool)
(declare-fun E0x7f16e4bc4090 () Bool)
(declare-fun v0x7f16e4bc2690_0 () Real)
(declare-fun v0x7f16e4bc4290_0 () Bool)
(declare-fun v0x7f16e4bc5510_0 () Bool)
(declare-fun v0x7f16e4bc3710_0 () Real)
(declare-fun E0x7f16e4bc6690 () Bool)
(declare-fun E0x7f16e4bc37d0 () Bool)
(declare-fun v0x7f16e4bc3510_0 () Real)
(declare-fun v0x7f16e4bc45d0_0 () Real)
(declare-fun v0x7f16e4bc3650_0 () Bool)
(declare-fun v0x7f16e4bc4e90_0 () Bool)
(declare-fun v0x7f16e4bc2590_0 () Real)
(declare-fun v0x7f16e4bc2dd0_0 () Bool)
(declare-fun E0x7f16e4bc4690 () Bool)
(declare-fun v0x7f16e4bc6450_0 () Bool)
(declare-fun E0x7f16e4bc2fd0 () Bool)
(declare-fun post!bb1.i.i43.i.i!0 () Bool)
(declare-fun E0x7f16e4bc3990 () Bool)
(declare-fun v0x7f16e4bc3fd0_0 () Bool)
(declare-fun v0x7f16e4bc2c50_0 () Bool)
(declare-fun v0x7f16e4bc75d0_0 () Bool)
(declare-fun v0x7f16e4bc2d10_0 () Real)
(declare-fun v0x7f16e4bc2f10_0 () Bool)
(declare-fun F0x7f16e4bc8190 () Bool)
(declare-fun v0x7f16e4bc7490_0 () Bool)
(declare-fun v0x7f16e4bc4d50_0 () Bool)
(declare-fun v0x7f16e4bbf010_0 () Real)
(declare-fun F0x7f16e4bc8250 () Bool)
(declare-fun E0x7f16e4bc6290 () Bool)
(declare-fun v0x7f16e4bc5f90_0 () Bool)
(declare-fun v0x7f16e4bc4510_0 () Bool)
(declare-fun v0x7f16e4bc2750_0 () Real)
(declare-fun E0x7f16e4bc5310 () Bool)
(declare-fun v0x7f16e4bc2650_0 () Real)
(declare-fun v0x7f16e4bbf110_0 () Bool)
(declare-fun E0x7f16e4bc5dd0 () Bool)
(declare-fun F0x7f16e4bc80d0 () Bool)

(assert (=> F0x7f16e4bc80d0
    (and v0x7f16e4bbf110_0
         (<= v0x7f16e4bc2650_0 0.0)
         (>= v0x7f16e4bc2650_0 0.0)
         (<= v0x7f16e4bc2750_0 0.0)
         (>= v0x7f16e4bc2750_0 0.0)
         (<= v0x7f16e4bbf010_0 1.0)
         (>= v0x7f16e4bbf010_0 1.0))))
(assert (=> F0x7f16e4bc80d0 F0x7f16e4bc8190))
(assert (let ((a!1 (=> v0x7f16e4bc3650_0
               (or (and v0x7f16e4bc2f10_0
                        E0x7f16e4bc37d0
                        (<= v0x7f16e4bc3710_0 v0x7f16e4bc3510_0)
                        (>= v0x7f16e4bc3710_0 v0x7f16e4bc3510_0))
                   (and v0x7f16e4bc2c50_0
                        E0x7f16e4bc3990
                        v0x7f16e4bc2dd0_0
                        (<= v0x7f16e4bc3710_0 v0x7f16e4bc2690_0)
                        (>= v0x7f16e4bc3710_0 v0x7f16e4bc2690_0)))))
      (a!2 (=> v0x7f16e4bc3650_0
               (or (and E0x7f16e4bc37d0 (not E0x7f16e4bc3990))
                   (and E0x7f16e4bc3990 (not E0x7f16e4bc37d0)))))
      (a!3 (=> v0x7f16e4bc4510_0
               (or (and v0x7f16e4bc3fd0_0
                        E0x7f16e4bc4690
                        (<= v0x7f16e4bc45d0_0 v0x7f16e4bc43d0_0)
                        (>= v0x7f16e4bc45d0_0 v0x7f16e4bc43d0_0))
                   (and v0x7f16e4bc3650_0
                        E0x7f16e4bc4850
                        v0x7f16e4bc3e90_0
                        (<= v0x7f16e4bc45d0_0 v0x7f16e4bc2590_0)
                        (>= v0x7f16e4bc45d0_0 v0x7f16e4bc2590_0)))))
      (a!4 (=> v0x7f16e4bc4510_0
               (or (and E0x7f16e4bc4690 (not E0x7f16e4bc4850))
                   (and E0x7f16e4bc4850 (not E0x7f16e4bc4690)))))
      (a!5 (or (and v0x7f16e4bc5d10_0
                    E0x7f16e4bc6690
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0))
                    (<= v0x7f16e4bc65d0_0 v0x7f16e4bc6090_0)
                    (>= v0x7f16e4bc65d0_0 v0x7f16e4bc6090_0))
               (and v0x7f16e4bc4e90_0
                    E0x7f16e4bc6950
                    (not v0x7f16e4bc5110_0)
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0))
                    (and (<= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)
                         (>= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)))
               (and v0x7f16e4bc61d0_0
                    E0x7f16e4bc6bd0
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0))
                    (and (<= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)
                         (>= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)))
               (and v0x7f16e4bc5250_0
                    E0x7f16e4bc6dd0
                    (not v0x7f16e4bc5bd0_0)
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0))
                    (<= v0x7f16e4bc65d0_0 0.0)
                    (>= v0x7f16e4bc65d0_0 0.0))))
      (a!6 (=> v0x7f16e4bc6450_0
               (or (and E0x7f16e4bc6690
                        (not E0x7f16e4bc6950)
                        (not E0x7f16e4bc6bd0)
                        (not E0x7f16e4bc6dd0))
                   (and E0x7f16e4bc6950
                        (not E0x7f16e4bc6690)
                        (not E0x7f16e4bc6bd0)
                        (not E0x7f16e4bc6dd0))
                   (and E0x7f16e4bc6bd0
                        (not E0x7f16e4bc6690)
                        (not E0x7f16e4bc6950)
                        (not E0x7f16e4bc6dd0))
                   (and E0x7f16e4bc6dd0
                        (not E0x7f16e4bc6690)
                        (not E0x7f16e4bc6950)
                        (not E0x7f16e4bc6bd0))))))
(let ((a!7 (and (=> v0x7f16e4bc2f10_0
                    (and v0x7f16e4bc2c50_0
                         E0x7f16e4bc2fd0
                         (not v0x7f16e4bc2dd0_0)))
                (=> v0x7f16e4bc2f10_0 E0x7f16e4bc2fd0)
                a!1
                a!2
                (=> v0x7f16e4bc3fd0_0
                    (and v0x7f16e4bc3650_0
                         E0x7f16e4bc4090
                         (not v0x7f16e4bc3e90_0)))
                (=> v0x7f16e4bc3fd0_0 E0x7f16e4bc4090)
                a!3
                a!4
                (=> v0x7f16e4bc4e90_0
                    (and v0x7f16e4bc4510_0 E0x7f16e4bc4f50 v0x7f16e4bc4d50_0))
                (=> v0x7f16e4bc4e90_0 E0x7f16e4bc4f50)
                (=> v0x7f16e4bc5250_0
                    (and v0x7f16e4bc4510_0
                         E0x7f16e4bc5310
                         (not v0x7f16e4bc4d50_0)))
                (=> v0x7f16e4bc5250_0 E0x7f16e4bc5310)
                (=> v0x7f16e4bc5d10_0
                    (and v0x7f16e4bc4e90_0 E0x7f16e4bc5dd0 v0x7f16e4bc5110_0))
                (=> v0x7f16e4bc5d10_0 E0x7f16e4bc5dd0)
                (=> v0x7f16e4bc61d0_0
                    (and v0x7f16e4bc5250_0 E0x7f16e4bc6290 v0x7f16e4bc5bd0_0))
                (=> v0x7f16e4bc61d0_0 E0x7f16e4bc6290)
                (=> v0x7f16e4bc6450_0 a!5)
                a!6
                v0x7f16e4bc6450_0
                v0x7f16e4bc75d0_0
                (<= v0x7f16e4bc2650_0 v0x7f16e4bc65d0_0)
                (>= v0x7f16e4bc2650_0 v0x7f16e4bc65d0_0)
                (<= v0x7f16e4bc2750_0 v0x7f16e4bc45d0_0)
                (>= v0x7f16e4bc2750_0 v0x7f16e4bc45d0_0)
                (<= v0x7f16e4bbf010_0 v0x7f16e4bc6510_0)
                (>= v0x7f16e4bbf010_0 v0x7f16e4bc6510_0)
                (= v0x7f16e4bc2dd0_0 (= v0x7f16e4bc2d10_0 0.0))
                (= v0x7f16e4bc3210_0 (< v0x7f16e4bc2690_0 2.0))
                (= v0x7f16e4bc33d0_0 (ite v0x7f16e4bc3210_0 1.0 0.0))
                (= v0x7f16e4bc3510_0 (+ v0x7f16e4bc33d0_0 v0x7f16e4bc2690_0))
                (= v0x7f16e4bc3e90_0 (= v0x7f16e4bc3dd0_0 0.0))
                (= v0x7f16e4bc4290_0 (= v0x7f16e4bc2590_0 0.0))
                (= v0x7f16e4bc43d0_0 (ite v0x7f16e4bc4290_0 1.0 0.0))
                (= v0x7f16e4bc4d50_0 (= v0x7f16e4bc2410_0 0.0))
                (= v0x7f16e4bc5110_0 (> v0x7f16e4bc3710_0 1.0))
                (= v0x7f16e4bc5510_0 (> v0x7f16e4bc3710_0 0.0))
                (= v0x7f16e4bc5650_0 (+ v0x7f16e4bc3710_0 (- 1.0)))
                (= v0x7f16e4bc5810_0
                   (ite v0x7f16e4bc5510_0 v0x7f16e4bc5650_0 v0x7f16e4bc3710_0))
                (= v0x7f16e4bc5950_0 (= v0x7f16e4bc45d0_0 0.0))
                (= v0x7f16e4bc5a90_0 (= v0x7f16e4bc5810_0 0.0))
                (= v0x7f16e4bc5bd0_0 (and v0x7f16e4bc5950_0 v0x7f16e4bc5a90_0))
                (= v0x7f16e4bc5f90_0 (= v0x7f16e4bc45d0_0 0.0))
                (= v0x7f16e4bc6090_0
                   (ite v0x7f16e4bc5f90_0 1.0 v0x7f16e4bc2410_0))
                (= v0x7f16e4bc7390_0 (= v0x7f16e4bc45d0_0 0.0))
                (= v0x7f16e4bc7490_0 (= v0x7f16e4bc65d0_0 0.0))
                (= v0x7f16e4bc75d0_0 (or v0x7f16e4bc7490_0 v0x7f16e4bc7390_0)))))
  (=> F0x7f16e4bc8250 a!7))))
(assert (=> F0x7f16e4bc8250 F0x7f16e4bc8310))
(assert (let ((a!1 (=> v0x7f16e4bc3650_0
               (or (and v0x7f16e4bc2f10_0
                        E0x7f16e4bc37d0
                        (<= v0x7f16e4bc3710_0 v0x7f16e4bc3510_0)
                        (>= v0x7f16e4bc3710_0 v0x7f16e4bc3510_0))
                   (and v0x7f16e4bc2c50_0
                        E0x7f16e4bc3990
                        v0x7f16e4bc2dd0_0
                        (<= v0x7f16e4bc3710_0 v0x7f16e4bc2690_0)
                        (>= v0x7f16e4bc3710_0 v0x7f16e4bc2690_0)))))
      (a!2 (=> v0x7f16e4bc3650_0
               (or (and E0x7f16e4bc37d0 (not E0x7f16e4bc3990))
                   (and E0x7f16e4bc3990 (not E0x7f16e4bc37d0)))))
      (a!3 (=> v0x7f16e4bc4510_0
               (or (and v0x7f16e4bc3fd0_0
                        E0x7f16e4bc4690
                        (<= v0x7f16e4bc45d0_0 v0x7f16e4bc43d0_0)
                        (>= v0x7f16e4bc45d0_0 v0x7f16e4bc43d0_0))
                   (and v0x7f16e4bc3650_0
                        E0x7f16e4bc4850
                        v0x7f16e4bc3e90_0
                        (<= v0x7f16e4bc45d0_0 v0x7f16e4bc2590_0)
                        (>= v0x7f16e4bc45d0_0 v0x7f16e4bc2590_0)))))
      (a!4 (=> v0x7f16e4bc4510_0
               (or (and E0x7f16e4bc4690 (not E0x7f16e4bc4850))
                   (and E0x7f16e4bc4850 (not E0x7f16e4bc4690)))))
      (a!5 (or (and v0x7f16e4bc5d10_0
                    E0x7f16e4bc6690
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0))
                    (<= v0x7f16e4bc65d0_0 v0x7f16e4bc6090_0)
                    (>= v0x7f16e4bc65d0_0 v0x7f16e4bc6090_0))
               (and v0x7f16e4bc4e90_0
                    E0x7f16e4bc6950
                    (not v0x7f16e4bc5110_0)
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc3710_0))
                    (and (<= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)
                         (>= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)))
               (and v0x7f16e4bc61d0_0
                    E0x7f16e4bc6bd0
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0))
                    (and (<= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)
                         (>= v0x7f16e4bc65d0_0 v0x7f16e4bc2410_0)))
               (and v0x7f16e4bc5250_0
                    E0x7f16e4bc6dd0
                    (not v0x7f16e4bc5bd0_0)
                    (and (<= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0)
                         (>= v0x7f16e4bc6510_0 v0x7f16e4bc5810_0))
                    (<= v0x7f16e4bc65d0_0 0.0)
                    (>= v0x7f16e4bc65d0_0 0.0))))
      (a!6 (=> v0x7f16e4bc6450_0
               (or (and E0x7f16e4bc6690
                        (not E0x7f16e4bc6950)
                        (not E0x7f16e4bc6bd0)
                        (not E0x7f16e4bc6dd0))
                   (and E0x7f16e4bc6950
                        (not E0x7f16e4bc6690)
                        (not E0x7f16e4bc6bd0)
                        (not E0x7f16e4bc6dd0))
                   (and E0x7f16e4bc6bd0
                        (not E0x7f16e4bc6690)
                        (not E0x7f16e4bc6950)
                        (not E0x7f16e4bc6dd0))
                   (and E0x7f16e4bc6dd0
                        (not E0x7f16e4bc6690)
                        (not E0x7f16e4bc6950)
                        (not E0x7f16e4bc6bd0))))))
(let ((a!7 (and (=> v0x7f16e4bc2f10_0
                    (and v0x7f16e4bc2c50_0
                         E0x7f16e4bc2fd0
                         (not v0x7f16e4bc2dd0_0)))
                (=> v0x7f16e4bc2f10_0 E0x7f16e4bc2fd0)
                a!1
                a!2
                (=> v0x7f16e4bc3fd0_0
                    (and v0x7f16e4bc3650_0
                         E0x7f16e4bc4090
                         (not v0x7f16e4bc3e90_0)))
                (=> v0x7f16e4bc3fd0_0 E0x7f16e4bc4090)
                a!3
                a!4
                (=> v0x7f16e4bc4e90_0
                    (and v0x7f16e4bc4510_0 E0x7f16e4bc4f50 v0x7f16e4bc4d50_0))
                (=> v0x7f16e4bc4e90_0 E0x7f16e4bc4f50)
                (=> v0x7f16e4bc5250_0
                    (and v0x7f16e4bc4510_0
                         E0x7f16e4bc5310
                         (not v0x7f16e4bc4d50_0)))
                (=> v0x7f16e4bc5250_0 E0x7f16e4bc5310)
                (=> v0x7f16e4bc5d10_0
                    (and v0x7f16e4bc4e90_0 E0x7f16e4bc5dd0 v0x7f16e4bc5110_0))
                (=> v0x7f16e4bc5d10_0 E0x7f16e4bc5dd0)
                (=> v0x7f16e4bc61d0_0
                    (and v0x7f16e4bc5250_0 E0x7f16e4bc6290 v0x7f16e4bc5bd0_0))
                (=> v0x7f16e4bc61d0_0 E0x7f16e4bc6290)
                (=> v0x7f16e4bc6450_0 a!5)
                a!6
                v0x7f16e4bc6450_0
                (not v0x7f16e4bc75d0_0)
                (= v0x7f16e4bc2dd0_0 (= v0x7f16e4bc2d10_0 0.0))
                (= v0x7f16e4bc3210_0 (< v0x7f16e4bc2690_0 2.0))
                (= v0x7f16e4bc33d0_0 (ite v0x7f16e4bc3210_0 1.0 0.0))
                (= v0x7f16e4bc3510_0 (+ v0x7f16e4bc33d0_0 v0x7f16e4bc2690_0))
                (= v0x7f16e4bc3e90_0 (= v0x7f16e4bc3dd0_0 0.0))
                (= v0x7f16e4bc4290_0 (= v0x7f16e4bc2590_0 0.0))
                (= v0x7f16e4bc43d0_0 (ite v0x7f16e4bc4290_0 1.0 0.0))
                (= v0x7f16e4bc4d50_0 (= v0x7f16e4bc2410_0 0.0))
                (= v0x7f16e4bc5110_0 (> v0x7f16e4bc3710_0 1.0))
                (= v0x7f16e4bc5510_0 (> v0x7f16e4bc3710_0 0.0))
                (= v0x7f16e4bc5650_0 (+ v0x7f16e4bc3710_0 (- 1.0)))
                (= v0x7f16e4bc5810_0
                   (ite v0x7f16e4bc5510_0 v0x7f16e4bc5650_0 v0x7f16e4bc3710_0))
                (= v0x7f16e4bc5950_0 (= v0x7f16e4bc45d0_0 0.0))
                (= v0x7f16e4bc5a90_0 (= v0x7f16e4bc5810_0 0.0))
                (= v0x7f16e4bc5bd0_0 (and v0x7f16e4bc5950_0 v0x7f16e4bc5a90_0))
                (= v0x7f16e4bc5f90_0 (= v0x7f16e4bc45d0_0 0.0))
                (= v0x7f16e4bc6090_0
                   (ite v0x7f16e4bc5f90_0 1.0 v0x7f16e4bc2410_0))
                (= v0x7f16e4bc7390_0 (= v0x7f16e4bc45d0_0 0.0))
                (= v0x7f16e4bc7490_0 (= v0x7f16e4bc65d0_0 0.0))
                (= v0x7f16e4bc75d0_0 (or v0x7f16e4bc7490_0 v0x7f16e4bc7390_0)))))
  (=> F0x7f16e4bc83d0 a!7))))
(assert (=> F0x7f16e4bc83d0 F0x7f16e4bc8310))
(assert (=> F0x7f16e4bc8510 (or F0x7f16e4bc80d0 F0x7f16e4bc8250)))
(assert (=> F0x7f16e4bc84d0 F0x7f16e4bc83d0))
(assert (=> pre!entry!0 (=> F0x7f16e4bc8190 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f16e4bc8310 true)))
(assert (or (and (not post!bb1.i.i!0) F0x7f16e4bc8510 false)
    (and (not post!bb1.i.i43.i.i!0) F0x7f16e4bc84d0 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i43.i.i!0)
