(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun F0x7f6db4534c10 () Bool)
(declare-fun F0x7f6db4534b50 () Bool)
(declare-fun v0x7f6db4533990_0 () Bool)
(declare-fun v0x7f6db4533850_0 () Bool)
(declare-fun v0x7f6db4533710_0 () Bool)
(declare-fun v0x7f6db4532690_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f6db4532550_0 () Bool)
(declare-fun v0x7f6db4532410_0 () Bool)
(declare-fun v0x7f6db4531050_0 () Bool)
(declare-fun v0x7f6db452fdd0_0 () Real)
(declare-fun F0x7f6db4534a90 () Bool)
(declare-fun v0x7f6db4533c10_0 () Bool)
(declare-fun E0x7f6db4533290 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f6db452e350_0 () Bool)
(declare-fun v0x7f6db4532e10_0 () Real)
(declare-fun v0x7f6db4532a50_0 () Bool)
(declare-fun v0x7f6db4531350_0 () Real)
(declare-fun v0x7f6db45316d0_0 () Real)
(declare-fun v0x7f6db4531610_0 () Real)
(declare-fun v0x7f6db452d490_0 () Real)
(declare-fun v0x7f6db4531490_0 () Bool)
(declare-fun v0x7f6db4530c50_0 () Bool)
(declare-fun v0x7f6db452ef10_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun E0x7f6db45307d0 () Bool)
(declare-fun v0x7f6db452de50_0 () Real)
(declare-fun v0x7f6db4530510_0 () Real)
(declare-fun v0x7f6db452fe90_0 () Bool)
(declare-fun v0x7f6db452d310_0 () Real)
(declare-fun E0x7f6db4531dd0 () Bool)
(declare-fun v0x7f6db452d590_0 () Real)
(declare-fun v0x7f6db4530450_0 () Bool)
(declare-fun E0x7f6db4532b10 () Bool)
(declare-fun v0x7f6db452f510_0 () Real)
(declare-fun v0x7f6db452f650_0 () Bool)
(declare-fun F0x7f6db4534c50 () Bool)
(declare-fun v0x7f6db452efd0_0 () Bool)
(declare-fun E0x7f6db452f1d0 () Bool)
(declare-fun E0x7f6db452f7d0 () Bool)
(declare-fun v0x7f6db452f110_0 () Bool)
(declare-fun v0x7f6db4531550_0 () Real)
(declare-fun E0x7f6db452ead0 () Bool)
(declare-fun v0x7f6db452e790_0 () Bool)
(declare-fun E0x7f6db4530e50 () Bool)
(declare-fun v0x7f6db4532cd0_0 () Bool)
(declare-fun E0x7f6db452e110 () Bool)
(declare-fun E0x7f6db4531b50 () Bool)
(declare-fun v0x7f6db4532f50_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7f6db4530090 () Bool)
(declare-fun v0x7f6db452e050_0 () Bool)
(declare-fun v0x7f6db452dd90_0 () Bool)
(declare-fun E0x7f6db45305d0 () Bool)
(declare-fun v0x7f6db452f710_0 () Real)
(declare-fun v0x7f6db452e510_0 () Real)
(declare-fun v0x7f6db4531190_0 () Real)
(declare-fun F0x7f6db45348d0 () Bool)
(declare-fun v0x7f6db4530310_0 () Bool)
(declare-fun v0x7f6db452f3d0_0 () Bool)
(declare-fun F0x7f6db4534990 () Bool)
(declare-fun v0x7f6db452e650_0 () Real)
(declare-fun v0x7f6db4530d90_0 () Bool)
(declare-fun E0x7f6db4531790 () Bool)
(declare-fun v0x7f6db452c010_0 () Real)
(declare-fun v0x7f6db452d750_0 () Real)
(declare-fun v0x7f6db4532910_0 () Bool)
(declare-fun v0x7f6db452e850_0 () Real)
(declare-fun v0x7f6db452ffd0_0 () Bool)
(declare-fun E0x7f6db452e910 () Bool)
(declare-fun v0x7f6db4530250_0 () Real)
(declare-fun v0x7f6db45327d0_0 () Bool)
(declare-fun E0x7f6db452f990 () Bool)
(declare-fun v0x7f6db452d650_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f6db452d690_0 () Real)
(declare-fun v0x7f6db452d550_0 () Real)
(declare-fun v0x7f6db452c110_0 () Bool)
(declare-fun E0x7f6db45330d0 () Bool)
(declare-fun v0x7f6db4533ad0_0 () Bool)
(declare-fun F0x7f6db4534a50 () Bool)
(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun v0x7f6db4533010_0 () Real)
(declare-fun v0x7f6db452df10_0 () Bool)

(assert (=> F0x7f6db4534a50
    (and v0x7f6db452c110_0
         (<= v0x7f6db452d550_0 1.0)
         (>= v0x7f6db452d550_0 1.0)
         (<= v0x7f6db452d650_0 0.0)
         (>= v0x7f6db452d650_0 0.0)
         (<= v0x7f6db452d750_0 0.0)
         (>= v0x7f6db452d750_0 0.0)
         (<= v0x7f6db452c010_0 1.0)
         (>= v0x7f6db452c010_0 1.0))))
(assert (=> F0x7f6db4534a50 F0x7f6db4534990))
(assert (let ((a!1 (=> v0x7f6db452e790_0
               (or (and v0x7f6db452e050_0
                        E0x7f6db452e910
                        (<= v0x7f6db452e850_0 v0x7f6db452e650_0)
                        (>= v0x7f6db452e850_0 v0x7f6db452e650_0))
                   (and v0x7f6db452dd90_0
                        E0x7f6db452ead0
                        v0x7f6db452df10_0
                        (<= v0x7f6db452e850_0 v0x7f6db452d690_0)
                        (>= v0x7f6db452e850_0 v0x7f6db452d690_0)))))
      (a!2 (=> v0x7f6db452e790_0
               (or (and E0x7f6db452e910 (not E0x7f6db452ead0))
                   (and E0x7f6db452ead0 (not E0x7f6db452e910)))))
      (a!3 (=> v0x7f6db452f650_0
               (or (and v0x7f6db452f110_0
                        E0x7f6db452f7d0
                        (<= v0x7f6db452f710_0 v0x7f6db452f510_0)
                        (>= v0x7f6db452f710_0 v0x7f6db452f510_0))
                   (and v0x7f6db452e790_0
                        E0x7f6db452f990
                        v0x7f6db452efd0_0
                        (<= v0x7f6db452f710_0 v0x7f6db452d590_0)
                        (>= v0x7f6db452f710_0 v0x7f6db452d590_0)))))
      (a!4 (=> v0x7f6db452f650_0
               (or (and E0x7f6db452f7d0 (not E0x7f6db452f990))
                   (and E0x7f6db452f990 (not E0x7f6db452f7d0)))))
      (a!5 (=> v0x7f6db4530450_0
               (or (and v0x7f6db452ffd0_0
                        E0x7f6db45305d0
                        v0x7f6db4530310_0
                        (<= v0x7f6db4530510_0 v0x7f6db452d310_0)
                        (>= v0x7f6db4530510_0 v0x7f6db452d310_0))
                   (and v0x7f6db452f650_0
                        E0x7f6db45307d0
                        (not v0x7f6db452fe90_0)
                        (<= v0x7f6db4530510_0 1.0)
                        (>= v0x7f6db4530510_0 1.0)))))
      (a!6 (=> v0x7f6db4530450_0
               (or (and E0x7f6db45305d0 (not E0x7f6db45307d0))
                   (and E0x7f6db45307d0 (not E0x7f6db45305d0)))))
      (a!7 (or (and v0x7f6db4530d90_0
                    E0x7f6db4531790
                    (and (<= v0x7f6db4531550_0 v0x7f6db452d490_0)
                         (>= v0x7f6db4531550_0 v0x7f6db452d490_0))
                    (and (<= v0x7f6db4531610_0 v0x7f6db4530510_0)
                         (>= v0x7f6db4531610_0 v0x7f6db4530510_0))
                    (<= v0x7f6db45316d0_0 v0x7f6db4531350_0)
                    (>= v0x7f6db45316d0_0 v0x7f6db4531350_0))
               (and v0x7f6db4530450_0
                    E0x7f6db4531b50
                    v0x7f6db4530c50_0
                    (and (<= v0x7f6db4531550_0 v0x7f6db452d490_0)
                         (>= v0x7f6db4531550_0 v0x7f6db452d490_0))
                    (and (<= v0x7f6db4531610_0 v0x7f6db4530510_0)
                         (>= v0x7f6db4531610_0 v0x7f6db4530510_0))
                    (and (<= v0x7f6db45316d0_0 v0x7f6db452e850_0)
                         (>= v0x7f6db45316d0_0 v0x7f6db452e850_0)))
               (and v0x7f6db452ffd0_0
                    E0x7f6db4531dd0
                    (not v0x7f6db4530310_0)
                    (<= v0x7f6db4531550_0 0.0)
                    (>= v0x7f6db4531550_0 0.0)
                    (<= v0x7f6db4531610_0 0.0)
                    (>= v0x7f6db4531610_0 0.0)
                    (and (<= v0x7f6db45316d0_0 v0x7f6db452e850_0)
                         (>= v0x7f6db45316d0_0 v0x7f6db452e850_0)))))
      (a!8 (=> v0x7f6db4531490_0
               (or (and E0x7f6db4531790
                        (not E0x7f6db4531b50)
                        (not E0x7f6db4531dd0))
                   (and E0x7f6db4531b50
                        (not E0x7f6db4531790)
                        (not E0x7f6db4531dd0))
                   (and E0x7f6db4531dd0
                        (not E0x7f6db4531790)
                        (not E0x7f6db4531b50)))))
      (a!9 (=> v0x7f6db4532f50_0
               (or (and v0x7f6db4532a50_0
                        E0x7f6db45330d0
                        (<= v0x7f6db4533010_0 v0x7f6db4532e10_0)
                        (>= v0x7f6db4533010_0 v0x7f6db4532e10_0))
                   (and v0x7f6db4531490_0
                        E0x7f6db4533290
                        (not v0x7f6db4532910_0)
                        (<= v0x7f6db4533010_0 v0x7f6db4531550_0)
                        (>= v0x7f6db4533010_0 v0x7f6db4531550_0)))))
      (a!10 (=> v0x7f6db4532f50_0
                (or (and E0x7f6db45330d0 (not E0x7f6db4533290))
                    (and E0x7f6db4533290 (not E0x7f6db45330d0))))))
(let ((a!11 (and (=> v0x7f6db452e050_0
                     (and v0x7f6db452dd90_0
                          E0x7f6db452e110
                          (not v0x7f6db452df10_0)))
                 (=> v0x7f6db452e050_0 E0x7f6db452e110)
                 a!1
                 a!2
                 (=> v0x7f6db452f110_0
                     (and v0x7f6db452e790_0
                          E0x7f6db452f1d0
                          (not v0x7f6db452efd0_0)))
                 (=> v0x7f6db452f110_0 E0x7f6db452f1d0)
                 a!3
                 a!4
                 (=> v0x7f6db452ffd0_0
                     (and v0x7f6db452f650_0 E0x7f6db4530090 v0x7f6db452fe90_0))
                 (=> v0x7f6db452ffd0_0 E0x7f6db4530090)
                 a!5
                 a!6
                 (=> v0x7f6db4530d90_0
                     (and v0x7f6db4530450_0
                          E0x7f6db4530e50
                          (not v0x7f6db4530c50_0)))
                 (=> v0x7f6db4530d90_0 E0x7f6db4530e50)
                 (=> v0x7f6db4531490_0 a!7)
                 a!8
                 (=> v0x7f6db4532a50_0
                     (and v0x7f6db4531490_0 E0x7f6db4532b10 v0x7f6db4532910_0))
                 (=> v0x7f6db4532a50_0 E0x7f6db4532b10)
                 a!9
                 a!10
                 v0x7f6db4532f50_0
                 (not v0x7f6db4533c10_0)
                 (<= v0x7f6db452d550_0 v0x7f6db4531610_0)
                 (>= v0x7f6db452d550_0 v0x7f6db4531610_0)
                 (<= v0x7f6db452d650_0 v0x7f6db4533010_0)
                 (>= v0x7f6db452d650_0 v0x7f6db4533010_0)
                 (<= v0x7f6db452d750_0 v0x7f6db452f710_0)
                 (>= v0x7f6db452d750_0 v0x7f6db452f710_0)
                 (<= v0x7f6db452c010_0 v0x7f6db45316d0_0)
                 (>= v0x7f6db452c010_0 v0x7f6db45316d0_0)
                 (= v0x7f6db452df10_0 (= v0x7f6db452de50_0 0.0))
                 (= v0x7f6db452e350_0 (< v0x7f6db452d690_0 2.0))
                 (= v0x7f6db452e510_0 (ite v0x7f6db452e350_0 1.0 0.0))
                 (= v0x7f6db452e650_0 (+ v0x7f6db452e510_0 v0x7f6db452d690_0))
                 (= v0x7f6db452efd0_0 (= v0x7f6db452ef10_0 0.0))
                 (= v0x7f6db452f3d0_0 (= v0x7f6db452d590_0 0.0))
                 (= v0x7f6db452f510_0 (ite v0x7f6db452f3d0_0 1.0 0.0))
                 (= v0x7f6db452fe90_0 (= v0x7f6db452fdd0_0 0.0))
                 (= v0x7f6db4530310_0 (= v0x7f6db4530250_0 0.0))
                 (= v0x7f6db4530c50_0 (= v0x7f6db452d490_0 0.0))
                 (= v0x7f6db4531050_0 (> v0x7f6db452e850_0 0.0))
                 (= v0x7f6db4531190_0 (+ v0x7f6db452e850_0 (- 1.0)))
                 (= v0x7f6db4531350_0
                    (ite v0x7f6db4531050_0 v0x7f6db4531190_0 v0x7f6db452e850_0))
                 (= v0x7f6db4532410_0 (not (= v0x7f6db4531610_0 0.0)))
                 (= v0x7f6db4532550_0 (= v0x7f6db4531550_0 0.0))
                 (= v0x7f6db4532690_0 (> v0x7f6db45316d0_0 1.0))
                 (= v0x7f6db45327d0_0 (and v0x7f6db4532410_0 v0x7f6db4532550_0))
                 (= v0x7f6db4532910_0 (and v0x7f6db45327d0_0 v0x7f6db4532690_0))
                 (= v0x7f6db4532cd0_0 (= v0x7f6db452f710_0 0.0))
                 (= v0x7f6db4532e10_0
                    (ite v0x7f6db4532cd0_0 1.0 v0x7f6db4531550_0))
                 (= v0x7f6db4533710_0 (= v0x7f6db45316d0_0 2.0))
                 (= v0x7f6db4533850_0 (= v0x7f6db4533010_0 0.0))
                 (= v0x7f6db4533990_0 (or v0x7f6db4533850_0 v0x7f6db4533710_0))
                 (= v0x7f6db4533ad0_0 (xor v0x7f6db4533990_0 true))
                 (= v0x7f6db4533c10_0 (and v0x7f6db4532550_0 v0x7f6db4533ad0_0)))))
  (=> F0x7f6db45348d0 a!11))))
(assert (=> F0x7f6db45348d0 F0x7f6db4534a90))
(assert (let ((a!1 (=> v0x7f6db452e790_0
               (or (and v0x7f6db452e050_0
                        E0x7f6db452e910
                        (<= v0x7f6db452e850_0 v0x7f6db452e650_0)
                        (>= v0x7f6db452e850_0 v0x7f6db452e650_0))
                   (and v0x7f6db452dd90_0
                        E0x7f6db452ead0
                        v0x7f6db452df10_0
                        (<= v0x7f6db452e850_0 v0x7f6db452d690_0)
                        (>= v0x7f6db452e850_0 v0x7f6db452d690_0)))))
      (a!2 (=> v0x7f6db452e790_0
               (or (and E0x7f6db452e910 (not E0x7f6db452ead0))
                   (and E0x7f6db452ead0 (not E0x7f6db452e910)))))
      (a!3 (=> v0x7f6db452f650_0
               (or (and v0x7f6db452f110_0
                        E0x7f6db452f7d0
                        (<= v0x7f6db452f710_0 v0x7f6db452f510_0)
                        (>= v0x7f6db452f710_0 v0x7f6db452f510_0))
                   (and v0x7f6db452e790_0
                        E0x7f6db452f990
                        v0x7f6db452efd0_0
                        (<= v0x7f6db452f710_0 v0x7f6db452d590_0)
                        (>= v0x7f6db452f710_0 v0x7f6db452d590_0)))))
      (a!4 (=> v0x7f6db452f650_0
               (or (and E0x7f6db452f7d0 (not E0x7f6db452f990))
                   (and E0x7f6db452f990 (not E0x7f6db452f7d0)))))
      (a!5 (=> v0x7f6db4530450_0
               (or (and v0x7f6db452ffd0_0
                        E0x7f6db45305d0
                        v0x7f6db4530310_0
                        (<= v0x7f6db4530510_0 v0x7f6db452d310_0)
                        (>= v0x7f6db4530510_0 v0x7f6db452d310_0))
                   (and v0x7f6db452f650_0
                        E0x7f6db45307d0
                        (not v0x7f6db452fe90_0)
                        (<= v0x7f6db4530510_0 1.0)
                        (>= v0x7f6db4530510_0 1.0)))))
      (a!6 (=> v0x7f6db4530450_0
               (or (and E0x7f6db45305d0 (not E0x7f6db45307d0))
                   (and E0x7f6db45307d0 (not E0x7f6db45305d0)))))
      (a!7 (or (and v0x7f6db4530d90_0
                    E0x7f6db4531790
                    (and (<= v0x7f6db4531550_0 v0x7f6db452d490_0)
                         (>= v0x7f6db4531550_0 v0x7f6db452d490_0))
                    (and (<= v0x7f6db4531610_0 v0x7f6db4530510_0)
                         (>= v0x7f6db4531610_0 v0x7f6db4530510_0))
                    (<= v0x7f6db45316d0_0 v0x7f6db4531350_0)
                    (>= v0x7f6db45316d0_0 v0x7f6db4531350_0))
               (and v0x7f6db4530450_0
                    E0x7f6db4531b50
                    v0x7f6db4530c50_0
                    (and (<= v0x7f6db4531550_0 v0x7f6db452d490_0)
                         (>= v0x7f6db4531550_0 v0x7f6db452d490_0))
                    (and (<= v0x7f6db4531610_0 v0x7f6db4530510_0)
                         (>= v0x7f6db4531610_0 v0x7f6db4530510_0))
                    (and (<= v0x7f6db45316d0_0 v0x7f6db452e850_0)
                         (>= v0x7f6db45316d0_0 v0x7f6db452e850_0)))
               (and v0x7f6db452ffd0_0
                    E0x7f6db4531dd0
                    (not v0x7f6db4530310_0)
                    (<= v0x7f6db4531550_0 0.0)
                    (>= v0x7f6db4531550_0 0.0)
                    (<= v0x7f6db4531610_0 0.0)
                    (>= v0x7f6db4531610_0 0.0)
                    (and (<= v0x7f6db45316d0_0 v0x7f6db452e850_0)
                         (>= v0x7f6db45316d0_0 v0x7f6db452e850_0)))))
      (a!8 (=> v0x7f6db4531490_0
               (or (and E0x7f6db4531790
                        (not E0x7f6db4531b50)
                        (not E0x7f6db4531dd0))
                   (and E0x7f6db4531b50
                        (not E0x7f6db4531790)
                        (not E0x7f6db4531dd0))
                   (and E0x7f6db4531dd0
                        (not E0x7f6db4531790)
                        (not E0x7f6db4531b50)))))
      (a!9 (=> v0x7f6db4532f50_0
               (or (and v0x7f6db4532a50_0
                        E0x7f6db45330d0
                        (<= v0x7f6db4533010_0 v0x7f6db4532e10_0)
                        (>= v0x7f6db4533010_0 v0x7f6db4532e10_0))
                   (and v0x7f6db4531490_0
                        E0x7f6db4533290
                        (not v0x7f6db4532910_0)
                        (<= v0x7f6db4533010_0 v0x7f6db4531550_0)
                        (>= v0x7f6db4533010_0 v0x7f6db4531550_0)))))
      (a!10 (=> v0x7f6db4532f50_0
                (or (and E0x7f6db45330d0 (not E0x7f6db4533290))
                    (and E0x7f6db4533290 (not E0x7f6db45330d0))))))
(let ((a!11 (and (=> v0x7f6db452e050_0
                     (and v0x7f6db452dd90_0
                          E0x7f6db452e110
                          (not v0x7f6db452df10_0)))
                 (=> v0x7f6db452e050_0 E0x7f6db452e110)
                 a!1
                 a!2
                 (=> v0x7f6db452f110_0
                     (and v0x7f6db452e790_0
                          E0x7f6db452f1d0
                          (not v0x7f6db452efd0_0)))
                 (=> v0x7f6db452f110_0 E0x7f6db452f1d0)
                 a!3
                 a!4
                 (=> v0x7f6db452ffd0_0
                     (and v0x7f6db452f650_0 E0x7f6db4530090 v0x7f6db452fe90_0))
                 (=> v0x7f6db452ffd0_0 E0x7f6db4530090)
                 a!5
                 a!6
                 (=> v0x7f6db4530d90_0
                     (and v0x7f6db4530450_0
                          E0x7f6db4530e50
                          (not v0x7f6db4530c50_0)))
                 (=> v0x7f6db4530d90_0 E0x7f6db4530e50)
                 (=> v0x7f6db4531490_0 a!7)
                 a!8
                 (=> v0x7f6db4532a50_0
                     (and v0x7f6db4531490_0 E0x7f6db4532b10 v0x7f6db4532910_0))
                 (=> v0x7f6db4532a50_0 E0x7f6db4532b10)
                 a!9
                 a!10
                 v0x7f6db4532f50_0
                 v0x7f6db4533c10_0
                 (= v0x7f6db452df10_0 (= v0x7f6db452de50_0 0.0))
                 (= v0x7f6db452e350_0 (< v0x7f6db452d690_0 2.0))
                 (= v0x7f6db452e510_0 (ite v0x7f6db452e350_0 1.0 0.0))
                 (= v0x7f6db452e650_0 (+ v0x7f6db452e510_0 v0x7f6db452d690_0))
                 (= v0x7f6db452efd0_0 (= v0x7f6db452ef10_0 0.0))
                 (= v0x7f6db452f3d0_0 (= v0x7f6db452d590_0 0.0))
                 (= v0x7f6db452f510_0 (ite v0x7f6db452f3d0_0 1.0 0.0))
                 (= v0x7f6db452fe90_0 (= v0x7f6db452fdd0_0 0.0))
                 (= v0x7f6db4530310_0 (= v0x7f6db4530250_0 0.0))
                 (= v0x7f6db4530c50_0 (= v0x7f6db452d490_0 0.0))
                 (= v0x7f6db4531050_0 (> v0x7f6db452e850_0 0.0))
                 (= v0x7f6db4531190_0 (+ v0x7f6db452e850_0 (- 1.0)))
                 (= v0x7f6db4531350_0
                    (ite v0x7f6db4531050_0 v0x7f6db4531190_0 v0x7f6db452e850_0))
                 (= v0x7f6db4532410_0 (not (= v0x7f6db4531610_0 0.0)))
                 (= v0x7f6db4532550_0 (= v0x7f6db4531550_0 0.0))
                 (= v0x7f6db4532690_0 (> v0x7f6db45316d0_0 1.0))
                 (= v0x7f6db45327d0_0 (and v0x7f6db4532410_0 v0x7f6db4532550_0))
                 (= v0x7f6db4532910_0 (and v0x7f6db45327d0_0 v0x7f6db4532690_0))
                 (= v0x7f6db4532cd0_0 (= v0x7f6db452f710_0 0.0))
                 (= v0x7f6db4532e10_0
                    (ite v0x7f6db4532cd0_0 1.0 v0x7f6db4531550_0))
                 (= v0x7f6db4533710_0 (= v0x7f6db45316d0_0 2.0))
                 (= v0x7f6db4533850_0 (= v0x7f6db4533010_0 0.0))
                 (= v0x7f6db4533990_0 (or v0x7f6db4533850_0 v0x7f6db4533710_0))
                 (= v0x7f6db4533ad0_0 (xor v0x7f6db4533990_0 true))
                 (= v0x7f6db4533c10_0 (and v0x7f6db4532550_0 v0x7f6db4533ad0_0)))))
  (=> F0x7f6db4534b50 a!11))))
(assert (=> F0x7f6db4534b50 F0x7f6db4534a90))
(assert (=> F0x7f6db4534c50 (or F0x7f6db4534a50 F0x7f6db45348d0)))
(assert (=> F0x7f6db4534c10 F0x7f6db4534b50))
(assert (=> pre!entry!0 (=> F0x7f6db4534990 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f6db4534a90 (<= v0x7f6db452d690_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f6db4534a90
        (or (<= v0x7f6db452d690_0 1.0) (>= v0x7f6db452d690_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f6db4534a90 (>= v0x7f6db452d690_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f6db4534a90
        (or (>= v0x7f6db452d690_0 1.0) (<= v0x7f6db452d690_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f6db4534c50
                (not (or (<= v0x7f6db452c010_0 1.0) (>= v0x7f6db452c010_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7f6db4534c50
                (not (or (>= v0x7f6db452c010_0 1.0) (<= v0x7f6db452c010_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f6db4534c50
           (not (<= v0x7f6db452c010_0 2.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f6db4534c50
           (not (>= v0x7f6db452c010_0 0.0)))
      a!2
      (and (not post!bb2.i.i23.i.i!0) F0x7f6db4534c10 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i23.i.i!0)
