(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7fe5ddb20d50 () Bool)
(declare-fun v0x7fe5ddb1fb90_0 () Bool)
(declare-fun v0x7fe5ddb1fa50_0 () Bool)
(declare-fun v0x7fe5ddb1e310_0 () Real)
(declare-fun v0x7fe5ddb1e1d0_0 () Bool)
(declare-fun F0x7fe5ddb20b50 () Bool)
(declare-fun v0x7fe5ddb1dc90_0 () Bool)
(declare-fun v0x7fe5ddb1e750_0 () Bool)
(declare-fun v0x7fe5ddb1fcd0_0 () Bool)
(declare-fun v0x7fe5ddb1c950_0 () Real)
(declare-fun F0x7fe5ddb20d10 () Bool)
(declare-fun v0x7fe5ddb1fe10_0 () Bool)
(declare-fun v0x7fe5ddb1ce10_0 () Bool)
(declare-fun v0x7fe5ddb1e4d0_0 () Real)
(declare-fun E0x7fe5ddb1f150 () Bool)
(declare-fun v0x7fe5ddb1ed10_0 () Real)
(declare-fun v0x7fe5ddb1ec50_0 () Bool)
(declare-fun v0x7fe5ddb1bf50_0 () Real)
(declare-fun v0x7fe5ddb1e890_0 () Bool)
(declare-fun v0x7fe5ddb1e9d0_0 () Bool)
(declare-fun E0x7fe5ddb1f410 () Bool)
(declare-fun E0x7fe5ddb1dfd0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fe5ddb1df10_0 () Bool)
(declare-fun E0x7fe5ddb1dad0 () Bool)
(declare-fun v0x7fe5ddb1b890_0 () Real)
(declare-fun v0x7fe5ddb1da10_0 () Bool)
(declare-fun v0x7fe5ddb1b110_0 () Real)
(declare-fun v0x7fe5ddb1af90_0 () Real)
(declare-fun v0x7fe5ddb1edd0_0 () Real)
(declare-fun E0x7fe5ddb1d3d0 () Bool)
(declare-fun v0x7fe5ddb1cf50_0 () Real)
(declare-fun E0x7fe5ddb1ea90 () Bool)
(declare-fun v0x7fe5ddb1cb50_0 () Bool)
(declare-fun v0x7fe5ddb1d150_0 () Real)
(declare-fun E0x7fe5ddb1c510 () Bool)
(declare-fun v0x7fe5ddb1c290_0 () Real)
(declare-fun v0x7fe5ddb1ca10_0 () Bool)
(declare-fun E0x7fe5ddb1cc10 () Bool)
(declare-fun E0x7fe5ddb1ee90 () Bool)
(declare-fun v0x7fe5ddb1b7d0_0 () Bool)
(declare-fun E0x7fe5ddb1c350 () Bool)
(declare-fun v0x7fe5ddb1ba90_0 () Bool)
(declare-fun v0x7fe5ddb1e610_0 () Bool)
(declare-fun v0x7fe5ddb1f950_0 () Bool)
(declare-fun E0x7fe5ddb1bb50 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fe5ddb1d8d0_0 () Bool)
(declare-fun F0x7fe5ddb20a90 () Bool)
(declare-fun v0x7fe5ddb1d090_0 () Bool)
(declare-fun F0x7fe5ddb20950 () Bool)
(declare-fun v0x7fe5ddb1c1d0_0 () Bool)
(declare-fun v0x7fe5ddb18010_0 () Real)
(declare-fun v0x7fe5ddb1b210_0 () Real)
(declare-fun v0x7fe5ddb1ddd0_0 () Real)
(declare-fun v0x7fe5ddb1b2d0_0 () Real)
(declare-fun v0x7fe5ddb1c090_0 () Real)
(declare-fun v0x7fe5ddb1b950_0 () Bool)
(declare-fun v0x7fe5ddb1b1d0_0 () Real)
(declare-fun v0x7fe5ddb1bd90_0 () Bool)
(declare-fun v0x7fe5ddb18110_0 () Bool)
(declare-fun E0x7fe5ddb1d210 () Bool)
(declare-fun F0x7fe5ddb20c10 () Bool)
(declare-fun F0x7fe5ddb20a50 () Bool)

(assert (=> F0x7fe5ddb20a50
    (and v0x7fe5ddb18110_0
         (<= v0x7fe5ddb1b1d0_0 0.0)
         (>= v0x7fe5ddb1b1d0_0 0.0)
         (<= v0x7fe5ddb1b2d0_0 0.0)
         (>= v0x7fe5ddb1b2d0_0 0.0)
         (<= v0x7fe5ddb18010_0 1.0)
         (>= v0x7fe5ddb18010_0 1.0))))
(assert (=> F0x7fe5ddb20a50 F0x7fe5ddb20950))
(assert (let ((a!1 (=> v0x7fe5ddb1c1d0_0
               (or (and v0x7fe5ddb1ba90_0
                        E0x7fe5ddb1c350
                        (<= v0x7fe5ddb1c290_0 v0x7fe5ddb1c090_0)
                        (>= v0x7fe5ddb1c290_0 v0x7fe5ddb1c090_0))
                   (and v0x7fe5ddb1b7d0_0
                        E0x7fe5ddb1c510
                        v0x7fe5ddb1b950_0
                        (<= v0x7fe5ddb1c290_0 v0x7fe5ddb1b210_0)
                        (>= v0x7fe5ddb1c290_0 v0x7fe5ddb1b210_0)))))
      (a!2 (=> v0x7fe5ddb1c1d0_0
               (or (and E0x7fe5ddb1c350 (not E0x7fe5ddb1c510))
                   (and E0x7fe5ddb1c510 (not E0x7fe5ddb1c350)))))
      (a!3 (=> v0x7fe5ddb1d090_0
               (or (and v0x7fe5ddb1cb50_0
                        E0x7fe5ddb1d210
                        (<= v0x7fe5ddb1d150_0 v0x7fe5ddb1cf50_0)
                        (>= v0x7fe5ddb1d150_0 v0x7fe5ddb1cf50_0))
                   (and v0x7fe5ddb1c1d0_0
                        E0x7fe5ddb1d3d0
                        v0x7fe5ddb1ca10_0
                        (<= v0x7fe5ddb1d150_0 v0x7fe5ddb1b110_0)
                        (>= v0x7fe5ddb1d150_0 v0x7fe5ddb1b110_0)))))
      (a!4 (=> v0x7fe5ddb1d090_0
               (or (and E0x7fe5ddb1d210 (not E0x7fe5ddb1d3d0))
                   (and E0x7fe5ddb1d3d0 (not E0x7fe5ddb1d210)))))
      (a!5 (or (and v0x7fe5ddb1da10_0
                    E0x7fe5ddb1ee90
                    (<= v0x7fe5ddb1ed10_0 v0x7fe5ddb1c290_0)
                    (>= v0x7fe5ddb1ed10_0 v0x7fe5ddb1c290_0)
                    (<= v0x7fe5ddb1edd0_0 v0x7fe5ddb1ddd0_0)
                    (>= v0x7fe5ddb1edd0_0 v0x7fe5ddb1ddd0_0))
               (and v0x7fe5ddb1e9d0_0
                    E0x7fe5ddb1f150
                    (and (<= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0)
                         (>= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0))
                    (<= v0x7fe5ddb1edd0_0 v0x7fe5ddb1af90_0)
                    (>= v0x7fe5ddb1edd0_0 v0x7fe5ddb1af90_0))
               (and v0x7fe5ddb1df10_0
                    E0x7fe5ddb1f410
                    (not v0x7fe5ddb1e890_0)
                    (and (<= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0)
                         (>= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0))
                    (<= v0x7fe5ddb1edd0_0 0.0)
                    (>= v0x7fe5ddb1edd0_0 0.0))))
      (a!6 (=> v0x7fe5ddb1ec50_0
               (or (and E0x7fe5ddb1ee90
                        (not E0x7fe5ddb1f150)
                        (not E0x7fe5ddb1f410))
                   (and E0x7fe5ddb1f150
                        (not E0x7fe5ddb1ee90)
                        (not E0x7fe5ddb1f410))
                   (and E0x7fe5ddb1f410
                        (not E0x7fe5ddb1ee90)
                        (not E0x7fe5ddb1f150))))))
(let ((a!7 (and (=> v0x7fe5ddb1ba90_0
                    (and v0x7fe5ddb1b7d0_0
                         E0x7fe5ddb1bb50
                         (not v0x7fe5ddb1b950_0)))
                (=> v0x7fe5ddb1ba90_0 E0x7fe5ddb1bb50)
                a!1
                a!2
                (=> v0x7fe5ddb1cb50_0
                    (and v0x7fe5ddb1c1d0_0
                         E0x7fe5ddb1cc10
                         (not v0x7fe5ddb1ca10_0)))
                (=> v0x7fe5ddb1cb50_0 E0x7fe5ddb1cc10)
                a!3
                a!4
                (=> v0x7fe5ddb1da10_0
                    (and v0x7fe5ddb1d090_0 E0x7fe5ddb1dad0 v0x7fe5ddb1d8d0_0))
                (=> v0x7fe5ddb1da10_0 E0x7fe5ddb1dad0)
                (=> v0x7fe5ddb1df10_0
                    (and v0x7fe5ddb1d090_0
                         E0x7fe5ddb1dfd0
                         (not v0x7fe5ddb1d8d0_0)))
                (=> v0x7fe5ddb1df10_0 E0x7fe5ddb1dfd0)
                (=> v0x7fe5ddb1e9d0_0
                    (and v0x7fe5ddb1df10_0 E0x7fe5ddb1ea90 v0x7fe5ddb1e890_0))
                (=> v0x7fe5ddb1e9d0_0 E0x7fe5ddb1ea90)
                (=> v0x7fe5ddb1ec50_0 a!5)
                a!6
                v0x7fe5ddb1ec50_0
                (not v0x7fe5ddb1fe10_0)
                (<= v0x7fe5ddb1b1d0_0 v0x7fe5ddb1edd0_0)
                (>= v0x7fe5ddb1b1d0_0 v0x7fe5ddb1edd0_0)
                (<= v0x7fe5ddb1b2d0_0 v0x7fe5ddb1d150_0)
                (>= v0x7fe5ddb1b2d0_0 v0x7fe5ddb1d150_0)
                (<= v0x7fe5ddb18010_0 v0x7fe5ddb1ed10_0)
                (>= v0x7fe5ddb18010_0 v0x7fe5ddb1ed10_0)
                (= v0x7fe5ddb1b950_0 (= v0x7fe5ddb1b890_0 0.0))
                (= v0x7fe5ddb1bd90_0 (< v0x7fe5ddb1b210_0 2.0))
                (= v0x7fe5ddb1bf50_0 (ite v0x7fe5ddb1bd90_0 1.0 0.0))
                (= v0x7fe5ddb1c090_0 (+ v0x7fe5ddb1bf50_0 v0x7fe5ddb1b210_0))
                (= v0x7fe5ddb1ca10_0 (= v0x7fe5ddb1c950_0 0.0))
                (= v0x7fe5ddb1ce10_0 (= v0x7fe5ddb1b110_0 0.0))
                (= v0x7fe5ddb1cf50_0 (ite v0x7fe5ddb1ce10_0 1.0 0.0))
                (= v0x7fe5ddb1d8d0_0 (= v0x7fe5ddb1af90_0 0.0))
                (= v0x7fe5ddb1dc90_0 (> v0x7fe5ddb1c290_0 1.0))
                (= v0x7fe5ddb1ddd0_0
                   (ite v0x7fe5ddb1dc90_0 1.0 v0x7fe5ddb1af90_0))
                (= v0x7fe5ddb1e1d0_0 (> v0x7fe5ddb1c290_0 0.0))
                (= v0x7fe5ddb1e310_0 (+ v0x7fe5ddb1c290_0 (- 1.0)))
                (= v0x7fe5ddb1e4d0_0
                   (ite v0x7fe5ddb1e1d0_0 v0x7fe5ddb1e310_0 v0x7fe5ddb1c290_0))
                (= v0x7fe5ddb1e610_0 (= v0x7fe5ddb1d150_0 0.0))
                (= v0x7fe5ddb1e750_0 (= v0x7fe5ddb1e4d0_0 0.0))
                (= v0x7fe5ddb1e890_0 (and v0x7fe5ddb1e610_0 v0x7fe5ddb1e750_0))
                (= v0x7fe5ddb1f950_0 (= v0x7fe5ddb1d150_0 0.0))
                (= v0x7fe5ddb1fa50_0 (= v0x7fe5ddb1ed10_0 2.0))
                (= v0x7fe5ddb1fb90_0 (= v0x7fe5ddb1edd0_0 0.0))
                (= v0x7fe5ddb1fcd0_0 (and v0x7fe5ddb1fa50_0 v0x7fe5ddb1f950_0))
                (= v0x7fe5ddb1fe10_0 (and v0x7fe5ddb1fcd0_0 v0x7fe5ddb1fb90_0)))))
  (=> F0x7fe5ddb20a90 a!7))))
(assert (=> F0x7fe5ddb20a90 F0x7fe5ddb20b50))
(assert (let ((a!1 (=> v0x7fe5ddb1c1d0_0
               (or (and v0x7fe5ddb1ba90_0
                        E0x7fe5ddb1c350
                        (<= v0x7fe5ddb1c290_0 v0x7fe5ddb1c090_0)
                        (>= v0x7fe5ddb1c290_0 v0x7fe5ddb1c090_0))
                   (and v0x7fe5ddb1b7d0_0
                        E0x7fe5ddb1c510
                        v0x7fe5ddb1b950_0
                        (<= v0x7fe5ddb1c290_0 v0x7fe5ddb1b210_0)
                        (>= v0x7fe5ddb1c290_0 v0x7fe5ddb1b210_0)))))
      (a!2 (=> v0x7fe5ddb1c1d0_0
               (or (and E0x7fe5ddb1c350 (not E0x7fe5ddb1c510))
                   (and E0x7fe5ddb1c510 (not E0x7fe5ddb1c350)))))
      (a!3 (=> v0x7fe5ddb1d090_0
               (or (and v0x7fe5ddb1cb50_0
                        E0x7fe5ddb1d210
                        (<= v0x7fe5ddb1d150_0 v0x7fe5ddb1cf50_0)
                        (>= v0x7fe5ddb1d150_0 v0x7fe5ddb1cf50_0))
                   (and v0x7fe5ddb1c1d0_0
                        E0x7fe5ddb1d3d0
                        v0x7fe5ddb1ca10_0
                        (<= v0x7fe5ddb1d150_0 v0x7fe5ddb1b110_0)
                        (>= v0x7fe5ddb1d150_0 v0x7fe5ddb1b110_0)))))
      (a!4 (=> v0x7fe5ddb1d090_0
               (or (and E0x7fe5ddb1d210 (not E0x7fe5ddb1d3d0))
                   (and E0x7fe5ddb1d3d0 (not E0x7fe5ddb1d210)))))
      (a!5 (or (and v0x7fe5ddb1da10_0
                    E0x7fe5ddb1ee90
                    (<= v0x7fe5ddb1ed10_0 v0x7fe5ddb1c290_0)
                    (>= v0x7fe5ddb1ed10_0 v0x7fe5ddb1c290_0)
                    (<= v0x7fe5ddb1edd0_0 v0x7fe5ddb1ddd0_0)
                    (>= v0x7fe5ddb1edd0_0 v0x7fe5ddb1ddd0_0))
               (and v0x7fe5ddb1e9d0_0
                    E0x7fe5ddb1f150
                    (and (<= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0)
                         (>= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0))
                    (<= v0x7fe5ddb1edd0_0 v0x7fe5ddb1af90_0)
                    (>= v0x7fe5ddb1edd0_0 v0x7fe5ddb1af90_0))
               (and v0x7fe5ddb1df10_0
                    E0x7fe5ddb1f410
                    (not v0x7fe5ddb1e890_0)
                    (and (<= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0)
                         (>= v0x7fe5ddb1ed10_0 v0x7fe5ddb1e4d0_0))
                    (<= v0x7fe5ddb1edd0_0 0.0)
                    (>= v0x7fe5ddb1edd0_0 0.0))))
      (a!6 (=> v0x7fe5ddb1ec50_0
               (or (and E0x7fe5ddb1ee90
                        (not E0x7fe5ddb1f150)
                        (not E0x7fe5ddb1f410))
                   (and E0x7fe5ddb1f150
                        (not E0x7fe5ddb1ee90)
                        (not E0x7fe5ddb1f410))
                   (and E0x7fe5ddb1f410
                        (not E0x7fe5ddb1ee90)
                        (not E0x7fe5ddb1f150))))))
(let ((a!7 (and (=> v0x7fe5ddb1ba90_0
                    (and v0x7fe5ddb1b7d0_0
                         E0x7fe5ddb1bb50
                         (not v0x7fe5ddb1b950_0)))
                (=> v0x7fe5ddb1ba90_0 E0x7fe5ddb1bb50)
                a!1
                a!2
                (=> v0x7fe5ddb1cb50_0
                    (and v0x7fe5ddb1c1d0_0
                         E0x7fe5ddb1cc10
                         (not v0x7fe5ddb1ca10_0)))
                (=> v0x7fe5ddb1cb50_0 E0x7fe5ddb1cc10)
                a!3
                a!4
                (=> v0x7fe5ddb1da10_0
                    (and v0x7fe5ddb1d090_0 E0x7fe5ddb1dad0 v0x7fe5ddb1d8d0_0))
                (=> v0x7fe5ddb1da10_0 E0x7fe5ddb1dad0)
                (=> v0x7fe5ddb1df10_0
                    (and v0x7fe5ddb1d090_0
                         E0x7fe5ddb1dfd0
                         (not v0x7fe5ddb1d8d0_0)))
                (=> v0x7fe5ddb1df10_0 E0x7fe5ddb1dfd0)
                (=> v0x7fe5ddb1e9d0_0
                    (and v0x7fe5ddb1df10_0 E0x7fe5ddb1ea90 v0x7fe5ddb1e890_0))
                (=> v0x7fe5ddb1e9d0_0 E0x7fe5ddb1ea90)
                (=> v0x7fe5ddb1ec50_0 a!5)
                a!6
                v0x7fe5ddb1ec50_0
                v0x7fe5ddb1fe10_0
                (= v0x7fe5ddb1b950_0 (= v0x7fe5ddb1b890_0 0.0))
                (= v0x7fe5ddb1bd90_0 (< v0x7fe5ddb1b210_0 2.0))
                (= v0x7fe5ddb1bf50_0 (ite v0x7fe5ddb1bd90_0 1.0 0.0))
                (= v0x7fe5ddb1c090_0 (+ v0x7fe5ddb1bf50_0 v0x7fe5ddb1b210_0))
                (= v0x7fe5ddb1ca10_0 (= v0x7fe5ddb1c950_0 0.0))
                (= v0x7fe5ddb1ce10_0 (= v0x7fe5ddb1b110_0 0.0))
                (= v0x7fe5ddb1cf50_0 (ite v0x7fe5ddb1ce10_0 1.0 0.0))
                (= v0x7fe5ddb1d8d0_0 (= v0x7fe5ddb1af90_0 0.0))
                (= v0x7fe5ddb1dc90_0 (> v0x7fe5ddb1c290_0 1.0))
                (= v0x7fe5ddb1ddd0_0
                   (ite v0x7fe5ddb1dc90_0 1.0 v0x7fe5ddb1af90_0))
                (= v0x7fe5ddb1e1d0_0 (> v0x7fe5ddb1c290_0 0.0))
                (= v0x7fe5ddb1e310_0 (+ v0x7fe5ddb1c290_0 (- 1.0)))
                (= v0x7fe5ddb1e4d0_0
                   (ite v0x7fe5ddb1e1d0_0 v0x7fe5ddb1e310_0 v0x7fe5ddb1c290_0))
                (= v0x7fe5ddb1e610_0 (= v0x7fe5ddb1d150_0 0.0))
                (= v0x7fe5ddb1e750_0 (= v0x7fe5ddb1e4d0_0 0.0))
                (= v0x7fe5ddb1e890_0 (and v0x7fe5ddb1e610_0 v0x7fe5ddb1e750_0))
                (= v0x7fe5ddb1f950_0 (= v0x7fe5ddb1d150_0 0.0))
                (= v0x7fe5ddb1fa50_0 (= v0x7fe5ddb1ed10_0 2.0))
                (= v0x7fe5ddb1fb90_0 (= v0x7fe5ddb1edd0_0 0.0))
                (= v0x7fe5ddb1fcd0_0 (and v0x7fe5ddb1fa50_0 v0x7fe5ddb1f950_0))
                (= v0x7fe5ddb1fe10_0 (and v0x7fe5ddb1fcd0_0 v0x7fe5ddb1fb90_0)))))
  (=> F0x7fe5ddb20c10 a!7))))
(assert (=> F0x7fe5ddb20c10 F0x7fe5ddb20b50))
(assert (=> F0x7fe5ddb20d50 (or F0x7fe5ddb20a50 F0x7fe5ddb20a90)))
(assert (=> F0x7fe5ddb20d10 F0x7fe5ddb20c10))
(assert (=> pre!entry!0 (=> F0x7fe5ddb20950 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fe5ddb20b50 (>= v0x7fe5ddb1af90_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7fe5ddb20b50 (not (<= 3.0 v0x7fe5ddb1b210_0)))))
(assert (or (and (not post!bb1.i.i!0) F0x7fe5ddb20d50 (not (>= v0x7fe5ddb1b1d0_0 0.0)))
    (and (not post!bb1.i.i!1) F0x7fe5ddb20d50 (<= 3.0 v0x7fe5ddb18010_0))
    (and (not post!bb2.i.i35.i.i!0) F0x7fe5ddb20d10 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
