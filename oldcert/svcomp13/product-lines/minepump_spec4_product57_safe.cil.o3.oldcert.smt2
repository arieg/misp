(declare-fun post!bb1.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f290aacba90 () Bool)
(declare-fun v0x7f290aac7c10_0 () Bool)
(declare-fun E0x7f290aaca510 () Bool)
(declare-fun v0x7f290aac9190_0 () Real)
(declare-fun E0x7f290aaca310 () Bool)
(declare-fun v0x7f290aac6010_0 () Real)
(declare-fun E0x7f290aaca090 () Bool)
(declare-fun v0x7f290aac97d0_0 () Real)
(declare-fun v0x7f290aac9d10_0 () Real)
(declare-fun E0x7f290aac9dd0 () Bool)
(declare-fun v0x7f290aac7750_0 () Real)
(declare-fun v0x7f290aac9b90_0 () Bool)
(declare-fun v0x7f290aac92d0_0 () Bool)
(declare-fun v0x7f290aac8a90_0 () Bool)
(declare-fun v0x7f290aacaad0_0 () Bool)
(declare-fun E0x7f290aac94d0 () Bool)
(declare-fun v0x7f290aac9410_0 () Bool)
(declare-fun v0x7f290aac8fd0_0 () Real)
(declare-fun E0x7f290aac8c90 () Bool)
(declare-fun v0x7f290aac8bd0_0 () Bool)
(declare-fun E0x7f290aac88d0 () Bool)
(declare-fun v0x7f290aac86d0_0 () Bool)
(declare-fun v0x7f290aac5d90_0 () Real)
(declare-fun F0x7f290aacbc90 () Bool)
(declare-fun E0x7f290aac81d0 () Bool)
(declare-fun v0x7f290aac7d50_0 () Real)
(declare-fun v0x7f290aac7f50_0 () Real)
(declare-fun v0x7f290aac8810_0 () Bool)
(declare-fun v0x7f290aac5f10_0 () Real)
(declare-fun E0x7f290aac7310 () Bool)
(declare-fun F0x7f290aacbc50 () Bool)
(declare-fun E0x7f290aac7150 () Bool)
(declare-fun v0x7f290aac6750_0 () Bool)
(declare-fun v0x7f290aac7090_0 () Real)
(declare-fun E0x7f290aac6950 () Bool)
(declare-fun v0x7f290aac7e90_0 () Bool)
(declare-fun v0x7f290aac7950_0 () Bool)
(declare-fun v0x7f290aac6890_0 () Bool)
(declare-fun v0x7f290aac9910_0 () Bool)
(declare-fun v0x7f290aac6fd0_0 () Bool)
(declare-fun v0x7f290aac6690_0 () Real)
(declare-fun F0x7f290aacb9d0 () Bool)
(declare-fun F0x7f290aacbb50 () Bool)
(declare-fun v0x7f290aac6b90_0 () Bool)
(declare-fun E0x7f290aac8010 () Bool)
(declare-fun v0x7f290aacad50_0 () Bool)
(declare-fun v0x7f290aac6e90_0 () Real)
(declare-fun v0x7f290aac65d0_0 () Bool)
(declare-fun E0x7f290aac7a10 () Bool)
(declare-fun F0x7f290aacb8d0 () Bool)
(declare-fun v0x7f290aac4010_0 () Real)
(declare-fun v0x7f290aac8e90_0 () Bool)
(declare-fun v0x7f290aac7810_0 () Bool)
(declare-fun v0x7f290aac6d50_0 () Real)
(declare-fun v0x7f290aac60d0_0 () Real)
(declare-fun v0x7f290aac5fd0_0 () Real)
(declare-fun v0x7f290aac9c50_0 () Real)
(declare-fun v0x7f290aac9690_0 () Bool)
(declare-fun v0x7f290aac4110_0 () Bool)
(declare-fun v0x7f290aacac10_0 () Bool)
(declare-fun E0x7f290aac99d0 () Bool)
(declare-fun F0x7f290aacb990 () Bool)

(assert (=> F0x7f290aacb990
    (and v0x7f290aac4110_0
         (<= v0x7f290aac5fd0_0 0.0)
         (>= v0x7f290aac5fd0_0 0.0)
         (<= v0x7f290aac60d0_0 1.0)
         (>= v0x7f290aac60d0_0 1.0)
         (<= v0x7f290aac4010_0 0.0)
         (>= v0x7f290aac4010_0 0.0))))
(assert (=> F0x7f290aacb990 F0x7f290aacb8d0))
(assert (let ((a!1 (=> v0x7f290aac6fd0_0
               (or (and v0x7f290aac6890_0
                        E0x7f290aac7150
                        (<= v0x7f290aac7090_0 v0x7f290aac6e90_0)
                        (>= v0x7f290aac7090_0 v0x7f290aac6e90_0))
                   (and v0x7f290aac65d0_0
                        E0x7f290aac7310
                        v0x7f290aac6750_0
                        (<= v0x7f290aac7090_0 v0x7f290aac5f10_0)
                        (>= v0x7f290aac7090_0 v0x7f290aac5f10_0)))))
      (a!2 (=> v0x7f290aac6fd0_0
               (or (and E0x7f290aac7150 (not E0x7f290aac7310))
                   (and E0x7f290aac7310 (not E0x7f290aac7150)))))
      (a!3 (=> v0x7f290aac7e90_0
               (or (and v0x7f290aac7950_0
                        E0x7f290aac8010
                        (<= v0x7f290aac7f50_0 v0x7f290aac7d50_0)
                        (>= v0x7f290aac7f50_0 v0x7f290aac7d50_0))
                   (and v0x7f290aac6fd0_0
                        E0x7f290aac81d0
                        v0x7f290aac7810_0
                        (<= v0x7f290aac7f50_0 v0x7f290aac5d90_0)
                        (>= v0x7f290aac7f50_0 v0x7f290aac5d90_0)))))
      (a!4 (=> v0x7f290aac7e90_0
               (or (and E0x7f290aac8010 (not E0x7f290aac81d0))
                   (and E0x7f290aac81d0 (not E0x7f290aac8010)))))
      (a!5 (or (and v0x7f290aac9410_0
                    E0x7f290aac9dd0
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac7090_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac7090_0))
                    (<= v0x7f290aac9d10_0 v0x7f290aac97d0_0)
                    (>= v0x7f290aac9d10_0 v0x7f290aac97d0_0))
               (and v0x7f290aac8810_0
                    E0x7f290aaca090
                    (not v0x7f290aac8a90_0)
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac7090_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac7090_0))
                    (and (<= v0x7f290aac9d10_0 v0x7f290aac6010_0)
                         (>= v0x7f290aac9d10_0 v0x7f290aac6010_0)))
               (and v0x7f290aac9910_0
                    E0x7f290aaca310
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac9190_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac9190_0))
                    (and (<= v0x7f290aac9d10_0 v0x7f290aac6010_0)
                         (>= v0x7f290aac9d10_0 v0x7f290aac6010_0)))
               (and v0x7f290aac8bd0_0
                    E0x7f290aaca510
                    (not v0x7f290aac92d0_0)
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac9190_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac9190_0))
                    (<= v0x7f290aac9d10_0 0.0)
                    (>= v0x7f290aac9d10_0 0.0))))
      (a!6 (=> v0x7f290aac9b90_0
               (or (and E0x7f290aac9dd0
                        (not E0x7f290aaca090)
                        (not E0x7f290aaca310)
                        (not E0x7f290aaca510))
                   (and E0x7f290aaca090
                        (not E0x7f290aac9dd0)
                        (not E0x7f290aaca310)
                        (not E0x7f290aaca510))
                   (and E0x7f290aaca310
                        (not E0x7f290aac9dd0)
                        (not E0x7f290aaca090)
                        (not E0x7f290aaca510))
                   (and E0x7f290aaca510
                        (not E0x7f290aac9dd0)
                        (not E0x7f290aaca090)
                        (not E0x7f290aaca310))))))
(let ((a!7 (and (=> v0x7f290aac6890_0
                    (and v0x7f290aac65d0_0
                         E0x7f290aac6950
                         (not v0x7f290aac6750_0)))
                (=> v0x7f290aac6890_0 E0x7f290aac6950)
                a!1
                a!2
                (=> v0x7f290aac7950_0
                    (and v0x7f290aac6fd0_0
                         E0x7f290aac7a10
                         (not v0x7f290aac7810_0)))
                (=> v0x7f290aac7950_0 E0x7f290aac7a10)
                a!3
                a!4
                (=> v0x7f290aac8810_0
                    (and v0x7f290aac7e90_0 E0x7f290aac88d0 v0x7f290aac86d0_0))
                (=> v0x7f290aac8810_0 E0x7f290aac88d0)
                (=> v0x7f290aac8bd0_0
                    (and v0x7f290aac7e90_0
                         E0x7f290aac8c90
                         (not v0x7f290aac86d0_0)))
                (=> v0x7f290aac8bd0_0 E0x7f290aac8c90)
                (=> v0x7f290aac9410_0
                    (and v0x7f290aac8810_0 E0x7f290aac94d0 v0x7f290aac8a90_0))
                (=> v0x7f290aac9410_0 E0x7f290aac94d0)
                (=> v0x7f290aac9910_0
                    (and v0x7f290aac8bd0_0 E0x7f290aac99d0 v0x7f290aac92d0_0))
                (=> v0x7f290aac9910_0 E0x7f290aac99d0)
                (=> v0x7f290aac9b90_0 a!5)
                a!6
                v0x7f290aac9b90_0
                v0x7f290aacad50_0
                (<= v0x7f290aac5fd0_0 v0x7f290aac7f50_0)
                (>= v0x7f290aac5fd0_0 v0x7f290aac7f50_0)
                (<= v0x7f290aac60d0_0 v0x7f290aac9c50_0)
                (>= v0x7f290aac60d0_0 v0x7f290aac9c50_0)
                (<= v0x7f290aac4010_0 v0x7f290aac9d10_0)
                (>= v0x7f290aac4010_0 v0x7f290aac9d10_0)
                (= v0x7f290aac6750_0 (= v0x7f290aac6690_0 0.0))
                (= v0x7f290aac6b90_0 (< v0x7f290aac5f10_0 2.0))
                (= v0x7f290aac6d50_0 (ite v0x7f290aac6b90_0 1.0 0.0))
                (= v0x7f290aac6e90_0 (+ v0x7f290aac6d50_0 v0x7f290aac5f10_0))
                (= v0x7f290aac7810_0 (= v0x7f290aac7750_0 0.0))
                (= v0x7f290aac7c10_0 (= v0x7f290aac5d90_0 0.0))
                (= v0x7f290aac7d50_0 (ite v0x7f290aac7c10_0 1.0 0.0))
                (= v0x7f290aac86d0_0 (= v0x7f290aac6010_0 0.0))
                (= v0x7f290aac8a90_0 (> v0x7f290aac7090_0 1.0))
                (= v0x7f290aac8e90_0 (> v0x7f290aac7090_0 0.0))
                (= v0x7f290aac8fd0_0 (+ v0x7f290aac7090_0 (- 1.0)))
                (= v0x7f290aac9190_0
                   (ite v0x7f290aac8e90_0 v0x7f290aac8fd0_0 v0x7f290aac7090_0))
                (= v0x7f290aac92d0_0 (= v0x7f290aac9190_0 0.0))
                (= v0x7f290aac9690_0 (= v0x7f290aac7f50_0 0.0))
                (= v0x7f290aac97d0_0
                   (ite v0x7f290aac9690_0 1.0 v0x7f290aac6010_0))
                (= v0x7f290aacaad0_0 (not (= v0x7f290aac9c50_0 0.0)))
                (= v0x7f290aacac10_0 (= v0x7f290aac9d10_0 0.0))
                (= v0x7f290aacad50_0 (or v0x7f290aacac10_0 v0x7f290aacaad0_0)))))
  (=> F0x7f290aacb9d0 a!7))))
(assert (=> F0x7f290aacb9d0 F0x7f290aacba90))
(assert (let ((a!1 (=> v0x7f290aac6fd0_0
               (or (and v0x7f290aac6890_0
                        E0x7f290aac7150
                        (<= v0x7f290aac7090_0 v0x7f290aac6e90_0)
                        (>= v0x7f290aac7090_0 v0x7f290aac6e90_0))
                   (and v0x7f290aac65d0_0
                        E0x7f290aac7310
                        v0x7f290aac6750_0
                        (<= v0x7f290aac7090_0 v0x7f290aac5f10_0)
                        (>= v0x7f290aac7090_0 v0x7f290aac5f10_0)))))
      (a!2 (=> v0x7f290aac6fd0_0
               (or (and E0x7f290aac7150 (not E0x7f290aac7310))
                   (and E0x7f290aac7310 (not E0x7f290aac7150)))))
      (a!3 (=> v0x7f290aac7e90_0
               (or (and v0x7f290aac7950_0
                        E0x7f290aac8010
                        (<= v0x7f290aac7f50_0 v0x7f290aac7d50_0)
                        (>= v0x7f290aac7f50_0 v0x7f290aac7d50_0))
                   (and v0x7f290aac6fd0_0
                        E0x7f290aac81d0
                        v0x7f290aac7810_0
                        (<= v0x7f290aac7f50_0 v0x7f290aac5d90_0)
                        (>= v0x7f290aac7f50_0 v0x7f290aac5d90_0)))))
      (a!4 (=> v0x7f290aac7e90_0
               (or (and E0x7f290aac8010 (not E0x7f290aac81d0))
                   (and E0x7f290aac81d0 (not E0x7f290aac8010)))))
      (a!5 (or (and v0x7f290aac9410_0
                    E0x7f290aac9dd0
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac7090_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac7090_0))
                    (<= v0x7f290aac9d10_0 v0x7f290aac97d0_0)
                    (>= v0x7f290aac9d10_0 v0x7f290aac97d0_0))
               (and v0x7f290aac8810_0
                    E0x7f290aaca090
                    (not v0x7f290aac8a90_0)
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac7090_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac7090_0))
                    (and (<= v0x7f290aac9d10_0 v0x7f290aac6010_0)
                         (>= v0x7f290aac9d10_0 v0x7f290aac6010_0)))
               (and v0x7f290aac9910_0
                    E0x7f290aaca310
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac9190_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac9190_0))
                    (and (<= v0x7f290aac9d10_0 v0x7f290aac6010_0)
                         (>= v0x7f290aac9d10_0 v0x7f290aac6010_0)))
               (and v0x7f290aac8bd0_0
                    E0x7f290aaca510
                    (not v0x7f290aac92d0_0)
                    (and (<= v0x7f290aac9c50_0 v0x7f290aac9190_0)
                         (>= v0x7f290aac9c50_0 v0x7f290aac9190_0))
                    (<= v0x7f290aac9d10_0 0.0)
                    (>= v0x7f290aac9d10_0 0.0))))
      (a!6 (=> v0x7f290aac9b90_0
               (or (and E0x7f290aac9dd0
                        (not E0x7f290aaca090)
                        (not E0x7f290aaca310)
                        (not E0x7f290aaca510))
                   (and E0x7f290aaca090
                        (not E0x7f290aac9dd0)
                        (not E0x7f290aaca310)
                        (not E0x7f290aaca510))
                   (and E0x7f290aaca310
                        (not E0x7f290aac9dd0)
                        (not E0x7f290aaca090)
                        (not E0x7f290aaca510))
                   (and E0x7f290aaca510
                        (not E0x7f290aac9dd0)
                        (not E0x7f290aaca090)
                        (not E0x7f290aaca310))))))
(let ((a!7 (and (=> v0x7f290aac6890_0
                    (and v0x7f290aac65d0_0
                         E0x7f290aac6950
                         (not v0x7f290aac6750_0)))
                (=> v0x7f290aac6890_0 E0x7f290aac6950)
                a!1
                a!2
                (=> v0x7f290aac7950_0
                    (and v0x7f290aac6fd0_0
                         E0x7f290aac7a10
                         (not v0x7f290aac7810_0)))
                (=> v0x7f290aac7950_0 E0x7f290aac7a10)
                a!3
                a!4
                (=> v0x7f290aac8810_0
                    (and v0x7f290aac7e90_0 E0x7f290aac88d0 v0x7f290aac86d0_0))
                (=> v0x7f290aac8810_0 E0x7f290aac88d0)
                (=> v0x7f290aac8bd0_0
                    (and v0x7f290aac7e90_0
                         E0x7f290aac8c90
                         (not v0x7f290aac86d0_0)))
                (=> v0x7f290aac8bd0_0 E0x7f290aac8c90)
                (=> v0x7f290aac9410_0
                    (and v0x7f290aac8810_0 E0x7f290aac94d0 v0x7f290aac8a90_0))
                (=> v0x7f290aac9410_0 E0x7f290aac94d0)
                (=> v0x7f290aac9910_0
                    (and v0x7f290aac8bd0_0 E0x7f290aac99d0 v0x7f290aac92d0_0))
                (=> v0x7f290aac9910_0 E0x7f290aac99d0)
                (=> v0x7f290aac9b90_0 a!5)
                a!6
                v0x7f290aac9b90_0
                (not v0x7f290aacad50_0)
                (= v0x7f290aac6750_0 (= v0x7f290aac6690_0 0.0))
                (= v0x7f290aac6b90_0 (< v0x7f290aac5f10_0 2.0))
                (= v0x7f290aac6d50_0 (ite v0x7f290aac6b90_0 1.0 0.0))
                (= v0x7f290aac6e90_0 (+ v0x7f290aac6d50_0 v0x7f290aac5f10_0))
                (= v0x7f290aac7810_0 (= v0x7f290aac7750_0 0.0))
                (= v0x7f290aac7c10_0 (= v0x7f290aac5d90_0 0.0))
                (= v0x7f290aac7d50_0 (ite v0x7f290aac7c10_0 1.0 0.0))
                (= v0x7f290aac86d0_0 (= v0x7f290aac6010_0 0.0))
                (= v0x7f290aac8a90_0 (> v0x7f290aac7090_0 1.0))
                (= v0x7f290aac8e90_0 (> v0x7f290aac7090_0 0.0))
                (= v0x7f290aac8fd0_0 (+ v0x7f290aac7090_0 (- 1.0)))
                (= v0x7f290aac9190_0
                   (ite v0x7f290aac8e90_0 v0x7f290aac8fd0_0 v0x7f290aac7090_0))
                (= v0x7f290aac92d0_0 (= v0x7f290aac9190_0 0.0))
                (= v0x7f290aac9690_0 (= v0x7f290aac7f50_0 0.0))
                (= v0x7f290aac97d0_0
                   (ite v0x7f290aac9690_0 1.0 v0x7f290aac6010_0))
                (= v0x7f290aacaad0_0 (not (= v0x7f290aac9c50_0 0.0)))
                (= v0x7f290aacac10_0 (= v0x7f290aac9d10_0 0.0))
                (= v0x7f290aacad50_0 (or v0x7f290aacac10_0 v0x7f290aacaad0_0)))))
  (=> F0x7f290aacbb50 a!7))))
(assert (=> F0x7f290aacbb50 F0x7f290aacba90))
(assert (=> F0x7f290aacbc90 (or F0x7f290aacb990 F0x7f290aacb9d0)))
(assert (=> F0x7f290aacbc50 F0x7f290aacbb50))
(assert (=> pre!entry!0 (=> F0x7f290aacb8d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f290aacba90 (>= v0x7f290aac6010_0 0.0))))
(assert (let ((a!1 (=> F0x7f290aacba90
               (or (not (<= v0x7f290aac5f10_0 1.0)) (<= v0x7f290aac6010_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (not (<= v0x7f290aac60d0_0 1.0)) (<= v0x7f290aac4010_0 0.0)))))
  (or (and (not post!bb1.i.i!0)
           F0x7f290aacbc90
           (not (>= v0x7f290aac4010_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f290aacbc90 a!1)
      (and (not post!bb1.i.i34.i.i!0) F0x7f290aacbc50 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i34.i.i!0)
