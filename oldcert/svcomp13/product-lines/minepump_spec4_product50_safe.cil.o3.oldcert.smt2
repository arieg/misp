(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fa61f5b7250 () Bool)
(declare-fun v0x7fa61f5b4dd0_0 () Real)
(declare-fun F0x7fa61f5b7110 () Bool)
(declare-fun v0x7fa61f5b4750_0 () Bool)
(declare-fun v0x7fa61f5b62d0_0 () Bool)
(declare-fun v0x7fa61f5b3810_0 () Real)
(declare-fun v0x7fa61f5b3650_0 () Bool)
(declare-fun E0x7fa61f5b5c50 () Bool)
(declare-fun v0x7fa61f5b4f90_0 () Real)
(declare-fun F0x7fa61f5b7210 () Bool)
(declare-fun v0x7fa61f5b4890_0 () Real)
(declare-fun v0x7fa61f5b5610_0 () Real)
(declare-fun v0x7fa61f5b50d0_0 () Bool)
(declare-fun E0x7fa61f5b52d0 () Bool)
(declare-fun v0x7fa61f5b2a90_0 () Real)
(declare-fun E0x7fa61f5b4a90 () Bool)
(declare-fun v0x7fa61f5b5550_0 () Real)
(declare-fun post!bb1.i.i26.i.i!0 () Bool)
(declare-fun v0x7fa61f5b4c90_0 () Bool)
(declare-fun v0x7fa61f5b44d0_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7fa61f5b3c10 () Bool)
(declare-fun E0x7fa61f5b56d0 () Bool)
(declare-fun v0x7fa61f5b6190_0 () Bool)
(declare-fun v0x7fa61f5b3950_0 () Real)
(declare-fun v0x7fa61f5b3a90_0 () Bool)
(declare-fun v0x7fa61f5b3210_0 () Bool)
(declare-fun v0x7fa61f5b3150_0 () Real)
(declare-fun E0x7fa61f5b3410 () Bool)
(declare-fun E0x7fa61f5b3dd0 () Bool)
(declare-fun v0x7fa61f5b3090_0 () Bool)
(declare-fun E0x7fa61f5b5990 () Bool)
(declare-fun F0x7fa61f5b7050 () Bool)
(declare-fun v0x7fa61f5b4390_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7fa61f5b3350_0 () Bool)
(declare-fun F0x7fa61f5b6f90 () Bool)
(declare-fun v0x7fa61f5b5210_0 () Bool)
(declare-fun v0x7fa61f5b6410_0 () Bool)
(declare-fun v0x7fa61f5b49d0_0 () Bool)
(declare-fun F0x7fa61f5b6e90 () Bool)
(declare-fun v0x7fa61f5b1010_0 () Real)
(declare-fun v0x7fa61f5b5490_0 () Bool)
(declare-fun v0x7fa61f5b2c10_0 () Real)
(declare-fun v0x7fa61f5b3b50_0 () Real)
(declare-fun E0x7fa61f5b4590 () Bool)
(declare-fun v0x7fa61f5b2cd0_0 () Real)
(declare-fun v0x7fa61f5b1110_0 () Bool)
(declare-fun F0x7fa61f5b6f50 () Bool)

(assert (=> F0x7fa61f5b6f50
    (and v0x7fa61f5b1110_0
         (<= v0x7fa61f5b2cd0_0 0.0)
         (>= v0x7fa61f5b2cd0_0 0.0)
         (<= v0x7fa61f5b1010_0 1.0)
         (>= v0x7fa61f5b1010_0 1.0))))
(assert (=> F0x7fa61f5b6f50 F0x7fa61f5b6e90))
(assert (let ((a!1 (=> v0x7fa61f5b3a90_0
               (or (and v0x7fa61f5b3350_0
                        E0x7fa61f5b3c10
                        (<= v0x7fa61f5b3b50_0 v0x7fa61f5b3950_0)
                        (>= v0x7fa61f5b3b50_0 v0x7fa61f5b3950_0))
                   (and v0x7fa61f5b3090_0
                        E0x7fa61f5b3dd0
                        v0x7fa61f5b3210_0
                        (<= v0x7fa61f5b3b50_0 v0x7fa61f5b2c10_0)
                        (>= v0x7fa61f5b3b50_0 v0x7fa61f5b2c10_0)))))
      (a!2 (=> v0x7fa61f5b3a90_0
               (or (and E0x7fa61f5b3c10 (not E0x7fa61f5b3dd0))
                   (and E0x7fa61f5b3dd0 (not E0x7fa61f5b3c10)))))
      (a!3 (or (and v0x7fa61f5b44d0_0
                    E0x7fa61f5b56d0
                    (<= v0x7fa61f5b5550_0 v0x7fa61f5b3b50_0)
                    (>= v0x7fa61f5b5550_0 v0x7fa61f5b3b50_0)
                    (<= v0x7fa61f5b5610_0 v0x7fa61f5b4890_0)
                    (>= v0x7fa61f5b5610_0 v0x7fa61f5b4890_0))
               (and v0x7fa61f5b5210_0
                    E0x7fa61f5b5990
                    (and (<= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0)
                         (>= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0))
                    (<= v0x7fa61f5b5610_0 v0x7fa61f5b2a90_0)
                    (>= v0x7fa61f5b5610_0 v0x7fa61f5b2a90_0))
               (and v0x7fa61f5b49d0_0
                    E0x7fa61f5b5c50
                    (not v0x7fa61f5b50d0_0)
                    (and (<= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0)
                         (>= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0))
                    (<= v0x7fa61f5b5610_0 0.0)
                    (>= v0x7fa61f5b5610_0 0.0))))
      (a!4 (=> v0x7fa61f5b5490_0
               (or (and E0x7fa61f5b56d0
                        (not E0x7fa61f5b5990)
                        (not E0x7fa61f5b5c50))
                   (and E0x7fa61f5b5990
                        (not E0x7fa61f5b56d0)
                        (not E0x7fa61f5b5c50))
                   (and E0x7fa61f5b5c50
                        (not E0x7fa61f5b56d0)
                        (not E0x7fa61f5b5990))))))
(let ((a!5 (and (=> v0x7fa61f5b3350_0
                    (and v0x7fa61f5b3090_0
                         E0x7fa61f5b3410
                         (not v0x7fa61f5b3210_0)))
                (=> v0x7fa61f5b3350_0 E0x7fa61f5b3410)
                a!1
                a!2
                (=> v0x7fa61f5b44d0_0
                    (and v0x7fa61f5b3a90_0 E0x7fa61f5b4590 v0x7fa61f5b4390_0))
                (=> v0x7fa61f5b44d0_0 E0x7fa61f5b4590)
                (=> v0x7fa61f5b49d0_0
                    (and v0x7fa61f5b3a90_0
                         E0x7fa61f5b4a90
                         (not v0x7fa61f5b4390_0)))
                (=> v0x7fa61f5b49d0_0 E0x7fa61f5b4a90)
                (=> v0x7fa61f5b5210_0
                    (and v0x7fa61f5b49d0_0 E0x7fa61f5b52d0 v0x7fa61f5b50d0_0))
                (=> v0x7fa61f5b5210_0 E0x7fa61f5b52d0)
                (=> v0x7fa61f5b5490_0 a!3)
                a!4
                v0x7fa61f5b5490_0
                v0x7fa61f5b6410_0
                (<= v0x7fa61f5b2cd0_0 v0x7fa61f5b5610_0)
                (>= v0x7fa61f5b2cd0_0 v0x7fa61f5b5610_0)
                (<= v0x7fa61f5b1010_0 v0x7fa61f5b5550_0)
                (>= v0x7fa61f5b1010_0 v0x7fa61f5b5550_0)
                (= v0x7fa61f5b3210_0 (= v0x7fa61f5b3150_0 0.0))
                (= v0x7fa61f5b3650_0 (< v0x7fa61f5b2c10_0 2.0))
                (= v0x7fa61f5b3810_0 (ite v0x7fa61f5b3650_0 1.0 0.0))
                (= v0x7fa61f5b3950_0 (+ v0x7fa61f5b3810_0 v0x7fa61f5b2c10_0))
                (= v0x7fa61f5b4390_0 (= v0x7fa61f5b2a90_0 0.0))
                (= v0x7fa61f5b4750_0 (> v0x7fa61f5b3b50_0 1.0))
                (= v0x7fa61f5b4890_0
                   (ite v0x7fa61f5b4750_0 1.0 v0x7fa61f5b2a90_0))
                (= v0x7fa61f5b4c90_0 (> v0x7fa61f5b3b50_0 0.0))
                (= v0x7fa61f5b4dd0_0 (+ v0x7fa61f5b3b50_0 (- 1.0)))
                (= v0x7fa61f5b4f90_0
                   (ite v0x7fa61f5b4c90_0 v0x7fa61f5b4dd0_0 v0x7fa61f5b3b50_0))
                (= v0x7fa61f5b50d0_0 (= v0x7fa61f5b4f90_0 0.0))
                (= v0x7fa61f5b6190_0 (not (= v0x7fa61f5b5550_0 0.0)))
                (= v0x7fa61f5b62d0_0 (= v0x7fa61f5b5610_0 0.0))
                (= v0x7fa61f5b6410_0 (or v0x7fa61f5b62d0_0 v0x7fa61f5b6190_0)))))
  (=> F0x7fa61f5b6f90 a!5))))
(assert (=> F0x7fa61f5b6f90 F0x7fa61f5b7050))
(assert (let ((a!1 (=> v0x7fa61f5b3a90_0
               (or (and v0x7fa61f5b3350_0
                        E0x7fa61f5b3c10
                        (<= v0x7fa61f5b3b50_0 v0x7fa61f5b3950_0)
                        (>= v0x7fa61f5b3b50_0 v0x7fa61f5b3950_0))
                   (and v0x7fa61f5b3090_0
                        E0x7fa61f5b3dd0
                        v0x7fa61f5b3210_0
                        (<= v0x7fa61f5b3b50_0 v0x7fa61f5b2c10_0)
                        (>= v0x7fa61f5b3b50_0 v0x7fa61f5b2c10_0)))))
      (a!2 (=> v0x7fa61f5b3a90_0
               (or (and E0x7fa61f5b3c10 (not E0x7fa61f5b3dd0))
                   (and E0x7fa61f5b3dd0 (not E0x7fa61f5b3c10)))))
      (a!3 (or (and v0x7fa61f5b44d0_0
                    E0x7fa61f5b56d0
                    (<= v0x7fa61f5b5550_0 v0x7fa61f5b3b50_0)
                    (>= v0x7fa61f5b5550_0 v0x7fa61f5b3b50_0)
                    (<= v0x7fa61f5b5610_0 v0x7fa61f5b4890_0)
                    (>= v0x7fa61f5b5610_0 v0x7fa61f5b4890_0))
               (and v0x7fa61f5b5210_0
                    E0x7fa61f5b5990
                    (and (<= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0)
                         (>= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0))
                    (<= v0x7fa61f5b5610_0 v0x7fa61f5b2a90_0)
                    (>= v0x7fa61f5b5610_0 v0x7fa61f5b2a90_0))
               (and v0x7fa61f5b49d0_0
                    E0x7fa61f5b5c50
                    (not v0x7fa61f5b50d0_0)
                    (and (<= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0)
                         (>= v0x7fa61f5b5550_0 v0x7fa61f5b4f90_0))
                    (<= v0x7fa61f5b5610_0 0.0)
                    (>= v0x7fa61f5b5610_0 0.0))))
      (a!4 (=> v0x7fa61f5b5490_0
               (or (and E0x7fa61f5b56d0
                        (not E0x7fa61f5b5990)
                        (not E0x7fa61f5b5c50))
                   (and E0x7fa61f5b5990
                        (not E0x7fa61f5b56d0)
                        (not E0x7fa61f5b5c50))
                   (and E0x7fa61f5b5c50
                        (not E0x7fa61f5b56d0)
                        (not E0x7fa61f5b5990))))))
(let ((a!5 (and (=> v0x7fa61f5b3350_0
                    (and v0x7fa61f5b3090_0
                         E0x7fa61f5b3410
                         (not v0x7fa61f5b3210_0)))
                (=> v0x7fa61f5b3350_0 E0x7fa61f5b3410)
                a!1
                a!2
                (=> v0x7fa61f5b44d0_0
                    (and v0x7fa61f5b3a90_0 E0x7fa61f5b4590 v0x7fa61f5b4390_0))
                (=> v0x7fa61f5b44d0_0 E0x7fa61f5b4590)
                (=> v0x7fa61f5b49d0_0
                    (and v0x7fa61f5b3a90_0
                         E0x7fa61f5b4a90
                         (not v0x7fa61f5b4390_0)))
                (=> v0x7fa61f5b49d0_0 E0x7fa61f5b4a90)
                (=> v0x7fa61f5b5210_0
                    (and v0x7fa61f5b49d0_0 E0x7fa61f5b52d0 v0x7fa61f5b50d0_0))
                (=> v0x7fa61f5b5210_0 E0x7fa61f5b52d0)
                (=> v0x7fa61f5b5490_0 a!3)
                a!4
                v0x7fa61f5b5490_0
                (not v0x7fa61f5b6410_0)
                (= v0x7fa61f5b3210_0 (= v0x7fa61f5b3150_0 0.0))
                (= v0x7fa61f5b3650_0 (< v0x7fa61f5b2c10_0 2.0))
                (= v0x7fa61f5b3810_0 (ite v0x7fa61f5b3650_0 1.0 0.0))
                (= v0x7fa61f5b3950_0 (+ v0x7fa61f5b3810_0 v0x7fa61f5b2c10_0))
                (= v0x7fa61f5b4390_0 (= v0x7fa61f5b2a90_0 0.0))
                (= v0x7fa61f5b4750_0 (> v0x7fa61f5b3b50_0 1.0))
                (= v0x7fa61f5b4890_0
                   (ite v0x7fa61f5b4750_0 1.0 v0x7fa61f5b2a90_0))
                (= v0x7fa61f5b4c90_0 (> v0x7fa61f5b3b50_0 0.0))
                (= v0x7fa61f5b4dd0_0 (+ v0x7fa61f5b3b50_0 (- 1.0)))
                (= v0x7fa61f5b4f90_0
                   (ite v0x7fa61f5b4c90_0 v0x7fa61f5b4dd0_0 v0x7fa61f5b3b50_0))
                (= v0x7fa61f5b50d0_0 (= v0x7fa61f5b4f90_0 0.0))
                (= v0x7fa61f5b6190_0 (not (= v0x7fa61f5b5550_0 0.0)))
                (= v0x7fa61f5b62d0_0 (= v0x7fa61f5b5610_0 0.0))
                (= v0x7fa61f5b6410_0 (or v0x7fa61f5b62d0_0 v0x7fa61f5b6190_0)))))
  (=> F0x7fa61f5b7110 a!5))))
(assert (=> F0x7fa61f5b7110 F0x7fa61f5b7050))
(assert (=> F0x7fa61f5b7250 (or F0x7fa61f5b6f50 F0x7fa61f5b6f90)))
(assert (=> F0x7fa61f5b7210 F0x7fa61f5b7110))
(assert (=> pre!entry!0 (=> F0x7fa61f5b6e90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fa61f5b7050 (>= v0x7fa61f5b2a90_0 0.0))))
(assert (let ((a!1 (=> F0x7fa61f5b7050
               (or (<= v0x7fa61f5b2a90_0 0.0) (not (<= v0x7fa61f5b2c10_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7fa61f5b2cd0_0 0.0) (not (<= v0x7fa61f5b1010_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7fa61f5b7250
           (not (>= v0x7fa61f5b2cd0_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7fa61f5b7250 a!1)
      (and (not post!bb1.i.i26.i.i!0) F0x7fa61f5b7210 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i26.i.i!0)
