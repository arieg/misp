(declare-fun post!bb2.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f503e334f10 () Bool)
(declare-fun F0x7f503e334f50 () Bool)
(declare-fun F0x7f503e334d50 () Bool)
(declare-fun v0x7f503e3318d0_0 () Bool)
(declare-fun v0x7f503e32f790_0 () Real)
(declare-fun v0x7f503e331e50_0 () Bool)
(declare-fun v0x7f503e32f5d0_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f503e330650_0 () Bool)
(declare-fun v0x7f503e32f0d0_0 () Real)
(declare-fun v0x7f503e333d90_0 () Bool)
(declare-fun v0x7f503e333990_0 () Bool)
(declare-fun v0x7f503e32e910_0 () Real)
(declare-fun E0x7f503e333b90 () Bool)
(declare-fun E0x7f503e333190 () Bool)
(declare-fun E0x7f503e332f90 () Bool)
(declare-fun v0x7f503e332450_0 () Real)
(declare-fun v0x7f503e332990_0 () Real)
(declare-fun v0x7f503e3328d0_0 () Real)
(declare-fun E0x7f503e332a50 () Bool)
(declare-fun v0x7f503e331f90_0 () Bool)
(declare-fun E0x7f503e332650 () Bool)
(declare-fun v0x7f503e332590_0 () Bool)
(declare-fun v0x7f503e331bd0_0 () Real)
(declare-fun v0x7f503e331a10_0 () Real)
(declare-fun v0x7f503e333ad0_0 () Bool)
(declare-fun v0x7f503e3314d0_0 () Bool)
(declare-fun E0x7f503e332190 () Bool)
(declare-fun v0x7f503e3320d0_0 () Bool)
(declare-fun v0x7f503e333850_0 () Bool)
(declare-fun E0x7f503e3316d0 () Bool)
(declare-fun v0x7f503e331610_0 () Bool)
(declare-fun v0x7f503e331110_0 () Bool)
(declare-fun E0x7f503e331310 () Bool)
(declare-fun v0x7f503e331250_0 () Bool)
(declare-fun v0x7f503e3308d0_0 () Bool)
(declare-fun v0x7f503e330390_0 () Bool)
(declare-fun v0x7f503e32e590_0 () Real)
(declare-fun v0x7f503e32e810_0 () Real)
(declare-fun E0x7f503e32fd50 () Bool)
(declare-fun v0x7f503e32f8d0_0 () Real)
(declare-fun v0x7f503e332350_0 () Bool)
(declare-fun v0x7f503e330250_0 () Bool)
(declare-fun v0x7f503e32fad0_0 () Real)
(declare-fun v0x7f503e32e710_0 () Real)
(declare-fun v0x7f503e331d10_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f503e330450 () Bool)
(declare-fun E0x7f503e32f390 () Bool)
(declare-fun v0x7f503e32f010_0 () Bool)
(declare-fun E0x7f503e32fb90 () Bool)
(declare-fun v0x7f503e333750_0 () Bool)
(declare-fun F0x7f503e334c90 () Bool)
(declare-fun v0x7f503e330190_0 () Real)
(declare-fun v0x7f503e330990_0 () Real)
(declare-fun E0x7f503e330c10 () Bool)
(declare-fun F0x7f503e334bd0 () Bool)
(declare-fun E0x7f503e330a50 () Bool)
(declare-fun v0x7f503e32f2d0_0 () Bool)
(declare-fun E0x7f503e332d10 () Bool)
(declare-fun v0x7f503e32b010_0 () Real)
(declare-fun v0x7f503e330790_0 () Real)
(declare-fun v0x7f503e32e9d0_0 () Real)
(declare-fun v0x7f503e332810_0 () Bool)
(declare-fun v0x7f503e32e8d0_0 () Real)
(declare-fun v0x7f503e32e7d0_0 () Real)
(declare-fun v0x7f503e32fa10_0 () Bool)
(declare-fun F0x7f503e334e10 () Bool)
(declare-fun v0x7f503e32f190_0 () Bool)
(declare-fun v0x7f503e32b110_0 () Bool)
(declare-fun F0x7f503e334b10 () Bool)

(assert (=> F0x7f503e334b10
    (and v0x7f503e32b110_0
         (<= v0x7f503e32e7d0_0 0.0)
         (>= v0x7f503e32e7d0_0 0.0)
         (<= v0x7f503e32e8d0_0 0.0)
         (>= v0x7f503e32e8d0_0 0.0)
         (<= v0x7f503e32e9d0_0 1.0)
         (>= v0x7f503e32e9d0_0 1.0)
         (<= v0x7f503e32b010_0 0.0)
         (>= v0x7f503e32b010_0 0.0))))
(assert (=> F0x7f503e334b10 F0x7f503e334bd0))
(assert (let ((a!1 (=> v0x7f503e32fa10_0
               (or (and v0x7f503e32f2d0_0
                        E0x7f503e32fb90
                        (<= v0x7f503e32fad0_0 v0x7f503e32f8d0_0)
                        (>= v0x7f503e32fad0_0 v0x7f503e32f8d0_0))
                   (and v0x7f503e32f010_0
                        E0x7f503e32fd50
                        v0x7f503e32f190_0
                        (<= v0x7f503e32fad0_0 v0x7f503e32e810_0)
                        (>= v0x7f503e32fad0_0 v0x7f503e32e810_0)))))
      (a!2 (=> v0x7f503e32fa10_0
               (or (and E0x7f503e32fb90 (not E0x7f503e32fd50))
                   (and E0x7f503e32fd50 (not E0x7f503e32fb90)))))
      (a!3 (=> v0x7f503e3308d0_0
               (or (and v0x7f503e330390_0
                        E0x7f503e330a50
                        (<= v0x7f503e330990_0 v0x7f503e330790_0)
                        (>= v0x7f503e330990_0 v0x7f503e330790_0))
                   (and v0x7f503e32fa10_0
                        E0x7f503e330c10
                        v0x7f503e330250_0
                        (<= v0x7f503e330990_0 v0x7f503e32e710_0)
                        (>= v0x7f503e330990_0 v0x7f503e32e710_0)))))
      (a!4 (=> v0x7f503e3308d0_0
               (or (and E0x7f503e330a50 (not E0x7f503e330c10))
                   (and E0x7f503e330c10 (not E0x7f503e330a50)))))
      (a!5 (or (and v0x7f503e3320d0_0
                    E0x7f503e332a50
                    (and (<= v0x7f503e3328d0_0 v0x7f503e32fad0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e32fad0_0))
                    (<= v0x7f503e332990_0 v0x7f503e332450_0)
                    (>= v0x7f503e332990_0 v0x7f503e332450_0))
               (and v0x7f503e331250_0
                    E0x7f503e332d10
                    (not v0x7f503e3314d0_0)
                    (and (<= v0x7f503e3328d0_0 v0x7f503e32fad0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e32fad0_0))
                    (and (<= v0x7f503e332990_0 v0x7f503e32e590_0)
                         (>= v0x7f503e332990_0 v0x7f503e32e590_0)))
               (and v0x7f503e332590_0
                    E0x7f503e332f90
                    (and (<= v0x7f503e3328d0_0 v0x7f503e331bd0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e331bd0_0))
                    (and (<= v0x7f503e332990_0 v0x7f503e32e590_0)
                         (>= v0x7f503e332990_0 v0x7f503e32e590_0)))
               (and v0x7f503e331610_0
                    E0x7f503e333190
                    (not v0x7f503e331f90_0)
                    (and (<= v0x7f503e3328d0_0 v0x7f503e331bd0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e331bd0_0))
                    (<= v0x7f503e332990_0 0.0)
                    (>= v0x7f503e332990_0 0.0))))
      (a!6 (=> v0x7f503e332810_0
               (or (and E0x7f503e332a50
                        (not E0x7f503e332d10)
                        (not E0x7f503e332f90)
                        (not E0x7f503e333190))
                   (and E0x7f503e332d10
                        (not E0x7f503e332a50)
                        (not E0x7f503e332f90)
                        (not E0x7f503e333190))
                   (and E0x7f503e332f90
                        (not E0x7f503e332a50)
                        (not E0x7f503e332d10)
                        (not E0x7f503e333190))
                   (and E0x7f503e333190
                        (not E0x7f503e332a50)
                        (not E0x7f503e332d10)
                        (not E0x7f503e332f90)))))
      (a!7 (or (and v0x7f503e333ad0_0
                    v0x7f503e333d90_0
                    (and (<= v0x7f503e32e7d0_0 v0x7f503e332990_0)
                         (>= v0x7f503e32e7d0_0 v0x7f503e332990_0))
                    (and (<= v0x7f503e32e8d0_0 v0x7f503e330990_0)
                         (>= v0x7f503e32e8d0_0 v0x7f503e330990_0))
                    (and (<= v0x7f503e32e9d0_0 v0x7f503e3328d0_0)
                         (>= v0x7f503e32e9d0_0 v0x7f503e3328d0_0))
                    (<= v0x7f503e32b010_0 1.0)
                    (>= v0x7f503e32b010_0 1.0))
               (and v0x7f503e332810_0
                    v0x7f503e333990_0
                    (and (<= v0x7f503e32e7d0_0 v0x7f503e332990_0)
                         (>= v0x7f503e32e7d0_0 v0x7f503e332990_0))
                    (and (<= v0x7f503e32e8d0_0 v0x7f503e330990_0)
                         (>= v0x7f503e32e8d0_0 v0x7f503e330990_0))
                    (and (<= v0x7f503e32e9d0_0 v0x7f503e3328d0_0)
                         (>= v0x7f503e32e9d0_0 v0x7f503e3328d0_0))
                    (<= v0x7f503e32b010_0 0.0)
                    (>= v0x7f503e32b010_0 0.0)))))
(let ((a!8 (and (=> v0x7f503e32f2d0_0
                    (and v0x7f503e32f010_0
                         E0x7f503e32f390
                         (not v0x7f503e32f190_0)))
                (=> v0x7f503e32f2d0_0 E0x7f503e32f390)
                a!1
                a!2
                (=> v0x7f503e330390_0
                    (and v0x7f503e32fa10_0
                         E0x7f503e330450
                         (not v0x7f503e330250_0)))
                (=> v0x7f503e330390_0 E0x7f503e330450)
                a!3
                a!4
                (=> v0x7f503e331250_0
                    (and v0x7f503e3308d0_0 E0x7f503e331310 v0x7f503e331110_0))
                (=> v0x7f503e331250_0 E0x7f503e331310)
                (=> v0x7f503e331610_0
                    (and v0x7f503e3308d0_0
                         E0x7f503e3316d0
                         (not v0x7f503e331110_0)))
                (=> v0x7f503e331610_0 E0x7f503e3316d0)
                (=> v0x7f503e3320d0_0
                    (and v0x7f503e331250_0 E0x7f503e332190 v0x7f503e3314d0_0))
                (=> v0x7f503e3320d0_0 E0x7f503e332190)
                (=> v0x7f503e332590_0
                    (and v0x7f503e331610_0 E0x7f503e332650 v0x7f503e331f90_0))
                (=> v0x7f503e332590_0 E0x7f503e332650)
                (=> v0x7f503e332810_0 a!5)
                a!6
                (=> v0x7f503e333ad0_0
                    (and v0x7f503e332810_0
                         E0x7f503e333b90
                         (not v0x7f503e333990_0)))
                (=> v0x7f503e333ad0_0 E0x7f503e333b90)
                a!7
                (= v0x7f503e32f190_0 (= v0x7f503e32f0d0_0 0.0))
                (= v0x7f503e32f5d0_0 (< v0x7f503e32e810_0 2.0))
                (= v0x7f503e32f790_0 (ite v0x7f503e32f5d0_0 1.0 0.0))
                (= v0x7f503e32f8d0_0 (+ v0x7f503e32f790_0 v0x7f503e32e810_0))
                (= v0x7f503e330250_0 (= v0x7f503e330190_0 0.0))
                (= v0x7f503e330650_0 (= v0x7f503e32e710_0 0.0))
                (= v0x7f503e330790_0 (ite v0x7f503e330650_0 1.0 0.0))
                (= v0x7f503e331110_0 (= v0x7f503e32e590_0 0.0))
                (= v0x7f503e3314d0_0 (> v0x7f503e32fad0_0 1.0))
                (= v0x7f503e3318d0_0 (> v0x7f503e32fad0_0 0.0))
                (= v0x7f503e331a10_0 (+ v0x7f503e32fad0_0 (- 1.0)))
                (= v0x7f503e331bd0_0
                   (ite v0x7f503e3318d0_0 v0x7f503e331a10_0 v0x7f503e32fad0_0))
                (= v0x7f503e331d10_0 (= v0x7f503e330990_0 0.0))
                (= v0x7f503e331e50_0 (= v0x7f503e331bd0_0 0.0))
                (= v0x7f503e331f90_0 (and v0x7f503e331d10_0 v0x7f503e331e50_0))
                (= v0x7f503e332350_0 (= v0x7f503e330990_0 0.0))
                (= v0x7f503e332450_0
                   (ite v0x7f503e332350_0 1.0 v0x7f503e32e590_0))
                (= v0x7f503e333750_0 (= v0x7f503e330990_0 0.0))
                (= v0x7f503e333850_0 (= v0x7f503e332990_0 0.0))
                (= v0x7f503e333990_0 (or v0x7f503e333850_0 v0x7f503e333750_0))
                (= v0x7f503e333d90_0 (= v0x7f503e32e910_0 0.0)))))
  (=> F0x7f503e334c90 a!8))))
(assert (=> F0x7f503e334c90 F0x7f503e334d50))
(assert (let ((a!1 (=> v0x7f503e32fa10_0
               (or (and v0x7f503e32f2d0_0
                        E0x7f503e32fb90
                        (<= v0x7f503e32fad0_0 v0x7f503e32f8d0_0)
                        (>= v0x7f503e32fad0_0 v0x7f503e32f8d0_0))
                   (and v0x7f503e32f010_0
                        E0x7f503e32fd50
                        v0x7f503e32f190_0
                        (<= v0x7f503e32fad0_0 v0x7f503e32e810_0)
                        (>= v0x7f503e32fad0_0 v0x7f503e32e810_0)))))
      (a!2 (=> v0x7f503e32fa10_0
               (or (and E0x7f503e32fb90 (not E0x7f503e32fd50))
                   (and E0x7f503e32fd50 (not E0x7f503e32fb90)))))
      (a!3 (=> v0x7f503e3308d0_0
               (or (and v0x7f503e330390_0
                        E0x7f503e330a50
                        (<= v0x7f503e330990_0 v0x7f503e330790_0)
                        (>= v0x7f503e330990_0 v0x7f503e330790_0))
                   (and v0x7f503e32fa10_0
                        E0x7f503e330c10
                        v0x7f503e330250_0
                        (<= v0x7f503e330990_0 v0x7f503e32e710_0)
                        (>= v0x7f503e330990_0 v0x7f503e32e710_0)))))
      (a!4 (=> v0x7f503e3308d0_0
               (or (and E0x7f503e330a50 (not E0x7f503e330c10))
                   (and E0x7f503e330c10 (not E0x7f503e330a50)))))
      (a!5 (or (and v0x7f503e3320d0_0
                    E0x7f503e332a50
                    (and (<= v0x7f503e3328d0_0 v0x7f503e32fad0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e32fad0_0))
                    (<= v0x7f503e332990_0 v0x7f503e332450_0)
                    (>= v0x7f503e332990_0 v0x7f503e332450_0))
               (and v0x7f503e331250_0
                    E0x7f503e332d10
                    (not v0x7f503e3314d0_0)
                    (and (<= v0x7f503e3328d0_0 v0x7f503e32fad0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e32fad0_0))
                    (and (<= v0x7f503e332990_0 v0x7f503e32e590_0)
                         (>= v0x7f503e332990_0 v0x7f503e32e590_0)))
               (and v0x7f503e332590_0
                    E0x7f503e332f90
                    (and (<= v0x7f503e3328d0_0 v0x7f503e331bd0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e331bd0_0))
                    (and (<= v0x7f503e332990_0 v0x7f503e32e590_0)
                         (>= v0x7f503e332990_0 v0x7f503e32e590_0)))
               (and v0x7f503e331610_0
                    E0x7f503e333190
                    (not v0x7f503e331f90_0)
                    (and (<= v0x7f503e3328d0_0 v0x7f503e331bd0_0)
                         (>= v0x7f503e3328d0_0 v0x7f503e331bd0_0))
                    (<= v0x7f503e332990_0 0.0)
                    (>= v0x7f503e332990_0 0.0))))
      (a!6 (=> v0x7f503e332810_0
               (or (and E0x7f503e332a50
                        (not E0x7f503e332d10)
                        (not E0x7f503e332f90)
                        (not E0x7f503e333190))
                   (and E0x7f503e332d10
                        (not E0x7f503e332a50)
                        (not E0x7f503e332f90)
                        (not E0x7f503e333190))
                   (and E0x7f503e332f90
                        (not E0x7f503e332a50)
                        (not E0x7f503e332d10)
                        (not E0x7f503e333190))
                   (and E0x7f503e333190
                        (not E0x7f503e332a50)
                        (not E0x7f503e332d10)
                        (not E0x7f503e332f90))))))
(let ((a!7 (and (=> v0x7f503e32f2d0_0
                    (and v0x7f503e32f010_0
                         E0x7f503e32f390
                         (not v0x7f503e32f190_0)))
                (=> v0x7f503e32f2d0_0 E0x7f503e32f390)
                a!1
                a!2
                (=> v0x7f503e330390_0
                    (and v0x7f503e32fa10_0
                         E0x7f503e330450
                         (not v0x7f503e330250_0)))
                (=> v0x7f503e330390_0 E0x7f503e330450)
                a!3
                a!4
                (=> v0x7f503e331250_0
                    (and v0x7f503e3308d0_0 E0x7f503e331310 v0x7f503e331110_0))
                (=> v0x7f503e331250_0 E0x7f503e331310)
                (=> v0x7f503e331610_0
                    (and v0x7f503e3308d0_0
                         E0x7f503e3316d0
                         (not v0x7f503e331110_0)))
                (=> v0x7f503e331610_0 E0x7f503e3316d0)
                (=> v0x7f503e3320d0_0
                    (and v0x7f503e331250_0 E0x7f503e332190 v0x7f503e3314d0_0))
                (=> v0x7f503e3320d0_0 E0x7f503e332190)
                (=> v0x7f503e332590_0
                    (and v0x7f503e331610_0 E0x7f503e332650 v0x7f503e331f90_0))
                (=> v0x7f503e332590_0 E0x7f503e332650)
                (=> v0x7f503e332810_0 a!5)
                a!6
                (=> v0x7f503e333ad0_0
                    (and v0x7f503e332810_0
                         E0x7f503e333b90
                         (not v0x7f503e333990_0)))
                (=> v0x7f503e333ad0_0 E0x7f503e333b90)
                v0x7f503e333ad0_0
                (not v0x7f503e333d90_0)
                (= v0x7f503e32f190_0 (= v0x7f503e32f0d0_0 0.0))
                (= v0x7f503e32f5d0_0 (< v0x7f503e32e810_0 2.0))
                (= v0x7f503e32f790_0 (ite v0x7f503e32f5d0_0 1.0 0.0))
                (= v0x7f503e32f8d0_0 (+ v0x7f503e32f790_0 v0x7f503e32e810_0))
                (= v0x7f503e330250_0 (= v0x7f503e330190_0 0.0))
                (= v0x7f503e330650_0 (= v0x7f503e32e710_0 0.0))
                (= v0x7f503e330790_0 (ite v0x7f503e330650_0 1.0 0.0))
                (= v0x7f503e331110_0 (= v0x7f503e32e590_0 0.0))
                (= v0x7f503e3314d0_0 (> v0x7f503e32fad0_0 1.0))
                (= v0x7f503e3318d0_0 (> v0x7f503e32fad0_0 0.0))
                (= v0x7f503e331a10_0 (+ v0x7f503e32fad0_0 (- 1.0)))
                (= v0x7f503e331bd0_0
                   (ite v0x7f503e3318d0_0 v0x7f503e331a10_0 v0x7f503e32fad0_0))
                (= v0x7f503e331d10_0 (= v0x7f503e330990_0 0.0))
                (= v0x7f503e331e50_0 (= v0x7f503e331bd0_0 0.0))
                (= v0x7f503e331f90_0 (and v0x7f503e331d10_0 v0x7f503e331e50_0))
                (= v0x7f503e332350_0 (= v0x7f503e330990_0 0.0))
                (= v0x7f503e332450_0
                   (ite v0x7f503e332350_0 1.0 v0x7f503e32e590_0))
                (= v0x7f503e333750_0 (= v0x7f503e330990_0 0.0))
                (= v0x7f503e333850_0 (= v0x7f503e332990_0 0.0))
                (= v0x7f503e333990_0 (or v0x7f503e333850_0 v0x7f503e333750_0))
                (= v0x7f503e333d90_0 (= v0x7f503e32e910_0 0.0)))))
  (=> F0x7f503e334e10 a!7))))
(assert (=> F0x7f503e334e10 F0x7f503e334d50))
(assert (=> F0x7f503e334f50 (or F0x7f503e334b10 F0x7f503e334c90)))
(assert (=> F0x7f503e334f10 F0x7f503e334e10))
(assert (=> pre!entry!0 (=> F0x7f503e334bd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f503e334d50 true)))
(assert (or (and (not post!bb1.i.i!0) F0x7f503e334f50 false)
    (and (not post!bb2.i.i43.i.i!0) F0x7f503e334f10 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0)
;(post-assumptions: post!bb1.i.i!0 post!bb2.i.i43.i.i!0)
