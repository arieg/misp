(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f66ed94eed0 () Bool)
(declare-fun F0x7f66ed94eb10 () Bool)
(declare-fun v0x7f66ed94dc50_0 () Bool)
(declare-fun v0x7f66ed94c050_0 () Bool)
(declare-fun v0x7f66ed94a690_0 () Bool)
(declare-fun v0x7f66ed949b50_0 () Real)
(declare-fun v0x7f66ed948cd0_0 () Bool)
(declare-fun v0x7f66ed947e10_0 () Real)
(declare-fun v0x7f66ed948810_0 () Real)
(declare-fun v0x7f66ed947c50_0 () Bool)
(declare-fun v0x7f66ed947750_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f66ed94de90_0 () Bool)
(declare-fun E0x7f66ed94d010 () Bool)
(declare-fun E0x7f66ed94ce90 () Bool)
(declare-fun v0x7f66ed9496d0_0 () Real)
(declare-fun v0x7f66ed94c510_0 () Bool)
(declare-fun v0x7f66ed94b8d0_0 () Bool)
(declare-fun E0x7f66ed94bad0 () Bool)
(declare-fun v0x7f66ed94ba10_0 () Bool)
(declare-fun E0x7f66ed94b710 () Bool)
(declare-fun v0x7f66ed94b650_0 () Bool)
(declare-fun v0x7f66ed94b250_0 () Bool)
(declare-fun v0x7f66ed94b390_0 () Bool)
(declare-fun v0x7f66ed94aad0_0 () Bool)
(declare-fun v0x7f66ed94a290_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f66ed94a3d0_0 () Bool)
(declare-fun E0x7f66ed94ae10 () Bool)
(declare-fun E0x7f66ed949f10 () Bool)
(declare-fun v0x7f66ed949d50_0 () Bool)
(declare-fun E0x7f66ed949990 () Bool)
(declare-fun v0x7f66ed948e10_0 () Real)
(declare-fun v0x7f66ed949c10_0 () Bool)
(declare-fun v0x7f66ed94c5d0_0 () Real)
(declare-fun v0x7f66ed949010_0 () Real)
(declare-fun v0x7f66ed94c290_0 () Bool)
(declare-fun v0x7f66ed94bc90_0 () Bool)
(declare-fun v0x7f66ed948f50_0 () Bool)
(declare-fun E0x7f66ed94ac50 () Bool)
(declare-fun v0x7f66ed94a990_0 () Real)
(declare-fun v0x7f66ed9488d0_0 () Bool)
(declare-fun E0x7f66ed94b450 () Bool)
(declare-fun v0x7f66ed946d90_0 () Real)
(declare-fun E0x7f66ed94a490 () Bool)
(declare-fun E0x7f66ed948ad0 () Bool)
(declare-fun v0x7f66ed94a7d0_0 () Real)
(declare-fun v0x7f66ed948a10_0 () Bool)
(declare-fun E0x7f66ed94d490 () Bool)
(declare-fun v0x7f66ed946e90_0 () Real)
(declare-fun v0x7f66ed94ab90_0 () Real)
(declare-fun v0x7f66ed946f90_0 () Real)
(declare-fun F0x7f66ed94ed90 () Bool)
(declare-fun E0x7f66ed9483d0 () Bool)
(declare-fun v0x7f66ed94c750_0 () Real)
(declare-fun v0x7f66ed946c10_0 () Real)
(declare-fun v0x7f66ed947f50_0 () Real)
(declare-fun v0x7f66ed947690_0 () Bool)
(declare-fun E0x7f66ed94cbd0 () Bool)
(declare-fun E0x7f66ed949e10 () Bool)
(declare-fun v0x7f66ed948090_0 () Bool)
(declare-fun E0x7f66ed94c810 () Bool)
(declare-fun E0x7f66ed947a10 () Bool)
(declare-fun v0x7f66ed947950_0 () Bool)
(declare-fun E0x7f66ed94d2d0 () Bool)
(declare-fun v0x7f66ed949790_0 () Bool)
(declare-fun E0x7f66ed94c350 () Bool)
(declare-fun F0x7f66ed94ebd0 () Bool)
(declare-fun F0x7f66ed94ec90 () Bool)
(declare-fun post!bb1.i.i29.i.i!0 () Bool)
(declare-fun v0x7f66ed945010_0 () Real)
(declare-fun v0x7f66ed948150_0 () Real)
(declare-fun v0x7f66ed94bdd0_0 () Bool)
(declare-fun v0x7f66ed946f50_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f66ed947810_0 () Bool)
(declare-fun E0x7f66ed94be90 () Bool)
(declare-fun v0x7f66ed9498d0_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun F0x7f66ed94ee90 () Bool)
(declare-fun v0x7f66ed94dd50_0 () Bool)
(declare-fun v0x7f66ed947050_0 () Real)
(declare-fun E0x7f66ed949290 () Bool)
(declare-fun E0x7f66ed948210 () Bool)
(declare-fun v0x7f66ed94c150_0 () Real)
(declare-fun v0x7f66ed946e50_0 () Real)
(declare-fun v0x7f66ed94c690_0 () Real)
(declare-fun v0x7f66ed945110_0 () Bool)
(declare-fun F0x7f66ed94ed50 () Bool)
(declare-fun E0x7f66ed9490d0 () Bool)

(assert (=> F0x7f66ed94ed50
    (and v0x7f66ed945110_0
         (<= v0x7f66ed946e50_0 1.0)
         (>= v0x7f66ed946e50_0 1.0)
         (<= v0x7f66ed946f50_0 0.0)
         (>= v0x7f66ed946f50_0 0.0)
         (<= v0x7f66ed947050_0 0.0)
         (>= v0x7f66ed947050_0 0.0)
         (<= v0x7f66ed945010_0 1.0)
         (>= v0x7f66ed945010_0 1.0))))
(assert (=> F0x7f66ed94ed50 F0x7f66ed94ec90))
(assert (let ((a!1 (=> v0x7f66ed948090_0
               (or (and v0x7f66ed947950_0
                        E0x7f66ed948210
                        (<= v0x7f66ed948150_0 v0x7f66ed947f50_0)
                        (>= v0x7f66ed948150_0 v0x7f66ed947f50_0))
                   (and v0x7f66ed947690_0
                        E0x7f66ed9483d0
                        v0x7f66ed947810_0
                        (<= v0x7f66ed948150_0 v0x7f66ed946f90_0)
                        (>= v0x7f66ed948150_0 v0x7f66ed946f90_0)))))
      (a!2 (=> v0x7f66ed948090_0
               (or (and E0x7f66ed948210 (not E0x7f66ed9483d0))
                   (and E0x7f66ed9483d0 (not E0x7f66ed948210)))))
      (a!3 (=> v0x7f66ed948f50_0
               (or (and v0x7f66ed948a10_0
                        E0x7f66ed9490d0
                        (<= v0x7f66ed949010_0 v0x7f66ed948e10_0)
                        (>= v0x7f66ed949010_0 v0x7f66ed948e10_0))
                   (and v0x7f66ed948090_0
                        E0x7f66ed949290
                        v0x7f66ed9488d0_0
                        (<= v0x7f66ed949010_0 v0x7f66ed946e90_0)
                        (>= v0x7f66ed949010_0 v0x7f66ed946e90_0)))))
      (a!4 (=> v0x7f66ed948f50_0
               (or (and E0x7f66ed9490d0 (not E0x7f66ed949290))
                   (and E0x7f66ed949290 (not E0x7f66ed9490d0)))))
      (a!5 (=> v0x7f66ed949d50_0
               (or (and v0x7f66ed9498d0_0 E0x7f66ed949e10 v0x7f66ed949c10_0)
                   (and v0x7f66ed948f50_0
                        E0x7f66ed949f10
                        (not v0x7f66ed949790_0)))))
      (a!6 (=> v0x7f66ed949d50_0
               (or (and E0x7f66ed949e10 (not E0x7f66ed949f10))
                   (and E0x7f66ed949f10 (not E0x7f66ed949e10)))))
      (a!7 (=> v0x7f66ed94aad0_0
               (or (and v0x7f66ed94a3d0_0
                        E0x7f66ed94ac50
                        (<= v0x7f66ed94ab90_0 v0x7f66ed94a990_0)
                        (>= v0x7f66ed94ab90_0 v0x7f66ed94a990_0))
                   (and v0x7f66ed949d50_0
                        E0x7f66ed94ae10
                        v0x7f66ed94a290_0
                        (<= v0x7f66ed94ab90_0 v0x7f66ed948150_0)
                        (>= v0x7f66ed94ab90_0 v0x7f66ed948150_0)))))
      (a!8 (=> v0x7f66ed94aad0_0
               (or (and E0x7f66ed94ac50 (not E0x7f66ed94ae10))
                   (and E0x7f66ed94ae10 (not E0x7f66ed94ac50)))))
      (a!9 (or (and v0x7f66ed94bdd0_0
                    E0x7f66ed94c810
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (<= v0x7f66ed94c750_0 v0x7f66ed94c150_0)
                    (>= v0x7f66ed94c750_0 v0x7f66ed94c150_0))
               (and v0x7f66ed94b650_0
                    E0x7f66ed94cbd0
                    (not v0x7f66ed94b8d0_0)
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 v0x7f66ed946d90_0)
                         (>= v0x7f66ed94c750_0 v0x7f66ed946d90_0)))
               (and v0x7f66ed94c290_0
                    E0x7f66ed94ce90
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 v0x7f66ed946d90_0)
                         (>= v0x7f66ed94c750_0 v0x7f66ed946d90_0)))
               (and v0x7f66ed94ba10_0
                    E0x7f66ed94d010
                    (not v0x7f66ed94bc90_0)
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 0.0) (>= v0x7f66ed94c750_0 0.0)))
               (and v0x7f66ed94aad0_0
                    E0x7f66ed94d2d0
                    v0x7f66ed94b250_0
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 v0x7f66ed946d90_0)
                         (>= v0x7f66ed94c750_0 v0x7f66ed946d90_0)))
               (and v0x7f66ed9498d0_0
                    E0x7f66ed94d490
                    (not v0x7f66ed949c10_0)
                    (<= v0x7f66ed94c5d0_0 0.0)
                    (>= v0x7f66ed94c5d0_0 0.0)
                    (<= v0x7f66ed94c690_0 v0x7f66ed948150_0)
                    (>= v0x7f66ed94c690_0 v0x7f66ed948150_0)
                    (and (<= v0x7f66ed94c750_0 0.0) (>= v0x7f66ed94c750_0 0.0)))))
      (a!10 (=> v0x7f66ed94c510_0
                (or (and E0x7f66ed94c810
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94cbd0
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94ce90
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94d010
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94d2d0
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94d490
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0))))))
(let ((a!11 (and (=> v0x7f66ed947950_0
                     (and v0x7f66ed947690_0
                          E0x7f66ed947a10
                          (not v0x7f66ed947810_0)))
                 (=> v0x7f66ed947950_0 E0x7f66ed947a10)
                 a!1
                 a!2
                 (=> v0x7f66ed948a10_0
                     (and v0x7f66ed948090_0
                          E0x7f66ed948ad0
                          (not v0x7f66ed9488d0_0)))
                 (=> v0x7f66ed948a10_0 E0x7f66ed948ad0)
                 a!3
                 a!4
                 (=> v0x7f66ed9498d0_0
                     (and v0x7f66ed948f50_0 E0x7f66ed949990 v0x7f66ed949790_0))
                 (=> v0x7f66ed9498d0_0 E0x7f66ed949990)
                 a!5
                 a!6
                 (=> v0x7f66ed94a3d0_0
                     (and v0x7f66ed949d50_0
                          E0x7f66ed94a490
                          (not v0x7f66ed94a290_0)))
                 (=> v0x7f66ed94a3d0_0 E0x7f66ed94a490)
                 a!7
                 a!8
                 (=> v0x7f66ed94b390_0
                     (and v0x7f66ed94aad0_0
                          E0x7f66ed94b450
                          (not v0x7f66ed94b250_0)))
                 (=> v0x7f66ed94b390_0 E0x7f66ed94b450)
                 (=> v0x7f66ed94b650_0
                     (and v0x7f66ed94b390_0 E0x7f66ed94b710 v0x7f66ed94a290_0))
                 (=> v0x7f66ed94b650_0 E0x7f66ed94b710)
                 (=> v0x7f66ed94ba10_0
                     (and v0x7f66ed94b390_0
                          E0x7f66ed94bad0
                          (not v0x7f66ed94a290_0)))
                 (=> v0x7f66ed94ba10_0 E0x7f66ed94bad0)
                 (=> v0x7f66ed94bdd0_0
                     (and v0x7f66ed94b650_0 E0x7f66ed94be90 v0x7f66ed94b8d0_0))
                 (=> v0x7f66ed94bdd0_0 E0x7f66ed94be90)
                 (=> v0x7f66ed94c290_0
                     (and v0x7f66ed94ba10_0 E0x7f66ed94c350 v0x7f66ed94bc90_0))
                 (=> v0x7f66ed94c290_0 E0x7f66ed94c350)
                 (=> v0x7f66ed94c510_0 a!9)
                 a!10
                 v0x7f66ed94c510_0
                 v0x7f66ed94de90_0
                 (<= v0x7f66ed946e50_0 v0x7f66ed94c5d0_0)
                 (>= v0x7f66ed946e50_0 v0x7f66ed94c5d0_0)
                 (<= v0x7f66ed946f50_0 v0x7f66ed94c750_0)
                 (>= v0x7f66ed946f50_0 v0x7f66ed94c750_0)
                 (<= v0x7f66ed947050_0 v0x7f66ed949010_0)
                 (>= v0x7f66ed947050_0 v0x7f66ed949010_0)
                 (<= v0x7f66ed945010_0 v0x7f66ed94c690_0)
                 (>= v0x7f66ed945010_0 v0x7f66ed94c690_0)
                 (= v0x7f66ed947810_0 (= v0x7f66ed947750_0 0.0))
                 (= v0x7f66ed947c50_0 (< v0x7f66ed946f90_0 2.0))
                 (= v0x7f66ed947e10_0 (ite v0x7f66ed947c50_0 1.0 0.0))
                 (= v0x7f66ed947f50_0 (+ v0x7f66ed947e10_0 v0x7f66ed946f90_0))
                 (= v0x7f66ed9488d0_0 (= v0x7f66ed948810_0 0.0))
                 (= v0x7f66ed948cd0_0 (= v0x7f66ed946e90_0 0.0))
                 (= v0x7f66ed948e10_0 (ite v0x7f66ed948cd0_0 1.0 0.0))
                 (= v0x7f66ed949790_0 (= v0x7f66ed9496d0_0 0.0))
                 (= v0x7f66ed949c10_0 (= v0x7f66ed949b50_0 0.0))
                 (= v0x7f66ed94a290_0 (= v0x7f66ed946d90_0 0.0))
                 (= v0x7f66ed94a690_0 (> v0x7f66ed948150_0 0.0))
                 (= v0x7f66ed94a7d0_0 (+ v0x7f66ed948150_0 (- 1.0)))
                 (= v0x7f66ed94a990_0
                    (ite v0x7f66ed94a690_0 v0x7f66ed94a7d0_0 v0x7f66ed948150_0))
                 (= v0x7f66ed94b250_0 (= v0x7f66ed946c10_0 0.0))
                 (= v0x7f66ed94b8d0_0 (> v0x7f66ed94ab90_0 1.0))
                 (= v0x7f66ed94bc90_0 (= v0x7f66ed949010_0 0.0))
                 (= v0x7f66ed94c050_0 (= v0x7f66ed949010_0 0.0))
                 (= v0x7f66ed94c150_0
                    (ite v0x7f66ed94c050_0 1.0 v0x7f66ed946d90_0))
                 (= v0x7f66ed94dc50_0 (= v0x7f66ed949010_0 0.0))
                 (= v0x7f66ed94dd50_0 (= v0x7f66ed94c750_0 0.0))
                 (= v0x7f66ed94de90_0 (or v0x7f66ed94dd50_0 v0x7f66ed94dc50_0)))))
  (=> F0x7f66ed94ebd0 a!11))))
(assert (=> F0x7f66ed94ebd0 F0x7f66ed94eb10))
(assert (let ((a!1 (=> v0x7f66ed948090_0
               (or (and v0x7f66ed947950_0
                        E0x7f66ed948210
                        (<= v0x7f66ed948150_0 v0x7f66ed947f50_0)
                        (>= v0x7f66ed948150_0 v0x7f66ed947f50_0))
                   (and v0x7f66ed947690_0
                        E0x7f66ed9483d0
                        v0x7f66ed947810_0
                        (<= v0x7f66ed948150_0 v0x7f66ed946f90_0)
                        (>= v0x7f66ed948150_0 v0x7f66ed946f90_0)))))
      (a!2 (=> v0x7f66ed948090_0
               (or (and E0x7f66ed948210 (not E0x7f66ed9483d0))
                   (and E0x7f66ed9483d0 (not E0x7f66ed948210)))))
      (a!3 (=> v0x7f66ed948f50_0
               (or (and v0x7f66ed948a10_0
                        E0x7f66ed9490d0
                        (<= v0x7f66ed949010_0 v0x7f66ed948e10_0)
                        (>= v0x7f66ed949010_0 v0x7f66ed948e10_0))
                   (and v0x7f66ed948090_0
                        E0x7f66ed949290
                        v0x7f66ed9488d0_0
                        (<= v0x7f66ed949010_0 v0x7f66ed946e90_0)
                        (>= v0x7f66ed949010_0 v0x7f66ed946e90_0)))))
      (a!4 (=> v0x7f66ed948f50_0
               (or (and E0x7f66ed9490d0 (not E0x7f66ed949290))
                   (and E0x7f66ed949290 (not E0x7f66ed9490d0)))))
      (a!5 (=> v0x7f66ed949d50_0
               (or (and v0x7f66ed9498d0_0 E0x7f66ed949e10 v0x7f66ed949c10_0)
                   (and v0x7f66ed948f50_0
                        E0x7f66ed949f10
                        (not v0x7f66ed949790_0)))))
      (a!6 (=> v0x7f66ed949d50_0
               (or (and E0x7f66ed949e10 (not E0x7f66ed949f10))
                   (and E0x7f66ed949f10 (not E0x7f66ed949e10)))))
      (a!7 (=> v0x7f66ed94aad0_0
               (or (and v0x7f66ed94a3d0_0
                        E0x7f66ed94ac50
                        (<= v0x7f66ed94ab90_0 v0x7f66ed94a990_0)
                        (>= v0x7f66ed94ab90_0 v0x7f66ed94a990_0))
                   (and v0x7f66ed949d50_0
                        E0x7f66ed94ae10
                        v0x7f66ed94a290_0
                        (<= v0x7f66ed94ab90_0 v0x7f66ed948150_0)
                        (>= v0x7f66ed94ab90_0 v0x7f66ed948150_0)))))
      (a!8 (=> v0x7f66ed94aad0_0
               (or (and E0x7f66ed94ac50 (not E0x7f66ed94ae10))
                   (and E0x7f66ed94ae10 (not E0x7f66ed94ac50)))))
      (a!9 (or (and v0x7f66ed94bdd0_0
                    E0x7f66ed94c810
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (<= v0x7f66ed94c750_0 v0x7f66ed94c150_0)
                    (>= v0x7f66ed94c750_0 v0x7f66ed94c150_0))
               (and v0x7f66ed94b650_0
                    E0x7f66ed94cbd0
                    (not v0x7f66ed94b8d0_0)
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 v0x7f66ed946d90_0)
                         (>= v0x7f66ed94c750_0 v0x7f66ed946d90_0)))
               (and v0x7f66ed94c290_0
                    E0x7f66ed94ce90
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 v0x7f66ed946d90_0)
                         (>= v0x7f66ed94c750_0 v0x7f66ed946d90_0)))
               (and v0x7f66ed94ba10_0
                    E0x7f66ed94d010
                    (not v0x7f66ed94bc90_0)
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 0.0) (>= v0x7f66ed94c750_0 0.0)))
               (and v0x7f66ed94aad0_0
                    E0x7f66ed94d2d0
                    v0x7f66ed94b250_0
                    (and (<= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0)
                         (>= v0x7f66ed94c5d0_0 v0x7f66ed946c10_0))
                    (and (<= v0x7f66ed94c690_0 v0x7f66ed94ab90_0)
                         (>= v0x7f66ed94c690_0 v0x7f66ed94ab90_0))
                    (and (<= v0x7f66ed94c750_0 v0x7f66ed946d90_0)
                         (>= v0x7f66ed94c750_0 v0x7f66ed946d90_0)))
               (and v0x7f66ed9498d0_0
                    E0x7f66ed94d490
                    (not v0x7f66ed949c10_0)
                    (<= v0x7f66ed94c5d0_0 0.0)
                    (>= v0x7f66ed94c5d0_0 0.0)
                    (<= v0x7f66ed94c690_0 v0x7f66ed948150_0)
                    (>= v0x7f66ed94c690_0 v0x7f66ed948150_0)
                    (and (<= v0x7f66ed94c750_0 0.0) (>= v0x7f66ed94c750_0 0.0)))))
      (a!10 (=> v0x7f66ed94c510_0
                (or (and E0x7f66ed94c810
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94cbd0
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94ce90
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94d010
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d2d0)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94d2d0
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d490))
                    (and E0x7f66ed94d490
                         (not E0x7f66ed94c810)
                         (not E0x7f66ed94cbd0)
                         (not E0x7f66ed94ce90)
                         (not E0x7f66ed94d010)
                         (not E0x7f66ed94d2d0))))))
(let ((a!11 (and (=> v0x7f66ed947950_0
                     (and v0x7f66ed947690_0
                          E0x7f66ed947a10
                          (not v0x7f66ed947810_0)))
                 (=> v0x7f66ed947950_0 E0x7f66ed947a10)
                 a!1
                 a!2
                 (=> v0x7f66ed948a10_0
                     (and v0x7f66ed948090_0
                          E0x7f66ed948ad0
                          (not v0x7f66ed9488d0_0)))
                 (=> v0x7f66ed948a10_0 E0x7f66ed948ad0)
                 a!3
                 a!4
                 (=> v0x7f66ed9498d0_0
                     (and v0x7f66ed948f50_0 E0x7f66ed949990 v0x7f66ed949790_0))
                 (=> v0x7f66ed9498d0_0 E0x7f66ed949990)
                 a!5
                 a!6
                 (=> v0x7f66ed94a3d0_0
                     (and v0x7f66ed949d50_0
                          E0x7f66ed94a490
                          (not v0x7f66ed94a290_0)))
                 (=> v0x7f66ed94a3d0_0 E0x7f66ed94a490)
                 a!7
                 a!8
                 (=> v0x7f66ed94b390_0
                     (and v0x7f66ed94aad0_0
                          E0x7f66ed94b450
                          (not v0x7f66ed94b250_0)))
                 (=> v0x7f66ed94b390_0 E0x7f66ed94b450)
                 (=> v0x7f66ed94b650_0
                     (and v0x7f66ed94b390_0 E0x7f66ed94b710 v0x7f66ed94a290_0))
                 (=> v0x7f66ed94b650_0 E0x7f66ed94b710)
                 (=> v0x7f66ed94ba10_0
                     (and v0x7f66ed94b390_0
                          E0x7f66ed94bad0
                          (not v0x7f66ed94a290_0)))
                 (=> v0x7f66ed94ba10_0 E0x7f66ed94bad0)
                 (=> v0x7f66ed94bdd0_0
                     (and v0x7f66ed94b650_0 E0x7f66ed94be90 v0x7f66ed94b8d0_0))
                 (=> v0x7f66ed94bdd0_0 E0x7f66ed94be90)
                 (=> v0x7f66ed94c290_0
                     (and v0x7f66ed94ba10_0 E0x7f66ed94c350 v0x7f66ed94bc90_0))
                 (=> v0x7f66ed94c290_0 E0x7f66ed94c350)
                 (=> v0x7f66ed94c510_0 a!9)
                 a!10
                 v0x7f66ed94c510_0
                 (not v0x7f66ed94de90_0)
                 (= v0x7f66ed947810_0 (= v0x7f66ed947750_0 0.0))
                 (= v0x7f66ed947c50_0 (< v0x7f66ed946f90_0 2.0))
                 (= v0x7f66ed947e10_0 (ite v0x7f66ed947c50_0 1.0 0.0))
                 (= v0x7f66ed947f50_0 (+ v0x7f66ed947e10_0 v0x7f66ed946f90_0))
                 (= v0x7f66ed9488d0_0 (= v0x7f66ed948810_0 0.0))
                 (= v0x7f66ed948cd0_0 (= v0x7f66ed946e90_0 0.0))
                 (= v0x7f66ed948e10_0 (ite v0x7f66ed948cd0_0 1.0 0.0))
                 (= v0x7f66ed949790_0 (= v0x7f66ed9496d0_0 0.0))
                 (= v0x7f66ed949c10_0 (= v0x7f66ed949b50_0 0.0))
                 (= v0x7f66ed94a290_0 (= v0x7f66ed946d90_0 0.0))
                 (= v0x7f66ed94a690_0 (> v0x7f66ed948150_0 0.0))
                 (= v0x7f66ed94a7d0_0 (+ v0x7f66ed948150_0 (- 1.0)))
                 (= v0x7f66ed94a990_0
                    (ite v0x7f66ed94a690_0 v0x7f66ed94a7d0_0 v0x7f66ed948150_0))
                 (= v0x7f66ed94b250_0 (= v0x7f66ed946c10_0 0.0))
                 (= v0x7f66ed94b8d0_0 (> v0x7f66ed94ab90_0 1.0))
                 (= v0x7f66ed94bc90_0 (= v0x7f66ed949010_0 0.0))
                 (= v0x7f66ed94c050_0 (= v0x7f66ed949010_0 0.0))
                 (= v0x7f66ed94c150_0
                    (ite v0x7f66ed94c050_0 1.0 v0x7f66ed946d90_0))
                 (= v0x7f66ed94dc50_0 (= v0x7f66ed949010_0 0.0))
                 (= v0x7f66ed94dd50_0 (= v0x7f66ed94c750_0 0.0))
                 (= v0x7f66ed94de90_0 (or v0x7f66ed94dd50_0 v0x7f66ed94dc50_0)))))
  (=> F0x7f66ed94ed90 a!11))))
(assert (=> F0x7f66ed94ed90 F0x7f66ed94eb10))
(assert (=> F0x7f66ed94eed0 (or F0x7f66ed94ed50 F0x7f66ed94ebd0)))
(assert (=> F0x7f66ed94ee90 F0x7f66ed94ed90))
(assert (=> pre!entry!0 (=> F0x7f66ed94ec90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f66ed94eb10 (>= v0x7f66ed946d90_0 0.0))))
(assert (let ((a!1 (=> F0x7f66ed94eb10
               (or (not (<= 1.0 v0x7f66ed946d90_0))
                   (not (<= 0.0 v0x7f66ed946c10_0))
                   (not (<= v0x7f66ed946c10_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f66ed94eb10
        (or (>= v0x7f66ed946d90_0 1.0) (<= v0x7f66ed946d90_0 0.0)))))
(assert (let ((a!1 (not (or (not (<= 1.0 v0x7f66ed946f50_0))
                    (not (<= 0.0 v0x7f66ed946e50_0))
                    (not (<= v0x7f66ed946e50_0 0.0)))))
      (a!2 (and (not post!bb1.i.i!2)
                F0x7f66ed94eed0
                (not (or (>= v0x7f66ed946f50_0 1.0) (<= v0x7f66ed946f50_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f66ed94eed0
           (not (>= v0x7f66ed946f50_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f66ed94eed0 a!1)
      a!2
      (and (not post!bb1.i.i29.i.i!0) F0x7f66ed94ee90 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i29.i.i!0)
