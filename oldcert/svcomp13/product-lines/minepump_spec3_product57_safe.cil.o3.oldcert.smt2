(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fd716dee010 () Bool)
(declare-fun F0x7fd716dedf10 () Bool)
(declare-fun F0x7fd716dede50 () Bool)
(declare-fun v0x7fd716decc50_0 () Bool)
(declare-fun v0x7fd716de8810_0 () Real)
(declare-fun v0x7fd716decd50_0 () Bool)
(declare-fun v0x7fd716ded110_0 () Bool)
(declare-fun E0x7fd716dec690 () Bool)
(declare-fun F0x7fd716dee050 () Bool)
(declare-fun E0x7fd716dec490 () Bool)
(declare-fun v0x7fd716de7f10_0 () Real)
(declare-fun v0x7fd716decfd0_0 () Bool)
(declare-fun E0x7fd716dec210 () Bool)
(declare-fun v0x7fd716de8d10_0 () Bool)
(declare-fun v0x7fd716dece90_0 () Bool)
(declare-fun v0x7fd716deb950_0 () Real)
(declare-fun v0x7fd716debe90_0 () Real)
(declare-fun v0x7fd716de9d90_0 () Bool)
(declare-fun v0x7fd716debdd0_0 () Real)
(declare-fun v0x7fd716debd10_0 () Bool)
(declare-fun v0x7fd716deac10_0 () Bool)
(declare-fun E0x7fd716deb650 () Bool)
(declare-fun E0x7fd716deae10 () Bool)
(declare-fun v0x7fd716dea850_0 () Bool)
(declare-fun v0x7fd716dea990_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7fd716deaa50 () Bool)
(declare-fun E0x7fd716dea350 () Bool)
(declare-fun v0x7fd716deb810_0 () Bool)
(declare-fun E0x7fd716debf50 () Bool)
(declare-fun v0x7fd716de9990_0 () Bool)
(declare-fun v0x7fd716deb310_0 () Real)
(declare-fun E0x7fd716de9b90 () Bool)
(declare-fun v0x7fd716de9ad0_0 () Bool)
(declare-fun v0x7fd716de8190_0 () Real)
(declare-fun v0x7fd716deb450_0 () Bool)
(declare-fun v0x7fd716deb590_0 () Bool)
(declare-fun v0x7fd716deba90_0 () Bool)
(declare-fun E0x7fd716de9490 () Bool)
(declare-fun v0x7fd716de9ed0_0 () Real)
(declare-fun v0x7fd716dea010_0 () Bool)
(declare-fun v0x7fd716de98d0_0 () Real)
(declare-fun E0x7fd716debb50 () Bool)
(declare-fun v0x7fd716de9010_0 () Real)
(declare-fun v0x7fd716de9210_0 () Real)
(declare-fun v0x7fd716de88d0_0 () Bool)
(declare-fun v0x7fd716de8a10_0 () Bool)
(declare-fun F0x7fd716dedcd0 () Bool)
(declare-fun v0x7fd716dead50_0 () Bool)
(declare-fun v0x7fd716deb150_0 () Real)
(declare-fun v0x7fd716dea0d0_0 () Real)
(declare-fun E0x7fd716dea190 () Bool)
(declare-fun v0x7fd716de8ed0_0 () Real)
(declare-fun v0x7fd716de8090_0 () Real)
(declare-fun E0x7fd716de92d0 () Bool)
(declare-fun post!bb2.i.i34.i.i!0 () Bool)
(declare-fun F0x7fd716dedc90 () Bool)
(declare-fun v0x7fd716de6010_0 () Real)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7fd716de9150_0 () Bool)
(declare-fun v0x7fd716de8250_0 () Real)
(declare-fun v0x7fd716de8150_0 () Real)
(declare-fun v0x7fd716de8750_0 () Bool)
(declare-fun F0x7fd716dedd90 () Bool)
(declare-fun E0x7fd716de8ad0 () Bool)
(declare-fun v0x7fd716de6110_0 () Bool)
(declare-fun v0x7fd716deb010_0 () Bool)

(assert (=> F0x7fd716dedd90
    (and v0x7fd716de6110_0
         (<= v0x7fd716de8150_0 0.0)
         (>= v0x7fd716de8150_0 0.0)
         (<= v0x7fd716de8250_0 0.0)
         (>= v0x7fd716de8250_0 0.0)
         (<= v0x7fd716de6010_0 1.0)
         (>= v0x7fd716de6010_0 1.0))))
(assert (=> F0x7fd716dedd90 F0x7fd716dedc90))
(assert (let ((a!1 (=> v0x7fd716de9150_0
               (or (and v0x7fd716de8a10_0
                        E0x7fd716de92d0
                        (<= v0x7fd716de9210_0 v0x7fd716de9010_0)
                        (>= v0x7fd716de9210_0 v0x7fd716de9010_0))
                   (and v0x7fd716de8750_0
                        E0x7fd716de9490
                        v0x7fd716de88d0_0
                        (<= v0x7fd716de9210_0 v0x7fd716de8190_0)
                        (>= v0x7fd716de9210_0 v0x7fd716de8190_0)))))
      (a!2 (=> v0x7fd716de9150_0
               (or (and E0x7fd716de92d0 (not E0x7fd716de9490))
                   (and E0x7fd716de9490 (not E0x7fd716de92d0)))))
      (a!3 (=> v0x7fd716dea010_0
               (or (and v0x7fd716de9ad0_0
                        E0x7fd716dea190
                        (<= v0x7fd716dea0d0_0 v0x7fd716de9ed0_0)
                        (>= v0x7fd716dea0d0_0 v0x7fd716de9ed0_0))
                   (and v0x7fd716de9150_0
                        E0x7fd716dea350
                        v0x7fd716de9990_0
                        (<= v0x7fd716dea0d0_0 v0x7fd716de8090_0)
                        (>= v0x7fd716dea0d0_0 v0x7fd716de8090_0)))))
      (a!4 (=> v0x7fd716dea010_0
               (or (and E0x7fd716dea190 (not E0x7fd716dea350))
                   (and E0x7fd716dea350 (not E0x7fd716dea190)))))
      (a!5 (or (and v0x7fd716deb590_0
                    E0x7fd716debf50
                    (and (<= v0x7fd716debdd0_0 v0x7fd716de9210_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716de9210_0))
                    (<= v0x7fd716debe90_0 v0x7fd716deb950_0)
                    (>= v0x7fd716debe90_0 v0x7fd716deb950_0))
               (and v0x7fd716dea990_0
                    E0x7fd716dec210
                    (not v0x7fd716deac10_0)
                    (and (<= v0x7fd716debdd0_0 v0x7fd716de9210_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716de9210_0))
                    (and (<= v0x7fd716debe90_0 v0x7fd716de7f10_0)
                         (>= v0x7fd716debe90_0 v0x7fd716de7f10_0)))
               (and v0x7fd716deba90_0
                    E0x7fd716dec490
                    (and (<= v0x7fd716debdd0_0 v0x7fd716deb310_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716deb310_0))
                    (and (<= v0x7fd716debe90_0 v0x7fd716de7f10_0)
                         (>= v0x7fd716debe90_0 v0x7fd716de7f10_0)))
               (and v0x7fd716dead50_0
                    E0x7fd716dec690
                    (not v0x7fd716deb450_0)
                    (and (<= v0x7fd716debdd0_0 v0x7fd716deb310_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716deb310_0))
                    (<= v0x7fd716debe90_0 0.0)
                    (>= v0x7fd716debe90_0 0.0))))
      (a!6 (=> v0x7fd716debd10_0
               (or (and E0x7fd716debf50
                        (not E0x7fd716dec210)
                        (not E0x7fd716dec490)
                        (not E0x7fd716dec690))
                   (and E0x7fd716dec210
                        (not E0x7fd716debf50)
                        (not E0x7fd716dec490)
                        (not E0x7fd716dec690))
                   (and E0x7fd716dec490
                        (not E0x7fd716debf50)
                        (not E0x7fd716dec210)
                        (not E0x7fd716dec690))
                   (and E0x7fd716dec690
                        (not E0x7fd716debf50)
                        (not E0x7fd716dec210)
                        (not E0x7fd716dec490))))))
(let ((a!7 (and (=> v0x7fd716de8a10_0
                    (and v0x7fd716de8750_0
                         E0x7fd716de8ad0
                         (not v0x7fd716de88d0_0)))
                (=> v0x7fd716de8a10_0 E0x7fd716de8ad0)
                a!1
                a!2
                (=> v0x7fd716de9ad0_0
                    (and v0x7fd716de9150_0
                         E0x7fd716de9b90
                         (not v0x7fd716de9990_0)))
                (=> v0x7fd716de9ad0_0 E0x7fd716de9b90)
                a!3
                a!4
                (=> v0x7fd716dea990_0
                    (and v0x7fd716dea010_0 E0x7fd716deaa50 v0x7fd716dea850_0))
                (=> v0x7fd716dea990_0 E0x7fd716deaa50)
                (=> v0x7fd716dead50_0
                    (and v0x7fd716dea010_0
                         E0x7fd716deae10
                         (not v0x7fd716dea850_0)))
                (=> v0x7fd716dead50_0 E0x7fd716deae10)
                (=> v0x7fd716deb590_0
                    (and v0x7fd716dea990_0 E0x7fd716deb650 v0x7fd716deac10_0))
                (=> v0x7fd716deb590_0 E0x7fd716deb650)
                (=> v0x7fd716deba90_0
                    (and v0x7fd716dead50_0 E0x7fd716debb50 v0x7fd716deb450_0))
                (=> v0x7fd716deba90_0 E0x7fd716debb50)
                (=> v0x7fd716debd10_0 a!5)
                a!6
                v0x7fd716debd10_0
                (not v0x7fd716ded110_0)
                (<= v0x7fd716de8150_0 v0x7fd716debe90_0)
                (>= v0x7fd716de8150_0 v0x7fd716debe90_0)
                (<= v0x7fd716de8250_0 v0x7fd716dea0d0_0)
                (>= v0x7fd716de8250_0 v0x7fd716dea0d0_0)
                (<= v0x7fd716de6010_0 v0x7fd716debdd0_0)
                (>= v0x7fd716de6010_0 v0x7fd716debdd0_0)
                (= v0x7fd716de88d0_0 (= v0x7fd716de8810_0 0.0))
                (= v0x7fd716de8d10_0 (< v0x7fd716de8190_0 2.0))
                (= v0x7fd716de8ed0_0 (ite v0x7fd716de8d10_0 1.0 0.0))
                (= v0x7fd716de9010_0 (+ v0x7fd716de8ed0_0 v0x7fd716de8190_0))
                (= v0x7fd716de9990_0 (= v0x7fd716de98d0_0 0.0))
                (= v0x7fd716de9d90_0 (= v0x7fd716de8090_0 0.0))
                (= v0x7fd716de9ed0_0 (ite v0x7fd716de9d90_0 1.0 0.0))
                (= v0x7fd716dea850_0 (= v0x7fd716de7f10_0 0.0))
                (= v0x7fd716deac10_0 (> v0x7fd716de9210_0 1.0))
                (= v0x7fd716deb010_0 (> v0x7fd716de9210_0 0.0))
                (= v0x7fd716deb150_0 (+ v0x7fd716de9210_0 (- 1.0)))
                (= v0x7fd716deb310_0
                   (ite v0x7fd716deb010_0 v0x7fd716deb150_0 v0x7fd716de9210_0))
                (= v0x7fd716deb450_0 (= v0x7fd716deb310_0 0.0))
                (= v0x7fd716deb810_0 (= v0x7fd716dea0d0_0 0.0))
                (= v0x7fd716deb950_0
                   (ite v0x7fd716deb810_0 1.0 v0x7fd716de7f10_0))
                (= v0x7fd716decc50_0 (= v0x7fd716dea0d0_0 0.0))
                (= v0x7fd716decd50_0 (= v0x7fd716debdd0_0 2.0))
                (= v0x7fd716dece90_0 (= v0x7fd716debe90_0 0.0))
                (= v0x7fd716decfd0_0 (and v0x7fd716decd50_0 v0x7fd716decc50_0))
                (= v0x7fd716ded110_0 (and v0x7fd716decfd0_0 v0x7fd716dece90_0)))))
  (=> F0x7fd716dedcd0 a!7))))
(assert (=> F0x7fd716dedcd0 F0x7fd716dede50))
(assert (let ((a!1 (=> v0x7fd716de9150_0
               (or (and v0x7fd716de8a10_0
                        E0x7fd716de92d0
                        (<= v0x7fd716de9210_0 v0x7fd716de9010_0)
                        (>= v0x7fd716de9210_0 v0x7fd716de9010_0))
                   (and v0x7fd716de8750_0
                        E0x7fd716de9490
                        v0x7fd716de88d0_0
                        (<= v0x7fd716de9210_0 v0x7fd716de8190_0)
                        (>= v0x7fd716de9210_0 v0x7fd716de8190_0)))))
      (a!2 (=> v0x7fd716de9150_0
               (or (and E0x7fd716de92d0 (not E0x7fd716de9490))
                   (and E0x7fd716de9490 (not E0x7fd716de92d0)))))
      (a!3 (=> v0x7fd716dea010_0
               (or (and v0x7fd716de9ad0_0
                        E0x7fd716dea190
                        (<= v0x7fd716dea0d0_0 v0x7fd716de9ed0_0)
                        (>= v0x7fd716dea0d0_0 v0x7fd716de9ed0_0))
                   (and v0x7fd716de9150_0
                        E0x7fd716dea350
                        v0x7fd716de9990_0
                        (<= v0x7fd716dea0d0_0 v0x7fd716de8090_0)
                        (>= v0x7fd716dea0d0_0 v0x7fd716de8090_0)))))
      (a!4 (=> v0x7fd716dea010_0
               (or (and E0x7fd716dea190 (not E0x7fd716dea350))
                   (and E0x7fd716dea350 (not E0x7fd716dea190)))))
      (a!5 (or (and v0x7fd716deb590_0
                    E0x7fd716debf50
                    (and (<= v0x7fd716debdd0_0 v0x7fd716de9210_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716de9210_0))
                    (<= v0x7fd716debe90_0 v0x7fd716deb950_0)
                    (>= v0x7fd716debe90_0 v0x7fd716deb950_0))
               (and v0x7fd716dea990_0
                    E0x7fd716dec210
                    (not v0x7fd716deac10_0)
                    (and (<= v0x7fd716debdd0_0 v0x7fd716de9210_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716de9210_0))
                    (and (<= v0x7fd716debe90_0 v0x7fd716de7f10_0)
                         (>= v0x7fd716debe90_0 v0x7fd716de7f10_0)))
               (and v0x7fd716deba90_0
                    E0x7fd716dec490
                    (and (<= v0x7fd716debdd0_0 v0x7fd716deb310_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716deb310_0))
                    (and (<= v0x7fd716debe90_0 v0x7fd716de7f10_0)
                         (>= v0x7fd716debe90_0 v0x7fd716de7f10_0)))
               (and v0x7fd716dead50_0
                    E0x7fd716dec690
                    (not v0x7fd716deb450_0)
                    (and (<= v0x7fd716debdd0_0 v0x7fd716deb310_0)
                         (>= v0x7fd716debdd0_0 v0x7fd716deb310_0))
                    (<= v0x7fd716debe90_0 0.0)
                    (>= v0x7fd716debe90_0 0.0))))
      (a!6 (=> v0x7fd716debd10_0
               (or (and E0x7fd716debf50
                        (not E0x7fd716dec210)
                        (not E0x7fd716dec490)
                        (not E0x7fd716dec690))
                   (and E0x7fd716dec210
                        (not E0x7fd716debf50)
                        (not E0x7fd716dec490)
                        (not E0x7fd716dec690))
                   (and E0x7fd716dec490
                        (not E0x7fd716debf50)
                        (not E0x7fd716dec210)
                        (not E0x7fd716dec690))
                   (and E0x7fd716dec690
                        (not E0x7fd716debf50)
                        (not E0x7fd716dec210)
                        (not E0x7fd716dec490))))))
(let ((a!7 (and (=> v0x7fd716de8a10_0
                    (and v0x7fd716de8750_0
                         E0x7fd716de8ad0
                         (not v0x7fd716de88d0_0)))
                (=> v0x7fd716de8a10_0 E0x7fd716de8ad0)
                a!1
                a!2
                (=> v0x7fd716de9ad0_0
                    (and v0x7fd716de9150_0
                         E0x7fd716de9b90
                         (not v0x7fd716de9990_0)))
                (=> v0x7fd716de9ad0_0 E0x7fd716de9b90)
                a!3
                a!4
                (=> v0x7fd716dea990_0
                    (and v0x7fd716dea010_0 E0x7fd716deaa50 v0x7fd716dea850_0))
                (=> v0x7fd716dea990_0 E0x7fd716deaa50)
                (=> v0x7fd716dead50_0
                    (and v0x7fd716dea010_0
                         E0x7fd716deae10
                         (not v0x7fd716dea850_0)))
                (=> v0x7fd716dead50_0 E0x7fd716deae10)
                (=> v0x7fd716deb590_0
                    (and v0x7fd716dea990_0 E0x7fd716deb650 v0x7fd716deac10_0))
                (=> v0x7fd716deb590_0 E0x7fd716deb650)
                (=> v0x7fd716deba90_0
                    (and v0x7fd716dead50_0 E0x7fd716debb50 v0x7fd716deb450_0))
                (=> v0x7fd716deba90_0 E0x7fd716debb50)
                (=> v0x7fd716debd10_0 a!5)
                a!6
                v0x7fd716debd10_0
                v0x7fd716ded110_0
                (= v0x7fd716de88d0_0 (= v0x7fd716de8810_0 0.0))
                (= v0x7fd716de8d10_0 (< v0x7fd716de8190_0 2.0))
                (= v0x7fd716de8ed0_0 (ite v0x7fd716de8d10_0 1.0 0.0))
                (= v0x7fd716de9010_0 (+ v0x7fd716de8ed0_0 v0x7fd716de8190_0))
                (= v0x7fd716de9990_0 (= v0x7fd716de98d0_0 0.0))
                (= v0x7fd716de9d90_0 (= v0x7fd716de8090_0 0.0))
                (= v0x7fd716de9ed0_0 (ite v0x7fd716de9d90_0 1.0 0.0))
                (= v0x7fd716dea850_0 (= v0x7fd716de7f10_0 0.0))
                (= v0x7fd716deac10_0 (> v0x7fd716de9210_0 1.0))
                (= v0x7fd716deb010_0 (> v0x7fd716de9210_0 0.0))
                (= v0x7fd716deb150_0 (+ v0x7fd716de9210_0 (- 1.0)))
                (= v0x7fd716deb310_0
                   (ite v0x7fd716deb010_0 v0x7fd716deb150_0 v0x7fd716de9210_0))
                (= v0x7fd716deb450_0 (= v0x7fd716deb310_0 0.0))
                (= v0x7fd716deb810_0 (= v0x7fd716dea0d0_0 0.0))
                (= v0x7fd716deb950_0
                   (ite v0x7fd716deb810_0 1.0 v0x7fd716de7f10_0))
                (= v0x7fd716decc50_0 (= v0x7fd716dea0d0_0 0.0))
                (= v0x7fd716decd50_0 (= v0x7fd716debdd0_0 2.0))
                (= v0x7fd716dece90_0 (= v0x7fd716debe90_0 0.0))
                (= v0x7fd716decfd0_0 (and v0x7fd716decd50_0 v0x7fd716decc50_0))
                (= v0x7fd716ded110_0 (and v0x7fd716decfd0_0 v0x7fd716dece90_0)))))
  (=> F0x7fd716dedf10 a!7))))
(assert (=> F0x7fd716dedf10 F0x7fd716dede50))
(assert (=> F0x7fd716dee050 (or F0x7fd716dedd90 F0x7fd716dedcd0)))
(assert (=> F0x7fd716dee010 F0x7fd716dedf10))
(assert (=> pre!entry!0 (=> F0x7fd716dedc90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd716dede50 (>= v0x7fd716de7f10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7fd716dede50 (>= v0x7fd716de8090_0 0.0))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fd716dede50 (not (<= 3.0 v0x7fd716de8190_0)))))
(assert (or (and (not post!bb1.i.i!0) F0x7fd716dee050 (not (>= v0x7fd716de8150_0 0.0)))
    (and (not post!bb1.i.i!1) F0x7fd716dee050 (not (>= v0x7fd716de8250_0 0.0)))
    (and (not post!bb1.i.i!2) F0x7fd716dee050 (<= 3.0 v0x7fd716de6010_0))
    (and (not post!bb2.i.i34.i.i!0) F0x7fd716dee010 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb2.i.i34.i.i!0)
