(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun F0x7f634c2169d0 () Bool)
(declare-fun v0x7f634c2141d0_0 () Real)
(declare-fun v0x7f634c214090_0 () Bool)
(declare-fun v0x7f634c213b50_0 () Bool)
(declare-fun F0x7f634c216dd0 () Bool)
(declare-fun v0x7f634c215c10_0 () Bool)
(declare-fun v0x7f634c215590_0 () Bool)
(declare-fun v0x7f634c215950_0 () Bool)
(declare-fun E0x7f634c215050 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f634c210c10_0 () Real)
(declare-fun v0x7f634c214390_0 () Real)
(declare-fun E0x7f634c214d90 () Bool)
(declare-fun v0x7f634c211e10_0 () Real)
(declare-fun v0x7f634c213c90_0 () Real)
(declare-fun v0x7f634c214a10_0 () Real)
(declare-fun v0x7f634c214950_0 () Real)
(declare-fun v0x7f634c214890_0 () Bool)
(declare-fun v0x7f634c213790_0 () Bool)
(declare-fun E0x7f634c213290 () Bool)
(declare-fun v0x7f634c213010_0 () Real)
(declare-fun F0x7f634c216ad0 () Bool)
(declare-fun v0x7f634c212a10_0 () Bool)
(declare-fun post!bb2.i.i26.i.i!0 () Bool)
(declare-fun v0x7f634c210e90_0 () Real)
(declare-fun v0x7f634c210f90_0 () Real)
(declare-fun v0x7f634c212f50_0 () Bool)
(declare-fun v0x7f634c212150_0 () Real)
(declare-fun v0x7f634c213dd0_0 () Bool)
(declare-fun v0x7f634c211810_0 () Bool)
(declare-fun E0x7f634c211a10 () Bool)
(declare-fun v0x7f634c212810_0 () Real)
(declare-fun v0x7f634c211750_0 () Real)
(declare-fun v0x7f634c211690_0 () Bool)
(declare-fun v0x7f634c210d90_0 () Real)
(declare-fun E0x7f634c214ad0 () Bool)
(declare-fun F0x7f634c216c10 () Bool)
(declare-fun v0x7f634c215810_0 () Bool)
(declare-fun v0x7f634c20f010_0 () Real)
(declare-fun v0x7f634c211f50_0 () Real)
(declare-fun E0x7f634c212ad0 () Bool)
(declare-fun E0x7f634c212210 () Bool)
(declare-fun v0x7f634c211050_0 () Real)
(declare-fun v0x7f634c2138d0_0 () Bool)
(declare-fun v0x7f634c210f50_0 () Real)
(declare-fun F0x7f634c216b90 () Bool)
(declare-fun E0x7f634c2123d0 () Bool)
(declare-fun v0x7f634c212090_0 () Bool)
(declare-fun v0x7f634c2128d0_0 () Bool)
(declare-fun v0x7f634c211950_0 () Bool)
(declare-fun E0x7f634c213e90 () Bool)
(declare-fun v0x7f634c212e10_0 () Real)
(declare-fun v0x7f634c210e50_0 () Real)
(declare-fun v0x7f634c212cd0_0 () Bool)
(declare-fun E0x7f634c213990 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun v0x7f634c20f110_0 () Bool)
(declare-fun v0x7f634c214610_0 () Bool)
(declare-fun v0x7f634c2144d0_0 () Bool)
(declare-fun F0x7f634c216d90 () Bool)
(declare-fun E0x7f634c2146d0 () Bool)
(declare-fun E0x7f634c2130d0 () Bool)
(declare-fun F0x7f634c216cd0 () Bool)
(declare-fun v0x7f634c211c50_0 () Bool)
(declare-fun v0x7f634c2156d0_0 () Bool)
(declare-fun E0x7f634c215a10 () Bool)

(assert (=> F0x7f634c216cd0
    (and v0x7f634c20f110_0
         (<= v0x7f634c210e50_0 0.0)
         (>= v0x7f634c210e50_0 0.0)
         (<= v0x7f634c210f50_0 0.0)
         (>= v0x7f634c210f50_0 0.0)
         (<= v0x7f634c211050_0 1.0)
         (>= v0x7f634c211050_0 1.0)
         (<= v0x7f634c20f010_0 0.0)
         (>= v0x7f634c20f010_0 0.0))))
(assert (=> F0x7f634c216cd0 F0x7f634c216c10))
(assert (let ((a!1 (=> v0x7f634c212090_0
               (or (and v0x7f634c211950_0
                        E0x7f634c212210
                        (<= v0x7f634c212150_0 v0x7f634c211f50_0)
                        (>= v0x7f634c212150_0 v0x7f634c211f50_0))
                   (and v0x7f634c211690_0
                        E0x7f634c2123d0
                        v0x7f634c211810_0
                        (<= v0x7f634c212150_0 v0x7f634c210e90_0)
                        (>= v0x7f634c212150_0 v0x7f634c210e90_0)))))
      (a!2 (=> v0x7f634c212090_0
               (or (and E0x7f634c212210 (not E0x7f634c2123d0))
                   (and E0x7f634c2123d0 (not E0x7f634c212210)))))
      (a!3 (=> v0x7f634c212f50_0
               (or (and v0x7f634c212a10_0
                        E0x7f634c2130d0
                        (<= v0x7f634c213010_0 v0x7f634c212e10_0)
                        (>= v0x7f634c213010_0 v0x7f634c212e10_0))
                   (and v0x7f634c212090_0
                        E0x7f634c213290
                        v0x7f634c2128d0_0
                        (<= v0x7f634c213010_0 v0x7f634c210d90_0)
                        (>= v0x7f634c213010_0 v0x7f634c210d90_0)))))
      (a!4 (=> v0x7f634c212f50_0
               (or (and E0x7f634c2130d0 (not E0x7f634c213290))
                   (and E0x7f634c213290 (not E0x7f634c2130d0)))))
      (a!5 (or (and v0x7f634c2138d0_0
                    E0x7f634c214ad0
                    (<= v0x7f634c214950_0 v0x7f634c212150_0)
                    (>= v0x7f634c214950_0 v0x7f634c212150_0)
                    (<= v0x7f634c214a10_0 v0x7f634c213c90_0)
                    (>= v0x7f634c214a10_0 v0x7f634c213c90_0))
               (and v0x7f634c214610_0
                    E0x7f634c214d90
                    (and (<= v0x7f634c214950_0 v0x7f634c214390_0)
                         (>= v0x7f634c214950_0 v0x7f634c214390_0))
                    (<= v0x7f634c214a10_0 v0x7f634c210c10_0)
                    (>= v0x7f634c214a10_0 v0x7f634c210c10_0))
               (and v0x7f634c213dd0_0
                    E0x7f634c215050
                    (not v0x7f634c2144d0_0)
                    (and (<= v0x7f634c214950_0 v0x7f634c214390_0)
                         (>= v0x7f634c214950_0 v0x7f634c214390_0))
                    (<= v0x7f634c214a10_0 0.0)
                    (>= v0x7f634c214a10_0 0.0))))
      (a!6 (=> v0x7f634c214890_0
               (or (and E0x7f634c214ad0
                        (not E0x7f634c214d90)
                        (not E0x7f634c215050))
                   (and E0x7f634c214d90
                        (not E0x7f634c214ad0)
                        (not E0x7f634c215050))
                   (and E0x7f634c215050
                        (not E0x7f634c214ad0)
                        (not E0x7f634c214d90)))))
      (a!7 (or (and v0x7f634c215950_0
                    v0x7f634c215c10_0
                    (and (<= v0x7f634c210e50_0 v0x7f634c214a10_0)
                         (>= v0x7f634c210e50_0 v0x7f634c214a10_0))
                    (and (<= v0x7f634c210f50_0 v0x7f634c213010_0)
                         (>= v0x7f634c210f50_0 v0x7f634c213010_0))
                    (and (<= v0x7f634c211050_0 v0x7f634c214950_0)
                         (>= v0x7f634c211050_0 v0x7f634c214950_0))
                    (<= v0x7f634c20f010_0 1.0)
                    (>= v0x7f634c20f010_0 1.0))
               (and v0x7f634c214890_0
                    v0x7f634c215810_0
                    (and (<= v0x7f634c210e50_0 v0x7f634c214a10_0)
                         (>= v0x7f634c210e50_0 v0x7f634c214a10_0))
                    (and (<= v0x7f634c210f50_0 v0x7f634c213010_0)
                         (>= v0x7f634c210f50_0 v0x7f634c213010_0))
                    (and (<= v0x7f634c211050_0 v0x7f634c214950_0)
                         (>= v0x7f634c211050_0 v0x7f634c214950_0))
                    (<= v0x7f634c20f010_0 0.0)
                    (>= v0x7f634c20f010_0 0.0)))))
(let ((a!8 (and (=> v0x7f634c211950_0
                    (and v0x7f634c211690_0
                         E0x7f634c211a10
                         (not v0x7f634c211810_0)))
                (=> v0x7f634c211950_0 E0x7f634c211a10)
                a!1
                a!2
                (=> v0x7f634c212a10_0
                    (and v0x7f634c212090_0
                         E0x7f634c212ad0
                         (not v0x7f634c2128d0_0)))
                (=> v0x7f634c212a10_0 E0x7f634c212ad0)
                a!3
                a!4
                (=> v0x7f634c2138d0_0
                    (and v0x7f634c212f50_0 E0x7f634c213990 v0x7f634c213790_0))
                (=> v0x7f634c2138d0_0 E0x7f634c213990)
                (=> v0x7f634c213dd0_0
                    (and v0x7f634c212f50_0
                         E0x7f634c213e90
                         (not v0x7f634c213790_0)))
                (=> v0x7f634c213dd0_0 E0x7f634c213e90)
                (=> v0x7f634c214610_0
                    (and v0x7f634c213dd0_0 E0x7f634c2146d0 v0x7f634c2144d0_0))
                (=> v0x7f634c214610_0 E0x7f634c2146d0)
                (=> v0x7f634c214890_0 a!5)
                a!6
                (=> v0x7f634c215950_0
                    (and v0x7f634c214890_0
                         E0x7f634c215a10
                         (not v0x7f634c215810_0)))
                (=> v0x7f634c215950_0 E0x7f634c215a10)
                a!7
                (= v0x7f634c211810_0 (= v0x7f634c211750_0 0.0))
                (= v0x7f634c211c50_0 (< v0x7f634c210e90_0 2.0))
                (= v0x7f634c211e10_0 (ite v0x7f634c211c50_0 1.0 0.0))
                (= v0x7f634c211f50_0 (+ v0x7f634c211e10_0 v0x7f634c210e90_0))
                (= v0x7f634c2128d0_0 (= v0x7f634c212810_0 0.0))
                (= v0x7f634c212cd0_0 (= v0x7f634c210d90_0 0.0))
                (= v0x7f634c212e10_0 (ite v0x7f634c212cd0_0 1.0 0.0))
                (= v0x7f634c213790_0 (= v0x7f634c210c10_0 0.0))
                (= v0x7f634c213b50_0 (> v0x7f634c212150_0 1.0))
                (= v0x7f634c213c90_0
                   (ite v0x7f634c213b50_0 1.0 v0x7f634c210c10_0))
                (= v0x7f634c214090_0 (> v0x7f634c212150_0 0.0))
                (= v0x7f634c2141d0_0 (+ v0x7f634c212150_0 (- 1.0)))
                (= v0x7f634c214390_0
                   (ite v0x7f634c214090_0 v0x7f634c2141d0_0 v0x7f634c212150_0))
                (= v0x7f634c2144d0_0 (= v0x7f634c214390_0 0.0))
                (= v0x7f634c215590_0 (= v0x7f634c213010_0 0.0))
                (= v0x7f634c2156d0_0 (= v0x7f634c214a10_0 0.0))
                (= v0x7f634c215810_0 (or v0x7f634c2156d0_0 v0x7f634c215590_0))
                (= v0x7f634c215c10_0 (= v0x7f634c210f90_0 0.0)))))
  (=> F0x7f634c216b90 a!8))))
(assert (=> F0x7f634c216b90 F0x7f634c216ad0))
(assert (let ((a!1 (=> v0x7f634c212090_0
               (or (and v0x7f634c211950_0
                        E0x7f634c212210
                        (<= v0x7f634c212150_0 v0x7f634c211f50_0)
                        (>= v0x7f634c212150_0 v0x7f634c211f50_0))
                   (and v0x7f634c211690_0
                        E0x7f634c2123d0
                        v0x7f634c211810_0
                        (<= v0x7f634c212150_0 v0x7f634c210e90_0)
                        (>= v0x7f634c212150_0 v0x7f634c210e90_0)))))
      (a!2 (=> v0x7f634c212090_0
               (or (and E0x7f634c212210 (not E0x7f634c2123d0))
                   (and E0x7f634c2123d0 (not E0x7f634c212210)))))
      (a!3 (=> v0x7f634c212f50_0
               (or (and v0x7f634c212a10_0
                        E0x7f634c2130d0
                        (<= v0x7f634c213010_0 v0x7f634c212e10_0)
                        (>= v0x7f634c213010_0 v0x7f634c212e10_0))
                   (and v0x7f634c212090_0
                        E0x7f634c213290
                        v0x7f634c2128d0_0
                        (<= v0x7f634c213010_0 v0x7f634c210d90_0)
                        (>= v0x7f634c213010_0 v0x7f634c210d90_0)))))
      (a!4 (=> v0x7f634c212f50_0
               (or (and E0x7f634c2130d0 (not E0x7f634c213290))
                   (and E0x7f634c213290 (not E0x7f634c2130d0)))))
      (a!5 (or (and v0x7f634c2138d0_0
                    E0x7f634c214ad0
                    (<= v0x7f634c214950_0 v0x7f634c212150_0)
                    (>= v0x7f634c214950_0 v0x7f634c212150_0)
                    (<= v0x7f634c214a10_0 v0x7f634c213c90_0)
                    (>= v0x7f634c214a10_0 v0x7f634c213c90_0))
               (and v0x7f634c214610_0
                    E0x7f634c214d90
                    (and (<= v0x7f634c214950_0 v0x7f634c214390_0)
                         (>= v0x7f634c214950_0 v0x7f634c214390_0))
                    (<= v0x7f634c214a10_0 v0x7f634c210c10_0)
                    (>= v0x7f634c214a10_0 v0x7f634c210c10_0))
               (and v0x7f634c213dd0_0
                    E0x7f634c215050
                    (not v0x7f634c2144d0_0)
                    (and (<= v0x7f634c214950_0 v0x7f634c214390_0)
                         (>= v0x7f634c214950_0 v0x7f634c214390_0))
                    (<= v0x7f634c214a10_0 0.0)
                    (>= v0x7f634c214a10_0 0.0))))
      (a!6 (=> v0x7f634c214890_0
               (or (and E0x7f634c214ad0
                        (not E0x7f634c214d90)
                        (not E0x7f634c215050))
                   (and E0x7f634c214d90
                        (not E0x7f634c214ad0)
                        (not E0x7f634c215050))
                   (and E0x7f634c215050
                        (not E0x7f634c214ad0)
                        (not E0x7f634c214d90))))))
(let ((a!7 (and (=> v0x7f634c211950_0
                    (and v0x7f634c211690_0
                         E0x7f634c211a10
                         (not v0x7f634c211810_0)))
                (=> v0x7f634c211950_0 E0x7f634c211a10)
                a!1
                a!2
                (=> v0x7f634c212a10_0
                    (and v0x7f634c212090_0
                         E0x7f634c212ad0
                         (not v0x7f634c2128d0_0)))
                (=> v0x7f634c212a10_0 E0x7f634c212ad0)
                a!3
                a!4
                (=> v0x7f634c2138d0_0
                    (and v0x7f634c212f50_0 E0x7f634c213990 v0x7f634c213790_0))
                (=> v0x7f634c2138d0_0 E0x7f634c213990)
                (=> v0x7f634c213dd0_0
                    (and v0x7f634c212f50_0
                         E0x7f634c213e90
                         (not v0x7f634c213790_0)))
                (=> v0x7f634c213dd0_0 E0x7f634c213e90)
                (=> v0x7f634c214610_0
                    (and v0x7f634c213dd0_0 E0x7f634c2146d0 v0x7f634c2144d0_0))
                (=> v0x7f634c214610_0 E0x7f634c2146d0)
                (=> v0x7f634c214890_0 a!5)
                a!6
                (=> v0x7f634c215950_0
                    (and v0x7f634c214890_0
                         E0x7f634c215a10
                         (not v0x7f634c215810_0)))
                (=> v0x7f634c215950_0 E0x7f634c215a10)
                v0x7f634c215950_0
                (not v0x7f634c215c10_0)
                (= v0x7f634c211810_0 (= v0x7f634c211750_0 0.0))
                (= v0x7f634c211c50_0 (< v0x7f634c210e90_0 2.0))
                (= v0x7f634c211e10_0 (ite v0x7f634c211c50_0 1.0 0.0))
                (= v0x7f634c211f50_0 (+ v0x7f634c211e10_0 v0x7f634c210e90_0))
                (= v0x7f634c2128d0_0 (= v0x7f634c212810_0 0.0))
                (= v0x7f634c212cd0_0 (= v0x7f634c210d90_0 0.0))
                (= v0x7f634c212e10_0 (ite v0x7f634c212cd0_0 1.0 0.0))
                (= v0x7f634c213790_0 (= v0x7f634c210c10_0 0.0))
                (= v0x7f634c213b50_0 (> v0x7f634c212150_0 1.0))
                (= v0x7f634c213c90_0
                   (ite v0x7f634c213b50_0 1.0 v0x7f634c210c10_0))
                (= v0x7f634c214090_0 (> v0x7f634c212150_0 0.0))
                (= v0x7f634c2141d0_0 (+ v0x7f634c212150_0 (- 1.0)))
                (= v0x7f634c214390_0
                   (ite v0x7f634c214090_0 v0x7f634c2141d0_0 v0x7f634c212150_0))
                (= v0x7f634c2144d0_0 (= v0x7f634c214390_0 0.0))
                (= v0x7f634c215590_0 (= v0x7f634c213010_0 0.0))
                (= v0x7f634c2156d0_0 (= v0x7f634c214a10_0 0.0))
                (= v0x7f634c215810_0 (or v0x7f634c2156d0_0 v0x7f634c215590_0))
                (= v0x7f634c215c10_0 (= v0x7f634c210f90_0 0.0)))))
  (=> F0x7f634c2169d0 a!7))))
(assert (=> F0x7f634c2169d0 F0x7f634c216ad0))
(assert (=> F0x7f634c216dd0 (or F0x7f634c216cd0 F0x7f634c216b90)))
(assert (=> F0x7f634c216d90 F0x7f634c2169d0))
(assert (=> pre!entry!0 (=> F0x7f634c216c10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f634c216ad0 (>= v0x7f634c210f90_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f634c216ad0
        (or (>= v0x7f634c210e90_0 0.0) (<= v0x7f634c210f90_0 0.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f634c216ad0 (>= v0x7f634c210c10_0 0.0))))
(assert (let ((a!1 (=> F0x7f634c216ad0
               (or (<= v0x7f634c210c10_0 0.0) (not (<= v0x7f634c210e90_0 1.0))))))
  (=> pre!bb1.i.i!3 a!1)))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f634c216ad0
        (or (<= v0x7f634c210f90_0 0.0) (>= v0x7f634c210c10_0 1.0)))))
(assert (=> pre!bb1.i.i!5 (=> F0x7f634c216ad0 (not (<= v0x7f634c210e90_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f634c216dd0
                (not (or (>= v0x7f634c211050_0 0.0) (<= v0x7f634c20f010_0 0.0)))))
      (a!2 (not (or (<= v0x7f634c210e50_0 0.0) (not (<= v0x7f634c211050_0 1.0)))))
      (a!3 (and (not post!bb1.i.i!4)
                F0x7f634c216dd0
                (not (or (<= v0x7f634c20f010_0 0.0) (>= v0x7f634c210e50_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f634c216dd0
           (not (>= v0x7f634c20f010_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f634c216dd0
           (not (>= v0x7f634c210e50_0 0.0)))
      (and (not post!bb1.i.i!3) F0x7f634c216dd0 a!2)
      a!3
      (and (not post!bb1.i.i!5) F0x7f634c216dd0 (<= v0x7f634c211050_0 0.0))
      (and (not post!bb2.i.i26.i.i!0) F0x7f634c216d90 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i26.i.i!0)
