(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f07eedfbe90 () Bool)
(declare-fun v0x7f07eedfaf10_0 () Bool)
(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun v0x7f07eedfadd0_0 () Bool)
(declare-fun v0x7f07eedf9050_0 () Bool)
(declare-fun v0x7f07eedf8190_0 () Bool)
(declare-fun v0x7f07eedf72d0_0 () Real)
(declare-fun F0x7f07eedfbf50 () Bool)
(declare-fun v0x7f07eedf7cd0_0 () Real)
(declare-fun v0x7f07eedfa110_0 () Bool)
(declare-fun v0x7f07eedf6c10_0 () Real)
(declare-fun v0x7f07eedfa450_0 () Real)
(declare-fun E0x7f07eedf9f50 () Bool)
(declare-fun v0x7f07eedf9e90_0 () Bool)
(declare-fun E0x7f07eedfa510 () Bool)
(declare-fun v0x7f07eedf7110_0 () Bool)
(declare-fun E0x7f07eedf97d0 () Bool)
(declare-fun v0x7f07eedf9350_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f07eedfac90_0 () Bool)
(declare-fun v0x7f07eedf9490_0 () Bool)
(declare-fun v0x7f07eedf8c50_0 () Bool)
(declare-fun v0x7f07eedf6490_0 () Real)
(declare-fun v0x7f07eedfb050_0 () Bool)
(declare-fun E0x7f07eedf8750 () Bool)
(declare-fun v0x7f07eedfa250_0 () Real)
(declare-fun v0x7f07eedfab50_0 () Bool)
(declare-fun v0x7f07eedf84d0_0 () Real)
(declare-fun v0x7f07eedf8410_0 () Bool)
(declare-fun E0x7f07eedf7f90 () Bool)
(declare-fun E0x7f07eedfa6d0 () Bool)
(declare-fun v0x7f07eedf82d0_0 () Real)
(declare-fun v0x7f07eedf9190_0 () Real)
(declare-fun F0x7f07eedfbdd0 () Bool)
(declare-fun v0x7f07eedf9d50_0 () Bool)
(declare-fun v0x7f07eedf9c10_0 () Bool)
(declare-fun v0x7f07eedf7ed0_0 () Bool)
(declare-fun v0x7f07eedf6590_0 () Real)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun F0x7f07eedfbf90 () Bool)
(declare-fun v0x7f07eedf8d90_0 () Bool)
(declare-fun v0x7f07eedfa390_0 () Bool)
(declare-fun v0x7f07eedf6310_0 () Real)
(declare-fun E0x7f07eedf76d0 () Bool)
(declare-fun v0x7f07eedf6cd0_0 () Bool)
(declare-fun E0x7f07eedf7890 () Bool)
(declare-fun E0x7f07eedf6ed0 () Bool)
(declare-fun v0x7f07eedf7410_0 () Real)
(declare-fun v0x7f07eedf7550_0 () Bool)
(declare-fun v0x7f07eedf6e10_0 () Bool)
(declare-fun F0x7f07eedfbc10 () Bool)
(declare-fun F0x7f07eedfbcd0 () Bool)
(declare-fun E0x7f07eedf8e50 () Bool)
(declare-fun v0x7f07eedf9550_0 () Real)
(declare-fun v0x7f07eedf6650_0 () Real)
(declare-fun v0x7f07eedf7610_0 () Real)
(declare-fun E0x7f07eedf9610 () Bool)
(declare-fun v0x7f07eedf6550_0 () Real)
(declare-fun v0x7f07eedf6b50_0 () Bool)
(declare-fun E0x7f07eedf8590 () Bool)
(declare-fun v0x7f07eedf5010_0 () Real)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f07eedf5110_0 () Bool)
(declare-fun v0x7f07eedf7d90_0 () Bool)
(declare-fun F0x7f07eedfbd90 () Bool)

(assert (=> F0x7f07eedfbd90
    (and v0x7f07eedf5110_0
         (<= v0x7f07eedf6550_0 0.0)
         (>= v0x7f07eedf6550_0 0.0)
         (<= v0x7f07eedf6650_0 0.0)
         (>= v0x7f07eedf6650_0 0.0)
         (<= v0x7f07eedf5010_0 1.0)
         (>= v0x7f07eedf5010_0 1.0))))
(assert (=> F0x7f07eedfbd90 F0x7f07eedfbcd0))
(assert (let ((a!1 (=> v0x7f07eedf7550_0
               (or (and v0x7f07eedf6e10_0
                        E0x7f07eedf76d0
                        (<= v0x7f07eedf7610_0 v0x7f07eedf7410_0)
                        (>= v0x7f07eedf7610_0 v0x7f07eedf7410_0))
                   (and v0x7f07eedf6b50_0
                        E0x7f07eedf7890
                        v0x7f07eedf6cd0_0
                        (<= v0x7f07eedf7610_0 v0x7f07eedf6590_0)
                        (>= v0x7f07eedf7610_0 v0x7f07eedf6590_0)))))
      (a!2 (=> v0x7f07eedf7550_0
               (or (and E0x7f07eedf76d0 (not E0x7f07eedf7890))
                   (and E0x7f07eedf7890 (not E0x7f07eedf76d0)))))
      (a!3 (=> v0x7f07eedf8410_0
               (or (and v0x7f07eedf7ed0_0
                        E0x7f07eedf8590
                        (<= v0x7f07eedf84d0_0 v0x7f07eedf82d0_0)
                        (>= v0x7f07eedf84d0_0 v0x7f07eedf82d0_0))
                   (and v0x7f07eedf7550_0
                        E0x7f07eedf8750
                        v0x7f07eedf7d90_0
                        (<= v0x7f07eedf84d0_0 v0x7f07eedf6490_0)
                        (>= v0x7f07eedf84d0_0 v0x7f07eedf6490_0)))))
      (a!4 (=> v0x7f07eedf8410_0
               (or (and E0x7f07eedf8590 (not E0x7f07eedf8750))
                   (and E0x7f07eedf8750 (not E0x7f07eedf8590)))))
      (a!5 (=> v0x7f07eedf9490_0
               (or (and v0x7f07eedf8d90_0
                        E0x7f07eedf9610
                        (<= v0x7f07eedf9550_0 v0x7f07eedf9350_0)
                        (>= v0x7f07eedf9550_0 v0x7f07eedf9350_0))
                   (and v0x7f07eedf8410_0
                        E0x7f07eedf97d0
                        v0x7f07eedf8c50_0
                        (<= v0x7f07eedf9550_0 v0x7f07eedf7610_0)
                        (>= v0x7f07eedf9550_0 v0x7f07eedf7610_0)))))
      (a!6 (=> v0x7f07eedf9490_0
               (or (and E0x7f07eedf9610 (not E0x7f07eedf97d0))
                   (and E0x7f07eedf97d0 (not E0x7f07eedf9610)))))
      (a!7 (=> v0x7f07eedfa390_0
               (or (and v0x7f07eedf9e90_0
                        E0x7f07eedfa510
                        (<= v0x7f07eedfa450_0 v0x7f07eedfa250_0)
                        (>= v0x7f07eedfa450_0 v0x7f07eedfa250_0))
                   (and v0x7f07eedf9490_0
                        E0x7f07eedfa6d0
                        (not v0x7f07eedf9d50_0)
                        (<= v0x7f07eedfa450_0 v0x7f07eedf6310_0)
                        (>= v0x7f07eedfa450_0 v0x7f07eedf6310_0)))))
      (a!8 (=> v0x7f07eedfa390_0
               (or (and E0x7f07eedfa510 (not E0x7f07eedfa6d0))
                   (and E0x7f07eedfa6d0 (not E0x7f07eedfa510))))))
(let ((a!9 (and (=> v0x7f07eedf6e10_0
                    (and v0x7f07eedf6b50_0
                         E0x7f07eedf6ed0
                         (not v0x7f07eedf6cd0_0)))
                (=> v0x7f07eedf6e10_0 E0x7f07eedf6ed0)
                a!1
                a!2
                (=> v0x7f07eedf7ed0_0
                    (and v0x7f07eedf7550_0
                         E0x7f07eedf7f90
                         (not v0x7f07eedf7d90_0)))
                (=> v0x7f07eedf7ed0_0 E0x7f07eedf7f90)
                a!3
                a!4
                (=> v0x7f07eedf8d90_0
                    (and v0x7f07eedf8410_0
                         E0x7f07eedf8e50
                         (not v0x7f07eedf8c50_0)))
                (=> v0x7f07eedf8d90_0 E0x7f07eedf8e50)
                a!5
                a!6
                (=> v0x7f07eedf9e90_0
                    (and v0x7f07eedf9490_0 E0x7f07eedf9f50 v0x7f07eedf9d50_0))
                (=> v0x7f07eedf9e90_0 E0x7f07eedf9f50)
                a!7
                a!8
                v0x7f07eedfa390_0
                (not v0x7f07eedfb050_0)
                (<= v0x7f07eedf6550_0 v0x7f07eedfa450_0)
                (>= v0x7f07eedf6550_0 v0x7f07eedfa450_0)
                (<= v0x7f07eedf6650_0 v0x7f07eedf84d0_0)
                (>= v0x7f07eedf6650_0 v0x7f07eedf84d0_0)
                (<= v0x7f07eedf5010_0 v0x7f07eedf9550_0)
                (>= v0x7f07eedf5010_0 v0x7f07eedf9550_0)
                (= v0x7f07eedf6cd0_0 (= v0x7f07eedf6c10_0 0.0))
                (= v0x7f07eedf7110_0 (< v0x7f07eedf6590_0 2.0))
                (= v0x7f07eedf72d0_0 (ite v0x7f07eedf7110_0 1.0 0.0))
                (= v0x7f07eedf7410_0 (+ v0x7f07eedf72d0_0 v0x7f07eedf6590_0))
                (= v0x7f07eedf7d90_0 (= v0x7f07eedf7cd0_0 0.0))
                (= v0x7f07eedf8190_0 (= v0x7f07eedf6490_0 0.0))
                (= v0x7f07eedf82d0_0 (ite v0x7f07eedf8190_0 1.0 0.0))
                (= v0x7f07eedf8c50_0 (= v0x7f07eedf6310_0 0.0))
                (= v0x7f07eedf9050_0 (> v0x7f07eedf7610_0 0.0))
                (= v0x7f07eedf9190_0 (+ v0x7f07eedf7610_0 (- 1.0)))
                (= v0x7f07eedf9350_0
                   (ite v0x7f07eedf9050_0 v0x7f07eedf9190_0 v0x7f07eedf7610_0))
                (= v0x7f07eedf9c10_0 (> v0x7f07eedf9550_0 1.0))
                (= v0x7f07eedf9d50_0 (and v0x7f07eedf9c10_0 v0x7f07eedf8c50_0))
                (= v0x7f07eedfa110_0 (= v0x7f07eedf84d0_0 0.0))
                (= v0x7f07eedfa250_0
                   (ite v0x7f07eedfa110_0 1.0 v0x7f07eedf6310_0))
                (= v0x7f07eedfab50_0 (= v0x7f07eedf9550_0 2.0))
                (= v0x7f07eedfac90_0 (= v0x7f07eedfa450_0 0.0))
                (= v0x7f07eedfadd0_0 (or v0x7f07eedfac90_0 v0x7f07eedfab50_0))
                (= v0x7f07eedfaf10_0 (xor v0x7f07eedfadd0_0 true))
                (= v0x7f07eedfb050_0 (and v0x7f07eedf8c50_0 v0x7f07eedfaf10_0)))))
  (=> F0x7f07eedfbc10 a!9))))
(assert (=> F0x7f07eedfbc10 F0x7f07eedfbdd0))
(assert (let ((a!1 (=> v0x7f07eedf7550_0
               (or (and v0x7f07eedf6e10_0
                        E0x7f07eedf76d0
                        (<= v0x7f07eedf7610_0 v0x7f07eedf7410_0)
                        (>= v0x7f07eedf7610_0 v0x7f07eedf7410_0))
                   (and v0x7f07eedf6b50_0
                        E0x7f07eedf7890
                        v0x7f07eedf6cd0_0
                        (<= v0x7f07eedf7610_0 v0x7f07eedf6590_0)
                        (>= v0x7f07eedf7610_0 v0x7f07eedf6590_0)))))
      (a!2 (=> v0x7f07eedf7550_0
               (or (and E0x7f07eedf76d0 (not E0x7f07eedf7890))
                   (and E0x7f07eedf7890 (not E0x7f07eedf76d0)))))
      (a!3 (=> v0x7f07eedf8410_0
               (or (and v0x7f07eedf7ed0_0
                        E0x7f07eedf8590
                        (<= v0x7f07eedf84d0_0 v0x7f07eedf82d0_0)
                        (>= v0x7f07eedf84d0_0 v0x7f07eedf82d0_0))
                   (and v0x7f07eedf7550_0
                        E0x7f07eedf8750
                        v0x7f07eedf7d90_0
                        (<= v0x7f07eedf84d0_0 v0x7f07eedf6490_0)
                        (>= v0x7f07eedf84d0_0 v0x7f07eedf6490_0)))))
      (a!4 (=> v0x7f07eedf8410_0
               (or (and E0x7f07eedf8590 (not E0x7f07eedf8750))
                   (and E0x7f07eedf8750 (not E0x7f07eedf8590)))))
      (a!5 (=> v0x7f07eedf9490_0
               (or (and v0x7f07eedf8d90_0
                        E0x7f07eedf9610
                        (<= v0x7f07eedf9550_0 v0x7f07eedf9350_0)
                        (>= v0x7f07eedf9550_0 v0x7f07eedf9350_0))
                   (and v0x7f07eedf8410_0
                        E0x7f07eedf97d0
                        v0x7f07eedf8c50_0
                        (<= v0x7f07eedf9550_0 v0x7f07eedf7610_0)
                        (>= v0x7f07eedf9550_0 v0x7f07eedf7610_0)))))
      (a!6 (=> v0x7f07eedf9490_0
               (or (and E0x7f07eedf9610 (not E0x7f07eedf97d0))
                   (and E0x7f07eedf97d0 (not E0x7f07eedf9610)))))
      (a!7 (=> v0x7f07eedfa390_0
               (or (and v0x7f07eedf9e90_0
                        E0x7f07eedfa510
                        (<= v0x7f07eedfa450_0 v0x7f07eedfa250_0)
                        (>= v0x7f07eedfa450_0 v0x7f07eedfa250_0))
                   (and v0x7f07eedf9490_0
                        E0x7f07eedfa6d0
                        (not v0x7f07eedf9d50_0)
                        (<= v0x7f07eedfa450_0 v0x7f07eedf6310_0)
                        (>= v0x7f07eedfa450_0 v0x7f07eedf6310_0)))))
      (a!8 (=> v0x7f07eedfa390_0
               (or (and E0x7f07eedfa510 (not E0x7f07eedfa6d0))
                   (and E0x7f07eedfa6d0 (not E0x7f07eedfa510))))))
(let ((a!9 (and (=> v0x7f07eedf6e10_0
                    (and v0x7f07eedf6b50_0
                         E0x7f07eedf6ed0
                         (not v0x7f07eedf6cd0_0)))
                (=> v0x7f07eedf6e10_0 E0x7f07eedf6ed0)
                a!1
                a!2
                (=> v0x7f07eedf7ed0_0
                    (and v0x7f07eedf7550_0
                         E0x7f07eedf7f90
                         (not v0x7f07eedf7d90_0)))
                (=> v0x7f07eedf7ed0_0 E0x7f07eedf7f90)
                a!3
                a!4
                (=> v0x7f07eedf8d90_0
                    (and v0x7f07eedf8410_0
                         E0x7f07eedf8e50
                         (not v0x7f07eedf8c50_0)))
                (=> v0x7f07eedf8d90_0 E0x7f07eedf8e50)
                a!5
                a!6
                (=> v0x7f07eedf9e90_0
                    (and v0x7f07eedf9490_0 E0x7f07eedf9f50 v0x7f07eedf9d50_0))
                (=> v0x7f07eedf9e90_0 E0x7f07eedf9f50)
                a!7
                a!8
                v0x7f07eedfa390_0
                v0x7f07eedfb050_0
                (= v0x7f07eedf6cd0_0 (= v0x7f07eedf6c10_0 0.0))
                (= v0x7f07eedf7110_0 (< v0x7f07eedf6590_0 2.0))
                (= v0x7f07eedf72d0_0 (ite v0x7f07eedf7110_0 1.0 0.0))
                (= v0x7f07eedf7410_0 (+ v0x7f07eedf72d0_0 v0x7f07eedf6590_0))
                (= v0x7f07eedf7d90_0 (= v0x7f07eedf7cd0_0 0.0))
                (= v0x7f07eedf8190_0 (= v0x7f07eedf6490_0 0.0))
                (= v0x7f07eedf82d0_0 (ite v0x7f07eedf8190_0 1.0 0.0))
                (= v0x7f07eedf8c50_0 (= v0x7f07eedf6310_0 0.0))
                (= v0x7f07eedf9050_0 (> v0x7f07eedf7610_0 0.0))
                (= v0x7f07eedf9190_0 (+ v0x7f07eedf7610_0 (- 1.0)))
                (= v0x7f07eedf9350_0
                   (ite v0x7f07eedf9050_0 v0x7f07eedf9190_0 v0x7f07eedf7610_0))
                (= v0x7f07eedf9c10_0 (> v0x7f07eedf9550_0 1.0))
                (= v0x7f07eedf9d50_0 (and v0x7f07eedf9c10_0 v0x7f07eedf8c50_0))
                (= v0x7f07eedfa110_0 (= v0x7f07eedf84d0_0 0.0))
                (= v0x7f07eedfa250_0
                   (ite v0x7f07eedfa110_0 1.0 v0x7f07eedf6310_0))
                (= v0x7f07eedfab50_0 (= v0x7f07eedf9550_0 2.0))
                (= v0x7f07eedfac90_0 (= v0x7f07eedfa450_0 0.0))
                (= v0x7f07eedfadd0_0 (or v0x7f07eedfac90_0 v0x7f07eedfab50_0))
                (= v0x7f07eedfaf10_0 (xor v0x7f07eedfadd0_0 true))
                (= v0x7f07eedfb050_0 (and v0x7f07eedf8c50_0 v0x7f07eedfaf10_0)))))
  (=> F0x7f07eedfbe90 a!9))))
(assert (=> F0x7f07eedfbe90 F0x7f07eedfbdd0))
(assert (=> F0x7f07eedfbf90 (or F0x7f07eedfbd90 F0x7f07eedfbc10)))
(assert (=> F0x7f07eedfbf50 F0x7f07eedfbe90))
(assert (=> pre!entry!0 (=> F0x7f07eedfbcd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f07eedfbdd0 (<= v0x7f07eedf6590_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f07eedfbdd0
        (or (<= v0x7f07eedf6590_0 1.0) (>= v0x7f07eedf6590_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f07eedfbdd0 (>= v0x7f07eedf6590_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f07eedfbdd0
        (or (>= v0x7f07eedf6590_0 1.0) (<= v0x7f07eedf6590_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f07eedfbf90
                (not (or (<= v0x7f07eedf5010_0 1.0) (>= v0x7f07eedf5010_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7f07eedfbf90
                (not (or (>= v0x7f07eedf5010_0 1.0) (<= v0x7f07eedf5010_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f07eedfbf90
           (not (<= v0x7f07eedf5010_0 2.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f07eedfbf90
           (not (>= v0x7f07eedf5010_0 0.0)))
      a!2
      (and (not post!bb2.i.i23.i.i!0) F0x7f07eedfbf50 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i23.i.i!0)
