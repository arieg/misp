(declare-fun post!bb1.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f958daedc10 () Bool)
(declare-fun F0x7f958daedb10 () Bool)
(declare-fun v0x7f958daeb690_0 () Bool)
(declare-fun v0x7f958daeae90_0 () Bool)
(declare-fun v0x7f958dae9c10_0 () Bool)
(declare-fun v0x7f958dae8d50_0 () Real)
(declare-fun v0x7f958daecd10_0 () Bool)
(declare-fun v0x7f958daeb190_0 () Real)
(declare-fun F0x7f958daeda50 () Bool)
(declare-fun E0x7f958daec310 () Bool)
(declare-fun v0x7f958dae8010_0 () Real)
(declare-fun E0x7f958daec090 () Bool)
(declare-fun v0x7f958daeb7d0_0 () Real)
(declare-fun v0x7f958daeb2d0_0 () Bool)
(declare-fun E0x7f958daec510 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7f958daeb9d0 () Bool)
(declare-fun v0x7f958daebb90_0 () Bool)
(declare-fun v0x7f958daeb910_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f958daeaa90_0 () Bool)
(declare-fun E0x7f958daeb4d0 () Bool)
(declare-fun v0x7f958daeb410_0 () Bool)
(declare-fun v0x7f958daeabd0_0 () Bool)
(declare-fun v0x7f958daecbd0_0 () Bool)
(declare-fun E0x7f958daea8d0 () Bool)
(declare-fun v0x7f958daea810_0 () Bool)
(declare-fun E0x7f958daeac90 () Bool)
(declare-fun v0x7f958dae9d50_0 () Real)
(declare-fun v0x7f958daeafd0_0 () Real)
(declare-fun v0x7f958dae9f50_0 () Real)
(declare-fun v0x7f958dae9810_0 () Bool)
(declare-fun E0x7f958dae9a10 () Bool)
(declare-fun v0x7f958dae8e90_0 () Real)
(declare-fun v0x7f958dae9090_0 () Real)
(declare-fun v0x7f958dae9950_0 () Bool)
(declare-fun v0x7f958dae8690_0 () Real)
(declare-fun E0x7f958dae9150 () Bool)
(declare-fun F0x7f958daedc50 () Bool)
(declare-fun v0x7f958dae8fd0_0 () Bool)
(declare-fun E0x7f958dae8950 () Bool)
(declare-fun v0x7f958dae9e90_0 () Bool)
(declare-fun v0x7f958daea6d0_0 () Bool)
(declare-fun v0x7f958daebc50_0 () Real)
(declare-fun v0x7f958dae8750_0 () Bool)
(declare-fun v0x7f958dae85d0_0 () Bool)
(declare-fun v0x7f958dae7d90_0 () Real)
(declare-fun v0x7f958dae9750_0 () Real)
(declare-fun E0x7f958dae9310 () Bool)
(declare-fun F0x7f958daed990 () Bool)
(declare-fun F0x7f958daed890 () Bool)
(declare-fun v0x7f958dae80d0_0 () Real)
(declare-fun v0x7f958dae6010_0 () Real)
(declare-fun v0x7f958daecad0_0 () Bool)
(declare-fun E0x7f958daea1d0 () Bool)
(declare-fun v0x7f958dae8890_0 () Bool)
(declare-fun v0x7f958dae7f10_0 () Real)
(declare-fun E0x7f958daea010 () Bool)
(declare-fun v0x7f958dae8b90_0 () Bool)
(declare-fun v0x7f958dae7fd0_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f958dae6110_0 () Bool)
(declare-fun v0x7f958daebd10_0 () Real)
(declare-fun E0x7f958daebdd0 () Bool)
(declare-fun F0x7f958daed950 () Bool)

(assert (=> F0x7f958daed950
    (and v0x7f958dae6110_0
         (<= v0x7f958dae7fd0_0 0.0)
         (>= v0x7f958dae7fd0_0 0.0)
         (<= v0x7f958dae80d0_0 1.0)
         (>= v0x7f958dae80d0_0 1.0)
         (<= v0x7f958dae6010_0 0.0)
         (>= v0x7f958dae6010_0 0.0))))
(assert (=> F0x7f958daed950 F0x7f958daed890))
(assert (let ((a!1 (=> v0x7f958dae8fd0_0
               (or (and v0x7f958dae8890_0
                        E0x7f958dae9150
                        (<= v0x7f958dae9090_0 v0x7f958dae8e90_0)
                        (>= v0x7f958dae9090_0 v0x7f958dae8e90_0))
                   (and v0x7f958dae85d0_0
                        E0x7f958dae9310
                        v0x7f958dae8750_0
                        (<= v0x7f958dae9090_0 v0x7f958dae7f10_0)
                        (>= v0x7f958dae9090_0 v0x7f958dae7f10_0)))))
      (a!2 (=> v0x7f958dae8fd0_0
               (or (and E0x7f958dae9150 (not E0x7f958dae9310))
                   (and E0x7f958dae9310 (not E0x7f958dae9150)))))
      (a!3 (=> v0x7f958dae9e90_0
               (or (and v0x7f958dae9950_0
                        E0x7f958daea010
                        (<= v0x7f958dae9f50_0 v0x7f958dae9d50_0)
                        (>= v0x7f958dae9f50_0 v0x7f958dae9d50_0))
                   (and v0x7f958dae8fd0_0
                        E0x7f958daea1d0
                        v0x7f958dae9810_0
                        (<= v0x7f958dae9f50_0 v0x7f958dae7d90_0)
                        (>= v0x7f958dae9f50_0 v0x7f958dae7d90_0)))))
      (a!4 (=> v0x7f958dae9e90_0
               (or (and E0x7f958daea010 (not E0x7f958daea1d0))
                   (and E0x7f958daea1d0 (not E0x7f958daea010)))))
      (a!5 (or (and v0x7f958daeb410_0
                    E0x7f958daebdd0
                    (and (<= v0x7f958daebc50_0 v0x7f958dae9090_0)
                         (>= v0x7f958daebc50_0 v0x7f958dae9090_0))
                    (<= v0x7f958daebd10_0 v0x7f958daeb7d0_0)
                    (>= v0x7f958daebd10_0 v0x7f958daeb7d0_0))
               (and v0x7f958daea810_0
                    E0x7f958daec090
                    (not v0x7f958daeaa90_0)
                    (and (<= v0x7f958daebc50_0 v0x7f958dae9090_0)
                         (>= v0x7f958daebc50_0 v0x7f958dae9090_0))
                    (and (<= v0x7f958daebd10_0 v0x7f958dae8010_0)
                         (>= v0x7f958daebd10_0 v0x7f958dae8010_0)))
               (and v0x7f958daeb910_0
                    E0x7f958daec310
                    (and (<= v0x7f958daebc50_0 v0x7f958daeb190_0)
                         (>= v0x7f958daebc50_0 v0x7f958daeb190_0))
                    (and (<= v0x7f958daebd10_0 v0x7f958dae8010_0)
                         (>= v0x7f958daebd10_0 v0x7f958dae8010_0)))
               (and v0x7f958daeabd0_0
                    E0x7f958daec510
                    (not v0x7f958daeb2d0_0)
                    (and (<= v0x7f958daebc50_0 v0x7f958daeb190_0)
                         (>= v0x7f958daebc50_0 v0x7f958daeb190_0))
                    (<= v0x7f958daebd10_0 0.0)
                    (>= v0x7f958daebd10_0 0.0))))
      (a!6 (=> v0x7f958daebb90_0
               (or (and E0x7f958daebdd0
                        (not E0x7f958daec090)
                        (not E0x7f958daec310)
                        (not E0x7f958daec510))
                   (and E0x7f958daec090
                        (not E0x7f958daebdd0)
                        (not E0x7f958daec310)
                        (not E0x7f958daec510))
                   (and E0x7f958daec310
                        (not E0x7f958daebdd0)
                        (not E0x7f958daec090)
                        (not E0x7f958daec510))
                   (and E0x7f958daec510
                        (not E0x7f958daebdd0)
                        (not E0x7f958daec090)
                        (not E0x7f958daec310))))))
(let ((a!7 (and (=> v0x7f958dae8890_0
                    (and v0x7f958dae85d0_0
                         E0x7f958dae8950
                         (not v0x7f958dae8750_0)))
                (=> v0x7f958dae8890_0 E0x7f958dae8950)
                a!1
                a!2
                (=> v0x7f958dae9950_0
                    (and v0x7f958dae8fd0_0
                         E0x7f958dae9a10
                         (not v0x7f958dae9810_0)))
                (=> v0x7f958dae9950_0 E0x7f958dae9a10)
                a!3
                a!4
                (=> v0x7f958daea810_0
                    (and v0x7f958dae9e90_0 E0x7f958daea8d0 v0x7f958daea6d0_0))
                (=> v0x7f958daea810_0 E0x7f958daea8d0)
                (=> v0x7f958daeabd0_0
                    (and v0x7f958dae9e90_0
                         E0x7f958daeac90
                         (not v0x7f958daea6d0_0)))
                (=> v0x7f958daeabd0_0 E0x7f958daeac90)
                (=> v0x7f958daeb410_0
                    (and v0x7f958daea810_0 E0x7f958daeb4d0 v0x7f958daeaa90_0))
                (=> v0x7f958daeb410_0 E0x7f958daeb4d0)
                (=> v0x7f958daeb910_0
                    (and v0x7f958daeabd0_0 E0x7f958daeb9d0 v0x7f958daeb2d0_0))
                (=> v0x7f958daeb910_0 E0x7f958daeb9d0)
                (=> v0x7f958daebb90_0 a!5)
                a!6
                v0x7f958daebb90_0
                v0x7f958daecd10_0
                (<= v0x7f958dae7fd0_0 v0x7f958dae9f50_0)
                (>= v0x7f958dae7fd0_0 v0x7f958dae9f50_0)
                (<= v0x7f958dae80d0_0 v0x7f958daebc50_0)
                (>= v0x7f958dae80d0_0 v0x7f958daebc50_0)
                (<= v0x7f958dae6010_0 v0x7f958daebd10_0)
                (>= v0x7f958dae6010_0 v0x7f958daebd10_0)
                (= v0x7f958dae8750_0 (= v0x7f958dae8690_0 0.0))
                (= v0x7f958dae8b90_0 (< v0x7f958dae7f10_0 2.0))
                (= v0x7f958dae8d50_0 (ite v0x7f958dae8b90_0 1.0 0.0))
                (= v0x7f958dae8e90_0 (+ v0x7f958dae8d50_0 v0x7f958dae7f10_0))
                (= v0x7f958dae9810_0 (= v0x7f958dae9750_0 0.0))
                (= v0x7f958dae9c10_0 (= v0x7f958dae7d90_0 0.0))
                (= v0x7f958dae9d50_0 (ite v0x7f958dae9c10_0 1.0 0.0))
                (= v0x7f958daea6d0_0 (= v0x7f958dae8010_0 0.0))
                (= v0x7f958daeaa90_0 (> v0x7f958dae9090_0 1.0))
                (= v0x7f958daeae90_0 (> v0x7f958dae9090_0 0.0))
                (= v0x7f958daeafd0_0 (+ v0x7f958dae9090_0 (- 1.0)))
                (= v0x7f958daeb190_0
                   (ite v0x7f958daeae90_0 v0x7f958daeafd0_0 v0x7f958dae9090_0))
                (= v0x7f958daeb2d0_0 (= v0x7f958daeb190_0 0.0))
                (= v0x7f958daeb690_0 (= v0x7f958dae9f50_0 0.0))
                (= v0x7f958daeb7d0_0
                   (ite v0x7f958daeb690_0 1.0 v0x7f958dae8010_0))
                (= v0x7f958daecad0_0 (= v0x7f958dae9f50_0 0.0))
                (= v0x7f958daecbd0_0 (= v0x7f958daebd10_0 0.0))
                (= v0x7f958daecd10_0 (or v0x7f958daecbd0_0 v0x7f958daecad0_0)))))
  (=> F0x7f958daed990 a!7))))
(assert (=> F0x7f958daed990 F0x7f958daeda50))
(assert (let ((a!1 (=> v0x7f958dae8fd0_0
               (or (and v0x7f958dae8890_0
                        E0x7f958dae9150
                        (<= v0x7f958dae9090_0 v0x7f958dae8e90_0)
                        (>= v0x7f958dae9090_0 v0x7f958dae8e90_0))
                   (and v0x7f958dae85d0_0
                        E0x7f958dae9310
                        v0x7f958dae8750_0
                        (<= v0x7f958dae9090_0 v0x7f958dae7f10_0)
                        (>= v0x7f958dae9090_0 v0x7f958dae7f10_0)))))
      (a!2 (=> v0x7f958dae8fd0_0
               (or (and E0x7f958dae9150 (not E0x7f958dae9310))
                   (and E0x7f958dae9310 (not E0x7f958dae9150)))))
      (a!3 (=> v0x7f958dae9e90_0
               (or (and v0x7f958dae9950_0
                        E0x7f958daea010
                        (<= v0x7f958dae9f50_0 v0x7f958dae9d50_0)
                        (>= v0x7f958dae9f50_0 v0x7f958dae9d50_0))
                   (and v0x7f958dae8fd0_0
                        E0x7f958daea1d0
                        v0x7f958dae9810_0
                        (<= v0x7f958dae9f50_0 v0x7f958dae7d90_0)
                        (>= v0x7f958dae9f50_0 v0x7f958dae7d90_0)))))
      (a!4 (=> v0x7f958dae9e90_0
               (or (and E0x7f958daea010 (not E0x7f958daea1d0))
                   (and E0x7f958daea1d0 (not E0x7f958daea010)))))
      (a!5 (or (and v0x7f958daeb410_0
                    E0x7f958daebdd0
                    (and (<= v0x7f958daebc50_0 v0x7f958dae9090_0)
                         (>= v0x7f958daebc50_0 v0x7f958dae9090_0))
                    (<= v0x7f958daebd10_0 v0x7f958daeb7d0_0)
                    (>= v0x7f958daebd10_0 v0x7f958daeb7d0_0))
               (and v0x7f958daea810_0
                    E0x7f958daec090
                    (not v0x7f958daeaa90_0)
                    (and (<= v0x7f958daebc50_0 v0x7f958dae9090_0)
                         (>= v0x7f958daebc50_0 v0x7f958dae9090_0))
                    (and (<= v0x7f958daebd10_0 v0x7f958dae8010_0)
                         (>= v0x7f958daebd10_0 v0x7f958dae8010_0)))
               (and v0x7f958daeb910_0
                    E0x7f958daec310
                    (and (<= v0x7f958daebc50_0 v0x7f958daeb190_0)
                         (>= v0x7f958daebc50_0 v0x7f958daeb190_0))
                    (and (<= v0x7f958daebd10_0 v0x7f958dae8010_0)
                         (>= v0x7f958daebd10_0 v0x7f958dae8010_0)))
               (and v0x7f958daeabd0_0
                    E0x7f958daec510
                    (not v0x7f958daeb2d0_0)
                    (and (<= v0x7f958daebc50_0 v0x7f958daeb190_0)
                         (>= v0x7f958daebc50_0 v0x7f958daeb190_0))
                    (<= v0x7f958daebd10_0 0.0)
                    (>= v0x7f958daebd10_0 0.0))))
      (a!6 (=> v0x7f958daebb90_0
               (or (and E0x7f958daebdd0
                        (not E0x7f958daec090)
                        (not E0x7f958daec310)
                        (not E0x7f958daec510))
                   (and E0x7f958daec090
                        (not E0x7f958daebdd0)
                        (not E0x7f958daec310)
                        (not E0x7f958daec510))
                   (and E0x7f958daec310
                        (not E0x7f958daebdd0)
                        (not E0x7f958daec090)
                        (not E0x7f958daec510))
                   (and E0x7f958daec510
                        (not E0x7f958daebdd0)
                        (not E0x7f958daec090)
                        (not E0x7f958daec310))))))
(let ((a!7 (and (=> v0x7f958dae8890_0
                    (and v0x7f958dae85d0_0
                         E0x7f958dae8950
                         (not v0x7f958dae8750_0)))
                (=> v0x7f958dae8890_0 E0x7f958dae8950)
                a!1
                a!2
                (=> v0x7f958dae9950_0
                    (and v0x7f958dae8fd0_0
                         E0x7f958dae9a10
                         (not v0x7f958dae9810_0)))
                (=> v0x7f958dae9950_0 E0x7f958dae9a10)
                a!3
                a!4
                (=> v0x7f958daea810_0
                    (and v0x7f958dae9e90_0 E0x7f958daea8d0 v0x7f958daea6d0_0))
                (=> v0x7f958daea810_0 E0x7f958daea8d0)
                (=> v0x7f958daeabd0_0
                    (and v0x7f958dae9e90_0
                         E0x7f958daeac90
                         (not v0x7f958daea6d0_0)))
                (=> v0x7f958daeabd0_0 E0x7f958daeac90)
                (=> v0x7f958daeb410_0
                    (and v0x7f958daea810_0 E0x7f958daeb4d0 v0x7f958daeaa90_0))
                (=> v0x7f958daeb410_0 E0x7f958daeb4d0)
                (=> v0x7f958daeb910_0
                    (and v0x7f958daeabd0_0 E0x7f958daeb9d0 v0x7f958daeb2d0_0))
                (=> v0x7f958daeb910_0 E0x7f958daeb9d0)
                (=> v0x7f958daebb90_0 a!5)
                a!6
                v0x7f958daebb90_0
                (not v0x7f958daecd10_0)
                (= v0x7f958dae8750_0 (= v0x7f958dae8690_0 0.0))
                (= v0x7f958dae8b90_0 (< v0x7f958dae7f10_0 2.0))
                (= v0x7f958dae8d50_0 (ite v0x7f958dae8b90_0 1.0 0.0))
                (= v0x7f958dae8e90_0 (+ v0x7f958dae8d50_0 v0x7f958dae7f10_0))
                (= v0x7f958dae9810_0 (= v0x7f958dae9750_0 0.0))
                (= v0x7f958dae9c10_0 (= v0x7f958dae7d90_0 0.0))
                (= v0x7f958dae9d50_0 (ite v0x7f958dae9c10_0 1.0 0.0))
                (= v0x7f958daea6d0_0 (= v0x7f958dae8010_0 0.0))
                (= v0x7f958daeaa90_0 (> v0x7f958dae9090_0 1.0))
                (= v0x7f958daeae90_0 (> v0x7f958dae9090_0 0.0))
                (= v0x7f958daeafd0_0 (+ v0x7f958dae9090_0 (- 1.0)))
                (= v0x7f958daeb190_0
                   (ite v0x7f958daeae90_0 v0x7f958daeafd0_0 v0x7f958dae9090_0))
                (= v0x7f958daeb2d0_0 (= v0x7f958daeb190_0 0.0))
                (= v0x7f958daeb690_0 (= v0x7f958dae9f50_0 0.0))
                (= v0x7f958daeb7d0_0
                   (ite v0x7f958daeb690_0 1.0 v0x7f958dae8010_0))
                (= v0x7f958daecad0_0 (= v0x7f958dae9f50_0 0.0))
                (= v0x7f958daecbd0_0 (= v0x7f958daebd10_0 0.0))
                (= v0x7f958daecd10_0 (or v0x7f958daecbd0_0 v0x7f958daecad0_0)))))
  (=> F0x7f958daedb10 a!7))))
(assert (=> F0x7f958daedb10 F0x7f958daeda50))
(assert (=> F0x7f958daedc50 (or F0x7f958daed950 F0x7f958daed990)))
(assert (=> F0x7f958daedc10 F0x7f958daedb10))
(assert (=> pre!entry!0 (=> F0x7f958daed890 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f958daeda50 (>= v0x7f958dae8010_0 0.0))))
(assert (let ((a!1 (=> F0x7f958daeda50
               (or (<= v0x7f958dae8010_0 0.0) (not (<= v0x7f958dae7f10_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7f958dae6010_0 0.0) (not (<= v0x7f958dae80d0_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f958daedc50
           (not (>= v0x7f958dae6010_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f958daedc50 a!1)
      (and (not post!bb1.i.i34.i.i!0) F0x7f958daedc10 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i34.i.i!0)
