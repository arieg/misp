(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f8cab382ad0 () Bool)
(declare-fun F0x7f8cab382a10 () Bool)
(declare-fun v0x7f8cab37ce10_0 () Real)
(declare-fun v0x7f8cab381410_0 () Bool)
(declare-fun v0x7f8cab380050_0 () Real)
(declare-fun v0x7f8cab37ff10_0 () Bool)
(declare-fun v0x7f8cab37eb50_0 () Bool)
(declare-fun v0x7f8cab37d5d0_0 () Real)
(declare-fun v0x7f8cab381a50_0 () Bool)
(declare-fun E0x7f8cab380ed0 () Bool)
(declare-fun v0x7f8cab380210_0 () Real)
(declare-fun F0x7f8cab382bd0 () Bool)
(declare-fun v0x7f8cab37fb10_0 () Real)
(declare-fun E0x7f8cab380950 () Bool)
(declare-fun v0x7f8cab37cd10_0 () Real)
(declare-fun v0x7f8cab380710_0 () Bool)
(declare-fun v0x7f8cab380490_0 () Bool)
(declare-fun v0x7f8cab37fc50_0 () Bool)
(declare-fun v0x7f8cab37f9d0_0 () Bool)
(declare-fun v0x7f8cab37ca90_0 () Real)
(declare-fun v0x7f8cab37ee90_0 () Real)
(declare-fun E0x7f8cab37ef50 () Bool)
(declare-fun v0x7f8cab37f750_0 () Bool)
(declare-fun v0x7f8cab37edd0_0 () Bool)
(declare-fun v0x7f8cab37e890_0 () Bool)
(declare-fun E0x7f8cab380550 () Bool)
(declare-fun E0x7f8cab37e250 () Bool)
(declare-fun v0x7f8cab37dfd0_0 () Real)
(declare-fun v0x7f8cab37e690_0 () Real)
(declare-fun F0x7f8cab382c10 () Bool)
(declare-fun v0x7f8cab37f610_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7f8cab37e090 () Bool)
(declare-fun v0x7f8cab381790_0 () Bool)
(declare-fun v0x7f8cab381650_0 () Bool)
(declare-fun E0x7f8cab37f110 () Bool)
(declare-fun v0x7f8cab37df10_0 () Bool)
(declare-fun v0x7f8cab37ec90_0 () Real)
(declare-fun E0x7f8cab380c10 () Bool)
(declare-fun v0x7f8cab37d510_0 () Bool)
(declare-fun v0x7f8cab3807d0_0 () Real)
(declare-fun F0x7f8cab382950 () Bool)
(declare-fun v0x7f8cab37d7d0_0 () Bool)
(declare-fun v0x7f8cab380350_0 () Bool)
(declare-fun v0x7f8cab37cc10_0 () Real)
(declare-fun E0x7f8cab37e950 () Bool)
(declare-fun v0x7f8cab37b010_0 () Real)
(declare-fun E0x7f8cab37fd10 () Bool)
(declare-fun v0x7f8cab37ced0_0 () Real)
(declare-fun v0x7f8cab37d690_0 () Bool)
(declare-fun v0x7f8cab37ddd0_0 () Real)
(declare-fun E0x7f8cab381850 () Bool)
(declare-fun v0x7f8cab37e750_0 () Bool)
(declare-fun v0x7f8cab37cdd0_0 () Real)
(declare-fun v0x7f8cab37ccd0_0 () Real)
(declare-fun v0x7f8cab37dad0_0 () Bool)
(declare-fun v0x7f8cab37dc90_0 () Real)
(declare-fun F0x7f8cab382850 () Bool)
(declare-fun v0x7f8cab380890_0 () Real)
(declare-fun E0x7f8cab37f810 () Bool)
(declare-fun v0x7f8cab37b110_0 () Bool)
(declare-fun v0x7f8cab381510_0 () Bool)
(declare-fun E0x7f8cab37d890 () Bool)
(declare-fun F0x7f8cab382910 () Bool)

(assert (=> F0x7f8cab382910
    (and v0x7f8cab37b110_0
         (<= v0x7f8cab37ccd0_0 0.0)
         (>= v0x7f8cab37ccd0_0 0.0)
         (<= v0x7f8cab37cdd0_0 1.0)
         (>= v0x7f8cab37cdd0_0 1.0)
         (<= v0x7f8cab37ced0_0 0.0)
         (>= v0x7f8cab37ced0_0 0.0)
         (<= v0x7f8cab37b010_0 0.0)
         (>= v0x7f8cab37b010_0 0.0))))
(assert (=> F0x7f8cab382910 F0x7f8cab382850))
(assert (let ((a!1 (=> v0x7f8cab37df10_0
               (or (and v0x7f8cab37d7d0_0
                        E0x7f8cab37e090
                        (<= v0x7f8cab37dfd0_0 v0x7f8cab37ddd0_0)
                        (>= v0x7f8cab37dfd0_0 v0x7f8cab37ddd0_0))
                   (and v0x7f8cab37d510_0
                        E0x7f8cab37e250
                        v0x7f8cab37d690_0
                        (<= v0x7f8cab37dfd0_0 v0x7f8cab37cc10_0)
                        (>= v0x7f8cab37dfd0_0 v0x7f8cab37cc10_0)))))
      (a!2 (=> v0x7f8cab37df10_0
               (or (and E0x7f8cab37e090 (not E0x7f8cab37e250))
                   (and E0x7f8cab37e250 (not E0x7f8cab37e090)))))
      (a!3 (=> v0x7f8cab37edd0_0
               (or (and v0x7f8cab37e890_0
                        E0x7f8cab37ef50
                        (<= v0x7f8cab37ee90_0 v0x7f8cab37ec90_0)
                        (>= v0x7f8cab37ee90_0 v0x7f8cab37ec90_0))
                   (and v0x7f8cab37df10_0
                        E0x7f8cab37f110
                        v0x7f8cab37e750_0
                        (<= v0x7f8cab37ee90_0 v0x7f8cab37ca90_0)
                        (>= v0x7f8cab37ee90_0 v0x7f8cab37ca90_0)))))
      (a!4 (=> v0x7f8cab37edd0_0
               (or (and E0x7f8cab37ef50 (not E0x7f8cab37f110))
                   (and E0x7f8cab37f110 (not E0x7f8cab37ef50)))))
      (a!5 (or (and v0x7f8cab37f750_0
                    E0x7f8cab380950
                    (<= v0x7f8cab3807d0_0 v0x7f8cab37dfd0_0)
                    (>= v0x7f8cab3807d0_0 v0x7f8cab37dfd0_0)
                    (<= v0x7f8cab380890_0 v0x7f8cab37fb10_0)
                    (>= v0x7f8cab380890_0 v0x7f8cab37fb10_0))
               (and v0x7f8cab380490_0
                    E0x7f8cab380c10
                    (and (<= v0x7f8cab3807d0_0 v0x7f8cab380210_0)
                         (>= v0x7f8cab3807d0_0 v0x7f8cab380210_0))
                    (<= v0x7f8cab380890_0 v0x7f8cab37cd10_0)
                    (>= v0x7f8cab380890_0 v0x7f8cab37cd10_0))
               (and v0x7f8cab37fc50_0
                    E0x7f8cab380ed0
                    (not v0x7f8cab380350_0)
                    (and (<= v0x7f8cab3807d0_0 v0x7f8cab380210_0)
                         (>= v0x7f8cab3807d0_0 v0x7f8cab380210_0))
                    (<= v0x7f8cab380890_0 0.0)
                    (>= v0x7f8cab380890_0 0.0))))
      (a!6 (=> v0x7f8cab380710_0
               (or (and E0x7f8cab380950
                        (not E0x7f8cab380c10)
                        (not E0x7f8cab380ed0))
                   (and E0x7f8cab380c10
                        (not E0x7f8cab380950)
                        (not E0x7f8cab380ed0))
                   (and E0x7f8cab380ed0
                        (not E0x7f8cab380950)
                        (not E0x7f8cab380c10)))))
      (a!7 (or (and v0x7f8cab381790_0
                    v0x7f8cab381a50_0
                    (and (<= v0x7f8cab37ccd0_0 v0x7f8cab37ee90_0)
                         (>= v0x7f8cab37ccd0_0 v0x7f8cab37ee90_0))
                    (and (<= v0x7f8cab37cdd0_0 v0x7f8cab3807d0_0)
                         (>= v0x7f8cab37cdd0_0 v0x7f8cab3807d0_0))
                    (and (<= v0x7f8cab37ced0_0 v0x7f8cab380890_0)
                         (>= v0x7f8cab37ced0_0 v0x7f8cab380890_0))
                    (<= v0x7f8cab37b010_0 1.0)
                    (>= v0x7f8cab37b010_0 1.0))
               (and v0x7f8cab380710_0
                    v0x7f8cab381650_0
                    (and (<= v0x7f8cab37ccd0_0 v0x7f8cab37ee90_0)
                         (>= v0x7f8cab37ccd0_0 v0x7f8cab37ee90_0))
                    (and (<= v0x7f8cab37cdd0_0 v0x7f8cab3807d0_0)
                         (>= v0x7f8cab37cdd0_0 v0x7f8cab3807d0_0))
                    (and (<= v0x7f8cab37ced0_0 v0x7f8cab380890_0)
                         (>= v0x7f8cab37ced0_0 v0x7f8cab380890_0))
                    (<= v0x7f8cab37b010_0 0.0)
                    (>= v0x7f8cab37b010_0 0.0)))))
(let ((a!8 (and (=> v0x7f8cab37d7d0_0
                    (and v0x7f8cab37d510_0
                         E0x7f8cab37d890
                         (not v0x7f8cab37d690_0)))
                (=> v0x7f8cab37d7d0_0 E0x7f8cab37d890)
                a!1
                a!2
                (=> v0x7f8cab37e890_0
                    (and v0x7f8cab37df10_0
                         E0x7f8cab37e950
                         (not v0x7f8cab37e750_0)))
                (=> v0x7f8cab37e890_0 E0x7f8cab37e950)
                a!3
                a!4
                (=> v0x7f8cab37f750_0
                    (and v0x7f8cab37edd0_0 E0x7f8cab37f810 v0x7f8cab37f610_0))
                (=> v0x7f8cab37f750_0 E0x7f8cab37f810)
                (=> v0x7f8cab37fc50_0
                    (and v0x7f8cab37edd0_0
                         E0x7f8cab37fd10
                         (not v0x7f8cab37f610_0)))
                (=> v0x7f8cab37fc50_0 E0x7f8cab37fd10)
                (=> v0x7f8cab380490_0
                    (and v0x7f8cab37fc50_0 E0x7f8cab380550 v0x7f8cab380350_0))
                (=> v0x7f8cab380490_0 E0x7f8cab380550)
                (=> v0x7f8cab380710_0 a!5)
                a!6
                (=> v0x7f8cab381790_0
                    (and v0x7f8cab380710_0
                         E0x7f8cab381850
                         (not v0x7f8cab381650_0)))
                (=> v0x7f8cab381790_0 E0x7f8cab381850)
                a!7
                (= v0x7f8cab37d690_0 (= v0x7f8cab37d5d0_0 0.0))
                (= v0x7f8cab37dad0_0 (< v0x7f8cab37cc10_0 2.0))
                (= v0x7f8cab37dc90_0 (ite v0x7f8cab37dad0_0 1.0 0.0))
                (= v0x7f8cab37ddd0_0 (+ v0x7f8cab37dc90_0 v0x7f8cab37cc10_0))
                (= v0x7f8cab37e750_0 (= v0x7f8cab37e690_0 0.0))
                (= v0x7f8cab37eb50_0 (= v0x7f8cab37ca90_0 0.0))
                (= v0x7f8cab37ec90_0 (ite v0x7f8cab37eb50_0 1.0 0.0))
                (= v0x7f8cab37f610_0 (= v0x7f8cab37cd10_0 0.0))
                (= v0x7f8cab37f9d0_0 (> v0x7f8cab37dfd0_0 1.0))
                (= v0x7f8cab37fb10_0
                   (ite v0x7f8cab37f9d0_0 1.0 v0x7f8cab37cd10_0))
                (= v0x7f8cab37ff10_0 (> v0x7f8cab37dfd0_0 0.0))
                (= v0x7f8cab380050_0 (+ v0x7f8cab37dfd0_0 (- 1.0)))
                (= v0x7f8cab380210_0
                   (ite v0x7f8cab37ff10_0 v0x7f8cab380050_0 v0x7f8cab37dfd0_0))
                (= v0x7f8cab380350_0 (= v0x7f8cab37ee90_0 0.0))
                (= v0x7f8cab381410_0 (= v0x7f8cab37ee90_0 0.0))
                (= v0x7f8cab381510_0 (= v0x7f8cab380890_0 0.0))
                (= v0x7f8cab381650_0 (or v0x7f8cab381510_0 v0x7f8cab381410_0))
                (= v0x7f8cab381a50_0 (= v0x7f8cab37ce10_0 0.0)))))
  (=> F0x7f8cab382950 a!8))))
(assert (=> F0x7f8cab382950 F0x7f8cab382a10))
(assert (let ((a!1 (=> v0x7f8cab37df10_0
               (or (and v0x7f8cab37d7d0_0
                        E0x7f8cab37e090
                        (<= v0x7f8cab37dfd0_0 v0x7f8cab37ddd0_0)
                        (>= v0x7f8cab37dfd0_0 v0x7f8cab37ddd0_0))
                   (and v0x7f8cab37d510_0
                        E0x7f8cab37e250
                        v0x7f8cab37d690_0
                        (<= v0x7f8cab37dfd0_0 v0x7f8cab37cc10_0)
                        (>= v0x7f8cab37dfd0_0 v0x7f8cab37cc10_0)))))
      (a!2 (=> v0x7f8cab37df10_0
               (or (and E0x7f8cab37e090 (not E0x7f8cab37e250))
                   (and E0x7f8cab37e250 (not E0x7f8cab37e090)))))
      (a!3 (=> v0x7f8cab37edd0_0
               (or (and v0x7f8cab37e890_0
                        E0x7f8cab37ef50
                        (<= v0x7f8cab37ee90_0 v0x7f8cab37ec90_0)
                        (>= v0x7f8cab37ee90_0 v0x7f8cab37ec90_0))
                   (and v0x7f8cab37df10_0
                        E0x7f8cab37f110
                        v0x7f8cab37e750_0
                        (<= v0x7f8cab37ee90_0 v0x7f8cab37ca90_0)
                        (>= v0x7f8cab37ee90_0 v0x7f8cab37ca90_0)))))
      (a!4 (=> v0x7f8cab37edd0_0
               (or (and E0x7f8cab37ef50 (not E0x7f8cab37f110))
                   (and E0x7f8cab37f110 (not E0x7f8cab37ef50)))))
      (a!5 (or (and v0x7f8cab37f750_0
                    E0x7f8cab380950
                    (<= v0x7f8cab3807d0_0 v0x7f8cab37dfd0_0)
                    (>= v0x7f8cab3807d0_0 v0x7f8cab37dfd0_0)
                    (<= v0x7f8cab380890_0 v0x7f8cab37fb10_0)
                    (>= v0x7f8cab380890_0 v0x7f8cab37fb10_0))
               (and v0x7f8cab380490_0
                    E0x7f8cab380c10
                    (and (<= v0x7f8cab3807d0_0 v0x7f8cab380210_0)
                         (>= v0x7f8cab3807d0_0 v0x7f8cab380210_0))
                    (<= v0x7f8cab380890_0 v0x7f8cab37cd10_0)
                    (>= v0x7f8cab380890_0 v0x7f8cab37cd10_0))
               (and v0x7f8cab37fc50_0
                    E0x7f8cab380ed0
                    (not v0x7f8cab380350_0)
                    (and (<= v0x7f8cab3807d0_0 v0x7f8cab380210_0)
                         (>= v0x7f8cab3807d0_0 v0x7f8cab380210_0))
                    (<= v0x7f8cab380890_0 0.0)
                    (>= v0x7f8cab380890_0 0.0))))
      (a!6 (=> v0x7f8cab380710_0
               (or (and E0x7f8cab380950
                        (not E0x7f8cab380c10)
                        (not E0x7f8cab380ed0))
                   (and E0x7f8cab380c10
                        (not E0x7f8cab380950)
                        (not E0x7f8cab380ed0))
                   (and E0x7f8cab380ed0
                        (not E0x7f8cab380950)
                        (not E0x7f8cab380c10))))))
(let ((a!7 (and (=> v0x7f8cab37d7d0_0
                    (and v0x7f8cab37d510_0
                         E0x7f8cab37d890
                         (not v0x7f8cab37d690_0)))
                (=> v0x7f8cab37d7d0_0 E0x7f8cab37d890)
                a!1
                a!2
                (=> v0x7f8cab37e890_0
                    (and v0x7f8cab37df10_0
                         E0x7f8cab37e950
                         (not v0x7f8cab37e750_0)))
                (=> v0x7f8cab37e890_0 E0x7f8cab37e950)
                a!3
                a!4
                (=> v0x7f8cab37f750_0
                    (and v0x7f8cab37edd0_0 E0x7f8cab37f810 v0x7f8cab37f610_0))
                (=> v0x7f8cab37f750_0 E0x7f8cab37f810)
                (=> v0x7f8cab37fc50_0
                    (and v0x7f8cab37edd0_0
                         E0x7f8cab37fd10
                         (not v0x7f8cab37f610_0)))
                (=> v0x7f8cab37fc50_0 E0x7f8cab37fd10)
                (=> v0x7f8cab380490_0
                    (and v0x7f8cab37fc50_0 E0x7f8cab380550 v0x7f8cab380350_0))
                (=> v0x7f8cab380490_0 E0x7f8cab380550)
                (=> v0x7f8cab380710_0 a!5)
                a!6
                (=> v0x7f8cab381790_0
                    (and v0x7f8cab380710_0
                         E0x7f8cab381850
                         (not v0x7f8cab381650_0)))
                (=> v0x7f8cab381790_0 E0x7f8cab381850)
                v0x7f8cab381790_0
                (not v0x7f8cab381a50_0)
                (= v0x7f8cab37d690_0 (= v0x7f8cab37d5d0_0 0.0))
                (= v0x7f8cab37dad0_0 (< v0x7f8cab37cc10_0 2.0))
                (= v0x7f8cab37dc90_0 (ite v0x7f8cab37dad0_0 1.0 0.0))
                (= v0x7f8cab37ddd0_0 (+ v0x7f8cab37dc90_0 v0x7f8cab37cc10_0))
                (= v0x7f8cab37e750_0 (= v0x7f8cab37e690_0 0.0))
                (= v0x7f8cab37eb50_0 (= v0x7f8cab37ca90_0 0.0))
                (= v0x7f8cab37ec90_0 (ite v0x7f8cab37eb50_0 1.0 0.0))
                (= v0x7f8cab37f610_0 (= v0x7f8cab37cd10_0 0.0))
                (= v0x7f8cab37f9d0_0 (> v0x7f8cab37dfd0_0 1.0))
                (= v0x7f8cab37fb10_0
                   (ite v0x7f8cab37f9d0_0 1.0 v0x7f8cab37cd10_0))
                (= v0x7f8cab37ff10_0 (> v0x7f8cab37dfd0_0 0.0))
                (= v0x7f8cab380050_0 (+ v0x7f8cab37dfd0_0 (- 1.0)))
                (= v0x7f8cab380210_0
                   (ite v0x7f8cab37ff10_0 v0x7f8cab380050_0 v0x7f8cab37dfd0_0))
                (= v0x7f8cab380350_0 (= v0x7f8cab37ee90_0 0.0))
                (= v0x7f8cab381410_0 (= v0x7f8cab37ee90_0 0.0))
                (= v0x7f8cab381510_0 (= v0x7f8cab380890_0 0.0))
                (= v0x7f8cab381650_0 (or v0x7f8cab381510_0 v0x7f8cab381410_0))
                (= v0x7f8cab381a50_0 (= v0x7f8cab37ce10_0 0.0)))))
  (=> F0x7f8cab382ad0 a!7))))
(assert (=> F0x7f8cab382ad0 F0x7f8cab382a10))
(assert (=> F0x7f8cab382c10 (or F0x7f8cab382910 F0x7f8cab382950)))
(assert (=> F0x7f8cab382bd0 F0x7f8cab382ad0))
(assert (=> pre!entry!0 (=> F0x7f8cab382850 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f8cab382a10 (>= v0x7f8cab37ce10_0 0.0))))
(assert (let ((a!1 (=> F0x7f8cab382a10
               (or (<= v0x7f8cab37ce10_0 0.0) (not (<= v0x7f8cab37cd10_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7f8cab37b010_0 0.0) (not (<= v0x7f8cab37ced0_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f8cab382c10
           (not (>= v0x7f8cab37b010_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f8cab382c10 a!1)
      (and (not post!bb2.i.i23.i.i!0) F0x7f8cab382bd0 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i23.i.i!0)
