(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f1972d52410 () Bool)
(declare-fun F0x7f1972d52510 () Bool)
(declare-fun F0x7f1972d52350 () Bool)
(declare-fun v0x7f1972d514d0_0 () Bool)
(declare-fun v0x7f1972d4f510_0 () Bool)
(declare-fun v0x7f1972d4d3d0_0 () Real)
(declare-fun v0x7f1972d4f650_0 () Real)
(declare-fun v0x7f1972d4d210_0 () Bool)
(declare-fun v0x7f1972d51610_0 () Bool)
(declare-fun E0x7f1972d50dd0 () Bool)
(declare-fun E0x7f1972d50bd0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7f1972d50950 () Bool)
(declare-fun v0x7f1972d4f950_0 () Bool)
(declare-fun v0x7f1972d50090_0 () Real)
(declare-fun E0x7f1972d50690 () Bool)
(declare-fun v0x7f1972d50510_0 () Real)
(declare-fun v0x7f1972d4f110_0 () Bool)
(declare-fun E0x7f1972d4f310 () Bool)
(declare-fun v0x7f1972d4f250_0 () Bool)
(declare-fun E0x7f1972d50290 () Bool)
(declare-fun E0x7f1972d4ef50 () Bool)
(declare-fun v0x7f1972d4ee90_0 () Bool)
(declare-fun v0x7f1972d51390_0 () Bool)
(declare-fun v0x7f1972d4ff90_0 () Bool)
(declare-fun v0x7f1972d4ed50_0 () Bool)
(declare-fun v0x7f1972d4c410_0 () Real)
(declare-fun E0x7f1972d4fdd0 () Bool)
(declare-fun E0x7f1972d4e850 () Bool)
(declare-fun v0x7f1972d505d0_0 () Real)
(declare-fun v0x7f1972d4de90_0 () Bool)
(declare-fun v0x7f1972d501d0_0 () Bool)
(declare-fun v0x7f1972d4e290_0 () Bool)
(declare-fun E0x7f1972d4e090 () Bool)
(declare-fun v0x7f1972d4dfd0_0 () Bool)
(declare-fun v0x7f1972d4fd10_0 () Bool)
(declare-fun v0x7f1972d4fa90_0 () Bool)
(declare-fun v0x7f1972d4c590_0 () Real)
(declare-fun v0x7f1972d4cd10_0 () Real)
(declare-fun E0x7f1972d4d990 () Bool)
(declare-fun post!bb1.i.i43.i.i!0 () Bool)
(declare-fun v0x7f1972d4cdd0_0 () Bool)
(declare-fun v0x7f1972d4c690_0 () Real)
(declare-fun v0x7f1972d4f810_0 () Real)
(declare-fun E0x7f1972d4d7d0 () Bool)
(declare-fun v0x7f1972d4e3d0_0 () Real)
(declare-fun v0x7f1972d4cf10_0 () Bool)
(declare-fun F0x7f1972d52110 () Bool)
(declare-fun v0x7f1972d4e5d0_0 () Real)
(declare-fun v0x7f1972d50450_0 () Bool)
(declare-fun v0x7f1972d4cc50_0 () Bool)
(declare-fun v0x7f1972d49010_0 () Real)
(declare-fun v0x7f1972d4c750_0 () Real)
(declare-fun v0x7f1972d4d650_0 () Bool)
(declare-fun v0x7f1972d4d710_0 () Real)
(declare-fun v0x7f1972d4c650_0 () Real)
(declare-fun v0x7f1972d4e510_0 () Bool)
(declare-fun E0x7f1972d4cfd0 () Bool)
(declare-fun v0x7f1972d4ddd0_0 () Real)
(declare-fun v0x7f1972d49110_0 () Bool)
(declare-fun F0x7f1972d521d0 () Bool)
(declare-fun v0x7f1972d4fbd0_0 () Bool)
(declare-fun E0x7f1972d4e690 () Bool)
(declare-fun v0x7f1972d4d510_0 () Real)
(declare-fun F0x7f1972d52290 () Bool)
(declare-fun F0x7f1972d52550 () Bool)

(assert (=> F0x7f1972d52290
    (and v0x7f1972d49110_0
         (<= v0x7f1972d4c650_0 0.0)
         (>= v0x7f1972d4c650_0 0.0)
         (<= v0x7f1972d4c750_0 1.0)
         (>= v0x7f1972d4c750_0 1.0)
         (<= v0x7f1972d49010_0 0.0)
         (>= v0x7f1972d49010_0 0.0))))
(assert (=> F0x7f1972d52290 F0x7f1972d521d0))
(assert (let ((a!1 (=> v0x7f1972d4d650_0
               (or (and v0x7f1972d4cf10_0
                        E0x7f1972d4d7d0
                        (<= v0x7f1972d4d710_0 v0x7f1972d4d510_0)
                        (>= v0x7f1972d4d710_0 v0x7f1972d4d510_0))
                   (and v0x7f1972d4cc50_0
                        E0x7f1972d4d990
                        v0x7f1972d4cdd0_0
                        (<= v0x7f1972d4d710_0 v0x7f1972d4c590_0)
                        (>= v0x7f1972d4d710_0 v0x7f1972d4c590_0)))))
      (a!2 (=> v0x7f1972d4d650_0
               (or (and E0x7f1972d4d7d0 (not E0x7f1972d4d990))
                   (and E0x7f1972d4d990 (not E0x7f1972d4d7d0)))))
      (a!3 (=> v0x7f1972d4e510_0
               (or (and v0x7f1972d4dfd0_0
                        E0x7f1972d4e690
                        (<= v0x7f1972d4e5d0_0 v0x7f1972d4e3d0_0)
                        (>= v0x7f1972d4e5d0_0 v0x7f1972d4e3d0_0))
                   (and v0x7f1972d4d650_0
                        E0x7f1972d4e850
                        v0x7f1972d4de90_0
                        (<= v0x7f1972d4e5d0_0 v0x7f1972d4c410_0)
                        (>= v0x7f1972d4e5d0_0 v0x7f1972d4c410_0)))))
      (a!4 (=> v0x7f1972d4e510_0
               (or (and E0x7f1972d4e690 (not E0x7f1972d4e850))
                   (and E0x7f1972d4e850 (not E0x7f1972d4e690)))))
      (a!5 (or (and v0x7f1972d4fd10_0
                    E0x7f1972d50690
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4d710_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4d710_0))
                    (<= v0x7f1972d505d0_0 v0x7f1972d50090_0)
                    (>= v0x7f1972d505d0_0 v0x7f1972d50090_0))
               (and v0x7f1972d4ee90_0
                    E0x7f1972d50950
                    (not v0x7f1972d4f110_0)
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4d710_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4d710_0))
                    (and (<= v0x7f1972d505d0_0 v0x7f1972d4c690_0)
                         (>= v0x7f1972d505d0_0 v0x7f1972d4c690_0)))
               (and v0x7f1972d501d0_0
                    E0x7f1972d50bd0
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4f810_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4f810_0))
                    (and (<= v0x7f1972d505d0_0 v0x7f1972d4c690_0)
                         (>= v0x7f1972d505d0_0 v0x7f1972d4c690_0)))
               (and v0x7f1972d4f250_0
                    E0x7f1972d50dd0
                    (not v0x7f1972d4fbd0_0)
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4f810_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4f810_0))
                    (<= v0x7f1972d505d0_0 0.0)
                    (>= v0x7f1972d505d0_0 0.0))))
      (a!6 (=> v0x7f1972d50450_0
               (or (and E0x7f1972d50690
                        (not E0x7f1972d50950)
                        (not E0x7f1972d50bd0)
                        (not E0x7f1972d50dd0))
                   (and E0x7f1972d50950
                        (not E0x7f1972d50690)
                        (not E0x7f1972d50bd0)
                        (not E0x7f1972d50dd0))
                   (and E0x7f1972d50bd0
                        (not E0x7f1972d50690)
                        (not E0x7f1972d50950)
                        (not E0x7f1972d50dd0))
                   (and E0x7f1972d50dd0
                        (not E0x7f1972d50690)
                        (not E0x7f1972d50950)
                        (not E0x7f1972d50bd0))))))
(let ((a!7 (and (=> v0x7f1972d4cf10_0
                    (and v0x7f1972d4cc50_0
                         E0x7f1972d4cfd0
                         (not v0x7f1972d4cdd0_0)))
                (=> v0x7f1972d4cf10_0 E0x7f1972d4cfd0)
                a!1
                a!2
                (=> v0x7f1972d4dfd0_0
                    (and v0x7f1972d4d650_0
                         E0x7f1972d4e090
                         (not v0x7f1972d4de90_0)))
                (=> v0x7f1972d4dfd0_0 E0x7f1972d4e090)
                a!3
                a!4
                (=> v0x7f1972d4ee90_0
                    (and v0x7f1972d4e510_0 E0x7f1972d4ef50 v0x7f1972d4ed50_0))
                (=> v0x7f1972d4ee90_0 E0x7f1972d4ef50)
                (=> v0x7f1972d4f250_0
                    (and v0x7f1972d4e510_0
                         E0x7f1972d4f310
                         (not v0x7f1972d4ed50_0)))
                (=> v0x7f1972d4f250_0 E0x7f1972d4f310)
                (=> v0x7f1972d4fd10_0
                    (and v0x7f1972d4ee90_0 E0x7f1972d4fdd0 v0x7f1972d4f110_0))
                (=> v0x7f1972d4fd10_0 E0x7f1972d4fdd0)
                (=> v0x7f1972d501d0_0
                    (and v0x7f1972d4f250_0 E0x7f1972d50290 v0x7f1972d4fbd0_0))
                (=> v0x7f1972d501d0_0 E0x7f1972d50290)
                (=> v0x7f1972d50450_0 a!5)
                a!6
                v0x7f1972d50450_0
                v0x7f1972d51610_0
                (<= v0x7f1972d4c650_0 v0x7f1972d4e5d0_0)
                (>= v0x7f1972d4c650_0 v0x7f1972d4e5d0_0)
                (<= v0x7f1972d4c750_0 v0x7f1972d50510_0)
                (>= v0x7f1972d4c750_0 v0x7f1972d50510_0)
                (<= v0x7f1972d49010_0 v0x7f1972d505d0_0)
                (>= v0x7f1972d49010_0 v0x7f1972d505d0_0)
                (= v0x7f1972d4cdd0_0 (= v0x7f1972d4cd10_0 0.0))
                (= v0x7f1972d4d210_0 (< v0x7f1972d4c590_0 2.0))
                (= v0x7f1972d4d3d0_0 (ite v0x7f1972d4d210_0 1.0 0.0))
                (= v0x7f1972d4d510_0 (+ v0x7f1972d4d3d0_0 v0x7f1972d4c590_0))
                (= v0x7f1972d4de90_0 (= v0x7f1972d4ddd0_0 0.0))
                (= v0x7f1972d4e290_0 (= v0x7f1972d4c410_0 0.0))
                (= v0x7f1972d4e3d0_0 (ite v0x7f1972d4e290_0 1.0 0.0))
                (= v0x7f1972d4ed50_0 (= v0x7f1972d4c690_0 0.0))
                (= v0x7f1972d4f110_0 (> v0x7f1972d4d710_0 1.0))
                (= v0x7f1972d4f510_0 (> v0x7f1972d4d710_0 0.0))
                (= v0x7f1972d4f650_0 (+ v0x7f1972d4d710_0 (- 1.0)))
                (= v0x7f1972d4f810_0
                   (ite v0x7f1972d4f510_0 v0x7f1972d4f650_0 v0x7f1972d4d710_0))
                (= v0x7f1972d4f950_0 (= v0x7f1972d4e5d0_0 0.0))
                (= v0x7f1972d4fa90_0 (= v0x7f1972d4f810_0 0.0))
                (= v0x7f1972d4fbd0_0 (and v0x7f1972d4f950_0 v0x7f1972d4fa90_0))
                (= v0x7f1972d4ff90_0 (= v0x7f1972d4e5d0_0 0.0))
                (= v0x7f1972d50090_0
                   (ite v0x7f1972d4ff90_0 1.0 v0x7f1972d4c690_0))
                (= v0x7f1972d51390_0 (not (= v0x7f1972d50510_0 0.0)))
                (= v0x7f1972d514d0_0 (= v0x7f1972d505d0_0 0.0))
                (= v0x7f1972d51610_0 (or v0x7f1972d514d0_0 v0x7f1972d51390_0)))))
  (=> F0x7f1972d52110 a!7))))
(assert (=> F0x7f1972d52110 F0x7f1972d52350))
(assert (let ((a!1 (=> v0x7f1972d4d650_0
               (or (and v0x7f1972d4cf10_0
                        E0x7f1972d4d7d0
                        (<= v0x7f1972d4d710_0 v0x7f1972d4d510_0)
                        (>= v0x7f1972d4d710_0 v0x7f1972d4d510_0))
                   (and v0x7f1972d4cc50_0
                        E0x7f1972d4d990
                        v0x7f1972d4cdd0_0
                        (<= v0x7f1972d4d710_0 v0x7f1972d4c590_0)
                        (>= v0x7f1972d4d710_0 v0x7f1972d4c590_0)))))
      (a!2 (=> v0x7f1972d4d650_0
               (or (and E0x7f1972d4d7d0 (not E0x7f1972d4d990))
                   (and E0x7f1972d4d990 (not E0x7f1972d4d7d0)))))
      (a!3 (=> v0x7f1972d4e510_0
               (or (and v0x7f1972d4dfd0_0
                        E0x7f1972d4e690
                        (<= v0x7f1972d4e5d0_0 v0x7f1972d4e3d0_0)
                        (>= v0x7f1972d4e5d0_0 v0x7f1972d4e3d0_0))
                   (and v0x7f1972d4d650_0
                        E0x7f1972d4e850
                        v0x7f1972d4de90_0
                        (<= v0x7f1972d4e5d0_0 v0x7f1972d4c410_0)
                        (>= v0x7f1972d4e5d0_0 v0x7f1972d4c410_0)))))
      (a!4 (=> v0x7f1972d4e510_0
               (or (and E0x7f1972d4e690 (not E0x7f1972d4e850))
                   (and E0x7f1972d4e850 (not E0x7f1972d4e690)))))
      (a!5 (or (and v0x7f1972d4fd10_0
                    E0x7f1972d50690
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4d710_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4d710_0))
                    (<= v0x7f1972d505d0_0 v0x7f1972d50090_0)
                    (>= v0x7f1972d505d0_0 v0x7f1972d50090_0))
               (and v0x7f1972d4ee90_0
                    E0x7f1972d50950
                    (not v0x7f1972d4f110_0)
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4d710_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4d710_0))
                    (and (<= v0x7f1972d505d0_0 v0x7f1972d4c690_0)
                         (>= v0x7f1972d505d0_0 v0x7f1972d4c690_0)))
               (and v0x7f1972d501d0_0
                    E0x7f1972d50bd0
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4f810_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4f810_0))
                    (and (<= v0x7f1972d505d0_0 v0x7f1972d4c690_0)
                         (>= v0x7f1972d505d0_0 v0x7f1972d4c690_0)))
               (and v0x7f1972d4f250_0
                    E0x7f1972d50dd0
                    (not v0x7f1972d4fbd0_0)
                    (and (<= v0x7f1972d50510_0 v0x7f1972d4f810_0)
                         (>= v0x7f1972d50510_0 v0x7f1972d4f810_0))
                    (<= v0x7f1972d505d0_0 0.0)
                    (>= v0x7f1972d505d0_0 0.0))))
      (a!6 (=> v0x7f1972d50450_0
               (or (and E0x7f1972d50690
                        (not E0x7f1972d50950)
                        (not E0x7f1972d50bd0)
                        (not E0x7f1972d50dd0))
                   (and E0x7f1972d50950
                        (not E0x7f1972d50690)
                        (not E0x7f1972d50bd0)
                        (not E0x7f1972d50dd0))
                   (and E0x7f1972d50bd0
                        (not E0x7f1972d50690)
                        (not E0x7f1972d50950)
                        (not E0x7f1972d50dd0))
                   (and E0x7f1972d50dd0
                        (not E0x7f1972d50690)
                        (not E0x7f1972d50950)
                        (not E0x7f1972d50bd0))))))
(let ((a!7 (and (=> v0x7f1972d4cf10_0
                    (and v0x7f1972d4cc50_0
                         E0x7f1972d4cfd0
                         (not v0x7f1972d4cdd0_0)))
                (=> v0x7f1972d4cf10_0 E0x7f1972d4cfd0)
                a!1
                a!2
                (=> v0x7f1972d4dfd0_0
                    (and v0x7f1972d4d650_0
                         E0x7f1972d4e090
                         (not v0x7f1972d4de90_0)))
                (=> v0x7f1972d4dfd0_0 E0x7f1972d4e090)
                a!3
                a!4
                (=> v0x7f1972d4ee90_0
                    (and v0x7f1972d4e510_0 E0x7f1972d4ef50 v0x7f1972d4ed50_0))
                (=> v0x7f1972d4ee90_0 E0x7f1972d4ef50)
                (=> v0x7f1972d4f250_0
                    (and v0x7f1972d4e510_0
                         E0x7f1972d4f310
                         (not v0x7f1972d4ed50_0)))
                (=> v0x7f1972d4f250_0 E0x7f1972d4f310)
                (=> v0x7f1972d4fd10_0
                    (and v0x7f1972d4ee90_0 E0x7f1972d4fdd0 v0x7f1972d4f110_0))
                (=> v0x7f1972d4fd10_0 E0x7f1972d4fdd0)
                (=> v0x7f1972d501d0_0
                    (and v0x7f1972d4f250_0 E0x7f1972d50290 v0x7f1972d4fbd0_0))
                (=> v0x7f1972d501d0_0 E0x7f1972d50290)
                (=> v0x7f1972d50450_0 a!5)
                a!6
                v0x7f1972d50450_0
                (not v0x7f1972d51610_0)
                (= v0x7f1972d4cdd0_0 (= v0x7f1972d4cd10_0 0.0))
                (= v0x7f1972d4d210_0 (< v0x7f1972d4c590_0 2.0))
                (= v0x7f1972d4d3d0_0 (ite v0x7f1972d4d210_0 1.0 0.0))
                (= v0x7f1972d4d510_0 (+ v0x7f1972d4d3d0_0 v0x7f1972d4c590_0))
                (= v0x7f1972d4de90_0 (= v0x7f1972d4ddd0_0 0.0))
                (= v0x7f1972d4e290_0 (= v0x7f1972d4c410_0 0.0))
                (= v0x7f1972d4e3d0_0 (ite v0x7f1972d4e290_0 1.0 0.0))
                (= v0x7f1972d4ed50_0 (= v0x7f1972d4c690_0 0.0))
                (= v0x7f1972d4f110_0 (> v0x7f1972d4d710_0 1.0))
                (= v0x7f1972d4f510_0 (> v0x7f1972d4d710_0 0.0))
                (= v0x7f1972d4f650_0 (+ v0x7f1972d4d710_0 (- 1.0)))
                (= v0x7f1972d4f810_0
                   (ite v0x7f1972d4f510_0 v0x7f1972d4f650_0 v0x7f1972d4d710_0))
                (= v0x7f1972d4f950_0 (= v0x7f1972d4e5d0_0 0.0))
                (= v0x7f1972d4fa90_0 (= v0x7f1972d4f810_0 0.0))
                (= v0x7f1972d4fbd0_0 (and v0x7f1972d4f950_0 v0x7f1972d4fa90_0))
                (= v0x7f1972d4ff90_0 (= v0x7f1972d4e5d0_0 0.0))
                (= v0x7f1972d50090_0
                   (ite v0x7f1972d4ff90_0 1.0 v0x7f1972d4c690_0))
                (= v0x7f1972d51390_0 (not (= v0x7f1972d50510_0 0.0)))
                (= v0x7f1972d514d0_0 (= v0x7f1972d505d0_0 0.0))
                (= v0x7f1972d51610_0 (or v0x7f1972d514d0_0 v0x7f1972d51390_0)))))
  (=> F0x7f1972d52410 a!7))))
(assert (=> F0x7f1972d52410 F0x7f1972d52350))
(assert (=> F0x7f1972d52550 (or F0x7f1972d52290 F0x7f1972d52110)))
(assert (=> F0x7f1972d52510 F0x7f1972d52410))
(assert (=> pre!entry!0 (=> F0x7f1972d521d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f1972d52350 (>= v0x7f1972d4c410_0 0.0))))
(assert (let ((a!1 (=> F0x7f1972d52350
               (or (not (<= v0x7f1972d4c590_0 1.0)) (<= v0x7f1972d4c690_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2 (=> F0x7f1972d52350 (>= v0x7f1972d4c690_0 0.0))))
(assert (let ((a!1 (not (or (not (<= v0x7f1972d4c750_0 1.0)) (<= v0x7f1972d49010_0 0.0)))))
  (or (and (not post!bb1.i.i!0)
           F0x7f1972d52550
           (not (>= v0x7f1972d4c650_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f1972d52550 a!1)
      (and (not post!bb1.i.i!2)
           F0x7f1972d52550
           (not (>= v0x7f1972d49010_0 0.0)))
      (and (not post!bb1.i.i43.i.i!0) F0x7f1972d52510 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i43.i.i!0)
