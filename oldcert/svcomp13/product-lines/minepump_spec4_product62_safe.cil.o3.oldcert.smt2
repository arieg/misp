(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f088bef8350 () Bool)
(declare-fun v0x7f088bef5510_0 () Bool)
(declare-fun v0x7f088bef3dd0_0 () Real)
(declare-fun v0x7f088bef33d0_0 () Real)
(declare-fun v0x7f088bef3210_0 () Bool)
(declare-fun v0x7f088bef2d10_0 () Real)
(declare-fun v0x7f088bef7610_0 () Bool)
(declare-fun v0x7f088bef5810_0 () Real)
(declare-fun E0x7f088bef6bd0 () Bool)
(declare-fun post!bb1.i.i43.i.i!0 () Bool)
(declare-fun v0x7f088bef2690_0 () Real)
(declare-fun E0x7f088bef6950 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f088bef4290_0 () Bool)
(declare-fun v0x7f088bef6090_0 () Real)
(declare-fun v0x7f088bef6510_0 () Real)
(declare-fun E0x7f088bef6dd0 () Bool)
(declare-fun v0x7f088bef5950_0 () Bool)
(declare-fun E0x7f088bef6690 () Bool)
(declare-fun v0x7f088bef5bd0_0 () Bool)
(declare-fun E0x7f088bef6290 () Bool)
(declare-fun v0x7f088bef74d0_0 () Bool)
(declare-fun v0x7f088bef61d0_0 () Bool)
(declare-fun v0x7f088bef5f90_0 () Bool)
(declare-fun v0x7f088bef4e90_0 () Bool)
(declare-fun v0x7f088bef2410_0 () Real)
(declare-fun E0x7f088bef4850 () Bool)
(declare-fun E0x7f088bef5dd0 () Bool)
(declare-fun v0x7f088bef4d50_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f088bef43d0_0 () Real)
(declare-fun v0x7f088bef6450_0 () Bool)
(declare-fun v0x7f088bef5d10_0 () Bool)
(declare-fun E0x7f088bef4090 () Bool)
(declare-fun v0x7f088bef5650_0 () Real)
(declare-fun v0x7f088bef5a90_0 () Bool)
(declare-fun v0x7f088bef3e90_0 () Bool)
(declare-fun v0x7f088bef3fd0_0 () Bool)
(declare-fun v0x7f088bef45d0_0 () Real)
(declare-fun v0x7f088bef3710_0 () Real)
(declare-fun F0x7f088bef8550 () Bool)
(declare-fun v0x7f088bef5250_0 () Bool)
(declare-fun E0x7f088bef37d0 () Bool)
(declare-fun v0x7f088bef5110_0 () Bool)
(declare-fun v0x7f088bef7390_0 () Bool)
(declare-fun F0x7f088bef8410 () Bool)
(declare-fun v0x7f088bef3650_0 () Bool)
(declare-fun v0x7f088bef2590_0 () Real)
(declare-fun v0x7f088bef3510_0 () Real)
(declare-fun F0x7f088bef8510 () Bool)
(declare-fun E0x7f088bef4690 () Bool)
(declare-fun v0x7f088bef65d0_0 () Real)
(declare-fun v0x7f088bef2dd0_0 () Bool)
(declare-fun E0x7f088bef4f50 () Bool)
(declare-fun E0x7f088bef2fd0 () Bool)
(declare-fun v0x7f088bef2c50_0 () Bool)
(declare-fun E0x7f088bef3990 () Bool)
(declare-fun F0x7f088bef81d0 () Bool)
(declare-fun v0x7f088bef2f10_0 () Bool)
(declare-fun v0x7f088bef2750_0 () Real)
(declare-fun E0x7f088bef5310 () Bool)
(declare-fun F0x7f088bef8110 () Bool)
(declare-fun v0x7f088beef010_0 () Real)
(declare-fun v0x7f088bef2650_0 () Real)
(declare-fun v0x7f088bef4510_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f088beef110_0 () Bool)
(declare-fun F0x7f088bef8290 () Bool)

(assert (=> F0x7f088bef8290
    (and v0x7f088beef110_0
         (<= v0x7f088bef2650_0 0.0)
         (>= v0x7f088bef2650_0 0.0)
         (<= v0x7f088bef2750_0 1.0)
         (>= v0x7f088bef2750_0 1.0)
         (<= v0x7f088beef010_0 0.0)
         (>= v0x7f088beef010_0 0.0))))
(assert (=> F0x7f088bef8290 F0x7f088bef81d0))
(assert (let ((a!1 (=> v0x7f088bef3650_0
               (or (and v0x7f088bef2f10_0
                        E0x7f088bef37d0
                        (<= v0x7f088bef3710_0 v0x7f088bef3510_0)
                        (>= v0x7f088bef3710_0 v0x7f088bef3510_0))
                   (and v0x7f088bef2c50_0
                        E0x7f088bef3990
                        v0x7f088bef2dd0_0
                        (<= v0x7f088bef3710_0 v0x7f088bef2590_0)
                        (>= v0x7f088bef3710_0 v0x7f088bef2590_0)))))
      (a!2 (=> v0x7f088bef3650_0
               (or (and E0x7f088bef37d0 (not E0x7f088bef3990))
                   (and E0x7f088bef3990 (not E0x7f088bef37d0)))))
      (a!3 (=> v0x7f088bef4510_0
               (or (and v0x7f088bef3fd0_0
                        E0x7f088bef4690
                        (<= v0x7f088bef45d0_0 v0x7f088bef43d0_0)
                        (>= v0x7f088bef45d0_0 v0x7f088bef43d0_0))
                   (and v0x7f088bef3650_0
                        E0x7f088bef4850
                        v0x7f088bef3e90_0
                        (<= v0x7f088bef45d0_0 v0x7f088bef2410_0)
                        (>= v0x7f088bef45d0_0 v0x7f088bef2410_0)))))
      (a!4 (=> v0x7f088bef4510_0
               (or (and E0x7f088bef4690 (not E0x7f088bef4850))
                   (and E0x7f088bef4850 (not E0x7f088bef4690)))))
      (a!5 (or (and v0x7f088bef5d10_0
                    E0x7f088bef6690
                    (and (<= v0x7f088bef6510_0 v0x7f088bef3710_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef3710_0))
                    (<= v0x7f088bef65d0_0 v0x7f088bef6090_0)
                    (>= v0x7f088bef65d0_0 v0x7f088bef6090_0))
               (and v0x7f088bef4e90_0
                    E0x7f088bef6950
                    (not v0x7f088bef5110_0)
                    (and (<= v0x7f088bef6510_0 v0x7f088bef3710_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef3710_0))
                    (and (<= v0x7f088bef65d0_0 v0x7f088bef2690_0)
                         (>= v0x7f088bef65d0_0 v0x7f088bef2690_0)))
               (and v0x7f088bef61d0_0
                    E0x7f088bef6bd0
                    (and (<= v0x7f088bef6510_0 v0x7f088bef5810_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef5810_0))
                    (and (<= v0x7f088bef65d0_0 v0x7f088bef2690_0)
                         (>= v0x7f088bef65d0_0 v0x7f088bef2690_0)))
               (and v0x7f088bef5250_0
                    E0x7f088bef6dd0
                    (not v0x7f088bef5bd0_0)
                    (and (<= v0x7f088bef6510_0 v0x7f088bef5810_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef5810_0))
                    (<= v0x7f088bef65d0_0 0.0)
                    (>= v0x7f088bef65d0_0 0.0))))
      (a!6 (=> v0x7f088bef6450_0
               (or (and E0x7f088bef6690
                        (not E0x7f088bef6950)
                        (not E0x7f088bef6bd0)
                        (not E0x7f088bef6dd0))
                   (and E0x7f088bef6950
                        (not E0x7f088bef6690)
                        (not E0x7f088bef6bd0)
                        (not E0x7f088bef6dd0))
                   (and E0x7f088bef6bd0
                        (not E0x7f088bef6690)
                        (not E0x7f088bef6950)
                        (not E0x7f088bef6dd0))
                   (and E0x7f088bef6dd0
                        (not E0x7f088bef6690)
                        (not E0x7f088bef6950)
                        (not E0x7f088bef6bd0))))))
(let ((a!7 (and (=> v0x7f088bef2f10_0
                    (and v0x7f088bef2c50_0
                         E0x7f088bef2fd0
                         (not v0x7f088bef2dd0_0)))
                (=> v0x7f088bef2f10_0 E0x7f088bef2fd0)
                a!1
                a!2
                (=> v0x7f088bef3fd0_0
                    (and v0x7f088bef3650_0
                         E0x7f088bef4090
                         (not v0x7f088bef3e90_0)))
                (=> v0x7f088bef3fd0_0 E0x7f088bef4090)
                a!3
                a!4
                (=> v0x7f088bef4e90_0
                    (and v0x7f088bef4510_0 E0x7f088bef4f50 v0x7f088bef4d50_0))
                (=> v0x7f088bef4e90_0 E0x7f088bef4f50)
                (=> v0x7f088bef5250_0
                    (and v0x7f088bef4510_0
                         E0x7f088bef5310
                         (not v0x7f088bef4d50_0)))
                (=> v0x7f088bef5250_0 E0x7f088bef5310)
                (=> v0x7f088bef5d10_0
                    (and v0x7f088bef4e90_0 E0x7f088bef5dd0 v0x7f088bef5110_0))
                (=> v0x7f088bef5d10_0 E0x7f088bef5dd0)
                (=> v0x7f088bef61d0_0
                    (and v0x7f088bef5250_0 E0x7f088bef6290 v0x7f088bef5bd0_0))
                (=> v0x7f088bef61d0_0 E0x7f088bef6290)
                (=> v0x7f088bef6450_0 a!5)
                a!6
                v0x7f088bef6450_0
                v0x7f088bef7610_0
                (<= v0x7f088bef2650_0 v0x7f088bef45d0_0)
                (>= v0x7f088bef2650_0 v0x7f088bef45d0_0)
                (<= v0x7f088bef2750_0 v0x7f088bef6510_0)
                (>= v0x7f088bef2750_0 v0x7f088bef6510_0)
                (<= v0x7f088beef010_0 v0x7f088bef65d0_0)
                (>= v0x7f088beef010_0 v0x7f088bef65d0_0)
                (= v0x7f088bef2dd0_0 (= v0x7f088bef2d10_0 0.0))
                (= v0x7f088bef3210_0 (< v0x7f088bef2590_0 2.0))
                (= v0x7f088bef33d0_0 (ite v0x7f088bef3210_0 1.0 0.0))
                (= v0x7f088bef3510_0 (+ v0x7f088bef33d0_0 v0x7f088bef2590_0))
                (= v0x7f088bef3e90_0 (= v0x7f088bef3dd0_0 0.0))
                (= v0x7f088bef4290_0 (= v0x7f088bef2410_0 0.0))
                (= v0x7f088bef43d0_0 (ite v0x7f088bef4290_0 1.0 0.0))
                (= v0x7f088bef4d50_0 (= v0x7f088bef2690_0 0.0))
                (= v0x7f088bef5110_0 (> v0x7f088bef3710_0 1.0))
                (= v0x7f088bef5510_0 (> v0x7f088bef3710_0 0.0))
                (= v0x7f088bef5650_0 (+ v0x7f088bef3710_0 (- 1.0)))
                (= v0x7f088bef5810_0
                   (ite v0x7f088bef5510_0 v0x7f088bef5650_0 v0x7f088bef3710_0))
                (= v0x7f088bef5950_0 (= v0x7f088bef45d0_0 0.0))
                (= v0x7f088bef5a90_0 (= v0x7f088bef5810_0 0.0))
                (= v0x7f088bef5bd0_0 (and v0x7f088bef5950_0 v0x7f088bef5a90_0))
                (= v0x7f088bef5f90_0 (= v0x7f088bef45d0_0 0.0))
                (= v0x7f088bef6090_0
                   (ite v0x7f088bef5f90_0 1.0 v0x7f088bef2690_0))
                (= v0x7f088bef7390_0 (not (= v0x7f088bef6510_0 0.0)))
                (= v0x7f088bef74d0_0 (= v0x7f088bef65d0_0 0.0))
                (= v0x7f088bef7610_0 (or v0x7f088bef74d0_0 v0x7f088bef7390_0)))))
  (=> F0x7f088bef8110 a!7))))
(assert (=> F0x7f088bef8110 F0x7f088bef8350))
(assert (let ((a!1 (=> v0x7f088bef3650_0
               (or (and v0x7f088bef2f10_0
                        E0x7f088bef37d0
                        (<= v0x7f088bef3710_0 v0x7f088bef3510_0)
                        (>= v0x7f088bef3710_0 v0x7f088bef3510_0))
                   (and v0x7f088bef2c50_0
                        E0x7f088bef3990
                        v0x7f088bef2dd0_0
                        (<= v0x7f088bef3710_0 v0x7f088bef2590_0)
                        (>= v0x7f088bef3710_0 v0x7f088bef2590_0)))))
      (a!2 (=> v0x7f088bef3650_0
               (or (and E0x7f088bef37d0 (not E0x7f088bef3990))
                   (and E0x7f088bef3990 (not E0x7f088bef37d0)))))
      (a!3 (=> v0x7f088bef4510_0
               (or (and v0x7f088bef3fd0_0
                        E0x7f088bef4690
                        (<= v0x7f088bef45d0_0 v0x7f088bef43d0_0)
                        (>= v0x7f088bef45d0_0 v0x7f088bef43d0_0))
                   (and v0x7f088bef3650_0
                        E0x7f088bef4850
                        v0x7f088bef3e90_0
                        (<= v0x7f088bef45d0_0 v0x7f088bef2410_0)
                        (>= v0x7f088bef45d0_0 v0x7f088bef2410_0)))))
      (a!4 (=> v0x7f088bef4510_0
               (or (and E0x7f088bef4690 (not E0x7f088bef4850))
                   (and E0x7f088bef4850 (not E0x7f088bef4690)))))
      (a!5 (or (and v0x7f088bef5d10_0
                    E0x7f088bef6690
                    (and (<= v0x7f088bef6510_0 v0x7f088bef3710_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef3710_0))
                    (<= v0x7f088bef65d0_0 v0x7f088bef6090_0)
                    (>= v0x7f088bef65d0_0 v0x7f088bef6090_0))
               (and v0x7f088bef4e90_0
                    E0x7f088bef6950
                    (not v0x7f088bef5110_0)
                    (and (<= v0x7f088bef6510_0 v0x7f088bef3710_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef3710_0))
                    (and (<= v0x7f088bef65d0_0 v0x7f088bef2690_0)
                         (>= v0x7f088bef65d0_0 v0x7f088bef2690_0)))
               (and v0x7f088bef61d0_0
                    E0x7f088bef6bd0
                    (and (<= v0x7f088bef6510_0 v0x7f088bef5810_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef5810_0))
                    (and (<= v0x7f088bef65d0_0 v0x7f088bef2690_0)
                         (>= v0x7f088bef65d0_0 v0x7f088bef2690_0)))
               (and v0x7f088bef5250_0
                    E0x7f088bef6dd0
                    (not v0x7f088bef5bd0_0)
                    (and (<= v0x7f088bef6510_0 v0x7f088bef5810_0)
                         (>= v0x7f088bef6510_0 v0x7f088bef5810_0))
                    (<= v0x7f088bef65d0_0 0.0)
                    (>= v0x7f088bef65d0_0 0.0))))
      (a!6 (=> v0x7f088bef6450_0
               (or (and E0x7f088bef6690
                        (not E0x7f088bef6950)
                        (not E0x7f088bef6bd0)
                        (not E0x7f088bef6dd0))
                   (and E0x7f088bef6950
                        (not E0x7f088bef6690)
                        (not E0x7f088bef6bd0)
                        (not E0x7f088bef6dd0))
                   (and E0x7f088bef6bd0
                        (not E0x7f088bef6690)
                        (not E0x7f088bef6950)
                        (not E0x7f088bef6dd0))
                   (and E0x7f088bef6dd0
                        (not E0x7f088bef6690)
                        (not E0x7f088bef6950)
                        (not E0x7f088bef6bd0))))))
(let ((a!7 (and (=> v0x7f088bef2f10_0
                    (and v0x7f088bef2c50_0
                         E0x7f088bef2fd0
                         (not v0x7f088bef2dd0_0)))
                (=> v0x7f088bef2f10_0 E0x7f088bef2fd0)
                a!1
                a!2
                (=> v0x7f088bef3fd0_0
                    (and v0x7f088bef3650_0
                         E0x7f088bef4090
                         (not v0x7f088bef3e90_0)))
                (=> v0x7f088bef3fd0_0 E0x7f088bef4090)
                a!3
                a!4
                (=> v0x7f088bef4e90_0
                    (and v0x7f088bef4510_0 E0x7f088bef4f50 v0x7f088bef4d50_0))
                (=> v0x7f088bef4e90_0 E0x7f088bef4f50)
                (=> v0x7f088bef5250_0
                    (and v0x7f088bef4510_0
                         E0x7f088bef5310
                         (not v0x7f088bef4d50_0)))
                (=> v0x7f088bef5250_0 E0x7f088bef5310)
                (=> v0x7f088bef5d10_0
                    (and v0x7f088bef4e90_0 E0x7f088bef5dd0 v0x7f088bef5110_0))
                (=> v0x7f088bef5d10_0 E0x7f088bef5dd0)
                (=> v0x7f088bef61d0_0
                    (and v0x7f088bef5250_0 E0x7f088bef6290 v0x7f088bef5bd0_0))
                (=> v0x7f088bef61d0_0 E0x7f088bef6290)
                (=> v0x7f088bef6450_0 a!5)
                a!6
                v0x7f088bef6450_0
                (not v0x7f088bef7610_0)
                (= v0x7f088bef2dd0_0 (= v0x7f088bef2d10_0 0.0))
                (= v0x7f088bef3210_0 (< v0x7f088bef2590_0 2.0))
                (= v0x7f088bef33d0_0 (ite v0x7f088bef3210_0 1.0 0.0))
                (= v0x7f088bef3510_0 (+ v0x7f088bef33d0_0 v0x7f088bef2590_0))
                (= v0x7f088bef3e90_0 (= v0x7f088bef3dd0_0 0.0))
                (= v0x7f088bef4290_0 (= v0x7f088bef2410_0 0.0))
                (= v0x7f088bef43d0_0 (ite v0x7f088bef4290_0 1.0 0.0))
                (= v0x7f088bef4d50_0 (= v0x7f088bef2690_0 0.0))
                (= v0x7f088bef5110_0 (> v0x7f088bef3710_0 1.0))
                (= v0x7f088bef5510_0 (> v0x7f088bef3710_0 0.0))
                (= v0x7f088bef5650_0 (+ v0x7f088bef3710_0 (- 1.0)))
                (= v0x7f088bef5810_0
                   (ite v0x7f088bef5510_0 v0x7f088bef5650_0 v0x7f088bef3710_0))
                (= v0x7f088bef5950_0 (= v0x7f088bef45d0_0 0.0))
                (= v0x7f088bef5a90_0 (= v0x7f088bef5810_0 0.0))
                (= v0x7f088bef5bd0_0 (and v0x7f088bef5950_0 v0x7f088bef5a90_0))
                (= v0x7f088bef5f90_0 (= v0x7f088bef45d0_0 0.0))
                (= v0x7f088bef6090_0
                   (ite v0x7f088bef5f90_0 1.0 v0x7f088bef2690_0))
                (= v0x7f088bef7390_0 (not (= v0x7f088bef6510_0 0.0)))
                (= v0x7f088bef74d0_0 (= v0x7f088bef65d0_0 0.0))
                (= v0x7f088bef7610_0 (or v0x7f088bef74d0_0 v0x7f088bef7390_0)))))
  (=> F0x7f088bef8410 a!7))))
(assert (=> F0x7f088bef8410 F0x7f088bef8350))
(assert (=> F0x7f088bef8550 (or F0x7f088bef8290 F0x7f088bef8110)))
(assert (=> F0x7f088bef8510 F0x7f088bef8410))
(assert (=> pre!entry!0 (=> F0x7f088bef81d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f088bef8350 (>= v0x7f088bef2410_0 0.0))))
(assert (let ((a!1 (=> F0x7f088bef8350
               (or (not (<= v0x7f088bef2590_0 1.0)) (<= v0x7f088bef2690_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2 (=> F0x7f088bef8350 (>= v0x7f088bef2690_0 0.0))))
(assert (let ((a!1 (not (or (not (<= v0x7f088bef2750_0 1.0)) (<= v0x7f088beef010_0 0.0)))))
  (or (and (not post!bb1.i.i!0)
           F0x7f088bef8550
           (not (>= v0x7f088bef2650_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f088bef8550 a!1)
      (and (not post!bb1.i.i!2)
           F0x7f088bef8550
           (not (>= v0x7f088beef010_0 0.0)))
      (and (not post!bb1.i.i43.i.i!0) F0x7f088bef8510 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i43.i.i!0)
