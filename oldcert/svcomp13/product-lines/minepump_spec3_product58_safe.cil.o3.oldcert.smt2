(declare-fun post!bb2.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fee9f5a3010 () Bool)
(declare-fun F0x7fee9f5a3050 () Bool)
(declare-fun F0x7fee9f5a2e50 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fee9f5a1e90_0 () Bool)
(declare-fun F0x7fee9f5a2f10 () Bool)
(declare-fun v0x7fee9f5a1c50_0 () Bool)
(declare-fun v0x7fee9f5a0810_0 () Bool)
(declare-fun v0x7fee9f5a0150_0 () Real)
(declare-fun v0x7fee9f5a0010_0 () Bool)
(declare-fun v0x7fee9f59e8d0_0 () Real)
(declare-fun v0x7fee9f59ed90_0 () Bool)
(declare-fun v0x7fee9f59d810_0 () Real)
(declare-fun v0x7fee9f5a0310_0 () Real)
(declare-fun v0x7fee9f59cf10_0 () Real)
(declare-fun E0x7fee9f5a1210 () Bool)
(declare-fun v0x7fee9f5a0d10_0 () Bool)
(declare-fun v0x7fee9f5a0450_0 () Bool)
(declare-fun v0x7fee9f59fc10_0 () Bool)
(declare-fun E0x7fee9f5a0650 () Bool)
(declare-fun E0x7fee9f59fe10 () Bool)
(declare-fun v0x7fee9f59f850_0 () Bool)
(declare-fun v0x7fee9f59dd10_0 () Bool)
(declare-fun v0x7fee9f5a1fd0_0 () Bool)
(declare-fun E0x7fee9f59fa50 () Bool)
(declare-fun v0x7fee9f5a0a90_0 () Bool)
(declare-fun v0x7fee9f59eed0_0 () Real)
(declare-fun v0x7fee9f59f990_0 () Bool)
(declare-fun v0x7fee9f5a0dd0_0 () Real)
(declare-fun E0x7fee9f5a1490 () Bool)
(declare-fun E0x7fee9f59f190 () Bool)
(declare-fun v0x7fee9f5a2110_0 () Bool)
(declare-fun E0x7fee9f5a0b50 () Bool)
(declare-fun v0x7fee9f59f010_0 () Bool)
(declare-fun v0x7fee9f59ead0_0 () Bool)
(declare-fun v0x7fee9f59f0d0_0 () Real)
(declare-fun E0x7fee9f59e2d0 () Bool)
(declare-fun v0x7fee9f59d190_0 () Real)
(declare-fun v0x7fee9f59ded0_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7fee9f59e010_0 () Real)
(declare-fun v0x7fee9f59e150_0 () Bool)
(declare-fun E0x7fee9f5a1690 () Bool)
(declare-fun E0x7fee9f59eb90 () Bool)
(declare-fun v0x7fee9f5a0590_0 () Bool)
(declare-fun v0x7fee9f59fd50_0 () Bool)
(declare-fun E0x7fee9f5a0f50 () Bool)
(declare-fun v0x7fee9f5a1d50_0 () Bool)
(declare-fun v0x7fee9f59d8d0_0 () Bool)
(declare-fun v0x7fee9f59e210_0 () Real)
(declare-fun E0x7fee9f59dad0 () Bool)
(declare-fun E0x7fee9f59e490 () Bool)
(declare-fun v0x7fee9f59e990_0 () Bool)
(declare-fun v0x7fee9f59d750_0 () Bool)
(declare-fun v0x7fee9f59da10_0 () Bool)
(declare-fun v0x7fee9f59b010_0 () Real)
(declare-fun F0x7fee9f5a2c90 () Bool)
(declare-fun v0x7fee9f5a0950_0 () Real)
(declare-fun v0x7fee9f59b110_0 () Bool)
(declare-fun v0x7fee9f5a0e90_0 () Real)
(declare-fun v0x7fee9f59d090_0 () Real)
(declare-fun F0x7fee9f5a2cd0 () Bool)
(declare-fun E0x7fee9f59f350 () Bool)
(declare-fun v0x7fee9f59d150_0 () Real)
(declare-fun v0x7fee9f59d250_0 () Real)
(declare-fun F0x7fee9f5a2d90 () Bool)

(assert (=> F0x7fee9f5a2d90
    (and v0x7fee9f59b110_0
         (<= v0x7fee9f59d150_0 0.0)
         (>= v0x7fee9f59d150_0 0.0)
         (<= v0x7fee9f59d250_0 0.0)
         (>= v0x7fee9f59d250_0 0.0)
         (<= v0x7fee9f59b010_0 1.0)
         (>= v0x7fee9f59b010_0 1.0))))
(assert (=> F0x7fee9f5a2d90 F0x7fee9f5a2c90))
(assert (let ((a!1 (=> v0x7fee9f59e150_0
               (or (and v0x7fee9f59da10_0
                        E0x7fee9f59e2d0
                        (<= v0x7fee9f59e210_0 v0x7fee9f59e010_0)
                        (>= v0x7fee9f59e210_0 v0x7fee9f59e010_0))
                   (and v0x7fee9f59d750_0
                        E0x7fee9f59e490
                        v0x7fee9f59d8d0_0
                        (<= v0x7fee9f59e210_0 v0x7fee9f59d190_0)
                        (>= v0x7fee9f59e210_0 v0x7fee9f59d190_0)))))
      (a!2 (=> v0x7fee9f59e150_0
               (or (and E0x7fee9f59e2d0 (not E0x7fee9f59e490))
                   (and E0x7fee9f59e490 (not E0x7fee9f59e2d0)))))
      (a!3 (=> v0x7fee9f59f010_0
               (or (and v0x7fee9f59ead0_0
                        E0x7fee9f59f190
                        (<= v0x7fee9f59f0d0_0 v0x7fee9f59eed0_0)
                        (>= v0x7fee9f59f0d0_0 v0x7fee9f59eed0_0))
                   (and v0x7fee9f59e150_0
                        E0x7fee9f59f350
                        v0x7fee9f59e990_0
                        (<= v0x7fee9f59f0d0_0 v0x7fee9f59d090_0)
                        (>= v0x7fee9f59f0d0_0 v0x7fee9f59d090_0)))))
      (a!4 (=> v0x7fee9f59f010_0
               (or (and E0x7fee9f59f190 (not E0x7fee9f59f350))
                   (and E0x7fee9f59f350 (not E0x7fee9f59f190)))))
      (a!5 (or (and v0x7fee9f5a0590_0
                    E0x7fee9f5a0f50
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0))
                    (<= v0x7fee9f5a0e90_0 v0x7fee9f5a0950_0)
                    (>= v0x7fee9f5a0e90_0 v0x7fee9f5a0950_0))
               (and v0x7fee9f59f990_0
                    E0x7fee9f5a1210
                    (not v0x7fee9f59fc10_0)
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0))
                    (and (<= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)
                         (>= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)))
               (and v0x7fee9f5a0a90_0
                    E0x7fee9f5a1490
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0))
                    (and (<= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)
                         (>= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)))
               (and v0x7fee9f59fd50_0
                    E0x7fee9f5a1690
                    (not v0x7fee9f5a0450_0)
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0))
                    (<= v0x7fee9f5a0e90_0 0.0)
                    (>= v0x7fee9f5a0e90_0 0.0))))
      (a!6 (=> v0x7fee9f5a0d10_0
               (or (and E0x7fee9f5a0f50
                        (not E0x7fee9f5a1210)
                        (not E0x7fee9f5a1490)
                        (not E0x7fee9f5a1690))
                   (and E0x7fee9f5a1210
                        (not E0x7fee9f5a0f50)
                        (not E0x7fee9f5a1490)
                        (not E0x7fee9f5a1690))
                   (and E0x7fee9f5a1490
                        (not E0x7fee9f5a0f50)
                        (not E0x7fee9f5a1210)
                        (not E0x7fee9f5a1690))
                   (and E0x7fee9f5a1690
                        (not E0x7fee9f5a0f50)
                        (not E0x7fee9f5a1210)
                        (not E0x7fee9f5a1490))))))
(let ((a!7 (and (=> v0x7fee9f59da10_0
                    (and v0x7fee9f59d750_0
                         E0x7fee9f59dad0
                         (not v0x7fee9f59d8d0_0)))
                (=> v0x7fee9f59da10_0 E0x7fee9f59dad0)
                a!1
                a!2
                (=> v0x7fee9f59ead0_0
                    (and v0x7fee9f59e150_0
                         E0x7fee9f59eb90
                         (not v0x7fee9f59e990_0)))
                (=> v0x7fee9f59ead0_0 E0x7fee9f59eb90)
                a!3
                a!4
                (=> v0x7fee9f59f990_0
                    (and v0x7fee9f59f010_0 E0x7fee9f59fa50 v0x7fee9f59f850_0))
                (=> v0x7fee9f59f990_0 E0x7fee9f59fa50)
                (=> v0x7fee9f59fd50_0
                    (and v0x7fee9f59f010_0
                         E0x7fee9f59fe10
                         (not v0x7fee9f59f850_0)))
                (=> v0x7fee9f59fd50_0 E0x7fee9f59fe10)
                (=> v0x7fee9f5a0590_0
                    (and v0x7fee9f59f990_0 E0x7fee9f5a0650 v0x7fee9f59fc10_0))
                (=> v0x7fee9f5a0590_0 E0x7fee9f5a0650)
                (=> v0x7fee9f5a0a90_0
                    (and v0x7fee9f59fd50_0 E0x7fee9f5a0b50 v0x7fee9f5a0450_0))
                (=> v0x7fee9f5a0a90_0 E0x7fee9f5a0b50)
                (=> v0x7fee9f5a0d10_0 a!5)
                a!6
                v0x7fee9f5a0d10_0
                (not v0x7fee9f5a2110_0)
                (<= v0x7fee9f59d150_0 v0x7fee9f5a0e90_0)
                (>= v0x7fee9f59d150_0 v0x7fee9f5a0e90_0)
                (<= v0x7fee9f59d250_0 v0x7fee9f59f0d0_0)
                (>= v0x7fee9f59d250_0 v0x7fee9f59f0d0_0)
                (<= v0x7fee9f59b010_0 v0x7fee9f5a0dd0_0)
                (>= v0x7fee9f59b010_0 v0x7fee9f5a0dd0_0)
                (= v0x7fee9f59d8d0_0 (= v0x7fee9f59d810_0 0.0))
                (= v0x7fee9f59dd10_0 (< v0x7fee9f59d190_0 2.0))
                (= v0x7fee9f59ded0_0 (ite v0x7fee9f59dd10_0 1.0 0.0))
                (= v0x7fee9f59e010_0 (+ v0x7fee9f59ded0_0 v0x7fee9f59d190_0))
                (= v0x7fee9f59e990_0 (= v0x7fee9f59e8d0_0 0.0))
                (= v0x7fee9f59ed90_0 (= v0x7fee9f59d090_0 0.0))
                (= v0x7fee9f59eed0_0 (ite v0x7fee9f59ed90_0 1.0 0.0))
                (= v0x7fee9f59f850_0 (= v0x7fee9f59cf10_0 0.0))
                (= v0x7fee9f59fc10_0 (> v0x7fee9f59e210_0 1.0))
                (= v0x7fee9f5a0010_0 (> v0x7fee9f59e210_0 0.0))
                (= v0x7fee9f5a0150_0 (+ v0x7fee9f59e210_0 (- 1.0)))
                (= v0x7fee9f5a0310_0
                   (ite v0x7fee9f5a0010_0 v0x7fee9f5a0150_0 v0x7fee9f59e210_0))
                (= v0x7fee9f5a0450_0 (= v0x7fee9f5a0310_0 0.0))
                (= v0x7fee9f5a0810_0 (= v0x7fee9f59f0d0_0 0.0))
                (= v0x7fee9f5a0950_0
                   (ite v0x7fee9f5a0810_0 1.0 v0x7fee9f59cf10_0))
                (= v0x7fee9f5a1c50_0 (= v0x7fee9f59f0d0_0 0.0))
                (= v0x7fee9f5a1d50_0 (= v0x7fee9f5a0dd0_0 2.0))
                (= v0x7fee9f5a1e90_0 (= v0x7fee9f5a0e90_0 0.0))
                (= v0x7fee9f5a1fd0_0 (and v0x7fee9f5a1d50_0 v0x7fee9f5a1c50_0))
                (= v0x7fee9f5a2110_0 (and v0x7fee9f5a1fd0_0 v0x7fee9f5a1e90_0)))))
  (=> F0x7fee9f5a2cd0 a!7))))
(assert (=> F0x7fee9f5a2cd0 F0x7fee9f5a2e50))
(assert (let ((a!1 (=> v0x7fee9f59e150_0
               (or (and v0x7fee9f59da10_0
                        E0x7fee9f59e2d0
                        (<= v0x7fee9f59e210_0 v0x7fee9f59e010_0)
                        (>= v0x7fee9f59e210_0 v0x7fee9f59e010_0))
                   (and v0x7fee9f59d750_0
                        E0x7fee9f59e490
                        v0x7fee9f59d8d0_0
                        (<= v0x7fee9f59e210_0 v0x7fee9f59d190_0)
                        (>= v0x7fee9f59e210_0 v0x7fee9f59d190_0)))))
      (a!2 (=> v0x7fee9f59e150_0
               (or (and E0x7fee9f59e2d0 (not E0x7fee9f59e490))
                   (and E0x7fee9f59e490 (not E0x7fee9f59e2d0)))))
      (a!3 (=> v0x7fee9f59f010_0
               (or (and v0x7fee9f59ead0_0
                        E0x7fee9f59f190
                        (<= v0x7fee9f59f0d0_0 v0x7fee9f59eed0_0)
                        (>= v0x7fee9f59f0d0_0 v0x7fee9f59eed0_0))
                   (and v0x7fee9f59e150_0
                        E0x7fee9f59f350
                        v0x7fee9f59e990_0
                        (<= v0x7fee9f59f0d0_0 v0x7fee9f59d090_0)
                        (>= v0x7fee9f59f0d0_0 v0x7fee9f59d090_0)))))
      (a!4 (=> v0x7fee9f59f010_0
               (or (and E0x7fee9f59f190 (not E0x7fee9f59f350))
                   (and E0x7fee9f59f350 (not E0x7fee9f59f190)))))
      (a!5 (or (and v0x7fee9f5a0590_0
                    E0x7fee9f5a0f50
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0))
                    (<= v0x7fee9f5a0e90_0 v0x7fee9f5a0950_0)
                    (>= v0x7fee9f5a0e90_0 v0x7fee9f5a0950_0))
               (and v0x7fee9f59f990_0
                    E0x7fee9f5a1210
                    (not v0x7fee9f59fc10_0)
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f59e210_0))
                    (and (<= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)
                         (>= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)))
               (and v0x7fee9f5a0a90_0
                    E0x7fee9f5a1490
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0))
                    (and (<= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)
                         (>= v0x7fee9f5a0e90_0 v0x7fee9f59cf10_0)))
               (and v0x7fee9f59fd50_0
                    E0x7fee9f5a1690
                    (not v0x7fee9f5a0450_0)
                    (and (<= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0)
                         (>= v0x7fee9f5a0dd0_0 v0x7fee9f5a0310_0))
                    (<= v0x7fee9f5a0e90_0 0.0)
                    (>= v0x7fee9f5a0e90_0 0.0))))
      (a!6 (=> v0x7fee9f5a0d10_0
               (or (and E0x7fee9f5a0f50
                        (not E0x7fee9f5a1210)
                        (not E0x7fee9f5a1490)
                        (not E0x7fee9f5a1690))
                   (and E0x7fee9f5a1210
                        (not E0x7fee9f5a0f50)
                        (not E0x7fee9f5a1490)
                        (not E0x7fee9f5a1690))
                   (and E0x7fee9f5a1490
                        (not E0x7fee9f5a0f50)
                        (not E0x7fee9f5a1210)
                        (not E0x7fee9f5a1690))
                   (and E0x7fee9f5a1690
                        (not E0x7fee9f5a0f50)
                        (not E0x7fee9f5a1210)
                        (not E0x7fee9f5a1490))))))
(let ((a!7 (and (=> v0x7fee9f59da10_0
                    (and v0x7fee9f59d750_0
                         E0x7fee9f59dad0
                         (not v0x7fee9f59d8d0_0)))
                (=> v0x7fee9f59da10_0 E0x7fee9f59dad0)
                a!1
                a!2
                (=> v0x7fee9f59ead0_0
                    (and v0x7fee9f59e150_0
                         E0x7fee9f59eb90
                         (not v0x7fee9f59e990_0)))
                (=> v0x7fee9f59ead0_0 E0x7fee9f59eb90)
                a!3
                a!4
                (=> v0x7fee9f59f990_0
                    (and v0x7fee9f59f010_0 E0x7fee9f59fa50 v0x7fee9f59f850_0))
                (=> v0x7fee9f59f990_0 E0x7fee9f59fa50)
                (=> v0x7fee9f59fd50_0
                    (and v0x7fee9f59f010_0
                         E0x7fee9f59fe10
                         (not v0x7fee9f59f850_0)))
                (=> v0x7fee9f59fd50_0 E0x7fee9f59fe10)
                (=> v0x7fee9f5a0590_0
                    (and v0x7fee9f59f990_0 E0x7fee9f5a0650 v0x7fee9f59fc10_0))
                (=> v0x7fee9f5a0590_0 E0x7fee9f5a0650)
                (=> v0x7fee9f5a0a90_0
                    (and v0x7fee9f59fd50_0 E0x7fee9f5a0b50 v0x7fee9f5a0450_0))
                (=> v0x7fee9f5a0a90_0 E0x7fee9f5a0b50)
                (=> v0x7fee9f5a0d10_0 a!5)
                a!6
                v0x7fee9f5a0d10_0
                v0x7fee9f5a2110_0
                (= v0x7fee9f59d8d0_0 (= v0x7fee9f59d810_0 0.0))
                (= v0x7fee9f59dd10_0 (< v0x7fee9f59d190_0 2.0))
                (= v0x7fee9f59ded0_0 (ite v0x7fee9f59dd10_0 1.0 0.0))
                (= v0x7fee9f59e010_0 (+ v0x7fee9f59ded0_0 v0x7fee9f59d190_0))
                (= v0x7fee9f59e990_0 (= v0x7fee9f59e8d0_0 0.0))
                (= v0x7fee9f59ed90_0 (= v0x7fee9f59d090_0 0.0))
                (= v0x7fee9f59eed0_0 (ite v0x7fee9f59ed90_0 1.0 0.0))
                (= v0x7fee9f59f850_0 (= v0x7fee9f59cf10_0 0.0))
                (= v0x7fee9f59fc10_0 (> v0x7fee9f59e210_0 1.0))
                (= v0x7fee9f5a0010_0 (> v0x7fee9f59e210_0 0.0))
                (= v0x7fee9f5a0150_0 (+ v0x7fee9f59e210_0 (- 1.0)))
                (= v0x7fee9f5a0310_0
                   (ite v0x7fee9f5a0010_0 v0x7fee9f5a0150_0 v0x7fee9f59e210_0))
                (= v0x7fee9f5a0450_0 (= v0x7fee9f5a0310_0 0.0))
                (= v0x7fee9f5a0810_0 (= v0x7fee9f59f0d0_0 0.0))
                (= v0x7fee9f5a0950_0
                   (ite v0x7fee9f5a0810_0 1.0 v0x7fee9f59cf10_0))
                (= v0x7fee9f5a1c50_0 (= v0x7fee9f59f0d0_0 0.0))
                (= v0x7fee9f5a1d50_0 (= v0x7fee9f5a0dd0_0 2.0))
                (= v0x7fee9f5a1e90_0 (= v0x7fee9f5a0e90_0 0.0))
                (= v0x7fee9f5a1fd0_0 (and v0x7fee9f5a1d50_0 v0x7fee9f5a1c50_0))
                (= v0x7fee9f5a2110_0 (and v0x7fee9f5a1fd0_0 v0x7fee9f5a1e90_0)))))
  (=> F0x7fee9f5a2f10 a!7))))
(assert (=> F0x7fee9f5a2f10 F0x7fee9f5a2e50))
(assert (=> F0x7fee9f5a3050 (or F0x7fee9f5a2d90 F0x7fee9f5a2cd0)))
(assert (=> F0x7fee9f5a3010 F0x7fee9f5a2f10))
(assert (=> pre!entry!0 (=> F0x7fee9f5a2c90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fee9f5a2e50 (>= v0x7fee9f59cf10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7fee9f5a2e50 (>= v0x7fee9f59d090_0 0.0))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fee9f5a2e50 (not (<= 3.0 v0x7fee9f59d190_0)))))
(assert (or (and (not post!bb1.i.i!0) F0x7fee9f5a3050 (not (>= v0x7fee9f59d150_0 0.0)))
    (and (not post!bb1.i.i!1) F0x7fee9f5a3050 (not (>= v0x7fee9f59d250_0 0.0)))
    (and (not post!bb1.i.i!2) F0x7fee9f5a3050 (<= 3.0 v0x7fee9f59b010_0))
    (and (not post!bb2.i.i34.i.i!0) F0x7fee9f5a3010 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb2.i.i34.i.i!0)
