(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f91a7bc53d0 () Bool)
(declare-fun v0x7f91a7bc4490_0 () Bool)
(declare-fun v0x7f91a7bc2f90_0 () Bool)
(declare-fun v0x7f91a7bc1d10_0 () Real)
(declare-fun v0x7f91a7bc1bd0_0 () Bool)
(declare-fun v0x7f91a7bc1090_0 () Real)
(declare-fun v0x7f91a7bc30d0_0 () Bool)
(declare-fun v0x7f91a7bbff90_0 () Bool)
(declare-fun E0x7f91a7bc3d10 () Bool)
(declare-fun E0x7f91a7bc3b50 () Bool)
(declare-fun v0x7f91a7bbfa90_0 () Real)
(declare-fun v0x7f91a7bc39d0_0 () Bool)
(declare-fun v0x7f91a7bc3210_0 () Bool)
(declare-fun E0x7f91a7bc3410 () Bool)
(declare-fun E0x7f91a7bc2950 () Bool)
(declare-fun v0x7f91a7bc1ed0_0 () Real)
(declare-fun v0x7f91a7bc2250_0 () Real)
(declare-fun v0x7f91a7bc0c10_0 () Real)
(declare-fun v0x7f91a7bc4850_0 () Bool)
(declare-fun v0x7f91a7bbf310_0 () Real)
(declare-fun v0x7f91a7bc17d0_0 () Bool)
(declare-fun E0x7f91a7bc1450 () Bool)
(declare-fun F0x7f91a7bc5410 () Bool)
(declare-fun E0x7f91a7bc1350 () Bool)
(declare-fun v0x7f91a7bc0e10_0 () Bool)
(declare-fun E0x7f91a7bc0ed0 () Bool)
(declare-fun v0x7f91a7bbf410_0 () Real)
(declare-fun E0x7f91a7bc37d0 () Bool)
(declare-fun v0x7f91a7bc0490_0 () Real)
(declare-fun v0x7f91a7bc03d0_0 () Bool)
(declare-fun v0x7f91a7bc4350_0 () Bool)
(declare-fun v0x7f91a7bc1910_0 () Bool)
(declare-fun v0x7f91a7bc20d0_0 () Real)
(declare-fun v0x7f91a7bc2010_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f91a7bc0290_0 () Real)
(declare-fun F0x7f91a7bc5610 () Bool)
(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun E0x7f91a7bc2310 () Bool)
(declare-fun v0x7f91a7bbfb50_0 () Bool)
(declare-fun E0x7f91a7bc3f10 () Bool)
(declare-fun v0x7f91a7bbf9d0_0 () Bool)
(declare-fun v0x7f91a7bbfc90_0 () Bool)
(declare-fun E0x7f91a7bc19d0 () Bool)
(declare-fun v0x7f91a7bc3710_0 () Bool)
(declare-fun F0x7f91a7bc56d0 () Bool)
(declare-fun v0x7f91a7bc4710_0 () Bool)
(declare-fun v0x7f91a7bbf190_0 () Real)
(declare-fun F0x7f91a7bc54d0 () Bool)
(declare-fun v0x7f91a7bc0150_0 () Real)
(declare-fun v0x7f91a7bc2190_0 () Real)
(declare-fun E0x7f91a7bbfd50 () Bool)
(declare-fun E0x7f91a7bc0550 () Bool)
(declare-fun v0x7f91a7bc1290_0 () Bool)
(declare-fun v0x7f91a7bc3350_0 () Bool)
(declare-fun v0x7f91a7bbf4d0_0 () Real)
(declare-fun E0x7f91a7bc26d0 () Bool)
(declare-fun v0x7f91a7bbf3d0_0 () Real)
(declare-fun F0x7f91a7bc5550 () Bool)
(declare-fun v0x7f91a7bc45d0_0 () Bool)
(declare-fun E0x7f91a7bc0710 () Bool)
(declare-fun v0x7f91a7bc35d0_0 () Bool)
(declare-fun v0x7f91a7bc1150_0 () Bool)
(declare-fun v0x7f91a7bbe110_0 () Bool)
(declare-fun v0x7f91a7bbe010_0 () Real)
(declare-fun v0x7f91a7bc3a90_0 () Real)
(declare-fun v0x7f91a7bc0cd0_0 () Bool)
(declare-fun F0x7f91a7bc57d0 () Bool)

(assert (=> F0x7f91a7bc57d0
    (and v0x7f91a7bbe110_0
         (<= v0x7f91a7bbf3d0_0 1.0)
         (>= v0x7f91a7bbf3d0_0 1.0)
         (<= v0x7f91a7bbf4d0_0 0.0)
         (>= v0x7f91a7bbf4d0_0 0.0)
         (<= v0x7f91a7bbe010_0 1.0)
         (>= v0x7f91a7bbe010_0 1.0))))
(assert (=> F0x7f91a7bc57d0 F0x7f91a7bc54d0))
(assert (let ((a!1 (=> v0x7f91a7bc03d0_0
               (or (and v0x7f91a7bbfc90_0
                        E0x7f91a7bc0550
                        (<= v0x7f91a7bc0490_0 v0x7f91a7bc0290_0)
                        (>= v0x7f91a7bc0490_0 v0x7f91a7bc0290_0))
                   (and v0x7f91a7bbf9d0_0
                        E0x7f91a7bc0710
                        v0x7f91a7bbfb50_0
                        (<= v0x7f91a7bc0490_0 v0x7f91a7bbf410_0)
                        (>= v0x7f91a7bc0490_0 v0x7f91a7bbf410_0)))))
      (a!2 (=> v0x7f91a7bc03d0_0
               (or (and E0x7f91a7bc0550 (not E0x7f91a7bc0710))
                   (and E0x7f91a7bc0710 (not E0x7f91a7bc0550)))))
      (a!3 (=> v0x7f91a7bc1290_0
               (or (and v0x7f91a7bc0e10_0 E0x7f91a7bc1350 v0x7f91a7bc1150_0)
                   (and v0x7f91a7bc03d0_0
                        E0x7f91a7bc1450
                        (not v0x7f91a7bc0cd0_0)))))
      (a!4 (=> v0x7f91a7bc1290_0
               (or (and E0x7f91a7bc1350 (not E0x7f91a7bc1450))
                   (and E0x7f91a7bc1450 (not E0x7f91a7bc1350)))))
      (a!5 (or (and v0x7f91a7bc1910_0
                    E0x7f91a7bc2310
                    (and (<= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0)
                         (>= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0))
                    (and (<= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0)
                         (>= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0))
                    (<= v0x7f91a7bc2250_0 v0x7f91a7bc1ed0_0)
                    (>= v0x7f91a7bc2250_0 v0x7f91a7bc1ed0_0))
               (and v0x7f91a7bc1290_0
                    E0x7f91a7bc26d0
                    v0x7f91a7bc17d0_0
                    (and (<= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0)
                         (>= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0))
                    (and (<= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0)
                         (>= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0))
                    (and (<= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)
                         (>= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)))
               (and v0x7f91a7bc0e10_0
                    E0x7f91a7bc2950
                    (not v0x7f91a7bc1150_0)
                    (<= v0x7f91a7bc20d0_0 0.0)
                    (>= v0x7f91a7bc20d0_0 0.0)
                    (<= v0x7f91a7bc2190_0 0.0)
                    (>= v0x7f91a7bc2190_0 0.0)
                    (and (<= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)
                         (>= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)))))
      (a!6 (=> v0x7f91a7bc2010_0
               (or (and E0x7f91a7bc2310
                        (not E0x7f91a7bc26d0)
                        (not E0x7f91a7bc2950))
                   (and E0x7f91a7bc26d0
                        (not E0x7f91a7bc2310)
                        (not E0x7f91a7bc2950))
                   (and E0x7f91a7bc2950
                        (not E0x7f91a7bc2310)
                        (not E0x7f91a7bc26d0)))))
      (a!7 (or (and v0x7f91a7bc3710_0
                    E0x7f91a7bc3b50
                    (and (<= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)
                         (>= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)))
               (and v0x7f91a7bc3350_0
                    E0x7f91a7bc3d10
                    v0x7f91a7bc35d0_0
                    (<= v0x7f91a7bc3a90_0 1.0)
                    (>= v0x7f91a7bc3a90_0 1.0))
               (and v0x7f91a7bc2010_0
                    E0x7f91a7bc3f10
                    (not v0x7f91a7bc3210_0)
                    (and (<= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)
                         (>= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)))))
      (a!8 (=> v0x7f91a7bc39d0_0
               (or (and E0x7f91a7bc3b50
                        (not E0x7f91a7bc3d10)
                        (not E0x7f91a7bc3f10))
                   (and E0x7f91a7bc3d10
                        (not E0x7f91a7bc3b50)
                        (not E0x7f91a7bc3f10))
                   (and E0x7f91a7bc3f10
                        (not E0x7f91a7bc3b50)
                        (not E0x7f91a7bc3d10))))))
(let ((a!9 (and (=> v0x7f91a7bbfc90_0
                    (and v0x7f91a7bbf9d0_0
                         E0x7f91a7bbfd50
                         (not v0x7f91a7bbfb50_0)))
                (=> v0x7f91a7bbfc90_0 E0x7f91a7bbfd50)
                a!1
                a!2
                (=> v0x7f91a7bc0e10_0
                    (and v0x7f91a7bc03d0_0 E0x7f91a7bc0ed0 v0x7f91a7bc0cd0_0))
                (=> v0x7f91a7bc0e10_0 E0x7f91a7bc0ed0)
                a!3
                a!4
                (=> v0x7f91a7bc1910_0
                    (and v0x7f91a7bc1290_0
                         E0x7f91a7bc19d0
                         (not v0x7f91a7bc17d0_0)))
                (=> v0x7f91a7bc1910_0 E0x7f91a7bc19d0)
                (=> v0x7f91a7bc2010_0 a!5)
                a!6
                (=> v0x7f91a7bc3350_0
                    (and v0x7f91a7bc2010_0 E0x7f91a7bc3410 v0x7f91a7bc3210_0))
                (=> v0x7f91a7bc3350_0 E0x7f91a7bc3410)
                (=> v0x7f91a7bc3710_0
                    (and v0x7f91a7bc3350_0
                         E0x7f91a7bc37d0
                         (not v0x7f91a7bc35d0_0)))
                (=> v0x7f91a7bc3710_0 E0x7f91a7bc37d0)
                (=> v0x7f91a7bc39d0_0 a!7)
                a!8
                v0x7f91a7bc39d0_0
                (not v0x7f91a7bc4850_0)
                (<= v0x7f91a7bbf3d0_0 v0x7f91a7bc2190_0)
                (>= v0x7f91a7bbf3d0_0 v0x7f91a7bc2190_0)
                (<= v0x7f91a7bbf4d0_0 v0x7f91a7bc3a90_0)
                (>= v0x7f91a7bbf4d0_0 v0x7f91a7bc3a90_0)
                (<= v0x7f91a7bbe010_0 v0x7f91a7bc2250_0)
                (>= v0x7f91a7bbe010_0 v0x7f91a7bc2250_0)
                (= v0x7f91a7bbfb50_0 (= v0x7f91a7bbfa90_0 0.0))
                (= v0x7f91a7bbff90_0 (< v0x7f91a7bbf410_0 2.0))
                (= v0x7f91a7bc0150_0 (ite v0x7f91a7bbff90_0 1.0 0.0))
                (= v0x7f91a7bc0290_0 (+ v0x7f91a7bc0150_0 v0x7f91a7bbf410_0))
                (= v0x7f91a7bc0cd0_0 (= v0x7f91a7bc0c10_0 0.0))
                (= v0x7f91a7bc1150_0 (= v0x7f91a7bc1090_0 0.0))
                (= v0x7f91a7bc17d0_0 (= v0x7f91a7bbf310_0 0.0))
                (= v0x7f91a7bc1bd0_0 (> v0x7f91a7bc0490_0 0.0))
                (= v0x7f91a7bc1d10_0 (+ v0x7f91a7bc0490_0 (- 1.0)))
                (= v0x7f91a7bc1ed0_0
                   (ite v0x7f91a7bc1bd0_0 v0x7f91a7bc1d10_0 v0x7f91a7bc0490_0))
                (= v0x7f91a7bc2f90_0 (not (= v0x7f91a7bc2190_0 0.0)))
                (= v0x7f91a7bc30d0_0 (= v0x7f91a7bc20d0_0 0.0))
                (= v0x7f91a7bc3210_0 (and v0x7f91a7bc2f90_0 v0x7f91a7bc30d0_0))
                (= v0x7f91a7bc35d0_0 (> v0x7f91a7bc2250_0 1.0))
                (= v0x7f91a7bc4350_0 (= v0x7f91a7bc2250_0 2.0))
                (= v0x7f91a7bc4490_0 (= v0x7f91a7bc3a90_0 0.0))
                (= v0x7f91a7bc45d0_0 (or v0x7f91a7bc4490_0 v0x7f91a7bc4350_0))
                (= v0x7f91a7bc4710_0 (xor v0x7f91a7bc45d0_0 true))
                (= v0x7f91a7bc4850_0 (and v0x7f91a7bc30d0_0 v0x7f91a7bc4710_0)))))
  (=> F0x7f91a7bc56d0 a!9))))
(assert (=> F0x7f91a7bc56d0 F0x7f91a7bc5610))
(assert (let ((a!1 (=> v0x7f91a7bc03d0_0
               (or (and v0x7f91a7bbfc90_0
                        E0x7f91a7bc0550
                        (<= v0x7f91a7bc0490_0 v0x7f91a7bc0290_0)
                        (>= v0x7f91a7bc0490_0 v0x7f91a7bc0290_0))
                   (and v0x7f91a7bbf9d0_0
                        E0x7f91a7bc0710
                        v0x7f91a7bbfb50_0
                        (<= v0x7f91a7bc0490_0 v0x7f91a7bbf410_0)
                        (>= v0x7f91a7bc0490_0 v0x7f91a7bbf410_0)))))
      (a!2 (=> v0x7f91a7bc03d0_0
               (or (and E0x7f91a7bc0550 (not E0x7f91a7bc0710))
                   (and E0x7f91a7bc0710 (not E0x7f91a7bc0550)))))
      (a!3 (=> v0x7f91a7bc1290_0
               (or (and v0x7f91a7bc0e10_0 E0x7f91a7bc1350 v0x7f91a7bc1150_0)
                   (and v0x7f91a7bc03d0_0
                        E0x7f91a7bc1450
                        (not v0x7f91a7bc0cd0_0)))))
      (a!4 (=> v0x7f91a7bc1290_0
               (or (and E0x7f91a7bc1350 (not E0x7f91a7bc1450))
                   (and E0x7f91a7bc1450 (not E0x7f91a7bc1350)))))
      (a!5 (or (and v0x7f91a7bc1910_0
                    E0x7f91a7bc2310
                    (and (<= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0)
                         (>= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0))
                    (and (<= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0)
                         (>= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0))
                    (<= v0x7f91a7bc2250_0 v0x7f91a7bc1ed0_0)
                    (>= v0x7f91a7bc2250_0 v0x7f91a7bc1ed0_0))
               (and v0x7f91a7bc1290_0
                    E0x7f91a7bc26d0
                    v0x7f91a7bc17d0_0
                    (and (<= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0)
                         (>= v0x7f91a7bc20d0_0 v0x7f91a7bbf310_0))
                    (and (<= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0)
                         (>= v0x7f91a7bc2190_0 v0x7f91a7bbf190_0))
                    (and (<= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)
                         (>= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)))
               (and v0x7f91a7bc0e10_0
                    E0x7f91a7bc2950
                    (not v0x7f91a7bc1150_0)
                    (<= v0x7f91a7bc20d0_0 0.0)
                    (>= v0x7f91a7bc20d0_0 0.0)
                    (<= v0x7f91a7bc2190_0 0.0)
                    (>= v0x7f91a7bc2190_0 0.0)
                    (and (<= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)
                         (>= v0x7f91a7bc2250_0 v0x7f91a7bc0490_0)))))
      (a!6 (=> v0x7f91a7bc2010_0
               (or (and E0x7f91a7bc2310
                        (not E0x7f91a7bc26d0)
                        (not E0x7f91a7bc2950))
                   (and E0x7f91a7bc26d0
                        (not E0x7f91a7bc2310)
                        (not E0x7f91a7bc2950))
                   (and E0x7f91a7bc2950
                        (not E0x7f91a7bc2310)
                        (not E0x7f91a7bc26d0)))))
      (a!7 (or (and v0x7f91a7bc3710_0
                    E0x7f91a7bc3b50
                    (and (<= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)
                         (>= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)))
               (and v0x7f91a7bc3350_0
                    E0x7f91a7bc3d10
                    v0x7f91a7bc35d0_0
                    (<= v0x7f91a7bc3a90_0 1.0)
                    (>= v0x7f91a7bc3a90_0 1.0))
               (and v0x7f91a7bc2010_0
                    E0x7f91a7bc3f10
                    (not v0x7f91a7bc3210_0)
                    (and (<= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)
                         (>= v0x7f91a7bc3a90_0 v0x7f91a7bc20d0_0)))))
      (a!8 (=> v0x7f91a7bc39d0_0
               (or (and E0x7f91a7bc3b50
                        (not E0x7f91a7bc3d10)
                        (not E0x7f91a7bc3f10))
                   (and E0x7f91a7bc3d10
                        (not E0x7f91a7bc3b50)
                        (not E0x7f91a7bc3f10))
                   (and E0x7f91a7bc3f10
                        (not E0x7f91a7bc3b50)
                        (not E0x7f91a7bc3d10))))))
(let ((a!9 (and (=> v0x7f91a7bbfc90_0
                    (and v0x7f91a7bbf9d0_0
                         E0x7f91a7bbfd50
                         (not v0x7f91a7bbfb50_0)))
                (=> v0x7f91a7bbfc90_0 E0x7f91a7bbfd50)
                a!1
                a!2
                (=> v0x7f91a7bc0e10_0
                    (and v0x7f91a7bc03d0_0 E0x7f91a7bc0ed0 v0x7f91a7bc0cd0_0))
                (=> v0x7f91a7bc0e10_0 E0x7f91a7bc0ed0)
                a!3
                a!4
                (=> v0x7f91a7bc1910_0
                    (and v0x7f91a7bc1290_0
                         E0x7f91a7bc19d0
                         (not v0x7f91a7bc17d0_0)))
                (=> v0x7f91a7bc1910_0 E0x7f91a7bc19d0)
                (=> v0x7f91a7bc2010_0 a!5)
                a!6
                (=> v0x7f91a7bc3350_0
                    (and v0x7f91a7bc2010_0 E0x7f91a7bc3410 v0x7f91a7bc3210_0))
                (=> v0x7f91a7bc3350_0 E0x7f91a7bc3410)
                (=> v0x7f91a7bc3710_0
                    (and v0x7f91a7bc3350_0
                         E0x7f91a7bc37d0
                         (not v0x7f91a7bc35d0_0)))
                (=> v0x7f91a7bc3710_0 E0x7f91a7bc37d0)
                (=> v0x7f91a7bc39d0_0 a!7)
                a!8
                v0x7f91a7bc39d0_0
                v0x7f91a7bc4850_0
                (= v0x7f91a7bbfb50_0 (= v0x7f91a7bbfa90_0 0.0))
                (= v0x7f91a7bbff90_0 (< v0x7f91a7bbf410_0 2.0))
                (= v0x7f91a7bc0150_0 (ite v0x7f91a7bbff90_0 1.0 0.0))
                (= v0x7f91a7bc0290_0 (+ v0x7f91a7bc0150_0 v0x7f91a7bbf410_0))
                (= v0x7f91a7bc0cd0_0 (= v0x7f91a7bc0c10_0 0.0))
                (= v0x7f91a7bc1150_0 (= v0x7f91a7bc1090_0 0.0))
                (= v0x7f91a7bc17d0_0 (= v0x7f91a7bbf310_0 0.0))
                (= v0x7f91a7bc1bd0_0 (> v0x7f91a7bc0490_0 0.0))
                (= v0x7f91a7bc1d10_0 (+ v0x7f91a7bc0490_0 (- 1.0)))
                (= v0x7f91a7bc1ed0_0
                   (ite v0x7f91a7bc1bd0_0 v0x7f91a7bc1d10_0 v0x7f91a7bc0490_0))
                (= v0x7f91a7bc2f90_0 (not (= v0x7f91a7bc2190_0 0.0)))
                (= v0x7f91a7bc30d0_0 (= v0x7f91a7bc20d0_0 0.0))
                (= v0x7f91a7bc3210_0 (and v0x7f91a7bc2f90_0 v0x7f91a7bc30d0_0))
                (= v0x7f91a7bc35d0_0 (> v0x7f91a7bc2250_0 1.0))
                (= v0x7f91a7bc4350_0 (= v0x7f91a7bc2250_0 2.0))
                (= v0x7f91a7bc4490_0 (= v0x7f91a7bc3a90_0 0.0))
                (= v0x7f91a7bc45d0_0 (or v0x7f91a7bc4490_0 v0x7f91a7bc4350_0))
                (= v0x7f91a7bc4710_0 (xor v0x7f91a7bc45d0_0 true))
                (= v0x7f91a7bc4850_0 (and v0x7f91a7bc30d0_0 v0x7f91a7bc4710_0)))))
  (=> F0x7f91a7bc5550 a!9))))
(assert (=> F0x7f91a7bc5550 F0x7f91a7bc5610))
(assert (=> F0x7f91a7bc53d0 (or F0x7f91a7bc57d0 F0x7f91a7bc56d0)))
(assert (=> F0x7f91a7bc5410 F0x7f91a7bc5550))
(assert (=> pre!entry!0 (=> F0x7f91a7bc54d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f91a7bc5610 (>= v0x7f91a7bbf190_0 0.0))))
(assert (let ((a!1 (not (or (not (>= v0x7f91a7bbf410_0 1.0))
                    (not (<= v0x7f91a7bbf190_0 1.0))
                    (not (<= v0x7f91a7bbf310_0 0.0))
                    (not (<= v0x7f91a7bbf410_0 1.0))
                    (not (>= v0x7f91a7bbf190_0 1.0))
                    (not (>= v0x7f91a7bbf310_0 0.0))))))
(let ((a!2 (=> F0x7f91a7bc5610
               (or a!1
                   (<= v0x7f91a7bbf190_0 0.0)
                   (not (<= v0x7f91a7bbf310_0 0.0))))))
  (=> pre!bb1.i.i!1 a!2))))
(assert (let ((a!1 (not (or (not (>= v0x7f91a7bbe010_0 1.0))
                    (not (<= v0x7f91a7bbf3d0_0 1.0))
                    (not (<= v0x7f91a7bbf4d0_0 0.0))
                    (not (<= v0x7f91a7bbe010_0 1.0))
                    (not (>= v0x7f91a7bbf3d0_0 1.0))
                    (not (>= v0x7f91a7bbf4d0_0 0.0))))))
(let ((a!2 (not (or a!1
                    (<= v0x7f91a7bbf3d0_0 0.0)
                    (not (<= v0x7f91a7bbf4d0_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f91a7bc53d0
           (not (>= v0x7f91a7bbf3d0_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f91a7bc53d0 a!2)
      (and (not post!bb2.i.i16.i.i!0) F0x7f91a7bc5410 true)))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i16.i.i!0)
