(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f8ef5858990 () Bool)
(declare-fun F0x7f8ef5858590 () Bool)
(declare-fun v0x7f8ef5857810_0 () Bool)
(declare-fun v0x7f8ef58576d0_0 () Bool)
(declare-fun v0x7f8ef5856290_0 () Bool)
(declare-fun v0x7f8ef5856150_0 () Bool)
(declare-fun v0x7f8ef5853dd0_0 () Real)
(declare-fun v0x7f8ef58533d0_0 () Bool)
(declare-fun v0x7f8ef5852f10_0 () Real)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f8ef5852510_0 () Real)
(declare-fun v0x7f8ef5852350_0 () Bool)
(declare-fun E0x7f8ef5856fd0 () Bool)
(declare-fun v0x7f8ef5855090_0 () Real)
(declare-fun v0x7f8ef5856b50_0 () Real)
(declare-fun F0x7f8ef5858950 () Bool)
(declare-fun v0x7f8ef5856c90_0 () Bool)
(declare-fun v0x7f8ef5855410_0 () Real)
(declare-fun E0x7f8ef5855b10 () Bool)
(declare-fun E0x7f8ef5856e10 () Bool)
(declare-fun v0x7f8ef5851590_0 () Real)
(declare-fun v0x7f8ef5855290_0 () Real)
(declare-fun v0x7f8ef5855350_0 () Real)
(declare-fun v0x7f8ef58551d0_0 () Bool)
(declare-fun v0x7f8ef5854990_0 () Bool)
(declare-fun v0x7f8ef5856d50_0 () Real)
(declare-fun E0x7f8ef5856850 () Bool)
(declare-fun v0x7f8ef5857450_0 () Bool)
(declare-fun E0x7f8ef5854b90 () Bool)
(declare-fun v0x7f8ef5856a10_0 () Bool)
(declare-fun v0x7f8ef5854ad0_0 () Bool)
(declare-fun v0x7f8ef5854310_0 () Bool)
(declare-fun v0x7f8ef5851690_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7f8ef5854450_0 () Bool)
(declare-fun E0x7f8ef5854090 () Bool)
(declare-fun v0x7f8ef5853fd0_0 () Bool)
(declare-fun v0x7f8ef5851310_0 () Real)
(declare-fun E0x7f8ef58554d0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun E0x7f8ef5853990 () Bool)
(declare-fun v0x7f8ef5853510_0 () Real)
(declare-fun v0x7f8ef5856650_0 () Bool)
(declare-fun v0x7f8ef58563d0_0 () Bool)
(declare-fun E0x7f8ef58537d0 () Bool)
(declare-fun E0x7f8ef58531d0 () Bool)
(declare-fun v0x7f8ef5853110_0 () Bool)
(declare-fun v0x7f8ef5856790_0 () Bool)
(declare-fun v0x7f8ef5851490_0 () Real)
(declare-fun v0x7f8ef5853e90_0 () Bool)
(declare-fun E0x7f8ef5852910 () Bool)
(declare-fun v0x7f8ef5853650_0 () Bool)
(declare-fun E0x7f8ef5854610 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f8ef5852790_0 () Bool)
(declare-fun v0x7f8ef5851e50_0 () Real)
(declare-fun v0x7f8ef5853710_0 () Real)
(declare-fun v0x7f8ef5851f10_0 () Bool)
(declare-fun v0x7f8ef5851d90_0 () Bool)
(declare-fun v0x7f8ef5852fd0_0 () Bool)
(declare-fun v0x7f8ef5854250_0 () Real)
(declare-fun E0x7f8ef5854510 () Bool)
(declare-fun v0x7f8ef5857950_0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun F0x7f8ef5858650 () Bool)
(declare-fun v0x7f8ef5854d90_0 () Bool)
(declare-fun E0x7f8ef5852110 () Bool)
(declare-fun v0x7f8ef5852050_0 () Bool)
(declare-fun F0x7f8ef5858710 () Bool)
(declare-fun v0x7f8ef5852850_0 () Real)
(declare-fun v0x7f8ef5850010_0 () Real)
(declare-fun v0x7f8ef5852650_0 () Real)
(declare-fun v0x7f8ef5856510_0 () Bool)
(declare-fun v0x7f8ef5851750_0 () Real)
(declare-fun v0x7f8ef5857590_0 () Bool)
(declare-fun v0x7f8ef5851550_0 () Real)
(declare-fun v0x7f8ef5851650_0 () Real)
(declare-fun E0x7f8ef5855890 () Bool)
(declare-fun F0x7f8ef5858890 () Bool)
(declare-fun E0x7f8ef5852ad0 () Bool)
(declare-fun v0x7f8ef5850110_0 () Bool)
(declare-fun v0x7f8ef5854ed0_0 () Real)
(declare-fun F0x7f8ef58587d0 () Bool)

(assert (=> F0x7f8ef58587d0
    (and v0x7f8ef5850110_0
         (<= v0x7f8ef5851550_0 0.0)
         (>= v0x7f8ef5851550_0 0.0)
         (<= v0x7f8ef5851650_0 1.0)
         (>= v0x7f8ef5851650_0 1.0)
         (<= v0x7f8ef5851750_0 1.0)
         (>= v0x7f8ef5851750_0 1.0)
         (<= v0x7f8ef5850010_0 0.0)
         (>= v0x7f8ef5850010_0 0.0))))
(assert (=> F0x7f8ef58587d0 F0x7f8ef5858710))
(assert (let ((a!1 (=> v0x7f8ef5852790_0
               (or (and v0x7f8ef5852050_0
                        E0x7f8ef5852910
                        (<= v0x7f8ef5852850_0 v0x7f8ef5852650_0)
                        (>= v0x7f8ef5852850_0 v0x7f8ef5852650_0))
                   (and v0x7f8ef5851d90_0
                        E0x7f8ef5852ad0
                        v0x7f8ef5851f10_0
                        (<= v0x7f8ef5852850_0 v0x7f8ef5851490_0)
                        (>= v0x7f8ef5852850_0 v0x7f8ef5851490_0)))))
      (a!2 (=> v0x7f8ef5852790_0
               (or (and E0x7f8ef5852910 (not E0x7f8ef5852ad0))
                   (and E0x7f8ef5852ad0 (not E0x7f8ef5852910)))))
      (a!3 (=> v0x7f8ef5853650_0
               (or (and v0x7f8ef5853110_0
                        E0x7f8ef58537d0
                        (<= v0x7f8ef5853710_0 v0x7f8ef5853510_0)
                        (>= v0x7f8ef5853710_0 v0x7f8ef5853510_0))
                   (and v0x7f8ef5852790_0
                        E0x7f8ef5853990
                        v0x7f8ef5852fd0_0
                        (<= v0x7f8ef5853710_0 v0x7f8ef5851310_0)
                        (>= v0x7f8ef5853710_0 v0x7f8ef5851310_0)))))
      (a!4 (=> v0x7f8ef5853650_0
               (or (and E0x7f8ef58537d0 (not E0x7f8ef5853990))
                   (and E0x7f8ef5853990 (not E0x7f8ef58537d0)))))
      (a!5 (=> v0x7f8ef5854450_0
               (or (and v0x7f8ef5853fd0_0 E0x7f8ef5854510 v0x7f8ef5854310_0)
                   (and v0x7f8ef5853650_0
                        E0x7f8ef5854610
                        (not v0x7f8ef5853e90_0)))))
      (a!6 (=> v0x7f8ef5854450_0
               (or (and E0x7f8ef5854510 (not E0x7f8ef5854610))
                   (and E0x7f8ef5854610 (not E0x7f8ef5854510)))))
      (a!7 (or (and v0x7f8ef5854ad0_0
                    E0x7f8ef58554d0
                    (and (<= v0x7f8ef5855290_0 v0x7f8ef5851690_0)
                         (>= v0x7f8ef5855290_0 v0x7f8ef5851690_0))
                    (and (<= v0x7f8ef5855350_0 v0x7f8ef5851590_0)
                         (>= v0x7f8ef5855350_0 v0x7f8ef5851590_0))
                    (<= v0x7f8ef5855410_0 v0x7f8ef5855090_0)
                    (>= v0x7f8ef5855410_0 v0x7f8ef5855090_0))
               (and v0x7f8ef5854450_0
                    E0x7f8ef5855890
                    v0x7f8ef5854990_0
                    (and (<= v0x7f8ef5855290_0 v0x7f8ef5851690_0)
                         (>= v0x7f8ef5855290_0 v0x7f8ef5851690_0))
                    (and (<= v0x7f8ef5855350_0 v0x7f8ef5851590_0)
                         (>= v0x7f8ef5855350_0 v0x7f8ef5851590_0))
                    (and (<= v0x7f8ef5855410_0 v0x7f8ef5852850_0)
                         (>= v0x7f8ef5855410_0 v0x7f8ef5852850_0)))
               (and v0x7f8ef5853fd0_0
                    E0x7f8ef5855b10
                    (not v0x7f8ef5854310_0)
                    (<= v0x7f8ef5855290_0 0.0)
                    (>= v0x7f8ef5855290_0 0.0)
                    (<= v0x7f8ef5855350_0 0.0)
                    (>= v0x7f8ef5855350_0 0.0)
                    (and (<= v0x7f8ef5855410_0 v0x7f8ef5852850_0)
                         (>= v0x7f8ef5855410_0 v0x7f8ef5852850_0)))))
      (a!8 (=> v0x7f8ef58551d0_0
               (or (and E0x7f8ef58554d0
                        (not E0x7f8ef5855890)
                        (not E0x7f8ef5855b10))
                   (and E0x7f8ef5855890
                        (not E0x7f8ef58554d0)
                        (not E0x7f8ef5855b10))
                   (and E0x7f8ef5855b10
                        (not E0x7f8ef58554d0)
                        (not E0x7f8ef5855890)))))
      (a!9 (=> v0x7f8ef5856c90_0
               (or (and v0x7f8ef5856790_0
                        E0x7f8ef5856e10
                        (<= v0x7f8ef5856d50_0 v0x7f8ef5856b50_0)
                        (>= v0x7f8ef5856d50_0 v0x7f8ef5856b50_0))
                   (and v0x7f8ef58551d0_0
                        E0x7f8ef5856fd0
                        (not v0x7f8ef5856650_0)
                        (<= v0x7f8ef5856d50_0 v0x7f8ef5855290_0)
                        (>= v0x7f8ef5856d50_0 v0x7f8ef5855290_0)))))
      (a!10 (=> v0x7f8ef5856c90_0
                (or (and E0x7f8ef5856e10 (not E0x7f8ef5856fd0))
                    (and E0x7f8ef5856fd0 (not E0x7f8ef5856e10))))))
(let ((a!11 (and (=> v0x7f8ef5852050_0
                     (and v0x7f8ef5851d90_0
                          E0x7f8ef5852110
                          (not v0x7f8ef5851f10_0)))
                 (=> v0x7f8ef5852050_0 E0x7f8ef5852110)
                 a!1
                 a!2
                 (=> v0x7f8ef5853110_0
                     (and v0x7f8ef5852790_0
                          E0x7f8ef58531d0
                          (not v0x7f8ef5852fd0_0)))
                 (=> v0x7f8ef5853110_0 E0x7f8ef58531d0)
                 a!3
                 a!4
                 (=> v0x7f8ef5853fd0_0
                     (and v0x7f8ef5853650_0 E0x7f8ef5854090 v0x7f8ef5853e90_0))
                 (=> v0x7f8ef5853fd0_0 E0x7f8ef5854090)
                 a!5
                 a!6
                 (=> v0x7f8ef5854ad0_0
                     (and v0x7f8ef5854450_0
                          E0x7f8ef5854b90
                          (not v0x7f8ef5854990_0)))
                 (=> v0x7f8ef5854ad0_0 E0x7f8ef5854b90)
                 (=> v0x7f8ef58551d0_0 a!7)
                 a!8
                 (=> v0x7f8ef5856790_0
                     (and v0x7f8ef58551d0_0 E0x7f8ef5856850 v0x7f8ef5856650_0))
                 (=> v0x7f8ef5856790_0 E0x7f8ef5856850)
                 a!9
                 a!10
                 v0x7f8ef5856c90_0
                 (not v0x7f8ef5857950_0)
                 (<= v0x7f8ef5851550_0 v0x7f8ef5853710_0)
                 (>= v0x7f8ef5851550_0 v0x7f8ef5853710_0)
                 (<= v0x7f8ef5851650_0 v0x7f8ef5855410_0)
                 (>= v0x7f8ef5851650_0 v0x7f8ef5855410_0)
                 (<= v0x7f8ef5851750_0 v0x7f8ef5855350_0)
                 (>= v0x7f8ef5851750_0 v0x7f8ef5855350_0)
                 (<= v0x7f8ef5850010_0 v0x7f8ef5856d50_0)
                 (>= v0x7f8ef5850010_0 v0x7f8ef5856d50_0)
                 (= v0x7f8ef5851f10_0 (= v0x7f8ef5851e50_0 0.0))
                 (= v0x7f8ef5852350_0 (< v0x7f8ef5851490_0 2.0))
                 (= v0x7f8ef5852510_0 (ite v0x7f8ef5852350_0 1.0 0.0))
                 (= v0x7f8ef5852650_0 (+ v0x7f8ef5852510_0 v0x7f8ef5851490_0))
                 (= v0x7f8ef5852fd0_0 (= v0x7f8ef5852f10_0 0.0))
                 (= v0x7f8ef58533d0_0 (= v0x7f8ef5851310_0 0.0))
                 (= v0x7f8ef5853510_0 (ite v0x7f8ef58533d0_0 1.0 0.0))
                 (= v0x7f8ef5853e90_0 (= v0x7f8ef5853dd0_0 0.0))
                 (= v0x7f8ef5854310_0 (= v0x7f8ef5854250_0 0.0))
                 (= v0x7f8ef5854990_0 (= v0x7f8ef5851690_0 0.0))
                 (= v0x7f8ef5854d90_0 (> v0x7f8ef5852850_0 0.0))
                 (= v0x7f8ef5854ed0_0 (+ v0x7f8ef5852850_0 (- 1.0)))
                 (= v0x7f8ef5855090_0
                    (ite v0x7f8ef5854d90_0 v0x7f8ef5854ed0_0 v0x7f8ef5852850_0))
                 (= v0x7f8ef5856150_0 (not (= v0x7f8ef5855350_0 0.0)))
                 (= v0x7f8ef5856290_0 (= v0x7f8ef5855290_0 0.0))
                 (= v0x7f8ef58563d0_0 (> v0x7f8ef5855410_0 1.0))
                 (= v0x7f8ef5856510_0 (and v0x7f8ef5856150_0 v0x7f8ef5856290_0))
                 (= v0x7f8ef5856650_0 (and v0x7f8ef5856510_0 v0x7f8ef58563d0_0))
                 (= v0x7f8ef5856a10_0 (= v0x7f8ef5853710_0 0.0))
                 (= v0x7f8ef5856b50_0
                    (ite v0x7f8ef5856a10_0 1.0 v0x7f8ef5855290_0))
                 (= v0x7f8ef5857450_0 (= v0x7f8ef5855410_0 2.0))
                 (= v0x7f8ef5857590_0 (= v0x7f8ef5856d50_0 0.0))
                 (= v0x7f8ef58576d0_0 (or v0x7f8ef5857590_0 v0x7f8ef5857450_0))
                 (= v0x7f8ef5857810_0 (xor v0x7f8ef58576d0_0 true))
                 (= v0x7f8ef5857950_0 (and v0x7f8ef5856290_0 v0x7f8ef5857810_0)))))
  (=> F0x7f8ef5858650 a!11))))
(assert (=> F0x7f8ef5858650 F0x7f8ef5858590))
(assert (let ((a!1 (=> v0x7f8ef5852790_0
               (or (and v0x7f8ef5852050_0
                        E0x7f8ef5852910
                        (<= v0x7f8ef5852850_0 v0x7f8ef5852650_0)
                        (>= v0x7f8ef5852850_0 v0x7f8ef5852650_0))
                   (and v0x7f8ef5851d90_0
                        E0x7f8ef5852ad0
                        v0x7f8ef5851f10_0
                        (<= v0x7f8ef5852850_0 v0x7f8ef5851490_0)
                        (>= v0x7f8ef5852850_0 v0x7f8ef5851490_0)))))
      (a!2 (=> v0x7f8ef5852790_0
               (or (and E0x7f8ef5852910 (not E0x7f8ef5852ad0))
                   (and E0x7f8ef5852ad0 (not E0x7f8ef5852910)))))
      (a!3 (=> v0x7f8ef5853650_0
               (or (and v0x7f8ef5853110_0
                        E0x7f8ef58537d0
                        (<= v0x7f8ef5853710_0 v0x7f8ef5853510_0)
                        (>= v0x7f8ef5853710_0 v0x7f8ef5853510_0))
                   (and v0x7f8ef5852790_0
                        E0x7f8ef5853990
                        v0x7f8ef5852fd0_0
                        (<= v0x7f8ef5853710_0 v0x7f8ef5851310_0)
                        (>= v0x7f8ef5853710_0 v0x7f8ef5851310_0)))))
      (a!4 (=> v0x7f8ef5853650_0
               (or (and E0x7f8ef58537d0 (not E0x7f8ef5853990))
                   (and E0x7f8ef5853990 (not E0x7f8ef58537d0)))))
      (a!5 (=> v0x7f8ef5854450_0
               (or (and v0x7f8ef5853fd0_0 E0x7f8ef5854510 v0x7f8ef5854310_0)
                   (and v0x7f8ef5853650_0
                        E0x7f8ef5854610
                        (not v0x7f8ef5853e90_0)))))
      (a!6 (=> v0x7f8ef5854450_0
               (or (and E0x7f8ef5854510 (not E0x7f8ef5854610))
                   (and E0x7f8ef5854610 (not E0x7f8ef5854510)))))
      (a!7 (or (and v0x7f8ef5854ad0_0
                    E0x7f8ef58554d0
                    (and (<= v0x7f8ef5855290_0 v0x7f8ef5851690_0)
                         (>= v0x7f8ef5855290_0 v0x7f8ef5851690_0))
                    (and (<= v0x7f8ef5855350_0 v0x7f8ef5851590_0)
                         (>= v0x7f8ef5855350_0 v0x7f8ef5851590_0))
                    (<= v0x7f8ef5855410_0 v0x7f8ef5855090_0)
                    (>= v0x7f8ef5855410_0 v0x7f8ef5855090_0))
               (and v0x7f8ef5854450_0
                    E0x7f8ef5855890
                    v0x7f8ef5854990_0
                    (and (<= v0x7f8ef5855290_0 v0x7f8ef5851690_0)
                         (>= v0x7f8ef5855290_0 v0x7f8ef5851690_0))
                    (and (<= v0x7f8ef5855350_0 v0x7f8ef5851590_0)
                         (>= v0x7f8ef5855350_0 v0x7f8ef5851590_0))
                    (and (<= v0x7f8ef5855410_0 v0x7f8ef5852850_0)
                         (>= v0x7f8ef5855410_0 v0x7f8ef5852850_0)))
               (and v0x7f8ef5853fd0_0
                    E0x7f8ef5855b10
                    (not v0x7f8ef5854310_0)
                    (<= v0x7f8ef5855290_0 0.0)
                    (>= v0x7f8ef5855290_0 0.0)
                    (<= v0x7f8ef5855350_0 0.0)
                    (>= v0x7f8ef5855350_0 0.0)
                    (and (<= v0x7f8ef5855410_0 v0x7f8ef5852850_0)
                         (>= v0x7f8ef5855410_0 v0x7f8ef5852850_0)))))
      (a!8 (=> v0x7f8ef58551d0_0
               (or (and E0x7f8ef58554d0
                        (not E0x7f8ef5855890)
                        (not E0x7f8ef5855b10))
                   (and E0x7f8ef5855890
                        (not E0x7f8ef58554d0)
                        (not E0x7f8ef5855b10))
                   (and E0x7f8ef5855b10
                        (not E0x7f8ef58554d0)
                        (not E0x7f8ef5855890)))))
      (a!9 (=> v0x7f8ef5856c90_0
               (or (and v0x7f8ef5856790_0
                        E0x7f8ef5856e10
                        (<= v0x7f8ef5856d50_0 v0x7f8ef5856b50_0)
                        (>= v0x7f8ef5856d50_0 v0x7f8ef5856b50_0))
                   (and v0x7f8ef58551d0_0
                        E0x7f8ef5856fd0
                        (not v0x7f8ef5856650_0)
                        (<= v0x7f8ef5856d50_0 v0x7f8ef5855290_0)
                        (>= v0x7f8ef5856d50_0 v0x7f8ef5855290_0)))))
      (a!10 (=> v0x7f8ef5856c90_0
                (or (and E0x7f8ef5856e10 (not E0x7f8ef5856fd0))
                    (and E0x7f8ef5856fd0 (not E0x7f8ef5856e10))))))
(let ((a!11 (and (=> v0x7f8ef5852050_0
                     (and v0x7f8ef5851d90_0
                          E0x7f8ef5852110
                          (not v0x7f8ef5851f10_0)))
                 (=> v0x7f8ef5852050_0 E0x7f8ef5852110)
                 a!1
                 a!2
                 (=> v0x7f8ef5853110_0
                     (and v0x7f8ef5852790_0
                          E0x7f8ef58531d0
                          (not v0x7f8ef5852fd0_0)))
                 (=> v0x7f8ef5853110_0 E0x7f8ef58531d0)
                 a!3
                 a!4
                 (=> v0x7f8ef5853fd0_0
                     (and v0x7f8ef5853650_0 E0x7f8ef5854090 v0x7f8ef5853e90_0))
                 (=> v0x7f8ef5853fd0_0 E0x7f8ef5854090)
                 a!5
                 a!6
                 (=> v0x7f8ef5854ad0_0
                     (and v0x7f8ef5854450_0
                          E0x7f8ef5854b90
                          (not v0x7f8ef5854990_0)))
                 (=> v0x7f8ef5854ad0_0 E0x7f8ef5854b90)
                 (=> v0x7f8ef58551d0_0 a!7)
                 a!8
                 (=> v0x7f8ef5856790_0
                     (and v0x7f8ef58551d0_0 E0x7f8ef5856850 v0x7f8ef5856650_0))
                 (=> v0x7f8ef5856790_0 E0x7f8ef5856850)
                 a!9
                 a!10
                 v0x7f8ef5856c90_0
                 v0x7f8ef5857950_0
                 (= v0x7f8ef5851f10_0 (= v0x7f8ef5851e50_0 0.0))
                 (= v0x7f8ef5852350_0 (< v0x7f8ef5851490_0 2.0))
                 (= v0x7f8ef5852510_0 (ite v0x7f8ef5852350_0 1.0 0.0))
                 (= v0x7f8ef5852650_0 (+ v0x7f8ef5852510_0 v0x7f8ef5851490_0))
                 (= v0x7f8ef5852fd0_0 (= v0x7f8ef5852f10_0 0.0))
                 (= v0x7f8ef58533d0_0 (= v0x7f8ef5851310_0 0.0))
                 (= v0x7f8ef5853510_0 (ite v0x7f8ef58533d0_0 1.0 0.0))
                 (= v0x7f8ef5853e90_0 (= v0x7f8ef5853dd0_0 0.0))
                 (= v0x7f8ef5854310_0 (= v0x7f8ef5854250_0 0.0))
                 (= v0x7f8ef5854990_0 (= v0x7f8ef5851690_0 0.0))
                 (= v0x7f8ef5854d90_0 (> v0x7f8ef5852850_0 0.0))
                 (= v0x7f8ef5854ed0_0 (+ v0x7f8ef5852850_0 (- 1.0)))
                 (= v0x7f8ef5855090_0
                    (ite v0x7f8ef5854d90_0 v0x7f8ef5854ed0_0 v0x7f8ef5852850_0))
                 (= v0x7f8ef5856150_0 (not (= v0x7f8ef5855350_0 0.0)))
                 (= v0x7f8ef5856290_0 (= v0x7f8ef5855290_0 0.0))
                 (= v0x7f8ef58563d0_0 (> v0x7f8ef5855410_0 1.0))
                 (= v0x7f8ef5856510_0 (and v0x7f8ef5856150_0 v0x7f8ef5856290_0))
                 (= v0x7f8ef5856650_0 (and v0x7f8ef5856510_0 v0x7f8ef58563d0_0))
                 (= v0x7f8ef5856a10_0 (= v0x7f8ef5853710_0 0.0))
                 (= v0x7f8ef5856b50_0
                    (ite v0x7f8ef5856a10_0 1.0 v0x7f8ef5855290_0))
                 (= v0x7f8ef5857450_0 (= v0x7f8ef5855410_0 2.0))
                 (= v0x7f8ef5857590_0 (= v0x7f8ef5856d50_0 0.0))
                 (= v0x7f8ef58576d0_0 (or v0x7f8ef5857590_0 v0x7f8ef5857450_0))
                 (= v0x7f8ef5857810_0 (xor v0x7f8ef58576d0_0 true))
                 (= v0x7f8ef5857950_0 (and v0x7f8ef5856290_0 v0x7f8ef5857810_0)))))
  (=> F0x7f8ef5858890 a!11))))
(assert (=> F0x7f8ef5858890 F0x7f8ef5858590))
(assert (=> F0x7f8ef5858990 (or F0x7f8ef58587d0 F0x7f8ef5858650)))
(assert (=> F0x7f8ef5858950 F0x7f8ef5858890))
(assert (=> pre!entry!0 (=> F0x7f8ef5858710 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f8ef5858590 (>= v0x7f8ef5851590_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f8ef5858590 (<= v0x7f8ef5851490_0 2.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f8ef5858590
        (or (<= v0x7f8ef5851490_0 1.0) (>= v0x7f8ef5851490_0 2.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f8ef5858590 (>= v0x7f8ef5851490_0 0.0))))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f8ef5858590
        (or (>= v0x7f8ef5851490_0 1.0) (<= v0x7f8ef5851490_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!2)
                F0x7f8ef5858990
                (not (or (<= v0x7f8ef5851650_0 1.0) (>= v0x7f8ef5851650_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!4)
                F0x7f8ef5858990
                (not (or (>= v0x7f8ef5851650_0 1.0) (<= v0x7f8ef5851650_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f8ef5858990
           (not (>= v0x7f8ef5851750_0 0.0)))
      (and (not post!bb1.i.i!1)
           F0x7f8ef5858990
           (not (<= v0x7f8ef5851650_0 2.0)))
      a!1
      (and (not post!bb1.i.i!3)
           F0x7f8ef5858990
           (not (>= v0x7f8ef5851650_0 0.0)))
      a!2
      (and (not post!bb2.i.i23.i.i!0) F0x7f8ef5858950 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i23.i.i!0)
