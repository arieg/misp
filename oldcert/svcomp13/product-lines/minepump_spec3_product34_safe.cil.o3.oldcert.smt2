(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7fcc3bc8e7d0 () Bool)
(declare-fun v0x7fcc3bc8d810_0 () Bool)
(declare-fun v0x7fcc3bc8d6d0_0 () Bool)
(declare-fun v0x7fcc3bc89f90_0 () Bool)
(declare-fun v0x7fcc3bc8c590_0 () Real)
(declare-fun E0x7fcc3bc8d0d0 () Bool)
(declare-fun v0x7fcc3bc8a150_0 () Real)
(declare-fun v0x7fcc3bc89410_0 () Real)
(declare-fun v0x7fcc3bc8d590_0 () Bool)
(declare-fun v0x7fcc3bc8ca50_0 () Real)
(declare-fun E0x7fcc3bc8cbd0 () Bool)
(declare-fun v0x7fcc3bc8c990_0 () Bool)
(declare-fun E0x7fcc3bc8c790 () Bool)
(declare-fun v0x7fcc3bc8cb10_0 () Real)
(declare-fun E0x7fcc3bc8bcd0 () Bool)
(declare-fun v0x7fcc3bc89a90_0 () Real)
(declare-fun v0x7fcc3bc8bc10_0 () Bool)
(declare-fun v0x7fcc3bc89190_0 () Real)
(declare-fun v0x7fcc3bc8d950_0 () Bool)
(declare-fun v0x7fcc3bc8b010_0 () Bool)
(declare-fun E0x7fcc3bc8b5d0 () Bool)
(declare-fun v0x7fcc3bc8b150_0 () Real)
(declare-fun E0x7fcc3bc8b410 () Bool)
(declare-fun v0x7fcc3bc8ac10_0 () Bool)
(declare-fun E0x7fcc3bc8ae10 () Bool)
(declare-fun v0x7fcc3bc89310_0 () Real)
(declare-fun v0x7fcc3bc8b290_0 () Bool)
(declare-fun E0x7fcc3bc8a710 () Bool)
(declare-fun E0x7fcc3bc8a550 () Bool)
(declare-fun v0x7fcc3bc8da90_0 () Bool)
(declare-fun v0x7fcc3bc8c6d0_0 () Bool)
(declare-fun v0x7fcc3bc8a3d0_0 () Bool)
(declare-fun v0x7fcc3bc8bfd0_0 () Bool)
(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun v0x7fcc3bc89b50_0 () Bool)
(declare-fun F0x7fcc3bc8e890 () Bool)
(declare-fun v0x7fcc3bc8c3d0_0 () Real)
(declare-fun v0x7fcc3bc8a290_0 () Real)
(declare-fun F0x7fcc3bc8e990 () Bool)
(declare-fun v0x7fcc3bc899d0_0 () Bool)
(declare-fun v0x7fcc3bc8bad0_0 () Bool)
(declare-fun v0x7fcc3bc8a490_0 () Real)
(declare-fun F0x7fcc3bc8e650 () Bool)
(declare-fun v0x7fcc3bc8ab50_0 () Real)
(declare-fun v0x7fcc3bc88010_0 () Real)
(declare-fun E0x7fcc3bc8c090 () Bool)
(declare-fun v0x7fcc3bc894d0_0 () Real)
(declare-fun E0x7fcc3bc8ce90 () Bool)
(declare-fun v0x7fcc3bc8be90_0 () Bool)
(declare-fun v0x7fcc3bc89c90_0 () Bool)
(declare-fun v0x7fcc3bc893d0_0 () Real)
(declare-fun v0x7fcc3bc88110_0 () Bool)
(declare-fun E0x7fcc3bc89d50 () Bool)
(declare-fun F0x7fcc3bc8e710 () Bool)
(declare-fun F0x7fcc3bc8e9d0 () Bool)
(declare-fun v0x7fcc3bc8c290_0 () Bool)
(declare-fun F0x7fcc3bc8e590 () Bool)
(declare-fun v0x7fcc3bc8ad50_0 () Bool)
(declare-fun v0x7fcc3bc8b350_0 () Real)

(assert (=> F0x7fcc3bc8e590
    (and v0x7fcc3bc88110_0
         (<= v0x7fcc3bc893d0_0 0.0)
         (>= v0x7fcc3bc893d0_0 0.0)
         (<= v0x7fcc3bc894d0_0 1.0)
         (>= v0x7fcc3bc894d0_0 1.0)
         (<= v0x7fcc3bc88010_0 0.0)
         (>= v0x7fcc3bc88010_0 0.0))))
(assert (=> F0x7fcc3bc8e590 F0x7fcc3bc8e650))
(assert (let ((a!1 (=> v0x7fcc3bc8a3d0_0
               (or (and v0x7fcc3bc89c90_0
                        E0x7fcc3bc8a550
                        (<= v0x7fcc3bc8a490_0 v0x7fcc3bc8a290_0)
                        (>= v0x7fcc3bc8a490_0 v0x7fcc3bc8a290_0))
                   (and v0x7fcc3bc899d0_0
                        E0x7fcc3bc8a710
                        v0x7fcc3bc89b50_0
                        (<= v0x7fcc3bc8a490_0 v0x7fcc3bc89310_0)
                        (>= v0x7fcc3bc8a490_0 v0x7fcc3bc89310_0)))))
      (a!2 (=> v0x7fcc3bc8a3d0_0
               (or (and E0x7fcc3bc8a550 (not E0x7fcc3bc8a710))
                   (and E0x7fcc3bc8a710 (not E0x7fcc3bc8a550)))))
      (a!3 (=> v0x7fcc3bc8b290_0
               (or (and v0x7fcc3bc8ad50_0
                        E0x7fcc3bc8b410
                        (<= v0x7fcc3bc8b350_0 v0x7fcc3bc8b150_0)
                        (>= v0x7fcc3bc8b350_0 v0x7fcc3bc8b150_0))
                   (and v0x7fcc3bc8a3d0_0
                        E0x7fcc3bc8b5d0
                        v0x7fcc3bc8ac10_0
                        (<= v0x7fcc3bc8b350_0 v0x7fcc3bc89190_0)
                        (>= v0x7fcc3bc8b350_0 v0x7fcc3bc89190_0)))))
      (a!4 (=> v0x7fcc3bc8b290_0
               (or (and E0x7fcc3bc8b410 (not E0x7fcc3bc8b5d0))
                   (and E0x7fcc3bc8b5d0 (not E0x7fcc3bc8b410)))))
      (a!5 (or (and v0x7fcc3bc8c6d0_0
                    E0x7fcc3bc8cbd0
                    (and (<= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0)
                         (>= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0))
                    (and (<= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)
                         (>= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)))
               (and v0x7fcc3bc8bc10_0
                    E0x7fcc3bc8ce90
                    v0x7fcc3bc8be90_0
                    (and (<= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0)
                         (>= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0))
                    (<= v0x7fcc3bc8cb10_0 1.0)
                    (>= v0x7fcc3bc8cb10_0 1.0))
               (and v0x7fcc3bc8bfd0_0
                    E0x7fcc3bc8d0d0
                    (<= v0x7fcc3bc8ca50_0 v0x7fcc3bc8c590_0)
                    (>= v0x7fcc3bc8ca50_0 v0x7fcc3bc8c590_0)
                    (and (<= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)
                         (>= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)))))
      (a!6 (=> v0x7fcc3bc8c990_0
               (or (and E0x7fcc3bc8cbd0
                        (not E0x7fcc3bc8ce90)
                        (not E0x7fcc3bc8d0d0))
                   (and E0x7fcc3bc8ce90
                        (not E0x7fcc3bc8cbd0)
                        (not E0x7fcc3bc8d0d0))
                   (and E0x7fcc3bc8d0d0
                        (not E0x7fcc3bc8cbd0)
                        (not E0x7fcc3bc8ce90))))))
(let ((a!7 (and (=> v0x7fcc3bc89c90_0
                    (and v0x7fcc3bc899d0_0
                         E0x7fcc3bc89d50
                         (not v0x7fcc3bc89b50_0)))
                (=> v0x7fcc3bc89c90_0 E0x7fcc3bc89d50)
                a!1
                a!2
                (=> v0x7fcc3bc8ad50_0
                    (and v0x7fcc3bc8a3d0_0
                         E0x7fcc3bc8ae10
                         (not v0x7fcc3bc8ac10_0)))
                (=> v0x7fcc3bc8ad50_0 E0x7fcc3bc8ae10)
                a!3
                a!4
                (=> v0x7fcc3bc8bc10_0
                    (and v0x7fcc3bc8b290_0 E0x7fcc3bc8bcd0 v0x7fcc3bc8bad0_0))
                (=> v0x7fcc3bc8bc10_0 E0x7fcc3bc8bcd0)
                (=> v0x7fcc3bc8bfd0_0
                    (and v0x7fcc3bc8b290_0
                         E0x7fcc3bc8c090
                         (not v0x7fcc3bc8bad0_0)))
                (=> v0x7fcc3bc8bfd0_0 E0x7fcc3bc8c090)
                (=> v0x7fcc3bc8c6d0_0
                    (and v0x7fcc3bc8bc10_0
                         E0x7fcc3bc8c790
                         (not v0x7fcc3bc8be90_0)))
                (=> v0x7fcc3bc8c6d0_0 E0x7fcc3bc8c790)
                (=> v0x7fcc3bc8c990_0 a!5)
                a!6
                v0x7fcc3bc8c990_0
                (not v0x7fcc3bc8da90_0)
                (<= v0x7fcc3bc893d0_0 v0x7fcc3bc8b350_0)
                (>= v0x7fcc3bc893d0_0 v0x7fcc3bc8b350_0)
                (<= v0x7fcc3bc894d0_0 v0x7fcc3bc8ca50_0)
                (>= v0x7fcc3bc894d0_0 v0x7fcc3bc8ca50_0)
                (<= v0x7fcc3bc88010_0 v0x7fcc3bc8cb10_0)
                (>= v0x7fcc3bc88010_0 v0x7fcc3bc8cb10_0)
                (= v0x7fcc3bc89b50_0 (= v0x7fcc3bc89a90_0 0.0))
                (= v0x7fcc3bc89f90_0 (< v0x7fcc3bc89310_0 2.0))
                (= v0x7fcc3bc8a150_0 (ite v0x7fcc3bc89f90_0 1.0 0.0))
                (= v0x7fcc3bc8a290_0 (+ v0x7fcc3bc8a150_0 v0x7fcc3bc89310_0))
                (= v0x7fcc3bc8ac10_0 (= v0x7fcc3bc8ab50_0 0.0))
                (= v0x7fcc3bc8b010_0 (= v0x7fcc3bc89190_0 0.0))
                (= v0x7fcc3bc8b150_0 (ite v0x7fcc3bc8b010_0 1.0 0.0))
                (= v0x7fcc3bc8bad0_0 (= v0x7fcc3bc89410_0 0.0))
                (= v0x7fcc3bc8be90_0 (> v0x7fcc3bc8a490_0 1.0))
                (= v0x7fcc3bc8c290_0 (> v0x7fcc3bc8a490_0 0.0))
                (= v0x7fcc3bc8c3d0_0 (+ v0x7fcc3bc8a490_0 (- 1.0)))
                (= v0x7fcc3bc8c590_0
                   (ite v0x7fcc3bc8c290_0 v0x7fcc3bc8c3d0_0 v0x7fcc3bc8a490_0))
                (= v0x7fcc3bc8d590_0 (= v0x7fcc3bc8b350_0 0.0))
                (= v0x7fcc3bc8d6d0_0 (= v0x7fcc3bc8ca50_0 2.0))
                (= v0x7fcc3bc8d810_0 (= v0x7fcc3bc8cb10_0 0.0))
                (= v0x7fcc3bc8d950_0 (and v0x7fcc3bc8d6d0_0 v0x7fcc3bc8d590_0))
                (= v0x7fcc3bc8da90_0 (and v0x7fcc3bc8d950_0 v0x7fcc3bc8d810_0)))))
  (=> F0x7fcc3bc8e710 a!7))))
(assert (=> F0x7fcc3bc8e710 F0x7fcc3bc8e7d0))
(assert (let ((a!1 (=> v0x7fcc3bc8a3d0_0
               (or (and v0x7fcc3bc89c90_0
                        E0x7fcc3bc8a550
                        (<= v0x7fcc3bc8a490_0 v0x7fcc3bc8a290_0)
                        (>= v0x7fcc3bc8a490_0 v0x7fcc3bc8a290_0))
                   (and v0x7fcc3bc899d0_0
                        E0x7fcc3bc8a710
                        v0x7fcc3bc89b50_0
                        (<= v0x7fcc3bc8a490_0 v0x7fcc3bc89310_0)
                        (>= v0x7fcc3bc8a490_0 v0x7fcc3bc89310_0)))))
      (a!2 (=> v0x7fcc3bc8a3d0_0
               (or (and E0x7fcc3bc8a550 (not E0x7fcc3bc8a710))
                   (and E0x7fcc3bc8a710 (not E0x7fcc3bc8a550)))))
      (a!3 (=> v0x7fcc3bc8b290_0
               (or (and v0x7fcc3bc8ad50_0
                        E0x7fcc3bc8b410
                        (<= v0x7fcc3bc8b350_0 v0x7fcc3bc8b150_0)
                        (>= v0x7fcc3bc8b350_0 v0x7fcc3bc8b150_0))
                   (and v0x7fcc3bc8a3d0_0
                        E0x7fcc3bc8b5d0
                        v0x7fcc3bc8ac10_0
                        (<= v0x7fcc3bc8b350_0 v0x7fcc3bc89190_0)
                        (>= v0x7fcc3bc8b350_0 v0x7fcc3bc89190_0)))))
      (a!4 (=> v0x7fcc3bc8b290_0
               (or (and E0x7fcc3bc8b410 (not E0x7fcc3bc8b5d0))
                   (and E0x7fcc3bc8b5d0 (not E0x7fcc3bc8b410)))))
      (a!5 (or (and v0x7fcc3bc8c6d0_0
                    E0x7fcc3bc8cbd0
                    (and (<= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0)
                         (>= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0))
                    (and (<= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)
                         (>= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)))
               (and v0x7fcc3bc8bc10_0
                    E0x7fcc3bc8ce90
                    v0x7fcc3bc8be90_0
                    (and (<= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0)
                         (>= v0x7fcc3bc8ca50_0 v0x7fcc3bc8a490_0))
                    (<= v0x7fcc3bc8cb10_0 1.0)
                    (>= v0x7fcc3bc8cb10_0 1.0))
               (and v0x7fcc3bc8bfd0_0
                    E0x7fcc3bc8d0d0
                    (<= v0x7fcc3bc8ca50_0 v0x7fcc3bc8c590_0)
                    (>= v0x7fcc3bc8ca50_0 v0x7fcc3bc8c590_0)
                    (and (<= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)
                         (>= v0x7fcc3bc8cb10_0 v0x7fcc3bc89410_0)))))
      (a!6 (=> v0x7fcc3bc8c990_0
               (or (and E0x7fcc3bc8cbd0
                        (not E0x7fcc3bc8ce90)
                        (not E0x7fcc3bc8d0d0))
                   (and E0x7fcc3bc8ce90
                        (not E0x7fcc3bc8cbd0)
                        (not E0x7fcc3bc8d0d0))
                   (and E0x7fcc3bc8d0d0
                        (not E0x7fcc3bc8cbd0)
                        (not E0x7fcc3bc8ce90))))))
(let ((a!7 (and (=> v0x7fcc3bc89c90_0
                    (and v0x7fcc3bc899d0_0
                         E0x7fcc3bc89d50
                         (not v0x7fcc3bc89b50_0)))
                (=> v0x7fcc3bc89c90_0 E0x7fcc3bc89d50)
                a!1
                a!2
                (=> v0x7fcc3bc8ad50_0
                    (and v0x7fcc3bc8a3d0_0
                         E0x7fcc3bc8ae10
                         (not v0x7fcc3bc8ac10_0)))
                (=> v0x7fcc3bc8ad50_0 E0x7fcc3bc8ae10)
                a!3
                a!4
                (=> v0x7fcc3bc8bc10_0
                    (and v0x7fcc3bc8b290_0 E0x7fcc3bc8bcd0 v0x7fcc3bc8bad0_0))
                (=> v0x7fcc3bc8bc10_0 E0x7fcc3bc8bcd0)
                (=> v0x7fcc3bc8bfd0_0
                    (and v0x7fcc3bc8b290_0
                         E0x7fcc3bc8c090
                         (not v0x7fcc3bc8bad0_0)))
                (=> v0x7fcc3bc8bfd0_0 E0x7fcc3bc8c090)
                (=> v0x7fcc3bc8c6d0_0
                    (and v0x7fcc3bc8bc10_0
                         E0x7fcc3bc8c790
                         (not v0x7fcc3bc8be90_0)))
                (=> v0x7fcc3bc8c6d0_0 E0x7fcc3bc8c790)
                (=> v0x7fcc3bc8c990_0 a!5)
                a!6
                v0x7fcc3bc8c990_0
                v0x7fcc3bc8da90_0
                (= v0x7fcc3bc89b50_0 (= v0x7fcc3bc89a90_0 0.0))
                (= v0x7fcc3bc89f90_0 (< v0x7fcc3bc89310_0 2.0))
                (= v0x7fcc3bc8a150_0 (ite v0x7fcc3bc89f90_0 1.0 0.0))
                (= v0x7fcc3bc8a290_0 (+ v0x7fcc3bc8a150_0 v0x7fcc3bc89310_0))
                (= v0x7fcc3bc8ac10_0 (= v0x7fcc3bc8ab50_0 0.0))
                (= v0x7fcc3bc8b010_0 (= v0x7fcc3bc89190_0 0.0))
                (= v0x7fcc3bc8b150_0 (ite v0x7fcc3bc8b010_0 1.0 0.0))
                (= v0x7fcc3bc8bad0_0 (= v0x7fcc3bc89410_0 0.0))
                (= v0x7fcc3bc8be90_0 (> v0x7fcc3bc8a490_0 1.0))
                (= v0x7fcc3bc8c290_0 (> v0x7fcc3bc8a490_0 0.0))
                (= v0x7fcc3bc8c3d0_0 (+ v0x7fcc3bc8a490_0 (- 1.0)))
                (= v0x7fcc3bc8c590_0
                   (ite v0x7fcc3bc8c290_0 v0x7fcc3bc8c3d0_0 v0x7fcc3bc8a490_0))
                (= v0x7fcc3bc8d590_0 (= v0x7fcc3bc8b350_0 0.0))
                (= v0x7fcc3bc8d6d0_0 (= v0x7fcc3bc8ca50_0 2.0))
                (= v0x7fcc3bc8d810_0 (= v0x7fcc3bc8cb10_0 0.0))
                (= v0x7fcc3bc8d950_0 (and v0x7fcc3bc8d6d0_0 v0x7fcc3bc8d590_0))
                (= v0x7fcc3bc8da90_0 (and v0x7fcc3bc8d950_0 v0x7fcc3bc8d810_0)))))
  (=> F0x7fcc3bc8e890 a!7))))
(assert (=> F0x7fcc3bc8e890 F0x7fcc3bc8e7d0))
(assert (=> F0x7fcc3bc8e9d0 (or F0x7fcc3bc8e590 F0x7fcc3bc8e710)))
(assert (=> F0x7fcc3bc8e990 F0x7fcc3bc8e890))
(assert (=> pre!entry!0 (=> F0x7fcc3bc8e650 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fcc3bc8e7d0 true)))
(assert (or (and (not post!bb1.i.i!0) F0x7fcc3bc8e9d0 false)
    (and (not post!bb2.i.i16.i.i!0) F0x7fcc3bc8e990 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0)
;(post-assumptions: post!bb1.i.i!0 post!bb2.i.i16.i.i!0)
