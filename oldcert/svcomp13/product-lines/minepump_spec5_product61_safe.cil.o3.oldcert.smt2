(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f9983547910 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun F0x7f9983547550 () Bool)
(declare-fun v0x7f9983546790_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f9983546650_0 () Bool)
(declare-fun v0x7f9983546510_0 () Bool)
(declare-fun v0x7f9983544690_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f9983547850 () Bool)
(declare-fun v0x7f9983543410_0 () Bool)
(declare-fun E0x7f9983545d50 () Bool)
(declare-fun v0x7f9983541810_0 () Real)
(declare-fun E0x7f9983545ad0 () Bool)
(declare-fun v0x7f9983545750_0 () Real)
(declare-fun v0x7f9983545690_0 () Real)
(declare-fun v0x7f9983541e90_0 () Real)
(declare-fun E0x7f9983545810 () Bool)
(declare-fun post!bb2.i.i45.i.i!0 () Bool)
(declare-fun v0x7f99835455d0_0 () Bool)
(declare-fun v0x7f9983545350_0 () Bool)
(declare-fun E0x7f9983544f50 () Bool)
(declare-fun v0x7f9983545110_0 () Bool)
(declare-fun v0x7f9983544ad0_0 () Bool)
(declare-fun v0x7f99835443d0_0 () Bool)
(declare-fun v0x7f9983543750_0 () Real)
(declare-fun E0x7f9983545410 () Bool)
(declare-fun v0x7f9983543690_0 () Bool)
(declare-fun v0x7f9983544290_0 () Bool)
(declare-fun v0x7f9983543150_0 () Bool)
(declare-fun v0x7f9983543010_0 () Bool)
(declare-fun v0x7f9983542890_0 () Real)
(declare-fun E0x7f99835440d0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f9983541710_0 () Real)
(declare-fun v0x7f99835468d0_0 () Bool)
(declare-fun v0x7f9983542690_0 () Real)
(declare-fun E0x7f9983545f50 () Bool)
(declare-fun v0x7f9983542390_0 () Bool)
(declare-fun F0x7f9983547950 () Bool)
(declare-fun v0x7f9983544010_0 () Bool)
(declare-fun v0x7f9983541590_0 () Real)
(declare-fun v0x7f99835447d0_0 () Real)
(declare-fun v0x7f9983544d50_0 () Bool)
(declare-fun E0x7f99835439d0 () Bool)
(declare-fun E0x7f9983542950 () Bool)
(declare-fun v0x7f9983545210_0 () Real)
(declare-fun v0x7f99835427d0_0 () Bool)
(declare-fun v0x7f9983541f50_0 () Bool)
(declare-fun v0x7f9983541dd0_0 () Bool)
(declare-fun v0x7f9983542090_0 () Bool)
(declare-fun E0x7f9983542b10 () Bool)
(declare-fun E0x7f9983544490 () Bool)
(declare-fun F0x7f9983547610 () Bool)
(declare-fun v0x7f9983544990_0 () Real)
(declare-fun E0x7f9983543810 () Bool)
(declare-fun v0x7f9983543ed0_0 () Bool)
(declare-fun v0x7f9983542550_0 () Real)
(declare-fun F0x7f99835476d0 () Bool)
(declare-fun v0x7f9983546a10_0 () Bool)
(declare-fun v0x7f9983544c10_0 () Bool)
(declare-fun v0x7f9983543550_0 () Real)
(declare-fun v0x7f99835417d0_0 () Real)
(declare-fun E0x7f9983542150 () Bool)
(declare-fun E0x7f9983543210 () Bool)
(declare-fun v0x7f998353e110_0 () Bool)
(declare-fun v0x7f99835418d0_0 () Real)
(declare-fun v0x7f9983542f50_0 () Real)
(declare-fun v0x7f9983544e90_0 () Bool)
(declare-fun v0x7f998353e010_0 () Real)
(declare-fun F0x7f9983547790 () Bool)

(assert (=> F0x7f9983547790
    (and v0x7f998353e110_0
         (<= v0x7f99835417d0_0 0.0)
         (>= v0x7f99835417d0_0 0.0)
         (<= v0x7f99835418d0_0 1.0)
         (>= v0x7f99835418d0_0 1.0)
         (<= v0x7f998353e010_0 0.0)
         (>= v0x7f998353e010_0 0.0))))
(assert (=> F0x7f9983547790 F0x7f99835476d0))
(assert (let ((a!1 (=> v0x7f99835427d0_0
               (or (and v0x7f9983542090_0
                        E0x7f9983542950
                        (<= v0x7f9983542890_0 v0x7f9983542690_0)
                        (>= v0x7f9983542890_0 v0x7f9983542690_0))
                   (and v0x7f9983541dd0_0
                        E0x7f9983542b10
                        v0x7f9983541f50_0
                        (<= v0x7f9983542890_0 v0x7f9983541710_0)
                        (>= v0x7f9983542890_0 v0x7f9983541710_0)))))
      (a!2 (=> v0x7f99835427d0_0
               (or (and E0x7f9983542950 (not E0x7f9983542b10))
                   (and E0x7f9983542b10 (not E0x7f9983542950)))))
      (a!3 (=> v0x7f9983543690_0
               (or (and v0x7f9983543150_0
                        E0x7f9983543810
                        (<= v0x7f9983543750_0 v0x7f9983543550_0)
                        (>= v0x7f9983543750_0 v0x7f9983543550_0))
                   (and v0x7f99835427d0_0
                        E0x7f99835439d0
                        v0x7f9983543010_0
                        (<= v0x7f9983543750_0 v0x7f9983541590_0)
                        (>= v0x7f9983543750_0 v0x7f9983541590_0)))))
      (a!4 (=> v0x7f9983543690_0
               (or (and E0x7f9983543810 (not E0x7f99835439d0))
                   (and E0x7f99835439d0 (not E0x7f9983543810)))))
      (a!5 (or (and v0x7f9983544e90_0
                    E0x7f9983545810
                    (and (<= v0x7f9983545690_0 v0x7f9983542890_0)
                         (>= v0x7f9983545690_0 v0x7f9983542890_0))
                    (<= v0x7f9983545750_0 v0x7f9983545210_0)
                    (>= v0x7f9983545750_0 v0x7f9983545210_0))
               (and v0x7f9983544010_0
                    E0x7f9983545ad0
                    (not v0x7f9983544290_0)
                    (and (<= v0x7f9983545690_0 v0x7f9983542890_0)
                         (>= v0x7f9983545690_0 v0x7f9983542890_0))
                    (and (<= v0x7f9983545750_0 v0x7f9983541810_0)
                         (>= v0x7f9983545750_0 v0x7f9983541810_0)))
               (and v0x7f9983545350_0
                    E0x7f9983545d50
                    (and (<= v0x7f9983545690_0 v0x7f9983544990_0)
                         (>= v0x7f9983545690_0 v0x7f9983544990_0))
                    (and (<= v0x7f9983545750_0 v0x7f9983541810_0)
                         (>= v0x7f9983545750_0 v0x7f9983541810_0)))
               (and v0x7f99835443d0_0
                    E0x7f9983545f50
                    (not v0x7f9983544d50_0)
                    (and (<= v0x7f9983545690_0 v0x7f9983544990_0)
                         (>= v0x7f9983545690_0 v0x7f9983544990_0))
                    (<= v0x7f9983545750_0 0.0)
                    (>= v0x7f9983545750_0 0.0))))
      (a!6 (=> v0x7f99835455d0_0
               (or (and E0x7f9983545810
                        (not E0x7f9983545ad0)
                        (not E0x7f9983545d50)
                        (not E0x7f9983545f50))
                   (and E0x7f9983545ad0
                        (not E0x7f9983545810)
                        (not E0x7f9983545d50)
                        (not E0x7f9983545f50))
                   (and E0x7f9983545d50
                        (not E0x7f9983545810)
                        (not E0x7f9983545ad0)
                        (not E0x7f9983545f50))
                   (and E0x7f9983545f50
                        (not E0x7f9983545810)
                        (not E0x7f9983545ad0)
                        (not E0x7f9983545d50))))))
(let ((a!7 (and (=> v0x7f9983542090_0
                    (and v0x7f9983541dd0_0
                         E0x7f9983542150
                         (not v0x7f9983541f50_0)))
                (=> v0x7f9983542090_0 E0x7f9983542150)
                a!1
                a!2
                (=> v0x7f9983543150_0
                    (and v0x7f99835427d0_0
                         E0x7f9983543210
                         (not v0x7f9983543010_0)))
                (=> v0x7f9983543150_0 E0x7f9983543210)
                a!3
                a!4
                (=> v0x7f9983544010_0
                    (and v0x7f9983543690_0 E0x7f99835440d0 v0x7f9983543ed0_0))
                (=> v0x7f9983544010_0 E0x7f99835440d0)
                (=> v0x7f99835443d0_0
                    (and v0x7f9983543690_0
                         E0x7f9983544490
                         (not v0x7f9983543ed0_0)))
                (=> v0x7f99835443d0_0 E0x7f9983544490)
                (=> v0x7f9983544e90_0
                    (and v0x7f9983544010_0 E0x7f9983544f50 v0x7f9983544290_0))
                (=> v0x7f9983544e90_0 E0x7f9983544f50)
                (=> v0x7f9983545350_0
                    (and v0x7f99835443d0_0 E0x7f9983545410 v0x7f9983544d50_0))
                (=> v0x7f9983545350_0 E0x7f9983545410)
                (=> v0x7f99835455d0_0 a!5)
                a!6
                v0x7f99835455d0_0
                (not v0x7f9983546a10_0)
                (<= v0x7f99835417d0_0 v0x7f9983543750_0)
                (>= v0x7f99835417d0_0 v0x7f9983543750_0)
                (<= v0x7f99835418d0_0 v0x7f9983545690_0)
                (>= v0x7f99835418d0_0 v0x7f9983545690_0)
                (<= v0x7f998353e010_0 v0x7f9983545750_0)
                (>= v0x7f998353e010_0 v0x7f9983545750_0)
                (= v0x7f9983541f50_0 (= v0x7f9983541e90_0 0.0))
                (= v0x7f9983542390_0 (< v0x7f9983541710_0 2.0))
                (= v0x7f9983542550_0 (ite v0x7f9983542390_0 1.0 0.0))
                (= v0x7f9983542690_0 (+ v0x7f9983542550_0 v0x7f9983541710_0))
                (= v0x7f9983543010_0 (= v0x7f9983542f50_0 0.0))
                (= v0x7f9983543410_0 (= v0x7f9983541590_0 0.0))
                (= v0x7f9983543550_0 (ite v0x7f9983543410_0 1.0 0.0))
                (= v0x7f9983543ed0_0 (= v0x7f9983541810_0 0.0))
                (= v0x7f9983544290_0 (> v0x7f9983542890_0 1.0))
                (= v0x7f9983544690_0 (> v0x7f9983542890_0 0.0))
                (= v0x7f99835447d0_0 (+ v0x7f9983542890_0 (- 1.0)))
                (= v0x7f9983544990_0
                   (ite v0x7f9983544690_0 v0x7f99835447d0_0 v0x7f9983542890_0))
                (= v0x7f9983544ad0_0 (= v0x7f9983543750_0 0.0))
                (= v0x7f9983544c10_0 (= v0x7f9983544990_0 0.0))
                (= v0x7f9983544d50_0 (and v0x7f9983544ad0_0 v0x7f9983544c10_0))
                (= v0x7f9983545110_0 (= v0x7f9983543750_0 0.0))
                (= v0x7f9983545210_0
                   (ite v0x7f9983545110_0 1.0 v0x7f9983541810_0))
                (= v0x7f9983546510_0 (= v0x7f9983545690_0 2.0))
                (= v0x7f9983546650_0 (= v0x7f9983545750_0 0.0))
                (= v0x7f9983546790_0 (or v0x7f9983546650_0 v0x7f9983546510_0))
                (= v0x7f99835468d0_0 (xor v0x7f9983546790_0 true))
                (= v0x7f9983546a10_0 (and v0x7f9983543ed0_0 v0x7f99835468d0_0)))))
  (=> F0x7f9983547610 a!7))))
(assert (=> F0x7f9983547610 F0x7f9983547550))
(assert (let ((a!1 (=> v0x7f99835427d0_0
               (or (and v0x7f9983542090_0
                        E0x7f9983542950
                        (<= v0x7f9983542890_0 v0x7f9983542690_0)
                        (>= v0x7f9983542890_0 v0x7f9983542690_0))
                   (and v0x7f9983541dd0_0
                        E0x7f9983542b10
                        v0x7f9983541f50_0
                        (<= v0x7f9983542890_0 v0x7f9983541710_0)
                        (>= v0x7f9983542890_0 v0x7f9983541710_0)))))
      (a!2 (=> v0x7f99835427d0_0
               (or (and E0x7f9983542950 (not E0x7f9983542b10))
                   (and E0x7f9983542b10 (not E0x7f9983542950)))))
      (a!3 (=> v0x7f9983543690_0
               (or (and v0x7f9983543150_0
                        E0x7f9983543810
                        (<= v0x7f9983543750_0 v0x7f9983543550_0)
                        (>= v0x7f9983543750_0 v0x7f9983543550_0))
                   (and v0x7f99835427d0_0
                        E0x7f99835439d0
                        v0x7f9983543010_0
                        (<= v0x7f9983543750_0 v0x7f9983541590_0)
                        (>= v0x7f9983543750_0 v0x7f9983541590_0)))))
      (a!4 (=> v0x7f9983543690_0
               (or (and E0x7f9983543810 (not E0x7f99835439d0))
                   (and E0x7f99835439d0 (not E0x7f9983543810)))))
      (a!5 (or (and v0x7f9983544e90_0
                    E0x7f9983545810
                    (and (<= v0x7f9983545690_0 v0x7f9983542890_0)
                         (>= v0x7f9983545690_0 v0x7f9983542890_0))
                    (<= v0x7f9983545750_0 v0x7f9983545210_0)
                    (>= v0x7f9983545750_0 v0x7f9983545210_0))
               (and v0x7f9983544010_0
                    E0x7f9983545ad0
                    (not v0x7f9983544290_0)
                    (and (<= v0x7f9983545690_0 v0x7f9983542890_0)
                         (>= v0x7f9983545690_0 v0x7f9983542890_0))
                    (and (<= v0x7f9983545750_0 v0x7f9983541810_0)
                         (>= v0x7f9983545750_0 v0x7f9983541810_0)))
               (and v0x7f9983545350_0
                    E0x7f9983545d50
                    (and (<= v0x7f9983545690_0 v0x7f9983544990_0)
                         (>= v0x7f9983545690_0 v0x7f9983544990_0))
                    (and (<= v0x7f9983545750_0 v0x7f9983541810_0)
                         (>= v0x7f9983545750_0 v0x7f9983541810_0)))
               (and v0x7f99835443d0_0
                    E0x7f9983545f50
                    (not v0x7f9983544d50_0)
                    (and (<= v0x7f9983545690_0 v0x7f9983544990_0)
                         (>= v0x7f9983545690_0 v0x7f9983544990_0))
                    (<= v0x7f9983545750_0 0.0)
                    (>= v0x7f9983545750_0 0.0))))
      (a!6 (=> v0x7f99835455d0_0
               (or (and E0x7f9983545810
                        (not E0x7f9983545ad0)
                        (not E0x7f9983545d50)
                        (not E0x7f9983545f50))
                   (and E0x7f9983545ad0
                        (not E0x7f9983545810)
                        (not E0x7f9983545d50)
                        (not E0x7f9983545f50))
                   (and E0x7f9983545d50
                        (not E0x7f9983545810)
                        (not E0x7f9983545ad0)
                        (not E0x7f9983545f50))
                   (and E0x7f9983545f50
                        (not E0x7f9983545810)
                        (not E0x7f9983545ad0)
                        (not E0x7f9983545d50))))))
(let ((a!7 (and (=> v0x7f9983542090_0
                    (and v0x7f9983541dd0_0
                         E0x7f9983542150
                         (not v0x7f9983541f50_0)))
                (=> v0x7f9983542090_0 E0x7f9983542150)
                a!1
                a!2
                (=> v0x7f9983543150_0
                    (and v0x7f99835427d0_0
                         E0x7f9983543210
                         (not v0x7f9983543010_0)))
                (=> v0x7f9983543150_0 E0x7f9983543210)
                a!3
                a!4
                (=> v0x7f9983544010_0
                    (and v0x7f9983543690_0 E0x7f99835440d0 v0x7f9983543ed0_0))
                (=> v0x7f9983544010_0 E0x7f99835440d0)
                (=> v0x7f99835443d0_0
                    (and v0x7f9983543690_0
                         E0x7f9983544490
                         (not v0x7f9983543ed0_0)))
                (=> v0x7f99835443d0_0 E0x7f9983544490)
                (=> v0x7f9983544e90_0
                    (and v0x7f9983544010_0 E0x7f9983544f50 v0x7f9983544290_0))
                (=> v0x7f9983544e90_0 E0x7f9983544f50)
                (=> v0x7f9983545350_0
                    (and v0x7f99835443d0_0 E0x7f9983545410 v0x7f9983544d50_0))
                (=> v0x7f9983545350_0 E0x7f9983545410)
                (=> v0x7f99835455d0_0 a!5)
                a!6
                v0x7f99835455d0_0
                v0x7f9983546a10_0
                (= v0x7f9983541f50_0 (= v0x7f9983541e90_0 0.0))
                (= v0x7f9983542390_0 (< v0x7f9983541710_0 2.0))
                (= v0x7f9983542550_0 (ite v0x7f9983542390_0 1.0 0.0))
                (= v0x7f9983542690_0 (+ v0x7f9983542550_0 v0x7f9983541710_0))
                (= v0x7f9983543010_0 (= v0x7f9983542f50_0 0.0))
                (= v0x7f9983543410_0 (= v0x7f9983541590_0 0.0))
                (= v0x7f9983543550_0 (ite v0x7f9983543410_0 1.0 0.0))
                (= v0x7f9983543ed0_0 (= v0x7f9983541810_0 0.0))
                (= v0x7f9983544290_0 (> v0x7f9983542890_0 1.0))
                (= v0x7f9983544690_0 (> v0x7f9983542890_0 0.0))
                (= v0x7f99835447d0_0 (+ v0x7f9983542890_0 (- 1.0)))
                (= v0x7f9983544990_0
                   (ite v0x7f9983544690_0 v0x7f99835447d0_0 v0x7f9983542890_0))
                (= v0x7f9983544ad0_0 (= v0x7f9983543750_0 0.0))
                (= v0x7f9983544c10_0 (= v0x7f9983544990_0 0.0))
                (= v0x7f9983544d50_0 (and v0x7f9983544ad0_0 v0x7f9983544c10_0))
                (= v0x7f9983545110_0 (= v0x7f9983543750_0 0.0))
                (= v0x7f9983545210_0
                   (ite v0x7f9983545110_0 1.0 v0x7f9983541810_0))
                (= v0x7f9983546510_0 (= v0x7f9983545690_0 2.0))
                (= v0x7f9983546650_0 (= v0x7f9983545750_0 0.0))
                (= v0x7f9983546790_0 (or v0x7f9983546650_0 v0x7f9983546510_0))
                (= v0x7f99835468d0_0 (xor v0x7f9983546790_0 true))
                (= v0x7f9983546a10_0 (and v0x7f9983543ed0_0 v0x7f99835468d0_0)))))
  (=> F0x7f9983547850 a!7))))
(assert (=> F0x7f9983547850 F0x7f9983547550))
(assert (=> F0x7f9983547950 (or F0x7f9983547790 F0x7f9983547610)))
(assert (=> F0x7f9983547910 F0x7f9983547850))
(assert (=> pre!entry!0 (=> F0x7f99835476d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f9983547550 (>= v0x7f9983541590_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f9983547550
        (or (>= v0x7f9983541710_0 2.0) (<= v0x7f9983541710_0 1.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f9983547550 (>= v0x7f9983541710_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f9983547550
        (or (>= v0x7f9983541710_0 1.0) (<= v0x7f9983541710_0 0.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f9983547550 (<= v0x7f9983541710_0 2.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f9983547950
                (not (or (>= v0x7f99835418d0_0 2.0) (<= v0x7f99835418d0_0 1.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7f9983547950
                (not (or (>= v0x7f99835418d0_0 1.0) (<= v0x7f99835418d0_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f9983547950
           (not (>= v0x7f99835417d0_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f9983547950
           (not (>= v0x7f99835418d0_0 0.0)))
      a!2
      (and (not post!bb1.i.i!4)
           F0x7f9983547950
           (not (<= v0x7f99835418d0_0 2.0)))
      (and (not post!bb2.i.i45.i.i!0) F0x7f9983547910 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i45.i.i!0)
