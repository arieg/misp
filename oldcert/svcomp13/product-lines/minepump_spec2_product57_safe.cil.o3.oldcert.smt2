(declare-fun post!bb2.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!6 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun F0x7f7f4bdb7690 () Bool)
(declare-fun F0x7f7f4bdb7450 () Bool)
(declare-fun v0x7f7f4bdb1090_0 () Real)
(declare-fun v0x7f7f4bdb64d0_0 () Bool)
(declare-fun v0x7f7f4bdb60d0_0 () Bool)
(declare-fun v0x7f7f4bdb2fd0_0 () Bool)
(declare-fun E0x7f7f4bdb5190 () Bool)
(declare-fun v0x7f7f4bdb1a50_0 () Real)
(declare-fun v0x7f7f4bdb2b10_0 () Real)
(declare-fun E0x7f7f4bdb4d90 () Bool)
(declare-fun v0x7f7f4bdb1f50_0 () Bool)
(declare-fun E0x7f7f4bdb5450 () Bool)
(declare-fun v0x7f7f4bdb4cd0_0 () Bool)
(declare-fun E0x7f7f4bdb4890 () Bool)
(declare-fun E0x7f7f4bdb62d0 () Bool)
(declare-fun E0x7f7f4bdb4050 () Bool)
(declare-fun v0x7f7f4bdb3f90_0 () Bool)
(declare-fun E0x7f7f4bdb58d0 () Bool)
(declare-fun v0x7f7f4bdb3a90_0 () Bool)
(declare-fun v0x7f7f4bdb3250_0 () Bool)
(declare-fun v0x7f7f4bdb2bd0_0 () Bool)
(declare-fun v0x7f7f4bdb2d10_0 () Bool)
(declare-fun E0x7f7f4bdb3c90 () Bool)
(declare-fun v0x7f7f4bdb4b90_0 () Real)
(declare-fun v0x7f7f4bdb1290_0 () Real)
(declare-fun E0x7f7f4bdb26d0 () Bool)
(declare-fun v0x7f7f4bdb2250_0 () Real)
(declare-fun post!bb1.i.i!6 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f7f4bdb3310_0 () Real)
(declare-fun v0x7f7f4bdb4250_0 () Bool)
(declare-fun v0x7f7f4bdb47d0_0 () Bool)
(declare-fun E0x7f7f4bdb2510 () Bool)
(declare-fun v0x7f7f4bdb2390_0 () Bool)
(declare-fun v0x7f7f4bdb3e50_0 () Bool)
(declare-fun v0x7f7f4bdb2110_0 () Real)
(declare-fun v0x7f7f4bdb6210_0 () Bool)
(declare-fun v0x7f7f4bdb1990_0 () Bool)
(declare-fun v0x7f7f4bdb50d0_0 () Real)
(declare-fun v0x7f7f4bdb0f10_0 () Real)
(declare-fun v0x7f7f4bdb1190_0 () Real)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun v0x7f7f4bdb2450_0 () Real)
(declare-fun v0x7f7f4bdb5f90_0 () Bool)
(declare-fun v0x7f7f4bdb1c50_0 () Bool)
(declare-fun E0x7f7f4bdb3590 () Bool)
(declare-fun v0x7f7f4bdb4f50_0 () Bool)
(declare-fun v0x7f7f4bdb5010_0 () Real)
(declare-fun F0x7f7f4bdb7250 () Bool)
(declare-fun E0x7f7f4bdb1d10 () Bool)
(declare-fun v0x7f7f4bdb4550_0 () Real)
(declare-fun v0x7f7f4bdb5e90_0 () Bool)
(declare-fun F0x7f7f4bdb7510 () Bool)
(declare-fun E0x7f7f4bdb56d0 () Bool)
(declare-fun F0x7f7f4bdb75d0 () Bool)
(declare-fun E0x7f7f4bdb2dd0 () Bool)
(declare-fun v0x7f7f4bdb1250_0 () Real)
(declare-fun v0x7f7f4bdaf010_0 () Real)
(declare-fun v0x7f7f4bdb1150_0 () Real)
(declare-fun v0x7f7f4bdaf110_0 () Bool)
(declare-fun v0x7f7f4bdb4390_0 () Real)
(declare-fun v0x7f7f4bdb3110_0 () Real)
(declare-fun v0x7f7f4bdb1b10_0 () Bool)
(declare-fun v0x7f7f4bdb4a50_0 () Bool)
(declare-fun F0x7f7f4bdb73d0 () Bool)
(declare-fun v0x7f7f4bdb4690_0 () Bool)
(declare-fun v0x7f7f4bdb3bd0_0 () Bool)
(declare-fun E0x7f7f4bdb33d0 () Bool)
(declare-fun v0x7f7f4bdb1350_0 () Real)
(declare-fun F0x7f7f4bdb7650 () Bool)

(assert (=> F0x7f7f4bdb7650
    (and v0x7f7f4bdaf110_0
         (<= v0x7f7f4bdb1150_0 0.0)
         (>= v0x7f7f4bdb1150_0 0.0)
         (<= v0x7f7f4bdb1250_0 0.0)
         (>= v0x7f7f4bdb1250_0 0.0)
         (<= v0x7f7f4bdb1350_0 0.0)
         (>= v0x7f7f4bdb1350_0 0.0)
         (<= v0x7f7f4bdaf010_0 1.0)
         (>= v0x7f7f4bdaf010_0 1.0))))
(assert (=> F0x7f7f4bdb7650 F0x7f7f4bdb75d0))
(assert (let ((a!1 (=> v0x7f7f4bdb2390_0
               (or (and v0x7f7f4bdb1c50_0
                        E0x7f7f4bdb2510
                        (<= v0x7f7f4bdb2450_0 v0x7f7f4bdb2250_0)
                        (>= v0x7f7f4bdb2450_0 v0x7f7f4bdb2250_0))
                   (and v0x7f7f4bdb1990_0
                        E0x7f7f4bdb26d0
                        v0x7f7f4bdb1b10_0
                        (<= v0x7f7f4bdb2450_0 v0x7f7f4bdb1290_0)
                        (>= v0x7f7f4bdb2450_0 v0x7f7f4bdb1290_0)))))
      (a!2 (=> v0x7f7f4bdb2390_0
               (or (and E0x7f7f4bdb2510 (not E0x7f7f4bdb26d0))
                   (and E0x7f7f4bdb26d0 (not E0x7f7f4bdb2510)))))
      (a!3 (=> v0x7f7f4bdb3250_0
               (or (and v0x7f7f4bdb2d10_0
                        E0x7f7f4bdb33d0
                        (<= v0x7f7f4bdb3310_0 v0x7f7f4bdb3110_0)
                        (>= v0x7f7f4bdb3310_0 v0x7f7f4bdb3110_0))
                   (and v0x7f7f4bdb2390_0
                        E0x7f7f4bdb3590
                        v0x7f7f4bdb2bd0_0
                        (<= v0x7f7f4bdb3310_0 v0x7f7f4bdb1190_0)
                        (>= v0x7f7f4bdb3310_0 v0x7f7f4bdb1190_0)))))
      (a!4 (=> v0x7f7f4bdb3250_0
               (or (and E0x7f7f4bdb33d0 (not E0x7f7f4bdb3590))
                   (and E0x7f7f4bdb3590 (not E0x7f7f4bdb33d0)))))
      (a!5 (or (and v0x7f7f4bdb47d0_0
                    E0x7f7f4bdb5190
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0))
                    (<= v0x7f7f4bdb50d0_0 v0x7f7f4bdb4b90_0)
                    (>= v0x7f7f4bdb50d0_0 v0x7f7f4bdb4b90_0))
               (and v0x7f7f4bdb3bd0_0
                    E0x7f7f4bdb5450
                    (not v0x7f7f4bdb3e50_0)
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0))
                    (and (<= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)
                         (>= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)))
               (and v0x7f7f4bdb4cd0_0
                    E0x7f7f4bdb56d0
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0))
                    (and (<= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)
                         (>= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)))
               (and v0x7f7f4bdb3f90_0
                    E0x7f7f4bdb58d0
                    (not v0x7f7f4bdb4690_0)
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0))
                    (<= v0x7f7f4bdb50d0_0 0.0)
                    (>= v0x7f7f4bdb50d0_0 0.0))))
      (a!6 (=> v0x7f7f4bdb4f50_0
               (or (and E0x7f7f4bdb5190
                        (not E0x7f7f4bdb5450)
                        (not E0x7f7f4bdb56d0)
                        (not E0x7f7f4bdb58d0))
                   (and E0x7f7f4bdb5450
                        (not E0x7f7f4bdb5190)
                        (not E0x7f7f4bdb56d0)
                        (not E0x7f7f4bdb58d0))
                   (and E0x7f7f4bdb56d0
                        (not E0x7f7f4bdb5190)
                        (not E0x7f7f4bdb5450)
                        (not E0x7f7f4bdb58d0))
                   (and E0x7f7f4bdb58d0
                        (not E0x7f7f4bdb5190)
                        (not E0x7f7f4bdb5450)
                        (not E0x7f7f4bdb56d0)))))
      (a!7 (or (and v0x7f7f4bdb6210_0
                    v0x7f7f4bdb64d0_0
                    (and (<= v0x7f7f4bdb1150_0 v0x7f7f4bdb50d0_0)
                         (>= v0x7f7f4bdb1150_0 v0x7f7f4bdb50d0_0))
                    (<= v0x7f7f4bdb1250_0 1.0)
                    (>= v0x7f7f4bdb1250_0 1.0)
                    (and (<= v0x7f7f4bdb1350_0 v0x7f7f4bdb3310_0)
                         (>= v0x7f7f4bdb1350_0 v0x7f7f4bdb3310_0))
                    (and (<= v0x7f7f4bdaf010_0 v0x7f7f4bdb5010_0)
                         (>= v0x7f7f4bdaf010_0 v0x7f7f4bdb5010_0)))
               (and v0x7f7f4bdb4f50_0
                    v0x7f7f4bdb60d0_0
                    (and (<= v0x7f7f4bdb1150_0 v0x7f7f4bdb50d0_0)
                         (>= v0x7f7f4bdb1150_0 v0x7f7f4bdb50d0_0))
                    (<= v0x7f7f4bdb1250_0 0.0)
                    (>= v0x7f7f4bdb1250_0 0.0)
                    (and (<= v0x7f7f4bdb1350_0 v0x7f7f4bdb3310_0)
                         (>= v0x7f7f4bdb1350_0 v0x7f7f4bdb3310_0))
                    (and (<= v0x7f7f4bdaf010_0 v0x7f7f4bdb5010_0)
                         (>= v0x7f7f4bdaf010_0 v0x7f7f4bdb5010_0))))))
(let ((a!8 (and (=> v0x7f7f4bdb1c50_0
                    (and v0x7f7f4bdb1990_0
                         E0x7f7f4bdb1d10
                         (not v0x7f7f4bdb1b10_0)))
                (=> v0x7f7f4bdb1c50_0 E0x7f7f4bdb1d10)
                a!1
                a!2
                (=> v0x7f7f4bdb2d10_0
                    (and v0x7f7f4bdb2390_0
                         E0x7f7f4bdb2dd0
                         (not v0x7f7f4bdb2bd0_0)))
                (=> v0x7f7f4bdb2d10_0 E0x7f7f4bdb2dd0)
                a!3
                a!4
                (=> v0x7f7f4bdb3bd0_0
                    (and v0x7f7f4bdb3250_0 E0x7f7f4bdb3c90 v0x7f7f4bdb3a90_0))
                (=> v0x7f7f4bdb3bd0_0 E0x7f7f4bdb3c90)
                (=> v0x7f7f4bdb3f90_0
                    (and v0x7f7f4bdb3250_0
                         E0x7f7f4bdb4050
                         (not v0x7f7f4bdb3a90_0)))
                (=> v0x7f7f4bdb3f90_0 E0x7f7f4bdb4050)
                (=> v0x7f7f4bdb47d0_0
                    (and v0x7f7f4bdb3bd0_0 E0x7f7f4bdb4890 v0x7f7f4bdb3e50_0))
                (=> v0x7f7f4bdb47d0_0 E0x7f7f4bdb4890)
                (=> v0x7f7f4bdb4cd0_0
                    (and v0x7f7f4bdb3f90_0 E0x7f7f4bdb4d90 v0x7f7f4bdb4690_0))
                (=> v0x7f7f4bdb4cd0_0 E0x7f7f4bdb4d90)
                (=> v0x7f7f4bdb4f50_0 a!5)
                a!6
                (=> v0x7f7f4bdb6210_0
                    (and v0x7f7f4bdb4f50_0
                         E0x7f7f4bdb62d0
                         (not v0x7f7f4bdb60d0_0)))
                (=> v0x7f7f4bdb6210_0 E0x7f7f4bdb62d0)
                a!7
                (= v0x7f7f4bdb1b10_0 (= v0x7f7f4bdb1a50_0 0.0))
                (= v0x7f7f4bdb1f50_0 (< v0x7f7f4bdb1290_0 2.0))
                (= v0x7f7f4bdb2110_0 (ite v0x7f7f4bdb1f50_0 1.0 0.0))
                (= v0x7f7f4bdb2250_0 (+ v0x7f7f4bdb2110_0 v0x7f7f4bdb1290_0))
                (= v0x7f7f4bdb2bd0_0 (= v0x7f7f4bdb2b10_0 0.0))
                (= v0x7f7f4bdb2fd0_0 (= v0x7f7f4bdb1190_0 0.0))
                (= v0x7f7f4bdb3110_0 (ite v0x7f7f4bdb2fd0_0 1.0 0.0))
                (= v0x7f7f4bdb3a90_0 (= v0x7f7f4bdb0f10_0 0.0))
                (= v0x7f7f4bdb3e50_0 (> v0x7f7f4bdb2450_0 1.0))
                (= v0x7f7f4bdb4250_0 (> v0x7f7f4bdb2450_0 0.0))
                (= v0x7f7f4bdb4390_0 (+ v0x7f7f4bdb2450_0 (- 1.0)))
                (= v0x7f7f4bdb4550_0
                   (ite v0x7f7f4bdb4250_0 v0x7f7f4bdb4390_0 v0x7f7f4bdb2450_0))
                (= v0x7f7f4bdb4690_0 (= v0x7f7f4bdb4550_0 0.0))
                (= v0x7f7f4bdb4a50_0 (= v0x7f7f4bdb3310_0 0.0))
                (= v0x7f7f4bdb4b90_0
                   (ite v0x7f7f4bdb4a50_0 1.0 v0x7f7f4bdb0f10_0))
                (= v0x7f7f4bdb5e90_0 (= v0x7f7f4bdb3310_0 0.0))
                (= v0x7f7f4bdb5f90_0 (= v0x7f7f4bdb50d0_0 0.0))
                (= v0x7f7f4bdb60d0_0 (or v0x7f7f4bdb5f90_0 v0x7f7f4bdb5e90_0))
                (= v0x7f7f4bdb64d0_0 (= v0x7f7f4bdb1090_0 0.0)))))
  (=> F0x7f7f4bdb7510 a!8))))
(assert (=> F0x7f7f4bdb7510 F0x7f7f4bdb7450))
(assert (let ((a!1 (=> v0x7f7f4bdb2390_0
               (or (and v0x7f7f4bdb1c50_0
                        E0x7f7f4bdb2510
                        (<= v0x7f7f4bdb2450_0 v0x7f7f4bdb2250_0)
                        (>= v0x7f7f4bdb2450_0 v0x7f7f4bdb2250_0))
                   (and v0x7f7f4bdb1990_0
                        E0x7f7f4bdb26d0
                        v0x7f7f4bdb1b10_0
                        (<= v0x7f7f4bdb2450_0 v0x7f7f4bdb1290_0)
                        (>= v0x7f7f4bdb2450_0 v0x7f7f4bdb1290_0)))))
      (a!2 (=> v0x7f7f4bdb2390_0
               (or (and E0x7f7f4bdb2510 (not E0x7f7f4bdb26d0))
                   (and E0x7f7f4bdb26d0 (not E0x7f7f4bdb2510)))))
      (a!3 (=> v0x7f7f4bdb3250_0
               (or (and v0x7f7f4bdb2d10_0
                        E0x7f7f4bdb33d0
                        (<= v0x7f7f4bdb3310_0 v0x7f7f4bdb3110_0)
                        (>= v0x7f7f4bdb3310_0 v0x7f7f4bdb3110_0))
                   (and v0x7f7f4bdb2390_0
                        E0x7f7f4bdb3590
                        v0x7f7f4bdb2bd0_0
                        (<= v0x7f7f4bdb3310_0 v0x7f7f4bdb1190_0)
                        (>= v0x7f7f4bdb3310_0 v0x7f7f4bdb1190_0)))))
      (a!4 (=> v0x7f7f4bdb3250_0
               (or (and E0x7f7f4bdb33d0 (not E0x7f7f4bdb3590))
                   (and E0x7f7f4bdb3590 (not E0x7f7f4bdb33d0)))))
      (a!5 (or (and v0x7f7f4bdb47d0_0
                    E0x7f7f4bdb5190
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0))
                    (<= v0x7f7f4bdb50d0_0 v0x7f7f4bdb4b90_0)
                    (>= v0x7f7f4bdb50d0_0 v0x7f7f4bdb4b90_0))
               (and v0x7f7f4bdb3bd0_0
                    E0x7f7f4bdb5450
                    (not v0x7f7f4bdb3e50_0)
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb2450_0))
                    (and (<= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)
                         (>= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)))
               (and v0x7f7f4bdb4cd0_0
                    E0x7f7f4bdb56d0
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0))
                    (and (<= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)
                         (>= v0x7f7f4bdb50d0_0 v0x7f7f4bdb0f10_0)))
               (and v0x7f7f4bdb3f90_0
                    E0x7f7f4bdb58d0
                    (not v0x7f7f4bdb4690_0)
                    (and (<= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0)
                         (>= v0x7f7f4bdb5010_0 v0x7f7f4bdb4550_0))
                    (<= v0x7f7f4bdb50d0_0 0.0)
                    (>= v0x7f7f4bdb50d0_0 0.0))))
      (a!6 (=> v0x7f7f4bdb4f50_0
               (or (and E0x7f7f4bdb5190
                        (not E0x7f7f4bdb5450)
                        (not E0x7f7f4bdb56d0)
                        (not E0x7f7f4bdb58d0))
                   (and E0x7f7f4bdb5450
                        (not E0x7f7f4bdb5190)
                        (not E0x7f7f4bdb56d0)
                        (not E0x7f7f4bdb58d0))
                   (and E0x7f7f4bdb56d0
                        (not E0x7f7f4bdb5190)
                        (not E0x7f7f4bdb5450)
                        (not E0x7f7f4bdb58d0))
                   (and E0x7f7f4bdb58d0
                        (not E0x7f7f4bdb5190)
                        (not E0x7f7f4bdb5450)
                        (not E0x7f7f4bdb56d0))))))
(let ((a!7 (and (=> v0x7f7f4bdb1c50_0
                    (and v0x7f7f4bdb1990_0
                         E0x7f7f4bdb1d10
                         (not v0x7f7f4bdb1b10_0)))
                (=> v0x7f7f4bdb1c50_0 E0x7f7f4bdb1d10)
                a!1
                a!2
                (=> v0x7f7f4bdb2d10_0
                    (and v0x7f7f4bdb2390_0
                         E0x7f7f4bdb2dd0
                         (not v0x7f7f4bdb2bd0_0)))
                (=> v0x7f7f4bdb2d10_0 E0x7f7f4bdb2dd0)
                a!3
                a!4
                (=> v0x7f7f4bdb3bd0_0
                    (and v0x7f7f4bdb3250_0 E0x7f7f4bdb3c90 v0x7f7f4bdb3a90_0))
                (=> v0x7f7f4bdb3bd0_0 E0x7f7f4bdb3c90)
                (=> v0x7f7f4bdb3f90_0
                    (and v0x7f7f4bdb3250_0
                         E0x7f7f4bdb4050
                         (not v0x7f7f4bdb3a90_0)))
                (=> v0x7f7f4bdb3f90_0 E0x7f7f4bdb4050)
                (=> v0x7f7f4bdb47d0_0
                    (and v0x7f7f4bdb3bd0_0 E0x7f7f4bdb4890 v0x7f7f4bdb3e50_0))
                (=> v0x7f7f4bdb47d0_0 E0x7f7f4bdb4890)
                (=> v0x7f7f4bdb4cd0_0
                    (and v0x7f7f4bdb3f90_0 E0x7f7f4bdb4d90 v0x7f7f4bdb4690_0))
                (=> v0x7f7f4bdb4cd0_0 E0x7f7f4bdb4d90)
                (=> v0x7f7f4bdb4f50_0 a!5)
                a!6
                (=> v0x7f7f4bdb6210_0
                    (and v0x7f7f4bdb4f50_0
                         E0x7f7f4bdb62d0
                         (not v0x7f7f4bdb60d0_0)))
                (=> v0x7f7f4bdb6210_0 E0x7f7f4bdb62d0)
                v0x7f7f4bdb6210_0
                (not v0x7f7f4bdb64d0_0)
                (= v0x7f7f4bdb1b10_0 (= v0x7f7f4bdb1a50_0 0.0))
                (= v0x7f7f4bdb1f50_0 (< v0x7f7f4bdb1290_0 2.0))
                (= v0x7f7f4bdb2110_0 (ite v0x7f7f4bdb1f50_0 1.0 0.0))
                (= v0x7f7f4bdb2250_0 (+ v0x7f7f4bdb2110_0 v0x7f7f4bdb1290_0))
                (= v0x7f7f4bdb2bd0_0 (= v0x7f7f4bdb2b10_0 0.0))
                (= v0x7f7f4bdb2fd0_0 (= v0x7f7f4bdb1190_0 0.0))
                (= v0x7f7f4bdb3110_0 (ite v0x7f7f4bdb2fd0_0 1.0 0.0))
                (= v0x7f7f4bdb3a90_0 (= v0x7f7f4bdb0f10_0 0.0))
                (= v0x7f7f4bdb3e50_0 (> v0x7f7f4bdb2450_0 1.0))
                (= v0x7f7f4bdb4250_0 (> v0x7f7f4bdb2450_0 0.0))
                (= v0x7f7f4bdb4390_0 (+ v0x7f7f4bdb2450_0 (- 1.0)))
                (= v0x7f7f4bdb4550_0
                   (ite v0x7f7f4bdb4250_0 v0x7f7f4bdb4390_0 v0x7f7f4bdb2450_0))
                (= v0x7f7f4bdb4690_0 (= v0x7f7f4bdb4550_0 0.0))
                (= v0x7f7f4bdb4a50_0 (= v0x7f7f4bdb3310_0 0.0))
                (= v0x7f7f4bdb4b90_0
                   (ite v0x7f7f4bdb4a50_0 1.0 v0x7f7f4bdb0f10_0))
                (= v0x7f7f4bdb5e90_0 (= v0x7f7f4bdb3310_0 0.0))
                (= v0x7f7f4bdb5f90_0 (= v0x7f7f4bdb50d0_0 0.0))
                (= v0x7f7f4bdb60d0_0 (or v0x7f7f4bdb5f90_0 v0x7f7f4bdb5e90_0))
                (= v0x7f7f4bdb64d0_0 (= v0x7f7f4bdb1090_0 0.0)))))
  (=> F0x7f7f4bdb73d0 a!7))))
(assert (=> F0x7f7f4bdb73d0 F0x7f7f4bdb7450))
(assert (=> F0x7f7f4bdb7690 (or F0x7f7f4bdb7650 F0x7f7f4bdb7510)))
(assert (=> F0x7f7f4bdb7250 F0x7f7f4bdb73d0))
(assert (=> pre!entry!0 (=> F0x7f7f4bdb75d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f7f4bdb7450 (>= v0x7f7f4bdb1090_0 0.0))))
(assert (let ((a!1 (=> F0x7f7f4bdb7450
               (or (<= v0x7f7f4bdb1090_0 0.0)
                   (not (<= v0x7f7f4bdb1190_0 0.0))
                   (not (<= 0.0 v0x7f7f4bdb1190_0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f7f4bdb7450
        (or (>= v0x7f7f4bdb1190_0 1.0) (<= v0x7f7f4bdb1190_0 0.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f7f4bdb7450 (not (<= v0x7f7f4bdb1290_0 0.0)))))
(assert (let ((a!1 (=> F0x7f7f4bdb7450
               (or (<= v0x7f7f4bdb0f10_0 0.0) (not (<= v0x7f7f4bdb1290_0 1.0))))))
  (=> pre!bb1.i.i!4 a!1)))
(assert (=> pre!bb1.i.i!5 (=> F0x7f7f4bdb7450 (<= v0x7f7f4bdb1090_0 0.0))))
(assert (=> pre!bb1.i.i!6 (=> F0x7f7f4bdb7450 (>= v0x7f7f4bdb0f10_0 0.0))))
(assert (let ((a!1 (not (or (<= v0x7f7f4bdb1250_0 0.0)
                    (not (<= v0x7f7f4bdb1350_0 0.0))
                    (not (<= 0.0 v0x7f7f4bdb1350_0)))))
      (a!2 (and (not post!bb1.i.i!2)
                F0x7f7f4bdb7690
                (not (or (>= v0x7f7f4bdb1350_0 1.0) (<= v0x7f7f4bdb1350_0 0.0)))))
      (a!3 (not (or (<= v0x7f7f4bdb1150_0 0.0) (not (<= v0x7f7f4bdaf010_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f7f4bdb7690
           (not (>= v0x7f7f4bdb1250_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7f7f4bdb7690 a!1)
      a!2
      (and (not post!bb1.i.i!3) F0x7f7f4bdb7690 (<= v0x7f7f4bdaf010_0 0.0))
      (and (not post!bb1.i.i!4) F0x7f7f4bdb7690 a!3)
      (and (not post!bb1.i.i!5)
           F0x7f7f4bdb7690
           (not (<= v0x7f7f4bdb1250_0 0.0)))
      (and (not post!bb1.i.i!6)
           F0x7f7f4bdb7690
           (not (>= v0x7f7f4bdb1150_0 0.0)))
      (and (not post!bb2.i.i34.i.i!0) F0x7f7f4bdb7250 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5 pre!bb1.i.i!6)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb1.i.i!6 post!bb2.i.i34.i.i!0)
