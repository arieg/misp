(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f0d48ae0b90_0 () Bool)
(declare-fun v0x7f0d48ae0a50_0 () Bool)
(declare-fun v0x7f0d48ae0950_0 () Bool)
(declare-fun v0x7f0d48adf750_0 () Bool)
(declare-fun v0x7f0d48adf310_0 () Real)
(declare-fun v0x7f0d48adf1d0_0 () Bool)
(declare-fun v0x7f0d48adec90_0 () Bool)
(declare-fun v0x7f0d48adde10_0 () Bool)
(declare-fun v0x7f0d48add950_0 () Real)
(declare-fun v0x7f0d48adcf50_0 () Real)
(declare-fun v0x7f0d48adc890_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f0d48adc210_0 () Real)
(declare-fun v0x7f0d48adf4d0_0 () Real)
(declare-fun v0x7f0d48adedd0_0 () Real)
(declare-fun v0x7f0d48adfdd0_0 () Real)
(declare-fun v0x7f0d48adfd10_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f0d48adfc50_0 () Bool)
(declare-fun v0x7f0d48adf890_0 () Bool)
(declare-fun E0x7f0d48adfa90 () Bool)
(declare-fun v0x7f0d48adf610_0 () Bool)
(declare-fun v0x7f0d48ae0cd0_0 () Bool)
(declare-fun E0x7f0d48adefd0 () Bool)
(declare-fun v0x7f0d48adef10_0 () Bool)
(declare-fun v0x7f0d48ade8d0_0 () Bool)
(declare-fun E0x7f0d48ae0410 () Bool)
(declare-fun F0x7f0d48ae1b50 () Bool)
(declare-fun E0x7f0d48ade3d0 () Bool)
(declare-fun E0x7f0d48ade210 () Bool)
(declare-fun v0x7f0d48adda10_0 () Bool)
(declare-fun v0x7f0d48addb50_0 () Bool)
(declare-fun v0x7f0d48adea10_0 () Bool)
(declare-fun v0x7f0d48adc110_0 () Real)
(declare-fun v0x7f0d48ade090_0 () Bool)
(declare-fun E0x7f0d48add510 () Bool)
(declare-fun F0x7f0d48ae1c10 () Bool)
(declare-fun v0x7f0d48ae0e10_0 () Bool)
(declare-fun v0x7f0d48add090_0 () Real)
(declare-fun v0x7f0d48ade150_0 () Real)
(declare-fun E0x7f0d48add350 () Bool)
(declare-fun v0x7f0d48adca90_0 () Bool)
(declare-fun v0x7f0d48adc7d0_0 () Bool)
(declare-fun v0x7f0d48adcd90_0 () Bool)
(declare-fun v0x7f0d48add1d0_0 () Bool)
(declare-fun E0x7f0d48adead0 () Bool)
(declare-fun v0x7f0d48adc950_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f0d48ae1a90 () Bool)
(declare-fun E0x7f0d48ae0150 () Bool)
(declare-fun F0x7f0d48ae1950 () Bool)
(declare-fun E0x7f0d48adcb50 () Bool)
(declare-fun E0x7f0d48adfe90 () Bool)
(declare-fun v0x7f0d48adbf90_0 () Real)
(declare-fun v0x7f0d48add290_0 () Real)
(declare-fun v0x7f0d48ad9010_0 () Real)
(declare-fun v0x7f0d48addf50_0 () Real)
(declare-fun F0x7f0d48ae1d10 () Bool)
(declare-fun v0x7f0d48adc2d0_0 () Real)
(declare-fun F0x7f0d48ae1d50 () Bool)
(declare-fun v0x7f0d48adf9d0_0 () Bool)
(declare-fun v0x7f0d48ad9110_0 () Bool)
(declare-fun E0x7f0d48addc10 () Bool)
(declare-fun F0x7f0d48ae1a50 () Bool)
(declare-fun v0x7f0d48adc1d0_0 () Real)

(assert (=> F0x7f0d48ae1a50
    (and v0x7f0d48ad9110_0
         (<= v0x7f0d48adc1d0_0 0.0)
         (>= v0x7f0d48adc1d0_0 0.0)
         (<= v0x7f0d48adc2d0_0 1.0)
         (>= v0x7f0d48adc2d0_0 1.0)
         (<= v0x7f0d48ad9010_0 0.0)
         (>= v0x7f0d48ad9010_0 0.0))))
(assert (=> F0x7f0d48ae1a50 F0x7f0d48ae1950))
(assert (let ((a!1 (=> v0x7f0d48add1d0_0
               (or (and v0x7f0d48adca90_0
                        E0x7f0d48add350
                        (<= v0x7f0d48add290_0 v0x7f0d48add090_0)
                        (>= v0x7f0d48add290_0 v0x7f0d48add090_0))
                   (and v0x7f0d48adc7d0_0
                        E0x7f0d48add510
                        v0x7f0d48adc950_0
                        (<= v0x7f0d48add290_0 v0x7f0d48adc110_0)
                        (>= v0x7f0d48add290_0 v0x7f0d48adc110_0)))))
      (a!2 (=> v0x7f0d48add1d0_0
               (or (and E0x7f0d48add350 (not E0x7f0d48add510))
                   (and E0x7f0d48add510 (not E0x7f0d48add350)))))
      (a!3 (=> v0x7f0d48ade090_0
               (or (and v0x7f0d48addb50_0
                        E0x7f0d48ade210
                        (<= v0x7f0d48ade150_0 v0x7f0d48addf50_0)
                        (>= v0x7f0d48ade150_0 v0x7f0d48addf50_0))
                   (and v0x7f0d48add1d0_0
                        E0x7f0d48ade3d0
                        v0x7f0d48adda10_0
                        (<= v0x7f0d48ade150_0 v0x7f0d48adbf90_0)
                        (>= v0x7f0d48ade150_0 v0x7f0d48adbf90_0)))))
      (a!4 (=> v0x7f0d48ade090_0
               (or (and E0x7f0d48ade210 (not E0x7f0d48ade3d0))
                   (and E0x7f0d48ade3d0 (not E0x7f0d48ade210)))))
      (a!5 (or (and v0x7f0d48adea10_0
                    E0x7f0d48adfe90
                    (<= v0x7f0d48adfd10_0 v0x7f0d48add290_0)
                    (>= v0x7f0d48adfd10_0 v0x7f0d48add290_0)
                    (<= v0x7f0d48adfdd0_0 v0x7f0d48adedd0_0)
                    (>= v0x7f0d48adfdd0_0 v0x7f0d48adedd0_0))
               (and v0x7f0d48adf9d0_0
                    E0x7f0d48ae0150
                    (and (<= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0)
                         (>= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0))
                    (<= v0x7f0d48adfdd0_0 v0x7f0d48adc210_0)
                    (>= v0x7f0d48adfdd0_0 v0x7f0d48adc210_0))
               (and v0x7f0d48adef10_0
                    E0x7f0d48ae0410
                    (not v0x7f0d48adf890_0)
                    (and (<= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0)
                         (>= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0))
                    (<= v0x7f0d48adfdd0_0 0.0)
                    (>= v0x7f0d48adfdd0_0 0.0))))
      (a!6 (=> v0x7f0d48adfc50_0
               (or (and E0x7f0d48adfe90
                        (not E0x7f0d48ae0150)
                        (not E0x7f0d48ae0410))
                   (and E0x7f0d48ae0150
                        (not E0x7f0d48adfe90)
                        (not E0x7f0d48ae0410))
                   (and E0x7f0d48ae0410
                        (not E0x7f0d48adfe90)
                        (not E0x7f0d48ae0150))))))
(let ((a!7 (and (=> v0x7f0d48adca90_0
                    (and v0x7f0d48adc7d0_0
                         E0x7f0d48adcb50
                         (not v0x7f0d48adc950_0)))
                (=> v0x7f0d48adca90_0 E0x7f0d48adcb50)
                a!1
                a!2
                (=> v0x7f0d48addb50_0
                    (and v0x7f0d48add1d0_0
                         E0x7f0d48addc10
                         (not v0x7f0d48adda10_0)))
                (=> v0x7f0d48addb50_0 E0x7f0d48addc10)
                a!3
                a!4
                (=> v0x7f0d48adea10_0
                    (and v0x7f0d48ade090_0 E0x7f0d48adead0 v0x7f0d48ade8d0_0))
                (=> v0x7f0d48adea10_0 E0x7f0d48adead0)
                (=> v0x7f0d48adef10_0
                    (and v0x7f0d48ade090_0
                         E0x7f0d48adefd0
                         (not v0x7f0d48ade8d0_0)))
                (=> v0x7f0d48adef10_0 E0x7f0d48adefd0)
                (=> v0x7f0d48adf9d0_0
                    (and v0x7f0d48adef10_0 E0x7f0d48adfa90 v0x7f0d48adf890_0))
                (=> v0x7f0d48adf9d0_0 E0x7f0d48adfa90)
                (=> v0x7f0d48adfc50_0 a!5)
                a!6
                v0x7f0d48adfc50_0
                (not v0x7f0d48ae0e10_0)
                (<= v0x7f0d48adc1d0_0 v0x7f0d48ade150_0)
                (>= v0x7f0d48adc1d0_0 v0x7f0d48ade150_0)
                (<= v0x7f0d48adc2d0_0 v0x7f0d48adfd10_0)
                (>= v0x7f0d48adc2d0_0 v0x7f0d48adfd10_0)
                (<= v0x7f0d48ad9010_0 v0x7f0d48adfdd0_0)
                (>= v0x7f0d48ad9010_0 v0x7f0d48adfdd0_0)
                (= v0x7f0d48adc950_0 (= v0x7f0d48adc890_0 0.0))
                (= v0x7f0d48adcd90_0 (< v0x7f0d48adc110_0 2.0))
                (= v0x7f0d48adcf50_0 (ite v0x7f0d48adcd90_0 1.0 0.0))
                (= v0x7f0d48add090_0 (+ v0x7f0d48adcf50_0 v0x7f0d48adc110_0))
                (= v0x7f0d48adda10_0 (= v0x7f0d48add950_0 0.0))
                (= v0x7f0d48adde10_0 (= v0x7f0d48adbf90_0 0.0))
                (= v0x7f0d48addf50_0 (ite v0x7f0d48adde10_0 1.0 0.0))
                (= v0x7f0d48ade8d0_0 (= v0x7f0d48adc210_0 0.0))
                (= v0x7f0d48adec90_0 (> v0x7f0d48add290_0 1.0))
                (= v0x7f0d48adedd0_0
                   (ite v0x7f0d48adec90_0 1.0 v0x7f0d48adc210_0))
                (= v0x7f0d48adf1d0_0 (> v0x7f0d48add290_0 0.0))
                (= v0x7f0d48adf310_0 (+ v0x7f0d48add290_0 (- 1.0)))
                (= v0x7f0d48adf4d0_0
                   (ite v0x7f0d48adf1d0_0 v0x7f0d48adf310_0 v0x7f0d48add290_0))
                (= v0x7f0d48adf610_0 (= v0x7f0d48ade150_0 0.0))
                (= v0x7f0d48adf750_0 (= v0x7f0d48adf4d0_0 0.0))
                (= v0x7f0d48adf890_0 (and v0x7f0d48adf610_0 v0x7f0d48adf750_0))
                (= v0x7f0d48ae0950_0 (= v0x7f0d48ade150_0 0.0))
                (= v0x7f0d48ae0a50_0 (= v0x7f0d48adfd10_0 2.0))
                (= v0x7f0d48ae0b90_0 (= v0x7f0d48adfdd0_0 0.0))
                (= v0x7f0d48ae0cd0_0 (and v0x7f0d48ae0a50_0 v0x7f0d48ae0950_0))
                (= v0x7f0d48ae0e10_0 (and v0x7f0d48ae0cd0_0 v0x7f0d48ae0b90_0)))))
  (=> F0x7f0d48ae1a90 a!7))))
(assert (=> F0x7f0d48ae1a90 F0x7f0d48ae1b50))
(assert (let ((a!1 (=> v0x7f0d48add1d0_0
               (or (and v0x7f0d48adca90_0
                        E0x7f0d48add350
                        (<= v0x7f0d48add290_0 v0x7f0d48add090_0)
                        (>= v0x7f0d48add290_0 v0x7f0d48add090_0))
                   (and v0x7f0d48adc7d0_0
                        E0x7f0d48add510
                        v0x7f0d48adc950_0
                        (<= v0x7f0d48add290_0 v0x7f0d48adc110_0)
                        (>= v0x7f0d48add290_0 v0x7f0d48adc110_0)))))
      (a!2 (=> v0x7f0d48add1d0_0
               (or (and E0x7f0d48add350 (not E0x7f0d48add510))
                   (and E0x7f0d48add510 (not E0x7f0d48add350)))))
      (a!3 (=> v0x7f0d48ade090_0
               (or (and v0x7f0d48addb50_0
                        E0x7f0d48ade210
                        (<= v0x7f0d48ade150_0 v0x7f0d48addf50_0)
                        (>= v0x7f0d48ade150_0 v0x7f0d48addf50_0))
                   (and v0x7f0d48add1d0_0
                        E0x7f0d48ade3d0
                        v0x7f0d48adda10_0
                        (<= v0x7f0d48ade150_0 v0x7f0d48adbf90_0)
                        (>= v0x7f0d48ade150_0 v0x7f0d48adbf90_0)))))
      (a!4 (=> v0x7f0d48ade090_0
               (or (and E0x7f0d48ade210 (not E0x7f0d48ade3d0))
                   (and E0x7f0d48ade3d0 (not E0x7f0d48ade210)))))
      (a!5 (or (and v0x7f0d48adea10_0
                    E0x7f0d48adfe90
                    (<= v0x7f0d48adfd10_0 v0x7f0d48add290_0)
                    (>= v0x7f0d48adfd10_0 v0x7f0d48add290_0)
                    (<= v0x7f0d48adfdd0_0 v0x7f0d48adedd0_0)
                    (>= v0x7f0d48adfdd0_0 v0x7f0d48adedd0_0))
               (and v0x7f0d48adf9d0_0
                    E0x7f0d48ae0150
                    (and (<= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0)
                         (>= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0))
                    (<= v0x7f0d48adfdd0_0 v0x7f0d48adc210_0)
                    (>= v0x7f0d48adfdd0_0 v0x7f0d48adc210_0))
               (and v0x7f0d48adef10_0
                    E0x7f0d48ae0410
                    (not v0x7f0d48adf890_0)
                    (and (<= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0)
                         (>= v0x7f0d48adfd10_0 v0x7f0d48adf4d0_0))
                    (<= v0x7f0d48adfdd0_0 0.0)
                    (>= v0x7f0d48adfdd0_0 0.0))))
      (a!6 (=> v0x7f0d48adfc50_0
               (or (and E0x7f0d48adfe90
                        (not E0x7f0d48ae0150)
                        (not E0x7f0d48ae0410))
                   (and E0x7f0d48ae0150
                        (not E0x7f0d48adfe90)
                        (not E0x7f0d48ae0410))
                   (and E0x7f0d48ae0410
                        (not E0x7f0d48adfe90)
                        (not E0x7f0d48ae0150))))))
(let ((a!7 (and (=> v0x7f0d48adca90_0
                    (and v0x7f0d48adc7d0_0
                         E0x7f0d48adcb50
                         (not v0x7f0d48adc950_0)))
                (=> v0x7f0d48adca90_0 E0x7f0d48adcb50)
                a!1
                a!2
                (=> v0x7f0d48addb50_0
                    (and v0x7f0d48add1d0_0
                         E0x7f0d48addc10
                         (not v0x7f0d48adda10_0)))
                (=> v0x7f0d48addb50_0 E0x7f0d48addc10)
                a!3
                a!4
                (=> v0x7f0d48adea10_0
                    (and v0x7f0d48ade090_0 E0x7f0d48adead0 v0x7f0d48ade8d0_0))
                (=> v0x7f0d48adea10_0 E0x7f0d48adead0)
                (=> v0x7f0d48adef10_0
                    (and v0x7f0d48ade090_0
                         E0x7f0d48adefd0
                         (not v0x7f0d48ade8d0_0)))
                (=> v0x7f0d48adef10_0 E0x7f0d48adefd0)
                (=> v0x7f0d48adf9d0_0
                    (and v0x7f0d48adef10_0 E0x7f0d48adfa90 v0x7f0d48adf890_0))
                (=> v0x7f0d48adf9d0_0 E0x7f0d48adfa90)
                (=> v0x7f0d48adfc50_0 a!5)
                a!6
                v0x7f0d48adfc50_0
                v0x7f0d48ae0e10_0
                (= v0x7f0d48adc950_0 (= v0x7f0d48adc890_0 0.0))
                (= v0x7f0d48adcd90_0 (< v0x7f0d48adc110_0 2.0))
                (= v0x7f0d48adcf50_0 (ite v0x7f0d48adcd90_0 1.0 0.0))
                (= v0x7f0d48add090_0 (+ v0x7f0d48adcf50_0 v0x7f0d48adc110_0))
                (= v0x7f0d48adda10_0 (= v0x7f0d48add950_0 0.0))
                (= v0x7f0d48adde10_0 (= v0x7f0d48adbf90_0 0.0))
                (= v0x7f0d48addf50_0 (ite v0x7f0d48adde10_0 1.0 0.0))
                (= v0x7f0d48ade8d0_0 (= v0x7f0d48adc210_0 0.0))
                (= v0x7f0d48adec90_0 (> v0x7f0d48add290_0 1.0))
                (= v0x7f0d48adedd0_0
                   (ite v0x7f0d48adec90_0 1.0 v0x7f0d48adc210_0))
                (= v0x7f0d48adf1d0_0 (> v0x7f0d48add290_0 0.0))
                (= v0x7f0d48adf310_0 (+ v0x7f0d48add290_0 (- 1.0)))
                (= v0x7f0d48adf4d0_0
                   (ite v0x7f0d48adf1d0_0 v0x7f0d48adf310_0 v0x7f0d48add290_0))
                (= v0x7f0d48adf610_0 (= v0x7f0d48ade150_0 0.0))
                (= v0x7f0d48adf750_0 (= v0x7f0d48adf4d0_0 0.0))
                (= v0x7f0d48adf890_0 (and v0x7f0d48adf610_0 v0x7f0d48adf750_0))
                (= v0x7f0d48ae0950_0 (= v0x7f0d48ade150_0 0.0))
                (= v0x7f0d48ae0a50_0 (= v0x7f0d48adfd10_0 2.0))
                (= v0x7f0d48ae0b90_0 (= v0x7f0d48adfdd0_0 0.0))
                (= v0x7f0d48ae0cd0_0 (and v0x7f0d48ae0a50_0 v0x7f0d48ae0950_0))
                (= v0x7f0d48ae0e10_0 (and v0x7f0d48ae0cd0_0 v0x7f0d48ae0b90_0)))))
  (=> F0x7f0d48ae1c10 a!7))))
(assert (=> F0x7f0d48ae1c10 F0x7f0d48ae1b50))
(assert (=> F0x7f0d48ae1d50 (or F0x7f0d48ae1a50 F0x7f0d48ae1a90)))
(assert (=> F0x7f0d48ae1d10 F0x7f0d48ae1c10))
(assert (=> pre!entry!0 (=> F0x7f0d48ae1950 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f0d48ae1b50 (>= v0x7f0d48adc210_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f0d48ae1b50 (not (<= 3.0 v0x7f0d48adc110_0)))))
(assert (or (and (not post!bb1.i.i!0) F0x7f0d48ae1d50 (not (>= v0x7f0d48ad9010_0 0.0)))
    (and (not post!bb1.i.i!1) F0x7f0d48ae1d50 (<= 3.0 v0x7f0d48adc2d0_0))
    (and (not post!bb2.i.i35.i.i!0) F0x7f0d48ae1d10 true)))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
