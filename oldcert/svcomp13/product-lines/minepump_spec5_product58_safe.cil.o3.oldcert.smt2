(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7ffef2b1d090 () Bool)
(declare-fun F0x7ffef2b1cf90 () Bool)
(declare-fun F0x7ffef2b1cc90 () Bool)
(declare-fun v0x7ffef2b1bed0_0 () Bool)
(declare-fun v0x7ffef2b18d90_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7ffef2b188d0_0 () Real)
(declare-fun v0x7ffef2b1a310_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun E0x7ffef2b1b210 () Bool)
(declare-fun v0x7ffef2b1a950_0 () Real)
(declare-fun v0x7ffef2b1ae90_0 () Real)
(declare-fun v0x7ffef2b1add0_0 () Real)
(declare-fun v0x7ffef2b19c10_0 () Bool)
(declare-fun v0x7ffef2b16f10_0 () Real)
(declare-fun F0x7ffef2b1d050 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7ffef2b1a650 () Bool)
(declare-fun v0x7ffef2b17ed0_0 () Real)
(declare-fun v0x7ffef2b19d50_0 () Bool)
(declare-fun E0x7ffef2b19e10 () Bool)
(declare-fun v0x7ffef2b19990_0 () Bool)
(declare-fun v0x7ffef2b17810_0 () Real)
(declare-fun v0x7ffef2b1aa90_0 () Bool)
(declare-fun v0x7ffef2b19850_0 () Bool)
(declare-fun v0x7ffef2b18ed0_0 () Real)
(declare-fun v0x7ffef2b190d0_0 () Real)
(declare-fun E0x7ffef2b19350 () Bool)
(declare-fun v0x7ffef2b19010_0 () Bool)
(declare-fun v0x7ffef2b17090_0 () Real)
(declare-fun v0x7ffef2b18990_0 () Bool)
(declare-fun E0x7ffef2b1ab50 () Bool)
(declare-fun v0x7ffef2b18010_0 () Real)
(declare-fun v0x7ffef2b17190_0 () Real)
(declare-fun E0x7ffef2b182d0 () Bool)
(declare-fun v0x7ffef2b18ad0_0 () Bool)
(declare-fun E0x7ffef2b18b90 () Bool)
(declare-fun v0x7ffef2b18150_0 () Bool)
(declare-fun v0x7ffef2b1a450_0 () Bool)
(declare-fun F0x7ffef2b1cd50 () Bool)
(declare-fun F0x7ffef2b1ce10 () Bool)
(declare-fun v0x7ffef2b18210_0 () Real)
(declare-fun post!bb2.i.i36.i.i!0 () Bool)
(declare-fun v0x7ffef2b17750_0 () Bool)
(declare-fun v0x7ffef2b1c010_0 () Bool)
(declare-fun E0x7ffef2b17ad0 () Bool)
(declare-fun v0x7ffef2b15010_0 () Real)
(declare-fun E0x7ffef2b1b690 () Bool)
(declare-fun v0x7ffef2b17a10_0 () Bool)
(declare-fun E0x7ffef2b1b490 () Bool)
(declare-fun v0x7ffef2b1a150_0 () Real)
(declare-fun v0x7ffef2b1a810_0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun E0x7ffef2b19190 () Bool)
(declare-fun v0x7ffef2b17250_0 () Real)
(declare-fun E0x7ffef2b18490 () Bool)
(declare-fun v0x7ffef2b1a010_0 () Bool)
(declare-fun v0x7ffef2b17150_0 () Real)
(declare-fun v0x7ffef2b1a590_0 () Bool)
(declare-fun v0x7ffef2b1c150_0 () Bool)
(declare-fun v0x7ffef2b1bd90_0 () Bool)
(declare-fun v0x7ffef2b15110_0 () Bool)
(declare-fun E0x7ffef2b1af50 () Bool)
(declare-fun v0x7ffef2b17d10_0 () Bool)
(declare-fun v0x7ffef2b1bc50_0 () Bool)
(declare-fun E0x7ffef2b19a50 () Bool)
(declare-fun v0x7ffef2b1ad10_0 () Bool)
(declare-fun F0x7ffef2b1ced0 () Bool)
(declare-fun v0x7ffef2b178d0_0 () Bool)

(assert (=> F0x7ffef2b1ced0
    (and v0x7ffef2b15110_0
         (<= v0x7ffef2b17150_0 0.0)
         (>= v0x7ffef2b17150_0 0.0)
         (<= v0x7ffef2b17250_0 0.0)
         (>= v0x7ffef2b17250_0 0.0)
         (<= v0x7ffef2b15010_0 1.0)
         (>= v0x7ffef2b15010_0 1.0))))
(assert (=> F0x7ffef2b1ced0 F0x7ffef2b1ce10))
(assert (let ((a!1 (=> v0x7ffef2b18150_0
               (or (and v0x7ffef2b17a10_0
                        E0x7ffef2b182d0
                        (<= v0x7ffef2b18210_0 v0x7ffef2b18010_0)
                        (>= v0x7ffef2b18210_0 v0x7ffef2b18010_0))
                   (and v0x7ffef2b17750_0
                        E0x7ffef2b18490
                        v0x7ffef2b178d0_0
                        (<= v0x7ffef2b18210_0 v0x7ffef2b17190_0)
                        (>= v0x7ffef2b18210_0 v0x7ffef2b17190_0)))))
      (a!2 (=> v0x7ffef2b18150_0
               (or (and E0x7ffef2b182d0 (not E0x7ffef2b18490))
                   (and E0x7ffef2b18490 (not E0x7ffef2b182d0)))))
      (a!3 (=> v0x7ffef2b19010_0
               (or (and v0x7ffef2b18ad0_0
                        E0x7ffef2b19190
                        (<= v0x7ffef2b190d0_0 v0x7ffef2b18ed0_0)
                        (>= v0x7ffef2b190d0_0 v0x7ffef2b18ed0_0))
                   (and v0x7ffef2b18150_0
                        E0x7ffef2b19350
                        v0x7ffef2b18990_0
                        (<= v0x7ffef2b190d0_0 v0x7ffef2b17090_0)
                        (>= v0x7ffef2b190d0_0 v0x7ffef2b17090_0)))))
      (a!4 (=> v0x7ffef2b19010_0
               (or (and E0x7ffef2b19190 (not E0x7ffef2b19350))
                   (and E0x7ffef2b19350 (not E0x7ffef2b19190)))))
      (a!5 (or (and v0x7ffef2b1a590_0
                    E0x7ffef2b1af50
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b18210_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b18210_0))
                    (<= v0x7ffef2b1ae90_0 v0x7ffef2b1a950_0)
                    (>= v0x7ffef2b1ae90_0 v0x7ffef2b1a950_0))
               (and v0x7ffef2b19990_0
                    E0x7ffef2b1b210
                    (not v0x7ffef2b19c10_0)
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b18210_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b18210_0))
                    (and (<= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)
                         (>= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)))
               (and v0x7ffef2b1aa90_0
                    E0x7ffef2b1b490
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0))
                    (and (<= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)
                         (>= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)))
               (and v0x7ffef2b19d50_0
                    E0x7ffef2b1b690
                    (not v0x7ffef2b1a450_0)
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0))
                    (<= v0x7ffef2b1ae90_0 0.0)
                    (>= v0x7ffef2b1ae90_0 0.0))))
      (a!6 (=> v0x7ffef2b1ad10_0
               (or (and E0x7ffef2b1af50
                        (not E0x7ffef2b1b210)
                        (not E0x7ffef2b1b490)
                        (not E0x7ffef2b1b690))
                   (and E0x7ffef2b1b210
                        (not E0x7ffef2b1af50)
                        (not E0x7ffef2b1b490)
                        (not E0x7ffef2b1b690))
                   (and E0x7ffef2b1b490
                        (not E0x7ffef2b1af50)
                        (not E0x7ffef2b1b210)
                        (not E0x7ffef2b1b690))
                   (and E0x7ffef2b1b690
                        (not E0x7ffef2b1af50)
                        (not E0x7ffef2b1b210)
                        (not E0x7ffef2b1b490))))))
(let ((a!7 (and (=> v0x7ffef2b17a10_0
                    (and v0x7ffef2b17750_0
                         E0x7ffef2b17ad0
                         (not v0x7ffef2b178d0_0)))
                (=> v0x7ffef2b17a10_0 E0x7ffef2b17ad0)
                a!1
                a!2
                (=> v0x7ffef2b18ad0_0
                    (and v0x7ffef2b18150_0
                         E0x7ffef2b18b90
                         (not v0x7ffef2b18990_0)))
                (=> v0x7ffef2b18ad0_0 E0x7ffef2b18b90)
                a!3
                a!4
                (=> v0x7ffef2b19990_0
                    (and v0x7ffef2b19010_0 E0x7ffef2b19a50 v0x7ffef2b19850_0))
                (=> v0x7ffef2b19990_0 E0x7ffef2b19a50)
                (=> v0x7ffef2b19d50_0
                    (and v0x7ffef2b19010_0
                         E0x7ffef2b19e10
                         (not v0x7ffef2b19850_0)))
                (=> v0x7ffef2b19d50_0 E0x7ffef2b19e10)
                (=> v0x7ffef2b1a590_0
                    (and v0x7ffef2b19990_0 E0x7ffef2b1a650 v0x7ffef2b19c10_0))
                (=> v0x7ffef2b1a590_0 E0x7ffef2b1a650)
                (=> v0x7ffef2b1aa90_0
                    (and v0x7ffef2b19d50_0 E0x7ffef2b1ab50 v0x7ffef2b1a450_0))
                (=> v0x7ffef2b1aa90_0 E0x7ffef2b1ab50)
                (=> v0x7ffef2b1ad10_0 a!5)
                a!6
                v0x7ffef2b1ad10_0
                (not v0x7ffef2b1c150_0)
                (<= v0x7ffef2b17150_0 v0x7ffef2b1ae90_0)
                (>= v0x7ffef2b17150_0 v0x7ffef2b1ae90_0)
                (<= v0x7ffef2b17250_0 v0x7ffef2b190d0_0)
                (>= v0x7ffef2b17250_0 v0x7ffef2b190d0_0)
                (<= v0x7ffef2b15010_0 v0x7ffef2b1add0_0)
                (>= v0x7ffef2b15010_0 v0x7ffef2b1add0_0)
                (= v0x7ffef2b178d0_0 (= v0x7ffef2b17810_0 0.0))
                (= v0x7ffef2b17d10_0 (< v0x7ffef2b17190_0 2.0))
                (= v0x7ffef2b17ed0_0 (ite v0x7ffef2b17d10_0 1.0 0.0))
                (= v0x7ffef2b18010_0 (+ v0x7ffef2b17ed0_0 v0x7ffef2b17190_0))
                (= v0x7ffef2b18990_0 (= v0x7ffef2b188d0_0 0.0))
                (= v0x7ffef2b18d90_0 (= v0x7ffef2b17090_0 0.0))
                (= v0x7ffef2b18ed0_0 (ite v0x7ffef2b18d90_0 1.0 0.0))
                (= v0x7ffef2b19850_0 (= v0x7ffef2b16f10_0 0.0))
                (= v0x7ffef2b19c10_0 (> v0x7ffef2b18210_0 1.0))
                (= v0x7ffef2b1a010_0 (> v0x7ffef2b18210_0 0.0))
                (= v0x7ffef2b1a150_0 (+ v0x7ffef2b18210_0 (- 1.0)))
                (= v0x7ffef2b1a310_0
                   (ite v0x7ffef2b1a010_0 v0x7ffef2b1a150_0 v0x7ffef2b18210_0))
                (= v0x7ffef2b1a450_0 (= v0x7ffef2b1a310_0 0.0))
                (= v0x7ffef2b1a810_0 (= v0x7ffef2b190d0_0 0.0))
                (= v0x7ffef2b1a950_0
                   (ite v0x7ffef2b1a810_0 1.0 v0x7ffef2b16f10_0))
                (= v0x7ffef2b1bc50_0 (= v0x7ffef2b1add0_0 2.0))
                (= v0x7ffef2b1bd90_0 (= v0x7ffef2b1ae90_0 0.0))
                (= v0x7ffef2b1bed0_0 (or v0x7ffef2b1bd90_0 v0x7ffef2b1bc50_0))
                (= v0x7ffef2b1c010_0 (xor v0x7ffef2b1bed0_0 true))
                (= v0x7ffef2b1c150_0 (and v0x7ffef2b19850_0 v0x7ffef2b1c010_0)))))
  (=> F0x7ffef2b1cd50 a!7))))
(assert (=> F0x7ffef2b1cd50 F0x7ffef2b1cc90))
(assert (let ((a!1 (=> v0x7ffef2b18150_0
               (or (and v0x7ffef2b17a10_0
                        E0x7ffef2b182d0
                        (<= v0x7ffef2b18210_0 v0x7ffef2b18010_0)
                        (>= v0x7ffef2b18210_0 v0x7ffef2b18010_0))
                   (and v0x7ffef2b17750_0
                        E0x7ffef2b18490
                        v0x7ffef2b178d0_0
                        (<= v0x7ffef2b18210_0 v0x7ffef2b17190_0)
                        (>= v0x7ffef2b18210_0 v0x7ffef2b17190_0)))))
      (a!2 (=> v0x7ffef2b18150_0
               (or (and E0x7ffef2b182d0 (not E0x7ffef2b18490))
                   (and E0x7ffef2b18490 (not E0x7ffef2b182d0)))))
      (a!3 (=> v0x7ffef2b19010_0
               (or (and v0x7ffef2b18ad0_0
                        E0x7ffef2b19190
                        (<= v0x7ffef2b190d0_0 v0x7ffef2b18ed0_0)
                        (>= v0x7ffef2b190d0_0 v0x7ffef2b18ed0_0))
                   (and v0x7ffef2b18150_0
                        E0x7ffef2b19350
                        v0x7ffef2b18990_0
                        (<= v0x7ffef2b190d0_0 v0x7ffef2b17090_0)
                        (>= v0x7ffef2b190d0_0 v0x7ffef2b17090_0)))))
      (a!4 (=> v0x7ffef2b19010_0
               (or (and E0x7ffef2b19190 (not E0x7ffef2b19350))
                   (and E0x7ffef2b19350 (not E0x7ffef2b19190)))))
      (a!5 (or (and v0x7ffef2b1a590_0
                    E0x7ffef2b1af50
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b18210_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b18210_0))
                    (<= v0x7ffef2b1ae90_0 v0x7ffef2b1a950_0)
                    (>= v0x7ffef2b1ae90_0 v0x7ffef2b1a950_0))
               (and v0x7ffef2b19990_0
                    E0x7ffef2b1b210
                    (not v0x7ffef2b19c10_0)
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b18210_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b18210_0))
                    (and (<= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)
                         (>= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)))
               (and v0x7ffef2b1aa90_0
                    E0x7ffef2b1b490
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0))
                    (and (<= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)
                         (>= v0x7ffef2b1ae90_0 v0x7ffef2b16f10_0)))
               (and v0x7ffef2b19d50_0
                    E0x7ffef2b1b690
                    (not v0x7ffef2b1a450_0)
                    (and (<= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0)
                         (>= v0x7ffef2b1add0_0 v0x7ffef2b1a310_0))
                    (<= v0x7ffef2b1ae90_0 0.0)
                    (>= v0x7ffef2b1ae90_0 0.0))))
      (a!6 (=> v0x7ffef2b1ad10_0
               (or (and E0x7ffef2b1af50
                        (not E0x7ffef2b1b210)
                        (not E0x7ffef2b1b490)
                        (not E0x7ffef2b1b690))
                   (and E0x7ffef2b1b210
                        (not E0x7ffef2b1af50)
                        (not E0x7ffef2b1b490)
                        (not E0x7ffef2b1b690))
                   (and E0x7ffef2b1b490
                        (not E0x7ffef2b1af50)
                        (not E0x7ffef2b1b210)
                        (not E0x7ffef2b1b690))
                   (and E0x7ffef2b1b690
                        (not E0x7ffef2b1af50)
                        (not E0x7ffef2b1b210)
                        (not E0x7ffef2b1b490))))))
(let ((a!7 (and (=> v0x7ffef2b17a10_0
                    (and v0x7ffef2b17750_0
                         E0x7ffef2b17ad0
                         (not v0x7ffef2b178d0_0)))
                (=> v0x7ffef2b17a10_0 E0x7ffef2b17ad0)
                a!1
                a!2
                (=> v0x7ffef2b18ad0_0
                    (and v0x7ffef2b18150_0
                         E0x7ffef2b18b90
                         (not v0x7ffef2b18990_0)))
                (=> v0x7ffef2b18ad0_0 E0x7ffef2b18b90)
                a!3
                a!4
                (=> v0x7ffef2b19990_0
                    (and v0x7ffef2b19010_0 E0x7ffef2b19a50 v0x7ffef2b19850_0))
                (=> v0x7ffef2b19990_0 E0x7ffef2b19a50)
                (=> v0x7ffef2b19d50_0
                    (and v0x7ffef2b19010_0
                         E0x7ffef2b19e10
                         (not v0x7ffef2b19850_0)))
                (=> v0x7ffef2b19d50_0 E0x7ffef2b19e10)
                (=> v0x7ffef2b1a590_0
                    (and v0x7ffef2b19990_0 E0x7ffef2b1a650 v0x7ffef2b19c10_0))
                (=> v0x7ffef2b1a590_0 E0x7ffef2b1a650)
                (=> v0x7ffef2b1aa90_0
                    (and v0x7ffef2b19d50_0 E0x7ffef2b1ab50 v0x7ffef2b1a450_0))
                (=> v0x7ffef2b1aa90_0 E0x7ffef2b1ab50)
                (=> v0x7ffef2b1ad10_0 a!5)
                a!6
                v0x7ffef2b1ad10_0
                v0x7ffef2b1c150_0
                (= v0x7ffef2b178d0_0 (= v0x7ffef2b17810_0 0.0))
                (= v0x7ffef2b17d10_0 (< v0x7ffef2b17190_0 2.0))
                (= v0x7ffef2b17ed0_0 (ite v0x7ffef2b17d10_0 1.0 0.0))
                (= v0x7ffef2b18010_0 (+ v0x7ffef2b17ed0_0 v0x7ffef2b17190_0))
                (= v0x7ffef2b18990_0 (= v0x7ffef2b188d0_0 0.0))
                (= v0x7ffef2b18d90_0 (= v0x7ffef2b17090_0 0.0))
                (= v0x7ffef2b18ed0_0 (ite v0x7ffef2b18d90_0 1.0 0.0))
                (= v0x7ffef2b19850_0 (= v0x7ffef2b16f10_0 0.0))
                (= v0x7ffef2b19c10_0 (> v0x7ffef2b18210_0 1.0))
                (= v0x7ffef2b1a010_0 (> v0x7ffef2b18210_0 0.0))
                (= v0x7ffef2b1a150_0 (+ v0x7ffef2b18210_0 (- 1.0)))
                (= v0x7ffef2b1a310_0
                   (ite v0x7ffef2b1a010_0 v0x7ffef2b1a150_0 v0x7ffef2b18210_0))
                (= v0x7ffef2b1a450_0 (= v0x7ffef2b1a310_0 0.0))
                (= v0x7ffef2b1a810_0 (= v0x7ffef2b190d0_0 0.0))
                (= v0x7ffef2b1a950_0
                   (ite v0x7ffef2b1a810_0 1.0 v0x7ffef2b16f10_0))
                (= v0x7ffef2b1bc50_0 (= v0x7ffef2b1add0_0 2.0))
                (= v0x7ffef2b1bd90_0 (= v0x7ffef2b1ae90_0 0.0))
                (= v0x7ffef2b1bed0_0 (or v0x7ffef2b1bd90_0 v0x7ffef2b1bc50_0))
                (= v0x7ffef2b1c010_0 (xor v0x7ffef2b1bed0_0 true))
                (= v0x7ffef2b1c150_0 (and v0x7ffef2b19850_0 v0x7ffef2b1c010_0)))))
  (=> F0x7ffef2b1cf90 a!7))))
(assert (=> F0x7ffef2b1cf90 F0x7ffef2b1cc90))
(assert (=> F0x7ffef2b1d090 (or F0x7ffef2b1ced0 F0x7ffef2b1cd50)))
(assert (=> F0x7ffef2b1d050 F0x7ffef2b1cf90))
(assert (=> pre!entry!0 (=> F0x7ffef2b1ce10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7ffef2b1cc90 (>= v0x7ffef2b17090_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7ffef2b1cc90
        (or (<= v0x7ffef2b17190_0 1.0) (>= v0x7ffef2b17190_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7ffef2b1cc90 (>= v0x7ffef2b17190_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7ffef2b1cc90
        (or (>= v0x7ffef2b17190_0 1.0) (<= v0x7ffef2b17190_0 0.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7ffef2b1cc90 (<= v0x7ffef2b17190_0 2.0))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7ffef2b1d090
                (not (or (<= v0x7ffef2b15010_0 1.0) (>= v0x7ffef2b15010_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7ffef2b1d090
                (not (or (>= v0x7ffef2b15010_0 1.0) (<= v0x7ffef2b15010_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7ffef2b1d090
           (not (>= v0x7ffef2b17250_0 0.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7ffef2b1d090
           (not (>= v0x7ffef2b15010_0 0.0)))
      a!2
      (and (not post!bb1.i.i!4)
           F0x7ffef2b1d090
           (not (<= v0x7ffef2b15010_0 2.0)))
      (and (not post!bb2.i.i36.i.i!0) F0x7ffef2b1d050 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i36.i.i!0)
