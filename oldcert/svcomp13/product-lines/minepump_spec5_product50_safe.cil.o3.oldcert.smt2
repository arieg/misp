(declare-fun post!bb2.i.i28.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f58356ec610 () Bool)
(declare-fun F0x7f58356ec650 () Bool)
(declare-fun F0x7f58356ec290 () Bool)
(declare-fun F0x7f58356ec350 () Bool)
(declare-fun v0x7f58356eb6d0_0 () Bool)
(declare-fun v0x7f58356eb590_0 () Bool)
(declare-fun v0x7f58356eb310_0 () Bool)
(declare-fun v0x7f58356e9e10_0 () Bool)
(declare-fun v0x7f58356e98d0_0 () Bool)
(declare-fun v0x7f58356e8990_0 () Real)
(declare-fun v0x7f58356e87d0_0 () Bool)
(declare-fun v0x7f58356eb810_0 () Bool)
(declare-fun v0x7f58356e7d90_0 () Real)
(declare-fun v0x7f58356ea110_0 () Real)
(declare-fun E0x7f58356eab10 () Bool)
(declare-fun v0x7f58356e9a10_0 () Real)
(declare-fun v0x7f58356ea6d0_0 () Real)
(declare-fun E0x7f58356ea450 () Bool)
(declare-fun E0x7f58356ea850 () Bool)
(declare-fun v0x7f58356e82d0_0 () Real)
(declare-fun v0x7f58356ea390_0 () Bool)
(declare-fun v0x7f58356ea790_0 () Real)
(declare-fun E0x7f58356e9c10 () Bool)
(declare-fun v0x7f58356e7c10_0 () Real)
(declare-fun v0x7f58356e8ad0_0 () Real)
(declare-fun E0x7f58356e8d90 () Bool)
(declare-fun v0x7f58356e9b50_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f58356e8390_0 () Bool)
(declare-fun v0x7f58356e8210_0 () Bool)
(declare-fun v0x7f58356ea610_0 () Bool)
(declare-fun E0x7f58356e8f50 () Bool)
(declare-fun v0x7f58356e9510_0 () Bool)
(declare-fun E0x7f58356e8590 () Bool)
(declare-fun v0x7f58356ea250_0 () Bool)
(declare-fun v0x7f58356e84d0_0 () Bool)
(declare-fun v0x7f58356e9650_0 () Bool)
(declare-fun v0x7f58356e8cd0_0 () Real)
(declare-fun F0x7f58356ec410 () Bool)
(declare-fun E0x7f58356eadd0 () Bool)
(declare-fun F0x7f58356ec4d0 () Bool)
(declare-fun v0x7f58356e6010_0 () Real)
(declare-fun v0x7f58356e8c10_0 () Bool)
(declare-fun v0x7f58356e7e50_0 () Real)
(declare-fun v0x7f58356e6110_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f58356eb450_0 () Bool)
(declare-fun v0x7f58356e9f50_0 () Real)
(declare-fun E0x7f58356e9710 () Bool)
(declare-fun F0x7f58356ec590 () Bool)

(assert (=> F0x7f58356ec590
    (and v0x7f58356e6110_0
         (<= v0x7f58356e7e50_0 1.0)
         (>= v0x7f58356e7e50_0 1.0)
         (<= v0x7f58356e6010_0 0.0)
         (>= v0x7f58356e6010_0 0.0))))
(assert (=> F0x7f58356ec590 F0x7f58356ec4d0))
(assert (let ((a!1 (=> v0x7f58356e8c10_0
               (or (and v0x7f58356e84d0_0
                        E0x7f58356e8d90
                        (<= v0x7f58356e8cd0_0 v0x7f58356e8ad0_0)
                        (>= v0x7f58356e8cd0_0 v0x7f58356e8ad0_0))
                   (and v0x7f58356e8210_0
                        E0x7f58356e8f50
                        v0x7f58356e8390_0
                        (<= v0x7f58356e8cd0_0 v0x7f58356e7c10_0)
                        (>= v0x7f58356e8cd0_0 v0x7f58356e7c10_0)))))
      (a!2 (=> v0x7f58356e8c10_0
               (or (and E0x7f58356e8d90 (not E0x7f58356e8f50))
                   (and E0x7f58356e8f50 (not E0x7f58356e8d90)))))
      (a!3 (or (and v0x7f58356e9650_0
                    E0x7f58356ea850
                    (<= v0x7f58356ea6d0_0 v0x7f58356e8cd0_0)
                    (>= v0x7f58356ea6d0_0 v0x7f58356e8cd0_0)
                    (<= v0x7f58356ea790_0 v0x7f58356e9a10_0)
                    (>= v0x7f58356ea790_0 v0x7f58356e9a10_0))
               (and v0x7f58356ea390_0
                    E0x7f58356eab10
                    (and (<= v0x7f58356ea6d0_0 v0x7f58356ea110_0)
                         (>= v0x7f58356ea6d0_0 v0x7f58356ea110_0))
                    (<= v0x7f58356ea790_0 v0x7f58356e7d90_0)
                    (>= v0x7f58356ea790_0 v0x7f58356e7d90_0))
               (and v0x7f58356e9b50_0
                    E0x7f58356eadd0
                    (not v0x7f58356ea250_0)
                    (and (<= v0x7f58356ea6d0_0 v0x7f58356ea110_0)
                         (>= v0x7f58356ea6d0_0 v0x7f58356ea110_0))
                    (<= v0x7f58356ea790_0 0.0)
                    (>= v0x7f58356ea790_0 0.0))))
      (a!4 (=> v0x7f58356ea610_0
               (or (and E0x7f58356ea850
                        (not E0x7f58356eab10)
                        (not E0x7f58356eadd0))
                   (and E0x7f58356eab10
                        (not E0x7f58356ea850)
                        (not E0x7f58356eadd0))
                   (and E0x7f58356eadd0
                        (not E0x7f58356ea850)
                        (not E0x7f58356eab10))))))
(let ((a!5 (and (=> v0x7f58356e84d0_0
                    (and v0x7f58356e8210_0
                         E0x7f58356e8590
                         (not v0x7f58356e8390_0)))
                (=> v0x7f58356e84d0_0 E0x7f58356e8590)
                a!1
                a!2
                (=> v0x7f58356e9650_0
                    (and v0x7f58356e8c10_0 E0x7f58356e9710 v0x7f58356e9510_0))
                (=> v0x7f58356e9650_0 E0x7f58356e9710)
                (=> v0x7f58356e9b50_0
                    (and v0x7f58356e8c10_0
                         E0x7f58356e9c10
                         (not v0x7f58356e9510_0)))
                (=> v0x7f58356e9b50_0 E0x7f58356e9c10)
                (=> v0x7f58356ea390_0
                    (and v0x7f58356e9b50_0 E0x7f58356ea450 v0x7f58356ea250_0))
                (=> v0x7f58356ea390_0 E0x7f58356ea450)
                (=> v0x7f58356ea610_0 a!3)
                a!4
                v0x7f58356ea610_0
                (not v0x7f58356eb810_0)
                (<= v0x7f58356e7e50_0 v0x7f58356ea6d0_0)
                (>= v0x7f58356e7e50_0 v0x7f58356ea6d0_0)
                (<= v0x7f58356e6010_0 v0x7f58356ea790_0)
                (>= v0x7f58356e6010_0 v0x7f58356ea790_0)
                (= v0x7f58356e8390_0 (= v0x7f58356e82d0_0 0.0))
                (= v0x7f58356e87d0_0 (< v0x7f58356e7c10_0 2.0))
                (= v0x7f58356e8990_0 (ite v0x7f58356e87d0_0 1.0 0.0))
                (= v0x7f58356e8ad0_0 (+ v0x7f58356e8990_0 v0x7f58356e7c10_0))
                (= v0x7f58356e9510_0 (= v0x7f58356e7d90_0 0.0))
                (= v0x7f58356e98d0_0 (> v0x7f58356e8cd0_0 1.0))
                (= v0x7f58356e9a10_0
                   (ite v0x7f58356e98d0_0 1.0 v0x7f58356e7d90_0))
                (= v0x7f58356e9e10_0 (> v0x7f58356e8cd0_0 0.0))
                (= v0x7f58356e9f50_0 (+ v0x7f58356e8cd0_0 (- 1.0)))
                (= v0x7f58356ea110_0
                   (ite v0x7f58356e9e10_0 v0x7f58356e9f50_0 v0x7f58356e8cd0_0))
                (= v0x7f58356ea250_0 (= v0x7f58356ea110_0 0.0))
                (= v0x7f58356eb310_0 (= v0x7f58356ea6d0_0 2.0))
                (= v0x7f58356eb450_0 (= v0x7f58356ea790_0 0.0))
                (= v0x7f58356eb590_0 (or v0x7f58356eb450_0 v0x7f58356eb310_0))
                (= v0x7f58356eb6d0_0 (xor v0x7f58356eb590_0 true))
                (= v0x7f58356eb810_0 (and v0x7f58356e9510_0 v0x7f58356eb6d0_0)))))
  (=> F0x7f58356ec410 a!5))))
(assert (=> F0x7f58356ec410 F0x7f58356ec350))
(assert (let ((a!1 (=> v0x7f58356e8c10_0
               (or (and v0x7f58356e84d0_0
                        E0x7f58356e8d90
                        (<= v0x7f58356e8cd0_0 v0x7f58356e8ad0_0)
                        (>= v0x7f58356e8cd0_0 v0x7f58356e8ad0_0))
                   (and v0x7f58356e8210_0
                        E0x7f58356e8f50
                        v0x7f58356e8390_0
                        (<= v0x7f58356e8cd0_0 v0x7f58356e7c10_0)
                        (>= v0x7f58356e8cd0_0 v0x7f58356e7c10_0)))))
      (a!2 (=> v0x7f58356e8c10_0
               (or (and E0x7f58356e8d90 (not E0x7f58356e8f50))
                   (and E0x7f58356e8f50 (not E0x7f58356e8d90)))))
      (a!3 (or (and v0x7f58356e9650_0
                    E0x7f58356ea850
                    (<= v0x7f58356ea6d0_0 v0x7f58356e8cd0_0)
                    (>= v0x7f58356ea6d0_0 v0x7f58356e8cd0_0)
                    (<= v0x7f58356ea790_0 v0x7f58356e9a10_0)
                    (>= v0x7f58356ea790_0 v0x7f58356e9a10_0))
               (and v0x7f58356ea390_0
                    E0x7f58356eab10
                    (and (<= v0x7f58356ea6d0_0 v0x7f58356ea110_0)
                         (>= v0x7f58356ea6d0_0 v0x7f58356ea110_0))
                    (<= v0x7f58356ea790_0 v0x7f58356e7d90_0)
                    (>= v0x7f58356ea790_0 v0x7f58356e7d90_0))
               (and v0x7f58356e9b50_0
                    E0x7f58356eadd0
                    (not v0x7f58356ea250_0)
                    (and (<= v0x7f58356ea6d0_0 v0x7f58356ea110_0)
                         (>= v0x7f58356ea6d0_0 v0x7f58356ea110_0))
                    (<= v0x7f58356ea790_0 0.0)
                    (>= v0x7f58356ea790_0 0.0))))
      (a!4 (=> v0x7f58356ea610_0
               (or (and E0x7f58356ea850
                        (not E0x7f58356eab10)
                        (not E0x7f58356eadd0))
                   (and E0x7f58356eab10
                        (not E0x7f58356ea850)
                        (not E0x7f58356eadd0))
                   (and E0x7f58356eadd0
                        (not E0x7f58356ea850)
                        (not E0x7f58356eab10))))))
(let ((a!5 (and (=> v0x7f58356e84d0_0
                    (and v0x7f58356e8210_0
                         E0x7f58356e8590
                         (not v0x7f58356e8390_0)))
                (=> v0x7f58356e84d0_0 E0x7f58356e8590)
                a!1
                a!2
                (=> v0x7f58356e9650_0
                    (and v0x7f58356e8c10_0 E0x7f58356e9710 v0x7f58356e9510_0))
                (=> v0x7f58356e9650_0 E0x7f58356e9710)
                (=> v0x7f58356e9b50_0
                    (and v0x7f58356e8c10_0
                         E0x7f58356e9c10
                         (not v0x7f58356e9510_0)))
                (=> v0x7f58356e9b50_0 E0x7f58356e9c10)
                (=> v0x7f58356ea390_0
                    (and v0x7f58356e9b50_0 E0x7f58356ea450 v0x7f58356ea250_0))
                (=> v0x7f58356ea390_0 E0x7f58356ea450)
                (=> v0x7f58356ea610_0 a!3)
                a!4
                v0x7f58356ea610_0
                v0x7f58356eb810_0
                (= v0x7f58356e8390_0 (= v0x7f58356e82d0_0 0.0))
                (= v0x7f58356e87d0_0 (< v0x7f58356e7c10_0 2.0))
                (= v0x7f58356e8990_0 (ite v0x7f58356e87d0_0 1.0 0.0))
                (= v0x7f58356e8ad0_0 (+ v0x7f58356e8990_0 v0x7f58356e7c10_0))
                (= v0x7f58356e9510_0 (= v0x7f58356e7d90_0 0.0))
                (= v0x7f58356e98d0_0 (> v0x7f58356e8cd0_0 1.0))
                (= v0x7f58356e9a10_0
                   (ite v0x7f58356e98d0_0 1.0 v0x7f58356e7d90_0))
                (= v0x7f58356e9e10_0 (> v0x7f58356e8cd0_0 0.0))
                (= v0x7f58356e9f50_0 (+ v0x7f58356e8cd0_0 (- 1.0)))
                (= v0x7f58356ea110_0
                   (ite v0x7f58356e9e10_0 v0x7f58356e9f50_0 v0x7f58356e8cd0_0))
                (= v0x7f58356ea250_0 (= v0x7f58356ea110_0 0.0))
                (= v0x7f58356eb310_0 (= v0x7f58356ea6d0_0 2.0))
                (= v0x7f58356eb450_0 (= v0x7f58356ea790_0 0.0))
                (= v0x7f58356eb590_0 (or v0x7f58356eb450_0 v0x7f58356eb310_0))
                (= v0x7f58356eb6d0_0 (xor v0x7f58356eb590_0 true))
                (= v0x7f58356eb810_0 (and v0x7f58356e9510_0 v0x7f58356eb6d0_0)))))
  (=> F0x7f58356ec290 a!5))))
(assert (=> F0x7f58356ec290 F0x7f58356ec350))
(assert (=> F0x7f58356ec650 (or F0x7f58356ec590 F0x7f58356ec410)))
(assert (=> F0x7f58356ec610 F0x7f58356ec290))
(assert (=> pre!entry!0 (=> F0x7f58356ec4d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f58356ec350 (<= v0x7f58356e7c10_0 2.0))))
(assert (let ((a!1 (not (or (not (>= v0x7f58356e7d90_0 0.0))
                    (not (<= v0x7f58356e7c10_0 1.0))
                    (not (<= v0x7f58356e7d90_0 0.0))
                    (not (>= v0x7f58356e7c10_0 1.0))))))
  (=> pre!bb1.i.i!1 (=> F0x7f58356ec350 (or (>= v0x7f58356e7c10_0 2.0) a!1)))))
(assert (let ((a!1 (not (or (not (>= v0x7f58356e6010_0 0.0))
                    (not (<= v0x7f58356e7e50_0 1.0))
                    (not (<= v0x7f58356e6010_0 0.0))
                    (not (>= v0x7f58356e7e50_0 1.0))))))
(let ((a!2 (and (not post!bb1.i.i!1)
                F0x7f58356ec650
                (not (or (>= v0x7f58356e7e50_0 2.0) a!1)))))
  (or (and (not post!bb1.i.i!0)
           F0x7f58356ec650
           (not (<= v0x7f58356e7e50_0 2.0)))
      a!2
      (and (not post!bb2.i.i28.i.i!0) F0x7f58356ec610 true)))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i28.i.i!0)
