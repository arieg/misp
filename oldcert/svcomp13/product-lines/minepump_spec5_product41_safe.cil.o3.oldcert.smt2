(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun F0x7f3e89871f50 () Bool)
(declare-fun F0x7f3e89871f90 () Bool)
(declare-fun F0x7f3e89871e90 () Bool)
(declare-fun F0x7f3e89871dd0 () Bool)
(declare-fun v0x7f3e89870c90_0 () Bool)
(declare-fun v0x7f3e89870b50_0 () Bool)
(declare-fun v0x7f3e89870110_0 () Bool)
(declare-fun v0x7f3e8986f050_0 () Bool)
(declare-fun v0x7f3e8986d2d0_0 () Real)
(declare-fun v0x7f3e89870f10_0 () Bool)
(declare-fun v0x7f3e8986d110_0 () Bool)
(declare-fun v0x7f3e89870dd0_0 () Bool)
(declare-fun v0x7f3e8986cc10_0 () Real)
(declare-fun v0x7f3e89871050_0 () Bool)
(declare-fun v0x7f3e8986dcd0_0 () Real)
(declare-fun E0x7f3e898706d0 () Bool)
(declare-fun E0x7f3e89870510 () Bool)
(declare-fun v0x7f3e8986fd50_0 () Bool)
(declare-fun E0x7f3e8986ff50 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f3e8986fe90_0 () Bool)
(declare-fun v0x7f3e8986f350_0 () Real)
(declare-fun v0x7f3e8986ec50_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f3e8986f190_0 () Real)
(declare-fun E0x7f3e8986ee50 () Bool)
(declare-fun E0x7f3e8986f610 () Bool)
(declare-fun v0x7f3e8986c490_0 () Real)
(declare-fun E0x7f3e8986e750 () Bool)
(declare-fun v0x7f3e8986e4d0_0 () Real)
(declare-fun v0x7f3e8986e190_0 () Bool)
(declare-fun v0x7f3e8986e410_0 () Bool)
(declare-fun v0x7f3e8986dd90_0 () Bool)
(declare-fun E0x7f3e8986e590 () Bool)
(declare-fun v0x7f3e8986ded0_0 () Bool)
(declare-fun v0x7f3e8986c590_0 () Real)
(declare-fun E0x7f3e8986d890 () Bool)
(declare-fun v0x7f3e8986ed90_0 () Bool)
(declare-fun v0x7f3e89870390_0 () Bool)
(declare-fun v0x7f3e8986fc10_0 () Bool)
(declare-fun v0x7f3e8986d410_0 () Real)
(declare-fun v0x7f3e89870450_0 () Real)
(declare-fun E0x7f3e8986d6d0 () Bool)
(declare-fun v0x7f3e8986d610_0 () Real)
(declare-fun E0x7f3e8986f7d0 () Bool)
(declare-fun v0x7f3e8986d550_0 () Bool)
(declare-fun E0x7f3e8986ced0 () Bool)
(declare-fun v0x7f3e8986cb50_0 () Bool)
(declare-fun v0x7f3e8986e2d0_0 () Real)
(declare-fun F0x7f3e89871c10 () Bool)
(declare-fun F0x7f3e89871cd0 () Bool)
(declare-fun v0x7f3e8986b010_0 () Real)
(declare-fun v0x7f3e8986c310_0 () Real)
(declare-fun v0x7f3e8986c650_0 () Real)
(declare-fun E0x7f3e8986df90 () Bool)
(declare-fun v0x7f3e8986ce10_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun v0x7f3e8986c550_0 () Real)
(declare-fun v0x7f3e8986f550_0 () Real)
(declare-fun v0x7f3e8986f490_0 () Bool)
(declare-fun v0x7f3e8986ccd0_0 () Bool)
(declare-fun v0x7f3e89870250_0 () Real)
(declare-fun v0x7f3e8986b110_0 () Bool)
(declare-fun F0x7f3e89871d90 () Bool)

(assert (=> F0x7f3e89871d90
    (and v0x7f3e8986b110_0
         (<= v0x7f3e8986c550_0 0.0)
         (>= v0x7f3e8986c550_0 0.0)
         (<= v0x7f3e8986c650_0 0.0)
         (>= v0x7f3e8986c650_0 0.0)
         (<= v0x7f3e8986b010_0 1.0)
         (>= v0x7f3e8986b010_0 1.0))))
(assert (=> F0x7f3e89871d90 F0x7f3e89871cd0))
(assert (let ((a!1 (=> v0x7f3e8986d550_0
               (or (and v0x7f3e8986ce10_0
                        E0x7f3e8986d6d0
                        (<= v0x7f3e8986d610_0 v0x7f3e8986d410_0)
                        (>= v0x7f3e8986d610_0 v0x7f3e8986d410_0))
                   (and v0x7f3e8986cb50_0
                        E0x7f3e8986d890
                        v0x7f3e8986ccd0_0
                        (<= v0x7f3e8986d610_0 v0x7f3e8986c590_0)
                        (>= v0x7f3e8986d610_0 v0x7f3e8986c590_0)))))
      (a!2 (=> v0x7f3e8986d550_0
               (or (and E0x7f3e8986d6d0 (not E0x7f3e8986d890))
                   (and E0x7f3e8986d890 (not E0x7f3e8986d6d0)))))
      (a!3 (=> v0x7f3e8986e410_0
               (or (and v0x7f3e8986ded0_0
                        E0x7f3e8986e590
                        (<= v0x7f3e8986e4d0_0 v0x7f3e8986e2d0_0)
                        (>= v0x7f3e8986e4d0_0 v0x7f3e8986e2d0_0))
                   (and v0x7f3e8986d550_0
                        E0x7f3e8986e750
                        v0x7f3e8986dd90_0
                        (<= v0x7f3e8986e4d0_0 v0x7f3e8986c490_0)
                        (>= v0x7f3e8986e4d0_0 v0x7f3e8986c490_0)))))
      (a!4 (=> v0x7f3e8986e410_0
               (or (and E0x7f3e8986e590 (not E0x7f3e8986e750))
                   (and E0x7f3e8986e750 (not E0x7f3e8986e590)))))
      (a!5 (=> v0x7f3e8986f490_0
               (or (and v0x7f3e8986ed90_0
                        E0x7f3e8986f610
                        (<= v0x7f3e8986f550_0 v0x7f3e8986f350_0)
                        (>= v0x7f3e8986f550_0 v0x7f3e8986f350_0))
                   (and v0x7f3e8986e410_0
                        E0x7f3e8986f7d0
                        v0x7f3e8986ec50_0
                        (<= v0x7f3e8986f550_0 v0x7f3e8986d610_0)
                        (>= v0x7f3e8986f550_0 v0x7f3e8986d610_0)))))
      (a!6 (=> v0x7f3e8986f490_0
               (or (and E0x7f3e8986f610 (not E0x7f3e8986f7d0))
                   (and E0x7f3e8986f7d0 (not E0x7f3e8986f610)))))
      (a!7 (=> v0x7f3e89870390_0
               (or (and v0x7f3e8986fe90_0
                        E0x7f3e89870510
                        (<= v0x7f3e89870450_0 v0x7f3e89870250_0)
                        (>= v0x7f3e89870450_0 v0x7f3e89870250_0))
                   (and v0x7f3e8986f490_0
                        E0x7f3e898706d0
                        (not v0x7f3e8986fd50_0)
                        (<= v0x7f3e89870450_0 v0x7f3e8986c310_0)
                        (>= v0x7f3e89870450_0 v0x7f3e8986c310_0)))))
      (a!8 (=> v0x7f3e89870390_0
               (or (and E0x7f3e89870510 (not E0x7f3e898706d0))
                   (and E0x7f3e898706d0 (not E0x7f3e89870510))))))
(let ((a!9 (and (=> v0x7f3e8986ce10_0
                    (and v0x7f3e8986cb50_0
                         E0x7f3e8986ced0
                         (not v0x7f3e8986ccd0_0)))
                (=> v0x7f3e8986ce10_0 E0x7f3e8986ced0)
                a!1
                a!2
                (=> v0x7f3e8986ded0_0
                    (and v0x7f3e8986d550_0
                         E0x7f3e8986df90
                         (not v0x7f3e8986dd90_0)))
                (=> v0x7f3e8986ded0_0 E0x7f3e8986df90)
                a!3
                a!4
                (=> v0x7f3e8986ed90_0
                    (and v0x7f3e8986e410_0
                         E0x7f3e8986ee50
                         (not v0x7f3e8986ec50_0)))
                (=> v0x7f3e8986ed90_0 E0x7f3e8986ee50)
                a!5
                a!6
                (=> v0x7f3e8986fe90_0
                    (and v0x7f3e8986f490_0 E0x7f3e8986ff50 v0x7f3e8986fd50_0))
                (=> v0x7f3e8986fe90_0 E0x7f3e8986ff50)
                a!7
                a!8
                v0x7f3e89870390_0
                (not v0x7f3e89871050_0)
                (<= v0x7f3e8986c550_0 v0x7f3e89870450_0)
                (>= v0x7f3e8986c550_0 v0x7f3e89870450_0)
                (<= v0x7f3e8986c650_0 v0x7f3e8986e4d0_0)
                (>= v0x7f3e8986c650_0 v0x7f3e8986e4d0_0)
                (<= v0x7f3e8986b010_0 v0x7f3e8986f550_0)
                (>= v0x7f3e8986b010_0 v0x7f3e8986f550_0)
                (= v0x7f3e8986ccd0_0 (= v0x7f3e8986cc10_0 0.0))
                (= v0x7f3e8986d110_0 (< v0x7f3e8986c590_0 2.0))
                (= v0x7f3e8986d2d0_0 (ite v0x7f3e8986d110_0 1.0 0.0))
                (= v0x7f3e8986d410_0 (+ v0x7f3e8986d2d0_0 v0x7f3e8986c590_0))
                (= v0x7f3e8986dd90_0 (= v0x7f3e8986dcd0_0 0.0))
                (= v0x7f3e8986e190_0 (= v0x7f3e8986c490_0 0.0))
                (= v0x7f3e8986e2d0_0 (ite v0x7f3e8986e190_0 1.0 0.0))
                (= v0x7f3e8986ec50_0 (= v0x7f3e8986c310_0 0.0))
                (= v0x7f3e8986f050_0 (> v0x7f3e8986d610_0 0.0))
                (= v0x7f3e8986f190_0 (+ v0x7f3e8986d610_0 (- 1.0)))
                (= v0x7f3e8986f350_0
                   (ite v0x7f3e8986f050_0 v0x7f3e8986f190_0 v0x7f3e8986d610_0))
                (= v0x7f3e8986fc10_0 (> v0x7f3e8986f550_0 1.0))
                (= v0x7f3e8986fd50_0 (and v0x7f3e8986fc10_0 v0x7f3e8986ec50_0))
                (= v0x7f3e89870110_0 (= v0x7f3e8986e4d0_0 0.0))
                (= v0x7f3e89870250_0
                   (ite v0x7f3e89870110_0 1.0 v0x7f3e8986c310_0))
                (= v0x7f3e89870b50_0 (= v0x7f3e8986f550_0 2.0))
                (= v0x7f3e89870c90_0 (= v0x7f3e89870450_0 0.0))
                (= v0x7f3e89870dd0_0 (or v0x7f3e89870c90_0 v0x7f3e89870b50_0))
                (= v0x7f3e89870f10_0 (xor v0x7f3e89870dd0_0 true))
                (= v0x7f3e89871050_0 (and v0x7f3e8986ec50_0 v0x7f3e89870f10_0)))))
  (=> F0x7f3e89871c10 a!9))))
(assert (=> F0x7f3e89871c10 F0x7f3e89871dd0))
(assert (let ((a!1 (=> v0x7f3e8986d550_0
               (or (and v0x7f3e8986ce10_0
                        E0x7f3e8986d6d0
                        (<= v0x7f3e8986d610_0 v0x7f3e8986d410_0)
                        (>= v0x7f3e8986d610_0 v0x7f3e8986d410_0))
                   (and v0x7f3e8986cb50_0
                        E0x7f3e8986d890
                        v0x7f3e8986ccd0_0
                        (<= v0x7f3e8986d610_0 v0x7f3e8986c590_0)
                        (>= v0x7f3e8986d610_0 v0x7f3e8986c590_0)))))
      (a!2 (=> v0x7f3e8986d550_0
               (or (and E0x7f3e8986d6d0 (not E0x7f3e8986d890))
                   (and E0x7f3e8986d890 (not E0x7f3e8986d6d0)))))
      (a!3 (=> v0x7f3e8986e410_0
               (or (and v0x7f3e8986ded0_0
                        E0x7f3e8986e590
                        (<= v0x7f3e8986e4d0_0 v0x7f3e8986e2d0_0)
                        (>= v0x7f3e8986e4d0_0 v0x7f3e8986e2d0_0))
                   (and v0x7f3e8986d550_0
                        E0x7f3e8986e750
                        v0x7f3e8986dd90_0
                        (<= v0x7f3e8986e4d0_0 v0x7f3e8986c490_0)
                        (>= v0x7f3e8986e4d0_0 v0x7f3e8986c490_0)))))
      (a!4 (=> v0x7f3e8986e410_0
               (or (and E0x7f3e8986e590 (not E0x7f3e8986e750))
                   (and E0x7f3e8986e750 (not E0x7f3e8986e590)))))
      (a!5 (=> v0x7f3e8986f490_0
               (or (and v0x7f3e8986ed90_0
                        E0x7f3e8986f610
                        (<= v0x7f3e8986f550_0 v0x7f3e8986f350_0)
                        (>= v0x7f3e8986f550_0 v0x7f3e8986f350_0))
                   (and v0x7f3e8986e410_0
                        E0x7f3e8986f7d0
                        v0x7f3e8986ec50_0
                        (<= v0x7f3e8986f550_0 v0x7f3e8986d610_0)
                        (>= v0x7f3e8986f550_0 v0x7f3e8986d610_0)))))
      (a!6 (=> v0x7f3e8986f490_0
               (or (and E0x7f3e8986f610 (not E0x7f3e8986f7d0))
                   (and E0x7f3e8986f7d0 (not E0x7f3e8986f610)))))
      (a!7 (=> v0x7f3e89870390_0
               (or (and v0x7f3e8986fe90_0
                        E0x7f3e89870510
                        (<= v0x7f3e89870450_0 v0x7f3e89870250_0)
                        (>= v0x7f3e89870450_0 v0x7f3e89870250_0))
                   (and v0x7f3e8986f490_0
                        E0x7f3e898706d0
                        (not v0x7f3e8986fd50_0)
                        (<= v0x7f3e89870450_0 v0x7f3e8986c310_0)
                        (>= v0x7f3e89870450_0 v0x7f3e8986c310_0)))))
      (a!8 (=> v0x7f3e89870390_0
               (or (and E0x7f3e89870510 (not E0x7f3e898706d0))
                   (and E0x7f3e898706d0 (not E0x7f3e89870510))))))
(let ((a!9 (and (=> v0x7f3e8986ce10_0
                    (and v0x7f3e8986cb50_0
                         E0x7f3e8986ced0
                         (not v0x7f3e8986ccd0_0)))
                (=> v0x7f3e8986ce10_0 E0x7f3e8986ced0)
                a!1
                a!2
                (=> v0x7f3e8986ded0_0
                    (and v0x7f3e8986d550_0
                         E0x7f3e8986df90
                         (not v0x7f3e8986dd90_0)))
                (=> v0x7f3e8986ded0_0 E0x7f3e8986df90)
                a!3
                a!4
                (=> v0x7f3e8986ed90_0
                    (and v0x7f3e8986e410_0
                         E0x7f3e8986ee50
                         (not v0x7f3e8986ec50_0)))
                (=> v0x7f3e8986ed90_0 E0x7f3e8986ee50)
                a!5
                a!6
                (=> v0x7f3e8986fe90_0
                    (and v0x7f3e8986f490_0 E0x7f3e8986ff50 v0x7f3e8986fd50_0))
                (=> v0x7f3e8986fe90_0 E0x7f3e8986ff50)
                a!7
                a!8
                v0x7f3e89870390_0
                v0x7f3e89871050_0
                (= v0x7f3e8986ccd0_0 (= v0x7f3e8986cc10_0 0.0))
                (= v0x7f3e8986d110_0 (< v0x7f3e8986c590_0 2.0))
                (= v0x7f3e8986d2d0_0 (ite v0x7f3e8986d110_0 1.0 0.0))
                (= v0x7f3e8986d410_0 (+ v0x7f3e8986d2d0_0 v0x7f3e8986c590_0))
                (= v0x7f3e8986dd90_0 (= v0x7f3e8986dcd0_0 0.0))
                (= v0x7f3e8986e190_0 (= v0x7f3e8986c490_0 0.0))
                (= v0x7f3e8986e2d0_0 (ite v0x7f3e8986e190_0 1.0 0.0))
                (= v0x7f3e8986ec50_0 (= v0x7f3e8986c310_0 0.0))
                (= v0x7f3e8986f050_0 (> v0x7f3e8986d610_0 0.0))
                (= v0x7f3e8986f190_0 (+ v0x7f3e8986d610_0 (- 1.0)))
                (= v0x7f3e8986f350_0
                   (ite v0x7f3e8986f050_0 v0x7f3e8986f190_0 v0x7f3e8986d610_0))
                (= v0x7f3e8986fc10_0 (> v0x7f3e8986f550_0 1.0))
                (= v0x7f3e8986fd50_0 (and v0x7f3e8986fc10_0 v0x7f3e8986ec50_0))
                (= v0x7f3e89870110_0 (= v0x7f3e8986e4d0_0 0.0))
                (= v0x7f3e89870250_0
                   (ite v0x7f3e89870110_0 1.0 v0x7f3e8986c310_0))
                (= v0x7f3e89870b50_0 (= v0x7f3e8986f550_0 2.0))
                (= v0x7f3e89870c90_0 (= v0x7f3e89870450_0 0.0))
                (= v0x7f3e89870dd0_0 (or v0x7f3e89870c90_0 v0x7f3e89870b50_0))
                (= v0x7f3e89870f10_0 (xor v0x7f3e89870dd0_0 true))
                (= v0x7f3e89871050_0 (and v0x7f3e8986ec50_0 v0x7f3e89870f10_0)))))
  (=> F0x7f3e89871e90 a!9))))
(assert (=> F0x7f3e89871e90 F0x7f3e89871dd0))
(assert (=> F0x7f3e89871f90 (or F0x7f3e89871d90 F0x7f3e89871c10)))
(assert (=> F0x7f3e89871f50 F0x7f3e89871e90))
(assert (=> pre!entry!0 (=> F0x7f3e89871cd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f3e89871dd0 (<= v0x7f3e8986c590_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f3e89871dd0
        (or (<= v0x7f3e8986c590_0 1.0) (>= v0x7f3e8986c590_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f3e89871dd0 (>= v0x7f3e8986c590_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f3e89871dd0
        (or (>= v0x7f3e8986c590_0 1.0) (<= v0x7f3e8986c590_0 0.0)))))
(assert (let ((a!1 (and (not post!bb1.i.i!1)
                F0x7f3e89871f90
                (not (or (<= v0x7f3e8986b010_0 1.0) (>= v0x7f3e8986b010_0 2.0)))))
      (a!2 (and (not post!bb1.i.i!3)
                F0x7f3e89871f90
                (not (or (>= v0x7f3e8986b010_0 1.0) (<= v0x7f3e8986b010_0 0.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7f3e89871f90
           (not (<= v0x7f3e8986b010_0 2.0)))
      a!1
      (and (not post!bb1.i.i!2)
           F0x7f3e89871f90
           (not (>= v0x7f3e8986b010_0 0.0)))
      a!2
      (and (not post!bb2.i.i23.i.i!0) F0x7f3e89871f50 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i23.i.i!0)
