(declare-fun post!bb1.i.i35.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7faff07d8790 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7faff07d7910_0 () Bool)
(declare-fun v0x7faff07d77d0_0 () Bool)
(declare-fun v0x7faff07d5b10_0 () Bool)
(declare-fun v0x7faff07d47d0_0 () Real)
(declare-fun v0x7faff07d3710_0 () Real)
(declare-fun E0x7faff07d6fd0 () Bool)
(declare-fun v0x7faff07d6c50_0 () Real)
(declare-fun E0x7faff07d6d10 () Bool)
(declare-fun v0x7faff07d6ad0_0 () Bool)
(declare-fun v0x7faff07d6710_0 () Bool)
(declare-fun E0x7faff07d5e50 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7faff07d5d90_0 () Bool)
(declare-fun F0x7faff07d8950 () Bool)
(declare-fun v0x7faff07d2e10_0 () Real)
(declare-fun E0x7faff07d5250 () Bool)
(declare-fun v0x7faff07d5c50_0 () Real)
(declare-fun v0x7faff07d4c90_0 () Bool)
(declare-fun v0x7faff07d4dd0_0 () Real)
(declare-fun v0x7faff07d4f10_0 () Bool)
(declare-fun v0x7faff07d7a50_0 () Bool)
(declare-fun v0x7faff07d6050_0 () Bool)
(declare-fun v0x7faff07d4fd0_0 () Real)
(declare-fun v0x7faff07d3c10_0 () Bool)
(declare-fun v0x7faff07d4890_0 () Bool)
(declare-fun F0x7faff07d8850 () Bool)
(declare-fun v0x7faff07d49d0_0 () Bool)
(declare-fun v0x7faff07d6350_0 () Real)
(declare-fun v0x7faff07d3090_0 () Real)
(declare-fun v0x7faff07d5890_0 () Bool)
(declare-fun v0x7faff07d3f10_0 () Real)
(declare-fun E0x7faff07d6910 () Bool)
(declare-fun v0x7faff07d6490_0 () Bool)
(declare-fun v0x7faff07d6190_0 () Real)
(declare-fun E0x7faff07d5950 () Bool)
(declare-fun v0x7faff07d4110_0 () Real)
(declare-fun v0x7faff07d4050_0 () Bool)
(declare-fun E0x7faff07d7290 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7faff07d39d0 () Bool)
(declare-fun F0x7faff07d86d0 () Bool)
(declare-fun v0x7faff07d6850_0 () Bool)
(declare-fun v0x7faff07d5750_0 () Bool)
(declare-fun F0x7faff07d85d0 () Bool)
(declare-fun v0x7faff07d65d0_0 () Bool)
(declare-fun v0x7faff07d2f90_0 () Real)
(declare-fun E0x7faff07d4390 () Bool)
(declare-fun E0x7faff07d5090 () Bool)
(declare-fun v0x7faff07d3dd0_0 () Real)
(declare-fun v0x7faff07d0010_0 () Real)
(declare-fun F0x7faff07d8990 () Bool)
(declare-fun v0x7faff07d3150_0 () Real)
(declare-fun v0x7faff07d3050_0 () Real)
(declare-fun v0x7faff07d6b90_0 () Real)
(declare-fun E0x7faff07d41d0 () Bool)
(declare-fun v0x7faff07d0110_0 () Bool)
(declare-fun E0x7faff07d4a90 () Bool)
(declare-fun F0x7faff07d8690 () Bool)
(declare-fun v0x7faff07d3650_0 () Bool)
(declare-fun v0x7faff07d37d0_0 () Bool)
(declare-fun v0x7faff07d3910_0 () Bool)

(assert (=> F0x7faff07d8690
    (and v0x7faff07d0110_0
         (<= v0x7faff07d3050_0 0.0)
         (>= v0x7faff07d3050_0 0.0)
         (<= v0x7faff07d3150_0 1.0)
         (>= v0x7faff07d3150_0 1.0)
         (<= v0x7faff07d0010_0 0.0)
         (>= v0x7faff07d0010_0 0.0))))
(assert (=> F0x7faff07d8690 F0x7faff07d85d0))
(assert (let ((a!1 (=> v0x7faff07d4050_0
               (or (and v0x7faff07d3910_0
                        E0x7faff07d41d0
                        (<= v0x7faff07d4110_0 v0x7faff07d3f10_0)
                        (>= v0x7faff07d4110_0 v0x7faff07d3f10_0))
                   (and v0x7faff07d3650_0
                        E0x7faff07d4390
                        v0x7faff07d37d0_0
                        (<= v0x7faff07d4110_0 v0x7faff07d2f90_0)
                        (>= v0x7faff07d4110_0 v0x7faff07d2f90_0)))))
      (a!2 (=> v0x7faff07d4050_0
               (or (and E0x7faff07d41d0 (not E0x7faff07d4390))
                   (and E0x7faff07d4390 (not E0x7faff07d41d0)))))
      (a!3 (=> v0x7faff07d4f10_0
               (or (and v0x7faff07d49d0_0
                        E0x7faff07d5090
                        (<= v0x7faff07d4fd0_0 v0x7faff07d4dd0_0)
                        (>= v0x7faff07d4fd0_0 v0x7faff07d4dd0_0))
                   (and v0x7faff07d4050_0
                        E0x7faff07d5250
                        v0x7faff07d4890_0
                        (<= v0x7faff07d4fd0_0 v0x7faff07d2e10_0)
                        (>= v0x7faff07d4fd0_0 v0x7faff07d2e10_0)))))
      (a!4 (=> v0x7faff07d4f10_0
               (or (and E0x7faff07d5090 (not E0x7faff07d5250))
                   (and E0x7faff07d5250 (not E0x7faff07d5090)))))
      (a!5 (or (and v0x7faff07d5890_0
                    E0x7faff07d6d10
                    (<= v0x7faff07d6b90_0 v0x7faff07d4110_0)
                    (>= v0x7faff07d6b90_0 v0x7faff07d4110_0)
                    (<= v0x7faff07d6c50_0 v0x7faff07d5c50_0)
                    (>= v0x7faff07d6c50_0 v0x7faff07d5c50_0))
               (and v0x7faff07d6850_0
                    E0x7faff07d6fd0
                    (and (<= v0x7faff07d6b90_0 v0x7faff07d6350_0)
                         (>= v0x7faff07d6b90_0 v0x7faff07d6350_0))
                    (<= v0x7faff07d6c50_0 v0x7faff07d3090_0)
                    (>= v0x7faff07d6c50_0 v0x7faff07d3090_0))
               (and v0x7faff07d5d90_0
                    E0x7faff07d7290
                    (not v0x7faff07d6710_0)
                    (and (<= v0x7faff07d6b90_0 v0x7faff07d6350_0)
                         (>= v0x7faff07d6b90_0 v0x7faff07d6350_0))
                    (<= v0x7faff07d6c50_0 0.0)
                    (>= v0x7faff07d6c50_0 0.0))))
      (a!6 (=> v0x7faff07d6ad0_0
               (or (and E0x7faff07d6d10
                        (not E0x7faff07d6fd0)
                        (not E0x7faff07d7290))
                   (and E0x7faff07d6fd0
                        (not E0x7faff07d6d10)
                        (not E0x7faff07d7290))
                   (and E0x7faff07d7290
                        (not E0x7faff07d6d10)
                        (not E0x7faff07d6fd0))))))
(let ((a!7 (and (=> v0x7faff07d3910_0
                    (and v0x7faff07d3650_0
                         E0x7faff07d39d0
                         (not v0x7faff07d37d0_0)))
                (=> v0x7faff07d3910_0 E0x7faff07d39d0)
                a!1
                a!2
                (=> v0x7faff07d49d0_0
                    (and v0x7faff07d4050_0
                         E0x7faff07d4a90
                         (not v0x7faff07d4890_0)))
                (=> v0x7faff07d49d0_0 E0x7faff07d4a90)
                a!3
                a!4
                (=> v0x7faff07d5890_0
                    (and v0x7faff07d4f10_0 E0x7faff07d5950 v0x7faff07d5750_0))
                (=> v0x7faff07d5890_0 E0x7faff07d5950)
                (=> v0x7faff07d5d90_0
                    (and v0x7faff07d4f10_0
                         E0x7faff07d5e50
                         (not v0x7faff07d5750_0)))
                (=> v0x7faff07d5d90_0 E0x7faff07d5e50)
                (=> v0x7faff07d6850_0
                    (and v0x7faff07d5d90_0 E0x7faff07d6910 v0x7faff07d6710_0))
                (=> v0x7faff07d6850_0 E0x7faff07d6910)
                (=> v0x7faff07d6ad0_0 a!5)
                a!6
                v0x7faff07d6ad0_0
                v0x7faff07d7a50_0
                (<= v0x7faff07d3050_0 v0x7faff07d4fd0_0)
                (>= v0x7faff07d3050_0 v0x7faff07d4fd0_0)
                (<= v0x7faff07d3150_0 v0x7faff07d6b90_0)
                (>= v0x7faff07d3150_0 v0x7faff07d6b90_0)
                (<= v0x7faff07d0010_0 v0x7faff07d6c50_0)
                (>= v0x7faff07d0010_0 v0x7faff07d6c50_0)
                (= v0x7faff07d37d0_0 (= v0x7faff07d3710_0 0.0))
                (= v0x7faff07d3c10_0 (< v0x7faff07d2f90_0 2.0))
                (= v0x7faff07d3dd0_0 (ite v0x7faff07d3c10_0 1.0 0.0))
                (= v0x7faff07d3f10_0 (+ v0x7faff07d3dd0_0 v0x7faff07d2f90_0))
                (= v0x7faff07d4890_0 (= v0x7faff07d47d0_0 0.0))
                (= v0x7faff07d4c90_0 (= v0x7faff07d2e10_0 0.0))
                (= v0x7faff07d4dd0_0 (ite v0x7faff07d4c90_0 1.0 0.0))
                (= v0x7faff07d5750_0 (= v0x7faff07d3090_0 0.0))
                (= v0x7faff07d5b10_0 (> v0x7faff07d4110_0 1.0))
                (= v0x7faff07d5c50_0
                   (ite v0x7faff07d5b10_0 1.0 v0x7faff07d3090_0))
                (= v0x7faff07d6050_0 (> v0x7faff07d4110_0 0.0))
                (= v0x7faff07d6190_0 (+ v0x7faff07d4110_0 (- 1.0)))
                (= v0x7faff07d6350_0
                   (ite v0x7faff07d6050_0 v0x7faff07d6190_0 v0x7faff07d4110_0))
                (= v0x7faff07d6490_0 (= v0x7faff07d4fd0_0 0.0))
                (= v0x7faff07d65d0_0 (= v0x7faff07d6350_0 0.0))
                (= v0x7faff07d6710_0 (and v0x7faff07d6490_0 v0x7faff07d65d0_0))
                (= v0x7faff07d77d0_0 (not (= v0x7faff07d6b90_0 0.0)))
                (= v0x7faff07d7910_0 (= v0x7faff07d6c50_0 0.0))
                (= v0x7faff07d7a50_0 (or v0x7faff07d7910_0 v0x7faff07d77d0_0)))))
  (=> F0x7faff07d86d0 a!7))))
(assert (=> F0x7faff07d86d0 F0x7faff07d8790))
(assert (let ((a!1 (=> v0x7faff07d4050_0
               (or (and v0x7faff07d3910_0
                        E0x7faff07d41d0
                        (<= v0x7faff07d4110_0 v0x7faff07d3f10_0)
                        (>= v0x7faff07d4110_0 v0x7faff07d3f10_0))
                   (and v0x7faff07d3650_0
                        E0x7faff07d4390
                        v0x7faff07d37d0_0
                        (<= v0x7faff07d4110_0 v0x7faff07d2f90_0)
                        (>= v0x7faff07d4110_0 v0x7faff07d2f90_0)))))
      (a!2 (=> v0x7faff07d4050_0
               (or (and E0x7faff07d41d0 (not E0x7faff07d4390))
                   (and E0x7faff07d4390 (not E0x7faff07d41d0)))))
      (a!3 (=> v0x7faff07d4f10_0
               (or (and v0x7faff07d49d0_0
                        E0x7faff07d5090
                        (<= v0x7faff07d4fd0_0 v0x7faff07d4dd0_0)
                        (>= v0x7faff07d4fd0_0 v0x7faff07d4dd0_0))
                   (and v0x7faff07d4050_0
                        E0x7faff07d5250
                        v0x7faff07d4890_0
                        (<= v0x7faff07d4fd0_0 v0x7faff07d2e10_0)
                        (>= v0x7faff07d4fd0_0 v0x7faff07d2e10_0)))))
      (a!4 (=> v0x7faff07d4f10_0
               (or (and E0x7faff07d5090 (not E0x7faff07d5250))
                   (and E0x7faff07d5250 (not E0x7faff07d5090)))))
      (a!5 (or (and v0x7faff07d5890_0
                    E0x7faff07d6d10
                    (<= v0x7faff07d6b90_0 v0x7faff07d4110_0)
                    (>= v0x7faff07d6b90_0 v0x7faff07d4110_0)
                    (<= v0x7faff07d6c50_0 v0x7faff07d5c50_0)
                    (>= v0x7faff07d6c50_0 v0x7faff07d5c50_0))
               (and v0x7faff07d6850_0
                    E0x7faff07d6fd0
                    (and (<= v0x7faff07d6b90_0 v0x7faff07d6350_0)
                         (>= v0x7faff07d6b90_0 v0x7faff07d6350_0))
                    (<= v0x7faff07d6c50_0 v0x7faff07d3090_0)
                    (>= v0x7faff07d6c50_0 v0x7faff07d3090_0))
               (and v0x7faff07d5d90_0
                    E0x7faff07d7290
                    (not v0x7faff07d6710_0)
                    (and (<= v0x7faff07d6b90_0 v0x7faff07d6350_0)
                         (>= v0x7faff07d6b90_0 v0x7faff07d6350_0))
                    (<= v0x7faff07d6c50_0 0.0)
                    (>= v0x7faff07d6c50_0 0.0))))
      (a!6 (=> v0x7faff07d6ad0_0
               (or (and E0x7faff07d6d10
                        (not E0x7faff07d6fd0)
                        (not E0x7faff07d7290))
                   (and E0x7faff07d6fd0
                        (not E0x7faff07d6d10)
                        (not E0x7faff07d7290))
                   (and E0x7faff07d7290
                        (not E0x7faff07d6d10)
                        (not E0x7faff07d6fd0))))))
(let ((a!7 (and (=> v0x7faff07d3910_0
                    (and v0x7faff07d3650_0
                         E0x7faff07d39d0
                         (not v0x7faff07d37d0_0)))
                (=> v0x7faff07d3910_0 E0x7faff07d39d0)
                a!1
                a!2
                (=> v0x7faff07d49d0_0
                    (and v0x7faff07d4050_0
                         E0x7faff07d4a90
                         (not v0x7faff07d4890_0)))
                (=> v0x7faff07d49d0_0 E0x7faff07d4a90)
                a!3
                a!4
                (=> v0x7faff07d5890_0
                    (and v0x7faff07d4f10_0 E0x7faff07d5950 v0x7faff07d5750_0))
                (=> v0x7faff07d5890_0 E0x7faff07d5950)
                (=> v0x7faff07d5d90_0
                    (and v0x7faff07d4f10_0
                         E0x7faff07d5e50
                         (not v0x7faff07d5750_0)))
                (=> v0x7faff07d5d90_0 E0x7faff07d5e50)
                (=> v0x7faff07d6850_0
                    (and v0x7faff07d5d90_0 E0x7faff07d6910 v0x7faff07d6710_0))
                (=> v0x7faff07d6850_0 E0x7faff07d6910)
                (=> v0x7faff07d6ad0_0 a!5)
                a!6
                v0x7faff07d6ad0_0
                (not v0x7faff07d7a50_0)
                (= v0x7faff07d37d0_0 (= v0x7faff07d3710_0 0.0))
                (= v0x7faff07d3c10_0 (< v0x7faff07d2f90_0 2.0))
                (= v0x7faff07d3dd0_0 (ite v0x7faff07d3c10_0 1.0 0.0))
                (= v0x7faff07d3f10_0 (+ v0x7faff07d3dd0_0 v0x7faff07d2f90_0))
                (= v0x7faff07d4890_0 (= v0x7faff07d47d0_0 0.0))
                (= v0x7faff07d4c90_0 (= v0x7faff07d2e10_0 0.0))
                (= v0x7faff07d4dd0_0 (ite v0x7faff07d4c90_0 1.0 0.0))
                (= v0x7faff07d5750_0 (= v0x7faff07d3090_0 0.0))
                (= v0x7faff07d5b10_0 (> v0x7faff07d4110_0 1.0))
                (= v0x7faff07d5c50_0
                   (ite v0x7faff07d5b10_0 1.0 v0x7faff07d3090_0))
                (= v0x7faff07d6050_0 (> v0x7faff07d4110_0 0.0))
                (= v0x7faff07d6190_0 (+ v0x7faff07d4110_0 (- 1.0)))
                (= v0x7faff07d6350_0
                   (ite v0x7faff07d6050_0 v0x7faff07d6190_0 v0x7faff07d4110_0))
                (= v0x7faff07d6490_0 (= v0x7faff07d4fd0_0 0.0))
                (= v0x7faff07d65d0_0 (= v0x7faff07d6350_0 0.0))
                (= v0x7faff07d6710_0 (and v0x7faff07d6490_0 v0x7faff07d65d0_0))
                (= v0x7faff07d77d0_0 (not (= v0x7faff07d6b90_0 0.0)))
                (= v0x7faff07d7910_0 (= v0x7faff07d6c50_0 0.0))
                (= v0x7faff07d7a50_0 (or v0x7faff07d7910_0 v0x7faff07d77d0_0)))))
  (=> F0x7faff07d8850 a!7))))
(assert (=> F0x7faff07d8850 F0x7faff07d8790))
(assert (=> F0x7faff07d8990 (or F0x7faff07d8690 F0x7faff07d86d0)))
(assert (=> F0x7faff07d8950 F0x7faff07d8850))
(assert (=> pre!entry!0 (=> F0x7faff07d85d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7faff07d8790 (>= v0x7faff07d3090_0 0.0))))
(assert (let ((a!1 (=> F0x7faff07d8790
               (or (<= v0x7faff07d3090_0 0.0) (not (<= v0x7faff07d2f90_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (not (or (<= v0x7faff07d0010_0 0.0) (not (<= v0x7faff07d3150_0 1.0))))))
  (or (and (not post!bb1.i.i!0)
           F0x7faff07d8990
           (not (>= v0x7faff07d0010_0 0.0)))
      (and (not post!bb1.i.i!1) F0x7faff07d8990 a!1)
      (and (not post!bb1.i.i35.i.i!0) F0x7faff07d8950 true))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
;(post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i35.i.i!0)
