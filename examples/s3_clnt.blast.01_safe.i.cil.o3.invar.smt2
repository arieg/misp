(declare-fun v%_22_0 () Real)
(declare-fun CP!__UFO__0
             (Real
              Real
              Real
              Real
              Real
              Real
              Real
              Real
              Real
              Real
              Bool
              Real
              Real
              Real
              Bool
              Bool
              Bool
              Real
              Real
              Real
              Real
              Real
              Bool
              Real
              Real
              Real
              Real
              Real
              Real
              Real
              Real
              Real)
             Bool)
(declare-fun v%.32.9.2_0 () Real)
(declare-fun v%blastFlag.i.2_0 () Real)
(declare-fun v%_20_0 () Real)
(declare-fun v%_47_0 () Real)
(declare-fun v%_46_0 () Real)
(declare-fun v%_45_0 () Real)
(declare-fun v%.15.2_0 () Real)
(declare-fun v%_43_0 () Real)
(declare-fun v%storemerge1_0 () Real)
(declare-fun v%.13.2_0 () Real)
(declare-fun v%storemerge_0 () Real)
(declare-fun v%_38_0 () Bool)
(declare-fun v%_44_0 () Bool)
(declare-fun v%_37_0 () Bool)
(declare-fun v%_36_0 () Real)
(declare-fun v%_32_0 () Real)
(declare-fun v%.13.1_0 () Real)
(declare-fun v%_31_0 () Real)
(declare-fun v%_39_0 () Bool)
(declare-fun v%storemerge2_0 () Real)
(declare-fun v%.c_0 () Real)
(declare-fun v%_5_0 () Real)
(declare-fun v%_21_0 () Real)
(declare-fun v%_10_0 () Real)
(declare-fun v%_4_0 () Real)
(declare-fun v%_3_0 () Real)
(declare-fun v%_34_0 () Bool)
(declare-fun v%_2_0 () Real)
(declare-fun v%_35_0 () Real)
(declare-fun CP!NodeBlock115
             (Real
              Real
              Real
              Real
              Real
              Real
              Real
              Bool
              Real
              Real
              Real
              Bool
              Bool
              Bool
              Real
              Real
              Real
              Real
              Real
              Bool
              Real
              Real
              Real
              Real
              Real
              Real
              Real
              Real
              Real)
             Bool)
(declare-fun v%_40_0 () Real)
(declare-fun CP!entry () Bool)
(declare-fun v%.0.1_0 () Real)

(assert (=> CP!entry true))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or v%_39_0 (>= v%storemerge2_0 4368.0))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (>= v%.13.1_0 12292.0) v%_34_0)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (>= v%.13.1_0 4368.0) (<= v%.13.1_0 3.0) (<= v%.32.9.2_0 4560.0))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (<= v%.13.1_0 4560.0) (not (<= 5.0 v%blastFlag.i.2_0)))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (<= v%.13.1_0 4352.0)
        (>= v%.13.1_0 4384.0)
        (not (<= 5.0 v%blastFlag.i.2_0)))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (not (<= 5.0 v%blastFlag.i.2_0))
        (<= v%.13.1_0 4368.0)
        (>= v%.13.1_0 4416.0))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (>= v%.13.1_0 4432.0)
        (not (<= 4416.0 v%.13.1_0))
        (not (>= v%blastFlag.i.2_0 5.0)))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (not (>= v%blastFlag.i.2_0 5.0))
        (>= v%.13.1_0 4464.0)
        (not (<= 4432.0 v%.13.1_0)))))
(assert (let ((a!1 (not (or (not true) (not (<= v%blastFlag.i.2_0 0.0))))))
(let ((a!2 (or (not v%_39_0)
               (<= v%.13.1_0 4560.0)
               a!1
               (<= (+ v%.13.1_0 (* (- 1.0) v%.32.9.2_0)) 0.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!2))))
(assert (let ((a!1 (or (<= (+ v%.32.9.2_0 (* (- 1.0) v%.13.1_0)) 0.0)
               (>= v%.13.1_0 4368.0)
               (<= v%.13.1_0 3.0)
               (>= v%.32.9.2_0 4560.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!1)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (not v%_39_0)
        (>= v%.13.1_0 4368.0)
        (<= v%.13.1_0 3.0)
        (>= v%.32.9.2_0 4560.0)
        (not (<= 5.0 v%blastFlag.i.2_0)))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (<= v%storemerge_0 4560.0)))
(assert (let ((a!1 (or (<= (+ v%.13.1_0 (* (- 1.0) v%storemerge_0)) 0.0)
               (>= v%.13.1_0 4416.0)
               (<= v%.13.1_0 4384.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!1)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or v%_39_0 (>= v%storemerge2_0 4512.0))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (>= v%storemerge_0 4368.0)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (not (<= 4432.0 v%.32.9.2_0))
        (not (<= 4432.0 v%storemerge2_0))
        (<= v%.13.1_0 4384.0)
        (>= v%.13.1_0 4432.0))))
(assert (let ((a!1 (or (not (<= 4560.0 v%.32.9.2_0))
               (<= (+ v%storemerge2_0 (* (- 1.0) v%.13.1_0)) 0.0)
               (<= v%.13.1_0 4384.0)
               (<= (+ v%storemerge1_0 (* (- 1.0) v%.13.1_0)) 0.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!1)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (<= v%storemerge1_0 4480.0)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (>= v%.13.2_0 4496.0)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (>= v%storemerge1_0 4464.0)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or v%_39_0 (>= v%storemerge_0 4560.0))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (not v%_39_0) (<= v%storemerge_0 4400.0))))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (not v%_39_0) (<= v%storemerge2_0 3.0))))
(assert (let ((a!1 (or (<= v%.13.1_0 4384.0)
               (>= v%.13.1_0 4416.0)
               (<= (+ v%storemerge2_0 (* (- 1.0) v%.13.1_0)) 0.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!1)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (or (not (>= v%storemerge2_0 4432.0)) (<= v%blastFlag.i.2_0 2.0))))
(assert (let ((a!1 (or (>= v%.13.1_0 4432.0)
               (<= v%.13.1_0 4384.0)
               (<= (+ v%.13.1_0 (* (- 1.0) v%.32.9.2_0)) 0.0)
               (<= (+ v%storemerge2_0 (* (- 1.0) v%.13.1_0)) 0.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!1)))
(assert (let ((a!1 (or v%_39_0
               (<= (+ v%.32.9.2_0 (* (- 1.0) v%.13.1_0)) 0.0)
               (>= v%.13.1_0 4368.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!1)))
(assert (=> (CP!NodeBlock115 v%_2_0
                     v%_3_0
                     v%_4_0
                     v%_5_0
                     v%_10_0
                     v%_31_0
                     v%_32_0
                     v%_34_0
                     v%.c_0
                     v%_35_0
                     v%_36_0
                     v%_37_0
                     v%_38_0
                     v%_39_0
                     v%storemerge_0
                     v%_40_0
                     v%storemerge1_0
                     v%.13.2_0
                     v%_43_0
                     v%_44_0
                     v%_45_0
                     v%_46_0
                     v%_47_0
                     v%storemerge2_0
                     v%blastFlag.i.2_0
                     v%.15.2_0
                     v%.13.1_0
                     v%.0.1_0
                     v%.32.9.2_0)
    (<= v%.13.2_0 4512.0)))
(assert (let ((a!1 (or (<= v%.13.1_0 3.0)
               (<= (+ v%.32.9.2_0 (* (- 1.0) v%.13.1_0)) 0.0)
               (>= v%.13.1_0 4352.0))))
  (=> (CP!NodeBlock115 v%_2_0
                       v%_3_0
                       v%_4_0
                       v%_5_0
                       v%_10_0
                       v%_31_0
                       v%_32_0
                       v%_34_0
                       v%.c_0
                       v%_35_0
                       v%_36_0
                       v%_37_0
                       v%_38_0
                       v%_39_0
                       v%storemerge_0
                       v%_40_0
                       v%storemerge1_0
                       v%.13.2_0
                       v%_43_0
                       v%_44_0
                       v%_45_0
                       v%_46_0
                       v%_47_0
                       v%storemerge2_0
                       v%blastFlag.i.2_0
                       v%.15.2_0
                       v%.13.1_0
                       v%.0.1_0
                       v%.32.9.2_0)
      a!1)))
(assert (=> (CP!__UFO__0 v%_2_0
                 v%_3_0
                 v%_4_0
                 v%_5_0
                 v%_10_0
                 v%_20_0
                 v%_21_0
                 v%_22_0
                 v%_31_0
                 v%_32_0
                 v%_34_0
                 v%.c_0
                 v%_35_0
                 v%_36_0
                 v%_37_0
                 v%_38_0
                 v%_39_0
                 v%storemerge_0
                 v%_40_0
                 v%storemerge1_0
                 v%.13.2_0
                 v%_43_0
                 v%_44_0
                 v%_45_0
                 v%_46_0
                 v%_47_0
                 v%storemerge2_0
                 v%blastFlag.i.2_0
                 v%.15.2_0
                 v%.13.1_0
                 v%.0.1_0
                 v%.32.9.2_0)
    true))
(check-sat)
