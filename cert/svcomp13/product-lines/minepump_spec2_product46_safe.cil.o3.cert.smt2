(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f99d7018490 () Bool)
(declare-fun F0x7f99d70184d0 () Bool)
(declare-fun F0x7f99d7018390 () Bool)
(declare-fun v0x7f99d7011d90_0 () Real)
(declare-fun v0x7f99d70158d0_0 () Bool)
(declare-fun v0x7f99d7013e50_0 () Bool)
(declare-fun v0x7f99d7013990_0 () Real)
(declare-fun v0x7f99d7012f90_0 () Real)
(declare-fun v0x7f99d7011f10_0 () Real)
(declare-fun v0x7f99d70159d0_0 () Real)
(declare-fun E0x7f99d7016290 () Bool)
(declare-fun v0x7f99d7015f10_0 () Real)
(declare-fun v0x7f99d7015e50_0 () Real)
(declare-fun v0x7f99d7015d90_0 () Bool)
(declare-fun v0x7f99d7016f10_0 () Bool)
(declare-fun v0x7f99d7015b10_0 () Bool)
(declare-fun v0x7f99d70153d0_0 () Real)
(declare-fun v0x7f99d7014cd0_0 () Bool)
(declare-fun E0x7f99d7017110 () Bool)
(declare-fun v0x7f99d7016dd0_0 () Bool)
(declare-fun E0x7f99d7015710 () Bool)
(declare-fun v0x7f99d7015650_0 () Bool)
(declare-fun E0x7f99d7014ed0 () Bool)
(declare-fun v0x7f99d7012010_0 () Real)
(declare-fun v0x7f99d70140d0_0 () Bool)
(declare-fun v0x7f99d7013b90_0 () Bool)
(declare-fun v0x7f99d7017310_0 () Bool)
(declare-fun v0x7f99d7012110_0 () Real)
(declare-fun v0x7f99d70132d0_0 () Real)
(declare-fun E0x7f99d7012b90 () Bool)
(declare-fun v0x7f99d7012810_0 () Bool)
(declare-fun v0x7f99d7012ad0_0 () Bool)
(declare-fun F0x7f99d7018210 () Bool)
(declare-fun v0x7f99d7014a50_0 () Bool)
(declare-fun E0x7f99d7014410 () Bool)
(declare-fun F0x7f99d7018150 () Bool)
(declare-fun E0x7f99d7013c50 () Bool)
(declare-fun E0x7f99d7015bd0 () Bool)
(declare-fun v0x7f99d7016cd0_0 () Bool)
(declare-fun v0x7f99d7010010_0 () Real)
(declare-fun v0x7f99d70150d0_0 () Bool)
(declare-fun E0x7f99d7013550 () Bool)
(declare-fun v0x7f99d70121d0_0 () Real)
(declare-fun v0x7f99d7017050_0 () Bool)
(declare-fun v0x7f99d7014910_0 () Bool)
(declare-fun v0x7f99d70120d0_0 () Real)
(declare-fun v0x7f99d70130d0_0 () Real)
(declare-fun v0x7f99d7014e10_0 () Bool)
(declare-fun E0x7f99d7016510 () Bool)
(declare-fun F0x7f99d70182d0 () Bool)
(declare-fun v0x7f99d7014190_0 () Real)
(declare-fun v0x7f99d70128d0_0 () Real)
(declare-fun v0x7f99d7013210_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f99d7012dd0_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f99d7013f90_0 () Real)
(declare-fun v0x7f99d7011fd0_0 () Real)
(declare-fun v0x7f99d7013a50_0 () Bool)
(declare-fun v0x7f99d7010110_0 () Bool)
(declare-fun v0x7f99d7015210_0 () Real)
(declare-fun lemma!bb2.i.i29.i.i!0 () Bool)
(declare-fun E0x7f99d7014250 () Bool)
(declare-fun E0x7f99d7015fd0 () Bool)
(declare-fun v0x7f99d7012990_0 () Bool)
(declare-fun E0x7f99d7016710 () Bool)
(declare-fun E0x7f99d7014b10 () Bool)
(declare-fun E0x7f99d7013390 () Bool)
(declare-fun v0x7f99d7015510_0 () Bool)
(declare-fun post!bb2.i.i29.i.i!0 () Bool)
(declare-fun F0x7f99d7018090 () Bool)

(assert (=> F0x7f99d7018090
    (and v0x7f99d7010110_0
         (<= v0x7f99d7011fd0_0 0.0)
         (>= v0x7f99d7011fd0_0 0.0)
         (<= v0x7f99d70120d0_0 0.0)
         (>= v0x7f99d70120d0_0 0.0)
         (<= v0x7f99d70121d0_0 0.0)
         (>= v0x7f99d70121d0_0 0.0)
         (<= v0x7f99d7010010_0 1.0)
         (>= v0x7f99d7010010_0 1.0))))
(assert (=> F0x7f99d7018090 F0x7f99d7018150))
(assert (let ((a!1 (=> v0x7f99d7013210_0
               (or (and v0x7f99d7012ad0_0
                        E0x7f99d7013390
                        (<= v0x7f99d70132d0_0 v0x7f99d70130d0_0)
                        (>= v0x7f99d70132d0_0 v0x7f99d70130d0_0))
                   (and v0x7f99d7012810_0
                        E0x7f99d7013550
                        v0x7f99d7012990_0
                        (<= v0x7f99d70132d0_0 v0x7f99d7012110_0)
                        (>= v0x7f99d70132d0_0 v0x7f99d7012110_0)))))
      (a!2 (=> v0x7f99d7013210_0
               (or (and E0x7f99d7013390 (not E0x7f99d7013550))
                   (and E0x7f99d7013550 (not E0x7f99d7013390)))))
      (a!3 (=> v0x7f99d70140d0_0
               (or (and v0x7f99d7013b90_0
                        E0x7f99d7014250
                        (<= v0x7f99d7014190_0 v0x7f99d7013f90_0)
                        (>= v0x7f99d7014190_0 v0x7f99d7013f90_0))
                   (and v0x7f99d7013210_0
                        E0x7f99d7014410
                        v0x7f99d7013a50_0
                        (<= v0x7f99d7014190_0 v0x7f99d7012010_0)
                        (>= v0x7f99d7014190_0 v0x7f99d7012010_0)))))
      (a!4 (=> v0x7f99d70140d0_0
               (or (and E0x7f99d7014250 (not E0x7f99d7014410))
                   (and E0x7f99d7014410 (not E0x7f99d7014250)))))
      (a!5 (or (and v0x7f99d7015650_0
                    E0x7f99d7015fd0
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70132d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70132d0_0))
                    (<= v0x7f99d7015f10_0 v0x7f99d70159d0_0)
                    (>= v0x7f99d7015f10_0 v0x7f99d70159d0_0))
               (and v0x7f99d7014a50_0
                    E0x7f99d7016290
                    (not v0x7f99d7014cd0_0)
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70132d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70132d0_0))
                    (and (<= v0x7f99d7015f10_0 v0x7f99d7011f10_0)
                         (>= v0x7f99d7015f10_0 v0x7f99d7011f10_0)))
               (and v0x7f99d7015b10_0
                    E0x7f99d7016510
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70153d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70153d0_0))
                    (and (<= v0x7f99d7015f10_0 v0x7f99d7011f10_0)
                         (>= v0x7f99d7015f10_0 v0x7f99d7011f10_0)))
               (and v0x7f99d7014e10_0
                    E0x7f99d7016710
                    (not v0x7f99d7015510_0)
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70153d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70153d0_0))
                    (<= v0x7f99d7015f10_0 0.0)
                    (>= v0x7f99d7015f10_0 0.0))))
      (a!6 (=> v0x7f99d7015d90_0
               (or (and E0x7f99d7015fd0
                        (not E0x7f99d7016290)
                        (not E0x7f99d7016510)
                        (not E0x7f99d7016710))
                   (and E0x7f99d7016290
                        (not E0x7f99d7015fd0)
                        (not E0x7f99d7016510)
                        (not E0x7f99d7016710))
                   (and E0x7f99d7016510
                        (not E0x7f99d7015fd0)
                        (not E0x7f99d7016290)
                        (not E0x7f99d7016710))
                   (and E0x7f99d7016710
                        (not E0x7f99d7015fd0)
                        (not E0x7f99d7016290)
                        (not E0x7f99d7016510)))))
      (a!7 (or (and v0x7f99d7017050_0
                    v0x7f99d7017310_0
                    (<= v0x7f99d7011fd0_0 1.0)
                    (>= v0x7f99d7011fd0_0 1.0)
                    (and (<= v0x7f99d70120d0_0 v0x7f99d7015f10_0)
                         (>= v0x7f99d70120d0_0 v0x7f99d7015f10_0))
                    (and (<= v0x7f99d70121d0_0 v0x7f99d7014190_0)
                         (>= v0x7f99d70121d0_0 v0x7f99d7014190_0))
                    (and (<= v0x7f99d7010010_0 v0x7f99d7015e50_0)
                         (>= v0x7f99d7010010_0 v0x7f99d7015e50_0)))
               (and v0x7f99d7015d90_0
                    v0x7f99d7016f10_0
                    (<= v0x7f99d7011fd0_0 0.0)
                    (>= v0x7f99d7011fd0_0 0.0)
                    (and (<= v0x7f99d70120d0_0 v0x7f99d7015f10_0)
                         (>= v0x7f99d70120d0_0 v0x7f99d7015f10_0))
                    (and (<= v0x7f99d70121d0_0 v0x7f99d7014190_0)
                         (>= v0x7f99d70121d0_0 v0x7f99d7014190_0))
                    (and (<= v0x7f99d7010010_0 v0x7f99d7015e50_0)
                         (>= v0x7f99d7010010_0 v0x7f99d7015e50_0))))))
(let ((a!8 (and (=> v0x7f99d7012ad0_0
                    (and v0x7f99d7012810_0
                         E0x7f99d7012b90
                         (not v0x7f99d7012990_0)))
                (=> v0x7f99d7012ad0_0 E0x7f99d7012b90)
                a!1
                a!2
                (=> v0x7f99d7013b90_0
                    (and v0x7f99d7013210_0
                         E0x7f99d7013c50
                         (not v0x7f99d7013a50_0)))
                (=> v0x7f99d7013b90_0 E0x7f99d7013c50)
                a!3
                a!4
                (=> v0x7f99d7014a50_0
                    (and v0x7f99d70140d0_0 E0x7f99d7014b10 v0x7f99d7014910_0))
                (=> v0x7f99d7014a50_0 E0x7f99d7014b10)
                (=> v0x7f99d7014e10_0
                    (and v0x7f99d70140d0_0
                         E0x7f99d7014ed0
                         (not v0x7f99d7014910_0)))
                (=> v0x7f99d7014e10_0 E0x7f99d7014ed0)
                (=> v0x7f99d7015650_0
                    (and v0x7f99d7014a50_0 E0x7f99d7015710 v0x7f99d7014cd0_0))
                (=> v0x7f99d7015650_0 E0x7f99d7015710)
                (=> v0x7f99d7015b10_0
                    (and v0x7f99d7014e10_0 E0x7f99d7015bd0 v0x7f99d7015510_0))
                (=> v0x7f99d7015b10_0 E0x7f99d7015bd0)
                (=> v0x7f99d7015d90_0 a!5)
                a!6
                (=> v0x7f99d7017050_0
                    (and v0x7f99d7015d90_0
                         E0x7f99d7017110
                         (not v0x7f99d7016f10_0)))
                (=> v0x7f99d7017050_0 E0x7f99d7017110)
                a!7
                (= v0x7f99d7012990_0 (= v0x7f99d70128d0_0 0.0))
                (= v0x7f99d7012dd0_0 (< v0x7f99d7012110_0 2.0))
                (= v0x7f99d7012f90_0 (ite v0x7f99d7012dd0_0 1.0 0.0))
                (= v0x7f99d70130d0_0 (+ v0x7f99d7012f90_0 v0x7f99d7012110_0))
                (= v0x7f99d7013a50_0 (= v0x7f99d7013990_0 0.0))
                (= v0x7f99d7013e50_0 (= v0x7f99d7012010_0 0.0))
                (= v0x7f99d7013f90_0 (ite v0x7f99d7013e50_0 1.0 0.0))
                (= v0x7f99d7014910_0 (= v0x7f99d7011f10_0 0.0))
                (= v0x7f99d7014cd0_0 (> v0x7f99d70132d0_0 1.0))
                (= v0x7f99d70150d0_0 (> v0x7f99d70132d0_0 0.0))
                (= v0x7f99d7015210_0 (+ v0x7f99d70132d0_0 (- 1.0)))
                (= v0x7f99d70153d0_0
                   (ite v0x7f99d70150d0_0 v0x7f99d7015210_0 v0x7f99d70132d0_0))
                (= v0x7f99d7015510_0 (= v0x7f99d7014190_0 0.0))
                (= v0x7f99d70158d0_0 (= v0x7f99d7014190_0 0.0))
                (= v0x7f99d70159d0_0
                   (ite v0x7f99d70158d0_0 1.0 v0x7f99d7011f10_0))
                (= v0x7f99d7016cd0_0 (= v0x7f99d7014190_0 0.0))
                (= v0x7f99d7016dd0_0 (= v0x7f99d7015f10_0 0.0))
                (= v0x7f99d7016f10_0 (or v0x7f99d7016dd0_0 v0x7f99d7016cd0_0))
                (= v0x7f99d7017310_0 (= v0x7f99d7011d90_0 0.0)))))
  (=> F0x7f99d7018210 a!8))))
(assert (=> F0x7f99d7018210 F0x7f99d70182d0))
(assert (let ((a!1 (=> v0x7f99d7013210_0
               (or (and v0x7f99d7012ad0_0
                        E0x7f99d7013390
                        (<= v0x7f99d70132d0_0 v0x7f99d70130d0_0)
                        (>= v0x7f99d70132d0_0 v0x7f99d70130d0_0))
                   (and v0x7f99d7012810_0
                        E0x7f99d7013550
                        v0x7f99d7012990_0
                        (<= v0x7f99d70132d0_0 v0x7f99d7012110_0)
                        (>= v0x7f99d70132d0_0 v0x7f99d7012110_0)))))
      (a!2 (=> v0x7f99d7013210_0
               (or (and E0x7f99d7013390 (not E0x7f99d7013550))
                   (and E0x7f99d7013550 (not E0x7f99d7013390)))))
      (a!3 (=> v0x7f99d70140d0_0
               (or (and v0x7f99d7013b90_0
                        E0x7f99d7014250
                        (<= v0x7f99d7014190_0 v0x7f99d7013f90_0)
                        (>= v0x7f99d7014190_0 v0x7f99d7013f90_0))
                   (and v0x7f99d7013210_0
                        E0x7f99d7014410
                        v0x7f99d7013a50_0
                        (<= v0x7f99d7014190_0 v0x7f99d7012010_0)
                        (>= v0x7f99d7014190_0 v0x7f99d7012010_0)))))
      (a!4 (=> v0x7f99d70140d0_0
               (or (and E0x7f99d7014250 (not E0x7f99d7014410))
                   (and E0x7f99d7014410 (not E0x7f99d7014250)))))
      (a!5 (or (and v0x7f99d7015650_0
                    E0x7f99d7015fd0
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70132d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70132d0_0))
                    (<= v0x7f99d7015f10_0 v0x7f99d70159d0_0)
                    (>= v0x7f99d7015f10_0 v0x7f99d70159d0_0))
               (and v0x7f99d7014a50_0
                    E0x7f99d7016290
                    (not v0x7f99d7014cd0_0)
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70132d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70132d0_0))
                    (and (<= v0x7f99d7015f10_0 v0x7f99d7011f10_0)
                         (>= v0x7f99d7015f10_0 v0x7f99d7011f10_0)))
               (and v0x7f99d7015b10_0
                    E0x7f99d7016510
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70153d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70153d0_0))
                    (and (<= v0x7f99d7015f10_0 v0x7f99d7011f10_0)
                         (>= v0x7f99d7015f10_0 v0x7f99d7011f10_0)))
               (and v0x7f99d7014e10_0
                    E0x7f99d7016710
                    (not v0x7f99d7015510_0)
                    (and (<= v0x7f99d7015e50_0 v0x7f99d70153d0_0)
                         (>= v0x7f99d7015e50_0 v0x7f99d70153d0_0))
                    (<= v0x7f99d7015f10_0 0.0)
                    (>= v0x7f99d7015f10_0 0.0))))
      (a!6 (=> v0x7f99d7015d90_0
               (or (and E0x7f99d7015fd0
                        (not E0x7f99d7016290)
                        (not E0x7f99d7016510)
                        (not E0x7f99d7016710))
                   (and E0x7f99d7016290
                        (not E0x7f99d7015fd0)
                        (not E0x7f99d7016510)
                        (not E0x7f99d7016710))
                   (and E0x7f99d7016510
                        (not E0x7f99d7015fd0)
                        (not E0x7f99d7016290)
                        (not E0x7f99d7016710))
                   (and E0x7f99d7016710
                        (not E0x7f99d7015fd0)
                        (not E0x7f99d7016290)
                        (not E0x7f99d7016510))))))
(let ((a!7 (and (=> v0x7f99d7012ad0_0
                    (and v0x7f99d7012810_0
                         E0x7f99d7012b90
                         (not v0x7f99d7012990_0)))
                (=> v0x7f99d7012ad0_0 E0x7f99d7012b90)
                a!1
                a!2
                (=> v0x7f99d7013b90_0
                    (and v0x7f99d7013210_0
                         E0x7f99d7013c50
                         (not v0x7f99d7013a50_0)))
                (=> v0x7f99d7013b90_0 E0x7f99d7013c50)
                a!3
                a!4
                (=> v0x7f99d7014a50_0
                    (and v0x7f99d70140d0_0 E0x7f99d7014b10 v0x7f99d7014910_0))
                (=> v0x7f99d7014a50_0 E0x7f99d7014b10)
                (=> v0x7f99d7014e10_0
                    (and v0x7f99d70140d0_0
                         E0x7f99d7014ed0
                         (not v0x7f99d7014910_0)))
                (=> v0x7f99d7014e10_0 E0x7f99d7014ed0)
                (=> v0x7f99d7015650_0
                    (and v0x7f99d7014a50_0 E0x7f99d7015710 v0x7f99d7014cd0_0))
                (=> v0x7f99d7015650_0 E0x7f99d7015710)
                (=> v0x7f99d7015b10_0
                    (and v0x7f99d7014e10_0 E0x7f99d7015bd0 v0x7f99d7015510_0))
                (=> v0x7f99d7015b10_0 E0x7f99d7015bd0)
                (=> v0x7f99d7015d90_0 a!5)
                a!6
                (=> v0x7f99d7017050_0
                    (and v0x7f99d7015d90_0
                         E0x7f99d7017110
                         (not v0x7f99d7016f10_0)))
                (=> v0x7f99d7017050_0 E0x7f99d7017110)
                v0x7f99d7017050_0
                (not v0x7f99d7017310_0)
                (= v0x7f99d7012990_0 (= v0x7f99d70128d0_0 0.0))
                (= v0x7f99d7012dd0_0 (< v0x7f99d7012110_0 2.0))
                (= v0x7f99d7012f90_0 (ite v0x7f99d7012dd0_0 1.0 0.0))
                (= v0x7f99d70130d0_0 (+ v0x7f99d7012f90_0 v0x7f99d7012110_0))
                (= v0x7f99d7013a50_0 (= v0x7f99d7013990_0 0.0))
                (= v0x7f99d7013e50_0 (= v0x7f99d7012010_0 0.0))
                (= v0x7f99d7013f90_0 (ite v0x7f99d7013e50_0 1.0 0.0))
                (= v0x7f99d7014910_0 (= v0x7f99d7011f10_0 0.0))
                (= v0x7f99d7014cd0_0 (> v0x7f99d70132d0_0 1.0))
                (= v0x7f99d70150d0_0 (> v0x7f99d70132d0_0 0.0))
                (= v0x7f99d7015210_0 (+ v0x7f99d70132d0_0 (- 1.0)))
                (= v0x7f99d70153d0_0
                   (ite v0x7f99d70150d0_0 v0x7f99d7015210_0 v0x7f99d70132d0_0))
                (= v0x7f99d7015510_0 (= v0x7f99d7014190_0 0.0))
                (= v0x7f99d70158d0_0 (= v0x7f99d7014190_0 0.0))
                (= v0x7f99d70159d0_0
                   (ite v0x7f99d70158d0_0 1.0 v0x7f99d7011f10_0))
                (= v0x7f99d7016cd0_0 (= v0x7f99d7014190_0 0.0))
                (= v0x7f99d7016dd0_0 (= v0x7f99d7015f10_0 0.0))
                (= v0x7f99d7016f10_0 (or v0x7f99d7016dd0_0 v0x7f99d7016cd0_0))
                (= v0x7f99d7017310_0 (= v0x7f99d7011d90_0 0.0)))))
  (=> F0x7f99d7018390 a!7))))
(assert (=> F0x7f99d7018390 F0x7f99d70182d0))
(assert (=> F0x7f99d70184d0 (or F0x7f99d7018090 F0x7f99d7018210)))
(assert (=> F0x7f99d7018490 F0x7f99d7018390))
(assert (=> pre!entry!0 (=> F0x7f99d7018150 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f99d70182d0 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f99d70184d0 true)))
(assert (= lemma!bb2.i.i29.i.i!0 (=> F0x7f99d7018490 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i29.i.i!0) (not lemma!bb2.i.i29.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f99d7018150)
; (error: F0x7f99d7018490)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i29.i.i!0)
