(declare-fun post!bb2.i.i25.i.i!0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i25.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun F0x7f74ad5755d0 () Bool)
(declare-fun v0x7f74ad574450_0 () Bool)
(declare-fun v0x7f74ad574310_0 () Bool)
(declare-fun v0x7f74ad571910_0 () Bool)
(declare-fun v0x7f74ad571450_0 () Real)
(declare-fun E0x7f74ad573c90 () Bool)
(declare-fun v0x7f74ad56fa90_0 () Real)
(declare-fun E0x7f74ad5739d0 () Bool)
(declare-fun v0x7f74ad573650_0 () Real)
(declare-fun v0x7f74ad572cd0_0 () Bool)
(declare-fun v0x7f74ad5734d0_0 () Bool)
(declare-fun v0x7f74ad573110_0 () Bool)
(declare-fun v0x7f74ad574590_0 () Bool)
(declare-fun E0x7f74ad573310 () Bool)
(declare-fun E0x7f74ad572ad0 () Bool)
(declare-fun v0x7f74ad5741d0_0 () Bool)
(declare-fun F0x7f74ad5752d0 () Bool)
(declare-fun v0x7f74ad572a10_0 () Bool)
(declare-fun E0x7f74ad5725d0 () Bool)
(declare-fun v0x7f74ad56fc10_0 () Real)
(declare-fun v0x7f74ad573250_0 () Bool)
(declare-fun v0x7f74ad571a50_0 () Real)
(declare-fun v0x7f74ad571510_0 () Bool)
(declare-fun v0x7f74ad571650_0 () Bool)
(declare-fun v0x7f74ad5728d0_0 () Real)
(declare-fun E0x7f74ad573710 () Bool)
(declare-fun v0x7f74ad56fd10_0 () Real)
(declare-fun E0x7f74ad571010 () Bool)
(declare-fun v0x7f74ad570890_0 () Bool)
(declare-fun v0x7f74ad5746d0_0 () Bool)
(declare-fun v0x7f74ad570d90_0 () Real)
(declare-fun v0x7f74ad5723d0_0 () Bool)
(declare-fun F0x7f74ad575610 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun v0x7f74ad570450_0 () Bool)
(declare-fun v0x7f74ad5702d0_0 () Bool)
(declare-fun E0x7f74ad571710 () Bool)
(declare-fun F0x7f74ad575290 () Bool)
(declare-fun E0x7f74ad570650 () Bool)
(declare-fun v0x7f74ad570590_0 () Bool)
(declare-fun v0x7f74ad570390_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun v0x7f74ad56e010_0 () Real)
(declare-fun E0x7f74ad571ed0 () Bool)
(declare-fun v0x7f74ad571c50_0 () Real)
(declare-fun v0x7f74ad572790_0 () Bool)
(declare-fun F0x7f74ad575510 () Bool)
(declare-fun F0x7f74ad575390 () Bool)
(declare-fun v0x7f74ad570b90_0 () Real)
(declare-fun v0x7f74ad56fdd0_0 () Real)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun v0x7f74ad572510_0 () Bool)
(declare-fun E0x7f74ad570e50 () Bool)
(declare-fun E0x7f74ad571d10 () Bool)
(declare-fun v0x7f74ad56fcd0_0 () Real)
(declare-fun v0x7f74ad572e10_0 () Real)
(declare-fun v0x7f74ad570cd0_0 () Bool)
(declare-fun v0x7f74ad570a50_0 () Real)
(declare-fun v0x7f74ad56e110_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f74ad573590_0 () Real)
(declare-fun v0x7f74ad571b90_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun F0x7f74ad5754d0 () Bool)
(declare-fun v0x7f74ad572fd0_0 () Real)

(assert (=> F0x7f74ad5754d0
    (and v0x7f74ad56e110_0
         (<= v0x7f74ad56fcd0_0 0.0)
         (>= v0x7f74ad56fcd0_0 0.0)
         (<= v0x7f74ad56fdd0_0 0.0)
         (>= v0x7f74ad56fdd0_0 0.0)
         (<= v0x7f74ad56e010_0 1.0)
         (>= v0x7f74ad56e010_0 1.0))))
(assert (=> F0x7f74ad5754d0 F0x7f74ad575290))
(assert (let ((a!1 (=> v0x7f74ad570cd0_0
               (or (and v0x7f74ad570590_0
                        E0x7f74ad570e50
                        (<= v0x7f74ad570d90_0 v0x7f74ad570b90_0)
                        (>= v0x7f74ad570d90_0 v0x7f74ad570b90_0))
                   (and v0x7f74ad5702d0_0
                        E0x7f74ad571010
                        v0x7f74ad570450_0
                        (<= v0x7f74ad570d90_0 v0x7f74ad56fd10_0)
                        (>= v0x7f74ad570d90_0 v0x7f74ad56fd10_0)))))
      (a!2 (=> v0x7f74ad570cd0_0
               (or (and E0x7f74ad570e50 (not E0x7f74ad571010))
                   (and E0x7f74ad571010 (not E0x7f74ad570e50)))))
      (a!3 (=> v0x7f74ad571b90_0
               (or (and v0x7f74ad571650_0
                        E0x7f74ad571d10
                        (<= v0x7f74ad571c50_0 v0x7f74ad571a50_0)
                        (>= v0x7f74ad571c50_0 v0x7f74ad571a50_0))
                   (and v0x7f74ad570cd0_0
                        E0x7f74ad571ed0
                        v0x7f74ad571510_0
                        (<= v0x7f74ad571c50_0 v0x7f74ad56fc10_0)
                        (>= v0x7f74ad571c50_0 v0x7f74ad56fc10_0)))))
      (a!4 (=> v0x7f74ad571b90_0
               (or (and E0x7f74ad571d10 (not E0x7f74ad571ed0))
                   (and E0x7f74ad571ed0 (not E0x7f74ad571d10)))))
      (a!5 (or (and v0x7f74ad572510_0
                    E0x7f74ad573710
                    (<= v0x7f74ad573590_0 v0x7f74ad570d90_0)
                    (>= v0x7f74ad573590_0 v0x7f74ad570d90_0)
                    (<= v0x7f74ad573650_0 v0x7f74ad5728d0_0)
                    (>= v0x7f74ad573650_0 v0x7f74ad5728d0_0))
               (and v0x7f74ad573250_0
                    E0x7f74ad5739d0
                    (and (<= v0x7f74ad573590_0 v0x7f74ad572fd0_0)
                         (>= v0x7f74ad573590_0 v0x7f74ad572fd0_0))
                    (<= v0x7f74ad573650_0 v0x7f74ad56fa90_0)
                    (>= v0x7f74ad573650_0 v0x7f74ad56fa90_0))
               (and v0x7f74ad572a10_0
                    E0x7f74ad573c90
                    (not v0x7f74ad573110_0)
                    (and (<= v0x7f74ad573590_0 v0x7f74ad572fd0_0)
                         (>= v0x7f74ad573590_0 v0x7f74ad572fd0_0))
                    (<= v0x7f74ad573650_0 0.0)
                    (>= v0x7f74ad573650_0 0.0))))
      (a!6 (=> v0x7f74ad5734d0_0
               (or (and E0x7f74ad573710
                        (not E0x7f74ad5739d0)
                        (not E0x7f74ad573c90))
                   (and E0x7f74ad5739d0
                        (not E0x7f74ad573710)
                        (not E0x7f74ad573c90))
                   (and E0x7f74ad573c90
                        (not E0x7f74ad573710)
                        (not E0x7f74ad5739d0))))))
(let ((a!7 (and (=> v0x7f74ad570590_0
                    (and v0x7f74ad5702d0_0
                         E0x7f74ad570650
                         (not v0x7f74ad570450_0)))
                (=> v0x7f74ad570590_0 E0x7f74ad570650)
                a!1
                a!2
                (=> v0x7f74ad571650_0
                    (and v0x7f74ad570cd0_0
                         E0x7f74ad571710
                         (not v0x7f74ad571510_0)))
                (=> v0x7f74ad571650_0 E0x7f74ad571710)
                a!3
                a!4
                (=> v0x7f74ad572510_0
                    (and v0x7f74ad571b90_0 E0x7f74ad5725d0 v0x7f74ad5723d0_0))
                (=> v0x7f74ad572510_0 E0x7f74ad5725d0)
                (=> v0x7f74ad572a10_0
                    (and v0x7f74ad571b90_0
                         E0x7f74ad572ad0
                         (not v0x7f74ad5723d0_0)))
                (=> v0x7f74ad572a10_0 E0x7f74ad572ad0)
                (=> v0x7f74ad573250_0
                    (and v0x7f74ad572a10_0 E0x7f74ad573310 v0x7f74ad573110_0))
                (=> v0x7f74ad573250_0 E0x7f74ad573310)
                (=> v0x7f74ad5734d0_0 a!5)
                a!6
                v0x7f74ad5734d0_0
                (not v0x7f74ad5746d0_0)
                (<= v0x7f74ad56fcd0_0 v0x7f74ad573650_0)
                (>= v0x7f74ad56fcd0_0 v0x7f74ad573650_0)
                (<= v0x7f74ad56fdd0_0 v0x7f74ad571c50_0)
                (>= v0x7f74ad56fdd0_0 v0x7f74ad571c50_0)
                (<= v0x7f74ad56e010_0 v0x7f74ad573590_0)
                (>= v0x7f74ad56e010_0 v0x7f74ad573590_0)
                (= v0x7f74ad570450_0 (= v0x7f74ad570390_0 0.0))
                (= v0x7f74ad570890_0 (< v0x7f74ad56fd10_0 2.0))
                (= v0x7f74ad570a50_0 (ite v0x7f74ad570890_0 1.0 0.0))
                (= v0x7f74ad570b90_0 (+ v0x7f74ad570a50_0 v0x7f74ad56fd10_0))
                (= v0x7f74ad571510_0 (= v0x7f74ad571450_0 0.0))
                (= v0x7f74ad571910_0 (= v0x7f74ad56fc10_0 0.0))
                (= v0x7f74ad571a50_0 (ite v0x7f74ad571910_0 1.0 0.0))
                (= v0x7f74ad5723d0_0 (= v0x7f74ad56fa90_0 0.0))
                (= v0x7f74ad572790_0 (> v0x7f74ad570d90_0 1.0))
                (= v0x7f74ad5728d0_0
                   (ite v0x7f74ad572790_0 1.0 v0x7f74ad56fa90_0))
                (= v0x7f74ad572cd0_0 (> v0x7f74ad570d90_0 0.0))
                (= v0x7f74ad572e10_0 (+ v0x7f74ad570d90_0 (- 1.0)))
                (= v0x7f74ad572fd0_0
                   (ite v0x7f74ad572cd0_0 v0x7f74ad572e10_0 v0x7f74ad570d90_0))
                (= v0x7f74ad573110_0 (= v0x7f74ad571c50_0 0.0))
                (= v0x7f74ad5741d0_0 (= v0x7f74ad573590_0 2.0))
                (= v0x7f74ad574310_0 (= v0x7f74ad573650_0 0.0))
                (= v0x7f74ad574450_0 (or v0x7f74ad574310_0 v0x7f74ad5741d0_0))
                (= v0x7f74ad574590_0 (xor v0x7f74ad574450_0 true))
                (= v0x7f74ad5746d0_0 (and v0x7f74ad5723d0_0 v0x7f74ad574590_0)))))
  (=> F0x7f74ad575390 a!7))))
(assert (=> F0x7f74ad575390 F0x7f74ad5752d0))
(assert (let ((a!1 (=> v0x7f74ad570cd0_0
               (or (and v0x7f74ad570590_0
                        E0x7f74ad570e50
                        (<= v0x7f74ad570d90_0 v0x7f74ad570b90_0)
                        (>= v0x7f74ad570d90_0 v0x7f74ad570b90_0))
                   (and v0x7f74ad5702d0_0
                        E0x7f74ad571010
                        v0x7f74ad570450_0
                        (<= v0x7f74ad570d90_0 v0x7f74ad56fd10_0)
                        (>= v0x7f74ad570d90_0 v0x7f74ad56fd10_0)))))
      (a!2 (=> v0x7f74ad570cd0_0
               (or (and E0x7f74ad570e50 (not E0x7f74ad571010))
                   (and E0x7f74ad571010 (not E0x7f74ad570e50)))))
      (a!3 (=> v0x7f74ad571b90_0
               (or (and v0x7f74ad571650_0
                        E0x7f74ad571d10
                        (<= v0x7f74ad571c50_0 v0x7f74ad571a50_0)
                        (>= v0x7f74ad571c50_0 v0x7f74ad571a50_0))
                   (and v0x7f74ad570cd0_0
                        E0x7f74ad571ed0
                        v0x7f74ad571510_0
                        (<= v0x7f74ad571c50_0 v0x7f74ad56fc10_0)
                        (>= v0x7f74ad571c50_0 v0x7f74ad56fc10_0)))))
      (a!4 (=> v0x7f74ad571b90_0
               (or (and E0x7f74ad571d10 (not E0x7f74ad571ed0))
                   (and E0x7f74ad571ed0 (not E0x7f74ad571d10)))))
      (a!5 (or (and v0x7f74ad572510_0
                    E0x7f74ad573710
                    (<= v0x7f74ad573590_0 v0x7f74ad570d90_0)
                    (>= v0x7f74ad573590_0 v0x7f74ad570d90_0)
                    (<= v0x7f74ad573650_0 v0x7f74ad5728d0_0)
                    (>= v0x7f74ad573650_0 v0x7f74ad5728d0_0))
               (and v0x7f74ad573250_0
                    E0x7f74ad5739d0
                    (and (<= v0x7f74ad573590_0 v0x7f74ad572fd0_0)
                         (>= v0x7f74ad573590_0 v0x7f74ad572fd0_0))
                    (<= v0x7f74ad573650_0 v0x7f74ad56fa90_0)
                    (>= v0x7f74ad573650_0 v0x7f74ad56fa90_0))
               (and v0x7f74ad572a10_0
                    E0x7f74ad573c90
                    (not v0x7f74ad573110_0)
                    (and (<= v0x7f74ad573590_0 v0x7f74ad572fd0_0)
                         (>= v0x7f74ad573590_0 v0x7f74ad572fd0_0))
                    (<= v0x7f74ad573650_0 0.0)
                    (>= v0x7f74ad573650_0 0.0))))
      (a!6 (=> v0x7f74ad5734d0_0
               (or (and E0x7f74ad573710
                        (not E0x7f74ad5739d0)
                        (not E0x7f74ad573c90))
                   (and E0x7f74ad5739d0
                        (not E0x7f74ad573710)
                        (not E0x7f74ad573c90))
                   (and E0x7f74ad573c90
                        (not E0x7f74ad573710)
                        (not E0x7f74ad5739d0))))))
(let ((a!7 (and (=> v0x7f74ad570590_0
                    (and v0x7f74ad5702d0_0
                         E0x7f74ad570650
                         (not v0x7f74ad570450_0)))
                (=> v0x7f74ad570590_0 E0x7f74ad570650)
                a!1
                a!2
                (=> v0x7f74ad571650_0
                    (and v0x7f74ad570cd0_0
                         E0x7f74ad571710
                         (not v0x7f74ad571510_0)))
                (=> v0x7f74ad571650_0 E0x7f74ad571710)
                a!3
                a!4
                (=> v0x7f74ad572510_0
                    (and v0x7f74ad571b90_0 E0x7f74ad5725d0 v0x7f74ad5723d0_0))
                (=> v0x7f74ad572510_0 E0x7f74ad5725d0)
                (=> v0x7f74ad572a10_0
                    (and v0x7f74ad571b90_0
                         E0x7f74ad572ad0
                         (not v0x7f74ad5723d0_0)))
                (=> v0x7f74ad572a10_0 E0x7f74ad572ad0)
                (=> v0x7f74ad573250_0
                    (and v0x7f74ad572a10_0 E0x7f74ad573310 v0x7f74ad573110_0))
                (=> v0x7f74ad573250_0 E0x7f74ad573310)
                (=> v0x7f74ad5734d0_0 a!5)
                a!6
                v0x7f74ad5734d0_0
                v0x7f74ad5746d0_0
                (= v0x7f74ad570450_0 (= v0x7f74ad570390_0 0.0))
                (= v0x7f74ad570890_0 (< v0x7f74ad56fd10_0 2.0))
                (= v0x7f74ad570a50_0 (ite v0x7f74ad570890_0 1.0 0.0))
                (= v0x7f74ad570b90_0 (+ v0x7f74ad570a50_0 v0x7f74ad56fd10_0))
                (= v0x7f74ad571510_0 (= v0x7f74ad571450_0 0.0))
                (= v0x7f74ad571910_0 (= v0x7f74ad56fc10_0 0.0))
                (= v0x7f74ad571a50_0 (ite v0x7f74ad571910_0 1.0 0.0))
                (= v0x7f74ad5723d0_0 (= v0x7f74ad56fa90_0 0.0))
                (= v0x7f74ad572790_0 (> v0x7f74ad570d90_0 1.0))
                (= v0x7f74ad5728d0_0
                   (ite v0x7f74ad572790_0 1.0 v0x7f74ad56fa90_0))
                (= v0x7f74ad572cd0_0 (> v0x7f74ad570d90_0 0.0))
                (= v0x7f74ad572e10_0 (+ v0x7f74ad570d90_0 (- 1.0)))
                (= v0x7f74ad572fd0_0
                   (ite v0x7f74ad572cd0_0 v0x7f74ad572e10_0 v0x7f74ad570d90_0))
                (= v0x7f74ad573110_0 (= v0x7f74ad571c50_0 0.0))
                (= v0x7f74ad5741d0_0 (= v0x7f74ad573590_0 2.0))
                (= v0x7f74ad574310_0 (= v0x7f74ad573650_0 0.0))
                (= v0x7f74ad574450_0 (or v0x7f74ad574310_0 v0x7f74ad5741d0_0))
                (= v0x7f74ad574590_0 (xor v0x7f74ad574450_0 true))
                (= v0x7f74ad5746d0_0 (and v0x7f74ad5723d0_0 v0x7f74ad574590_0)))))
  (=> F0x7f74ad575510 a!7))))
(assert (=> F0x7f74ad575510 F0x7f74ad5752d0))
(assert (=> F0x7f74ad575610 (or F0x7f74ad5754d0 F0x7f74ad575390)))
(assert (=> F0x7f74ad5755d0 F0x7f74ad575510))
(assert (=> pre!entry!0 (=> F0x7f74ad575290 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f74ad5752d0 (>= v0x7f74ad56fc10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f74ad5752d0 (>= v0x7f74ad56fd10_0 0.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f74ad5752d0
        (or (<= v0x7f74ad56fd10_0 1.0) (>= v0x7f74ad56fd10_0 2.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f74ad5752d0 (<= v0x7f74ad56fd10_0 2.0))))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f74ad5752d0
        (or (<= v0x7f74ad56fd10_0 0.0) (>= v0x7f74ad56fd10_0 1.0)))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7f74ad5752d0
        (or (<= v0x7f74ad56fd10_0 1.0) (>= v0x7f74ad56fa90_0 1.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f74ad575610 (>= v0x7f74ad56fdd0_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f74ad575610 (>= v0x7f74ad56e010_0 0.0))))
(assert (= lemma!bb1.i.i!2
   (=> F0x7f74ad575610
       (or (<= v0x7f74ad56e010_0 1.0) (>= v0x7f74ad56e010_0 2.0)))))
(assert (= lemma!bb1.i.i!3 (=> F0x7f74ad575610 (<= v0x7f74ad56e010_0 2.0))))
(assert (= lemma!bb1.i.i!4
   (=> F0x7f74ad575610
       (or (<= v0x7f74ad56e010_0 0.0) (>= v0x7f74ad56e010_0 1.0)))))
(assert (= lemma!bb1.i.i!5
   (=> F0x7f74ad575610
       (or (<= v0x7f74ad56e010_0 1.0) (>= v0x7f74ad56fcd0_0 1.0)))))
(assert (= lemma!bb2.i.i25.i.i!0 (=> F0x7f74ad5755d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb2.i.i25.i.i!0) (not lemma!bb2.i.i25.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
; (init: F0x7f74ad575290)
; (error: F0x7f74ad5755d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i25.i.i!0)
