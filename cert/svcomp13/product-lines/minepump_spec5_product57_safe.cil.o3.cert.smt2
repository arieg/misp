(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7fc03a85a090 () Bool)
(declare-fun F0x7fc03a85a050 () Bool)
(declare-fun v0x7fc03a857150_0 () Real)
(declare-fun v0x7fc03a857010_0 () Bool)
(declare-fun v0x7fc03a855d90_0 () Bool)
(declare-fun v0x7fc03a8558d0_0 () Real)
(declare-fun E0x7fc03a858490 () Bool)
(declare-fun E0x7fc03a858210 () Bool)
(declare-fun v0x7fc03a857e90_0 () Real)
(declare-fun v0x7fc03a854ed0_0 () Real)
(declare-fun lemma!bb2.i.i36.i.i!0 () Bool)
(declare-fun v0x7fc03a857dd0_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fc03a857310_0 () Real)
(declare-fun v0x7fc03a859010_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fc03a857d10_0 () Bool)
(declare-fun v0x7fc03a858ed0_0 () Bool)
(declare-fun v0x7fc03a857450_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7fc03a857b50 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7fc03a854d10_0 () Bool)
(declare-fun v0x7fc03a856c10_0 () Bool)
(declare-fun E0x7fc03a857650 () Bool)
(declare-fun v0x7fc03a853f10_0 () Real)
(declare-fun v0x7fc03a857a90_0 () Bool)
(declare-fun E0x7fc03a856e10 () Bool)
(declare-fun v0x7fc03a857590_0 () Bool)
(declare-fun F0x7fc03a859f90 () Bool)
(declare-fun v0x7fc03a856d50_0 () Bool)
(declare-fun E0x7fc03a856a50 () Bool)
(declare-fun v0x7fc03a856990_0 () Bool)
(declare-fun v0x7fc03a8560d0_0 () Real)
(declare-fun E0x7fc03a856350 () Bool)
(declare-fun E0x7fc03a856190 () Bool)
(declare-fun v0x7fc03a858c50_0 () Bool)
(declare-fun v0x7fc03a856010_0 () Bool)
(declare-fun v0x7fc03a855990_0 () Bool)
(declare-fun E0x7fc03a855b90 () Bool)
(declare-fun v0x7fc03a856850_0 () Bool)
(declare-fun v0x7fc03a857810_0 () Bool)
(declare-fun v0x7fc03a855010_0 () Real)
(declare-fun v0x7fc03a857950_0 () Real)
(declare-fun post!bb2.i.i36.i.i!0 () Bool)
(declare-fun v0x7fc03a855210_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun E0x7fc03a855490 () Bool)
(declare-fun v0x7fc03a8548d0_0 () Bool)
(declare-fun v0x7fc03a854750_0 () Bool)
(declare-fun E0x7fc03a858690 () Bool)
(declare-fun v0x7fc03a854a10_0 () Bool)
(declare-fun F0x7fc03a859c90 () Bool)
(declare-fun v0x7fc03a854090_0 () Real)
(declare-fun E0x7fc03a857f50 () Bool)
(declare-fun v0x7fc03a854810_0 () Real)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun F0x7fc03a859d50 () Bool)
(declare-fun E0x7fc03a854ad0 () Bool)
(declare-fun F0x7fc03a859e10 () Bool)
(declare-fun v0x7fc03a852010_0 () Real)
(declare-fun v0x7fc03a859150_0 () Bool)
(declare-fun v0x7fc03a855ed0_0 () Real)
(declare-fun E0x7fc03a8552d0 () Bool)
(declare-fun v0x7fc03a855ad0_0 () Bool)
(declare-fun v0x7fc03a854150_0 () Real)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7fc03a854250_0 () Real)
(declare-fun v0x7fc03a858d90_0 () Bool)
(declare-fun v0x7fc03a852110_0 () Bool)
(declare-fun v0x7fc03a854190_0 () Real)
(declare-fun v0x7fc03a855150_0 () Bool)
(declare-fun F0x7fc03a859ed0 () Bool)

(assert (=> F0x7fc03a859ed0
    (and v0x7fc03a852110_0
         (<= v0x7fc03a854150_0 0.0)
         (>= v0x7fc03a854150_0 0.0)
         (<= v0x7fc03a854250_0 0.0)
         (>= v0x7fc03a854250_0 0.0)
         (<= v0x7fc03a852010_0 1.0)
         (>= v0x7fc03a852010_0 1.0))))
(assert (=> F0x7fc03a859ed0 F0x7fc03a859e10))
(assert (let ((a!1 (=> v0x7fc03a855150_0
               (or (and v0x7fc03a854a10_0
                        E0x7fc03a8552d0
                        (<= v0x7fc03a855210_0 v0x7fc03a855010_0)
                        (>= v0x7fc03a855210_0 v0x7fc03a855010_0))
                   (and v0x7fc03a854750_0
                        E0x7fc03a855490
                        v0x7fc03a8548d0_0
                        (<= v0x7fc03a855210_0 v0x7fc03a854190_0)
                        (>= v0x7fc03a855210_0 v0x7fc03a854190_0)))))
      (a!2 (=> v0x7fc03a855150_0
               (or (and E0x7fc03a8552d0 (not E0x7fc03a855490))
                   (and E0x7fc03a855490 (not E0x7fc03a8552d0)))))
      (a!3 (=> v0x7fc03a856010_0
               (or (and v0x7fc03a855ad0_0
                        E0x7fc03a856190
                        (<= v0x7fc03a8560d0_0 v0x7fc03a855ed0_0)
                        (>= v0x7fc03a8560d0_0 v0x7fc03a855ed0_0))
                   (and v0x7fc03a855150_0
                        E0x7fc03a856350
                        v0x7fc03a855990_0
                        (<= v0x7fc03a8560d0_0 v0x7fc03a854090_0)
                        (>= v0x7fc03a8560d0_0 v0x7fc03a854090_0)))))
      (a!4 (=> v0x7fc03a856010_0
               (or (and E0x7fc03a856190 (not E0x7fc03a856350))
                   (and E0x7fc03a856350 (not E0x7fc03a856190)))))
      (a!5 (or (and v0x7fc03a857590_0
                    E0x7fc03a857f50
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a855210_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a855210_0))
                    (<= v0x7fc03a857e90_0 v0x7fc03a857950_0)
                    (>= v0x7fc03a857e90_0 v0x7fc03a857950_0))
               (and v0x7fc03a856990_0
                    E0x7fc03a858210
                    (not v0x7fc03a856c10_0)
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a855210_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a855210_0))
                    (and (<= v0x7fc03a857e90_0 v0x7fc03a853f10_0)
                         (>= v0x7fc03a857e90_0 v0x7fc03a853f10_0)))
               (and v0x7fc03a857a90_0
                    E0x7fc03a858490
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a857310_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a857310_0))
                    (and (<= v0x7fc03a857e90_0 v0x7fc03a853f10_0)
                         (>= v0x7fc03a857e90_0 v0x7fc03a853f10_0)))
               (and v0x7fc03a856d50_0
                    E0x7fc03a858690
                    (not v0x7fc03a857450_0)
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a857310_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a857310_0))
                    (<= v0x7fc03a857e90_0 0.0)
                    (>= v0x7fc03a857e90_0 0.0))))
      (a!6 (=> v0x7fc03a857d10_0
               (or (and E0x7fc03a857f50
                        (not E0x7fc03a858210)
                        (not E0x7fc03a858490)
                        (not E0x7fc03a858690))
                   (and E0x7fc03a858210
                        (not E0x7fc03a857f50)
                        (not E0x7fc03a858490)
                        (not E0x7fc03a858690))
                   (and E0x7fc03a858490
                        (not E0x7fc03a857f50)
                        (not E0x7fc03a858210)
                        (not E0x7fc03a858690))
                   (and E0x7fc03a858690
                        (not E0x7fc03a857f50)
                        (not E0x7fc03a858210)
                        (not E0x7fc03a858490))))))
(let ((a!7 (and (=> v0x7fc03a854a10_0
                    (and v0x7fc03a854750_0
                         E0x7fc03a854ad0
                         (not v0x7fc03a8548d0_0)))
                (=> v0x7fc03a854a10_0 E0x7fc03a854ad0)
                a!1
                a!2
                (=> v0x7fc03a855ad0_0
                    (and v0x7fc03a855150_0
                         E0x7fc03a855b90
                         (not v0x7fc03a855990_0)))
                (=> v0x7fc03a855ad0_0 E0x7fc03a855b90)
                a!3
                a!4
                (=> v0x7fc03a856990_0
                    (and v0x7fc03a856010_0 E0x7fc03a856a50 v0x7fc03a856850_0))
                (=> v0x7fc03a856990_0 E0x7fc03a856a50)
                (=> v0x7fc03a856d50_0
                    (and v0x7fc03a856010_0
                         E0x7fc03a856e10
                         (not v0x7fc03a856850_0)))
                (=> v0x7fc03a856d50_0 E0x7fc03a856e10)
                (=> v0x7fc03a857590_0
                    (and v0x7fc03a856990_0 E0x7fc03a857650 v0x7fc03a856c10_0))
                (=> v0x7fc03a857590_0 E0x7fc03a857650)
                (=> v0x7fc03a857a90_0
                    (and v0x7fc03a856d50_0 E0x7fc03a857b50 v0x7fc03a857450_0))
                (=> v0x7fc03a857a90_0 E0x7fc03a857b50)
                (=> v0x7fc03a857d10_0 a!5)
                a!6
                v0x7fc03a857d10_0
                (not v0x7fc03a859150_0)
                (<= v0x7fc03a854150_0 v0x7fc03a857e90_0)
                (>= v0x7fc03a854150_0 v0x7fc03a857e90_0)
                (<= v0x7fc03a854250_0 v0x7fc03a8560d0_0)
                (>= v0x7fc03a854250_0 v0x7fc03a8560d0_0)
                (<= v0x7fc03a852010_0 v0x7fc03a857dd0_0)
                (>= v0x7fc03a852010_0 v0x7fc03a857dd0_0)
                (= v0x7fc03a8548d0_0 (= v0x7fc03a854810_0 0.0))
                (= v0x7fc03a854d10_0 (< v0x7fc03a854190_0 2.0))
                (= v0x7fc03a854ed0_0 (ite v0x7fc03a854d10_0 1.0 0.0))
                (= v0x7fc03a855010_0 (+ v0x7fc03a854ed0_0 v0x7fc03a854190_0))
                (= v0x7fc03a855990_0 (= v0x7fc03a8558d0_0 0.0))
                (= v0x7fc03a855d90_0 (= v0x7fc03a854090_0 0.0))
                (= v0x7fc03a855ed0_0 (ite v0x7fc03a855d90_0 1.0 0.0))
                (= v0x7fc03a856850_0 (= v0x7fc03a853f10_0 0.0))
                (= v0x7fc03a856c10_0 (> v0x7fc03a855210_0 1.0))
                (= v0x7fc03a857010_0 (> v0x7fc03a855210_0 0.0))
                (= v0x7fc03a857150_0 (+ v0x7fc03a855210_0 (- 1.0)))
                (= v0x7fc03a857310_0
                   (ite v0x7fc03a857010_0 v0x7fc03a857150_0 v0x7fc03a855210_0))
                (= v0x7fc03a857450_0 (= v0x7fc03a857310_0 0.0))
                (= v0x7fc03a857810_0 (= v0x7fc03a8560d0_0 0.0))
                (= v0x7fc03a857950_0
                   (ite v0x7fc03a857810_0 1.0 v0x7fc03a853f10_0))
                (= v0x7fc03a858c50_0 (= v0x7fc03a857dd0_0 2.0))
                (= v0x7fc03a858d90_0 (= v0x7fc03a857e90_0 0.0))
                (= v0x7fc03a858ed0_0 (or v0x7fc03a858d90_0 v0x7fc03a858c50_0))
                (= v0x7fc03a859010_0 (xor v0x7fc03a858ed0_0 true))
                (= v0x7fc03a859150_0 (and v0x7fc03a856850_0 v0x7fc03a859010_0)))))
  (=> F0x7fc03a859d50 a!7))))
(assert (=> F0x7fc03a859d50 F0x7fc03a859c90))
(assert (let ((a!1 (=> v0x7fc03a855150_0
               (or (and v0x7fc03a854a10_0
                        E0x7fc03a8552d0
                        (<= v0x7fc03a855210_0 v0x7fc03a855010_0)
                        (>= v0x7fc03a855210_0 v0x7fc03a855010_0))
                   (and v0x7fc03a854750_0
                        E0x7fc03a855490
                        v0x7fc03a8548d0_0
                        (<= v0x7fc03a855210_0 v0x7fc03a854190_0)
                        (>= v0x7fc03a855210_0 v0x7fc03a854190_0)))))
      (a!2 (=> v0x7fc03a855150_0
               (or (and E0x7fc03a8552d0 (not E0x7fc03a855490))
                   (and E0x7fc03a855490 (not E0x7fc03a8552d0)))))
      (a!3 (=> v0x7fc03a856010_0
               (or (and v0x7fc03a855ad0_0
                        E0x7fc03a856190
                        (<= v0x7fc03a8560d0_0 v0x7fc03a855ed0_0)
                        (>= v0x7fc03a8560d0_0 v0x7fc03a855ed0_0))
                   (and v0x7fc03a855150_0
                        E0x7fc03a856350
                        v0x7fc03a855990_0
                        (<= v0x7fc03a8560d0_0 v0x7fc03a854090_0)
                        (>= v0x7fc03a8560d0_0 v0x7fc03a854090_0)))))
      (a!4 (=> v0x7fc03a856010_0
               (or (and E0x7fc03a856190 (not E0x7fc03a856350))
                   (and E0x7fc03a856350 (not E0x7fc03a856190)))))
      (a!5 (or (and v0x7fc03a857590_0
                    E0x7fc03a857f50
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a855210_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a855210_0))
                    (<= v0x7fc03a857e90_0 v0x7fc03a857950_0)
                    (>= v0x7fc03a857e90_0 v0x7fc03a857950_0))
               (and v0x7fc03a856990_0
                    E0x7fc03a858210
                    (not v0x7fc03a856c10_0)
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a855210_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a855210_0))
                    (and (<= v0x7fc03a857e90_0 v0x7fc03a853f10_0)
                         (>= v0x7fc03a857e90_0 v0x7fc03a853f10_0)))
               (and v0x7fc03a857a90_0
                    E0x7fc03a858490
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a857310_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a857310_0))
                    (and (<= v0x7fc03a857e90_0 v0x7fc03a853f10_0)
                         (>= v0x7fc03a857e90_0 v0x7fc03a853f10_0)))
               (and v0x7fc03a856d50_0
                    E0x7fc03a858690
                    (not v0x7fc03a857450_0)
                    (and (<= v0x7fc03a857dd0_0 v0x7fc03a857310_0)
                         (>= v0x7fc03a857dd0_0 v0x7fc03a857310_0))
                    (<= v0x7fc03a857e90_0 0.0)
                    (>= v0x7fc03a857e90_0 0.0))))
      (a!6 (=> v0x7fc03a857d10_0
               (or (and E0x7fc03a857f50
                        (not E0x7fc03a858210)
                        (not E0x7fc03a858490)
                        (not E0x7fc03a858690))
                   (and E0x7fc03a858210
                        (not E0x7fc03a857f50)
                        (not E0x7fc03a858490)
                        (not E0x7fc03a858690))
                   (and E0x7fc03a858490
                        (not E0x7fc03a857f50)
                        (not E0x7fc03a858210)
                        (not E0x7fc03a858690))
                   (and E0x7fc03a858690
                        (not E0x7fc03a857f50)
                        (not E0x7fc03a858210)
                        (not E0x7fc03a858490))))))
(let ((a!7 (and (=> v0x7fc03a854a10_0
                    (and v0x7fc03a854750_0
                         E0x7fc03a854ad0
                         (not v0x7fc03a8548d0_0)))
                (=> v0x7fc03a854a10_0 E0x7fc03a854ad0)
                a!1
                a!2
                (=> v0x7fc03a855ad0_0
                    (and v0x7fc03a855150_0
                         E0x7fc03a855b90
                         (not v0x7fc03a855990_0)))
                (=> v0x7fc03a855ad0_0 E0x7fc03a855b90)
                a!3
                a!4
                (=> v0x7fc03a856990_0
                    (and v0x7fc03a856010_0 E0x7fc03a856a50 v0x7fc03a856850_0))
                (=> v0x7fc03a856990_0 E0x7fc03a856a50)
                (=> v0x7fc03a856d50_0
                    (and v0x7fc03a856010_0
                         E0x7fc03a856e10
                         (not v0x7fc03a856850_0)))
                (=> v0x7fc03a856d50_0 E0x7fc03a856e10)
                (=> v0x7fc03a857590_0
                    (and v0x7fc03a856990_0 E0x7fc03a857650 v0x7fc03a856c10_0))
                (=> v0x7fc03a857590_0 E0x7fc03a857650)
                (=> v0x7fc03a857a90_0
                    (and v0x7fc03a856d50_0 E0x7fc03a857b50 v0x7fc03a857450_0))
                (=> v0x7fc03a857a90_0 E0x7fc03a857b50)
                (=> v0x7fc03a857d10_0 a!5)
                a!6
                v0x7fc03a857d10_0
                v0x7fc03a859150_0
                (= v0x7fc03a8548d0_0 (= v0x7fc03a854810_0 0.0))
                (= v0x7fc03a854d10_0 (< v0x7fc03a854190_0 2.0))
                (= v0x7fc03a854ed0_0 (ite v0x7fc03a854d10_0 1.0 0.0))
                (= v0x7fc03a855010_0 (+ v0x7fc03a854ed0_0 v0x7fc03a854190_0))
                (= v0x7fc03a855990_0 (= v0x7fc03a8558d0_0 0.0))
                (= v0x7fc03a855d90_0 (= v0x7fc03a854090_0 0.0))
                (= v0x7fc03a855ed0_0 (ite v0x7fc03a855d90_0 1.0 0.0))
                (= v0x7fc03a856850_0 (= v0x7fc03a853f10_0 0.0))
                (= v0x7fc03a856c10_0 (> v0x7fc03a855210_0 1.0))
                (= v0x7fc03a857010_0 (> v0x7fc03a855210_0 0.0))
                (= v0x7fc03a857150_0 (+ v0x7fc03a855210_0 (- 1.0)))
                (= v0x7fc03a857310_0
                   (ite v0x7fc03a857010_0 v0x7fc03a857150_0 v0x7fc03a855210_0))
                (= v0x7fc03a857450_0 (= v0x7fc03a857310_0 0.0))
                (= v0x7fc03a857810_0 (= v0x7fc03a8560d0_0 0.0))
                (= v0x7fc03a857950_0
                   (ite v0x7fc03a857810_0 1.0 v0x7fc03a853f10_0))
                (= v0x7fc03a858c50_0 (= v0x7fc03a857dd0_0 2.0))
                (= v0x7fc03a858d90_0 (= v0x7fc03a857e90_0 0.0))
                (= v0x7fc03a858ed0_0 (or v0x7fc03a858d90_0 v0x7fc03a858c50_0))
                (= v0x7fc03a859010_0 (xor v0x7fc03a858ed0_0 true))
                (= v0x7fc03a859150_0 (and v0x7fc03a856850_0 v0x7fc03a859010_0)))))
  (=> F0x7fc03a859f90 a!7))))
(assert (=> F0x7fc03a859f90 F0x7fc03a859c90))
(assert (=> F0x7fc03a85a090 (or F0x7fc03a859ed0 F0x7fc03a859d50)))
(assert (=> F0x7fc03a85a050 F0x7fc03a859f90))
(assert (=> pre!entry!0 (=> F0x7fc03a859e10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fc03a859c90 (>= v0x7fc03a854090_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fc03a859c90
        (or (<= v0x7fc03a854190_0 1.0) (>= v0x7fc03a854190_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fc03a859c90 (>= v0x7fc03a854190_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fc03a859c90
        (or (>= v0x7fc03a854190_0 1.0) (<= v0x7fc03a854190_0 0.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7fc03a859c90 (<= v0x7fc03a854190_0 2.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fc03a85a090 (>= v0x7fc03a854250_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fc03a85a090
       (or (<= v0x7fc03a852010_0 1.0) (>= v0x7fc03a852010_0 2.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fc03a85a090 (>= v0x7fc03a852010_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7fc03a85a090
       (or (>= v0x7fc03a852010_0 1.0) (<= v0x7fc03a852010_0 0.0)))))
(assert (= lemma!bb1.i.i!4 (=> F0x7fc03a85a090 (<= v0x7fc03a852010_0 2.0))))
(assert (= lemma!bb2.i.i36.i.i!0 (=> F0x7fc03a85a050 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb2.i.i36.i.i!0) (not lemma!bb2.i.i36.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
; (init: F0x7fc03a859e10)
; (error: F0x7fc03a85a050)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i36.i.i!0)
