(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i35.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f60d6baf350 () Bool)
(declare-fun F0x7f60d6baf390 () Bool)
(declare-fun F0x7f60d6baf250 () Bool)
(declare-fun v0x7f60d6badb90_0 () Bool)
(declare-fun v0x7f60d6bac990_0 () Bool)
(declare-fun v0x7f60d6ba9310_0 () Real)
(declare-fun v0x7f60d6bac850_0 () Bool)
(declare-fun v0x7f60d6bac550_0 () Real)
(declare-fun v0x7f60d6bac410_0 () Bool)
(declare-fun v0x7f60d6bab050_0 () Bool)
(declare-fun E0x7f60d6bad650 () Bool)
(declare-fun v0x7f60d6bac010_0 () Real)
(declare-fun v0x7f60d6badf10_0 () Bool)
(declare-fun v0x7f60d6bad010_0 () Real)
(declare-fun v0x7f60d6bacf50_0 () Real)
(declare-fun E0x7f60d6bad0d0 () Bool)
(declare-fun v0x7f60d6bace90_0 () Bool)
(declare-fun v0x7f60d6bacad0_0 () Bool)
(declare-fun E0x7f60d6baccd0 () Bool)
(declare-fun v0x7f60d6bacc10_0 () Bool)
(declare-fun E0x7f60d6bac210 () Bool)
(declare-fun F0x7f60d6baf190 () Bool)
(declare-fun v0x7f60d6bae1d0_0 () Bool)
(declare-fun v0x7f60d6babb10_0 () Bool)
(declare-fun E0x7f60d6bab610 () Bool)
(declare-fun E0x7f60d6bad390 () Bool)
(declare-fun v0x7f60d6baab90_0 () Real)
(declare-fun E0x7f60d6babd10 () Bool)
(declare-fun v0x7f60d6bab190_0 () Real)
(declare-fun v0x7f60d6ba8f90_0 () Real)
(declare-fun v0x7f60d6bab390_0 () Real)
(declare-fun v0x7f60d6baac50_0 () Bool)
(declare-fun v0x7f60d6bab2d0_0 () Bool)
(declare-fun E0x7f60d6baae50 () Bool)
(declare-fun v0x7f60d6baa190_0 () Real)
(declare-fun v0x7f60d6ba9ad0_0 () Real)
(declare-fun v0x7f60d6badc90_0 () Bool)
(declare-fun v0x7f60d6babc50_0 () Bool)
(declare-fun E0x7f60d6baa750 () Bool)
(declare-fun v0x7f60d6baa4d0_0 () Real)
(declare-fun E0x7f60d6ba9d90 () Bool)
(declare-fun E0x7f60d6badfd0 () Bool)
(declare-fun v0x7f60d6baa410_0 () Bool)
(declare-fun v0x7f60d6baddd0_0 () Bool)
(declare-fun v0x7f60d6ba9b90_0 () Bool)
(declare-fun v0x7f60d6ba9210_0 () Real)
(declare-fun F0x7f60d6baf0d0 () Bool)
(declare-fun E0x7f60d6baa590 () Bool)
(declare-fun F0x7f60d6baefd0 () Bool)
(declare-fun v0x7f60d6ba6010_0 () Real)
(declare-fun v0x7f60d6ba9a10_0 () Bool)
(declare-fun v0x7f60d6bac150_0 () Bool)
(declare-fun v0x7f60d6ba9cd0_0 () Bool)
(declare-fun v0x7f60d6bac710_0 () Real)
(declare-fun v0x7f60d6ba9110_0 () Real)
(declare-fun v0x7f60d6ba91d0_0 () Real)
(declare-fun v0x7f60d6ba93d0_0 () Real)
(declare-fun v0x7f60d6baad90_0 () Bool)
(declare-fun v0x7f60d6ba9fd0_0 () Bool)
(declare-fun v0x7f60d6ba92d0_0 () Real)
(declare-fun v0x7f60d6ba6110_0 () Bool)
(declare-fun E0x7f60d6bab450 () Bool)
(declare-fun v0x7f60d6baa2d0_0 () Real)
(declare-fun v0x7f60d6babed0_0 () Bool)
(declare-fun F0x7f60d6baf090 () Bool)

(assert (=> F0x7f60d6baf090
    (and v0x7f60d6ba6110_0
         (<= v0x7f60d6ba91d0_0 0.0)
         (>= v0x7f60d6ba91d0_0 0.0)
         (<= v0x7f60d6ba92d0_0 1.0)
         (>= v0x7f60d6ba92d0_0 1.0)
         (<= v0x7f60d6ba93d0_0 0.0)
         (>= v0x7f60d6ba93d0_0 0.0)
         (<= v0x7f60d6ba6010_0 0.0)
         (>= v0x7f60d6ba6010_0 0.0))))
(assert (=> F0x7f60d6baf090 F0x7f60d6baefd0))
(assert (let ((a!1 (=> v0x7f60d6baa410_0
               (or (and v0x7f60d6ba9cd0_0
                        E0x7f60d6baa590
                        (<= v0x7f60d6baa4d0_0 v0x7f60d6baa2d0_0)
                        (>= v0x7f60d6baa4d0_0 v0x7f60d6baa2d0_0))
                   (and v0x7f60d6ba9a10_0
                        E0x7f60d6baa750
                        v0x7f60d6ba9b90_0
                        (<= v0x7f60d6baa4d0_0 v0x7f60d6ba9110_0)
                        (>= v0x7f60d6baa4d0_0 v0x7f60d6ba9110_0)))))
      (a!2 (=> v0x7f60d6baa410_0
               (or (and E0x7f60d6baa590 (not E0x7f60d6baa750))
                   (and E0x7f60d6baa750 (not E0x7f60d6baa590)))))
      (a!3 (=> v0x7f60d6bab2d0_0
               (or (and v0x7f60d6baad90_0
                        E0x7f60d6bab450
                        (<= v0x7f60d6bab390_0 v0x7f60d6bab190_0)
                        (>= v0x7f60d6bab390_0 v0x7f60d6bab190_0))
                   (and v0x7f60d6baa410_0
                        E0x7f60d6bab610
                        v0x7f60d6baac50_0
                        (<= v0x7f60d6bab390_0 v0x7f60d6ba8f90_0)
                        (>= v0x7f60d6bab390_0 v0x7f60d6ba8f90_0)))))
      (a!4 (=> v0x7f60d6bab2d0_0
               (or (and E0x7f60d6bab450 (not E0x7f60d6bab610))
                   (and E0x7f60d6bab610 (not E0x7f60d6bab450)))))
      (a!5 (or (and v0x7f60d6babc50_0
                    E0x7f60d6bad0d0
                    (<= v0x7f60d6bacf50_0 v0x7f60d6baa4d0_0)
                    (>= v0x7f60d6bacf50_0 v0x7f60d6baa4d0_0)
                    (<= v0x7f60d6bad010_0 v0x7f60d6bac010_0)
                    (>= v0x7f60d6bad010_0 v0x7f60d6bac010_0))
               (and v0x7f60d6bacc10_0
                    E0x7f60d6bad390
                    (and (<= v0x7f60d6bacf50_0 v0x7f60d6bac710_0)
                         (>= v0x7f60d6bacf50_0 v0x7f60d6bac710_0))
                    (<= v0x7f60d6bad010_0 v0x7f60d6ba9210_0)
                    (>= v0x7f60d6bad010_0 v0x7f60d6ba9210_0))
               (and v0x7f60d6bac150_0
                    E0x7f60d6bad650
                    (not v0x7f60d6bacad0_0)
                    (and (<= v0x7f60d6bacf50_0 v0x7f60d6bac710_0)
                         (>= v0x7f60d6bacf50_0 v0x7f60d6bac710_0))
                    (<= v0x7f60d6bad010_0 0.0)
                    (>= v0x7f60d6bad010_0 0.0))))
      (a!6 (=> v0x7f60d6bace90_0
               (or (and E0x7f60d6bad0d0
                        (not E0x7f60d6bad390)
                        (not E0x7f60d6bad650))
                   (and E0x7f60d6bad390
                        (not E0x7f60d6bad0d0)
                        (not E0x7f60d6bad650))
                   (and E0x7f60d6bad650
                        (not E0x7f60d6bad0d0)
                        (not E0x7f60d6bad390)))))
      (a!7 (or (and v0x7f60d6badf10_0
                    v0x7f60d6bae1d0_0
                    (and (<= v0x7f60d6ba91d0_0 v0x7f60d6bab390_0)
                         (>= v0x7f60d6ba91d0_0 v0x7f60d6bab390_0))
                    (and (<= v0x7f60d6ba92d0_0 v0x7f60d6bacf50_0)
                         (>= v0x7f60d6ba92d0_0 v0x7f60d6bacf50_0))
                    (and (<= v0x7f60d6ba93d0_0 v0x7f60d6bad010_0)
                         (>= v0x7f60d6ba93d0_0 v0x7f60d6bad010_0))
                    (<= v0x7f60d6ba6010_0 1.0)
                    (>= v0x7f60d6ba6010_0 1.0))
               (and v0x7f60d6bace90_0
                    v0x7f60d6baddd0_0
                    (and (<= v0x7f60d6ba91d0_0 v0x7f60d6bab390_0)
                         (>= v0x7f60d6ba91d0_0 v0x7f60d6bab390_0))
                    (and (<= v0x7f60d6ba92d0_0 v0x7f60d6bacf50_0)
                         (>= v0x7f60d6ba92d0_0 v0x7f60d6bacf50_0))
                    (and (<= v0x7f60d6ba93d0_0 v0x7f60d6bad010_0)
                         (>= v0x7f60d6ba93d0_0 v0x7f60d6bad010_0))
                    (<= v0x7f60d6ba6010_0 0.0)
                    (>= v0x7f60d6ba6010_0 0.0)))))
(let ((a!8 (and (=> v0x7f60d6ba9cd0_0
                    (and v0x7f60d6ba9a10_0
                         E0x7f60d6ba9d90
                         (not v0x7f60d6ba9b90_0)))
                (=> v0x7f60d6ba9cd0_0 E0x7f60d6ba9d90)
                a!1
                a!2
                (=> v0x7f60d6baad90_0
                    (and v0x7f60d6baa410_0
                         E0x7f60d6baae50
                         (not v0x7f60d6baac50_0)))
                (=> v0x7f60d6baad90_0 E0x7f60d6baae50)
                a!3
                a!4
                (=> v0x7f60d6babc50_0
                    (and v0x7f60d6bab2d0_0 E0x7f60d6babd10 v0x7f60d6babb10_0))
                (=> v0x7f60d6babc50_0 E0x7f60d6babd10)
                (=> v0x7f60d6bac150_0
                    (and v0x7f60d6bab2d0_0
                         E0x7f60d6bac210
                         (not v0x7f60d6babb10_0)))
                (=> v0x7f60d6bac150_0 E0x7f60d6bac210)
                (=> v0x7f60d6bacc10_0
                    (and v0x7f60d6bac150_0 E0x7f60d6baccd0 v0x7f60d6bacad0_0))
                (=> v0x7f60d6bacc10_0 E0x7f60d6baccd0)
                (=> v0x7f60d6bace90_0 a!5)
                a!6
                (=> v0x7f60d6badf10_0
                    (and v0x7f60d6bace90_0
                         E0x7f60d6badfd0
                         (not v0x7f60d6baddd0_0)))
                (=> v0x7f60d6badf10_0 E0x7f60d6badfd0)
                a!7
                (= v0x7f60d6ba9b90_0 (= v0x7f60d6ba9ad0_0 0.0))
                (= v0x7f60d6ba9fd0_0 (< v0x7f60d6ba9110_0 2.0))
                (= v0x7f60d6baa190_0 (ite v0x7f60d6ba9fd0_0 1.0 0.0))
                (= v0x7f60d6baa2d0_0 (+ v0x7f60d6baa190_0 v0x7f60d6ba9110_0))
                (= v0x7f60d6baac50_0 (= v0x7f60d6baab90_0 0.0))
                (= v0x7f60d6bab050_0 (= v0x7f60d6ba8f90_0 0.0))
                (= v0x7f60d6bab190_0 (ite v0x7f60d6bab050_0 1.0 0.0))
                (= v0x7f60d6babb10_0 (= v0x7f60d6ba9210_0 0.0))
                (= v0x7f60d6babed0_0 (> v0x7f60d6baa4d0_0 1.0))
                (= v0x7f60d6bac010_0
                   (ite v0x7f60d6babed0_0 1.0 v0x7f60d6ba9210_0))
                (= v0x7f60d6bac410_0 (> v0x7f60d6baa4d0_0 0.0))
                (= v0x7f60d6bac550_0 (+ v0x7f60d6baa4d0_0 (- 1.0)))
                (= v0x7f60d6bac710_0
                   (ite v0x7f60d6bac410_0 v0x7f60d6bac550_0 v0x7f60d6baa4d0_0))
                (= v0x7f60d6bac850_0 (= v0x7f60d6bab390_0 0.0))
                (= v0x7f60d6bac990_0 (= v0x7f60d6bac710_0 0.0))
                (= v0x7f60d6bacad0_0 (and v0x7f60d6bac850_0 v0x7f60d6bac990_0))
                (= v0x7f60d6badb90_0 (= v0x7f60d6bab390_0 0.0))
                (= v0x7f60d6badc90_0 (= v0x7f60d6bad010_0 0.0))
                (= v0x7f60d6baddd0_0 (or v0x7f60d6badc90_0 v0x7f60d6badb90_0))
                (= v0x7f60d6bae1d0_0 (= v0x7f60d6ba9310_0 0.0)))))
  (=> F0x7f60d6baf0d0 a!8))))
(assert (=> F0x7f60d6baf0d0 F0x7f60d6baf190))
(assert (let ((a!1 (=> v0x7f60d6baa410_0
               (or (and v0x7f60d6ba9cd0_0
                        E0x7f60d6baa590
                        (<= v0x7f60d6baa4d0_0 v0x7f60d6baa2d0_0)
                        (>= v0x7f60d6baa4d0_0 v0x7f60d6baa2d0_0))
                   (and v0x7f60d6ba9a10_0
                        E0x7f60d6baa750
                        v0x7f60d6ba9b90_0
                        (<= v0x7f60d6baa4d0_0 v0x7f60d6ba9110_0)
                        (>= v0x7f60d6baa4d0_0 v0x7f60d6ba9110_0)))))
      (a!2 (=> v0x7f60d6baa410_0
               (or (and E0x7f60d6baa590 (not E0x7f60d6baa750))
                   (and E0x7f60d6baa750 (not E0x7f60d6baa590)))))
      (a!3 (=> v0x7f60d6bab2d0_0
               (or (and v0x7f60d6baad90_0
                        E0x7f60d6bab450
                        (<= v0x7f60d6bab390_0 v0x7f60d6bab190_0)
                        (>= v0x7f60d6bab390_0 v0x7f60d6bab190_0))
                   (and v0x7f60d6baa410_0
                        E0x7f60d6bab610
                        v0x7f60d6baac50_0
                        (<= v0x7f60d6bab390_0 v0x7f60d6ba8f90_0)
                        (>= v0x7f60d6bab390_0 v0x7f60d6ba8f90_0)))))
      (a!4 (=> v0x7f60d6bab2d0_0
               (or (and E0x7f60d6bab450 (not E0x7f60d6bab610))
                   (and E0x7f60d6bab610 (not E0x7f60d6bab450)))))
      (a!5 (or (and v0x7f60d6babc50_0
                    E0x7f60d6bad0d0
                    (<= v0x7f60d6bacf50_0 v0x7f60d6baa4d0_0)
                    (>= v0x7f60d6bacf50_0 v0x7f60d6baa4d0_0)
                    (<= v0x7f60d6bad010_0 v0x7f60d6bac010_0)
                    (>= v0x7f60d6bad010_0 v0x7f60d6bac010_0))
               (and v0x7f60d6bacc10_0
                    E0x7f60d6bad390
                    (and (<= v0x7f60d6bacf50_0 v0x7f60d6bac710_0)
                         (>= v0x7f60d6bacf50_0 v0x7f60d6bac710_0))
                    (<= v0x7f60d6bad010_0 v0x7f60d6ba9210_0)
                    (>= v0x7f60d6bad010_0 v0x7f60d6ba9210_0))
               (and v0x7f60d6bac150_0
                    E0x7f60d6bad650
                    (not v0x7f60d6bacad0_0)
                    (and (<= v0x7f60d6bacf50_0 v0x7f60d6bac710_0)
                         (>= v0x7f60d6bacf50_0 v0x7f60d6bac710_0))
                    (<= v0x7f60d6bad010_0 0.0)
                    (>= v0x7f60d6bad010_0 0.0))))
      (a!6 (=> v0x7f60d6bace90_0
               (or (and E0x7f60d6bad0d0
                        (not E0x7f60d6bad390)
                        (not E0x7f60d6bad650))
                   (and E0x7f60d6bad390
                        (not E0x7f60d6bad0d0)
                        (not E0x7f60d6bad650))
                   (and E0x7f60d6bad650
                        (not E0x7f60d6bad0d0)
                        (not E0x7f60d6bad390))))))
(let ((a!7 (and (=> v0x7f60d6ba9cd0_0
                    (and v0x7f60d6ba9a10_0
                         E0x7f60d6ba9d90
                         (not v0x7f60d6ba9b90_0)))
                (=> v0x7f60d6ba9cd0_0 E0x7f60d6ba9d90)
                a!1
                a!2
                (=> v0x7f60d6baad90_0
                    (and v0x7f60d6baa410_0
                         E0x7f60d6baae50
                         (not v0x7f60d6baac50_0)))
                (=> v0x7f60d6baad90_0 E0x7f60d6baae50)
                a!3
                a!4
                (=> v0x7f60d6babc50_0
                    (and v0x7f60d6bab2d0_0 E0x7f60d6babd10 v0x7f60d6babb10_0))
                (=> v0x7f60d6babc50_0 E0x7f60d6babd10)
                (=> v0x7f60d6bac150_0
                    (and v0x7f60d6bab2d0_0
                         E0x7f60d6bac210
                         (not v0x7f60d6babb10_0)))
                (=> v0x7f60d6bac150_0 E0x7f60d6bac210)
                (=> v0x7f60d6bacc10_0
                    (and v0x7f60d6bac150_0 E0x7f60d6baccd0 v0x7f60d6bacad0_0))
                (=> v0x7f60d6bacc10_0 E0x7f60d6baccd0)
                (=> v0x7f60d6bace90_0 a!5)
                a!6
                (=> v0x7f60d6badf10_0
                    (and v0x7f60d6bace90_0
                         E0x7f60d6badfd0
                         (not v0x7f60d6baddd0_0)))
                (=> v0x7f60d6badf10_0 E0x7f60d6badfd0)
                v0x7f60d6badf10_0
                (not v0x7f60d6bae1d0_0)
                (= v0x7f60d6ba9b90_0 (= v0x7f60d6ba9ad0_0 0.0))
                (= v0x7f60d6ba9fd0_0 (< v0x7f60d6ba9110_0 2.0))
                (= v0x7f60d6baa190_0 (ite v0x7f60d6ba9fd0_0 1.0 0.0))
                (= v0x7f60d6baa2d0_0 (+ v0x7f60d6baa190_0 v0x7f60d6ba9110_0))
                (= v0x7f60d6baac50_0 (= v0x7f60d6baab90_0 0.0))
                (= v0x7f60d6bab050_0 (= v0x7f60d6ba8f90_0 0.0))
                (= v0x7f60d6bab190_0 (ite v0x7f60d6bab050_0 1.0 0.0))
                (= v0x7f60d6babb10_0 (= v0x7f60d6ba9210_0 0.0))
                (= v0x7f60d6babed0_0 (> v0x7f60d6baa4d0_0 1.0))
                (= v0x7f60d6bac010_0
                   (ite v0x7f60d6babed0_0 1.0 v0x7f60d6ba9210_0))
                (= v0x7f60d6bac410_0 (> v0x7f60d6baa4d0_0 0.0))
                (= v0x7f60d6bac550_0 (+ v0x7f60d6baa4d0_0 (- 1.0)))
                (= v0x7f60d6bac710_0
                   (ite v0x7f60d6bac410_0 v0x7f60d6bac550_0 v0x7f60d6baa4d0_0))
                (= v0x7f60d6bac850_0 (= v0x7f60d6bab390_0 0.0))
                (= v0x7f60d6bac990_0 (= v0x7f60d6bac710_0 0.0))
                (= v0x7f60d6bacad0_0 (and v0x7f60d6bac850_0 v0x7f60d6bac990_0))
                (= v0x7f60d6badb90_0 (= v0x7f60d6bab390_0 0.0))
                (= v0x7f60d6badc90_0 (= v0x7f60d6bad010_0 0.0))
                (= v0x7f60d6baddd0_0 (or v0x7f60d6badc90_0 v0x7f60d6badb90_0))
                (= v0x7f60d6bae1d0_0 (= v0x7f60d6ba9310_0 0.0)))))
  (=> F0x7f60d6baf250 a!7))))
(assert (=> F0x7f60d6baf250 F0x7f60d6baf190))
(assert (=> F0x7f60d6baf390 (or F0x7f60d6baf090 F0x7f60d6baf0d0)))
(assert (=> F0x7f60d6baf350 F0x7f60d6baf250))
(assert (=> pre!entry!0 (=> F0x7f60d6baefd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f60d6baf190 (>= v0x7f60d6ba9310_0 0.0))))
(assert (let ((a!1 (=> F0x7f60d6baf190
               (or (<= v0x7f60d6ba9310_0 0.0) (not (<= v0x7f60d6ba9210_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f60d6baf390 (>= v0x7f60d6ba6010_0 0.0))))
(assert (let ((a!1 (=> F0x7f60d6baf390
               (or (<= v0x7f60d6ba6010_0 0.0) (not (<= v0x7f60d6ba93d0_0 0.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb2.i.i35.i.i!0 (=> F0x7f60d6baf350 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i35.i.i!0) (not lemma!bb2.i.i35.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f60d6baefd0)
; (error: F0x7f60d6baf350)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
