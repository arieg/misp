(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f7ebdbfb7d0 () Bool)
(declare-fun F0x7f7ebdbfb610 () Bool)
(declare-fun v0x7f7ebdbfa510_0 () Bool)
(declare-fun v0x7f7ebdbf8c10_0 () Bool)
(declare-fun v0x7f7ebdbf87d0_0 () Real)
(declare-fun v0x7f7ebdbf8ad0_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7f7ebdbf6f50_0 () Real)
(declare-fun v0x7f7ebdbfa890_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f7ebdbf6390_0 () Bool)
(declare-fun v0x7f7ebdbf5e90_0 () Real)
(declare-fun v0x7f7ebdbfa9d0_0 () Bool)
(declare-fun v0x7f7ebdbf8990_0 () Real)
(declare-fun E0x7f7ebdbf9ad0 () Bool)
(declare-fun v0x7f7ebdbf9210_0 () Real)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f7ebdbf9690_0 () Real)
(declare-fun E0x7f7ebdbf9410 () Bool)
(declare-fun v0x7f7ebdbf95d0_0 () Bool)
(declare-fun v0x7f7ebdbf9350_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f7ebdbf8e90_0 () Bool)
(declare-fun v0x7f7ebdbf5590_0 () Real)
(declare-fun E0x7f7ebdbf8490 () Bool)
(declare-fun E0x7f7ebdbf80d0 () Bool)
(declare-fun E0x7f7ebdbf9810 () Bool)
(declare-fun v0x7f7ebdbf8690_0 () Bool)
(declare-fun v0x7f7ebdbf5710_0 () Real)
(declare-fun v0x7f7ebdbf8290_0 () Bool)
(declare-fun E0x7f7ebdbf79d0 () Bool)
(declare-fun v0x7f7ebdbf7410_0 () Bool)
(declare-fun v0x7f7ebdbf7690_0 () Bool)
(declare-fun E0x7f7ebdbf9d50 () Bool)
(declare-fun v0x7f7ebdbf7010_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f7ebdbf7210 () Bool)
(declare-fun v0x7f7ebdbf7150_0 () Bool)
(declare-fun v0x7f7ebdbf7750_0 () Real)
(declare-fun v0x7f7ebdbf7550_0 () Real)
(declare-fun v0x7f7ebdbf9750_0 () Real)
(declare-fun v0x7f7ebdbf5810_0 () Real)
(declare-fun E0x7f7ebdbf6b10 () Bool)
(declare-fun E0x7f7ebdbf7810 () Bool)
(declare-fun v0x7f7ebdbf6890_0 () Real)
(declare-fun E0x7f7ebdbf8f50 () Bool)
(declare-fun F0x7f7ebdbfb8d0 () Bool)
(declare-fun E0x7f7ebdbf6950 () Bool)
(declare-fun v0x7f7ebdbf9110_0 () Bool)
(declare-fun v0x7f7ebdbf5dd0_0 () Bool)
(declare-fun v0x7f7ebdbf6090_0 () Bool)
(declare-fun v0x7f7ebdbf6690_0 () Real)
(declare-fun lemma!bb2.i.i43.i.i!0 () Bool)
(declare-fun v0x7f7ebdbfa750_0 () Bool)
(declare-fun v0x7f7ebdbf67d0_0 () Bool)
(declare-fun F0x7f7ebdbfb650 () Bool)
(declare-fun v0x7f7ebdbf7ed0_0 () Bool)
(declare-fun v0x7f7ebdbfa610_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f7ebdbf2010_0 () Real)
(declare-fun v0x7f7ebdbf58d0_0 () Real)
(declare-fun F0x7f7ebdbfb910 () Bool)
(declare-fun F0x7f7ebdbfb550 () Bool)
(declare-fun post!bb2.i.i43.i.i!0 () Bool)
(declare-fun E0x7f7ebdbf6150 () Bool)
(declare-fun v0x7f7ebdbf8010_0 () Bool)
(declare-fun v0x7f7ebdbf57d0_0 () Real)
(declare-fun v0x7f7ebdbf83d0_0 () Bool)
(declare-fun v0x7f7ebdbf6550_0 () Real)
(declare-fun v0x7f7ebdbf2110_0 () Bool)
(declare-fun E0x7f7ebdbf9f50 () Bool)
(declare-fun v0x7f7ebdbf5f50_0 () Bool)
(declare-fun v0x7f7ebdbf8d50_0 () Bool)
(declare-fun F0x7f7ebdbfb710 () Bool)

(assert (=> F0x7f7ebdbfb710
    (and v0x7f7ebdbf2110_0
         (<= v0x7f7ebdbf57d0_0 0.0)
         (>= v0x7f7ebdbf57d0_0 0.0)
         (<= v0x7f7ebdbf58d0_0 0.0)
         (>= v0x7f7ebdbf58d0_0 0.0)
         (<= v0x7f7ebdbf2010_0 1.0)
         (>= v0x7f7ebdbf2010_0 1.0))))
(assert (=> F0x7f7ebdbfb710 F0x7f7ebdbfb650))
(assert (let ((a!1 (=> v0x7f7ebdbf67d0_0
               (or (and v0x7f7ebdbf6090_0
                        E0x7f7ebdbf6950
                        (<= v0x7f7ebdbf6890_0 v0x7f7ebdbf6690_0)
                        (>= v0x7f7ebdbf6890_0 v0x7f7ebdbf6690_0))
                   (and v0x7f7ebdbf5dd0_0
                        E0x7f7ebdbf6b10
                        v0x7f7ebdbf5f50_0
                        (<= v0x7f7ebdbf6890_0 v0x7f7ebdbf5810_0)
                        (>= v0x7f7ebdbf6890_0 v0x7f7ebdbf5810_0)))))
      (a!2 (=> v0x7f7ebdbf67d0_0
               (or (and E0x7f7ebdbf6950 (not E0x7f7ebdbf6b10))
                   (and E0x7f7ebdbf6b10 (not E0x7f7ebdbf6950)))))
      (a!3 (=> v0x7f7ebdbf7690_0
               (or (and v0x7f7ebdbf7150_0
                        E0x7f7ebdbf7810
                        (<= v0x7f7ebdbf7750_0 v0x7f7ebdbf7550_0)
                        (>= v0x7f7ebdbf7750_0 v0x7f7ebdbf7550_0))
                   (and v0x7f7ebdbf67d0_0
                        E0x7f7ebdbf79d0
                        v0x7f7ebdbf7010_0
                        (<= v0x7f7ebdbf7750_0 v0x7f7ebdbf5710_0)
                        (>= v0x7f7ebdbf7750_0 v0x7f7ebdbf5710_0)))))
      (a!4 (=> v0x7f7ebdbf7690_0
               (or (and E0x7f7ebdbf7810 (not E0x7f7ebdbf79d0))
                   (and E0x7f7ebdbf79d0 (not E0x7f7ebdbf7810)))))
      (a!5 (or (and v0x7f7ebdbf8e90_0
                    E0x7f7ebdbf9810
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0))
                    (<= v0x7f7ebdbf9750_0 v0x7f7ebdbf9210_0)
                    (>= v0x7f7ebdbf9750_0 v0x7f7ebdbf9210_0))
               (and v0x7f7ebdbf8010_0
                    E0x7f7ebdbf9ad0
                    (not v0x7f7ebdbf8290_0)
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0))
                    (and (<= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)
                         (>= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)))
               (and v0x7f7ebdbf9350_0
                    E0x7f7ebdbf9d50
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0))
                    (and (<= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)
                         (>= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)))
               (and v0x7f7ebdbf83d0_0
                    E0x7f7ebdbf9f50
                    (not v0x7f7ebdbf8d50_0)
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0))
                    (<= v0x7f7ebdbf9750_0 0.0)
                    (>= v0x7f7ebdbf9750_0 0.0))))
      (a!6 (=> v0x7f7ebdbf95d0_0
               (or (and E0x7f7ebdbf9810
                        (not E0x7f7ebdbf9ad0)
                        (not E0x7f7ebdbf9d50)
                        (not E0x7f7ebdbf9f50))
                   (and E0x7f7ebdbf9ad0
                        (not E0x7f7ebdbf9810)
                        (not E0x7f7ebdbf9d50)
                        (not E0x7f7ebdbf9f50))
                   (and E0x7f7ebdbf9d50
                        (not E0x7f7ebdbf9810)
                        (not E0x7f7ebdbf9ad0)
                        (not E0x7f7ebdbf9f50))
                   (and E0x7f7ebdbf9f50
                        (not E0x7f7ebdbf9810)
                        (not E0x7f7ebdbf9ad0)
                        (not E0x7f7ebdbf9d50))))))
(let ((a!7 (and (=> v0x7f7ebdbf6090_0
                    (and v0x7f7ebdbf5dd0_0
                         E0x7f7ebdbf6150
                         (not v0x7f7ebdbf5f50_0)))
                (=> v0x7f7ebdbf6090_0 E0x7f7ebdbf6150)
                a!1
                a!2
                (=> v0x7f7ebdbf7150_0
                    (and v0x7f7ebdbf67d0_0
                         E0x7f7ebdbf7210
                         (not v0x7f7ebdbf7010_0)))
                (=> v0x7f7ebdbf7150_0 E0x7f7ebdbf7210)
                a!3
                a!4
                (=> v0x7f7ebdbf8010_0
                    (and v0x7f7ebdbf7690_0 E0x7f7ebdbf80d0 v0x7f7ebdbf7ed0_0))
                (=> v0x7f7ebdbf8010_0 E0x7f7ebdbf80d0)
                (=> v0x7f7ebdbf83d0_0
                    (and v0x7f7ebdbf7690_0
                         E0x7f7ebdbf8490
                         (not v0x7f7ebdbf7ed0_0)))
                (=> v0x7f7ebdbf83d0_0 E0x7f7ebdbf8490)
                (=> v0x7f7ebdbf8e90_0
                    (and v0x7f7ebdbf8010_0 E0x7f7ebdbf8f50 v0x7f7ebdbf8290_0))
                (=> v0x7f7ebdbf8e90_0 E0x7f7ebdbf8f50)
                (=> v0x7f7ebdbf9350_0
                    (and v0x7f7ebdbf83d0_0 E0x7f7ebdbf9410 v0x7f7ebdbf8d50_0))
                (=> v0x7f7ebdbf9350_0 E0x7f7ebdbf9410)
                (=> v0x7f7ebdbf95d0_0 a!5)
                a!6
                v0x7f7ebdbf95d0_0
                (not v0x7f7ebdbfa9d0_0)
                (<= v0x7f7ebdbf57d0_0 v0x7f7ebdbf9750_0)
                (>= v0x7f7ebdbf57d0_0 v0x7f7ebdbf9750_0)
                (<= v0x7f7ebdbf58d0_0 v0x7f7ebdbf7750_0)
                (>= v0x7f7ebdbf58d0_0 v0x7f7ebdbf7750_0)
                (<= v0x7f7ebdbf2010_0 v0x7f7ebdbf9690_0)
                (>= v0x7f7ebdbf2010_0 v0x7f7ebdbf9690_0)
                (= v0x7f7ebdbf5f50_0 (= v0x7f7ebdbf5e90_0 0.0))
                (= v0x7f7ebdbf6390_0 (< v0x7f7ebdbf5810_0 2.0))
                (= v0x7f7ebdbf6550_0 (ite v0x7f7ebdbf6390_0 1.0 0.0))
                (= v0x7f7ebdbf6690_0 (+ v0x7f7ebdbf6550_0 v0x7f7ebdbf5810_0))
                (= v0x7f7ebdbf7010_0 (= v0x7f7ebdbf6f50_0 0.0))
                (= v0x7f7ebdbf7410_0 (= v0x7f7ebdbf5710_0 0.0))
                (= v0x7f7ebdbf7550_0 (ite v0x7f7ebdbf7410_0 1.0 0.0))
                (= v0x7f7ebdbf7ed0_0 (= v0x7f7ebdbf5590_0 0.0))
                (= v0x7f7ebdbf8290_0 (> v0x7f7ebdbf6890_0 1.0))
                (= v0x7f7ebdbf8690_0 (> v0x7f7ebdbf6890_0 0.0))
                (= v0x7f7ebdbf87d0_0 (+ v0x7f7ebdbf6890_0 (- 1.0)))
                (= v0x7f7ebdbf8990_0
                   (ite v0x7f7ebdbf8690_0 v0x7f7ebdbf87d0_0 v0x7f7ebdbf6890_0))
                (= v0x7f7ebdbf8ad0_0 (= v0x7f7ebdbf7750_0 0.0))
                (= v0x7f7ebdbf8c10_0 (= v0x7f7ebdbf8990_0 0.0))
                (= v0x7f7ebdbf8d50_0 (and v0x7f7ebdbf8ad0_0 v0x7f7ebdbf8c10_0))
                (= v0x7f7ebdbf9110_0 (= v0x7f7ebdbf7750_0 0.0))
                (= v0x7f7ebdbf9210_0
                   (ite v0x7f7ebdbf9110_0 1.0 v0x7f7ebdbf5590_0))
                (= v0x7f7ebdbfa510_0 (= v0x7f7ebdbf7750_0 0.0))
                (= v0x7f7ebdbfa610_0 (= v0x7f7ebdbf9690_0 2.0))
                (= v0x7f7ebdbfa750_0 (= v0x7f7ebdbf9750_0 0.0))
                (= v0x7f7ebdbfa890_0 (and v0x7f7ebdbfa610_0 v0x7f7ebdbfa510_0))
                (= v0x7f7ebdbfa9d0_0 (and v0x7f7ebdbfa890_0 v0x7f7ebdbfa750_0)))))
  (=> F0x7f7ebdbfb550 a!7))))
(assert (=> F0x7f7ebdbfb550 F0x7f7ebdbfb610))
(assert (let ((a!1 (=> v0x7f7ebdbf67d0_0
               (or (and v0x7f7ebdbf6090_0
                        E0x7f7ebdbf6950
                        (<= v0x7f7ebdbf6890_0 v0x7f7ebdbf6690_0)
                        (>= v0x7f7ebdbf6890_0 v0x7f7ebdbf6690_0))
                   (and v0x7f7ebdbf5dd0_0
                        E0x7f7ebdbf6b10
                        v0x7f7ebdbf5f50_0
                        (<= v0x7f7ebdbf6890_0 v0x7f7ebdbf5810_0)
                        (>= v0x7f7ebdbf6890_0 v0x7f7ebdbf5810_0)))))
      (a!2 (=> v0x7f7ebdbf67d0_0
               (or (and E0x7f7ebdbf6950 (not E0x7f7ebdbf6b10))
                   (and E0x7f7ebdbf6b10 (not E0x7f7ebdbf6950)))))
      (a!3 (=> v0x7f7ebdbf7690_0
               (or (and v0x7f7ebdbf7150_0
                        E0x7f7ebdbf7810
                        (<= v0x7f7ebdbf7750_0 v0x7f7ebdbf7550_0)
                        (>= v0x7f7ebdbf7750_0 v0x7f7ebdbf7550_0))
                   (and v0x7f7ebdbf67d0_0
                        E0x7f7ebdbf79d0
                        v0x7f7ebdbf7010_0
                        (<= v0x7f7ebdbf7750_0 v0x7f7ebdbf5710_0)
                        (>= v0x7f7ebdbf7750_0 v0x7f7ebdbf5710_0)))))
      (a!4 (=> v0x7f7ebdbf7690_0
               (or (and E0x7f7ebdbf7810 (not E0x7f7ebdbf79d0))
                   (and E0x7f7ebdbf79d0 (not E0x7f7ebdbf7810)))))
      (a!5 (or (and v0x7f7ebdbf8e90_0
                    E0x7f7ebdbf9810
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0))
                    (<= v0x7f7ebdbf9750_0 v0x7f7ebdbf9210_0)
                    (>= v0x7f7ebdbf9750_0 v0x7f7ebdbf9210_0))
               (and v0x7f7ebdbf8010_0
                    E0x7f7ebdbf9ad0
                    (not v0x7f7ebdbf8290_0)
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf6890_0))
                    (and (<= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)
                         (>= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)))
               (and v0x7f7ebdbf9350_0
                    E0x7f7ebdbf9d50
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0))
                    (and (<= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)
                         (>= v0x7f7ebdbf9750_0 v0x7f7ebdbf5590_0)))
               (and v0x7f7ebdbf83d0_0
                    E0x7f7ebdbf9f50
                    (not v0x7f7ebdbf8d50_0)
                    (and (<= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0)
                         (>= v0x7f7ebdbf9690_0 v0x7f7ebdbf8990_0))
                    (<= v0x7f7ebdbf9750_0 0.0)
                    (>= v0x7f7ebdbf9750_0 0.0))))
      (a!6 (=> v0x7f7ebdbf95d0_0
               (or (and E0x7f7ebdbf9810
                        (not E0x7f7ebdbf9ad0)
                        (not E0x7f7ebdbf9d50)
                        (not E0x7f7ebdbf9f50))
                   (and E0x7f7ebdbf9ad0
                        (not E0x7f7ebdbf9810)
                        (not E0x7f7ebdbf9d50)
                        (not E0x7f7ebdbf9f50))
                   (and E0x7f7ebdbf9d50
                        (not E0x7f7ebdbf9810)
                        (not E0x7f7ebdbf9ad0)
                        (not E0x7f7ebdbf9f50))
                   (and E0x7f7ebdbf9f50
                        (not E0x7f7ebdbf9810)
                        (not E0x7f7ebdbf9ad0)
                        (not E0x7f7ebdbf9d50))))))
(let ((a!7 (and (=> v0x7f7ebdbf6090_0
                    (and v0x7f7ebdbf5dd0_0
                         E0x7f7ebdbf6150
                         (not v0x7f7ebdbf5f50_0)))
                (=> v0x7f7ebdbf6090_0 E0x7f7ebdbf6150)
                a!1
                a!2
                (=> v0x7f7ebdbf7150_0
                    (and v0x7f7ebdbf67d0_0
                         E0x7f7ebdbf7210
                         (not v0x7f7ebdbf7010_0)))
                (=> v0x7f7ebdbf7150_0 E0x7f7ebdbf7210)
                a!3
                a!4
                (=> v0x7f7ebdbf8010_0
                    (and v0x7f7ebdbf7690_0 E0x7f7ebdbf80d0 v0x7f7ebdbf7ed0_0))
                (=> v0x7f7ebdbf8010_0 E0x7f7ebdbf80d0)
                (=> v0x7f7ebdbf83d0_0
                    (and v0x7f7ebdbf7690_0
                         E0x7f7ebdbf8490
                         (not v0x7f7ebdbf7ed0_0)))
                (=> v0x7f7ebdbf83d0_0 E0x7f7ebdbf8490)
                (=> v0x7f7ebdbf8e90_0
                    (and v0x7f7ebdbf8010_0 E0x7f7ebdbf8f50 v0x7f7ebdbf8290_0))
                (=> v0x7f7ebdbf8e90_0 E0x7f7ebdbf8f50)
                (=> v0x7f7ebdbf9350_0
                    (and v0x7f7ebdbf83d0_0 E0x7f7ebdbf9410 v0x7f7ebdbf8d50_0))
                (=> v0x7f7ebdbf9350_0 E0x7f7ebdbf9410)
                (=> v0x7f7ebdbf95d0_0 a!5)
                a!6
                v0x7f7ebdbf95d0_0
                v0x7f7ebdbfa9d0_0
                (= v0x7f7ebdbf5f50_0 (= v0x7f7ebdbf5e90_0 0.0))
                (= v0x7f7ebdbf6390_0 (< v0x7f7ebdbf5810_0 2.0))
                (= v0x7f7ebdbf6550_0 (ite v0x7f7ebdbf6390_0 1.0 0.0))
                (= v0x7f7ebdbf6690_0 (+ v0x7f7ebdbf6550_0 v0x7f7ebdbf5810_0))
                (= v0x7f7ebdbf7010_0 (= v0x7f7ebdbf6f50_0 0.0))
                (= v0x7f7ebdbf7410_0 (= v0x7f7ebdbf5710_0 0.0))
                (= v0x7f7ebdbf7550_0 (ite v0x7f7ebdbf7410_0 1.0 0.0))
                (= v0x7f7ebdbf7ed0_0 (= v0x7f7ebdbf5590_0 0.0))
                (= v0x7f7ebdbf8290_0 (> v0x7f7ebdbf6890_0 1.0))
                (= v0x7f7ebdbf8690_0 (> v0x7f7ebdbf6890_0 0.0))
                (= v0x7f7ebdbf87d0_0 (+ v0x7f7ebdbf6890_0 (- 1.0)))
                (= v0x7f7ebdbf8990_0
                   (ite v0x7f7ebdbf8690_0 v0x7f7ebdbf87d0_0 v0x7f7ebdbf6890_0))
                (= v0x7f7ebdbf8ad0_0 (= v0x7f7ebdbf7750_0 0.0))
                (= v0x7f7ebdbf8c10_0 (= v0x7f7ebdbf8990_0 0.0))
                (= v0x7f7ebdbf8d50_0 (and v0x7f7ebdbf8ad0_0 v0x7f7ebdbf8c10_0))
                (= v0x7f7ebdbf9110_0 (= v0x7f7ebdbf7750_0 0.0))
                (= v0x7f7ebdbf9210_0
                   (ite v0x7f7ebdbf9110_0 1.0 v0x7f7ebdbf5590_0))
                (= v0x7f7ebdbfa510_0 (= v0x7f7ebdbf7750_0 0.0))
                (= v0x7f7ebdbfa610_0 (= v0x7f7ebdbf9690_0 2.0))
                (= v0x7f7ebdbfa750_0 (= v0x7f7ebdbf9750_0 0.0))
                (= v0x7f7ebdbfa890_0 (and v0x7f7ebdbfa610_0 v0x7f7ebdbfa510_0))
                (= v0x7f7ebdbfa9d0_0 (and v0x7f7ebdbfa890_0 v0x7f7ebdbfa750_0)))))
  (=> F0x7f7ebdbfb7d0 a!7))))
(assert (=> F0x7f7ebdbfb7d0 F0x7f7ebdbfb610))
(assert (=> F0x7f7ebdbfb910 (or F0x7f7ebdbfb710 F0x7f7ebdbfb550)))
(assert (=> F0x7f7ebdbfb8d0 F0x7f7ebdbfb7d0))
(assert (=> pre!entry!0 (=> F0x7f7ebdbfb650 true)))
(assert (=> pre!bb1.i.i!0
    (=> F0x7f7ebdbfb610
        (or (<= v0x7f7ebdbf5710_0 0.0) (<= v0x7f7ebdbf5590_0 0.0)))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f7ebdbfb610 (>= v0x7f7ebdbf5590_0 0.0))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f7ebdbfb610 (not (<= 3.0 v0x7f7ebdbf5810_0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f7ebdbfb610 (>= v0x7f7ebdbf5710_0 0.0))))
(assert (= lemma!bb1.i.i!0
   (=> F0x7f7ebdbfb910
       (or (<= v0x7f7ebdbf58d0_0 0.0) (<= v0x7f7ebdbf57d0_0 0.0)))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f7ebdbfb910 (>= v0x7f7ebdbf57d0_0 0.0))))
(assert (= lemma!bb1.i.i!2 (=> F0x7f7ebdbfb910 (not (<= 3.0 v0x7f7ebdbf2010_0)))))
(assert (= lemma!bb1.i.i!3 (=> F0x7f7ebdbfb910 (>= v0x7f7ebdbf58d0_0 0.0))))
(assert (= lemma!bb2.i.i43.i.i!0 (=> F0x7f7ebdbfb8d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb2.i.i43.i.i!0) (not lemma!bb2.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7f7ebdbfb650)
; (error: F0x7f7ebdbfb8d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i43.i.i!0)
