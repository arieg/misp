(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i35.i.i!0 () Bool)
(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun F0x7ffe244f5350 () Bool)
(declare-fun F0x7ffe244f5390 () Bool)
(declare-fun v0x7ffe244f2550_0 () Real)
(declare-fun v0x7ffe244f1ed0_0 () Bool)
(declare-fun v0x7ffe244f1050_0 () Bool)
(declare-fun v0x7ffe244f0b90_0 () Real)
(declare-fun v0x7ffe244f0190_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7ffe244efad0_0 () Real)
(declare-fun v0x7ffe244f3dd0_0 () Bool)
(declare-fun v0x7ffe244f3f10_0 () Bool)
(declare-fun v0x7ffe244f2410_0 () Bool)
(declare-fun E0x7ffe244f3650 () Bool)
(declare-fun v0x7ffe244ef310_0 () Real)
(declare-fun v0x7ffe244f2f50_0 () Real)
(declare-fun E0x7ffe244f30d0 () Bool)
(declare-fun v0x7ffe244effd0_0 () Bool)
(declare-fun v0x7ffe244f2e90_0 () Bool)
(declare-fun v0x7ffe244f2ad0_0 () Bool)
(declare-fun E0x7ffe244f2cd0 () Bool)
(declare-fun E0x7ffe244f2210 () Bool)
(declare-fun v0x7ffe244f3c90_0 () Bool)
(declare-fun v0x7ffe244f2150_0 () Bool)
(declare-fun v0x7ffe244f2850_0 () Bool)
(declare-fun v0x7ffe244f1b10_0 () Bool)
(declare-fun E0x7ffe244f1d10 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7ffe244f1610 () Bool)
(declare-fun F0x7ffe244f5250 () Bool)
(declare-fun v0x7ffe244f1390_0 () Real)
(declare-fun v0x7ffe244f12d0_0 () Bool)
(declare-fun F0x7ffe244f5190 () Bool)
(declare-fun v0x7ffe244f2710_0 () Real)
(declare-fun v0x7ffe244f3b90_0 () Bool)
(declare-fun E0x7ffe244f0750 () Bool)
(declare-fun v0x7ffe244f02d0_0 () Real)
(declare-fun v0x7ffe244f0c50_0 () Bool)
(declare-fun E0x7ffe244f0590 () Bool)
(declare-fun v0x7ffe244efcd0_0 () Bool)
(declare-fun v0x7ffe244efa10_0 () Bool)
(declare-fun v0x7ffe244f2990_0 () Bool)
(declare-fun F0x7ffe244f50d0 () Bool)
(declare-fun v0x7ffe244f1190_0 () Real)
(declare-fun v0x7ffe244f0410_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7ffe244f1450 () Bool)
(declare-fun F0x7ffe244f4fd0 () Bool)
(declare-fun v0x7ffe244ec010_0 () Real)
(declare-fun E0x7ffe244f3390 () Bool)
(declare-fun v0x7ffe244ef3d0_0 () Real)
(declare-fun v0x7ffe244f2010_0 () Real)
(declare-fun v0x7ffe244f41d0_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7ffe244eef90_0 () Real)
(declare-fun v0x7ffe244f2c10_0 () Bool)
(declare-fun v0x7ffe244ef2d0_0 () Real)
(declare-fun v0x7ffe244f0d90_0 () Bool)
(declare-fun E0x7ffe244efd90 () Bool)
(declare-fun v0x7ffe244ef210_0 () Real)
(declare-fun v0x7ffe244f3010_0 () Real)
(declare-fun E0x7ffe244f3fd0 () Bool)
(declare-fun v0x7ffe244ef110_0 () Real)
(declare-fun v0x7ffe244ef1d0_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7ffe244f1c50_0 () Bool)
(declare-fun v0x7ffe244ec110_0 () Bool)
(declare-fun E0x7ffe244f0e50 () Bool)
(declare-fun v0x7ffe244efb90_0 () Bool)
(declare-fun v0x7ffe244f04d0_0 () Real)
(declare-fun F0x7ffe244f5090 () Bool)

(assert (=> F0x7ffe244f5090
    (and v0x7ffe244ec110_0
         (<= v0x7ffe244ef1d0_0 0.0)
         (>= v0x7ffe244ef1d0_0 0.0)
         (<= v0x7ffe244ef2d0_0 1.0)
         (>= v0x7ffe244ef2d0_0 1.0)
         (<= v0x7ffe244ef3d0_0 0.0)
         (>= v0x7ffe244ef3d0_0 0.0)
         (<= v0x7ffe244ec010_0 0.0)
         (>= v0x7ffe244ec010_0 0.0))))
(assert (=> F0x7ffe244f5090 F0x7ffe244f4fd0))
(assert (let ((a!1 (=> v0x7ffe244f0410_0
               (or (and v0x7ffe244efcd0_0
                        E0x7ffe244f0590
                        (<= v0x7ffe244f04d0_0 v0x7ffe244f02d0_0)
                        (>= v0x7ffe244f04d0_0 v0x7ffe244f02d0_0))
                   (and v0x7ffe244efa10_0
                        E0x7ffe244f0750
                        v0x7ffe244efb90_0
                        (<= v0x7ffe244f04d0_0 v0x7ffe244ef110_0)
                        (>= v0x7ffe244f04d0_0 v0x7ffe244ef110_0)))))
      (a!2 (=> v0x7ffe244f0410_0
               (or (and E0x7ffe244f0590 (not E0x7ffe244f0750))
                   (and E0x7ffe244f0750 (not E0x7ffe244f0590)))))
      (a!3 (=> v0x7ffe244f12d0_0
               (or (and v0x7ffe244f0d90_0
                        E0x7ffe244f1450
                        (<= v0x7ffe244f1390_0 v0x7ffe244f1190_0)
                        (>= v0x7ffe244f1390_0 v0x7ffe244f1190_0))
                   (and v0x7ffe244f0410_0
                        E0x7ffe244f1610
                        v0x7ffe244f0c50_0
                        (<= v0x7ffe244f1390_0 v0x7ffe244eef90_0)
                        (>= v0x7ffe244f1390_0 v0x7ffe244eef90_0)))))
      (a!4 (=> v0x7ffe244f12d0_0
               (or (and E0x7ffe244f1450 (not E0x7ffe244f1610))
                   (and E0x7ffe244f1610 (not E0x7ffe244f1450)))))
      (a!5 (or (and v0x7ffe244f1c50_0
                    E0x7ffe244f30d0
                    (<= v0x7ffe244f2f50_0 v0x7ffe244f04d0_0)
                    (>= v0x7ffe244f2f50_0 v0x7ffe244f04d0_0)
                    (<= v0x7ffe244f3010_0 v0x7ffe244f2010_0)
                    (>= v0x7ffe244f3010_0 v0x7ffe244f2010_0))
               (and v0x7ffe244f2c10_0
                    E0x7ffe244f3390
                    (and (<= v0x7ffe244f2f50_0 v0x7ffe244f2710_0)
                         (>= v0x7ffe244f2f50_0 v0x7ffe244f2710_0))
                    (<= v0x7ffe244f3010_0 v0x7ffe244ef310_0)
                    (>= v0x7ffe244f3010_0 v0x7ffe244ef310_0))
               (and v0x7ffe244f2150_0
                    E0x7ffe244f3650
                    (not v0x7ffe244f2ad0_0)
                    (and (<= v0x7ffe244f2f50_0 v0x7ffe244f2710_0)
                         (>= v0x7ffe244f2f50_0 v0x7ffe244f2710_0))
                    (<= v0x7ffe244f3010_0 0.0)
                    (>= v0x7ffe244f3010_0 0.0))))
      (a!6 (=> v0x7ffe244f2e90_0
               (or (and E0x7ffe244f30d0
                        (not E0x7ffe244f3390)
                        (not E0x7ffe244f3650))
                   (and E0x7ffe244f3390
                        (not E0x7ffe244f30d0)
                        (not E0x7ffe244f3650))
                   (and E0x7ffe244f3650
                        (not E0x7ffe244f30d0)
                        (not E0x7ffe244f3390)))))
      (a!7 (or (and v0x7ffe244f3f10_0
                    v0x7ffe244f41d0_0
                    (and (<= v0x7ffe244ef1d0_0 v0x7ffe244f1390_0)
                         (>= v0x7ffe244ef1d0_0 v0x7ffe244f1390_0))
                    (and (<= v0x7ffe244ef2d0_0 v0x7ffe244f2f50_0)
                         (>= v0x7ffe244ef2d0_0 v0x7ffe244f2f50_0))
                    (<= v0x7ffe244ef3d0_0 1.0)
                    (>= v0x7ffe244ef3d0_0 1.0)
                    (and (<= v0x7ffe244ec010_0 v0x7ffe244f3010_0)
                         (>= v0x7ffe244ec010_0 v0x7ffe244f3010_0)))
               (and v0x7ffe244f2e90_0
                    v0x7ffe244f3dd0_0
                    (and (<= v0x7ffe244ef1d0_0 v0x7ffe244f1390_0)
                         (>= v0x7ffe244ef1d0_0 v0x7ffe244f1390_0))
                    (and (<= v0x7ffe244ef2d0_0 v0x7ffe244f2f50_0)
                         (>= v0x7ffe244ef2d0_0 v0x7ffe244f2f50_0))
                    (<= v0x7ffe244ef3d0_0 0.0)
                    (>= v0x7ffe244ef3d0_0 0.0)
                    (and (<= v0x7ffe244ec010_0 v0x7ffe244f3010_0)
                         (>= v0x7ffe244ec010_0 v0x7ffe244f3010_0))))))
(let ((a!8 (and (=> v0x7ffe244efcd0_0
                    (and v0x7ffe244efa10_0
                         E0x7ffe244efd90
                         (not v0x7ffe244efb90_0)))
                (=> v0x7ffe244efcd0_0 E0x7ffe244efd90)
                a!1
                a!2
                (=> v0x7ffe244f0d90_0
                    (and v0x7ffe244f0410_0
                         E0x7ffe244f0e50
                         (not v0x7ffe244f0c50_0)))
                (=> v0x7ffe244f0d90_0 E0x7ffe244f0e50)
                a!3
                a!4
                (=> v0x7ffe244f1c50_0
                    (and v0x7ffe244f12d0_0 E0x7ffe244f1d10 v0x7ffe244f1b10_0))
                (=> v0x7ffe244f1c50_0 E0x7ffe244f1d10)
                (=> v0x7ffe244f2150_0
                    (and v0x7ffe244f12d0_0
                         E0x7ffe244f2210
                         (not v0x7ffe244f1b10_0)))
                (=> v0x7ffe244f2150_0 E0x7ffe244f2210)
                (=> v0x7ffe244f2c10_0
                    (and v0x7ffe244f2150_0 E0x7ffe244f2cd0 v0x7ffe244f2ad0_0))
                (=> v0x7ffe244f2c10_0 E0x7ffe244f2cd0)
                (=> v0x7ffe244f2e90_0 a!5)
                a!6
                (=> v0x7ffe244f3f10_0
                    (and v0x7ffe244f2e90_0
                         E0x7ffe244f3fd0
                         (not v0x7ffe244f3dd0_0)))
                (=> v0x7ffe244f3f10_0 E0x7ffe244f3fd0)
                a!7
                (= v0x7ffe244efb90_0 (= v0x7ffe244efad0_0 0.0))
                (= v0x7ffe244effd0_0 (< v0x7ffe244ef110_0 2.0))
                (= v0x7ffe244f0190_0 (ite v0x7ffe244effd0_0 1.0 0.0))
                (= v0x7ffe244f02d0_0 (+ v0x7ffe244f0190_0 v0x7ffe244ef110_0))
                (= v0x7ffe244f0c50_0 (= v0x7ffe244f0b90_0 0.0))
                (= v0x7ffe244f1050_0 (= v0x7ffe244eef90_0 0.0))
                (= v0x7ffe244f1190_0 (ite v0x7ffe244f1050_0 1.0 0.0))
                (= v0x7ffe244f1b10_0 (= v0x7ffe244ef310_0 0.0))
                (= v0x7ffe244f1ed0_0 (> v0x7ffe244f04d0_0 1.0))
                (= v0x7ffe244f2010_0
                   (ite v0x7ffe244f1ed0_0 1.0 v0x7ffe244ef310_0))
                (= v0x7ffe244f2410_0 (> v0x7ffe244f04d0_0 0.0))
                (= v0x7ffe244f2550_0 (+ v0x7ffe244f04d0_0 (- 1.0)))
                (= v0x7ffe244f2710_0
                   (ite v0x7ffe244f2410_0 v0x7ffe244f2550_0 v0x7ffe244f04d0_0))
                (= v0x7ffe244f2850_0 (= v0x7ffe244f1390_0 0.0))
                (= v0x7ffe244f2990_0 (= v0x7ffe244f2710_0 0.0))
                (= v0x7ffe244f2ad0_0 (and v0x7ffe244f2850_0 v0x7ffe244f2990_0))
                (= v0x7ffe244f3b90_0 (= v0x7ffe244f1390_0 0.0))
                (= v0x7ffe244f3c90_0 (= v0x7ffe244f3010_0 0.0))
                (= v0x7ffe244f3dd0_0 (or v0x7ffe244f3c90_0 v0x7ffe244f3b90_0))
                (= v0x7ffe244f41d0_0 (= v0x7ffe244ef210_0 0.0)))))
  (=> F0x7ffe244f50d0 a!8))))
(assert (=> F0x7ffe244f50d0 F0x7ffe244f5190))
(assert (let ((a!1 (=> v0x7ffe244f0410_0
               (or (and v0x7ffe244efcd0_0
                        E0x7ffe244f0590
                        (<= v0x7ffe244f04d0_0 v0x7ffe244f02d0_0)
                        (>= v0x7ffe244f04d0_0 v0x7ffe244f02d0_0))
                   (and v0x7ffe244efa10_0
                        E0x7ffe244f0750
                        v0x7ffe244efb90_0
                        (<= v0x7ffe244f04d0_0 v0x7ffe244ef110_0)
                        (>= v0x7ffe244f04d0_0 v0x7ffe244ef110_0)))))
      (a!2 (=> v0x7ffe244f0410_0
               (or (and E0x7ffe244f0590 (not E0x7ffe244f0750))
                   (and E0x7ffe244f0750 (not E0x7ffe244f0590)))))
      (a!3 (=> v0x7ffe244f12d0_0
               (or (and v0x7ffe244f0d90_0
                        E0x7ffe244f1450
                        (<= v0x7ffe244f1390_0 v0x7ffe244f1190_0)
                        (>= v0x7ffe244f1390_0 v0x7ffe244f1190_0))
                   (and v0x7ffe244f0410_0
                        E0x7ffe244f1610
                        v0x7ffe244f0c50_0
                        (<= v0x7ffe244f1390_0 v0x7ffe244eef90_0)
                        (>= v0x7ffe244f1390_0 v0x7ffe244eef90_0)))))
      (a!4 (=> v0x7ffe244f12d0_0
               (or (and E0x7ffe244f1450 (not E0x7ffe244f1610))
                   (and E0x7ffe244f1610 (not E0x7ffe244f1450)))))
      (a!5 (or (and v0x7ffe244f1c50_0
                    E0x7ffe244f30d0
                    (<= v0x7ffe244f2f50_0 v0x7ffe244f04d0_0)
                    (>= v0x7ffe244f2f50_0 v0x7ffe244f04d0_0)
                    (<= v0x7ffe244f3010_0 v0x7ffe244f2010_0)
                    (>= v0x7ffe244f3010_0 v0x7ffe244f2010_0))
               (and v0x7ffe244f2c10_0
                    E0x7ffe244f3390
                    (and (<= v0x7ffe244f2f50_0 v0x7ffe244f2710_0)
                         (>= v0x7ffe244f2f50_0 v0x7ffe244f2710_0))
                    (<= v0x7ffe244f3010_0 v0x7ffe244ef310_0)
                    (>= v0x7ffe244f3010_0 v0x7ffe244ef310_0))
               (and v0x7ffe244f2150_0
                    E0x7ffe244f3650
                    (not v0x7ffe244f2ad0_0)
                    (and (<= v0x7ffe244f2f50_0 v0x7ffe244f2710_0)
                         (>= v0x7ffe244f2f50_0 v0x7ffe244f2710_0))
                    (<= v0x7ffe244f3010_0 0.0)
                    (>= v0x7ffe244f3010_0 0.0))))
      (a!6 (=> v0x7ffe244f2e90_0
               (or (and E0x7ffe244f30d0
                        (not E0x7ffe244f3390)
                        (not E0x7ffe244f3650))
                   (and E0x7ffe244f3390
                        (not E0x7ffe244f30d0)
                        (not E0x7ffe244f3650))
                   (and E0x7ffe244f3650
                        (not E0x7ffe244f30d0)
                        (not E0x7ffe244f3390))))))
(let ((a!7 (and (=> v0x7ffe244efcd0_0
                    (and v0x7ffe244efa10_0
                         E0x7ffe244efd90
                         (not v0x7ffe244efb90_0)))
                (=> v0x7ffe244efcd0_0 E0x7ffe244efd90)
                a!1
                a!2
                (=> v0x7ffe244f0d90_0
                    (and v0x7ffe244f0410_0
                         E0x7ffe244f0e50
                         (not v0x7ffe244f0c50_0)))
                (=> v0x7ffe244f0d90_0 E0x7ffe244f0e50)
                a!3
                a!4
                (=> v0x7ffe244f1c50_0
                    (and v0x7ffe244f12d0_0 E0x7ffe244f1d10 v0x7ffe244f1b10_0))
                (=> v0x7ffe244f1c50_0 E0x7ffe244f1d10)
                (=> v0x7ffe244f2150_0
                    (and v0x7ffe244f12d0_0
                         E0x7ffe244f2210
                         (not v0x7ffe244f1b10_0)))
                (=> v0x7ffe244f2150_0 E0x7ffe244f2210)
                (=> v0x7ffe244f2c10_0
                    (and v0x7ffe244f2150_0 E0x7ffe244f2cd0 v0x7ffe244f2ad0_0))
                (=> v0x7ffe244f2c10_0 E0x7ffe244f2cd0)
                (=> v0x7ffe244f2e90_0 a!5)
                a!6
                (=> v0x7ffe244f3f10_0
                    (and v0x7ffe244f2e90_0
                         E0x7ffe244f3fd0
                         (not v0x7ffe244f3dd0_0)))
                (=> v0x7ffe244f3f10_0 E0x7ffe244f3fd0)
                v0x7ffe244f3f10_0
                (not v0x7ffe244f41d0_0)
                (= v0x7ffe244efb90_0 (= v0x7ffe244efad0_0 0.0))
                (= v0x7ffe244effd0_0 (< v0x7ffe244ef110_0 2.0))
                (= v0x7ffe244f0190_0 (ite v0x7ffe244effd0_0 1.0 0.0))
                (= v0x7ffe244f02d0_0 (+ v0x7ffe244f0190_0 v0x7ffe244ef110_0))
                (= v0x7ffe244f0c50_0 (= v0x7ffe244f0b90_0 0.0))
                (= v0x7ffe244f1050_0 (= v0x7ffe244eef90_0 0.0))
                (= v0x7ffe244f1190_0 (ite v0x7ffe244f1050_0 1.0 0.0))
                (= v0x7ffe244f1b10_0 (= v0x7ffe244ef310_0 0.0))
                (= v0x7ffe244f1ed0_0 (> v0x7ffe244f04d0_0 1.0))
                (= v0x7ffe244f2010_0
                   (ite v0x7ffe244f1ed0_0 1.0 v0x7ffe244ef310_0))
                (= v0x7ffe244f2410_0 (> v0x7ffe244f04d0_0 0.0))
                (= v0x7ffe244f2550_0 (+ v0x7ffe244f04d0_0 (- 1.0)))
                (= v0x7ffe244f2710_0
                   (ite v0x7ffe244f2410_0 v0x7ffe244f2550_0 v0x7ffe244f04d0_0))
                (= v0x7ffe244f2850_0 (= v0x7ffe244f1390_0 0.0))
                (= v0x7ffe244f2990_0 (= v0x7ffe244f2710_0 0.0))
                (= v0x7ffe244f2ad0_0 (and v0x7ffe244f2850_0 v0x7ffe244f2990_0))
                (= v0x7ffe244f3b90_0 (= v0x7ffe244f1390_0 0.0))
                (= v0x7ffe244f3c90_0 (= v0x7ffe244f3010_0 0.0))
                (= v0x7ffe244f3dd0_0 (or v0x7ffe244f3c90_0 v0x7ffe244f3b90_0))
                (= v0x7ffe244f41d0_0 (= v0x7ffe244ef210_0 0.0)))))
  (=> F0x7ffe244f5250 a!7))))
(assert (=> F0x7ffe244f5250 F0x7ffe244f5190))
(assert (=> F0x7ffe244f5390 (or F0x7ffe244f5090 F0x7ffe244f50d0)))
(assert (=> F0x7ffe244f5350 F0x7ffe244f5250))
(assert (=> pre!entry!0 (=> F0x7ffe244f4fd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7ffe244f5190 (>= v0x7ffe244ef210_0 0.0))))
(assert (let ((a!1 (=> F0x7ffe244f5190
               (or (<= v0x7ffe244ef210_0 0.0) (not (<= v0x7ffe244ef310_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7ffe244f5390 (>= v0x7ffe244ef3d0_0 0.0))))
(assert (let ((a!1 (=> F0x7ffe244f5390
               (or (<= v0x7ffe244ef3d0_0 0.0) (not (<= v0x7ffe244ec010_0 0.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb2.i.i35.i.i!0 (=> F0x7ffe244f5350 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i35.i.i!0) (not lemma!bb2.i.i35.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7ffe244f4fd0)
; (error: F0x7ffe244f5350)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
