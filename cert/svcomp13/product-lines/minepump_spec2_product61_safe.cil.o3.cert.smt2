(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun F0x7f9fb90e7f10 () Bool)
(declare-fun F0x7f9fb90e7f50 () Bool)
(declare-fun F0x7f9fb90e7d50 () Bool)
(declare-fun v0x7f9fb90e1910_0 () Real)
(declare-fun v0x7f9fb90e6750_0 () Bool)
(declare-fun v0x7f9fb90e4a10_0 () Real)
(declare-fun v0x7f9fb90e3650_0 () Bool)
(declare-fun v0x7f9fb90e2790_0 () Real)
(declare-fun v0x7f9fb90e6990_0 () Bool)
(declare-fun E0x7f9fb90e6b90 () Bool)
(declare-fun E0x7f9fb90e6190 () Bool)
(declare-fun v0x7f9fb90e1590_0 () Real)
(declare-fun E0x7f9fb90e5d10 () Bool)
(declare-fun v0x7f9fb90e5990_0 () Real)
(declare-fun F0x7f9fb90e7e10 () Bool)
(declare-fun v0x7f9fb90e58d0_0 () Real)
(declare-fun v0x7f9fb90e5810_0 () Bool)
(declare-fun v0x7f9fb90e4f90_0 () Bool)
(declare-fun v0x7f9fb90e50d0_0 () Bool)
(declare-fun v0x7f9fb90e6ad0_0 () Bool)
(declare-fun v0x7f9fb90e4610_0 () Bool)
(declare-fun E0x7f9fb90e46d0 () Bool)
(declare-fun v0x7f9fb90e4d10_0 () Bool)
(declare-fun v0x7f9fb90e4110_0 () Bool)
(declare-fun E0x7f9fb90e4310 () Bool)
(declare-fun E0x7f9fb90e3c10 () Bool)
(declare-fun v0x7f9fb90e4bd0_0 () Real)
(declare-fun v0x7f9fb90e44d0_0 () Bool)
(declare-fun v0x7f9fb90e3990_0 () Real)
(declare-fun v0x7f9fb90e38d0_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f9fb90e5a50 () Bool)
(declare-fun v0x7f9fb90e1810_0 () Real)
(declare-fun v0x7f9fb90e2ad0_0 () Real)
(declare-fun v0x7f9fb90e1710_0 () Real)
(declare-fun v0x7f9fb90e5450_0 () Real)
(declare-fun v0x7f9fb90e3250_0 () Bool)
(declare-fun v0x7f9fb90e3390_0 () Bool)
(declare-fun v0x7f9fb90e2190_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f9fb90e2390 () Bool)
(declare-fun E0x7f9fb90e5190 () Bool)
(declare-fun v0x7f9fb90e2010_0 () Bool)
(declare-fun v0x7f9fb90e22d0_0 () Bool)
(declare-fun E0x7f9fb90e5650 () Bool)
(declare-fun v0x7f9fb90e5350_0 () Bool)
(declare-fun v0x7f9fb90e28d0_0 () Real)
(declare-fun v0x7f9fb90e3190_0 () Real)
(declare-fun v0x7f9fb90e4250_0 () Bool)
(declare-fun v0x7f9fb90e2a10_0 () Bool)
(declare-fun v0x7f9fb90e48d0_0 () Bool)
(declare-fun E0x7f9fb90e5f90 () Bool)
(declare-fun v0x7f9fb90e6850_0 () Bool)
(declare-fun E0x7f9fb90e2d50 () Bool)
(declare-fun F0x7f9fb90e7c90 () Bool)
(declare-fun v0x7f9fb90e4e50_0 () Bool)
(declare-fun v0x7f9fb90e6d90_0 () Bool)
(declare-fun v0x7f9fb90e20d0_0 () Real)
(declare-fun E0x7f9fb90e3450 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f9fb90e7bd0 () Bool)
(declare-fun v0x7f9fb90e25d0_0 () Bool)
(declare-fun v0x7f9fb90de010_0 () Real)
(declare-fun v0x7f9fb90e3790_0 () Real)
(declare-fun v0x7f9fb90e19d0_0 () Real)
(declare-fun v0x7f9fb90e18d0_0 () Real)
(declare-fun v0x7f9fb90de110_0 () Bool)
(declare-fun E0x7f9fb90e2b90 () Bool)
(declare-fun lemma!bb2.i.i43.i.i!0 () Bool)
(declare-fun v0x7f9fb90e5590_0 () Bool)
(declare-fun E0x7f9fb90e3a50 () Bool)
(declare-fun post!bb2.i.i43.i.i!0 () Bool)
(declare-fun v0x7f9fb90e17d0_0 () Real)
(declare-fun F0x7f9fb90e7b10 () Bool)

(assert (=> F0x7f9fb90e7b10
    (and v0x7f9fb90de110_0
         (<= v0x7f9fb90e17d0_0 0.0)
         (>= v0x7f9fb90e17d0_0 0.0)
         (<= v0x7f9fb90e18d0_0 0.0)
         (>= v0x7f9fb90e18d0_0 0.0)
         (<= v0x7f9fb90e19d0_0 1.0)
         (>= v0x7f9fb90e19d0_0 1.0)
         (<= v0x7f9fb90de010_0 0.0)
         (>= v0x7f9fb90de010_0 0.0))))
(assert (=> F0x7f9fb90e7b10 F0x7f9fb90e7bd0))
(assert (let ((a!1 (=> v0x7f9fb90e2a10_0
               (or (and v0x7f9fb90e22d0_0
                        E0x7f9fb90e2b90
                        (<= v0x7f9fb90e2ad0_0 v0x7f9fb90e28d0_0)
                        (>= v0x7f9fb90e2ad0_0 v0x7f9fb90e28d0_0))
                   (and v0x7f9fb90e2010_0
                        E0x7f9fb90e2d50
                        v0x7f9fb90e2190_0
                        (<= v0x7f9fb90e2ad0_0 v0x7f9fb90e1810_0)
                        (>= v0x7f9fb90e2ad0_0 v0x7f9fb90e1810_0)))))
      (a!2 (=> v0x7f9fb90e2a10_0
               (or (and E0x7f9fb90e2b90 (not E0x7f9fb90e2d50))
                   (and E0x7f9fb90e2d50 (not E0x7f9fb90e2b90)))))
      (a!3 (=> v0x7f9fb90e38d0_0
               (or (and v0x7f9fb90e3390_0
                        E0x7f9fb90e3a50
                        (<= v0x7f9fb90e3990_0 v0x7f9fb90e3790_0)
                        (>= v0x7f9fb90e3990_0 v0x7f9fb90e3790_0))
                   (and v0x7f9fb90e2a10_0
                        E0x7f9fb90e3c10
                        v0x7f9fb90e3250_0
                        (<= v0x7f9fb90e3990_0 v0x7f9fb90e1710_0)
                        (>= v0x7f9fb90e3990_0 v0x7f9fb90e1710_0)))))
      (a!4 (=> v0x7f9fb90e38d0_0
               (or (and E0x7f9fb90e3a50 (not E0x7f9fb90e3c10))
                   (and E0x7f9fb90e3c10 (not E0x7f9fb90e3a50)))))
      (a!5 (or (and v0x7f9fb90e50d0_0
                    E0x7f9fb90e5a50
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0))
                    (<= v0x7f9fb90e5990_0 v0x7f9fb90e5450_0)
                    (>= v0x7f9fb90e5990_0 v0x7f9fb90e5450_0))
               (and v0x7f9fb90e4250_0
                    E0x7f9fb90e5d10
                    (not v0x7f9fb90e44d0_0)
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0))
                    (and (<= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)
                         (>= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)))
               (and v0x7f9fb90e5590_0
                    E0x7f9fb90e5f90
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0))
                    (and (<= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)
                         (>= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)))
               (and v0x7f9fb90e4610_0
                    E0x7f9fb90e6190
                    (not v0x7f9fb90e4f90_0)
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0))
                    (<= v0x7f9fb90e5990_0 0.0)
                    (>= v0x7f9fb90e5990_0 0.0))))
      (a!6 (=> v0x7f9fb90e5810_0
               (or (and E0x7f9fb90e5a50
                        (not E0x7f9fb90e5d10)
                        (not E0x7f9fb90e5f90)
                        (not E0x7f9fb90e6190))
                   (and E0x7f9fb90e5d10
                        (not E0x7f9fb90e5a50)
                        (not E0x7f9fb90e5f90)
                        (not E0x7f9fb90e6190))
                   (and E0x7f9fb90e5f90
                        (not E0x7f9fb90e5a50)
                        (not E0x7f9fb90e5d10)
                        (not E0x7f9fb90e6190))
                   (and E0x7f9fb90e6190
                        (not E0x7f9fb90e5a50)
                        (not E0x7f9fb90e5d10)
                        (not E0x7f9fb90e5f90)))))
      (a!7 (or (and v0x7f9fb90e6ad0_0
                    v0x7f9fb90e6d90_0
                    (and (<= v0x7f9fb90e17d0_0 v0x7f9fb90e5990_0)
                         (>= v0x7f9fb90e17d0_0 v0x7f9fb90e5990_0))
                    (and (<= v0x7f9fb90e18d0_0 v0x7f9fb90e3990_0)
                         (>= v0x7f9fb90e18d0_0 v0x7f9fb90e3990_0))
                    (and (<= v0x7f9fb90e19d0_0 v0x7f9fb90e58d0_0)
                         (>= v0x7f9fb90e19d0_0 v0x7f9fb90e58d0_0))
                    (<= v0x7f9fb90de010_0 1.0)
                    (>= v0x7f9fb90de010_0 1.0))
               (and v0x7f9fb90e5810_0
                    v0x7f9fb90e6990_0
                    (and (<= v0x7f9fb90e17d0_0 v0x7f9fb90e5990_0)
                         (>= v0x7f9fb90e17d0_0 v0x7f9fb90e5990_0))
                    (and (<= v0x7f9fb90e18d0_0 v0x7f9fb90e3990_0)
                         (>= v0x7f9fb90e18d0_0 v0x7f9fb90e3990_0))
                    (and (<= v0x7f9fb90e19d0_0 v0x7f9fb90e58d0_0)
                         (>= v0x7f9fb90e19d0_0 v0x7f9fb90e58d0_0))
                    (<= v0x7f9fb90de010_0 0.0)
                    (>= v0x7f9fb90de010_0 0.0)))))
(let ((a!8 (and (=> v0x7f9fb90e22d0_0
                    (and v0x7f9fb90e2010_0
                         E0x7f9fb90e2390
                         (not v0x7f9fb90e2190_0)))
                (=> v0x7f9fb90e22d0_0 E0x7f9fb90e2390)
                a!1
                a!2
                (=> v0x7f9fb90e3390_0
                    (and v0x7f9fb90e2a10_0
                         E0x7f9fb90e3450
                         (not v0x7f9fb90e3250_0)))
                (=> v0x7f9fb90e3390_0 E0x7f9fb90e3450)
                a!3
                a!4
                (=> v0x7f9fb90e4250_0
                    (and v0x7f9fb90e38d0_0 E0x7f9fb90e4310 v0x7f9fb90e4110_0))
                (=> v0x7f9fb90e4250_0 E0x7f9fb90e4310)
                (=> v0x7f9fb90e4610_0
                    (and v0x7f9fb90e38d0_0
                         E0x7f9fb90e46d0
                         (not v0x7f9fb90e4110_0)))
                (=> v0x7f9fb90e4610_0 E0x7f9fb90e46d0)
                (=> v0x7f9fb90e50d0_0
                    (and v0x7f9fb90e4250_0 E0x7f9fb90e5190 v0x7f9fb90e44d0_0))
                (=> v0x7f9fb90e50d0_0 E0x7f9fb90e5190)
                (=> v0x7f9fb90e5590_0
                    (and v0x7f9fb90e4610_0 E0x7f9fb90e5650 v0x7f9fb90e4f90_0))
                (=> v0x7f9fb90e5590_0 E0x7f9fb90e5650)
                (=> v0x7f9fb90e5810_0 a!5)
                a!6
                (=> v0x7f9fb90e6ad0_0
                    (and v0x7f9fb90e5810_0
                         E0x7f9fb90e6b90
                         (not v0x7f9fb90e6990_0)))
                (=> v0x7f9fb90e6ad0_0 E0x7f9fb90e6b90)
                a!7
                (= v0x7f9fb90e2190_0 (= v0x7f9fb90e20d0_0 0.0))
                (= v0x7f9fb90e25d0_0 (< v0x7f9fb90e1810_0 2.0))
                (= v0x7f9fb90e2790_0 (ite v0x7f9fb90e25d0_0 1.0 0.0))
                (= v0x7f9fb90e28d0_0 (+ v0x7f9fb90e2790_0 v0x7f9fb90e1810_0))
                (= v0x7f9fb90e3250_0 (= v0x7f9fb90e3190_0 0.0))
                (= v0x7f9fb90e3650_0 (= v0x7f9fb90e1710_0 0.0))
                (= v0x7f9fb90e3790_0 (ite v0x7f9fb90e3650_0 1.0 0.0))
                (= v0x7f9fb90e4110_0 (= v0x7f9fb90e1590_0 0.0))
                (= v0x7f9fb90e44d0_0 (> v0x7f9fb90e2ad0_0 1.0))
                (= v0x7f9fb90e48d0_0 (> v0x7f9fb90e2ad0_0 0.0))
                (= v0x7f9fb90e4a10_0 (+ v0x7f9fb90e2ad0_0 (- 1.0)))
                (= v0x7f9fb90e4bd0_0
                   (ite v0x7f9fb90e48d0_0 v0x7f9fb90e4a10_0 v0x7f9fb90e2ad0_0))
                (= v0x7f9fb90e4d10_0 (= v0x7f9fb90e3990_0 0.0))
                (= v0x7f9fb90e4e50_0 (= v0x7f9fb90e4bd0_0 0.0))
                (= v0x7f9fb90e4f90_0 (and v0x7f9fb90e4d10_0 v0x7f9fb90e4e50_0))
                (= v0x7f9fb90e5350_0 (= v0x7f9fb90e3990_0 0.0))
                (= v0x7f9fb90e5450_0
                   (ite v0x7f9fb90e5350_0 1.0 v0x7f9fb90e1590_0))
                (= v0x7f9fb90e6750_0 (= v0x7f9fb90e3990_0 0.0))
                (= v0x7f9fb90e6850_0 (= v0x7f9fb90e5990_0 0.0))
                (= v0x7f9fb90e6990_0 (or v0x7f9fb90e6850_0 v0x7f9fb90e6750_0))
                (= v0x7f9fb90e6d90_0 (= v0x7f9fb90e1910_0 0.0)))))
  (=> F0x7f9fb90e7c90 a!8))))
(assert (=> F0x7f9fb90e7c90 F0x7f9fb90e7d50))
(assert (let ((a!1 (=> v0x7f9fb90e2a10_0
               (or (and v0x7f9fb90e22d0_0
                        E0x7f9fb90e2b90
                        (<= v0x7f9fb90e2ad0_0 v0x7f9fb90e28d0_0)
                        (>= v0x7f9fb90e2ad0_0 v0x7f9fb90e28d0_0))
                   (and v0x7f9fb90e2010_0
                        E0x7f9fb90e2d50
                        v0x7f9fb90e2190_0
                        (<= v0x7f9fb90e2ad0_0 v0x7f9fb90e1810_0)
                        (>= v0x7f9fb90e2ad0_0 v0x7f9fb90e1810_0)))))
      (a!2 (=> v0x7f9fb90e2a10_0
               (or (and E0x7f9fb90e2b90 (not E0x7f9fb90e2d50))
                   (and E0x7f9fb90e2d50 (not E0x7f9fb90e2b90)))))
      (a!3 (=> v0x7f9fb90e38d0_0
               (or (and v0x7f9fb90e3390_0
                        E0x7f9fb90e3a50
                        (<= v0x7f9fb90e3990_0 v0x7f9fb90e3790_0)
                        (>= v0x7f9fb90e3990_0 v0x7f9fb90e3790_0))
                   (and v0x7f9fb90e2a10_0
                        E0x7f9fb90e3c10
                        v0x7f9fb90e3250_0
                        (<= v0x7f9fb90e3990_0 v0x7f9fb90e1710_0)
                        (>= v0x7f9fb90e3990_0 v0x7f9fb90e1710_0)))))
      (a!4 (=> v0x7f9fb90e38d0_0
               (or (and E0x7f9fb90e3a50 (not E0x7f9fb90e3c10))
                   (and E0x7f9fb90e3c10 (not E0x7f9fb90e3a50)))))
      (a!5 (or (and v0x7f9fb90e50d0_0
                    E0x7f9fb90e5a50
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0))
                    (<= v0x7f9fb90e5990_0 v0x7f9fb90e5450_0)
                    (>= v0x7f9fb90e5990_0 v0x7f9fb90e5450_0))
               (and v0x7f9fb90e4250_0
                    E0x7f9fb90e5d10
                    (not v0x7f9fb90e44d0_0)
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e2ad0_0))
                    (and (<= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)
                         (>= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)))
               (and v0x7f9fb90e5590_0
                    E0x7f9fb90e5f90
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0))
                    (and (<= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)
                         (>= v0x7f9fb90e5990_0 v0x7f9fb90e1590_0)))
               (and v0x7f9fb90e4610_0
                    E0x7f9fb90e6190
                    (not v0x7f9fb90e4f90_0)
                    (and (<= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0)
                         (>= v0x7f9fb90e58d0_0 v0x7f9fb90e4bd0_0))
                    (<= v0x7f9fb90e5990_0 0.0)
                    (>= v0x7f9fb90e5990_0 0.0))))
      (a!6 (=> v0x7f9fb90e5810_0
               (or (and E0x7f9fb90e5a50
                        (not E0x7f9fb90e5d10)
                        (not E0x7f9fb90e5f90)
                        (not E0x7f9fb90e6190))
                   (and E0x7f9fb90e5d10
                        (not E0x7f9fb90e5a50)
                        (not E0x7f9fb90e5f90)
                        (not E0x7f9fb90e6190))
                   (and E0x7f9fb90e5f90
                        (not E0x7f9fb90e5a50)
                        (not E0x7f9fb90e5d10)
                        (not E0x7f9fb90e6190))
                   (and E0x7f9fb90e6190
                        (not E0x7f9fb90e5a50)
                        (not E0x7f9fb90e5d10)
                        (not E0x7f9fb90e5f90))))))
(let ((a!7 (and (=> v0x7f9fb90e22d0_0
                    (and v0x7f9fb90e2010_0
                         E0x7f9fb90e2390
                         (not v0x7f9fb90e2190_0)))
                (=> v0x7f9fb90e22d0_0 E0x7f9fb90e2390)
                a!1
                a!2
                (=> v0x7f9fb90e3390_0
                    (and v0x7f9fb90e2a10_0
                         E0x7f9fb90e3450
                         (not v0x7f9fb90e3250_0)))
                (=> v0x7f9fb90e3390_0 E0x7f9fb90e3450)
                a!3
                a!4
                (=> v0x7f9fb90e4250_0
                    (and v0x7f9fb90e38d0_0 E0x7f9fb90e4310 v0x7f9fb90e4110_0))
                (=> v0x7f9fb90e4250_0 E0x7f9fb90e4310)
                (=> v0x7f9fb90e4610_0
                    (and v0x7f9fb90e38d0_0
                         E0x7f9fb90e46d0
                         (not v0x7f9fb90e4110_0)))
                (=> v0x7f9fb90e4610_0 E0x7f9fb90e46d0)
                (=> v0x7f9fb90e50d0_0
                    (and v0x7f9fb90e4250_0 E0x7f9fb90e5190 v0x7f9fb90e44d0_0))
                (=> v0x7f9fb90e50d0_0 E0x7f9fb90e5190)
                (=> v0x7f9fb90e5590_0
                    (and v0x7f9fb90e4610_0 E0x7f9fb90e5650 v0x7f9fb90e4f90_0))
                (=> v0x7f9fb90e5590_0 E0x7f9fb90e5650)
                (=> v0x7f9fb90e5810_0 a!5)
                a!6
                (=> v0x7f9fb90e6ad0_0
                    (and v0x7f9fb90e5810_0
                         E0x7f9fb90e6b90
                         (not v0x7f9fb90e6990_0)))
                (=> v0x7f9fb90e6ad0_0 E0x7f9fb90e6b90)
                v0x7f9fb90e6ad0_0
                (not v0x7f9fb90e6d90_0)
                (= v0x7f9fb90e2190_0 (= v0x7f9fb90e20d0_0 0.0))
                (= v0x7f9fb90e25d0_0 (< v0x7f9fb90e1810_0 2.0))
                (= v0x7f9fb90e2790_0 (ite v0x7f9fb90e25d0_0 1.0 0.0))
                (= v0x7f9fb90e28d0_0 (+ v0x7f9fb90e2790_0 v0x7f9fb90e1810_0))
                (= v0x7f9fb90e3250_0 (= v0x7f9fb90e3190_0 0.0))
                (= v0x7f9fb90e3650_0 (= v0x7f9fb90e1710_0 0.0))
                (= v0x7f9fb90e3790_0 (ite v0x7f9fb90e3650_0 1.0 0.0))
                (= v0x7f9fb90e4110_0 (= v0x7f9fb90e1590_0 0.0))
                (= v0x7f9fb90e44d0_0 (> v0x7f9fb90e2ad0_0 1.0))
                (= v0x7f9fb90e48d0_0 (> v0x7f9fb90e2ad0_0 0.0))
                (= v0x7f9fb90e4a10_0 (+ v0x7f9fb90e2ad0_0 (- 1.0)))
                (= v0x7f9fb90e4bd0_0
                   (ite v0x7f9fb90e48d0_0 v0x7f9fb90e4a10_0 v0x7f9fb90e2ad0_0))
                (= v0x7f9fb90e4d10_0 (= v0x7f9fb90e3990_0 0.0))
                (= v0x7f9fb90e4e50_0 (= v0x7f9fb90e4bd0_0 0.0))
                (= v0x7f9fb90e4f90_0 (and v0x7f9fb90e4d10_0 v0x7f9fb90e4e50_0))
                (= v0x7f9fb90e5350_0 (= v0x7f9fb90e3990_0 0.0))
                (= v0x7f9fb90e5450_0
                   (ite v0x7f9fb90e5350_0 1.0 v0x7f9fb90e1590_0))
                (= v0x7f9fb90e6750_0 (= v0x7f9fb90e3990_0 0.0))
                (= v0x7f9fb90e6850_0 (= v0x7f9fb90e5990_0 0.0))
                (= v0x7f9fb90e6990_0 (or v0x7f9fb90e6850_0 v0x7f9fb90e6750_0))
                (= v0x7f9fb90e6d90_0 (= v0x7f9fb90e1910_0 0.0)))))
  (=> F0x7f9fb90e7e10 a!7))))
(assert (=> F0x7f9fb90e7e10 F0x7f9fb90e7d50))
(assert (=> F0x7f9fb90e7f50 (or F0x7f9fb90e7b10 F0x7f9fb90e7c90)))
(assert (=> F0x7f9fb90e7f10 F0x7f9fb90e7e10))
(assert (=> pre!entry!0 (=> F0x7f9fb90e7bd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f9fb90e7d50 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f9fb90e7f50 true)))
(assert (= lemma!bb2.i.i43.i.i!0 (=> F0x7f9fb90e7f10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i43.i.i!0) (not lemma!bb2.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f9fb90e7bd0)
; (error: F0x7f9fb90e7f10)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i43.i.i!0)
