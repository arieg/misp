(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i29.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f000d3df4d0 () Bool)
(declare-fun F0x7f000d3df390 () Bool)
(declare-fun F0x7f000d3df2d0 () Bool)
(declare-fun v0x7f000d3ddcd0_0 () Bool)
(declare-fun v0x7f000d3dddd0_0 () Bool)
(declare-fun v0x7f000d3dc0d0_0 () Bool)
(declare-fun v0x7f000d3dae50_0 () Bool)
(declare-fun v0x7f000d3dc210_0 () Real)
(declare-fun v0x7f000d3dc8d0_0 () Bool)
(declare-fun v0x7f000d3d98d0_0 () Real)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f000d3de310_0 () Bool)
(declare-fun v0x7f000d3ddf10_0 () Bool)
(declare-fun E0x7f000d3de110 () Bool)
(declare-fun v0x7f000d3d8d90_0 () Real)
(declare-fun E0x7f000d3dd510 () Bool)
(declare-fun v0x7f000d3d9110_0 () Real)
(declare-fun v0x7f000d3dc9d0_0 () Real)
(declare-fun v0x7f000d3dce50_0 () Real)
(declare-fun E0x7f000d3dcbd0 () Bool)
(declare-fun E0x7f000d3dcfd0 () Bool)
(declare-fun E0x7f000d3dbed0 () Bool)
(declare-fun v0x7f000d3dcb10_0 () Bool)
(declare-fun v0x7f000d3dba50_0 () Bool)
(declare-fun E0x7f000d3db410 () Bool)
(declare-fun v0x7f000d3dcf10_0 () Real)
(declare-fun v0x7f000d3de050_0 () Bool)
(declare-fun v0x7f000d3db910_0 () Bool)
(declare-fun E0x7f000d3db250 () Bool)
(declare-fun v0x7f000d3dc3d0_0 () Real)
(declare-fun v0x7f000d3d9010_0 () Real)
(declare-fun E0x7f000d3da550 () Bool)
(declare-fun v0x7f000d3da990_0 () Real)
(declare-fun v0x7f000d3db0d0_0 () Bool)
(declare-fun v0x7f000d3daa50_0 () Bool)
(declare-fun v0x7f000d3dcd90_0 () Bool)
(declare-fun E0x7f000d3da390 () Bool)
(declare-fun v0x7f000d3da210_0 () Bool)
(declare-fun v0x7f000d3d9990_0 () Bool)
(declare-fun E0x7f000d3dac50 () Bool)
(declare-fun E0x7f000d3d9b90 () Bool)
(declare-fun F0x7f000d3df210 () Bool)
(declare-fun v0x7f000d3d9ad0_0 () Bool)
(declare-fun F0x7f000d3df150 () Bool)
(declare-fun E0x7f000d3dc710 () Bool)
(declare-fun v0x7f000d3dab90_0 () Bool)
(declare-fun v0x7f000d3da0d0_0 () Real)
(declare-fun v0x7f000d3dbe10_0 () Bool)
(declare-fun post!bb2.i.i29.i.i!0 () Bool)
(declare-fun v0x7f000d3daf90_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f000d3d7010_0 () Real)
(declare-fun v0x7f000d3dbcd0_0 () Bool)
(declare-fun v0x7f000d3d8f10_0 () Real)
(declare-fun F0x7f000d3df490 () Bool)
(declare-fun v0x7f000d3d90d0_0 () Real)
(declare-fun v0x7f000d3dc650_0 () Bool)
(declare-fun v0x7f000d3d9f90_0 () Real)
(declare-fun v0x7f000d3d8fd0_0 () Real)
(declare-fun v0x7f000d3dc510_0 () Bool)
(declare-fun E0x7f000d3dbb10 () Bool)
(declare-fun v0x7f000d3d91d0_0 () Real)
(declare-fun v0x7f000d3d7110_0 () Bool)
(declare-fun v0x7f000d3da2d0_0 () Real)
(declare-fun v0x7f000d3db190_0 () Real)
(declare-fun E0x7f000d3dd290 () Bool)
(declare-fun v0x7f000d3d9dd0_0 () Bool)
(declare-fun F0x7f000d3df090 () Bool)
(declare-fun E0x7f000d3dd710 () Bool)
(declare-fun v0x7f000d3d9810_0 () Bool)

(assert (=> F0x7f000d3df090
    (and v0x7f000d3d7110_0
         (<= v0x7f000d3d8fd0_0 0.0)
         (>= v0x7f000d3d8fd0_0 0.0)
         (<= v0x7f000d3d90d0_0 0.0)
         (>= v0x7f000d3d90d0_0 0.0)
         (<= v0x7f000d3d91d0_0 1.0)
         (>= v0x7f000d3d91d0_0 1.0)
         (<= v0x7f000d3d7010_0 0.0)
         (>= v0x7f000d3d7010_0 0.0))))
(assert (=> F0x7f000d3df090 F0x7f000d3df150))
(assert (let ((a!1 (=> v0x7f000d3da210_0
               (or (and v0x7f000d3d9ad0_0
                        E0x7f000d3da390
                        (<= v0x7f000d3da2d0_0 v0x7f000d3da0d0_0)
                        (>= v0x7f000d3da2d0_0 v0x7f000d3da0d0_0))
                   (and v0x7f000d3d9810_0
                        E0x7f000d3da550
                        v0x7f000d3d9990_0
                        (<= v0x7f000d3da2d0_0 v0x7f000d3d9010_0)
                        (>= v0x7f000d3da2d0_0 v0x7f000d3d9010_0)))))
      (a!2 (=> v0x7f000d3da210_0
               (or (and E0x7f000d3da390 (not E0x7f000d3da550))
                   (and E0x7f000d3da550 (not E0x7f000d3da390)))))
      (a!3 (=> v0x7f000d3db0d0_0
               (or (and v0x7f000d3dab90_0
                        E0x7f000d3db250
                        (<= v0x7f000d3db190_0 v0x7f000d3daf90_0)
                        (>= v0x7f000d3db190_0 v0x7f000d3daf90_0))
                   (and v0x7f000d3da210_0
                        E0x7f000d3db410
                        v0x7f000d3daa50_0
                        (<= v0x7f000d3db190_0 v0x7f000d3d8f10_0)
                        (>= v0x7f000d3db190_0 v0x7f000d3d8f10_0)))))
      (a!4 (=> v0x7f000d3db0d0_0
               (or (and E0x7f000d3db250 (not E0x7f000d3db410))
                   (and E0x7f000d3db410 (not E0x7f000d3db250)))))
      (a!5 (or (and v0x7f000d3dc650_0
                    E0x7f000d3dcfd0
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3da2d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3da2d0_0))
                    (<= v0x7f000d3dcf10_0 v0x7f000d3dc9d0_0)
                    (>= v0x7f000d3dcf10_0 v0x7f000d3dc9d0_0))
               (and v0x7f000d3dba50_0
                    E0x7f000d3dd290
                    (not v0x7f000d3dbcd0_0)
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3da2d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3da2d0_0))
                    (and (<= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)
                         (>= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)))
               (and v0x7f000d3dcb10_0
                    E0x7f000d3dd510
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0))
                    (and (<= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)
                         (>= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)))
               (and v0x7f000d3dbe10_0
                    E0x7f000d3dd710
                    (not v0x7f000d3dc510_0)
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0))
                    (<= v0x7f000d3dcf10_0 0.0)
                    (>= v0x7f000d3dcf10_0 0.0))))
      (a!6 (=> v0x7f000d3dcd90_0
               (or (and E0x7f000d3dcfd0
                        (not E0x7f000d3dd290)
                        (not E0x7f000d3dd510)
                        (not E0x7f000d3dd710))
                   (and E0x7f000d3dd290
                        (not E0x7f000d3dcfd0)
                        (not E0x7f000d3dd510)
                        (not E0x7f000d3dd710))
                   (and E0x7f000d3dd510
                        (not E0x7f000d3dcfd0)
                        (not E0x7f000d3dd290)
                        (not E0x7f000d3dd710))
                   (and E0x7f000d3dd710
                        (not E0x7f000d3dcfd0)
                        (not E0x7f000d3dd290)
                        (not E0x7f000d3dd510)))))
      (a!7 (or (and v0x7f000d3de050_0
                    v0x7f000d3de310_0
                    (<= v0x7f000d3d8fd0_0 1.0)
                    (>= v0x7f000d3d8fd0_0 1.0)
                    (and (<= v0x7f000d3d90d0_0 v0x7f000d3db190_0)
                         (>= v0x7f000d3d90d0_0 v0x7f000d3db190_0))
                    (and (<= v0x7f000d3d91d0_0 v0x7f000d3dce50_0)
                         (>= v0x7f000d3d91d0_0 v0x7f000d3dce50_0))
                    (and (<= v0x7f000d3d7010_0 v0x7f000d3dcf10_0)
                         (>= v0x7f000d3d7010_0 v0x7f000d3dcf10_0)))
               (and v0x7f000d3dcd90_0
                    v0x7f000d3ddf10_0
                    (<= v0x7f000d3d8fd0_0 0.0)
                    (>= v0x7f000d3d8fd0_0 0.0)
                    (and (<= v0x7f000d3d90d0_0 v0x7f000d3db190_0)
                         (>= v0x7f000d3d90d0_0 v0x7f000d3db190_0))
                    (and (<= v0x7f000d3d91d0_0 v0x7f000d3dce50_0)
                         (>= v0x7f000d3d91d0_0 v0x7f000d3dce50_0))
                    (and (<= v0x7f000d3d7010_0 v0x7f000d3dcf10_0)
                         (>= v0x7f000d3d7010_0 v0x7f000d3dcf10_0))))))
(let ((a!8 (and (=> v0x7f000d3d9ad0_0
                    (and v0x7f000d3d9810_0
                         E0x7f000d3d9b90
                         (not v0x7f000d3d9990_0)))
                (=> v0x7f000d3d9ad0_0 E0x7f000d3d9b90)
                a!1
                a!2
                (=> v0x7f000d3dab90_0
                    (and v0x7f000d3da210_0
                         E0x7f000d3dac50
                         (not v0x7f000d3daa50_0)))
                (=> v0x7f000d3dab90_0 E0x7f000d3dac50)
                a!3
                a!4
                (=> v0x7f000d3dba50_0
                    (and v0x7f000d3db0d0_0 E0x7f000d3dbb10 v0x7f000d3db910_0))
                (=> v0x7f000d3dba50_0 E0x7f000d3dbb10)
                (=> v0x7f000d3dbe10_0
                    (and v0x7f000d3db0d0_0
                         E0x7f000d3dbed0
                         (not v0x7f000d3db910_0)))
                (=> v0x7f000d3dbe10_0 E0x7f000d3dbed0)
                (=> v0x7f000d3dc650_0
                    (and v0x7f000d3dba50_0 E0x7f000d3dc710 v0x7f000d3dbcd0_0))
                (=> v0x7f000d3dc650_0 E0x7f000d3dc710)
                (=> v0x7f000d3dcb10_0
                    (and v0x7f000d3dbe10_0 E0x7f000d3dcbd0 v0x7f000d3dc510_0))
                (=> v0x7f000d3dcb10_0 E0x7f000d3dcbd0)
                (=> v0x7f000d3dcd90_0 a!5)
                a!6
                (=> v0x7f000d3de050_0
                    (and v0x7f000d3dcd90_0
                         E0x7f000d3de110
                         (not v0x7f000d3ddf10_0)))
                (=> v0x7f000d3de050_0 E0x7f000d3de110)
                a!7
                (= v0x7f000d3d9990_0 (= v0x7f000d3d98d0_0 0.0))
                (= v0x7f000d3d9dd0_0 (< v0x7f000d3d9010_0 2.0))
                (= v0x7f000d3d9f90_0 (ite v0x7f000d3d9dd0_0 1.0 0.0))
                (= v0x7f000d3da0d0_0 (+ v0x7f000d3d9f90_0 v0x7f000d3d9010_0))
                (= v0x7f000d3daa50_0 (= v0x7f000d3da990_0 0.0))
                (= v0x7f000d3dae50_0 (= v0x7f000d3d8f10_0 0.0))
                (= v0x7f000d3daf90_0 (ite v0x7f000d3dae50_0 1.0 0.0))
                (= v0x7f000d3db910_0 (= v0x7f000d3d9110_0 0.0))
                (= v0x7f000d3dbcd0_0 (> v0x7f000d3da2d0_0 1.0))
                (= v0x7f000d3dc0d0_0 (> v0x7f000d3da2d0_0 0.0))
                (= v0x7f000d3dc210_0 (+ v0x7f000d3da2d0_0 (- 1.0)))
                (= v0x7f000d3dc3d0_0
                   (ite v0x7f000d3dc0d0_0 v0x7f000d3dc210_0 v0x7f000d3da2d0_0))
                (= v0x7f000d3dc510_0 (= v0x7f000d3db190_0 0.0))
                (= v0x7f000d3dc8d0_0 (= v0x7f000d3db190_0 0.0))
                (= v0x7f000d3dc9d0_0
                   (ite v0x7f000d3dc8d0_0 1.0 v0x7f000d3d9110_0))
                (= v0x7f000d3ddcd0_0 (= v0x7f000d3db190_0 0.0))
                (= v0x7f000d3dddd0_0 (= v0x7f000d3dcf10_0 0.0))
                (= v0x7f000d3ddf10_0 (or v0x7f000d3dddd0_0 v0x7f000d3ddcd0_0))
                (= v0x7f000d3de310_0 (= v0x7f000d3d8d90_0 0.0)))))
  (=> F0x7f000d3df210 a!8))))
(assert (=> F0x7f000d3df210 F0x7f000d3df2d0))
(assert (let ((a!1 (=> v0x7f000d3da210_0
               (or (and v0x7f000d3d9ad0_0
                        E0x7f000d3da390
                        (<= v0x7f000d3da2d0_0 v0x7f000d3da0d0_0)
                        (>= v0x7f000d3da2d0_0 v0x7f000d3da0d0_0))
                   (and v0x7f000d3d9810_0
                        E0x7f000d3da550
                        v0x7f000d3d9990_0
                        (<= v0x7f000d3da2d0_0 v0x7f000d3d9010_0)
                        (>= v0x7f000d3da2d0_0 v0x7f000d3d9010_0)))))
      (a!2 (=> v0x7f000d3da210_0
               (or (and E0x7f000d3da390 (not E0x7f000d3da550))
                   (and E0x7f000d3da550 (not E0x7f000d3da390)))))
      (a!3 (=> v0x7f000d3db0d0_0
               (or (and v0x7f000d3dab90_0
                        E0x7f000d3db250
                        (<= v0x7f000d3db190_0 v0x7f000d3daf90_0)
                        (>= v0x7f000d3db190_0 v0x7f000d3daf90_0))
                   (and v0x7f000d3da210_0
                        E0x7f000d3db410
                        v0x7f000d3daa50_0
                        (<= v0x7f000d3db190_0 v0x7f000d3d8f10_0)
                        (>= v0x7f000d3db190_0 v0x7f000d3d8f10_0)))))
      (a!4 (=> v0x7f000d3db0d0_0
               (or (and E0x7f000d3db250 (not E0x7f000d3db410))
                   (and E0x7f000d3db410 (not E0x7f000d3db250)))))
      (a!5 (or (and v0x7f000d3dc650_0
                    E0x7f000d3dcfd0
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3da2d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3da2d0_0))
                    (<= v0x7f000d3dcf10_0 v0x7f000d3dc9d0_0)
                    (>= v0x7f000d3dcf10_0 v0x7f000d3dc9d0_0))
               (and v0x7f000d3dba50_0
                    E0x7f000d3dd290
                    (not v0x7f000d3dbcd0_0)
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3da2d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3da2d0_0))
                    (and (<= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)
                         (>= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)))
               (and v0x7f000d3dcb10_0
                    E0x7f000d3dd510
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0))
                    (and (<= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)
                         (>= v0x7f000d3dcf10_0 v0x7f000d3d9110_0)))
               (and v0x7f000d3dbe10_0
                    E0x7f000d3dd710
                    (not v0x7f000d3dc510_0)
                    (and (<= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0)
                         (>= v0x7f000d3dce50_0 v0x7f000d3dc3d0_0))
                    (<= v0x7f000d3dcf10_0 0.0)
                    (>= v0x7f000d3dcf10_0 0.0))))
      (a!6 (=> v0x7f000d3dcd90_0
               (or (and E0x7f000d3dcfd0
                        (not E0x7f000d3dd290)
                        (not E0x7f000d3dd510)
                        (not E0x7f000d3dd710))
                   (and E0x7f000d3dd290
                        (not E0x7f000d3dcfd0)
                        (not E0x7f000d3dd510)
                        (not E0x7f000d3dd710))
                   (and E0x7f000d3dd510
                        (not E0x7f000d3dcfd0)
                        (not E0x7f000d3dd290)
                        (not E0x7f000d3dd710))
                   (and E0x7f000d3dd710
                        (not E0x7f000d3dcfd0)
                        (not E0x7f000d3dd290)
                        (not E0x7f000d3dd510))))))
(let ((a!7 (and (=> v0x7f000d3d9ad0_0
                    (and v0x7f000d3d9810_0
                         E0x7f000d3d9b90
                         (not v0x7f000d3d9990_0)))
                (=> v0x7f000d3d9ad0_0 E0x7f000d3d9b90)
                a!1
                a!2
                (=> v0x7f000d3dab90_0
                    (and v0x7f000d3da210_0
                         E0x7f000d3dac50
                         (not v0x7f000d3daa50_0)))
                (=> v0x7f000d3dab90_0 E0x7f000d3dac50)
                a!3
                a!4
                (=> v0x7f000d3dba50_0
                    (and v0x7f000d3db0d0_0 E0x7f000d3dbb10 v0x7f000d3db910_0))
                (=> v0x7f000d3dba50_0 E0x7f000d3dbb10)
                (=> v0x7f000d3dbe10_0
                    (and v0x7f000d3db0d0_0
                         E0x7f000d3dbed0
                         (not v0x7f000d3db910_0)))
                (=> v0x7f000d3dbe10_0 E0x7f000d3dbed0)
                (=> v0x7f000d3dc650_0
                    (and v0x7f000d3dba50_0 E0x7f000d3dc710 v0x7f000d3dbcd0_0))
                (=> v0x7f000d3dc650_0 E0x7f000d3dc710)
                (=> v0x7f000d3dcb10_0
                    (and v0x7f000d3dbe10_0 E0x7f000d3dcbd0 v0x7f000d3dc510_0))
                (=> v0x7f000d3dcb10_0 E0x7f000d3dcbd0)
                (=> v0x7f000d3dcd90_0 a!5)
                a!6
                (=> v0x7f000d3de050_0
                    (and v0x7f000d3dcd90_0
                         E0x7f000d3de110
                         (not v0x7f000d3ddf10_0)))
                (=> v0x7f000d3de050_0 E0x7f000d3de110)
                v0x7f000d3de050_0
                (not v0x7f000d3de310_0)
                (= v0x7f000d3d9990_0 (= v0x7f000d3d98d0_0 0.0))
                (= v0x7f000d3d9dd0_0 (< v0x7f000d3d9010_0 2.0))
                (= v0x7f000d3d9f90_0 (ite v0x7f000d3d9dd0_0 1.0 0.0))
                (= v0x7f000d3da0d0_0 (+ v0x7f000d3d9f90_0 v0x7f000d3d9010_0))
                (= v0x7f000d3daa50_0 (= v0x7f000d3da990_0 0.0))
                (= v0x7f000d3dae50_0 (= v0x7f000d3d8f10_0 0.0))
                (= v0x7f000d3daf90_0 (ite v0x7f000d3dae50_0 1.0 0.0))
                (= v0x7f000d3db910_0 (= v0x7f000d3d9110_0 0.0))
                (= v0x7f000d3dbcd0_0 (> v0x7f000d3da2d0_0 1.0))
                (= v0x7f000d3dc0d0_0 (> v0x7f000d3da2d0_0 0.0))
                (= v0x7f000d3dc210_0 (+ v0x7f000d3da2d0_0 (- 1.0)))
                (= v0x7f000d3dc3d0_0
                   (ite v0x7f000d3dc0d0_0 v0x7f000d3dc210_0 v0x7f000d3da2d0_0))
                (= v0x7f000d3dc510_0 (= v0x7f000d3db190_0 0.0))
                (= v0x7f000d3dc8d0_0 (= v0x7f000d3db190_0 0.0))
                (= v0x7f000d3dc9d0_0
                   (ite v0x7f000d3dc8d0_0 1.0 v0x7f000d3d9110_0))
                (= v0x7f000d3ddcd0_0 (= v0x7f000d3db190_0 0.0))
                (= v0x7f000d3dddd0_0 (= v0x7f000d3dcf10_0 0.0))
                (= v0x7f000d3ddf10_0 (or v0x7f000d3dddd0_0 v0x7f000d3ddcd0_0))
                (= v0x7f000d3de310_0 (= v0x7f000d3d8d90_0 0.0)))))
  (=> F0x7f000d3df390 a!7))))
(assert (=> F0x7f000d3df390 F0x7f000d3df2d0))
(assert (=> F0x7f000d3df4d0 (or F0x7f000d3df090 F0x7f000d3df210)))
(assert (=> F0x7f000d3df490 F0x7f000d3df390))
(assert (=> pre!entry!0 (=> F0x7f000d3df150 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f000d3df2d0 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f000d3df4d0 true)))
(assert (= lemma!bb2.i.i29.i.i!0 (=> F0x7f000d3df490 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i29.i.i!0) (not lemma!bb2.i.i29.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f000d3df150)
; (error: F0x7f000d3df490)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i29.i.i!0)
