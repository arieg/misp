(declare-fun lemma!bb2.i.i35.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7fe75cbc9b90_0 () Bool)
(declare-fun v0x7fe75cbc9a50_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fe75cbc8310_0 () Real)
(declare-fun v0x7fe75cbc81d0_0 () Bool)
(declare-fun v0x7fe75cbc7c90_0 () Bool)
(declare-fun v0x7fe75cbc6e10_0 () Bool)
(declare-fun v0x7fe75cbc6950_0 () Real)
(declare-fun v0x7fe75cbc5d90_0 () Bool)
(declare-fun v0x7fe75cbc9e10_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fe75cbc5210_0 () Real)
(declare-fun E0x7fe75cbc9410 () Bool)
(declare-fun v0x7fe75cbc7dd0_0 () Real)
(declare-fun v0x7fe75cbc8dd0_0 () Real)
(declare-fun v0x7fe75cbc8d10_0 () Real)
(declare-fun E0x7fe75cbc8e90 () Bool)
(declare-fun E0x7fe75cbc8a90 () Bool)
(declare-fun v0x7fe75cbc89d0_0 () Bool)
(declare-fun E0x7fe75cbc7fd0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7fe75cbc5890_0 () Real)
(declare-fun F0x7fe75cbcad10 () Bool)
(declare-fun v0x7fe75cbc4f90_0 () Real)
(declare-fun F0x7fe75cbcad50 () Bool)
(declare-fun v0x7fe75cbc7150_0 () Real)
(declare-fun E0x7fe75cbc9150 () Bool)
(declare-fun v0x7fe75cbc6a10_0 () Bool)
(declare-fun v0x7fe75cbc84d0_0 () Real)
(declare-fun E0x7fe75cbc6c10 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7fe75cbc73d0 () Bool)
(declare-fun E0x7fe75cbc6510 () Bool)
(declare-fun v0x7fe75cbc6090_0 () Real)
(declare-fun v0x7fe75cbc7f10_0 () Bool)
(declare-fun v0x7fe75cbc8750_0 () Bool)
(declare-fun v0x7fe75cbc6b50_0 () Bool)
(declare-fun v0x7fe75cbc6290_0 () Real)
(declare-fun E0x7fe75cbc5b50 () Bool)
(declare-fun v0x7fe75cbc9950_0 () Bool)
(declare-fun v0x7fe75cbc6f50_0 () Real)
(declare-fun v0x7fe75cbc57d0_0 () Bool)
(declare-fun v0x7fe75cbc9cd0_0 () Bool)
(declare-fun F0x7fe75cbca950 () Bool)
(declare-fun v0x7fe75cbc8610_0 () Bool)
(declare-fun v0x7fe75cbc5f50_0 () Real)
(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun v0x7fe75cbc7090_0 () Bool)
(declare-fun v0x7fe75cbc8c50_0 () Bool)
(declare-fun v0x7fe75cbc2010_0 () Real)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7fe75cbc7210 () Bool)
(declare-fun v0x7fe75cbc78d0_0 () Bool)
(declare-fun v0x7fe75cbc52d0_0 () Real)
(declare-fun F0x7fe75cbcac10 () Bool)
(declare-fun F0x7fe75cbcaa90 () Bool)
(declare-fun v0x7fe75cbc61d0_0 () Bool)
(declare-fun v0x7fe75cbc51d0_0 () Real)
(declare-fun v0x7fe75cbc5110_0 () Real)
(declare-fun E0x7fe75cbc7ad0 () Bool)
(declare-fun v0x7fe75cbc5a90_0 () Bool)
(declare-fun F0x7fe75cbcab50 () Bool)
(declare-fun v0x7fe75cbc8890_0 () Bool)
(declare-fun v0x7fe75cbc2110_0 () Bool)
(declare-fun E0x7fe75cbc6350 () Bool)
(declare-fun v0x7fe75cbc7a10_0 () Bool)
(declare-fun F0x7fe75cbcaa50 () Bool)
(declare-fun v0x7fe75cbc5950_0 () Bool)

(assert (=> F0x7fe75cbcaa50
    (and v0x7fe75cbc2110_0
         (<= v0x7fe75cbc51d0_0 0.0)
         (>= v0x7fe75cbc51d0_0 0.0)
         (<= v0x7fe75cbc52d0_0 1.0)
         (>= v0x7fe75cbc52d0_0 1.0)
         (<= v0x7fe75cbc2010_0 0.0)
         (>= v0x7fe75cbc2010_0 0.0))))
(assert (=> F0x7fe75cbcaa50 F0x7fe75cbca950))
(assert (let ((a!1 (=> v0x7fe75cbc61d0_0
               (or (and v0x7fe75cbc5a90_0
                        E0x7fe75cbc6350
                        (<= v0x7fe75cbc6290_0 v0x7fe75cbc6090_0)
                        (>= v0x7fe75cbc6290_0 v0x7fe75cbc6090_0))
                   (and v0x7fe75cbc57d0_0
                        E0x7fe75cbc6510
                        v0x7fe75cbc5950_0
                        (<= v0x7fe75cbc6290_0 v0x7fe75cbc5110_0)
                        (>= v0x7fe75cbc6290_0 v0x7fe75cbc5110_0)))))
      (a!2 (=> v0x7fe75cbc61d0_0
               (or (and E0x7fe75cbc6350 (not E0x7fe75cbc6510))
                   (and E0x7fe75cbc6510 (not E0x7fe75cbc6350)))))
      (a!3 (=> v0x7fe75cbc7090_0
               (or (and v0x7fe75cbc6b50_0
                        E0x7fe75cbc7210
                        (<= v0x7fe75cbc7150_0 v0x7fe75cbc6f50_0)
                        (>= v0x7fe75cbc7150_0 v0x7fe75cbc6f50_0))
                   (and v0x7fe75cbc61d0_0
                        E0x7fe75cbc73d0
                        v0x7fe75cbc6a10_0
                        (<= v0x7fe75cbc7150_0 v0x7fe75cbc4f90_0)
                        (>= v0x7fe75cbc7150_0 v0x7fe75cbc4f90_0)))))
      (a!4 (=> v0x7fe75cbc7090_0
               (or (and E0x7fe75cbc7210 (not E0x7fe75cbc73d0))
                   (and E0x7fe75cbc73d0 (not E0x7fe75cbc7210)))))
      (a!5 (or (and v0x7fe75cbc7a10_0
                    E0x7fe75cbc8e90
                    (<= v0x7fe75cbc8d10_0 v0x7fe75cbc6290_0)
                    (>= v0x7fe75cbc8d10_0 v0x7fe75cbc6290_0)
                    (<= v0x7fe75cbc8dd0_0 v0x7fe75cbc7dd0_0)
                    (>= v0x7fe75cbc8dd0_0 v0x7fe75cbc7dd0_0))
               (and v0x7fe75cbc89d0_0
                    E0x7fe75cbc9150
                    (and (<= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0)
                         (>= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0))
                    (<= v0x7fe75cbc8dd0_0 v0x7fe75cbc5210_0)
                    (>= v0x7fe75cbc8dd0_0 v0x7fe75cbc5210_0))
               (and v0x7fe75cbc7f10_0
                    E0x7fe75cbc9410
                    (not v0x7fe75cbc8890_0)
                    (and (<= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0)
                         (>= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0))
                    (<= v0x7fe75cbc8dd0_0 0.0)
                    (>= v0x7fe75cbc8dd0_0 0.0))))
      (a!6 (=> v0x7fe75cbc8c50_0
               (or (and E0x7fe75cbc8e90
                        (not E0x7fe75cbc9150)
                        (not E0x7fe75cbc9410))
                   (and E0x7fe75cbc9150
                        (not E0x7fe75cbc8e90)
                        (not E0x7fe75cbc9410))
                   (and E0x7fe75cbc9410
                        (not E0x7fe75cbc8e90)
                        (not E0x7fe75cbc9150))))))
(let ((a!7 (and (=> v0x7fe75cbc5a90_0
                    (and v0x7fe75cbc57d0_0
                         E0x7fe75cbc5b50
                         (not v0x7fe75cbc5950_0)))
                (=> v0x7fe75cbc5a90_0 E0x7fe75cbc5b50)
                a!1
                a!2
                (=> v0x7fe75cbc6b50_0
                    (and v0x7fe75cbc61d0_0
                         E0x7fe75cbc6c10
                         (not v0x7fe75cbc6a10_0)))
                (=> v0x7fe75cbc6b50_0 E0x7fe75cbc6c10)
                a!3
                a!4
                (=> v0x7fe75cbc7a10_0
                    (and v0x7fe75cbc7090_0 E0x7fe75cbc7ad0 v0x7fe75cbc78d0_0))
                (=> v0x7fe75cbc7a10_0 E0x7fe75cbc7ad0)
                (=> v0x7fe75cbc7f10_0
                    (and v0x7fe75cbc7090_0
                         E0x7fe75cbc7fd0
                         (not v0x7fe75cbc78d0_0)))
                (=> v0x7fe75cbc7f10_0 E0x7fe75cbc7fd0)
                (=> v0x7fe75cbc89d0_0
                    (and v0x7fe75cbc7f10_0 E0x7fe75cbc8a90 v0x7fe75cbc8890_0))
                (=> v0x7fe75cbc89d0_0 E0x7fe75cbc8a90)
                (=> v0x7fe75cbc8c50_0 a!5)
                a!6
                v0x7fe75cbc8c50_0
                (not v0x7fe75cbc9e10_0)
                (<= v0x7fe75cbc51d0_0 v0x7fe75cbc7150_0)
                (>= v0x7fe75cbc51d0_0 v0x7fe75cbc7150_0)
                (<= v0x7fe75cbc52d0_0 v0x7fe75cbc8d10_0)
                (>= v0x7fe75cbc52d0_0 v0x7fe75cbc8d10_0)
                (<= v0x7fe75cbc2010_0 v0x7fe75cbc8dd0_0)
                (>= v0x7fe75cbc2010_0 v0x7fe75cbc8dd0_0)
                (= v0x7fe75cbc5950_0 (= v0x7fe75cbc5890_0 0.0))
                (= v0x7fe75cbc5d90_0 (< v0x7fe75cbc5110_0 2.0))
                (= v0x7fe75cbc5f50_0 (ite v0x7fe75cbc5d90_0 1.0 0.0))
                (= v0x7fe75cbc6090_0 (+ v0x7fe75cbc5f50_0 v0x7fe75cbc5110_0))
                (= v0x7fe75cbc6a10_0 (= v0x7fe75cbc6950_0 0.0))
                (= v0x7fe75cbc6e10_0 (= v0x7fe75cbc4f90_0 0.0))
                (= v0x7fe75cbc6f50_0 (ite v0x7fe75cbc6e10_0 1.0 0.0))
                (= v0x7fe75cbc78d0_0 (= v0x7fe75cbc5210_0 0.0))
                (= v0x7fe75cbc7c90_0 (> v0x7fe75cbc6290_0 1.0))
                (= v0x7fe75cbc7dd0_0
                   (ite v0x7fe75cbc7c90_0 1.0 v0x7fe75cbc5210_0))
                (= v0x7fe75cbc81d0_0 (> v0x7fe75cbc6290_0 0.0))
                (= v0x7fe75cbc8310_0 (+ v0x7fe75cbc6290_0 (- 1.0)))
                (= v0x7fe75cbc84d0_0
                   (ite v0x7fe75cbc81d0_0 v0x7fe75cbc8310_0 v0x7fe75cbc6290_0))
                (= v0x7fe75cbc8610_0 (= v0x7fe75cbc7150_0 0.0))
                (= v0x7fe75cbc8750_0 (= v0x7fe75cbc84d0_0 0.0))
                (= v0x7fe75cbc8890_0 (and v0x7fe75cbc8610_0 v0x7fe75cbc8750_0))
                (= v0x7fe75cbc9950_0 (= v0x7fe75cbc7150_0 0.0))
                (= v0x7fe75cbc9a50_0 (= v0x7fe75cbc8d10_0 2.0))
                (= v0x7fe75cbc9b90_0 (= v0x7fe75cbc8dd0_0 0.0))
                (= v0x7fe75cbc9cd0_0 (and v0x7fe75cbc9a50_0 v0x7fe75cbc9950_0))
                (= v0x7fe75cbc9e10_0 (and v0x7fe75cbc9cd0_0 v0x7fe75cbc9b90_0)))))
  (=> F0x7fe75cbcaa90 a!7))))
(assert (=> F0x7fe75cbcaa90 F0x7fe75cbcab50))
(assert (let ((a!1 (=> v0x7fe75cbc61d0_0
               (or (and v0x7fe75cbc5a90_0
                        E0x7fe75cbc6350
                        (<= v0x7fe75cbc6290_0 v0x7fe75cbc6090_0)
                        (>= v0x7fe75cbc6290_0 v0x7fe75cbc6090_0))
                   (and v0x7fe75cbc57d0_0
                        E0x7fe75cbc6510
                        v0x7fe75cbc5950_0
                        (<= v0x7fe75cbc6290_0 v0x7fe75cbc5110_0)
                        (>= v0x7fe75cbc6290_0 v0x7fe75cbc5110_0)))))
      (a!2 (=> v0x7fe75cbc61d0_0
               (or (and E0x7fe75cbc6350 (not E0x7fe75cbc6510))
                   (and E0x7fe75cbc6510 (not E0x7fe75cbc6350)))))
      (a!3 (=> v0x7fe75cbc7090_0
               (or (and v0x7fe75cbc6b50_0
                        E0x7fe75cbc7210
                        (<= v0x7fe75cbc7150_0 v0x7fe75cbc6f50_0)
                        (>= v0x7fe75cbc7150_0 v0x7fe75cbc6f50_0))
                   (and v0x7fe75cbc61d0_0
                        E0x7fe75cbc73d0
                        v0x7fe75cbc6a10_0
                        (<= v0x7fe75cbc7150_0 v0x7fe75cbc4f90_0)
                        (>= v0x7fe75cbc7150_0 v0x7fe75cbc4f90_0)))))
      (a!4 (=> v0x7fe75cbc7090_0
               (or (and E0x7fe75cbc7210 (not E0x7fe75cbc73d0))
                   (and E0x7fe75cbc73d0 (not E0x7fe75cbc7210)))))
      (a!5 (or (and v0x7fe75cbc7a10_0
                    E0x7fe75cbc8e90
                    (<= v0x7fe75cbc8d10_0 v0x7fe75cbc6290_0)
                    (>= v0x7fe75cbc8d10_0 v0x7fe75cbc6290_0)
                    (<= v0x7fe75cbc8dd0_0 v0x7fe75cbc7dd0_0)
                    (>= v0x7fe75cbc8dd0_0 v0x7fe75cbc7dd0_0))
               (and v0x7fe75cbc89d0_0
                    E0x7fe75cbc9150
                    (and (<= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0)
                         (>= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0))
                    (<= v0x7fe75cbc8dd0_0 v0x7fe75cbc5210_0)
                    (>= v0x7fe75cbc8dd0_0 v0x7fe75cbc5210_0))
               (and v0x7fe75cbc7f10_0
                    E0x7fe75cbc9410
                    (not v0x7fe75cbc8890_0)
                    (and (<= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0)
                         (>= v0x7fe75cbc8d10_0 v0x7fe75cbc84d0_0))
                    (<= v0x7fe75cbc8dd0_0 0.0)
                    (>= v0x7fe75cbc8dd0_0 0.0))))
      (a!6 (=> v0x7fe75cbc8c50_0
               (or (and E0x7fe75cbc8e90
                        (not E0x7fe75cbc9150)
                        (not E0x7fe75cbc9410))
                   (and E0x7fe75cbc9150
                        (not E0x7fe75cbc8e90)
                        (not E0x7fe75cbc9410))
                   (and E0x7fe75cbc9410
                        (not E0x7fe75cbc8e90)
                        (not E0x7fe75cbc9150))))))
(let ((a!7 (and (=> v0x7fe75cbc5a90_0
                    (and v0x7fe75cbc57d0_0
                         E0x7fe75cbc5b50
                         (not v0x7fe75cbc5950_0)))
                (=> v0x7fe75cbc5a90_0 E0x7fe75cbc5b50)
                a!1
                a!2
                (=> v0x7fe75cbc6b50_0
                    (and v0x7fe75cbc61d0_0
                         E0x7fe75cbc6c10
                         (not v0x7fe75cbc6a10_0)))
                (=> v0x7fe75cbc6b50_0 E0x7fe75cbc6c10)
                a!3
                a!4
                (=> v0x7fe75cbc7a10_0
                    (and v0x7fe75cbc7090_0 E0x7fe75cbc7ad0 v0x7fe75cbc78d0_0))
                (=> v0x7fe75cbc7a10_0 E0x7fe75cbc7ad0)
                (=> v0x7fe75cbc7f10_0
                    (and v0x7fe75cbc7090_0
                         E0x7fe75cbc7fd0
                         (not v0x7fe75cbc78d0_0)))
                (=> v0x7fe75cbc7f10_0 E0x7fe75cbc7fd0)
                (=> v0x7fe75cbc89d0_0
                    (and v0x7fe75cbc7f10_0 E0x7fe75cbc8a90 v0x7fe75cbc8890_0))
                (=> v0x7fe75cbc89d0_0 E0x7fe75cbc8a90)
                (=> v0x7fe75cbc8c50_0 a!5)
                a!6
                v0x7fe75cbc8c50_0
                v0x7fe75cbc9e10_0
                (= v0x7fe75cbc5950_0 (= v0x7fe75cbc5890_0 0.0))
                (= v0x7fe75cbc5d90_0 (< v0x7fe75cbc5110_0 2.0))
                (= v0x7fe75cbc5f50_0 (ite v0x7fe75cbc5d90_0 1.0 0.0))
                (= v0x7fe75cbc6090_0 (+ v0x7fe75cbc5f50_0 v0x7fe75cbc5110_0))
                (= v0x7fe75cbc6a10_0 (= v0x7fe75cbc6950_0 0.0))
                (= v0x7fe75cbc6e10_0 (= v0x7fe75cbc4f90_0 0.0))
                (= v0x7fe75cbc6f50_0 (ite v0x7fe75cbc6e10_0 1.0 0.0))
                (= v0x7fe75cbc78d0_0 (= v0x7fe75cbc5210_0 0.0))
                (= v0x7fe75cbc7c90_0 (> v0x7fe75cbc6290_0 1.0))
                (= v0x7fe75cbc7dd0_0
                   (ite v0x7fe75cbc7c90_0 1.0 v0x7fe75cbc5210_0))
                (= v0x7fe75cbc81d0_0 (> v0x7fe75cbc6290_0 0.0))
                (= v0x7fe75cbc8310_0 (+ v0x7fe75cbc6290_0 (- 1.0)))
                (= v0x7fe75cbc84d0_0
                   (ite v0x7fe75cbc81d0_0 v0x7fe75cbc8310_0 v0x7fe75cbc6290_0))
                (= v0x7fe75cbc8610_0 (= v0x7fe75cbc7150_0 0.0))
                (= v0x7fe75cbc8750_0 (= v0x7fe75cbc84d0_0 0.0))
                (= v0x7fe75cbc8890_0 (and v0x7fe75cbc8610_0 v0x7fe75cbc8750_0))
                (= v0x7fe75cbc9950_0 (= v0x7fe75cbc7150_0 0.0))
                (= v0x7fe75cbc9a50_0 (= v0x7fe75cbc8d10_0 2.0))
                (= v0x7fe75cbc9b90_0 (= v0x7fe75cbc8dd0_0 0.0))
                (= v0x7fe75cbc9cd0_0 (and v0x7fe75cbc9a50_0 v0x7fe75cbc9950_0))
                (= v0x7fe75cbc9e10_0 (and v0x7fe75cbc9cd0_0 v0x7fe75cbc9b90_0)))))
  (=> F0x7fe75cbcac10 a!7))))
(assert (=> F0x7fe75cbcac10 F0x7fe75cbcab50))
(assert (=> F0x7fe75cbcad50 (or F0x7fe75cbcaa50 F0x7fe75cbcaa90)))
(assert (=> F0x7fe75cbcad10 F0x7fe75cbcac10))
(assert (=> pre!entry!0 (=> F0x7fe75cbca950 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fe75cbcab50 (>= v0x7fe75cbc5210_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7fe75cbcab50 (not (<= 3.0 v0x7fe75cbc5110_0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fe75cbcad50 (>= v0x7fe75cbc2010_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7fe75cbcad50 (not (<= 3.0 v0x7fe75cbc52d0_0)))))
(assert (= lemma!bb2.i.i35.i.i!0 (=> F0x7fe75cbcad10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i35.i.i!0) (not lemma!bb2.i.i35.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7fe75cbca950)
; (error: F0x7fe75cbcad10)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
