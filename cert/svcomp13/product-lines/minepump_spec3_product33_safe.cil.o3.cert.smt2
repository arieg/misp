(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f37df6ee9d0 () Bool)
(declare-fun v0x7f37df6ed950_0 () Bool)
(declare-fun v0x7f37df6ed810_0 () Bool)
(declare-fun v0x7f37df6ed6d0_0 () Bool)
(declare-fun v0x7f37df6ed590_0 () Bool)
(declare-fun v0x7f37df6eb010_0 () Bool)
(declare-fun v0x7f37df6e9a90_0 () Real)
(declare-fun v0x7f37df6eda90_0 () Bool)
(declare-fun v0x7f37df6ec3d0_0 () Real)
(declare-fun v0x7f37df6ec290_0 () Bool)
(declare-fun E0x7f37df6ecbd0 () Bool)
(declare-fun v0x7f37df6ec990_0 () Bool)
(declare-fun v0x7f37df6ebe90_0 () Bool)
(declare-fun E0x7f37df6ec790 () Bool)
(declare-fun v0x7f37df6e9f90_0 () Bool)
(declare-fun E0x7f37df6ec090 () Bool)
(declare-fun v0x7f37df6eca50_0 () Real)
(declare-fun E0x7f37df6eb410 () Bool)
(declare-fun E0x7f37df6eb5d0 () Bool)
(declare-fun v0x7f37df6ebfd0_0 () Bool)
(declare-fun v0x7f37df6eb290_0 () Bool)
(declare-fun v0x7f37df6e9410_0 () Real)
(declare-fun v0x7f37df6eac10_0 () Bool)
(declare-fun E0x7f37df6ebcd0 () Bool)
(declare-fun v0x7f37df6eab50_0 () Real)
(declare-fun v0x7f37df6ec590_0 () Real)
(declare-fun E0x7f37df6ea710 () Bool)
(declare-fun E0x7f37df6ed0d0 () Bool)
(declare-fun E0x7f37df6ea550 () Bool)
(declare-fun v0x7f37df6ea3d0_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f37df6e9b50_0 () Bool)
(declare-fun E0x7f37df6e9d50 () Bool)
(declare-fun F0x7f37df6ee890 () Bool)
(declare-fun v0x7f37df6eb150_0 () Real)
(declare-fun E0x7f37df6ece90 () Bool)
(declare-fun v0x7f37df6e99d0_0 () Bool)
(declare-fun v0x7f37df6ea490_0 () Real)
(declare-fun E0x7f37df6eae10 () Bool)
(declare-fun v0x7f37df6e9c90_0 () Bool)
(declare-fun v0x7f37df6ebad0_0 () Bool)
(declare-fun F0x7f37df6ee7d0 () Bool)
(declare-fun v0x7f37df6ebc10_0 () Bool)
(declare-fun F0x7f37df6ee990 () Bool)
(declare-fun v0x7f37df6ecb10_0 () Real)
(declare-fun v0x7f37df6e9310_0 () Real)
(declare-fun lemma!bb2.i.i16.i.i!0 () Bool)
(declare-fun F0x7f37df6ee710 () Bool)
(declare-fun F0x7f37df6ee650 () Bool)
(declare-fun v0x7f37df6ea290_0 () Real)
(declare-fun v0x7f37df6e8010_0 () Real)
(declare-fun v0x7f37df6ea150_0 () Real)
(declare-fun v0x7f37df6e93d0_0 () Real)
(declare-fun v0x7f37df6e94d0_0 () Real)
(declare-fun v0x7f37df6ec6d0_0 () Bool)
(declare-fun v0x7f37df6e9190_0 () Real)
(declare-fun v0x7f37df6e8110_0 () Bool)
(declare-fun v0x7f37df6eb350_0 () Real)
(declare-fun v0x7f37df6ead50_0 () Bool)
(declare-fun F0x7f37df6ee590 () Bool)

(assert (=> F0x7f37df6ee590
    (and v0x7f37df6e8110_0
         (<= v0x7f37df6e93d0_0 0.0)
         (>= v0x7f37df6e93d0_0 0.0)
         (<= v0x7f37df6e94d0_0 0.0)
         (>= v0x7f37df6e94d0_0 0.0)
         (<= v0x7f37df6e8010_0 1.0)
         (>= v0x7f37df6e8010_0 1.0))))
(assert (=> F0x7f37df6ee590 F0x7f37df6ee650))
(assert (let ((a!1 (=> v0x7f37df6ea3d0_0
               (or (and v0x7f37df6e9c90_0
                        E0x7f37df6ea550
                        (<= v0x7f37df6ea490_0 v0x7f37df6ea290_0)
                        (>= v0x7f37df6ea490_0 v0x7f37df6ea290_0))
                   (and v0x7f37df6e99d0_0
                        E0x7f37df6ea710
                        v0x7f37df6e9b50_0
                        (<= v0x7f37df6ea490_0 v0x7f37df6e9410_0)
                        (>= v0x7f37df6ea490_0 v0x7f37df6e9410_0)))))
      (a!2 (=> v0x7f37df6ea3d0_0
               (or (and E0x7f37df6ea550 (not E0x7f37df6ea710))
                   (and E0x7f37df6ea710 (not E0x7f37df6ea550)))))
      (a!3 (=> v0x7f37df6eb290_0
               (or (and v0x7f37df6ead50_0
                        E0x7f37df6eb410
                        (<= v0x7f37df6eb350_0 v0x7f37df6eb150_0)
                        (>= v0x7f37df6eb350_0 v0x7f37df6eb150_0))
                   (and v0x7f37df6ea3d0_0
                        E0x7f37df6eb5d0
                        v0x7f37df6eac10_0
                        (<= v0x7f37df6eb350_0 v0x7f37df6e9310_0)
                        (>= v0x7f37df6eb350_0 v0x7f37df6e9310_0)))))
      (a!4 (=> v0x7f37df6eb290_0
               (or (and E0x7f37df6eb410 (not E0x7f37df6eb5d0))
                   (and E0x7f37df6eb5d0 (not E0x7f37df6eb410)))))
      (a!5 (or (and v0x7f37df6ec6d0_0
                    E0x7f37df6ecbd0
                    (and (<= v0x7f37df6eca50_0 v0x7f37df6ea490_0)
                         (>= v0x7f37df6eca50_0 v0x7f37df6ea490_0))
                    (and (<= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)
                         (>= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)))
               (and v0x7f37df6ebc10_0
                    E0x7f37df6ece90
                    v0x7f37df6ebe90_0
                    (and (<= v0x7f37df6eca50_0 v0x7f37df6ea490_0)
                         (>= v0x7f37df6eca50_0 v0x7f37df6ea490_0))
                    (<= v0x7f37df6ecb10_0 1.0)
                    (>= v0x7f37df6ecb10_0 1.0))
               (and v0x7f37df6ebfd0_0
                    E0x7f37df6ed0d0
                    (<= v0x7f37df6eca50_0 v0x7f37df6ec590_0)
                    (>= v0x7f37df6eca50_0 v0x7f37df6ec590_0)
                    (and (<= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)
                         (>= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)))))
      (a!6 (=> v0x7f37df6ec990_0
               (or (and E0x7f37df6ecbd0
                        (not E0x7f37df6ece90)
                        (not E0x7f37df6ed0d0))
                   (and E0x7f37df6ece90
                        (not E0x7f37df6ecbd0)
                        (not E0x7f37df6ed0d0))
                   (and E0x7f37df6ed0d0
                        (not E0x7f37df6ecbd0)
                        (not E0x7f37df6ece90))))))
(let ((a!7 (and (=> v0x7f37df6e9c90_0
                    (and v0x7f37df6e99d0_0
                         E0x7f37df6e9d50
                         (not v0x7f37df6e9b50_0)))
                (=> v0x7f37df6e9c90_0 E0x7f37df6e9d50)
                a!1
                a!2
                (=> v0x7f37df6ead50_0
                    (and v0x7f37df6ea3d0_0
                         E0x7f37df6eae10
                         (not v0x7f37df6eac10_0)))
                (=> v0x7f37df6ead50_0 E0x7f37df6eae10)
                a!3
                a!4
                (=> v0x7f37df6ebc10_0
                    (and v0x7f37df6eb290_0 E0x7f37df6ebcd0 v0x7f37df6ebad0_0))
                (=> v0x7f37df6ebc10_0 E0x7f37df6ebcd0)
                (=> v0x7f37df6ebfd0_0
                    (and v0x7f37df6eb290_0
                         E0x7f37df6ec090
                         (not v0x7f37df6ebad0_0)))
                (=> v0x7f37df6ebfd0_0 E0x7f37df6ec090)
                (=> v0x7f37df6ec6d0_0
                    (and v0x7f37df6ebc10_0
                         E0x7f37df6ec790
                         (not v0x7f37df6ebe90_0)))
                (=> v0x7f37df6ec6d0_0 E0x7f37df6ec790)
                (=> v0x7f37df6ec990_0 a!5)
                a!6
                v0x7f37df6ec990_0
                (not v0x7f37df6eda90_0)
                (<= v0x7f37df6e93d0_0 v0x7f37df6ecb10_0)
                (>= v0x7f37df6e93d0_0 v0x7f37df6ecb10_0)
                (<= v0x7f37df6e94d0_0 v0x7f37df6eb350_0)
                (>= v0x7f37df6e94d0_0 v0x7f37df6eb350_0)
                (<= v0x7f37df6e8010_0 v0x7f37df6eca50_0)
                (>= v0x7f37df6e8010_0 v0x7f37df6eca50_0)
                (= v0x7f37df6e9b50_0 (= v0x7f37df6e9a90_0 0.0))
                (= v0x7f37df6e9f90_0 (< v0x7f37df6e9410_0 2.0))
                (= v0x7f37df6ea150_0 (ite v0x7f37df6e9f90_0 1.0 0.0))
                (= v0x7f37df6ea290_0 (+ v0x7f37df6ea150_0 v0x7f37df6e9410_0))
                (= v0x7f37df6eac10_0 (= v0x7f37df6eab50_0 0.0))
                (= v0x7f37df6eb010_0 (= v0x7f37df6e9310_0 0.0))
                (= v0x7f37df6eb150_0 (ite v0x7f37df6eb010_0 1.0 0.0))
                (= v0x7f37df6ebad0_0 (= v0x7f37df6e9190_0 0.0))
                (= v0x7f37df6ebe90_0 (> v0x7f37df6ea490_0 1.0))
                (= v0x7f37df6ec290_0 (> v0x7f37df6ea490_0 0.0))
                (= v0x7f37df6ec3d0_0 (+ v0x7f37df6ea490_0 (- 1.0)))
                (= v0x7f37df6ec590_0
                   (ite v0x7f37df6ec290_0 v0x7f37df6ec3d0_0 v0x7f37df6ea490_0))
                (= v0x7f37df6ed590_0 (= v0x7f37df6eb350_0 0.0))
                (= v0x7f37df6ed6d0_0 (= v0x7f37df6eca50_0 2.0))
                (= v0x7f37df6ed810_0 (= v0x7f37df6ecb10_0 0.0))
                (= v0x7f37df6ed950_0 (and v0x7f37df6ed6d0_0 v0x7f37df6ed590_0))
                (= v0x7f37df6eda90_0 (and v0x7f37df6ed950_0 v0x7f37df6ed810_0)))))
  (=> F0x7f37df6ee710 a!7))))
(assert (=> F0x7f37df6ee710 F0x7f37df6ee7d0))
(assert (let ((a!1 (=> v0x7f37df6ea3d0_0
               (or (and v0x7f37df6e9c90_0
                        E0x7f37df6ea550
                        (<= v0x7f37df6ea490_0 v0x7f37df6ea290_0)
                        (>= v0x7f37df6ea490_0 v0x7f37df6ea290_0))
                   (and v0x7f37df6e99d0_0
                        E0x7f37df6ea710
                        v0x7f37df6e9b50_0
                        (<= v0x7f37df6ea490_0 v0x7f37df6e9410_0)
                        (>= v0x7f37df6ea490_0 v0x7f37df6e9410_0)))))
      (a!2 (=> v0x7f37df6ea3d0_0
               (or (and E0x7f37df6ea550 (not E0x7f37df6ea710))
                   (and E0x7f37df6ea710 (not E0x7f37df6ea550)))))
      (a!3 (=> v0x7f37df6eb290_0
               (or (and v0x7f37df6ead50_0
                        E0x7f37df6eb410
                        (<= v0x7f37df6eb350_0 v0x7f37df6eb150_0)
                        (>= v0x7f37df6eb350_0 v0x7f37df6eb150_0))
                   (and v0x7f37df6ea3d0_0
                        E0x7f37df6eb5d0
                        v0x7f37df6eac10_0
                        (<= v0x7f37df6eb350_0 v0x7f37df6e9310_0)
                        (>= v0x7f37df6eb350_0 v0x7f37df6e9310_0)))))
      (a!4 (=> v0x7f37df6eb290_0
               (or (and E0x7f37df6eb410 (not E0x7f37df6eb5d0))
                   (and E0x7f37df6eb5d0 (not E0x7f37df6eb410)))))
      (a!5 (or (and v0x7f37df6ec6d0_0
                    E0x7f37df6ecbd0
                    (and (<= v0x7f37df6eca50_0 v0x7f37df6ea490_0)
                         (>= v0x7f37df6eca50_0 v0x7f37df6ea490_0))
                    (and (<= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)
                         (>= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)))
               (and v0x7f37df6ebc10_0
                    E0x7f37df6ece90
                    v0x7f37df6ebe90_0
                    (and (<= v0x7f37df6eca50_0 v0x7f37df6ea490_0)
                         (>= v0x7f37df6eca50_0 v0x7f37df6ea490_0))
                    (<= v0x7f37df6ecb10_0 1.0)
                    (>= v0x7f37df6ecb10_0 1.0))
               (and v0x7f37df6ebfd0_0
                    E0x7f37df6ed0d0
                    (<= v0x7f37df6eca50_0 v0x7f37df6ec590_0)
                    (>= v0x7f37df6eca50_0 v0x7f37df6ec590_0)
                    (and (<= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)
                         (>= v0x7f37df6ecb10_0 v0x7f37df6e9190_0)))))
      (a!6 (=> v0x7f37df6ec990_0
               (or (and E0x7f37df6ecbd0
                        (not E0x7f37df6ece90)
                        (not E0x7f37df6ed0d0))
                   (and E0x7f37df6ece90
                        (not E0x7f37df6ecbd0)
                        (not E0x7f37df6ed0d0))
                   (and E0x7f37df6ed0d0
                        (not E0x7f37df6ecbd0)
                        (not E0x7f37df6ece90))))))
(let ((a!7 (and (=> v0x7f37df6e9c90_0
                    (and v0x7f37df6e99d0_0
                         E0x7f37df6e9d50
                         (not v0x7f37df6e9b50_0)))
                (=> v0x7f37df6e9c90_0 E0x7f37df6e9d50)
                a!1
                a!2
                (=> v0x7f37df6ead50_0
                    (and v0x7f37df6ea3d0_0
                         E0x7f37df6eae10
                         (not v0x7f37df6eac10_0)))
                (=> v0x7f37df6ead50_0 E0x7f37df6eae10)
                a!3
                a!4
                (=> v0x7f37df6ebc10_0
                    (and v0x7f37df6eb290_0 E0x7f37df6ebcd0 v0x7f37df6ebad0_0))
                (=> v0x7f37df6ebc10_0 E0x7f37df6ebcd0)
                (=> v0x7f37df6ebfd0_0
                    (and v0x7f37df6eb290_0
                         E0x7f37df6ec090
                         (not v0x7f37df6ebad0_0)))
                (=> v0x7f37df6ebfd0_0 E0x7f37df6ec090)
                (=> v0x7f37df6ec6d0_0
                    (and v0x7f37df6ebc10_0
                         E0x7f37df6ec790
                         (not v0x7f37df6ebe90_0)))
                (=> v0x7f37df6ec6d0_0 E0x7f37df6ec790)
                (=> v0x7f37df6ec990_0 a!5)
                a!6
                v0x7f37df6ec990_0
                v0x7f37df6eda90_0
                (= v0x7f37df6e9b50_0 (= v0x7f37df6e9a90_0 0.0))
                (= v0x7f37df6e9f90_0 (< v0x7f37df6e9410_0 2.0))
                (= v0x7f37df6ea150_0 (ite v0x7f37df6e9f90_0 1.0 0.0))
                (= v0x7f37df6ea290_0 (+ v0x7f37df6ea150_0 v0x7f37df6e9410_0))
                (= v0x7f37df6eac10_0 (= v0x7f37df6eab50_0 0.0))
                (= v0x7f37df6eb010_0 (= v0x7f37df6e9310_0 0.0))
                (= v0x7f37df6eb150_0 (ite v0x7f37df6eb010_0 1.0 0.0))
                (= v0x7f37df6ebad0_0 (= v0x7f37df6e9190_0 0.0))
                (= v0x7f37df6ebe90_0 (> v0x7f37df6ea490_0 1.0))
                (= v0x7f37df6ec290_0 (> v0x7f37df6ea490_0 0.0))
                (= v0x7f37df6ec3d0_0 (+ v0x7f37df6ea490_0 (- 1.0)))
                (= v0x7f37df6ec590_0
                   (ite v0x7f37df6ec290_0 v0x7f37df6ec3d0_0 v0x7f37df6ea490_0))
                (= v0x7f37df6ed590_0 (= v0x7f37df6eb350_0 0.0))
                (= v0x7f37df6ed6d0_0 (= v0x7f37df6eca50_0 2.0))
                (= v0x7f37df6ed810_0 (= v0x7f37df6ecb10_0 0.0))
                (= v0x7f37df6ed950_0 (and v0x7f37df6ed6d0_0 v0x7f37df6ed590_0))
                (= v0x7f37df6eda90_0 (and v0x7f37df6ed950_0 v0x7f37df6ed810_0)))))
  (=> F0x7f37df6ee890 a!7))))
(assert (=> F0x7f37df6ee890 F0x7f37df6ee7d0))
(assert (=> F0x7f37df6ee9d0 (or F0x7f37df6ee590 F0x7f37df6ee710)))
(assert (=> F0x7f37df6ee990 F0x7f37df6ee890))
(assert (=> pre!entry!0 (=> F0x7f37df6ee650 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f37df6ee7d0 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f37df6ee9d0 true)))
(assert (= lemma!bb2.i.i16.i.i!0 (=> F0x7f37df6ee990 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i16.i.i!0) (not lemma!bb2.i.i16.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f37df6ee650)
; (error: F0x7f37df6ee990)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i16.i.i!0)
