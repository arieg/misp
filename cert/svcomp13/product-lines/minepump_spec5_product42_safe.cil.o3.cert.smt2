(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun F0x7fa3eee5ff50 () Bool)
(declare-fun F0x7fa3eee5fe90 () Bool)
(declare-fun v0x7fa3eee5ec90_0 () Bool)
(declare-fun v0x7fa3eee5e110_0 () Bool)
(declare-fun v0x7fa3eee5d050_0 () Bool)
(declare-fun v0x7fa3eee5c190_0 () Bool)
(declare-fun v0x7fa3eee5dc10_0 () Bool)
(declare-fun v0x7fa3eee5edd0_0 () Bool)
(declare-fun v0x7fa3eee5bcd0_0 () Real)
(declare-fun v0x7fa3eee5f050_0 () Bool)
(declare-fun F0x7fa3eee5ff90 () Bool)
(declare-fun v0x7fa3eee5b110_0 () Bool)
(declare-fun E0x7fa3eee5e6d0 () Bool)
(declare-fun v0x7fa3eee5e250_0 () Real)
(declare-fun v0x7fa3eee5e390_0 () Bool)
(declare-fun v0x7fa3eee5dd50_0 () Bool)
(declare-fun v0x7fa3eee5eb50_0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun E0x7fa3eee5df50 () Bool)
(declare-fun v0x7fa3eee5cc50_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7fa3eee5ce50 () Bool)
(declare-fun v0x7fa3eee5cd90_0 () Bool)
(declare-fun E0x7fa3eee5c750 () Bool)
(declare-fun v0x7fa3eee5c4d0_0 () Real)
(declare-fun v0x7fa3eee5ac10_0 () Real)
(declare-fun v0x7fa3eee5a490_0 () Real)
(declare-fun v0x7fa3eee5a310_0 () Real)
(declare-fun E0x7fa3eee5c590 () Bool)
(declare-fun v0x7fa3eee5d490_0 () Bool)
(declare-fun v0x7fa3eee5bd90_0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun E0x7fa3eee5bf90 () Bool)
(declare-fun v0x7fa3eee5a590_0 () Real)
(declare-fun v0x7fa3eee5e450_0 () Real)
(declare-fun E0x7fa3eee5b890 () Bool)
(declare-fun E0x7fa3eee5b6d0 () Bool)
(declare-fun E0x7fa3eee5d610 () Bool)
(declare-fun v0x7fa3eee5d350_0 () Real)
(declare-fun v0x7fa3eee5b550_0 () Bool)
(declare-fun E0x7fa3eee5aed0 () Bool)
(declare-fun v0x7fa3eee5d190_0 () Real)
(declare-fun v0x7fa3eee5ae10_0 () Bool)
(declare-fun E0x7fa3eee5e510 () Bool)
(declare-fun F0x7fa3eee5fdd0 () Bool)
(declare-fun v0x7fa3eee5b610_0 () Real)
(declare-fun F0x7fa3eee5fc10 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7fa3eee5ab50_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fa3eee5d550_0 () Real)
(declare-fun F0x7fa3eee5fcd0 () Bool)
(declare-fun v0x7fa3eee59010_0 () Real)
(declare-fun v0x7fa3eee5a650_0 () Real)
(declare-fun v0x7fa3eee5b410_0 () Real)
(declare-fun v0x7fa3eee5a550_0 () Real)
(declare-fun v0x7fa3eee5ef10_0 () Bool)
(declare-fun v0x7fa3eee5c410_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fa3eee5bed0_0 () Bool)
(declare-fun v0x7fa3eee59110_0 () Bool)
(declare-fun v0x7fa3eee5de90_0 () Bool)
(declare-fun v0x7fa3eee5c2d0_0 () Real)
(declare-fun F0x7fa3eee5fd90 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7fa3eee5d7d0 () Bool)
(declare-fun v0x7fa3eee5b2d0_0 () Real)
(declare-fun v0x7fa3eee5acd0_0 () Bool)

(assert (=> F0x7fa3eee5fd90
    (and v0x7fa3eee59110_0
         (<= v0x7fa3eee5a550_0 0.0)
         (>= v0x7fa3eee5a550_0 0.0)
         (<= v0x7fa3eee5a650_0 0.0)
         (>= v0x7fa3eee5a650_0 0.0)
         (<= v0x7fa3eee59010_0 1.0)
         (>= v0x7fa3eee59010_0 1.0))))
(assert (=> F0x7fa3eee5fd90 F0x7fa3eee5fcd0))
(assert (let ((a!1 (=> v0x7fa3eee5b550_0
               (or (and v0x7fa3eee5ae10_0
                        E0x7fa3eee5b6d0
                        (<= v0x7fa3eee5b610_0 v0x7fa3eee5b410_0)
                        (>= v0x7fa3eee5b610_0 v0x7fa3eee5b410_0))
                   (and v0x7fa3eee5ab50_0
                        E0x7fa3eee5b890
                        v0x7fa3eee5acd0_0
                        (<= v0x7fa3eee5b610_0 v0x7fa3eee5a590_0)
                        (>= v0x7fa3eee5b610_0 v0x7fa3eee5a590_0)))))
      (a!2 (=> v0x7fa3eee5b550_0
               (or (and E0x7fa3eee5b6d0 (not E0x7fa3eee5b890))
                   (and E0x7fa3eee5b890 (not E0x7fa3eee5b6d0)))))
      (a!3 (=> v0x7fa3eee5c410_0
               (or (and v0x7fa3eee5bed0_0
                        E0x7fa3eee5c590
                        (<= v0x7fa3eee5c4d0_0 v0x7fa3eee5c2d0_0)
                        (>= v0x7fa3eee5c4d0_0 v0x7fa3eee5c2d0_0))
                   (and v0x7fa3eee5b550_0
                        E0x7fa3eee5c750
                        v0x7fa3eee5bd90_0
                        (<= v0x7fa3eee5c4d0_0 v0x7fa3eee5a490_0)
                        (>= v0x7fa3eee5c4d0_0 v0x7fa3eee5a490_0)))))
      (a!4 (=> v0x7fa3eee5c410_0
               (or (and E0x7fa3eee5c590 (not E0x7fa3eee5c750))
                   (and E0x7fa3eee5c750 (not E0x7fa3eee5c590)))))
      (a!5 (=> v0x7fa3eee5d490_0
               (or (and v0x7fa3eee5cd90_0
                        E0x7fa3eee5d610
                        (<= v0x7fa3eee5d550_0 v0x7fa3eee5d350_0)
                        (>= v0x7fa3eee5d550_0 v0x7fa3eee5d350_0))
                   (and v0x7fa3eee5c410_0
                        E0x7fa3eee5d7d0
                        v0x7fa3eee5cc50_0
                        (<= v0x7fa3eee5d550_0 v0x7fa3eee5b610_0)
                        (>= v0x7fa3eee5d550_0 v0x7fa3eee5b610_0)))))
      (a!6 (=> v0x7fa3eee5d490_0
               (or (and E0x7fa3eee5d610 (not E0x7fa3eee5d7d0))
                   (and E0x7fa3eee5d7d0 (not E0x7fa3eee5d610)))))
      (a!7 (=> v0x7fa3eee5e390_0
               (or (and v0x7fa3eee5de90_0
                        E0x7fa3eee5e510
                        (<= v0x7fa3eee5e450_0 v0x7fa3eee5e250_0)
                        (>= v0x7fa3eee5e450_0 v0x7fa3eee5e250_0))
                   (and v0x7fa3eee5d490_0
                        E0x7fa3eee5e6d0
                        (not v0x7fa3eee5dd50_0)
                        (<= v0x7fa3eee5e450_0 v0x7fa3eee5a310_0)
                        (>= v0x7fa3eee5e450_0 v0x7fa3eee5a310_0)))))
      (a!8 (=> v0x7fa3eee5e390_0
               (or (and E0x7fa3eee5e510 (not E0x7fa3eee5e6d0))
                   (and E0x7fa3eee5e6d0 (not E0x7fa3eee5e510))))))
(let ((a!9 (and (=> v0x7fa3eee5ae10_0
                    (and v0x7fa3eee5ab50_0
                         E0x7fa3eee5aed0
                         (not v0x7fa3eee5acd0_0)))
                (=> v0x7fa3eee5ae10_0 E0x7fa3eee5aed0)
                a!1
                a!2
                (=> v0x7fa3eee5bed0_0
                    (and v0x7fa3eee5b550_0
                         E0x7fa3eee5bf90
                         (not v0x7fa3eee5bd90_0)))
                (=> v0x7fa3eee5bed0_0 E0x7fa3eee5bf90)
                a!3
                a!4
                (=> v0x7fa3eee5cd90_0
                    (and v0x7fa3eee5c410_0
                         E0x7fa3eee5ce50
                         (not v0x7fa3eee5cc50_0)))
                (=> v0x7fa3eee5cd90_0 E0x7fa3eee5ce50)
                a!5
                a!6
                (=> v0x7fa3eee5de90_0
                    (and v0x7fa3eee5d490_0 E0x7fa3eee5df50 v0x7fa3eee5dd50_0))
                (=> v0x7fa3eee5de90_0 E0x7fa3eee5df50)
                a!7
                a!8
                v0x7fa3eee5e390_0
                (not v0x7fa3eee5f050_0)
                (<= v0x7fa3eee5a550_0 v0x7fa3eee5e450_0)
                (>= v0x7fa3eee5a550_0 v0x7fa3eee5e450_0)
                (<= v0x7fa3eee5a650_0 v0x7fa3eee5c4d0_0)
                (>= v0x7fa3eee5a650_0 v0x7fa3eee5c4d0_0)
                (<= v0x7fa3eee59010_0 v0x7fa3eee5d550_0)
                (>= v0x7fa3eee59010_0 v0x7fa3eee5d550_0)
                (= v0x7fa3eee5acd0_0 (= v0x7fa3eee5ac10_0 0.0))
                (= v0x7fa3eee5b110_0 (< v0x7fa3eee5a590_0 2.0))
                (= v0x7fa3eee5b2d0_0 (ite v0x7fa3eee5b110_0 1.0 0.0))
                (= v0x7fa3eee5b410_0 (+ v0x7fa3eee5b2d0_0 v0x7fa3eee5a590_0))
                (= v0x7fa3eee5bd90_0 (= v0x7fa3eee5bcd0_0 0.0))
                (= v0x7fa3eee5c190_0 (= v0x7fa3eee5a490_0 0.0))
                (= v0x7fa3eee5c2d0_0 (ite v0x7fa3eee5c190_0 1.0 0.0))
                (= v0x7fa3eee5cc50_0 (= v0x7fa3eee5a310_0 0.0))
                (= v0x7fa3eee5d050_0 (> v0x7fa3eee5b610_0 0.0))
                (= v0x7fa3eee5d190_0 (+ v0x7fa3eee5b610_0 (- 1.0)))
                (= v0x7fa3eee5d350_0
                   (ite v0x7fa3eee5d050_0 v0x7fa3eee5d190_0 v0x7fa3eee5b610_0))
                (= v0x7fa3eee5dc10_0 (> v0x7fa3eee5d550_0 1.0))
                (= v0x7fa3eee5dd50_0 (and v0x7fa3eee5dc10_0 v0x7fa3eee5cc50_0))
                (= v0x7fa3eee5e110_0 (= v0x7fa3eee5c4d0_0 0.0))
                (= v0x7fa3eee5e250_0
                   (ite v0x7fa3eee5e110_0 1.0 v0x7fa3eee5a310_0))
                (= v0x7fa3eee5eb50_0 (= v0x7fa3eee5d550_0 2.0))
                (= v0x7fa3eee5ec90_0 (= v0x7fa3eee5e450_0 0.0))
                (= v0x7fa3eee5edd0_0 (or v0x7fa3eee5ec90_0 v0x7fa3eee5eb50_0))
                (= v0x7fa3eee5ef10_0 (xor v0x7fa3eee5edd0_0 true))
                (= v0x7fa3eee5f050_0 (and v0x7fa3eee5cc50_0 v0x7fa3eee5ef10_0)))))
  (=> F0x7fa3eee5fc10 a!9))))
(assert (=> F0x7fa3eee5fc10 F0x7fa3eee5fdd0))
(assert (let ((a!1 (=> v0x7fa3eee5b550_0
               (or (and v0x7fa3eee5ae10_0
                        E0x7fa3eee5b6d0
                        (<= v0x7fa3eee5b610_0 v0x7fa3eee5b410_0)
                        (>= v0x7fa3eee5b610_0 v0x7fa3eee5b410_0))
                   (and v0x7fa3eee5ab50_0
                        E0x7fa3eee5b890
                        v0x7fa3eee5acd0_0
                        (<= v0x7fa3eee5b610_0 v0x7fa3eee5a590_0)
                        (>= v0x7fa3eee5b610_0 v0x7fa3eee5a590_0)))))
      (a!2 (=> v0x7fa3eee5b550_0
               (or (and E0x7fa3eee5b6d0 (not E0x7fa3eee5b890))
                   (and E0x7fa3eee5b890 (not E0x7fa3eee5b6d0)))))
      (a!3 (=> v0x7fa3eee5c410_0
               (or (and v0x7fa3eee5bed0_0
                        E0x7fa3eee5c590
                        (<= v0x7fa3eee5c4d0_0 v0x7fa3eee5c2d0_0)
                        (>= v0x7fa3eee5c4d0_0 v0x7fa3eee5c2d0_0))
                   (and v0x7fa3eee5b550_0
                        E0x7fa3eee5c750
                        v0x7fa3eee5bd90_0
                        (<= v0x7fa3eee5c4d0_0 v0x7fa3eee5a490_0)
                        (>= v0x7fa3eee5c4d0_0 v0x7fa3eee5a490_0)))))
      (a!4 (=> v0x7fa3eee5c410_0
               (or (and E0x7fa3eee5c590 (not E0x7fa3eee5c750))
                   (and E0x7fa3eee5c750 (not E0x7fa3eee5c590)))))
      (a!5 (=> v0x7fa3eee5d490_0
               (or (and v0x7fa3eee5cd90_0
                        E0x7fa3eee5d610
                        (<= v0x7fa3eee5d550_0 v0x7fa3eee5d350_0)
                        (>= v0x7fa3eee5d550_0 v0x7fa3eee5d350_0))
                   (and v0x7fa3eee5c410_0
                        E0x7fa3eee5d7d0
                        v0x7fa3eee5cc50_0
                        (<= v0x7fa3eee5d550_0 v0x7fa3eee5b610_0)
                        (>= v0x7fa3eee5d550_0 v0x7fa3eee5b610_0)))))
      (a!6 (=> v0x7fa3eee5d490_0
               (or (and E0x7fa3eee5d610 (not E0x7fa3eee5d7d0))
                   (and E0x7fa3eee5d7d0 (not E0x7fa3eee5d610)))))
      (a!7 (=> v0x7fa3eee5e390_0
               (or (and v0x7fa3eee5de90_0
                        E0x7fa3eee5e510
                        (<= v0x7fa3eee5e450_0 v0x7fa3eee5e250_0)
                        (>= v0x7fa3eee5e450_0 v0x7fa3eee5e250_0))
                   (and v0x7fa3eee5d490_0
                        E0x7fa3eee5e6d0
                        (not v0x7fa3eee5dd50_0)
                        (<= v0x7fa3eee5e450_0 v0x7fa3eee5a310_0)
                        (>= v0x7fa3eee5e450_0 v0x7fa3eee5a310_0)))))
      (a!8 (=> v0x7fa3eee5e390_0
               (or (and E0x7fa3eee5e510 (not E0x7fa3eee5e6d0))
                   (and E0x7fa3eee5e6d0 (not E0x7fa3eee5e510))))))
(let ((a!9 (and (=> v0x7fa3eee5ae10_0
                    (and v0x7fa3eee5ab50_0
                         E0x7fa3eee5aed0
                         (not v0x7fa3eee5acd0_0)))
                (=> v0x7fa3eee5ae10_0 E0x7fa3eee5aed0)
                a!1
                a!2
                (=> v0x7fa3eee5bed0_0
                    (and v0x7fa3eee5b550_0
                         E0x7fa3eee5bf90
                         (not v0x7fa3eee5bd90_0)))
                (=> v0x7fa3eee5bed0_0 E0x7fa3eee5bf90)
                a!3
                a!4
                (=> v0x7fa3eee5cd90_0
                    (and v0x7fa3eee5c410_0
                         E0x7fa3eee5ce50
                         (not v0x7fa3eee5cc50_0)))
                (=> v0x7fa3eee5cd90_0 E0x7fa3eee5ce50)
                a!5
                a!6
                (=> v0x7fa3eee5de90_0
                    (and v0x7fa3eee5d490_0 E0x7fa3eee5df50 v0x7fa3eee5dd50_0))
                (=> v0x7fa3eee5de90_0 E0x7fa3eee5df50)
                a!7
                a!8
                v0x7fa3eee5e390_0
                v0x7fa3eee5f050_0
                (= v0x7fa3eee5acd0_0 (= v0x7fa3eee5ac10_0 0.0))
                (= v0x7fa3eee5b110_0 (< v0x7fa3eee5a590_0 2.0))
                (= v0x7fa3eee5b2d0_0 (ite v0x7fa3eee5b110_0 1.0 0.0))
                (= v0x7fa3eee5b410_0 (+ v0x7fa3eee5b2d0_0 v0x7fa3eee5a590_0))
                (= v0x7fa3eee5bd90_0 (= v0x7fa3eee5bcd0_0 0.0))
                (= v0x7fa3eee5c190_0 (= v0x7fa3eee5a490_0 0.0))
                (= v0x7fa3eee5c2d0_0 (ite v0x7fa3eee5c190_0 1.0 0.0))
                (= v0x7fa3eee5cc50_0 (= v0x7fa3eee5a310_0 0.0))
                (= v0x7fa3eee5d050_0 (> v0x7fa3eee5b610_0 0.0))
                (= v0x7fa3eee5d190_0 (+ v0x7fa3eee5b610_0 (- 1.0)))
                (= v0x7fa3eee5d350_0
                   (ite v0x7fa3eee5d050_0 v0x7fa3eee5d190_0 v0x7fa3eee5b610_0))
                (= v0x7fa3eee5dc10_0 (> v0x7fa3eee5d550_0 1.0))
                (= v0x7fa3eee5dd50_0 (and v0x7fa3eee5dc10_0 v0x7fa3eee5cc50_0))
                (= v0x7fa3eee5e110_0 (= v0x7fa3eee5c4d0_0 0.0))
                (= v0x7fa3eee5e250_0
                   (ite v0x7fa3eee5e110_0 1.0 v0x7fa3eee5a310_0))
                (= v0x7fa3eee5eb50_0 (= v0x7fa3eee5d550_0 2.0))
                (= v0x7fa3eee5ec90_0 (= v0x7fa3eee5e450_0 0.0))
                (= v0x7fa3eee5edd0_0 (or v0x7fa3eee5ec90_0 v0x7fa3eee5eb50_0))
                (= v0x7fa3eee5ef10_0 (xor v0x7fa3eee5edd0_0 true))
                (= v0x7fa3eee5f050_0 (and v0x7fa3eee5cc50_0 v0x7fa3eee5ef10_0)))))
  (=> F0x7fa3eee5fe90 a!9))))
(assert (=> F0x7fa3eee5fe90 F0x7fa3eee5fdd0))
(assert (=> F0x7fa3eee5ff90 (or F0x7fa3eee5fd90 F0x7fa3eee5fc10)))
(assert (=> F0x7fa3eee5ff50 F0x7fa3eee5fe90))
(assert (=> pre!entry!0 (=> F0x7fa3eee5fcd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fa3eee5fdd0 (<= v0x7fa3eee5a590_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fa3eee5fdd0
        (or (<= v0x7fa3eee5a590_0 1.0) (>= v0x7fa3eee5a590_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fa3eee5fdd0 (>= v0x7fa3eee5a590_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fa3eee5fdd0
        (or (>= v0x7fa3eee5a590_0 1.0) (<= v0x7fa3eee5a590_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fa3eee5ff90 (<= v0x7fa3eee59010_0 2.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fa3eee5ff90
       (or (<= v0x7fa3eee59010_0 1.0) (>= v0x7fa3eee59010_0 2.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fa3eee5ff90 (>= v0x7fa3eee59010_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7fa3eee5ff90
       (or (>= v0x7fa3eee59010_0 1.0) (<= v0x7fa3eee59010_0 0.0)))))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7fa3eee5ff50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7fa3eee5fcd0)
; (error: F0x7fa3eee5ff50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i23.i.i!0)
