(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i34.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f54a0ef5c90 () Bool)
(declare-fun F0x7f54a0ef5b50 () Bool)
(declare-fun post!bb1.i.i34.i.i!0 () Bool)
(declare-fun F0x7f54a0ef5a90 () Bool)
(declare-fun v0x7f54a0ef4c10_0 () Bool)
(declare-fun v0x7f54a0ef2fd0_0 () Real)
(declare-fun v0x7f54a0ef2e90_0 () Bool)
(declare-fun v0x7f54a0ef0d50_0 () Real)
(declare-fun v0x7f54a0ef0b90_0 () Bool)
(declare-fun v0x7f54a0ef0690_0 () Real)
(declare-fun E0x7f54a0ef4510 () Bool)
(declare-fun v0x7f54a0ef3190_0 () Real)
(declare-fun E0x7f54a0ef4310 () Bool)
(declare-fun v0x7f54a0ef0010_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f54a0ef3d10_0 () Real)
(declare-fun v0x7f54a0ef3c50_0 () Real)
(declare-fun E0x7f54a0ef3dd0 () Bool)
(declare-fun v0x7f54a0ef32d0_0 () Bool)
(declare-fun E0x7f54a0ef39d0 () Bool)
(declare-fun v0x7f54a0ef2a90_0 () Bool)
(declare-fun E0x7f54a0ef2c90 () Bool)
(declare-fun v0x7f54a0ef2810_0 () Bool)
(declare-fun v0x7f54a0ef1d50_0 () Real)
(declare-fun v0x7f54a0ef4ad0_0 () Bool)
(declare-fun E0x7f54a0ef2010 () Bool)
(declare-fun E0x7f54a0ef28d0 () Bool)
(declare-fun E0x7f54a0ef1a10 () Bool)
(declare-fun E0x7f54a0ef34d0 () Bool)
(declare-fun v0x7f54a0ef2bd0_0 () Bool)
(declare-fun v0x7f54a0ef1c10_0 () Bool)
(declare-fun v0x7f54a0eeff10_0 () Real)
(declare-fun v0x7f54a0ef1810_0 () Bool)
(declare-fun v0x7f54a0ef1090_0 () Real)
(declare-fun E0x7f54a0ef1150 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f54a0ef0fd0_0 () Bool)
(declare-fun v0x7f54a0ef1750_0 () Real)
(declare-fun v0x7f54a0ef26d0_0 () Bool)
(declare-fun E0x7f54a0ef21d0 () Bool)
(declare-fun E0x7f54a0ef0950 () Bool)
(declare-fun v0x7f54a0ef1f50_0 () Real)
(declare-fun v0x7f54a0ef1950_0 () Bool)
(declare-fun v0x7f54a0ef3690_0 () Bool)
(declare-fun E0x7f54a0ef1310 () Bool)
(declare-fun v0x7f54a0ef0890_0 () Bool)
(declare-fun v0x7f54a0ef3b90_0 () Bool)
(declare-fun F0x7f54a0ef58d0 () Bool)
(declare-fun v0x7f54a0eee010_0 () Real)
(declare-fun E0x7f54a0ef4090 () Bool)
(declare-fun v0x7f54a0ef00d0_0 () Real)
(declare-fun v0x7f54a0ef37d0_0 () Real)
(declare-fun v0x7f54a0ef05d0_0 () Bool)
(declare-fun v0x7f54a0ef3910_0 () Bool)
(declare-fun F0x7f54a0ef59d0 () Bool)
(declare-fun v0x7f54a0eeffd0_0 () Real)
(declare-fun F0x7f54a0ef5c50 () Bool)
(declare-fun v0x7f54a0ef1e90_0 () Bool)
(declare-fun v0x7f54a0ef3410_0 () Bool)
(declare-fun v0x7f54a0eee110_0 () Bool)
(declare-fun v0x7f54a0ef4d50_0 () Bool)
(declare-fun v0x7f54a0ef0e90_0 () Real)
(declare-fun v0x7f54a0eefd90_0 () Real)
(declare-fun F0x7f54a0ef5990 () Bool)
(declare-fun v0x7f54a0ef0750_0 () Bool)

(assert (=> F0x7f54a0ef5990
    (and v0x7f54a0eee110_0
         (<= v0x7f54a0eeffd0_0 0.0)
         (>= v0x7f54a0eeffd0_0 0.0)
         (<= v0x7f54a0ef00d0_0 1.0)
         (>= v0x7f54a0ef00d0_0 1.0)
         (<= v0x7f54a0eee010_0 0.0)
         (>= v0x7f54a0eee010_0 0.0))))
(assert (=> F0x7f54a0ef5990 F0x7f54a0ef58d0))
(assert (let ((a!1 (=> v0x7f54a0ef0fd0_0
               (or (and v0x7f54a0ef0890_0
                        E0x7f54a0ef1150
                        (<= v0x7f54a0ef1090_0 v0x7f54a0ef0e90_0)
                        (>= v0x7f54a0ef1090_0 v0x7f54a0ef0e90_0))
                   (and v0x7f54a0ef05d0_0
                        E0x7f54a0ef1310
                        v0x7f54a0ef0750_0
                        (<= v0x7f54a0ef1090_0 v0x7f54a0eeff10_0)
                        (>= v0x7f54a0ef1090_0 v0x7f54a0eeff10_0)))))
      (a!2 (=> v0x7f54a0ef0fd0_0
               (or (and E0x7f54a0ef1150 (not E0x7f54a0ef1310))
                   (and E0x7f54a0ef1310 (not E0x7f54a0ef1150)))))
      (a!3 (=> v0x7f54a0ef1e90_0
               (or (and v0x7f54a0ef1950_0
                        E0x7f54a0ef2010
                        (<= v0x7f54a0ef1f50_0 v0x7f54a0ef1d50_0)
                        (>= v0x7f54a0ef1f50_0 v0x7f54a0ef1d50_0))
                   (and v0x7f54a0ef0fd0_0
                        E0x7f54a0ef21d0
                        v0x7f54a0ef1810_0
                        (<= v0x7f54a0ef1f50_0 v0x7f54a0eefd90_0)
                        (>= v0x7f54a0ef1f50_0 v0x7f54a0eefd90_0)))))
      (a!4 (=> v0x7f54a0ef1e90_0
               (or (and E0x7f54a0ef2010 (not E0x7f54a0ef21d0))
                   (and E0x7f54a0ef21d0 (not E0x7f54a0ef2010)))))
      (a!5 (or (and v0x7f54a0ef3410_0
                    E0x7f54a0ef3dd0
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0))
                    (<= v0x7f54a0ef3d10_0 v0x7f54a0ef37d0_0)
                    (>= v0x7f54a0ef3d10_0 v0x7f54a0ef37d0_0))
               (and v0x7f54a0ef2810_0
                    E0x7f54a0ef4090
                    (not v0x7f54a0ef2a90_0)
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0))
                    (and (<= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)
                         (>= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)))
               (and v0x7f54a0ef3910_0
                    E0x7f54a0ef4310
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0))
                    (and (<= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)
                         (>= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)))
               (and v0x7f54a0ef2bd0_0
                    E0x7f54a0ef4510
                    (not v0x7f54a0ef32d0_0)
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0))
                    (<= v0x7f54a0ef3d10_0 0.0)
                    (>= v0x7f54a0ef3d10_0 0.0))))
      (a!6 (=> v0x7f54a0ef3b90_0
               (or (and E0x7f54a0ef3dd0
                        (not E0x7f54a0ef4090)
                        (not E0x7f54a0ef4310)
                        (not E0x7f54a0ef4510))
                   (and E0x7f54a0ef4090
                        (not E0x7f54a0ef3dd0)
                        (not E0x7f54a0ef4310)
                        (not E0x7f54a0ef4510))
                   (and E0x7f54a0ef4310
                        (not E0x7f54a0ef3dd0)
                        (not E0x7f54a0ef4090)
                        (not E0x7f54a0ef4510))
                   (and E0x7f54a0ef4510
                        (not E0x7f54a0ef3dd0)
                        (not E0x7f54a0ef4090)
                        (not E0x7f54a0ef4310))))))
(let ((a!7 (and (=> v0x7f54a0ef0890_0
                    (and v0x7f54a0ef05d0_0
                         E0x7f54a0ef0950
                         (not v0x7f54a0ef0750_0)))
                (=> v0x7f54a0ef0890_0 E0x7f54a0ef0950)
                a!1
                a!2
                (=> v0x7f54a0ef1950_0
                    (and v0x7f54a0ef0fd0_0
                         E0x7f54a0ef1a10
                         (not v0x7f54a0ef1810_0)))
                (=> v0x7f54a0ef1950_0 E0x7f54a0ef1a10)
                a!3
                a!4
                (=> v0x7f54a0ef2810_0
                    (and v0x7f54a0ef1e90_0 E0x7f54a0ef28d0 v0x7f54a0ef26d0_0))
                (=> v0x7f54a0ef2810_0 E0x7f54a0ef28d0)
                (=> v0x7f54a0ef2bd0_0
                    (and v0x7f54a0ef1e90_0
                         E0x7f54a0ef2c90
                         (not v0x7f54a0ef26d0_0)))
                (=> v0x7f54a0ef2bd0_0 E0x7f54a0ef2c90)
                (=> v0x7f54a0ef3410_0
                    (and v0x7f54a0ef2810_0 E0x7f54a0ef34d0 v0x7f54a0ef2a90_0))
                (=> v0x7f54a0ef3410_0 E0x7f54a0ef34d0)
                (=> v0x7f54a0ef3910_0
                    (and v0x7f54a0ef2bd0_0 E0x7f54a0ef39d0 v0x7f54a0ef32d0_0))
                (=> v0x7f54a0ef3910_0 E0x7f54a0ef39d0)
                (=> v0x7f54a0ef3b90_0 a!5)
                a!6
                v0x7f54a0ef3b90_0
                v0x7f54a0ef4d50_0
                (<= v0x7f54a0eeffd0_0 v0x7f54a0ef1f50_0)
                (>= v0x7f54a0eeffd0_0 v0x7f54a0ef1f50_0)
                (<= v0x7f54a0ef00d0_0 v0x7f54a0ef3c50_0)
                (>= v0x7f54a0ef00d0_0 v0x7f54a0ef3c50_0)
                (<= v0x7f54a0eee010_0 v0x7f54a0ef3d10_0)
                (>= v0x7f54a0eee010_0 v0x7f54a0ef3d10_0)
                (= v0x7f54a0ef0750_0 (= v0x7f54a0ef0690_0 0.0))
                (= v0x7f54a0ef0b90_0 (< v0x7f54a0eeff10_0 2.0))
                (= v0x7f54a0ef0d50_0 (ite v0x7f54a0ef0b90_0 1.0 0.0))
                (= v0x7f54a0ef0e90_0 (+ v0x7f54a0ef0d50_0 v0x7f54a0eeff10_0))
                (= v0x7f54a0ef1810_0 (= v0x7f54a0ef1750_0 0.0))
                (= v0x7f54a0ef1c10_0 (= v0x7f54a0eefd90_0 0.0))
                (= v0x7f54a0ef1d50_0 (ite v0x7f54a0ef1c10_0 1.0 0.0))
                (= v0x7f54a0ef26d0_0 (= v0x7f54a0ef0010_0 0.0))
                (= v0x7f54a0ef2a90_0 (> v0x7f54a0ef1090_0 1.0))
                (= v0x7f54a0ef2e90_0 (> v0x7f54a0ef1090_0 0.0))
                (= v0x7f54a0ef2fd0_0 (+ v0x7f54a0ef1090_0 (- 1.0)))
                (= v0x7f54a0ef3190_0
                   (ite v0x7f54a0ef2e90_0 v0x7f54a0ef2fd0_0 v0x7f54a0ef1090_0))
                (= v0x7f54a0ef32d0_0 (= v0x7f54a0ef3190_0 0.0))
                (= v0x7f54a0ef3690_0 (= v0x7f54a0ef1f50_0 0.0))
                (= v0x7f54a0ef37d0_0
                   (ite v0x7f54a0ef3690_0 1.0 v0x7f54a0ef0010_0))
                (= v0x7f54a0ef4ad0_0 (not (= v0x7f54a0ef3c50_0 0.0)))
                (= v0x7f54a0ef4c10_0 (= v0x7f54a0ef3d10_0 0.0))
                (= v0x7f54a0ef4d50_0 (or v0x7f54a0ef4c10_0 v0x7f54a0ef4ad0_0)))))
  (=> F0x7f54a0ef59d0 a!7))))
(assert (=> F0x7f54a0ef59d0 F0x7f54a0ef5a90))
(assert (let ((a!1 (=> v0x7f54a0ef0fd0_0
               (or (and v0x7f54a0ef0890_0
                        E0x7f54a0ef1150
                        (<= v0x7f54a0ef1090_0 v0x7f54a0ef0e90_0)
                        (>= v0x7f54a0ef1090_0 v0x7f54a0ef0e90_0))
                   (and v0x7f54a0ef05d0_0
                        E0x7f54a0ef1310
                        v0x7f54a0ef0750_0
                        (<= v0x7f54a0ef1090_0 v0x7f54a0eeff10_0)
                        (>= v0x7f54a0ef1090_0 v0x7f54a0eeff10_0)))))
      (a!2 (=> v0x7f54a0ef0fd0_0
               (or (and E0x7f54a0ef1150 (not E0x7f54a0ef1310))
                   (and E0x7f54a0ef1310 (not E0x7f54a0ef1150)))))
      (a!3 (=> v0x7f54a0ef1e90_0
               (or (and v0x7f54a0ef1950_0
                        E0x7f54a0ef2010
                        (<= v0x7f54a0ef1f50_0 v0x7f54a0ef1d50_0)
                        (>= v0x7f54a0ef1f50_0 v0x7f54a0ef1d50_0))
                   (and v0x7f54a0ef0fd0_0
                        E0x7f54a0ef21d0
                        v0x7f54a0ef1810_0
                        (<= v0x7f54a0ef1f50_0 v0x7f54a0eefd90_0)
                        (>= v0x7f54a0ef1f50_0 v0x7f54a0eefd90_0)))))
      (a!4 (=> v0x7f54a0ef1e90_0
               (or (and E0x7f54a0ef2010 (not E0x7f54a0ef21d0))
                   (and E0x7f54a0ef21d0 (not E0x7f54a0ef2010)))))
      (a!5 (or (and v0x7f54a0ef3410_0
                    E0x7f54a0ef3dd0
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0))
                    (<= v0x7f54a0ef3d10_0 v0x7f54a0ef37d0_0)
                    (>= v0x7f54a0ef3d10_0 v0x7f54a0ef37d0_0))
               (and v0x7f54a0ef2810_0
                    E0x7f54a0ef4090
                    (not v0x7f54a0ef2a90_0)
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef1090_0))
                    (and (<= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)
                         (>= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)))
               (and v0x7f54a0ef3910_0
                    E0x7f54a0ef4310
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0))
                    (and (<= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)
                         (>= v0x7f54a0ef3d10_0 v0x7f54a0ef0010_0)))
               (and v0x7f54a0ef2bd0_0
                    E0x7f54a0ef4510
                    (not v0x7f54a0ef32d0_0)
                    (and (<= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0)
                         (>= v0x7f54a0ef3c50_0 v0x7f54a0ef3190_0))
                    (<= v0x7f54a0ef3d10_0 0.0)
                    (>= v0x7f54a0ef3d10_0 0.0))))
      (a!6 (=> v0x7f54a0ef3b90_0
               (or (and E0x7f54a0ef3dd0
                        (not E0x7f54a0ef4090)
                        (not E0x7f54a0ef4310)
                        (not E0x7f54a0ef4510))
                   (and E0x7f54a0ef4090
                        (not E0x7f54a0ef3dd0)
                        (not E0x7f54a0ef4310)
                        (not E0x7f54a0ef4510))
                   (and E0x7f54a0ef4310
                        (not E0x7f54a0ef3dd0)
                        (not E0x7f54a0ef4090)
                        (not E0x7f54a0ef4510))
                   (and E0x7f54a0ef4510
                        (not E0x7f54a0ef3dd0)
                        (not E0x7f54a0ef4090)
                        (not E0x7f54a0ef4310))))))
(let ((a!7 (and (=> v0x7f54a0ef0890_0
                    (and v0x7f54a0ef05d0_0
                         E0x7f54a0ef0950
                         (not v0x7f54a0ef0750_0)))
                (=> v0x7f54a0ef0890_0 E0x7f54a0ef0950)
                a!1
                a!2
                (=> v0x7f54a0ef1950_0
                    (and v0x7f54a0ef0fd0_0
                         E0x7f54a0ef1a10
                         (not v0x7f54a0ef1810_0)))
                (=> v0x7f54a0ef1950_0 E0x7f54a0ef1a10)
                a!3
                a!4
                (=> v0x7f54a0ef2810_0
                    (and v0x7f54a0ef1e90_0 E0x7f54a0ef28d0 v0x7f54a0ef26d0_0))
                (=> v0x7f54a0ef2810_0 E0x7f54a0ef28d0)
                (=> v0x7f54a0ef2bd0_0
                    (and v0x7f54a0ef1e90_0
                         E0x7f54a0ef2c90
                         (not v0x7f54a0ef26d0_0)))
                (=> v0x7f54a0ef2bd0_0 E0x7f54a0ef2c90)
                (=> v0x7f54a0ef3410_0
                    (and v0x7f54a0ef2810_0 E0x7f54a0ef34d0 v0x7f54a0ef2a90_0))
                (=> v0x7f54a0ef3410_0 E0x7f54a0ef34d0)
                (=> v0x7f54a0ef3910_0
                    (and v0x7f54a0ef2bd0_0 E0x7f54a0ef39d0 v0x7f54a0ef32d0_0))
                (=> v0x7f54a0ef3910_0 E0x7f54a0ef39d0)
                (=> v0x7f54a0ef3b90_0 a!5)
                a!6
                v0x7f54a0ef3b90_0
                (not v0x7f54a0ef4d50_0)
                (= v0x7f54a0ef0750_0 (= v0x7f54a0ef0690_0 0.0))
                (= v0x7f54a0ef0b90_0 (< v0x7f54a0eeff10_0 2.0))
                (= v0x7f54a0ef0d50_0 (ite v0x7f54a0ef0b90_0 1.0 0.0))
                (= v0x7f54a0ef0e90_0 (+ v0x7f54a0ef0d50_0 v0x7f54a0eeff10_0))
                (= v0x7f54a0ef1810_0 (= v0x7f54a0ef1750_0 0.0))
                (= v0x7f54a0ef1c10_0 (= v0x7f54a0eefd90_0 0.0))
                (= v0x7f54a0ef1d50_0 (ite v0x7f54a0ef1c10_0 1.0 0.0))
                (= v0x7f54a0ef26d0_0 (= v0x7f54a0ef0010_0 0.0))
                (= v0x7f54a0ef2a90_0 (> v0x7f54a0ef1090_0 1.0))
                (= v0x7f54a0ef2e90_0 (> v0x7f54a0ef1090_0 0.0))
                (= v0x7f54a0ef2fd0_0 (+ v0x7f54a0ef1090_0 (- 1.0)))
                (= v0x7f54a0ef3190_0
                   (ite v0x7f54a0ef2e90_0 v0x7f54a0ef2fd0_0 v0x7f54a0ef1090_0))
                (= v0x7f54a0ef32d0_0 (= v0x7f54a0ef3190_0 0.0))
                (= v0x7f54a0ef3690_0 (= v0x7f54a0ef1f50_0 0.0))
                (= v0x7f54a0ef37d0_0
                   (ite v0x7f54a0ef3690_0 1.0 v0x7f54a0ef0010_0))
                (= v0x7f54a0ef4ad0_0 (not (= v0x7f54a0ef3c50_0 0.0)))
                (= v0x7f54a0ef4c10_0 (= v0x7f54a0ef3d10_0 0.0))
                (= v0x7f54a0ef4d50_0 (or v0x7f54a0ef4c10_0 v0x7f54a0ef4ad0_0)))))
  (=> F0x7f54a0ef5b50 a!7))))
(assert (=> F0x7f54a0ef5b50 F0x7f54a0ef5a90))
(assert (=> F0x7f54a0ef5c90 (or F0x7f54a0ef5990 F0x7f54a0ef59d0)))
(assert (=> F0x7f54a0ef5c50 F0x7f54a0ef5b50))
(assert (=> pre!entry!0 (=> F0x7f54a0ef58d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f54a0ef5a90 (>= v0x7f54a0ef0010_0 0.0))))
(assert (let ((a!1 (=> F0x7f54a0ef5a90
               (or (not (<= v0x7f54a0eeff10_0 1.0)) (<= v0x7f54a0ef0010_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f54a0ef5c90 (>= v0x7f54a0eee010_0 0.0))))
(assert (let ((a!1 (=> F0x7f54a0ef5c90
               (or (not (<= v0x7f54a0ef00d0_0 1.0)) (<= v0x7f54a0eee010_0 0.0)))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i34.i.i!0 (=> F0x7f54a0ef5c50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i34.i.i!0) (not lemma!bb1.i.i34.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f54a0ef58d0)
; (error: F0x7f54a0ef5c50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i34.i.i!0)
