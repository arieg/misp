(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fe4fe380ad0 () Bool)
(declare-fun v0x7fe4fe37ae10_0 () Real)
(declare-fun v0x7fe4fe37f510_0 () Bool)
(declare-fun v0x7fe4fe37df10_0 () Bool)
(declare-fun v0x7fe4fe37bad0_0 () Bool)
(declare-fun v0x7fe4fe37b5d0_0 () Real)
(declare-fun v0x7fe4fe37fa50_0 () Bool)
(declare-fun v0x7fe4fe37f650_0 () Bool)
(declare-fun E0x7fe4fe37f850 () Bool)
(declare-fun v0x7fe4fe37ad10_0 () Real)
(declare-fun E0x7fe4fe37ec10 () Bool)
(declare-fun F0x7fe4fe380a10 () Bool)
(declare-fun v0x7fe4fe37db10_0 () Real)
(declare-fun v0x7fe4fe37f410_0 () Bool)
(declare-fun v0x7fe4fe37e350_0 () Bool)
(declare-fun E0x7fe4fe37e550 () Bool)
(declare-fun E0x7fe4fe37d810 () Bool)
(declare-fun v0x7fe4fe37e7d0_0 () Real)
(declare-fun v0x7fe4fe37e050_0 () Real)
(declare-fun v0x7fe4fe37dc50_0 () Bool)
(declare-fun v0x7fe4fe37d750_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7fe4fe37e950 () Bool)
(declare-fun v0x7fe4fe37aa90_0 () Real)
(declare-fun v0x7fe4fe37e210_0 () Real)
(declare-fun v0x7fe4fe37cdd0_0 () Bool)
(declare-fun v0x7fe4fe37ac10_0 () Real)
(declare-fun v0x7fe4fe37e490_0 () Bool)
(declare-fun v0x7fe4fe37cc90_0 () Real)
(declare-fun v0x7fe4fe37bf10_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7fe4fe37c090 () Bool)
(declare-fun E0x7fe4fe37dd10 () Bool)
(declare-fun E0x7fe4fe37d110 () Bool)
(declare-fun E0x7fe4fe37eed0 () Bool)
(declare-fun E0x7fe4fe37b890 () Bool)
(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun v0x7fe4fe37bfd0_0 () Real)
(declare-fun v0x7fe4fe37d610_0 () Bool)
(declare-fun v0x7fe4fe37b510_0 () Bool)
(declare-fun v0x7fe4fe37ce90_0 () Real)
(declare-fun E0x7fe4fe37cf50 () Bool)
(declare-fun v0x7fe4fe37e710_0 () Bool)
(declare-fun v0x7fe4fe37b690_0 () Bool)
(declare-fun v0x7fe4fe37d9d0_0 () Bool)
(declare-fun v0x7fe4fe37c890_0 () Bool)
(declare-fun v0x7fe4fe37b7d0_0 () Bool)
(declare-fun F0x7fe4fe380c10 () Bool)
(declare-fun v0x7fe4fe37bdd0_0 () Real)
(declare-fun F0x7fe4fe380950 () Bool)
(declare-fun E0x7fe4fe37c950 () Bool)
(declare-fun F0x7fe4fe380850 () Bool)
(declare-fun v0x7fe4fe379010_0 () Real)
(declare-fun v0x7fe4fe37aed0_0 () Real)
(declare-fun v0x7fe4fe37e890_0 () Real)
(declare-fun v0x7fe4fe37cb50_0 () Bool)
(declare-fun E0x7fe4fe37c250 () Bool)
(declare-fun v0x7fe4fe37add0_0 () Real)
(declare-fun v0x7fe4fe37acd0_0 () Real)
(declare-fun v0x7fe4fe379110_0 () Bool)
(declare-fun v0x7fe4fe37c750_0 () Bool)
(declare-fun v0x7fe4fe37c690_0 () Real)
(declare-fun F0x7fe4fe380bd0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun F0x7fe4fe380910 () Bool)
(declare-fun v0x7fe4fe37bc90_0 () Real)
(declare-fun v0x7fe4fe37f790_0 () Bool)

(assert (=> F0x7fe4fe380910
    (and v0x7fe4fe379110_0
         (<= v0x7fe4fe37acd0_0 0.0)
         (>= v0x7fe4fe37acd0_0 0.0)
         (<= v0x7fe4fe37add0_0 1.0)
         (>= v0x7fe4fe37add0_0 1.0)
         (<= v0x7fe4fe37aed0_0 0.0)
         (>= v0x7fe4fe37aed0_0 0.0)
         (<= v0x7fe4fe379010_0 0.0)
         (>= v0x7fe4fe379010_0 0.0))))
(assert (=> F0x7fe4fe380910 F0x7fe4fe380850))
(assert (let ((a!1 (=> v0x7fe4fe37bf10_0
               (or (and v0x7fe4fe37b7d0_0
                        E0x7fe4fe37c090
                        (<= v0x7fe4fe37bfd0_0 v0x7fe4fe37bdd0_0)
                        (>= v0x7fe4fe37bfd0_0 v0x7fe4fe37bdd0_0))
                   (and v0x7fe4fe37b510_0
                        E0x7fe4fe37c250
                        v0x7fe4fe37b690_0
                        (<= v0x7fe4fe37bfd0_0 v0x7fe4fe37ac10_0)
                        (>= v0x7fe4fe37bfd0_0 v0x7fe4fe37ac10_0)))))
      (a!2 (=> v0x7fe4fe37bf10_0
               (or (and E0x7fe4fe37c090 (not E0x7fe4fe37c250))
                   (and E0x7fe4fe37c250 (not E0x7fe4fe37c090)))))
      (a!3 (=> v0x7fe4fe37cdd0_0
               (or (and v0x7fe4fe37c890_0
                        E0x7fe4fe37cf50
                        (<= v0x7fe4fe37ce90_0 v0x7fe4fe37cc90_0)
                        (>= v0x7fe4fe37ce90_0 v0x7fe4fe37cc90_0))
                   (and v0x7fe4fe37bf10_0
                        E0x7fe4fe37d110
                        v0x7fe4fe37c750_0
                        (<= v0x7fe4fe37ce90_0 v0x7fe4fe37aa90_0)
                        (>= v0x7fe4fe37ce90_0 v0x7fe4fe37aa90_0)))))
      (a!4 (=> v0x7fe4fe37cdd0_0
               (or (and E0x7fe4fe37cf50 (not E0x7fe4fe37d110))
                   (and E0x7fe4fe37d110 (not E0x7fe4fe37cf50)))))
      (a!5 (or (and v0x7fe4fe37d750_0
                    E0x7fe4fe37e950
                    (<= v0x7fe4fe37e7d0_0 v0x7fe4fe37bfd0_0)
                    (>= v0x7fe4fe37e7d0_0 v0x7fe4fe37bfd0_0)
                    (<= v0x7fe4fe37e890_0 v0x7fe4fe37db10_0)
                    (>= v0x7fe4fe37e890_0 v0x7fe4fe37db10_0))
               (and v0x7fe4fe37e490_0
                    E0x7fe4fe37ec10
                    (and (<= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0)
                         (>= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0))
                    (<= v0x7fe4fe37e890_0 v0x7fe4fe37ad10_0)
                    (>= v0x7fe4fe37e890_0 v0x7fe4fe37ad10_0))
               (and v0x7fe4fe37dc50_0
                    E0x7fe4fe37eed0
                    (not v0x7fe4fe37e350_0)
                    (and (<= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0)
                         (>= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0))
                    (<= v0x7fe4fe37e890_0 0.0)
                    (>= v0x7fe4fe37e890_0 0.0))))
      (a!6 (=> v0x7fe4fe37e710_0
               (or (and E0x7fe4fe37e950
                        (not E0x7fe4fe37ec10)
                        (not E0x7fe4fe37eed0))
                   (and E0x7fe4fe37ec10
                        (not E0x7fe4fe37e950)
                        (not E0x7fe4fe37eed0))
                   (and E0x7fe4fe37eed0
                        (not E0x7fe4fe37e950)
                        (not E0x7fe4fe37ec10)))))
      (a!7 (or (and v0x7fe4fe37f790_0
                    v0x7fe4fe37fa50_0
                    (and (<= v0x7fe4fe37acd0_0 v0x7fe4fe37ce90_0)
                         (>= v0x7fe4fe37acd0_0 v0x7fe4fe37ce90_0))
                    (and (<= v0x7fe4fe37add0_0 v0x7fe4fe37e7d0_0)
                         (>= v0x7fe4fe37add0_0 v0x7fe4fe37e7d0_0))
                    (and (<= v0x7fe4fe37aed0_0 v0x7fe4fe37e890_0)
                         (>= v0x7fe4fe37aed0_0 v0x7fe4fe37e890_0))
                    (<= v0x7fe4fe379010_0 1.0)
                    (>= v0x7fe4fe379010_0 1.0))
               (and v0x7fe4fe37e710_0
                    v0x7fe4fe37f650_0
                    (and (<= v0x7fe4fe37acd0_0 v0x7fe4fe37ce90_0)
                         (>= v0x7fe4fe37acd0_0 v0x7fe4fe37ce90_0))
                    (and (<= v0x7fe4fe37add0_0 v0x7fe4fe37e7d0_0)
                         (>= v0x7fe4fe37add0_0 v0x7fe4fe37e7d0_0))
                    (and (<= v0x7fe4fe37aed0_0 v0x7fe4fe37e890_0)
                         (>= v0x7fe4fe37aed0_0 v0x7fe4fe37e890_0))
                    (<= v0x7fe4fe379010_0 0.0)
                    (>= v0x7fe4fe379010_0 0.0)))))
(let ((a!8 (and (=> v0x7fe4fe37b7d0_0
                    (and v0x7fe4fe37b510_0
                         E0x7fe4fe37b890
                         (not v0x7fe4fe37b690_0)))
                (=> v0x7fe4fe37b7d0_0 E0x7fe4fe37b890)
                a!1
                a!2
                (=> v0x7fe4fe37c890_0
                    (and v0x7fe4fe37bf10_0
                         E0x7fe4fe37c950
                         (not v0x7fe4fe37c750_0)))
                (=> v0x7fe4fe37c890_0 E0x7fe4fe37c950)
                a!3
                a!4
                (=> v0x7fe4fe37d750_0
                    (and v0x7fe4fe37cdd0_0 E0x7fe4fe37d810 v0x7fe4fe37d610_0))
                (=> v0x7fe4fe37d750_0 E0x7fe4fe37d810)
                (=> v0x7fe4fe37dc50_0
                    (and v0x7fe4fe37cdd0_0
                         E0x7fe4fe37dd10
                         (not v0x7fe4fe37d610_0)))
                (=> v0x7fe4fe37dc50_0 E0x7fe4fe37dd10)
                (=> v0x7fe4fe37e490_0
                    (and v0x7fe4fe37dc50_0 E0x7fe4fe37e550 v0x7fe4fe37e350_0))
                (=> v0x7fe4fe37e490_0 E0x7fe4fe37e550)
                (=> v0x7fe4fe37e710_0 a!5)
                a!6
                (=> v0x7fe4fe37f790_0
                    (and v0x7fe4fe37e710_0
                         E0x7fe4fe37f850
                         (not v0x7fe4fe37f650_0)))
                (=> v0x7fe4fe37f790_0 E0x7fe4fe37f850)
                a!7
                (= v0x7fe4fe37b690_0 (= v0x7fe4fe37b5d0_0 0.0))
                (= v0x7fe4fe37bad0_0 (< v0x7fe4fe37ac10_0 2.0))
                (= v0x7fe4fe37bc90_0 (ite v0x7fe4fe37bad0_0 1.0 0.0))
                (= v0x7fe4fe37bdd0_0 (+ v0x7fe4fe37bc90_0 v0x7fe4fe37ac10_0))
                (= v0x7fe4fe37c750_0 (= v0x7fe4fe37c690_0 0.0))
                (= v0x7fe4fe37cb50_0 (= v0x7fe4fe37aa90_0 0.0))
                (= v0x7fe4fe37cc90_0 (ite v0x7fe4fe37cb50_0 1.0 0.0))
                (= v0x7fe4fe37d610_0 (= v0x7fe4fe37ad10_0 0.0))
                (= v0x7fe4fe37d9d0_0 (> v0x7fe4fe37bfd0_0 1.0))
                (= v0x7fe4fe37db10_0
                   (ite v0x7fe4fe37d9d0_0 1.0 v0x7fe4fe37ad10_0))
                (= v0x7fe4fe37df10_0 (> v0x7fe4fe37bfd0_0 0.0))
                (= v0x7fe4fe37e050_0 (+ v0x7fe4fe37bfd0_0 (- 1.0)))
                (= v0x7fe4fe37e210_0
                   (ite v0x7fe4fe37df10_0 v0x7fe4fe37e050_0 v0x7fe4fe37bfd0_0))
                (= v0x7fe4fe37e350_0 (= v0x7fe4fe37ce90_0 0.0))
                (= v0x7fe4fe37f410_0 (= v0x7fe4fe37ce90_0 0.0))
                (= v0x7fe4fe37f510_0 (= v0x7fe4fe37e890_0 0.0))
                (= v0x7fe4fe37f650_0 (or v0x7fe4fe37f510_0 v0x7fe4fe37f410_0))
                (= v0x7fe4fe37fa50_0 (= v0x7fe4fe37ae10_0 0.0)))))
  (=> F0x7fe4fe380950 a!8))))
(assert (=> F0x7fe4fe380950 F0x7fe4fe380a10))
(assert (let ((a!1 (=> v0x7fe4fe37bf10_0
               (or (and v0x7fe4fe37b7d0_0
                        E0x7fe4fe37c090
                        (<= v0x7fe4fe37bfd0_0 v0x7fe4fe37bdd0_0)
                        (>= v0x7fe4fe37bfd0_0 v0x7fe4fe37bdd0_0))
                   (and v0x7fe4fe37b510_0
                        E0x7fe4fe37c250
                        v0x7fe4fe37b690_0
                        (<= v0x7fe4fe37bfd0_0 v0x7fe4fe37ac10_0)
                        (>= v0x7fe4fe37bfd0_0 v0x7fe4fe37ac10_0)))))
      (a!2 (=> v0x7fe4fe37bf10_0
               (or (and E0x7fe4fe37c090 (not E0x7fe4fe37c250))
                   (and E0x7fe4fe37c250 (not E0x7fe4fe37c090)))))
      (a!3 (=> v0x7fe4fe37cdd0_0
               (or (and v0x7fe4fe37c890_0
                        E0x7fe4fe37cf50
                        (<= v0x7fe4fe37ce90_0 v0x7fe4fe37cc90_0)
                        (>= v0x7fe4fe37ce90_0 v0x7fe4fe37cc90_0))
                   (and v0x7fe4fe37bf10_0
                        E0x7fe4fe37d110
                        v0x7fe4fe37c750_0
                        (<= v0x7fe4fe37ce90_0 v0x7fe4fe37aa90_0)
                        (>= v0x7fe4fe37ce90_0 v0x7fe4fe37aa90_0)))))
      (a!4 (=> v0x7fe4fe37cdd0_0
               (or (and E0x7fe4fe37cf50 (not E0x7fe4fe37d110))
                   (and E0x7fe4fe37d110 (not E0x7fe4fe37cf50)))))
      (a!5 (or (and v0x7fe4fe37d750_0
                    E0x7fe4fe37e950
                    (<= v0x7fe4fe37e7d0_0 v0x7fe4fe37bfd0_0)
                    (>= v0x7fe4fe37e7d0_0 v0x7fe4fe37bfd0_0)
                    (<= v0x7fe4fe37e890_0 v0x7fe4fe37db10_0)
                    (>= v0x7fe4fe37e890_0 v0x7fe4fe37db10_0))
               (and v0x7fe4fe37e490_0
                    E0x7fe4fe37ec10
                    (and (<= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0)
                         (>= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0))
                    (<= v0x7fe4fe37e890_0 v0x7fe4fe37ad10_0)
                    (>= v0x7fe4fe37e890_0 v0x7fe4fe37ad10_0))
               (and v0x7fe4fe37dc50_0
                    E0x7fe4fe37eed0
                    (not v0x7fe4fe37e350_0)
                    (and (<= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0)
                         (>= v0x7fe4fe37e7d0_0 v0x7fe4fe37e210_0))
                    (<= v0x7fe4fe37e890_0 0.0)
                    (>= v0x7fe4fe37e890_0 0.0))))
      (a!6 (=> v0x7fe4fe37e710_0
               (or (and E0x7fe4fe37e950
                        (not E0x7fe4fe37ec10)
                        (not E0x7fe4fe37eed0))
                   (and E0x7fe4fe37ec10
                        (not E0x7fe4fe37e950)
                        (not E0x7fe4fe37eed0))
                   (and E0x7fe4fe37eed0
                        (not E0x7fe4fe37e950)
                        (not E0x7fe4fe37ec10))))))
(let ((a!7 (and (=> v0x7fe4fe37b7d0_0
                    (and v0x7fe4fe37b510_0
                         E0x7fe4fe37b890
                         (not v0x7fe4fe37b690_0)))
                (=> v0x7fe4fe37b7d0_0 E0x7fe4fe37b890)
                a!1
                a!2
                (=> v0x7fe4fe37c890_0
                    (and v0x7fe4fe37bf10_0
                         E0x7fe4fe37c950
                         (not v0x7fe4fe37c750_0)))
                (=> v0x7fe4fe37c890_0 E0x7fe4fe37c950)
                a!3
                a!4
                (=> v0x7fe4fe37d750_0
                    (and v0x7fe4fe37cdd0_0 E0x7fe4fe37d810 v0x7fe4fe37d610_0))
                (=> v0x7fe4fe37d750_0 E0x7fe4fe37d810)
                (=> v0x7fe4fe37dc50_0
                    (and v0x7fe4fe37cdd0_0
                         E0x7fe4fe37dd10
                         (not v0x7fe4fe37d610_0)))
                (=> v0x7fe4fe37dc50_0 E0x7fe4fe37dd10)
                (=> v0x7fe4fe37e490_0
                    (and v0x7fe4fe37dc50_0 E0x7fe4fe37e550 v0x7fe4fe37e350_0))
                (=> v0x7fe4fe37e490_0 E0x7fe4fe37e550)
                (=> v0x7fe4fe37e710_0 a!5)
                a!6
                (=> v0x7fe4fe37f790_0
                    (and v0x7fe4fe37e710_0
                         E0x7fe4fe37f850
                         (not v0x7fe4fe37f650_0)))
                (=> v0x7fe4fe37f790_0 E0x7fe4fe37f850)
                v0x7fe4fe37f790_0
                (not v0x7fe4fe37fa50_0)
                (= v0x7fe4fe37b690_0 (= v0x7fe4fe37b5d0_0 0.0))
                (= v0x7fe4fe37bad0_0 (< v0x7fe4fe37ac10_0 2.0))
                (= v0x7fe4fe37bc90_0 (ite v0x7fe4fe37bad0_0 1.0 0.0))
                (= v0x7fe4fe37bdd0_0 (+ v0x7fe4fe37bc90_0 v0x7fe4fe37ac10_0))
                (= v0x7fe4fe37c750_0 (= v0x7fe4fe37c690_0 0.0))
                (= v0x7fe4fe37cb50_0 (= v0x7fe4fe37aa90_0 0.0))
                (= v0x7fe4fe37cc90_0 (ite v0x7fe4fe37cb50_0 1.0 0.0))
                (= v0x7fe4fe37d610_0 (= v0x7fe4fe37ad10_0 0.0))
                (= v0x7fe4fe37d9d0_0 (> v0x7fe4fe37bfd0_0 1.0))
                (= v0x7fe4fe37db10_0
                   (ite v0x7fe4fe37d9d0_0 1.0 v0x7fe4fe37ad10_0))
                (= v0x7fe4fe37df10_0 (> v0x7fe4fe37bfd0_0 0.0))
                (= v0x7fe4fe37e050_0 (+ v0x7fe4fe37bfd0_0 (- 1.0)))
                (= v0x7fe4fe37e210_0
                   (ite v0x7fe4fe37df10_0 v0x7fe4fe37e050_0 v0x7fe4fe37bfd0_0))
                (= v0x7fe4fe37e350_0 (= v0x7fe4fe37ce90_0 0.0))
                (= v0x7fe4fe37f410_0 (= v0x7fe4fe37ce90_0 0.0))
                (= v0x7fe4fe37f510_0 (= v0x7fe4fe37e890_0 0.0))
                (= v0x7fe4fe37f650_0 (or v0x7fe4fe37f510_0 v0x7fe4fe37f410_0))
                (= v0x7fe4fe37fa50_0 (= v0x7fe4fe37ae10_0 0.0)))))
  (=> F0x7fe4fe380ad0 a!7))))
(assert (=> F0x7fe4fe380ad0 F0x7fe4fe380a10))
(assert (=> F0x7fe4fe380c10 (or F0x7fe4fe380910 F0x7fe4fe380950)))
(assert (=> F0x7fe4fe380bd0 F0x7fe4fe380ad0))
(assert (=> pre!entry!0 (=> F0x7fe4fe380850 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fe4fe380a10 (>= v0x7fe4fe37ae10_0 0.0))))
(assert (let ((a!1 (=> F0x7fe4fe380a10
               (or (<= v0x7fe4fe37ae10_0 0.0) (not (<= v0x7fe4fe37ad10_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fe4fe380c10 (>= v0x7fe4fe379010_0 0.0))))
(assert (let ((a!1 (=> F0x7fe4fe380c10
               (or (<= v0x7fe4fe379010_0 0.0) (not (<= v0x7fe4fe37aed0_0 0.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7fe4fe380bd0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7fe4fe380850)
; (error: F0x7fe4fe380bd0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i23.i.i!0)
