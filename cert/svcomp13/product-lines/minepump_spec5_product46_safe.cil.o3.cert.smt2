(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun post!bb2.i.i31.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7feb6eb80ed0 () Bool)
(declare-fun F0x7feb6eb80dd0 () Bool)
(declare-fun v0x7feb6eb7fe50_0 () Bool)
(declare-fun v0x7feb6eb7dfd0_0 () Real)
(declare-fun v0x7feb6eb7cc10_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7feb6eb7c750_0 () Real)
(declare-fun v0x7feb6eb7bd50_0 () Real)
(declare-fun v0x7feb6eb7bb90_0 () Bool)
(declare-fun E0x7feb6eb7f4d0 () Bool)
(declare-fun v0x7feb6eb7fd10_0 () Bool)
(declare-fun E0x7feb6eb7f2d0 () Bool)
(declare-fun E0x7feb6eb7f050 () Bool)
(declare-fun F0x7feb6eb80e90 () Bool)
(declare-fun v0x7feb6eb7ecd0_0 () Real)
(declare-fun v0x7feb6eb7ec10_0 () Real)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun E0x7feb6eb7ed90 () Bool)
(declare-fun lemma!bb2.i.i31.i.i!0 () Bool)
(declare-fun v0x7feb6eb7e8d0_0 () Bool)
(declare-fun v0x7feb6eb7e190_0 () Real)
(declare-fun v0x7feb6eb7ff90_0 () Bool)
(declare-fun E0x7feb6eb7e4d0 () Bool)
(declare-fun v0x7feb6eb7eb50_0 () Bool)
(declare-fun v0x7feb6eb7af10_0 () Real)
(declare-fun v0x7feb6eb7d6d0_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7feb6eb7d8d0 () Bool)
(declare-fun F0x7feb6eb80ad0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7feb6eb7d1d0 () Bool)
(declare-fun v0x7feb6eb7e790_0 () Real)
(declare-fun v0x7feb6eb7cf50_0 () Real)
(declare-fun E0x7feb6eb7ca10 () Bool)
(declare-fun v0x7feb6eb7fa90_0 () Bool)
(declare-fun v0x7feb6eb7da90_0 () Bool)
(declare-fun v0x7feb6eb7de90_0 () Bool)
(declare-fun v0x7feb6eb7b010_0 () Real)
(declare-fun v0x7feb6eb7ce90_0 () Bool)
(declare-fun v0x7feb6eb7ad90_0 () Real)
(declare-fun v0x7feb6eb7be90_0 () Real)
(declare-fun E0x7feb6eb7b950 () Bool)
(declare-fun v0x7feb6eb7c810_0 () Bool)
(declare-fun v0x7feb6eb7d810_0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7feb6eb7b5d0_0 () Bool)
(declare-fun v0x7feb6eb7b890_0 () Bool)
(declare-fun E0x7feb6eb7c150 () Bool)
(declare-fun F0x7feb6eb80c50 () Bool)
(declare-fun F0x7feb6eb80b90 () Bool)
(declare-fun v0x7feb6eb79010_0 () Real)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun v0x7feb6eb7b750_0 () Bool)
(declare-fun v0x7feb6eb7e690_0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun E0x7feb6eb7c310 () Bool)
(declare-fun v0x7feb6eb7b0d0_0 () Real)
(declare-fun v0x7feb6eb7fbd0_0 () Bool)
(declare-fun v0x7feb6eb7cd50_0 () Real)
(declare-fun v0x7feb6eb7bfd0_0 () Bool)
(declare-fun E0x7feb6eb7d010 () Bool)
(declare-fun E0x7feb6eb7dc90 () Bool)
(declare-fun E0x7feb6eb7e990 () Bool)
(declare-fun v0x7feb6eb7dbd0_0 () Bool)
(declare-fun v0x7feb6eb7afd0_0 () Real)
(declare-fun v0x7feb6eb7e410_0 () Bool)
(declare-fun v0x7feb6eb7c090_0 () Real)
(declare-fun v0x7feb6eb7e2d0_0 () Bool)
(declare-fun v0x7feb6eb79110_0 () Bool)
(declare-fun v0x7feb6eb7c950_0 () Bool)
(declare-fun F0x7feb6eb80d10 () Bool)
(declare-fun v0x7feb6eb7b690_0 () Real)

(assert (=> F0x7feb6eb80d10
    (and v0x7feb6eb79110_0
         (<= v0x7feb6eb7afd0_0 0.0)
         (>= v0x7feb6eb7afd0_0 0.0)
         (<= v0x7feb6eb7b0d0_0 0.0)
         (>= v0x7feb6eb7b0d0_0 0.0)
         (<= v0x7feb6eb79010_0 1.0)
         (>= v0x7feb6eb79010_0 1.0))))
(assert (=> F0x7feb6eb80d10 F0x7feb6eb80c50))
(assert (let ((a!1 (=> v0x7feb6eb7bfd0_0
               (or (and v0x7feb6eb7b890_0
                        E0x7feb6eb7c150
                        (<= v0x7feb6eb7c090_0 v0x7feb6eb7be90_0)
                        (>= v0x7feb6eb7c090_0 v0x7feb6eb7be90_0))
                   (and v0x7feb6eb7b5d0_0
                        E0x7feb6eb7c310
                        v0x7feb6eb7b750_0
                        (<= v0x7feb6eb7c090_0 v0x7feb6eb7b010_0)
                        (>= v0x7feb6eb7c090_0 v0x7feb6eb7b010_0)))))
      (a!2 (=> v0x7feb6eb7bfd0_0
               (or (and E0x7feb6eb7c150 (not E0x7feb6eb7c310))
                   (and E0x7feb6eb7c310 (not E0x7feb6eb7c150)))))
      (a!3 (=> v0x7feb6eb7ce90_0
               (or (and v0x7feb6eb7c950_0
                        E0x7feb6eb7d010
                        (<= v0x7feb6eb7cf50_0 v0x7feb6eb7cd50_0)
                        (>= v0x7feb6eb7cf50_0 v0x7feb6eb7cd50_0))
                   (and v0x7feb6eb7bfd0_0
                        E0x7feb6eb7d1d0
                        v0x7feb6eb7c810_0
                        (<= v0x7feb6eb7cf50_0 v0x7feb6eb7af10_0)
                        (>= v0x7feb6eb7cf50_0 v0x7feb6eb7af10_0)))))
      (a!4 (=> v0x7feb6eb7ce90_0
               (or (and E0x7feb6eb7d010 (not E0x7feb6eb7d1d0))
                   (and E0x7feb6eb7d1d0 (not E0x7feb6eb7d010)))))
      (a!5 (or (and v0x7feb6eb7e410_0
                    E0x7feb6eb7ed90
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0))
                    (<= v0x7feb6eb7ecd0_0 v0x7feb6eb7e790_0)
                    (>= v0x7feb6eb7ecd0_0 v0x7feb6eb7e790_0))
               (and v0x7feb6eb7d810_0
                    E0x7feb6eb7f050
                    (not v0x7feb6eb7da90_0)
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0))
                    (and (<= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)
                         (>= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)))
               (and v0x7feb6eb7e8d0_0
                    E0x7feb6eb7f2d0
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0))
                    (and (<= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)
                         (>= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)))
               (and v0x7feb6eb7dbd0_0
                    E0x7feb6eb7f4d0
                    (not v0x7feb6eb7e2d0_0)
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0))
                    (<= v0x7feb6eb7ecd0_0 0.0)
                    (>= v0x7feb6eb7ecd0_0 0.0))))
      (a!6 (=> v0x7feb6eb7eb50_0
               (or (and E0x7feb6eb7ed90
                        (not E0x7feb6eb7f050)
                        (not E0x7feb6eb7f2d0)
                        (not E0x7feb6eb7f4d0))
                   (and E0x7feb6eb7f050
                        (not E0x7feb6eb7ed90)
                        (not E0x7feb6eb7f2d0)
                        (not E0x7feb6eb7f4d0))
                   (and E0x7feb6eb7f2d0
                        (not E0x7feb6eb7ed90)
                        (not E0x7feb6eb7f050)
                        (not E0x7feb6eb7f4d0))
                   (and E0x7feb6eb7f4d0
                        (not E0x7feb6eb7ed90)
                        (not E0x7feb6eb7f050)
                        (not E0x7feb6eb7f2d0))))))
(let ((a!7 (and (=> v0x7feb6eb7b890_0
                    (and v0x7feb6eb7b5d0_0
                         E0x7feb6eb7b950
                         (not v0x7feb6eb7b750_0)))
                (=> v0x7feb6eb7b890_0 E0x7feb6eb7b950)
                a!1
                a!2
                (=> v0x7feb6eb7c950_0
                    (and v0x7feb6eb7bfd0_0
                         E0x7feb6eb7ca10
                         (not v0x7feb6eb7c810_0)))
                (=> v0x7feb6eb7c950_0 E0x7feb6eb7ca10)
                a!3
                a!4
                (=> v0x7feb6eb7d810_0
                    (and v0x7feb6eb7ce90_0 E0x7feb6eb7d8d0 v0x7feb6eb7d6d0_0))
                (=> v0x7feb6eb7d810_0 E0x7feb6eb7d8d0)
                (=> v0x7feb6eb7dbd0_0
                    (and v0x7feb6eb7ce90_0
                         E0x7feb6eb7dc90
                         (not v0x7feb6eb7d6d0_0)))
                (=> v0x7feb6eb7dbd0_0 E0x7feb6eb7dc90)
                (=> v0x7feb6eb7e410_0
                    (and v0x7feb6eb7d810_0 E0x7feb6eb7e4d0 v0x7feb6eb7da90_0))
                (=> v0x7feb6eb7e410_0 E0x7feb6eb7e4d0)
                (=> v0x7feb6eb7e8d0_0
                    (and v0x7feb6eb7dbd0_0 E0x7feb6eb7e990 v0x7feb6eb7e2d0_0))
                (=> v0x7feb6eb7e8d0_0 E0x7feb6eb7e990)
                (=> v0x7feb6eb7eb50_0 a!5)
                a!6
                v0x7feb6eb7eb50_0
                (not v0x7feb6eb7ff90_0)
                (<= v0x7feb6eb7afd0_0 v0x7feb6eb7ecd0_0)
                (>= v0x7feb6eb7afd0_0 v0x7feb6eb7ecd0_0)
                (<= v0x7feb6eb7b0d0_0 v0x7feb6eb7cf50_0)
                (>= v0x7feb6eb7b0d0_0 v0x7feb6eb7cf50_0)
                (<= v0x7feb6eb79010_0 v0x7feb6eb7ec10_0)
                (>= v0x7feb6eb79010_0 v0x7feb6eb7ec10_0)
                (= v0x7feb6eb7b750_0 (= v0x7feb6eb7b690_0 0.0))
                (= v0x7feb6eb7bb90_0 (< v0x7feb6eb7b010_0 2.0))
                (= v0x7feb6eb7bd50_0 (ite v0x7feb6eb7bb90_0 1.0 0.0))
                (= v0x7feb6eb7be90_0 (+ v0x7feb6eb7bd50_0 v0x7feb6eb7b010_0))
                (= v0x7feb6eb7c810_0 (= v0x7feb6eb7c750_0 0.0))
                (= v0x7feb6eb7cc10_0 (= v0x7feb6eb7af10_0 0.0))
                (= v0x7feb6eb7cd50_0 (ite v0x7feb6eb7cc10_0 1.0 0.0))
                (= v0x7feb6eb7d6d0_0 (= v0x7feb6eb7ad90_0 0.0))
                (= v0x7feb6eb7da90_0 (> v0x7feb6eb7c090_0 1.0))
                (= v0x7feb6eb7de90_0 (> v0x7feb6eb7c090_0 0.0))
                (= v0x7feb6eb7dfd0_0 (+ v0x7feb6eb7c090_0 (- 1.0)))
                (= v0x7feb6eb7e190_0
                   (ite v0x7feb6eb7de90_0 v0x7feb6eb7dfd0_0 v0x7feb6eb7c090_0))
                (= v0x7feb6eb7e2d0_0 (= v0x7feb6eb7cf50_0 0.0))
                (= v0x7feb6eb7e690_0 (= v0x7feb6eb7cf50_0 0.0))
                (= v0x7feb6eb7e790_0
                   (ite v0x7feb6eb7e690_0 1.0 v0x7feb6eb7ad90_0))
                (= v0x7feb6eb7fa90_0 (= v0x7feb6eb7ec10_0 2.0))
                (= v0x7feb6eb7fbd0_0 (= v0x7feb6eb7ecd0_0 0.0))
                (= v0x7feb6eb7fd10_0 (or v0x7feb6eb7fbd0_0 v0x7feb6eb7fa90_0))
                (= v0x7feb6eb7fe50_0 (xor v0x7feb6eb7fd10_0 true))
                (= v0x7feb6eb7ff90_0 (and v0x7feb6eb7d6d0_0 v0x7feb6eb7fe50_0)))))
  (=> F0x7feb6eb80b90 a!7))))
(assert (=> F0x7feb6eb80b90 F0x7feb6eb80ad0))
(assert (let ((a!1 (=> v0x7feb6eb7bfd0_0
               (or (and v0x7feb6eb7b890_0
                        E0x7feb6eb7c150
                        (<= v0x7feb6eb7c090_0 v0x7feb6eb7be90_0)
                        (>= v0x7feb6eb7c090_0 v0x7feb6eb7be90_0))
                   (and v0x7feb6eb7b5d0_0
                        E0x7feb6eb7c310
                        v0x7feb6eb7b750_0
                        (<= v0x7feb6eb7c090_0 v0x7feb6eb7b010_0)
                        (>= v0x7feb6eb7c090_0 v0x7feb6eb7b010_0)))))
      (a!2 (=> v0x7feb6eb7bfd0_0
               (or (and E0x7feb6eb7c150 (not E0x7feb6eb7c310))
                   (and E0x7feb6eb7c310 (not E0x7feb6eb7c150)))))
      (a!3 (=> v0x7feb6eb7ce90_0
               (or (and v0x7feb6eb7c950_0
                        E0x7feb6eb7d010
                        (<= v0x7feb6eb7cf50_0 v0x7feb6eb7cd50_0)
                        (>= v0x7feb6eb7cf50_0 v0x7feb6eb7cd50_0))
                   (and v0x7feb6eb7bfd0_0
                        E0x7feb6eb7d1d0
                        v0x7feb6eb7c810_0
                        (<= v0x7feb6eb7cf50_0 v0x7feb6eb7af10_0)
                        (>= v0x7feb6eb7cf50_0 v0x7feb6eb7af10_0)))))
      (a!4 (=> v0x7feb6eb7ce90_0
               (or (and E0x7feb6eb7d010 (not E0x7feb6eb7d1d0))
                   (and E0x7feb6eb7d1d0 (not E0x7feb6eb7d010)))))
      (a!5 (or (and v0x7feb6eb7e410_0
                    E0x7feb6eb7ed90
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0))
                    (<= v0x7feb6eb7ecd0_0 v0x7feb6eb7e790_0)
                    (>= v0x7feb6eb7ecd0_0 v0x7feb6eb7e790_0))
               (and v0x7feb6eb7d810_0
                    E0x7feb6eb7f050
                    (not v0x7feb6eb7da90_0)
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7c090_0))
                    (and (<= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)
                         (>= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)))
               (and v0x7feb6eb7e8d0_0
                    E0x7feb6eb7f2d0
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0))
                    (and (<= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)
                         (>= v0x7feb6eb7ecd0_0 v0x7feb6eb7ad90_0)))
               (and v0x7feb6eb7dbd0_0
                    E0x7feb6eb7f4d0
                    (not v0x7feb6eb7e2d0_0)
                    (and (<= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0)
                         (>= v0x7feb6eb7ec10_0 v0x7feb6eb7e190_0))
                    (<= v0x7feb6eb7ecd0_0 0.0)
                    (>= v0x7feb6eb7ecd0_0 0.0))))
      (a!6 (=> v0x7feb6eb7eb50_0
               (or (and E0x7feb6eb7ed90
                        (not E0x7feb6eb7f050)
                        (not E0x7feb6eb7f2d0)
                        (not E0x7feb6eb7f4d0))
                   (and E0x7feb6eb7f050
                        (not E0x7feb6eb7ed90)
                        (not E0x7feb6eb7f2d0)
                        (not E0x7feb6eb7f4d0))
                   (and E0x7feb6eb7f2d0
                        (not E0x7feb6eb7ed90)
                        (not E0x7feb6eb7f050)
                        (not E0x7feb6eb7f4d0))
                   (and E0x7feb6eb7f4d0
                        (not E0x7feb6eb7ed90)
                        (not E0x7feb6eb7f050)
                        (not E0x7feb6eb7f2d0))))))
(let ((a!7 (and (=> v0x7feb6eb7b890_0
                    (and v0x7feb6eb7b5d0_0
                         E0x7feb6eb7b950
                         (not v0x7feb6eb7b750_0)))
                (=> v0x7feb6eb7b890_0 E0x7feb6eb7b950)
                a!1
                a!2
                (=> v0x7feb6eb7c950_0
                    (and v0x7feb6eb7bfd0_0
                         E0x7feb6eb7ca10
                         (not v0x7feb6eb7c810_0)))
                (=> v0x7feb6eb7c950_0 E0x7feb6eb7ca10)
                a!3
                a!4
                (=> v0x7feb6eb7d810_0
                    (and v0x7feb6eb7ce90_0 E0x7feb6eb7d8d0 v0x7feb6eb7d6d0_0))
                (=> v0x7feb6eb7d810_0 E0x7feb6eb7d8d0)
                (=> v0x7feb6eb7dbd0_0
                    (and v0x7feb6eb7ce90_0
                         E0x7feb6eb7dc90
                         (not v0x7feb6eb7d6d0_0)))
                (=> v0x7feb6eb7dbd0_0 E0x7feb6eb7dc90)
                (=> v0x7feb6eb7e410_0
                    (and v0x7feb6eb7d810_0 E0x7feb6eb7e4d0 v0x7feb6eb7da90_0))
                (=> v0x7feb6eb7e410_0 E0x7feb6eb7e4d0)
                (=> v0x7feb6eb7e8d0_0
                    (and v0x7feb6eb7dbd0_0 E0x7feb6eb7e990 v0x7feb6eb7e2d0_0))
                (=> v0x7feb6eb7e8d0_0 E0x7feb6eb7e990)
                (=> v0x7feb6eb7eb50_0 a!5)
                a!6
                v0x7feb6eb7eb50_0
                v0x7feb6eb7ff90_0
                (= v0x7feb6eb7b750_0 (= v0x7feb6eb7b690_0 0.0))
                (= v0x7feb6eb7bb90_0 (< v0x7feb6eb7b010_0 2.0))
                (= v0x7feb6eb7bd50_0 (ite v0x7feb6eb7bb90_0 1.0 0.0))
                (= v0x7feb6eb7be90_0 (+ v0x7feb6eb7bd50_0 v0x7feb6eb7b010_0))
                (= v0x7feb6eb7c810_0 (= v0x7feb6eb7c750_0 0.0))
                (= v0x7feb6eb7cc10_0 (= v0x7feb6eb7af10_0 0.0))
                (= v0x7feb6eb7cd50_0 (ite v0x7feb6eb7cc10_0 1.0 0.0))
                (= v0x7feb6eb7d6d0_0 (= v0x7feb6eb7ad90_0 0.0))
                (= v0x7feb6eb7da90_0 (> v0x7feb6eb7c090_0 1.0))
                (= v0x7feb6eb7de90_0 (> v0x7feb6eb7c090_0 0.0))
                (= v0x7feb6eb7dfd0_0 (+ v0x7feb6eb7c090_0 (- 1.0)))
                (= v0x7feb6eb7e190_0
                   (ite v0x7feb6eb7de90_0 v0x7feb6eb7dfd0_0 v0x7feb6eb7c090_0))
                (= v0x7feb6eb7e2d0_0 (= v0x7feb6eb7cf50_0 0.0))
                (= v0x7feb6eb7e690_0 (= v0x7feb6eb7cf50_0 0.0))
                (= v0x7feb6eb7e790_0
                   (ite v0x7feb6eb7e690_0 1.0 v0x7feb6eb7ad90_0))
                (= v0x7feb6eb7fa90_0 (= v0x7feb6eb7ec10_0 2.0))
                (= v0x7feb6eb7fbd0_0 (= v0x7feb6eb7ecd0_0 0.0))
                (= v0x7feb6eb7fd10_0 (or v0x7feb6eb7fbd0_0 v0x7feb6eb7fa90_0))
                (= v0x7feb6eb7fe50_0 (xor v0x7feb6eb7fd10_0 true))
                (= v0x7feb6eb7ff90_0 (and v0x7feb6eb7d6d0_0 v0x7feb6eb7fe50_0)))))
  (=> F0x7feb6eb80dd0 a!7))))
(assert (=> F0x7feb6eb80dd0 F0x7feb6eb80ad0))
(assert (=> F0x7feb6eb80ed0 (or F0x7feb6eb80d10 F0x7feb6eb80b90)))
(assert (=> F0x7feb6eb80e90 F0x7feb6eb80dd0))
(assert (=> pre!entry!0 (=> F0x7feb6eb80c50 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7feb6eb80ad0 (>= v0x7feb6eb7af10_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7feb6eb80ad0
        (or (>= v0x7feb6eb7b010_0 2.0) (<= v0x7feb6eb7b010_0 1.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7feb6eb80ad0 (>= v0x7feb6eb7b010_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7feb6eb80ad0
        (or (<= v0x7feb6eb7b010_0 0.0) (>= v0x7feb6eb7b010_0 1.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7feb6eb80ad0 (<= v0x7feb6eb7b010_0 2.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7feb6eb80ed0 (>= v0x7feb6eb7b0d0_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7feb6eb80ed0
       (or (>= v0x7feb6eb79010_0 2.0) (<= v0x7feb6eb79010_0 1.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7feb6eb80ed0 (>= v0x7feb6eb79010_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7feb6eb80ed0
       (or (<= v0x7feb6eb79010_0 0.0) (>= v0x7feb6eb79010_0 1.0)))))
(assert (= lemma!bb1.i.i!4 (=> F0x7feb6eb80ed0 (<= v0x7feb6eb79010_0 2.0))))
(assert (= lemma!bb2.i.i31.i.i!0 (=> F0x7feb6eb80e90 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb2.i.i31.i.i!0) (not lemma!bb2.i.i31.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
; (init: F0x7feb6eb80c50)
; (error: F0x7feb6eb80e90)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i31.i.i!0)
