(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i26.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f16f0795210 () Bool)
(declare-fun v0x7f16f0792dd0_0 () Real)
(declare-fun v0x7f16f0792750_0 () Bool)
(declare-fun v0x7f16f0791810_0 () Real)
(declare-fun v0x7f16f0791150_0 () Real)
(declare-fun E0x7f16f0793c50 () Bool)
(declare-fun v0x7f16f07942d0_0 () Bool)
(declare-fun v0x7f16f0794410_0 () Bool)
(declare-fun v0x7f16f0792f90_0 () Real)
(declare-fun v0x7f16f0793610_0 () Real)
(declare-fun v0x7f16f0793550_0 () Real)
(declare-fun v0x7f16f07930d0_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f16f07936d0 () Bool)
(declare-fun v0x7f16f0793210_0 () Bool)
(declare-fun E0x7f16f07932d0 () Bool)
(declare-fun lemma!bb1.i.i26.i.i!0 () Bool)
(declare-fun F0x7f16f0795250 () Bool)
(declare-fun E0x7f16f0793990 () Bool)
(declare-fun v0x7f16f0794190_0 () Bool)
(declare-fun v0x7f16f0792390_0 () Bool)
(declare-fun E0x7f16f0792590 () Bool)
(declare-fun E0x7f16f0791dd0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f16f0791b50_0 () Real)
(declare-fun v0x7f16f0791a90_0 () Bool)
(declare-fun v0x7f16f0791210_0 () Bool)
(declare-fun E0x7f16f0791410 () Bool)
(declare-fun v0x7f16f0791090_0 () Bool)
(declare-fun v0x7f16f0792890_0 () Real)
(declare-fun F0x7f16f0795110 () Bool)
(declare-fun v0x7f16f0791350_0 () Bool)
(declare-fun F0x7f16f0794e90 () Bool)
(declare-fun v0x7f16f078f010_0 () Real)
(declare-fun v0x7f16f0792c90_0 () Bool)
(declare-fun F0x7f16f0794f90 () Bool)
(declare-fun E0x7f16f0792a90 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f16f0790cd0_0 () Real)
(declare-fun v0x7f16f07924d0_0 () Bool)
(declare-fun v0x7f16f07929d0_0 () Bool)
(declare-fun F0x7f16f0795050 () Bool)
(declare-fun v0x7f16f0790c10_0 () Real)
(declare-fun v0x7f16f0791650_0 () Bool)
(declare-fun v0x7f16f0790a90_0 () Real)
(declare-fun v0x7f16f078f110_0 () Bool)
(declare-fun v0x7f16f0791950_0 () Real)
(declare-fun E0x7f16f0791c10 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f16f0793490_0 () Bool)
(declare-fun F0x7f16f0794f50 () Bool)

(assert (=> F0x7f16f0794f50
    (and v0x7f16f078f110_0
         (<= v0x7f16f0790cd0_0 0.0)
         (>= v0x7f16f0790cd0_0 0.0)
         (<= v0x7f16f078f010_0 1.0)
         (>= v0x7f16f078f010_0 1.0))))
(assert (=> F0x7f16f0794f50 F0x7f16f0794e90))
(assert (let ((a!1 (=> v0x7f16f0791a90_0
               (or (and v0x7f16f0791350_0
                        E0x7f16f0791c10
                        (<= v0x7f16f0791b50_0 v0x7f16f0791950_0)
                        (>= v0x7f16f0791b50_0 v0x7f16f0791950_0))
                   (and v0x7f16f0791090_0
                        E0x7f16f0791dd0
                        v0x7f16f0791210_0
                        (<= v0x7f16f0791b50_0 v0x7f16f0790c10_0)
                        (>= v0x7f16f0791b50_0 v0x7f16f0790c10_0)))))
      (a!2 (=> v0x7f16f0791a90_0
               (or (and E0x7f16f0791c10 (not E0x7f16f0791dd0))
                   (and E0x7f16f0791dd0 (not E0x7f16f0791c10)))))
      (a!3 (or (and v0x7f16f07924d0_0
                    E0x7f16f07936d0
                    (<= v0x7f16f0793550_0 v0x7f16f0791b50_0)
                    (>= v0x7f16f0793550_0 v0x7f16f0791b50_0)
                    (<= v0x7f16f0793610_0 v0x7f16f0792890_0)
                    (>= v0x7f16f0793610_0 v0x7f16f0792890_0))
               (and v0x7f16f0793210_0
                    E0x7f16f0793990
                    (and (<= v0x7f16f0793550_0 v0x7f16f0792f90_0)
                         (>= v0x7f16f0793550_0 v0x7f16f0792f90_0))
                    (<= v0x7f16f0793610_0 v0x7f16f0790a90_0)
                    (>= v0x7f16f0793610_0 v0x7f16f0790a90_0))
               (and v0x7f16f07929d0_0
                    E0x7f16f0793c50
                    (not v0x7f16f07930d0_0)
                    (and (<= v0x7f16f0793550_0 v0x7f16f0792f90_0)
                         (>= v0x7f16f0793550_0 v0x7f16f0792f90_0))
                    (<= v0x7f16f0793610_0 0.0)
                    (>= v0x7f16f0793610_0 0.0))))
      (a!4 (=> v0x7f16f0793490_0
               (or (and E0x7f16f07936d0
                        (not E0x7f16f0793990)
                        (not E0x7f16f0793c50))
                   (and E0x7f16f0793990
                        (not E0x7f16f07936d0)
                        (not E0x7f16f0793c50))
                   (and E0x7f16f0793c50
                        (not E0x7f16f07936d0)
                        (not E0x7f16f0793990))))))
(let ((a!5 (and (=> v0x7f16f0791350_0
                    (and v0x7f16f0791090_0
                         E0x7f16f0791410
                         (not v0x7f16f0791210_0)))
                (=> v0x7f16f0791350_0 E0x7f16f0791410)
                a!1
                a!2
                (=> v0x7f16f07924d0_0
                    (and v0x7f16f0791a90_0 E0x7f16f0792590 v0x7f16f0792390_0))
                (=> v0x7f16f07924d0_0 E0x7f16f0792590)
                (=> v0x7f16f07929d0_0
                    (and v0x7f16f0791a90_0
                         E0x7f16f0792a90
                         (not v0x7f16f0792390_0)))
                (=> v0x7f16f07929d0_0 E0x7f16f0792a90)
                (=> v0x7f16f0793210_0
                    (and v0x7f16f07929d0_0 E0x7f16f07932d0 v0x7f16f07930d0_0))
                (=> v0x7f16f0793210_0 E0x7f16f07932d0)
                (=> v0x7f16f0793490_0 a!3)
                a!4
                v0x7f16f0793490_0
                v0x7f16f0794410_0
                (<= v0x7f16f0790cd0_0 v0x7f16f0793610_0)
                (>= v0x7f16f0790cd0_0 v0x7f16f0793610_0)
                (<= v0x7f16f078f010_0 v0x7f16f0793550_0)
                (>= v0x7f16f078f010_0 v0x7f16f0793550_0)
                (= v0x7f16f0791210_0 (= v0x7f16f0791150_0 0.0))
                (= v0x7f16f0791650_0 (< v0x7f16f0790c10_0 2.0))
                (= v0x7f16f0791810_0 (ite v0x7f16f0791650_0 1.0 0.0))
                (= v0x7f16f0791950_0 (+ v0x7f16f0791810_0 v0x7f16f0790c10_0))
                (= v0x7f16f0792390_0 (= v0x7f16f0790a90_0 0.0))
                (= v0x7f16f0792750_0 (> v0x7f16f0791b50_0 1.0))
                (= v0x7f16f0792890_0
                   (ite v0x7f16f0792750_0 1.0 v0x7f16f0790a90_0))
                (= v0x7f16f0792c90_0 (> v0x7f16f0791b50_0 0.0))
                (= v0x7f16f0792dd0_0 (+ v0x7f16f0791b50_0 (- 1.0)))
                (= v0x7f16f0792f90_0
                   (ite v0x7f16f0792c90_0 v0x7f16f0792dd0_0 v0x7f16f0791b50_0))
                (= v0x7f16f07930d0_0 (= v0x7f16f0792f90_0 0.0))
                (= v0x7f16f0794190_0 (not (= v0x7f16f0793550_0 0.0)))
                (= v0x7f16f07942d0_0 (= v0x7f16f0793610_0 0.0))
                (= v0x7f16f0794410_0 (or v0x7f16f07942d0_0 v0x7f16f0794190_0)))))
  (=> F0x7f16f0794f90 a!5))))
(assert (=> F0x7f16f0794f90 F0x7f16f0795050))
(assert (let ((a!1 (=> v0x7f16f0791a90_0
               (or (and v0x7f16f0791350_0
                        E0x7f16f0791c10
                        (<= v0x7f16f0791b50_0 v0x7f16f0791950_0)
                        (>= v0x7f16f0791b50_0 v0x7f16f0791950_0))
                   (and v0x7f16f0791090_0
                        E0x7f16f0791dd0
                        v0x7f16f0791210_0
                        (<= v0x7f16f0791b50_0 v0x7f16f0790c10_0)
                        (>= v0x7f16f0791b50_0 v0x7f16f0790c10_0)))))
      (a!2 (=> v0x7f16f0791a90_0
               (or (and E0x7f16f0791c10 (not E0x7f16f0791dd0))
                   (and E0x7f16f0791dd0 (not E0x7f16f0791c10)))))
      (a!3 (or (and v0x7f16f07924d0_0
                    E0x7f16f07936d0
                    (<= v0x7f16f0793550_0 v0x7f16f0791b50_0)
                    (>= v0x7f16f0793550_0 v0x7f16f0791b50_0)
                    (<= v0x7f16f0793610_0 v0x7f16f0792890_0)
                    (>= v0x7f16f0793610_0 v0x7f16f0792890_0))
               (and v0x7f16f0793210_0
                    E0x7f16f0793990
                    (and (<= v0x7f16f0793550_0 v0x7f16f0792f90_0)
                         (>= v0x7f16f0793550_0 v0x7f16f0792f90_0))
                    (<= v0x7f16f0793610_0 v0x7f16f0790a90_0)
                    (>= v0x7f16f0793610_0 v0x7f16f0790a90_0))
               (and v0x7f16f07929d0_0
                    E0x7f16f0793c50
                    (not v0x7f16f07930d0_0)
                    (and (<= v0x7f16f0793550_0 v0x7f16f0792f90_0)
                         (>= v0x7f16f0793550_0 v0x7f16f0792f90_0))
                    (<= v0x7f16f0793610_0 0.0)
                    (>= v0x7f16f0793610_0 0.0))))
      (a!4 (=> v0x7f16f0793490_0
               (or (and E0x7f16f07936d0
                        (not E0x7f16f0793990)
                        (not E0x7f16f0793c50))
                   (and E0x7f16f0793990
                        (not E0x7f16f07936d0)
                        (not E0x7f16f0793c50))
                   (and E0x7f16f0793c50
                        (not E0x7f16f07936d0)
                        (not E0x7f16f0793990))))))
(let ((a!5 (and (=> v0x7f16f0791350_0
                    (and v0x7f16f0791090_0
                         E0x7f16f0791410
                         (not v0x7f16f0791210_0)))
                (=> v0x7f16f0791350_0 E0x7f16f0791410)
                a!1
                a!2
                (=> v0x7f16f07924d0_0
                    (and v0x7f16f0791a90_0 E0x7f16f0792590 v0x7f16f0792390_0))
                (=> v0x7f16f07924d0_0 E0x7f16f0792590)
                (=> v0x7f16f07929d0_0
                    (and v0x7f16f0791a90_0
                         E0x7f16f0792a90
                         (not v0x7f16f0792390_0)))
                (=> v0x7f16f07929d0_0 E0x7f16f0792a90)
                (=> v0x7f16f0793210_0
                    (and v0x7f16f07929d0_0 E0x7f16f07932d0 v0x7f16f07930d0_0))
                (=> v0x7f16f0793210_0 E0x7f16f07932d0)
                (=> v0x7f16f0793490_0 a!3)
                a!4
                v0x7f16f0793490_0
                (not v0x7f16f0794410_0)
                (= v0x7f16f0791210_0 (= v0x7f16f0791150_0 0.0))
                (= v0x7f16f0791650_0 (< v0x7f16f0790c10_0 2.0))
                (= v0x7f16f0791810_0 (ite v0x7f16f0791650_0 1.0 0.0))
                (= v0x7f16f0791950_0 (+ v0x7f16f0791810_0 v0x7f16f0790c10_0))
                (= v0x7f16f0792390_0 (= v0x7f16f0790a90_0 0.0))
                (= v0x7f16f0792750_0 (> v0x7f16f0791b50_0 1.0))
                (= v0x7f16f0792890_0
                   (ite v0x7f16f0792750_0 1.0 v0x7f16f0790a90_0))
                (= v0x7f16f0792c90_0 (> v0x7f16f0791b50_0 0.0))
                (= v0x7f16f0792dd0_0 (+ v0x7f16f0791b50_0 (- 1.0)))
                (= v0x7f16f0792f90_0
                   (ite v0x7f16f0792c90_0 v0x7f16f0792dd0_0 v0x7f16f0791b50_0))
                (= v0x7f16f07930d0_0 (= v0x7f16f0792f90_0 0.0))
                (= v0x7f16f0794190_0 (not (= v0x7f16f0793550_0 0.0)))
                (= v0x7f16f07942d0_0 (= v0x7f16f0793610_0 0.0))
                (= v0x7f16f0794410_0 (or v0x7f16f07942d0_0 v0x7f16f0794190_0)))))
  (=> F0x7f16f0795110 a!5))))
(assert (=> F0x7f16f0795110 F0x7f16f0795050))
(assert (=> F0x7f16f0795250 (or F0x7f16f0794f50 F0x7f16f0794f90)))
(assert (=> F0x7f16f0795210 F0x7f16f0795110))
(assert (=> pre!entry!0 (=> F0x7f16f0794e90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f16f0795050 (>= v0x7f16f0790a90_0 0.0))))
(assert (let ((a!1 (=> F0x7f16f0795050
               (or (<= v0x7f16f0790a90_0 0.0) (not (<= v0x7f16f0790c10_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f16f0795250 (>= v0x7f16f0790cd0_0 0.0))))
(assert (let ((a!1 (=> F0x7f16f0795250
               (or (<= v0x7f16f0790cd0_0 0.0) (not (<= v0x7f16f078f010_0 1.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i26.i.i!0 (=> F0x7f16f0795210 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i26.i.i!0) (not lemma!bb1.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f16f0794e90)
; (error: F0x7f16f0795210)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i26.i.i!0)
