(declare-fun post!bb2.i.i36.i.i!0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i36.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f691e1e4090 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun v0x7f691e1e2ed0_0 () Bool)
(declare-fun v0x7f691e1e2d90_0 () Bool)
(declare-fun v0x7f691e1e1010_0 () Bool)
(declare-fun v0x7f691e1de810_0 () Real)
(declare-fun v0x7f691e1deed0_0 () Real)
(declare-fun v0x7f691e1e1310_0 () Real)
(declare-fun v0x7f691e1ddf10_0 () Real)
(declare-fun E0x7f691e1e2210 () Bool)
(declare-fun v0x7f691e1ded10_0 () Bool)
(declare-fun F0x7f691e1e3c90 () Bool)
(declare-fun v0x7f691e1e1950_0 () Real)
(declare-fun v0x7f691e1e1150_0 () Real)
(declare-fun v0x7f691e1e1e90_0 () Real)
(declare-fun v0x7f691e1e1dd0_0 () Real)
(declare-fun E0x7f691e1e1f50 () Bool)
(declare-fun v0x7f691e1e1810_0 () Bool)
(declare-fun v0x7f691e1e1d10_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f691e1e1b50 () Bool)
(declare-fun v0x7f691e1e0c10_0 () Bool)
(declare-fun E0x7f691e1e1650 () Bool)
(declare-fun E0x7f691e1e0e10 () Bool)
(declare-fun v0x7f691e1e0850_0 () Bool)
(declare-fun v0x7f691e1e00d0_0 () Real)
(declare-fun E0x7f691e1e0190 () Bool)
(declare-fun E0x7f691e1dfb90 () Bool)
(declare-fun E0x7f691e1e2490 () Bool)
(declare-fun v0x7f691e1dfad0_0 () Bool)
(declare-fun v0x7f691e1e0990_0 () Bool)
(declare-fun F0x7f691e1e3f90 () Bool)
(declare-fun v0x7f691e1df210_0 () Real)
(declare-fun v0x7f691e1de090_0 () Real)
(declare-fun v0x7f691e1df150_0 () Bool)
(declare-fun v0x7f691e1de8d0_0 () Bool)
(declare-fun v0x7f691e1dfed0_0 () Real)
(declare-fun F0x7f691e1e4050 () Bool)
(declare-fun v0x7f691e1e0010_0 () Bool)
(declare-fun E0x7f691e1df2d0 () Bool)
(declare-fun E0x7f691e1dead0 () Bool)
(declare-fun v0x7f691e1e3010_0 () Bool)
(declare-fun v0x7f691e1dfd90_0 () Bool)
(declare-fun v0x7f691e1de750_0 () Bool)
(declare-fun v0x7f691e1e1590_0 () Bool)
(declare-fun v0x7f691e1e3150_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f691e1dea10_0 () Bool)
(declare-fun F0x7f691e1e3e10 () Bool)
(declare-fun v0x7f691e1e0d50_0 () Bool)
(declare-fun E0x7f691e1e2690 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7f691e1df010_0 () Real)
(declare-fun v0x7f691e1dc010_0 () Real)
(declare-fun v0x7f691e1df8d0_0 () Real)
(declare-fun E0x7f691e1e0350 () Bool)
(declare-fun v0x7f691e1de190_0 () Real)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun E0x7f691e1df490 () Bool)
(declare-fun v0x7f691e1e1a90_0 () Bool)
(declare-fun E0x7f691e1e0a50 () Bool)
(declare-fun v0x7f691e1de250_0 () Real)
(declare-fun v0x7f691e1de150_0 () Real)
(declare-fun v0x7f691e1e1450_0 () Bool)
(declare-fun v0x7f691e1df990_0 () Bool)
(declare-fun v0x7f691e1dc110_0 () Bool)
(declare-fun v0x7f691e1e2c50_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun F0x7f691e1e3d50 () Bool)
(declare-fun F0x7f691e1e3ed0 () Bool)

(assert (=> F0x7f691e1e3ed0
    (and v0x7f691e1dc110_0
         (<= v0x7f691e1de150_0 0.0)
         (>= v0x7f691e1de150_0 0.0)
         (<= v0x7f691e1de250_0 0.0)
         (>= v0x7f691e1de250_0 0.0)
         (<= v0x7f691e1dc010_0 1.0)
         (>= v0x7f691e1dc010_0 1.0))))
(assert (=> F0x7f691e1e3ed0 F0x7f691e1e3e10))
(assert (let ((a!1 (=> v0x7f691e1df150_0
               (or (and v0x7f691e1dea10_0
                        E0x7f691e1df2d0
                        (<= v0x7f691e1df210_0 v0x7f691e1df010_0)
                        (>= v0x7f691e1df210_0 v0x7f691e1df010_0))
                   (and v0x7f691e1de750_0
                        E0x7f691e1df490
                        v0x7f691e1de8d0_0
                        (<= v0x7f691e1df210_0 v0x7f691e1de190_0)
                        (>= v0x7f691e1df210_0 v0x7f691e1de190_0)))))
      (a!2 (=> v0x7f691e1df150_0
               (or (and E0x7f691e1df2d0 (not E0x7f691e1df490))
                   (and E0x7f691e1df490 (not E0x7f691e1df2d0)))))
      (a!3 (=> v0x7f691e1e0010_0
               (or (and v0x7f691e1dfad0_0
                        E0x7f691e1e0190
                        (<= v0x7f691e1e00d0_0 v0x7f691e1dfed0_0)
                        (>= v0x7f691e1e00d0_0 v0x7f691e1dfed0_0))
                   (and v0x7f691e1df150_0
                        E0x7f691e1e0350
                        v0x7f691e1df990_0
                        (<= v0x7f691e1e00d0_0 v0x7f691e1de090_0)
                        (>= v0x7f691e1e00d0_0 v0x7f691e1de090_0)))))
      (a!4 (=> v0x7f691e1e0010_0
               (or (and E0x7f691e1e0190 (not E0x7f691e1e0350))
                   (and E0x7f691e1e0350 (not E0x7f691e1e0190)))))
      (a!5 (or (and v0x7f691e1e1590_0
                    E0x7f691e1e1f50
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1df210_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1df210_0))
                    (<= v0x7f691e1e1e90_0 v0x7f691e1e1950_0)
                    (>= v0x7f691e1e1e90_0 v0x7f691e1e1950_0))
               (and v0x7f691e1e0990_0
                    E0x7f691e1e2210
                    (not v0x7f691e1e0c10_0)
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1df210_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1df210_0))
                    (and (<= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)
                         (>= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)))
               (and v0x7f691e1e1a90_0
                    E0x7f691e1e2490
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0))
                    (and (<= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)
                         (>= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)))
               (and v0x7f691e1e0d50_0
                    E0x7f691e1e2690
                    (not v0x7f691e1e1450_0)
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0))
                    (<= v0x7f691e1e1e90_0 0.0)
                    (>= v0x7f691e1e1e90_0 0.0))))
      (a!6 (=> v0x7f691e1e1d10_0
               (or (and E0x7f691e1e1f50
                        (not E0x7f691e1e2210)
                        (not E0x7f691e1e2490)
                        (not E0x7f691e1e2690))
                   (and E0x7f691e1e2210
                        (not E0x7f691e1e1f50)
                        (not E0x7f691e1e2490)
                        (not E0x7f691e1e2690))
                   (and E0x7f691e1e2490
                        (not E0x7f691e1e1f50)
                        (not E0x7f691e1e2210)
                        (not E0x7f691e1e2690))
                   (and E0x7f691e1e2690
                        (not E0x7f691e1e1f50)
                        (not E0x7f691e1e2210)
                        (not E0x7f691e1e2490))))))
(let ((a!7 (and (=> v0x7f691e1dea10_0
                    (and v0x7f691e1de750_0
                         E0x7f691e1dead0
                         (not v0x7f691e1de8d0_0)))
                (=> v0x7f691e1dea10_0 E0x7f691e1dead0)
                a!1
                a!2
                (=> v0x7f691e1dfad0_0
                    (and v0x7f691e1df150_0
                         E0x7f691e1dfb90
                         (not v0x7f691e1df990_0)))
                (=> v0x7f691e1dfad0_0 E0x7f691e1dfb90)
                a!3
                a!4
                (=> v0x7f691e1e0990_0
                    (and v0x7f691e1e0010_0 E0x7f691e1e0a50 v0x7f691e1e0850_0))
                (=> v0x7f691e1e0990_0 E0x7f691e1e0a50)
                (=> v0x7f691e1e0d50_0
                    (and v0x7f691e1e0010_0
                         E0x7f691e1e0e10
                         (not v0x7f691e1e0850_0)))
                (=> v0x7f691e1e0d50_0 E0x7f691e1e0e10)
                (=> v0x7f691e1e1590_0
                    (and v0x7f691e1e0990_0 E0x7f691e1e1650 v0x7f691e1e0c10_0))
                (=> v0x7f691e1e1590_0 E0x7f691e1e1650)
                (=> v0x7f691e1e1a90_0
                    (and v0x7f691e1e0d50_0 E0x7f691e1e1b50 v0x7f691e1e1450_0))
                (=> v0x7f691e1e1a90_0 E0x7f691e1e1b50)
                (=> v0x7f691e1e1d10_0 a!5)
                a!6
                v0x7f691e1e1d10_0
                (not v0x7f691e1e3150_0)
                (<= v0x7f691e1de150_0 v0x7f691e1e1e90_0)
                (>= v0x7f691e1de150_0 v0x7f691e1e1e90_0)
                (<= v0x7f691e1de250_0 v0x7f691e1e00d0_0)
                (>= v0x7f691e1de250_0 v0x7f691e1e00d0_0)
                (<= v0x7f691e1dc010_0 v0x7f691e1e1dd0_0)
                (>= v0x7f691e1dc010_0 v0x7f691e1e1dd0_0)
                (= v0x7f691e1de8d0_0 (= v0x7f691e1de810_0 0.0))
                (= v0x7f691e1ded10_0 (< v0x7f691e1de190_0 2.0))
                (= v0x7f691e1deed0_0 (ite v0x7f691e1ded10_0 1.0 0.0))
                (= v0x7f691e1df010_0 (+ v0x7f691e1deed0_0 v0x7f691e1de190_0))
                (= v0x7f691e1df990_0 (= v0x7f691e1df8d0_0 0.0))
                (= v0x7f691e1dfd90_0 (= v0x7f691e1de090_0 0.0))
                (= v0x7f691e1dfed0_0 (ite v0x7f691e1dfd90_0 1.0 0.0))
                (= v0x7f691e1e0850_0 (= v0x7f691e1ddf10_0 0.0))
                (= v0x7f691e1e0c10_0 (> v0x7f691e1df210_0 1.0))
                (= v0x7f691e1e1010_0 (> v0x7f691e1df210_0 0.0))
                (= v0x7f691e1e1150_0 (+ v0x7f691e1df210_0 (- 1.0)))
                (= v0x7f691e1e1310_0
                   (ite v0x7f691e1e1010_0 v0x7f691e1e1150_0 v0x7f691e1df210_0))
                (= v0x7f691e1e1450_0 (= v0x7f691e1e1310_0 0.0))
                (= v0x7f691e1e1810_0 (= v0x7f691e1e00d0_0 0.0))
                (= v0x7f691e1e1950_0
                   (ite v0x7f691e1e1810_0 1.0 v0x7f691e1ddf10_0))
                (= v0x7f691e1e2c50_0 (= v0x7f691e1e1dd0_0 2.0))
                (= v0x7f691e1e2d90_0 (= v0x7f691e1e1e90_0 0.0))
                (= v0x7f691e1e2ed0_0 (or v0x7f691e1e2d90_0 v0x7f691e1e2c50_0))
                (= v0x7f691e1e3010_0 (xor v0x7f691e1e2ed0_0 true))
                (= v0x7f691e1e3150_0 (and v0x7f691e1e0850_0 v0x7f691e1e3010_0)))))
  (=> F0x7f691e1e3d50 a!7))))
(assert (=> F0x7f691e1e3d50 F0x7f691e1e3c90))
(assert (let ((a!1 (=> v0x7f691e1df150_0
               (or (and v0x7f691e1dea10_0
                        E0x7f691e1df2d0
                        (<= v0x7f691e1df210_0 v0x7f691e1df010_0)
                        (>= v0x7f691e1df210_0 v0x7f691e1df010_0))
                   (and v0x7f691e1de750_0
                        E0x7f691e1df490
                        v0x7f691e1de8d0_0
                        (<= v0x7f691e1df210_0 v0x7f691e1de190_0)
                        (>= v0x7f691e1df210_0 v0x7f691e1de190_0)))))
      (a!2 (=> v0x7f691e1df150_0
               (or (and E0x7f691e1df2d0 (not E0x7f691e1df490))
                   (and E0x7f691e1df490 (not E0x7f691e1df2d0)))))
      (a!3 (=> v0x7f691e1e0010_0
               (or (and v0x7f691e1dfad0_0
                        E0x7f691e1e0190
                        (<= v0x7f691e1e00d0_0 v0x7f691e1dfed0_0)
                        (>= v0x7f691e1e00d0_0 v0x7f691e1dfed0_0))
                   (and v0x7f691e1df150_0
                        E0x7f691e1e0350
                        v0x7f691e1df990_0
                        (<= v0x7f691e1e00d0_0 v0x7f691e1de090_0)
                        (>= v0x7f691e1e00d0_0 v0x7f691e1de090_0)))))
      (a!4 (=> v0x7f691e1e0010_0
               (or (and E0x7f691e1e0190 (not E0x7f691e1e0350))
                   (and E0x7f691e1e0350 (not E0x7f691e1e0190)))))
      (a!5 (or (and v0x7f691e1e1590_0
                    E0x7f691e1e1f50
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1df210_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1df210_0))
                    (<= v0x7f691e1e1e90_0 v0x7f691e1e1950_0)
                    (>= v0x7f691e1e1e90_0 v0x7f691e1e1950_0))
               (and v0x7f691e1e0990_0
                    E0x7f691e1e2210
                    (not v0x7f691e1e0c10_0)
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1df210_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1df210_0))
                    (and (<= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)
                         (>= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)))
               (and v0x7f691e1e1a90_0
                    E0x7f691e1e2490
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0))
                    (and (<= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)
                         (>= v0x7f691e1e1e90_0 v0x7f691e1ddf10_0)))
               (and v0x7f691e1e0d50_0
                    E0x7f691e1e2690
                    (not v0x7f691e1e1450_0)
                    (and (<= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0)
                         (>= v0x7f691e1e1dd0_0 v0x7f691e1e1310_0))
                    (<= v0x7f691e1e1e90_0 0.0)
                    (>= v0x7f691e1e1e90_0 0.0))))
      (a!6 (=> v0x7f691e1e1d10_0
               (or (and E0x7f691e1e1f50
                        (not E0x7f691e1e2210)
                        (not E0x7f691e1e2490)
                        (not E0x7f691e1e2690))
                   (and E0x7f691e1e2210
                        (not E0x7f691e1e1f50)
                        (not E0x7f691e1e2490)
                        (not E0x7f691e1e2690))
                   (and E0x7f691e1e2490
                        (not E0x7f691e1e1f50)
                        (not E0x7f691e1e2210)
                        (not E0x7f691e1e2690))
                   (and E0x7f691e1e2690
                        (not E0x7f691e1e1f50)
                        (not E0x7f691e1e2210)
                        (not E0x7f691e1e2490))))))
(let ((a!7 (and (=> v0x7f691e1dea10_0
                    (and v0x7f691e1de750_0
                         E0x7f691e1dead0
                         (not v0x7f691e1de8d0_0)))
                (=> v0x7f691e1dea10_0 E0x7f691e1dead0)
                a!1
                a!2
                (=> v0x7f691e1dfad0_0
                    (and v0x7f691e1df150_0
                         E0x7f691e1dfb90
                         (not v0x7f691e1df990_0)))
                (=> v0x7f691e1dfad0_0 E0x7f691e1dfb90)
                a!3
                a!4
                (=> v0x7f691e1e0990_0
                    (and v0x7f691e1e0010_0 E0x7f691e1e0a50 v0x7f691e1e0850_0))
                (=> v0x7f691e1e0990_0 E0x7f691e1e0a50)
                (=> v0x7f691e1e0d50_0
                    (and v0x7f691e1e0010_0
                         E0x7f691e1e0e10
                         (not v0x7f691e1e0850_0)))
                (=> v0x7f691e1e0d50_0 E0x7f691e1e0e10)
                (=> v0x7f691e1e1590_0
                    (and v0x7f691e1e0990_0 E0x7f691e1e1650 v0x7f691e1e0c10_0))
                (=> v0x7f691e1e1590_0 E0x7f691e1e1650)
                (=> v0x7f691e1e1a90_0
                    (and v0x7f691e1e0d50_0 E0x7f691e1e1b50 v0x7f691e1e1450_0))
                (=> v0x7f691e1e1a90_0 E0x7f691e1e1b50)
                (=> v0x7f691e1e1d10_0 a!5)
                a!6
                v0x7f691e1e1d10_0
                v0x7f691e1e3150_0
                (= v0x7f691e1de8d0_0 (= v0x7f691e1de810_0 0.0))
                (= v0x7f691e1ded10_0 (< v0x7f691e1de190_0 2.0))
                (= v0x7f691e1deed0_0 (ite v0x7f691e1ded10_0 1.0 0.0))
                (= v0x7f691e1df010_0 (+ v0x7f691e1deed0_0 v0x7f691e1de190_0))
                (= v0x7f691e1df990_0 (= v0x7f691e1df8d0_0 0.0))
                (= v0x7f691e1dfd90_0 (= v0x7f691e1de090_0 0.0))
                (= v0x7f691e1dfed0_0 (ite v0x7f691e1dfd90_0 1.0 0.0))
                (= v0x7f691e1e0850_0 (= v0x7f691e1ddf10_0 0.0))
                (= v0x7f691e1e0c10_0 (> v0x7f691e1df210_0 1.0))
                (= v0x7f691e1e1010_0 (> v0x7f691e1df210_0 0.0))
                (= v0x7f691e1e1150_0 (+ v0x7f691e1df210_0 (- 1.0)))
                (= v0x7f691e1e1310_0
                   (ite v0x7f691e1e1010_0 v0x7f691e1e1150_0 v0x7f691e1df210_0))
                (= v0x7f691e1e1450_0 (= v0x7f691e1e1310_0 0.0))
                (= v0x7f691e1e1810_0 (= v0x7f691e1e00d0_0 0.0))
                (= v0x7f691e1e1950_0
                   (ite v0x7f691e1e1810_0 1.0 v0x7f691e1ddf10_0))
                (= v0x7f691e1e2c50_0 (= v0x7f691e1e1dd0_0 2.0))
                (= v0x7f691e1e2d90_0 (= v0x7f691e1e1e90_0 0.0))
                (= v0x7f691e1e2ed0_0 (or v0x7f691e1e2d90_0 v0x7f691e1e2c50_0))
                (= v0x7f691e1e3010_0 (xor v0x7f691e1e2ed0_0 true))
                (= v0x7f691e1e3150_0 (and v0x7f691e1e0850_0 v0x7f691e1e3010_0)))))
  (=> F0x7f691e1e3f90 a!7))))
(assert (=> F0x7f691e1e3f90 F0x7f691e1e3c90))
(assert (=> F0x7f691e1e4090 (or F0x7f691e1e3ed0 F0x7f691e1e3d50)))
(assert (=> F0x7f691e1e4050 F0x7f691e1e3f90))
(assert (=> pre!entry!0 (=> F0x7f691e1e3e10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f691e1e3c90 (>= v0x7f691e1de090_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f691e1e3c90
        (or (<= v0x7f691e1de190_0 1.0) (>= v0x7f691e1de190_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f691e1e3c90 (>= v0x7f691e1de190_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f691e1e3c90
        (or (>= v0x7f691e1de190_0 1.0) (<= v0x7f691e1de190_0 0.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f691e1e3c90 (<= v0x7f691e1de190_0 2.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f691e1e4090 (>= v0x7f691e1de250_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7f691e1e4090
       (or (<= v0x7f691e1dc010_0 1.0) (>= v0x7f691e1dc010_0 2.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7f691e1e4090 (>= v0x7f691e1dc010_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7f691e1e4090
       (or (>= v0x7f691e1dc010_0 1.0) (<= v0x7f691e1dc010_0 0.0)))))
(assert (= lemma!bb1.i.i!4 (=> F0x7f691e1e4090 (<= v0x7f691e1dc010_0 2.0))))
(assert (= lemma!bb2.i.i36.i.i!0 (=> F0x7f691e1e4050 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb2.i.i36.i.i!0) (not lemma!bb2.i.i36.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
; (init: F0x7f691e1e3e10)
; (error: F0x7f691e1e4050)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i36.i.i!0)
