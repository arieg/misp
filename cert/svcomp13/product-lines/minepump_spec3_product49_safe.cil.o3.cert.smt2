(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i26.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun post!bb2.i.i26.i.i!0 () Bool)
(declare-fun F0x7fe06b8df650 () Bool)
(declare-fun F0x7fe06b8df590 () Bool)
(declare-fun v0x7fe06b8de710_0 () Bool)
(declare-fun v0x7fe06b8de490_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fe06b8de350_0 () Bool)
(declare-fun v0x7fe06b8db5d0_0 () Real)
(declare-fun v0x7fe06b8dabd0_0 () Real)
(declare-fun v0x7fe06b8de5d0_0 () Bool)
(declare-fun v0x7fe06b8da510_0 () Real)
(declare-fun v0x7fe06b8de850_0 () Bool)
(declare-fun E0x7fe06b8dde10 () Bool)
(declare-fun E0x7fe06b8ddb50 () Bool)
(declare-fun v0x7fe06b8dd150_0 () Real)
(declare-fun v0x7fe06b8daa10_0 () Bool)
(declare-fun v0x7fe06b8dd710_0 () Real)
(declare-fun E0x7fe06b8dd890 () Bool)
(declare-fun v0x7fe06b8dd650_0 () Bool)
(declare-fun v0x7fe06b8dd290_0 () Bool)
(declare-fun E0x7fe06b8dd490 () Bool)
(declare-fun v0x7fe06b8dc910_0 () Bool)
(declare-fun v0x7fe06b8dc550_0 () Bool)
(declare-fun v0x7fe06b8dc690_0 () Bool)
(declare-fun v0x7fe06b8d9c10_0 () Real)
(declare-fun E0x7fe06b8dc050 () Bool)
(declare-fun F0x7fe06b8df790 () Bool)
(declare-fun v0x7fe06b8dcf90_0 () Real)
(declare-fun E0x7fe06b8dcc50 () Bool)
(declare-fun v0x7fe06b8dbbd0_0 () Real)
(declare-fun v0x7fe06b8d9e90_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fe06b8dbdd0_0 () Real)
(declare-fun v0x7fe06b8dcb90_0 () Bool)
(declare-fun v0x7fe06b8db690_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7fe06b8db7d0_0 () Bool)
(declare-fun v0x7fe06b8d9d90_0 () Real)
(declare-fun v0x7fe06b8dca50_0 () Real)
(declare-fun v0x7fe06b8dad10_0 () Real)
(declare-fun E0x7fe06b8dc750 () Bool)
(declare-fun v0x7fe06b8dae50_0 () Bool)
(declare-fun E0x7fe06b8da7d0 () Bool)
(declare-fun v0x7fe06b8daf10_0 () Real)
(declare-fun v0x7fe06b8da5d0_0 () Bool)
(declare-fun v0x7fe06b8da710_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fe06b8dce50_0 () Bool)
(declare-fun F0x7fe06b8df4d0 () Bool)
(declare-fun E0x7fe06b8dbe90 () Bool)
(declare-fun F0x7fe06b8df390 () Bool)
(declare-fun E0x7fe06b8db890 () Bool)
(declare-fun v0x7fe06b8d8010_0 () Real)
(declare-fun E0x7fe06b8db190 () Bool)
(declare-fun v0x7fe06b8dba90_0 () Bool)
(declare-fun F0x7fe06b8df750 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7fe06b8dbd10_0 () Bool)
(declare-fun v0x7fe06b8da450_0 () Bool)
(declare-fun E0x7fe06b8dafd0 () Bool)
(declare-fun v0x7fe06b8d9f50_0 () Real)
(declare-fun v0x7fe06b8dd3d0_0 () Bool)
(declare-fun v0x7fe06b8d9e50_0 () Real)
(declare-fun v0x7fe06b8dd7d0_0 () Real)
(declare-fun v0x7fe06b8d8110_0 () Bool)
(declare-fun F0x7fe06b8df490 () Bool)

(assert (=> F0x7fe06b8df490
    (and v0x7fe06b8d8110_0
         (<= v0x7fe06b8d9e50_0 0.0)
         (>= v0x7fe06b8d9e50_0 0.0)
         (<= v0x7fe06b8d9f50_0 1.0)
         (>= v0x7fe06b8d9f50_0 1.0)
         (<= v0x7fe06b8d8010_0 0.0)
         (>= v0x7fe06b8d8010_0 0.0))))
(assert (=> F0x7fe06b8df490 F0x7fe06b8df390))
(assert (let ((a!1 (=> v0x7fe06b8dae50_0
               (or (and v0x7fe06b8da710_0
                        E0x7fe06b8dafd0
                        (<= v0x7fe06b8daf10_0 v0x7fe06b8dad10_0)
                        (>= v0x7fe06b8daf10_0 v0x7fe06b8dad10_0))
                   (and v0x7fe06b8da450_0
                        E0x7fe06b8db190
                        v0x7fe06b8da5d0_0
                        (<= v0x7fe06b8daf10_0 v0x7fe06b8d9d90_0)
                        (>= v0x7fe06b8daf10_0 v0x7fe06b8d9d90_0)))))
      (a!2 (=> v0x7fe06b8dae50_0
               (or (and E0x7fe06b8dafd0 (not E0x7fe06b8db190))
                   (and E0x7fe06b8db190 (not E0x7fe06b8dafd0)))))
      (a!3 (=> v0x7fe06b8dbd10_0
               (or (and v0x7fe06b8db7d0_0
                        E0x7fe06b8dbe90
                        (<= v0x7fe06b8dbdd0_0 v0x7fe06b8dbbd0_0)
                        (>= v0x7fe06b8dbdd0_0 v0x7fe06b8dbbd0_0))
                   (and v0x7fe06b8dae50_0
                        E0x7fe06b8dc050
                        v0x7fe06b8db690_0
                        (<= v0x7fe06b8dbdd0_0 v0x7fe06b8d9c10_0)
                        (>= v0x7fe06b8dbdd0_0 v0x7fe06b8d9c10_0)))))
      (a!4 (=> v0x7fe06b8dbd10_0
               (or (and E0x7fe06b8dbe90 (not E0x7fe06b8dc050))
                   (and E0x7fe06b8dc050 (not E0x7fe06b8dbe90)))))
      (a!5 (or (and v0x7fe06b8dc690_0
                    E0x7fe06b8dd890
                    (<= v0x7fe06b8dd710_0 v0x7fe06b8daf10_0)
                    (>= v0x7fe06b8dd710_0 v0x7fe06b8daf10_0)
                    (<= v0x7fe06b8dd7d0_0 v0x7fe06b8dca50_0)
                    (>= v0x7fe06b8dd7d0_0 v0x7fe06b8dca50_0))
               (and v0x7fe06b8dd3d0_0
                    E0x7fe06b8ddb50
                    (and (<= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0)
                         (>= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0))
                    (<= v0x7fe06b8dd7d0_0 v0x7fe06b8d9e90_0)
                    (>= v0x7fe06b8dd7d0_0 v0x7fe06b8d9e90_0))
               (and v0x7fe06b8dcb90_0
                    E0x7fe06b8dde10
                    (not v0x7fe06b8dd290_0)
                    (and (<= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0)
                         (>= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0))
                    (<= v0x7fe06b8dd7d0_0 0.0)
                    (>= v0x7fe06b8dd7d0_0 0.0))))
      (a!6 (=> v0x7fe06b8dd650_0
               (or (and E0x7fe06b8dd890
                        (not E0x7fe06b8ddb50)
                        (not E0x7fe06b8dde10))
                   (and E0x7fe06b8ddb50
                        (not E0x7fe06b8dd890)
                        (not E0x7fe06b8dde10))
                   (and E0x7fe06b8dde10
                        (not E0x7fe06b8dd890)
                        (not E0x7fe06b8ddb50))))))
(let ((a!7 (and (=> v0x7fe06b8da710_0
                    (and v0x7fe06b8da450_0
                         E0x7fe06b8da7d0
                         (not v0x7fe06b8da5d0_0)))
                (=> v0x7fe06b8da710_0 E0x7fe06b8da7d0)
                a!1
                a!2
                (=> v0x7fe06b8db7d0_0
                    (and v0x7fe06b8dae50_0
                         E0x7fe06b8db890
                         (not v0x7fe06b8db690_0)))
                (=> v0x7fe06b8db7d0_0 E0x7fe06b8db890)
                a!3
                a!4
                (=> v0x7fe06b8dc690_0
                    (and v0x7fe06b8dbd10_0 E0x7fe06b8dc750 v0x7fe06b8dc550_0))
                (=> v0x7fe06b8dc690_0 E0x7fe06b8dc750)
                (=> v0x7fe06b8dcb90_0
                    (and v0x7fe06b8dbd10_0
                         E0x7fe06b8dcc50
                         (not v0x7fe06b8dc550_0)))
                (=> v0x7fe06b8dcb90_0 E0x7fe06b8dcc50)
                (=> v0x7fe06b8dd3d0_0
                    (and v0x7fe06b8dcb90_0 E0x7fe06b8dd490 v0x7fe06b8dd290_0))
                (=> v0x7fe06b8dd3d0_0 E0x7fe06b8dd490)
                (=> v0x7fe06b8dd650_0 a!5)
                a!6
                v0x7fe06b8dd650_0
                (not v0x7fe06b8de850_0)
                (<= v0x7fe06b8d9e50_0 v0x7fe06b8dbdd0_0)
                (>= v0x7fe06b8d9e50_0 v0x7fe06b8dbdd0_0)
                (<= v0x7fe06b8d9f50_0 v0x7fe06b8dd710_0)
                (>= v0x7fe06b8d9f50_0 v0x7fe06b8dd710_0)
                (<= v0x7fe06b8d8010_0 v0x7fe06b8dd7d0_0)
                (>= v0x7fe06b8d8010_0 v0x7fe06b8dd7d0_0)
                (= v0x7fe06b8da5d0_0 (= v0x7fe06b8da510_0 0.0))
                (= v0x7fe06b8daa10_0 (< v0x7fe06b8d9d90_0 2.0))
                (= v0x7fe06b8dabd0_0 (ite v0x7fe06b8daa10_0 1.0 0.0))
                (= v0x7fe06b8dad10_0 (+ v0x7fe06b8dabd0_0 v0x7fe06b8d9d90_0))
                (= v0x7fe06b8db690_0 (= v0x7fe06b8db5d0_0 0.0))
                (= v0x7fe06b8dba90_0 (= v0x7fe06b8d9c10_0 0.0))
                (= v0x7fe06b8dbbd0_0 (ite v0x7fe06b8dba90_0 1.0 0.0))
                (= v0x7fe06b8dc550_0 (= v0x7fe06b8d9e90_0 0.0))
                (= v0x7fe06b8dc910_0 (> v0x7fe06b8daf10_0 1.0))
                (= v0x7fe06b8dca50_0
                   (ite v0x7fe06b8dc910_0 1.0 v0x7fe06b8d9e90_0))
                (= v0x7fe06b8dce50_0 (> v0x7fe06b8daf10_0 0.0))
                (= v0x7fe06b8dcf90_0 (+ v0x7fe06b8daf10_0 (- 1.0)))
                (= v0x7fe06b8dd150_0
                   (ite v0x7fe06b8dce50_0 v0x7fe06b8dcf90_0 v0x7fe06b8daf10_0))
                (= v0x7fe06b8dd290_0 (= v0x7fe06b8dd150_0 0.0))
                (= v0x7fe06b8de350_0 (= v0x7fe06b8dbdd0_0 0.0))
                (= v0x7fe06b8de490_0 (= v0x7fe06b8dd710_0 2.0))
                (= v0x7fe06b8de5d0_0 (= v0x7fe06b8dd7d0_0 0.0))
                (= v0x7fe06b8de710_0 (and v0x7fe06b8de490_0 v0x7fe06b8de350_0))
                (= v0x7fe06b8de850_0 (and v0x7fe06b8de710_0 v0x7fe06b8de5d0_0)))))
  (=> F0x7fe06b8df4d0 a!7))))
(assert (=> F0x7fe06b8df4d0 F0x7fe06b8df590))
(assert (let ((a!1 (=> v0x7fe06b8dae50_0
               (or (and v0x7fe06b8da710_0
                        E0x7fe06b8dafd0
                        (<= v0x7fe06b8daf10_0 v0x7fe06b8dad10_0)
                        (>= v0x7fe06b8daf10_0 v0x7fe06b8dad10_0))
                   (and v0x7fe06b8da450_0
                        E0x7fe06b8db190
                        v0x7fe06b8da5d0_0
                        (<= v0x7fe06b8daf10_0 v0x7fe06b8d9d90_0)
                        (>= v0x7fe06b8daf10_0 v0x7fe06b8d9d90_0)))))
      (a!2 (=> v0x7fe06b8dae50_0
               (or (and E0x7fe06b8dafd0 (not E0x7fe06b8db190))
                   (and E0x7fe06b8db190 (not E0x7fe06b8dafd0)))))
      (a!3 (=> v0x7fe06b8dbd10_0
               (or (and v0x7fe06b8db7d0_0
                        E0x7fe06b8dbe90
                        (<= v0x7fe06b8dbdd0_0 v0x7fe06b8dbbd0_0)
                        (>= v0x7fe06b8dbdd0_0 v0x7fe06b8dbbd0_0))
                   (and v0x7fe06b8dae50_0
                        E0x7fe06b8dc050
                        v0x7fe06b8db690_0
                        (<= v0x7fe06b8dbdd0_0 v0x7fe06b8d9c10_0)
                        (>= v0x7fe06b8dbdd0_0 v0x7fe06b8d9c10_0)))))
      (a!4 (=> v0x7fe06b8dbd10_0
               (or (and E0x7fe06b8dbe90 (not E0x7fe06b8dc050))
                   (and E0x7fe06b8dc050 (not E0x7fe06b8dbe90)))))
      (a!5 (or (and v0x7fe06b8dc690_0
                    E0x7fe06b8dd890
                    (<= v0x7fe06b8dd710_0 v0x7fe06b8daf10_0)
                    (>= v0x7fe06b8dd710_0 v0x7fe06b8daf10_0)
                    (<= v0x7fe06b8dd7d0_0 v0x7fe06b8dca50_0)
                    (>= v0x7fe06b8dd7d0_0 v0x7fe06b8dca50_0))
               (and v0x7fe06b8dd3d0_0
                    E0x7fe06b8ddb50
                    (and (<= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0)
                         (>= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0))
                    (<= v0x7fe06b8dd7d0_0 v0x7fe06b8d9e90_0)
                    (>= v0x7fe06b8dd7d0_0 v0x7fe06b8d9e90_0))
               (and v0x7fe06b8dcb90_0
                    E0x7fe06b8dde10
                    (not v0x7fe06b8dd290_0)
                    (and (<= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0)
                         (>= v0x7fe06b8dd710_0 v0x7fe06b8dd150_0))
                    (<= v0x7fe06b8dd7d0_0 0.0)
                    (>= v0x7fe06b8dd7d0_0 0.0))))
      (a!6 (=> v0x7fe06b8dd650_0
               (or (and E0x7fe06b8dd890
                        (not E0x7fe06b8ddb50)
                        (not E0x7fe06b8dde10))
                   (and E0x7fe06b8ddb50
                        (not E0x7fe06b8dd890)
                        (not E0x7fe06b8dde10))
                   (and E0x7fe06b8dde10
                        (not E0x7fe06b8dd890)
                        (not E0x7fe06b8ddb50))))))
(let ((a!7 (and (=> v0x7fe06b8da710_0
                    (and v0x7fe06b8da450_0
                         E0x7fe06b8da7d0
                         (not v0x7fe06b8da5d0_0)))
                (=> v0x7fe06b8da710_0 E0x7fe06b8da7d0)
                a!1
                a!2
                (=> v0x7fe06b8db7d0_0
                    (and v0x7fe06b8dae50_0
                         E0x7fe06b8db890
                         (not v0x7fe06b8db690_0)))
                (=> v0x7fe06b8db7d0_0 E0x7fe06b8db890)
                a!3
                a!4
                (=> v0x7fe06b8dc690_0
                    (and v0x7fe06b8dbd10_0 E0x7fe06b8dc750 v0x7fe06b8dc550_0))
                (=> v0x7fe06b8dc690_0 E0x7fe06b8dc750)
                (=> v0x7fe06b8dcb90_0
                    (and v0x7fe06b8dbd10_0
                         E0x7fe06b8dcc50
                         (not v0x7fe06b8dc550_0)))
                (=> v0x7fe06b8dcb90_0 E0x7fe06b8dcc50)
                (=> v0x7fe06b8dd3d0_0
                    (and v0x7fe06b8dcb90_0 E0x7fe06b8dd490 v0x7fe06b8dd290_0))
                (=> v0x7fe06b8dd3d0_0 E0x7fe06b8dd490)
                (=> v0x7fe06b8dd650_0 a!5)
                a!6
                v0x7fe06b8dd650_0
                v0x7fe06b8de850_0
                (= v0x7fe06b8da5d0_0 (= v0x7fe06b8da510_0 0.0))
                (= v0x7fe06b8daa10_0 (< v0x7fe06b8d9d90_0 2.0))
                (= v0x7fe06b8dabd0_0 (ite v0x7fe06b8daa10_0 1.0 0.0))
                (= v0x7fe06b8dad10_0 (+ v0x7fe06b8dabd0_0 v0x7fe06b8d9d90_0))
                (= v0x7fe06b8db690_0 (= v0x7fe06b8db5d0_0 0.0))
                (= v0x7fe06b8dba90_0 (= v0x7fe06b8d9c10_0 0.0))
                (= v0x7fe06b8dbbd0_0 (ite v0x7fe06b8dba90_0 1.0 0.0))
                (= v0x7fe06b8dc550_0 (= v0x7fe06b8d9e90_0 0.0))
                (= v0x7fe06b8dc910_0 (> v0x7fe06b8daf10_0 1.0))
                (= v0x7fe06b8dca50_0
                   (ite v0x7fe06b8dc910_0 1.0 v0x7fe06b8d9e90_0))
                (= v0x7fe06b8dce50_0 (> v0x7fe06b8daf10_0 0.0))
                (= v0x7fe06b8dcf90_0 (+ v0x7fe06b8daf10_0 (- 1.0)))
                (= v0x7fe06b8dd150_0
                   (ite v0x7fe06b8dce50_0 v0x7fe06b8dcf90_0 v0x7fe06b8daf10_0))
                (= v0x7fe06b8dd290_0 (= v0x7fe06b8dd150_0 0.0))
                (= v0x7fe06b8de350_0 (= v0x7fe06b8dbdd0_0 0.0))
                (= v0x7fe06b8de490_0 (= v0x7fe06b8dd710_0 2.0))
                (= v0x7fe06b8de5d0_0 (= v0x7fe06b8dd7d0_0 0.0))
                (= v0x7fe06b8de710_0 (and v0x7fe06b8de490_0 v0x7fe06b8de350_0))
                (= v0x7fe06b8de850_0 (and v0x7fe06b8de710_0 v0x7fe06b8de5d0_0)))))
  (=> F0x7fe06b8df650 a!7))))
(assert (=> F0x7fe06b8df650 F0x7fe06b8df590))
(assert (=> F0x7fe06b8df790 (or F0x7fe06b8df490 F0x7fe06b8df4d0)))
(assert (=> F0x7fe06b8df750 F0x7fe06b8df650))
(assert (=> pre!entry!0 (=> F0x7fe06b8df390 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fe06b8df590 (>= v0x7fe06b8d9e90_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7fe06b8df590 (not (<= 3.0 v0x7fe06b8d9d90_0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fe06b8df790 (>= v0x7fe06b8d8010_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7fe06b8df790 (not (<= 3.0 v0x7fe06b8d9f50_0)))))
(assert (= lemma!bb2.i.i26.i.i!0 (=> F0x7fe06b8df750 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i26.i.i!0) (not lemma!bb2.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7fe06b8df390)
; (error: F0x7fe06b8df750)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i26.i.i!0)
