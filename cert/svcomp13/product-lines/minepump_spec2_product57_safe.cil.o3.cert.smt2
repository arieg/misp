(declare-fun post!bb1.i.i!6 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!6 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!6 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fd01b3a6690 () Bool)
(declare-fun F0x7fd01b3a63d0 () Bool)
(declare-fun v0x7fd01b3a4f90_0 () Bool)
(declare-fun v0x7fd01b3a3a50_0 () Bool)
(declare-fun v0x7fd01b3a0090_0 () Real)
(declare-fun post!bb2.i.i34.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7fd01b3a3390_0 () Real)
(declare-fun v0x7fd01b3a1110_0 () Real)
(declare-fun v0x7fd01b3a0f50_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fd01b3a0a50_0 () Real)
(declare-fun v0x7fd01b3a54d0_0 () Bool)
(declare-fun v0x7fd01b3a1b10_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun E0x7fd01b3a52d0 () Bool)
(declare-fun v0x7fd01b3a3550_0 () Real)
(declare-fun E0x7fd01b3a48d0 () Bool)
(declare-fun E0x7fd01b3a46d0 () Bool)
(declare-fun v0x7fd01b3a40d0_0 () Real)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun v0x7fd01b3a4010_0 () Real)
(declare-fun E0x7fd01b3a4190 () Bool)
(declare-fun F0x7fd01b3a6450 () Bool)
(declare-fun v0x7fd01b3a3f50_0 () Bool)
(declare-fun E0x7fd01b3a3890 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fd01b3a6250 () Bool)
(declare-fun v0x7fd01b39ff10_0 () Real)
(declare-fun v0x7fd01b3a37d0_0 () Bool)
(declare-fun v0x7fd01b3a2a90_0 () Bool)
(declare-fun E0x7fd01b3a4450 () Bool)
(declare-fun v0x7fd01b3a3250_0 () Bool)
(declare-fun v0x7fd01b3a3cd0_0 () Bool)
(declare-fun E0x7fd01b3a2c90 () Bool)
(declare-fun v0x7fd01b3a1fd0_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7fd01b3a0190_0 () Real)
(declare-fun v0x7fd01b3a2f90_0 () Bool)
(declare-fun E0x7fd01b3a3d90 () Bool)
(declare-fun v0x7fd01b3a2250_0 () Bool)
(declare-fun v0x7fd01b3a1bd0_0 () Bool)
(declare-fun v0x7fd01b3a2110_0 () Real)
(declare-fun E0x7fd01b3a1dd0 () Bool)
(declare-fun E0x7fd01b3a2590 () Bool)
(declare-fun v0x7fd01b3a2310_0 () Real)
(declare-fun v0x7fd01b3a1d10_0 () Bool)
(declare-fun v0x7fd01b3a5210_0 () Bool)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun v0x7fd01b3a0290_0 () Real)
(declare-fun v0x7fd01b3a1450_0 () Real)
(declare-fun v0x7fd01b3a3b90_0 () Real)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun v0x7fd01b3a0b10_0 () Bool)
(declare-fun E0x7fd01b3a23d0 () Bool)
(declare-fun v0x7fd01b3a0990_0 () Bool)
(declare-fun v0x7fd01b39e010_0 () Real)
(declare-fun F0x7fd01b3a6510 () Bool)
(declare-fun v0x7fd01b3a2e50_0 () Bool)
(declare-fun v0x7fd01b3a3690_0 () Bool)
(declare-fun v0x7fd01b3a0350_0 () Real)
(declare-fun v0x7fd01b3a0150_0 () Real)
(declare-fun v0x7fd01b3a4e90_0 () Bool)
(declare-fun E0x7fd01b3a1510 () Bool)
(declare-fun E0x7fd01b3a0d10 () Bool)
(declare-fun E0x7fd01b3a3050 () Bool)
(declare-fun v0x7fd01b3a0c50_0 () Bool)
(declare-fun v0x7fd01b3a1390_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fd01b39e110_0 () Bool)
(declare-fun lemma!bb2.i.i34.i.i!0 () Bool)
(declare-fun v0x7fd01b3a2bd0_0 () Bool)
(declare-fun F0x7fd01b3a65d0 () Bool)
(declare-fun v0x7fd01b3a1250_0 () Real)
(declare-fun F0x7fd01b3a6650 () Bool)
(declare-fun v0x7fd01b3a50d0_0 () Bool)
(declare-fun E0x7fd01b3a16d0 () Bool)
(declare-fun v0x7fd01b3a0250_0 () Real)

(assert (=> F0x7fd01b3a6650
    (and v0x7fd01b39e110_0
         (<= v0x7fd01b3a0150_0 0.0)
         (>= v0x7fd01b3a0150_0 0.0)
         (<= v0x7fd01b3a0250_0 0.0)
         (>= v0x7fd01b3a0250_0 0.0)
         (<= v0x7fd01b3a0350_0 0.0)
         (>= v0x7fd01b3a0350_0 0.0)
         (<= v0x7fd01b39e010_0 1.0)
         (>= v0x7fd01b39e010_0 1.0))))
(assert (=> F0x7fd01b3a6650 F0x7fd01b3a65d0))
(assert (let ((a!1 (=> v0x7fd01b3a1390_0
               (or (and v0x7fd01b3a0c50_0
                        E0x7fd01b3a1510
                        (<= v0x7fd01b3a1450_0 v0x7fd01b3a1250_0)
                        (>= v0x7fd01b3a1450_0 v0x7fd01b3a1250_0))
                   (and v0x7fd01b3a0990_0
                        E0x7fd01b3a16d0
                        v0x7fd01b3a0b10_0
                        (<= v0x7fd01b3a1450_0 v0x7fd01b3a0290_0)
                        (>= v0x7fd01b3a1450_0 v0x7fd01b3a0290_0)))))
      (a!2 (=> v0x7fd01b3a1390_0
               (or (and E0x7fd01b3a1510 (not E0x7fd01b3a16d0))
                   (and E0x7fd01b3a16d0 (not E0x7fd01b3a1510)))))
      (a!3 (=> v0x7fd01b3a2250_0
               (or (and v0x7fd01b3a1d10_0
                        E0x7fd01b3a23d0
                        (<= v0x7fd01b3a2310_0 v0x7fd01b3a2110_0)
                        (>= v0x7fd01b3a2310_0 v0x7fd01b3a2110_0))
                   (and v0x7fd01b3a1390_0
                        E0x7fd01b3a2590
                        v0x7fd01b3a1bd0_0
                        (<= v0x7fd01b3a2310_0 v0x7fd01b3a0190_0)
                        (>= v0x7fd01b3a2310_0 v0x7fd01b3a0190_0)))))
      (a!4 (=> v0x7fd01b3a2250_0
               (or (and E0x7fd01b3a23d0 (not E0x7fd01b3a2590))
                   (and E0x7fd01b3a2590 (not E0x7fd01b3a23d0)))))
      (a!5 (or (and v0x7fd01b3a37d0_0
                    E0x7fd01b3a4190
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0))
                    (<= v0x7fd01b3a40d0_0 v0x7fd01b3a3b90_0)
                    (>= v0x7fd01b3a40d0_0 v0x7fd01b3a3b90_0))
               (and v0x7fd01b3a2bd0_0
                    E0x7fd01b3a4450
                    (not v0x7fd01b3a2e50_0)
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0))
                    (and (<= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)
                         (>= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)))
               (and v0x7fd01b3a3cd0_0
                    E0x7fd01b3a46d0
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0))
                    (and (<= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)
                         (>= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)))
               (and v0x7fd01b3a2f90_0
                    E0x7fd01b3a48d0
                    (not v0x7fd01b3a3690_0)
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0))
                    (<= v0x7fd01b3a40d0_0 0.0)
                    (>= v0x7fd01b3a40d0_0 0.0))))
      (a!6 (=> v0x7fd01b3a3f50_0
               (or (and E0x7fd01b3a4190
                        (not E0x7fd01b3a4450)
                        (not E0x7fd01b3a46d0)
                        (not E0x7fd01b3a48d0))
                   (and E0x7fd01b3a4450
                        (not E0x7fd01b3a4190)
                        (not E0x7fd01b3a46d0)
                        (not E0x7fd01b3a48d0))
                   (and E0x7fd01b3a46d0
                        (not E0x7fd01b3a4190)
                        (not E0x7fd01b3a4450)
                        (not E0x7fd01b3a48d0))
                   (and E0x7fd01b3a48d0
                        (not E0x7fd01b3a4190)
                        (not E0x7fd01b3a4450)
                        (not E0x7fd01b3a46d0)))))
      (a!7 (or (and v0x7fd01b3a5210_0
                    v0x7fd01b3a54d0_0
                    (and (<= v0x7fd01b3a0150_0 v0x7fd01b3a40d0_0)
                         (>= v0x7fd01b3a0150_0 v0x7fd01b3a40d0_0))
                    (<= v0x7fd01b3a0250_0 1.0)
                    (>= v0x7fd01b3a0250_0 1.0)
                    (and (<= v0x7fd01b3a0350_0 v0x7fd01b3a2310_0)
                         (>= v0x7fd01b3a0350_0 v0x7fd01b3a2310_0))
                    (and (<= v0x7fd01b39e010_0 v0x7fd01b3a4010_0)
                         (>= v0x7fd01b39e010_0 v0x7fd01b3a4010_0)))
               (and v0x7fd01b3a3f50_0
                    v0x7fd01b3a50d0_0
                    (and (<= v0x7fd01b3a0150_0 v0x7fd01b3a40d0_0)
                         (>= v0x7fd01b3a0150_0 v0x7fd01b3a40d0_0))
                    (<= v0x7fd01b3a0250_0 0.0)
                    (>= v0x7fd01b3a0250_0 0.0)
                    (and (<= v0x7fd01b3a0350_0 v0x7fd01b3a2310_0)
                         (>= v0x7fd01b3a0350_0 v0x7fd01b3a2310_0))
                    (and (<= v0x7fd01b39e010_0 v0x7fd01b3a4010_0)
                         (>= v0x7fd01b39e010_0 v0x7fd01b3a4010_0))))))
(let ((a!8 (and (=> v0x7fd01b3a0c50_0
                    (and v0x7fd01b3a0990_0
                         E0x7fd01b3a0d10
                         (not v0x7fd01b3a0b10_0)))
                (=> v0x7fd01b3a0c50_0 E0x7fd01b3a0d10)
                a!1
                a!2
                (=> v0x7fd01b3a1d10_0
                    (and v0x7fd01b3a1390_0
                         E0x7fd01b3a1dd0
                         (not v0x7fd01b3a1bd0_0)))
                (=> v0x7fd01b3a1d10_0 E0x7fd01b3a1dd0)
                a!3
                a!4
                (=> v0x7fd01b3a2bd0_0
                    (and v0x7fd01b3a2250_0 E0x7fd01b3a2c90 v0x7fd01b3a2a90_0))
                (=> v0x7fd01b3a2bd0_0 E0x7fd01b3a2c90)
                (=> v0x7fd01b3a2f90_0
                    (and v0x7fd01b3a2250_0
                         E0x7fd01b3a3050
                         (not v0x7fd01b3a2a90_0)))
                (=> v0x7fd01b3a2f90_0 E0x7fd01b3a3050)
                (=> v0x7fd01b3a37d0_0
                    (and v0x7fd01b3a2bd0_0 E0x7fd01b3a3890 v0x7fd01b3a2e50_0))
                (=> v0x7fd01b3a37d0_0 E0x7fd01b3a3890)
                (=> v0x7fd01b3a3cd0_0
                    (and v0x7fd01b3a2f90_0 E0x7fd01b3a3d90 v0x7fd01b3a3690_0))
                (=> v0x7fd01b3a3cd0_0 E0x7fd01b3a3d90)
                (=> v0x7fd01b3a3f50_0 a!5)
                a!6
                (=> v0x7fd01b3a5210_0
                    (and v0x7fd01b3a3f50_0
                         E0x7fd01b3a52d0
                         (not v0x7fd01b3a50d0_0)))
                (=> v0x7fd01b3a5210_0 E0x7fd01b3a52d0)
                a!7
                (= v0x7fd01b3a0b10_0 (= v0x7fd01b3a0a50_0 0.0))
                (= v0x7fd01b3a0f50_0 (< v0x7fd01b3a0290_0 2.0))
                (= v0x7fd01b3a1110_0 (ite v0x7fd01b3a0f50_0 1.0 0.0))
                (= v0x7fd01b3a1250_0 (+ v0x7fd01b3a1110_0 v0x7fd01b3a0290_0))
                (= v0x7fd01b3a1bd0_0 (= v0x7fd01b3a1b10_0 0.0))
                (= v0x7fd01b3a1fd0_0 (= v0x7fd01b3a0190_0 0.0))
                (= v0x7fd01b3a2110_0 (ite v0x7fd01b3a1fd0_0 1.0 0.0))
                (= v0x7fd01b3a2a90_0 (= v0x7fd01b39ff10_0 0.0))
                (= v0x7fd01b3a2e50_0 (> v0x7fd01b3a1450_0 1.0))
                (= v0x7fd01b3a3250_0 (> v0x7fd01b3a1450_0 0.0))
                (= v0x7fd01b3a3390_0 (+ v0x7fd01b3a1450_0 (- 1.0)))
                (= v0x7fd01b3a3550_0
                   (ite v0x7fd01b3a3250_0 v0x7fd01b3a3390_0 v0x7fd01b3a1450_0))
                (= v0x7fd01b3a3690_0 (= v0x7fd01b3a3550_0 0.0))
                (= v0x7fd01b3a3a50_0 (= v0x7fd01b3a2310_0 0.0))
                (= v0x7fd01b3a3b90_0
                   (ite v0x7fd01b3a3a50_0 1.0 v0x7fd01b39ff10_0))
                (= v0x7fd01b3a4e90_0 (= v0x7fd01b3a2310_0 0.0))
                (= v0x7fd01b3a4f90_0 (= v0x7fd01b3a40d0_0 0.0))
                (= v0x7fd01b3a50d0_0 (or v0x7fd01b3a4f90_0 v0x7fd01b3a4e90_0))
                (= v0x7fd01b3a54d0_0 (= v0x7fd01b3a0090_0 0.0)))))
  (=> F0x7fd01b3a6510 a!8))))
(assert (=> F0x7fd01b3a6510 F0x7fd01b3a6450))
(assert (let ((a!1 (=> v0x7fd01b3a1390_0
               (or (and v0x7fd01b3a0c50_0
                        E0x7fd01b3a1510
                        (<= v0x7fd01b3a1450_0 v0x7fd01b3a1250_0)
                        (>= v0x7fd01b3a1450_0 v0x7fd01b3a1250_0))
                   (and v0x7fd01b3a0990_0
                        E0x7fd01b3a16d0
                        v0x7fd01b3a0b10_0
                        (<= v0x7fd01b3a1450_0 v0x7fd01b3a0290_0)
                        (>= v0x7fd01b3a1450_0 v0x7fd01b3a0290_0)))))
      (a!2 (=> v0x7fd01b3a1390_0
               (or (and E0x7fd01b3a1510 (not E0x7fd01b3a16d0))
                   (and E0x7fd01b3a16d0 (not E0x7fd01b3a1510)))))
      (a!3 (=> v0x7fd01b3a2250_0
               (or (and v0x7fd01b3a1d10_0
                        E0x7fd01b3a23d0
                        (<= v0x7fd01b3a2310_0 v0x7fd01b3a2110_0)
                        (>= v0x7fd01b3a2310_0 v0x7fd01b3a2110_0))
                   (and v0x7fd01b3a1390_0
                        E0x7fd01b3a2590
                        v0x7fd01b3a1bd0_0
                        (<= v0x7fd01b3a2310_0 v0x7fd01b3a0190_0)
                        (>= v0x7fd01b3a2310_0 v0x7fd01b3a0190_0)))))
      (a!4 (=> v0x7fd01b3a2250_0
               (or (and E0x7fd01b3a23d0 (not E0x7fd01b3a2590))
                   (and E0x7fd01b3a2590 (not E0x7fd01b3a23d0)))))
      (a!5 (or (and v0x7fd01b3a37d0_0
                    E0x7fd01b3a4190
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0))
                    (<= v0x7fd01b3a40d0_0 v0x7fd01b3a3b90_0)
                    (>= v0x7fd01b3a40d0_0 v0x7fd01b3a3b90_0))
               (and v0x7fd01b3a2bd0_0
                    E0x7fd01b3a4450
                    (not v0x7fd01b3a2e50_0)
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a1450_0))
                    (and (<= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)
                         (>= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)))
               (and v0x7fd01b3a3cd0_0
                    E0x7fd01b3a46d0
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0))
                    (and (<= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)
                         (>= v0x7fd01b3a40d0_0 v0x7fd01b39ff10_0)))
               (and v0x7fd01b3a2f90_0
                    E0x7fd01b3a48d0
                    (not v0x7fd01b3a3690_0)
                    (and (<= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0)
                         (>= v0x7fd01b3a4010_0 v0x7fd01b3a3550_0))
                    (<= v0x7fd01b3a40d0_0 0.0)
                    (>= v0x7fd01b3a40d0_0 0.0))))
      (a!6 (=> v0x7fd01b3a3f50_0
               (or (and E0x7fd01b3a4190
                        (not E0x7fd01b3a4450)
                        (not E0x7fd01b3a46d0)
                        (not E0x7fd01b3a48d0))
                   (and E0x7fd01b3a4450
                        (not E0x7fd01b3a4190)
                        (not E0x7fd01b3a46d0)
                        (not E0x7fd01b3a48d0))
                   (and E0x7fd01b3a46d0
                        (not E0x7fd01b3a4190)
                        (not E0x7fd01b3a4450)
                        (not E0x7fd01b3a48d0))
                   (and E0x7fd01b3a48d0
                        (not E0x7fd01b3a4190)
                        (not E0x7fd01b3a4450)
                        (not E0x7fd01b3a46d0))))))
(let ((a!7 (and (=> v0x7fd01b3a0c50_0
                    (and v0x7fd01b3a0990_0
                         E0x7fd01b3a0d10
                         (not v0x7fd01b3a0b10_0)))
                (=> v0x7fd01b3a0c50_0 E0x7fd01b3a0d10)
                a!1
                a!2
                (=> v0x7fd01b3a1d10_0
                    (and v0x7fd01b3a1390_0
                         E0x7fd01b3a1dd0
                         (not v0x7fd01b3a1bd0_0)))
                (=> v0x7fd01b3a1d10_0 E0x7fd01b3a1dd0)
                a!3
                a!4
                (=> v0x7fd01b3a2bd0_0
                    (and v0x7fd01b3a2250_0 E0x7fd01b3a2c90 v0x7fd01b3a2a90_0))
                (=> v0x7fd01b3a2bd0_0 E0x7fd01b3a2c90)
                (=> v0x7fd01b3a2f90_0
                    (and v0x7fd01b3a2250_0
                         E0x7fd01b3a3050
                         (not v0x7fd01b3a2a90_0)))
                (=> v0x7fd01b3a2f90_0 E0x7fd01b3a3050)
                (=> v0x7fd01b3a37d0_0
                    (and v0x7fd01b3a2bd0_0 E0x7fd01b3a3890 v0x7fd01b3a2e50_0))
                (=> v0x7fd01b3a37d0_0 E0x7fd01b3a3890)
                (=> v0x7fd01b3a3cd0_0
                    (and v0x7fd01b3a2f90_0 E0x7fd01b3a3d90 v0x7fd01b3a3690_0))
                (=> v0x7fd01b3a3cd0_0 E0x7fd01b3a3d90)
                (=> v0x7fd01b3a3f50_0 a!5)
                a!6
                (=> v0x7fd01b3a5210_0
                    (and v0x7fd01b3a3f50_0
                         E0x7fd01b3a52d0
                         (not v0x7fd01b3a50d0_0)))
                (=> v0x7fd01b3a5210_0 E0x7fd01b3a52d0)
                v0x7fd01b3a5210_0
                (not v0x7fd01b3a54d0_0)
                (= v0x7fd01b3a0b10_0 (= v0x7fd01b3a0a50_0 0.0))
                (= v0x7fd01b3a0f50_0 (< v0x7fd01b3a0290_0 2.0))
                (= v0x7fd01b3a1110_0 (ite v0x7fd01b3a0f50_0 1.0 0.0))
                (= v0x7fd01b3a1250_0 (+ v0x7fd01b3a1110_0 v0x7fd01b3a0290_0))
                (= v0x7fd01b3a1bd0_0 (= v0x7fd01b3a1b10_0 0.0))
                (= v0x7fd01b3a1fd0_0 (= v0x7fd01b3a0190_0 0.0))
                (= v0x7fd01b3a2110_0 (ite v0x7fd01b3a1fd0_0 1.0 0.0))
                (= v0x7fd01b3a2a90_0 (= v0x7fd01b39ff10_0 0.0))
                (= v0x7fd01b3a2e50_0 (> v0x7fd01b3a1450_0 1.0))
                (= v0x7fd01b3a3250_0 (> v0x7fd01b3a1450_0 0.0))
                (= v0x7fd01b3a3390_0 (+ v0x7fd01b3a1450_0 (- 1.0)))
                (= v0x7fd01b3a3550_0
                   (ite v0x7fd01b3a3250_0 v0x7fd01b3a3390_0 v0x7fd01b3a1450_0))
                (= v0x7fd01b3a3690_0 (= v0x7fd01b3a3550_0 0.0))
                (= v0x7fd01b3a3a50_0 (= v0x7fd01b3a2310_0 0.0))
                (= v0x7fd01b3a3b90_0
                   (ite v0x7fd01b3a3a50_0 1.0 v0x7fd01b39ff10_0))
                (= v0x7fd01b3a4e90_0 (= v0x7fd01b3a2310_0 0.0))
                (= v0x7fd01b3a4f90_0 (= v0x7fd01b3a40d0_0 0.0))
                (= v0x7fd01b3a50d0_0 (or v0x7fd01b3a4f90_0 v0x7fd01b3a4e90_0))
                (= v0x7fd01b3a54d0_0 (= v0x7fd01b3a0090_0 0.0)))))
  (=> F0x7fd01b3a63d0 a!7))))
(assert (=> F0x7fd01b3a63d0 F0x7fd01b3a6450))
(assert (=> F0x7fd01b3a6690 (or F0x7fd01b3a6650 F0x7fd01b3a6510)))
(assert (=> F0x7fd01b3a6250 F0x7fd01b3a63d0))
(assert (=> pre!entry!0 (=> F0x7fd01b3a65d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd01b3a6450 (>= v0x7fd01b3a0090_0 0.0))))
(assert (let ((a!1 (=> F0x7fd01b3a6450
               (or (<= v0x7fd01b3a0090_0 0.0)
                   (not (<= v0x7fd01b3a0190_0 0.0))
                   (not (<= 0.0 v0x7fd01b3a0190_0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2
    (=> F0x7fd01b3a6450
        (or (>= v0x7fd01b3a0190_0 1.0) (<= v0x7fd01b3a0190_0 0.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7fd01b3a6450 (not (<= v0x7fd01b3a0290_0 0.0)))))
(assert (let ((a!1 (=> F0x7fd01b3a6450
               (or (<= v0x7fd01b39ff10_0 0.0) (not (<= v0x7fd01b3a0290_0 1.0))))))
  (=> pre!bb1.i.i!4 a!1)))
(assert (=> pre!bb1.i.i!5 (=> F0x7fd01b3a6450 (<= v0x7fd01b3a0090_0 0.0))))
(assert (=> pre!bb1.i.i!6 (=> F0x7fd01b3a6450 (>= v0x7fd01b39ff10_0 0.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fd01b3a6690 (>= v0x7fd01b3a0250_0 0.0))))
(assert (let ((a!1 (=> F0x7fd01b3a6690
               (or (<= v0x7fd01b3a0250_0 0.0)
                   (not (<= v0x7fd01b3a0350_0 0.0))
                   (not (<= 0.0 v0x7fd01b3a0350_0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!2
   (=> F0x7fd01b3a6690
       (or (>= v0x7fd01b3a0350_0 1.0) (<= v0x7fd01b3a0350_0 0.0)))))
(assert (= lemma!bb1.i.i!3 (=> F0x7fd01b3a6690 (not (<= v0x7fd01b39e010_0 0.0)))))
(assert (let ((a!1 (=> F0x7fd01b3a6690
               (or (<= v0x7fd01b3a0150_0 0.0) (not (<= v0x7fd01b39e010_0 1.0))))))
  (= lemma!bb1.i.i!4 a!1)))
(assert (= lemma!bb1.i.i!5 (=> F0x7fd01b3a6690 (<= v0x7fd01b3a0250_0 0.0))))
(assert (= lemma!bb1.i.i!6 (=> F0x7fd01b3a6690 (>= v0x7fd01b3a0150_0 0.0))))
(assert (= lemma!bb2.i.i34.i.i!0 (=> F0x7fd01b3a6250 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb1.i.i!6) (not lemma!bb1.i.i!6))
    (and (not post!bb2.i.i34.i.i!0) (not lemma!bb2.i.i34.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5 pre!bb1.i.i!6)
; (init: F0x7fd01b3a65d0)
; (error: F0x7fd01b3a6250)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb1.i.i!6 post!bb2.i.i34.i.i!0)
