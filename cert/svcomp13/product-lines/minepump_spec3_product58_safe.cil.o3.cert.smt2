(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun post!bb2.i.i34.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f303f410010 () Bool)
(declare-fun F0x7f303f410050 () Bool)
(declare-fun F0x7f303f40ff10 () Bool)
(declare-fun lemma!bb2.i.i34.i.i!0 () Bool)
(declare-fun v0x7f303f40efd0_0 () Bool)
(declare-fun F0x7f303f40fe50 () Bool)
(declare-fun v0x7f303f40ed50_0 () Bool)
(declare-fun v0x7f303f40d010_0 () Bool)
(declare-fun v0x7f303f40b8d0_0 () Real)
(declare-fun v0x7f303f40aed0_0 () Real)
(declare-fun v0x7f303f40ad10_0 () Bool)
(declare-fun E0x7f303f40e690 () Bool)
(declare-fun E0x7f303f40e490 () Bool)
(declare-fun v0x7f303f40d310_0 () Real)
(declare-fun E0x7f303f40e210 () Bool)
(declare-fun E0x7f303f40df50 () Bool)
(declare-fun v0x7f303f40dd10_0 () Bool)
(declare-fun E0x7f303f40db50 () Bool)
(declare-fun v0x7f303f40da90_0 () Bool)
(declare-fun v0x7f303f40cc10_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f303f409f10_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7f303f40d590_0 () Bool)
(declare-fun v0x7f303f40d450_0 () Bool)
(declare-fun E0x7f303f40ce10 () Bool)
(declare-fun E0x7f303f40d650 () Bool)
(declare-fun v0x7f303f40d150_0 () Real)
(declare-fun v0x7f303f40cd50_0 () Bool)
(declare-fun v0x7f303f40c850_0 () Bool)
(declare-fun v0x7f303f40bed0_0 () Real)
(declare-fun v0x7f303f40ee90_0 () Bool)
(declare-fun v0x7f303f40f110_0 () Bool)
(declare-fun v0x7f303f40d810_0 () Bool)
(declare-fun v0x7f303f40c0d0_0 () Real)
(declare-fun E0x7f303f40bb90 () Bool)
(declare-fun v0x7f303f40bad0_0 () Bool)
(declare-fun E0x7f303f40b2d0 () Bool)
(declare-fun v0x7f303f40a190_0 () Real)
(declare-fun v0x7f303f40a810_0 () Real)
(declare-fun v0x7f303f40de90_0 () Real)
(declare-fun v0x7f303f40b150_0 () Bool)
(declare-fun v0x7f303f40c010_0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun E0x7f303f40aad0 () Bool)
(declare-fun v0x7f303f40aa10_0 () Bool)
(declare-fun v0x7f303f40c990_0 () Bool)
(declare-fun F0x7f303f40fc90 () Bool)
(declare-fun v0x7f303f40bd90_0 () Bool)
(declare-fun v0x7f303f40a8d0_0 () Bool)
(declare-fun v0x7f303f40b210_0 () Real)
(declare-fun E0x7f303f40ca50 () Bool)
(declare-fun v0x7f303f408010_0 () Real)
(declare-fun E0x7f303f40c190 () Bool)
(declare-fun v0x7f303f40a250_0 () Real)
(declare-fun E0x7f303f40b490 () Bool)
(declare-fun v0x7f303f40b990_0 () Bool)
(declare-fun v0x7f303f40b010_0 () Real)
(declare-fun v0x7f303f40ec50_0 () Bool)
(declare-fun v0x7f303f40a150_0 () Real)
(declare-fun v0x7f303f40a090_0 () Real)
(declare-fun v0x7f303f40a750_0 () Bool)
(declare-fun v0x7f303f40ddd0_0 () Real)
(declare-fun v0x7f303f408110_0 () Bool)
(declare-fun v0x7f303f40d950_0 () Real)
(declare-fun F0x7f303f40fcd0 () Bool)
(declare-fun E0x7f303f40c350 () Bool)
(declare-fun F0x7f303f40fd90 () Bool)

(assert (=> F0x7f303f40fd90
    (and v0x7f303f408110_0
         (<= v0x7f303f40a150_0 0.0)
         (>= v0x7f303f40a150_0 0.0)
         (<= v0x7f303f40a250_0 0.0)
         (>= v0x7f303f40a250_0 0.0)
         (<= v0x7f303f408010_0 1.0)
         (>= v0x7f303f408010_0 1.0))))
(assert (=> F0x7f303f40fd90 F0x7f303f40fc90))
(assert (let ((a!1 (=> v0x7f303f40b150_0
               (or (and v0x7f303f40aa10_0
                        E0x7f303f40b2d0
                        (<= v0x7f303f40b210_0 v0x7f303f40b010_0)
                        (>= v0x7f303f40b210_0 v0x7f303f40b010_0))
                   (and v0x7f303f40a750_0
                        E0x7f303f40b490
                        v0x7f303f40a8d0_0
                        (<= v0x7f303f40b210_0 v0x7f303f40a190_0)
                        (>= v0x7f303f40b210_0 v0x7f303f40a190_0)))))
      (a!2 (=> v0x7f303f40b150_0
               (or (and E0x7f303f40b2d0 (not E0x7f303f40b490))
                   (and E0x7f303f40b490 (not E0x7f303f40b2d0)))))
      (a!3 (=> v0x7f303f40c010_0
               (or (and v0x7f303f40bad0_0
                        E0x7f303f40c190
                        (<= v0x7f303f40c0d0_0 v0x7f303f40bed0_0)
                        (>= v0x7f303f40c0d0_0 v0x7f303f40bed0_0))
                   (and v0x7f303f40b150_0
                        E0x7f303f40c350
                        v0x7f303f40b990_0
                        (<= v0x7f303f40c0d0_0 v0x7f303f40a090_0)
                        (>= v0x7f303f40c0d0_0 v0x7f303f40a090_0)))))
      (a!4 (=> v0x7f303f40c010_0
               (or (and E0x7f303f40c190 (not E0x7f303f40c350))
                   (and E0x7f303f40c350 (not E0x7f303f40c190)))))
      (a!5 (or (and v0x7f303f40d590_0
                    E0x7f303f40df50
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40b210_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40b210_0))
                    (<= v0x7f303f40de90_0 v0x7f303f40d950_0)
                    (>= v0x7f303f40de90_0 v0x7f303f40d950_0))
               (and v0x7f303f40c990_0
                    E0x7f303f40e210
                    (not v0x7f303f40cc10_0)
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40b210_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40b210_0))
                    (and (<= v0x7f303f40de90_0 v0x7f303f409f10_0)
                         (>= v0x7f303f40de90_0 v0x7f303f409f10_0)))
               (and v0x7f303f40da90_0
                    E0x7f303f40e490
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40d310_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40d310_0))
                    (and (<= v0x7f303f40de90_0 v0x7f303f409f10_0)
                         (>= v0x7f303f40de90_0 v0x7f303f409f10_0)))
               (and v0x7f303f40cd50_0
                    E0x7f303f40e690
                    (not v0x7f303f40d450_0)
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40d310_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40d310_0))
                    (<= v0x7f303f40de90_0 0.0)
                    (>= v0x7f303f40de90_0 0.0))))
      (a!6 (=> v0x7f303f40dd10_0
               (or (and E0x7f303f40df50
                        (not E0x7f303f40e210)
                        (not E0x7f303f40e490)
                        (not E0x7f303f40e690))
                   (and E0x7f303f40e210
                        (not E0x7f303f40df50)
                        (not E0x7f303f40e490)
                        (not E0x7f303f40e690))
                   (and E0x7f303f40e490
                        (not E0x7f303f40df50)
                        (not E0x7f303f40e210)
                        (not E0x7f303f40e690))
                   (and E0x7f303f40e690
                        (not E0x7f303f40df50)
                        (not E0x7f303f40e210)
                        (not E0x7f303f40e490))))))
(let ((a!7 (and (=> v0x7f303f40aa10_0
                    (and v0x7f303f40a750_0
                         E0x7f303f40aad0
                         (not v0x7f303f40a8d0_0)))
                (=> v0x7f303f40aa10_0 E0x7f303f40aad0)
                a!1
                a!2
                (=> v0x7f303f40bad0_0
                    (and v0x7f303f40b150_0
                         E0x7f303f40bb90
                         (not v0x7f303f40b990_0)))
                (=> v0x7f303f40bad0_0 E0x7f303f40bb90)
                a!3
                a!4
                (=> v0x7f303f40c990_0
                    (and v0x7f303f40c010_0 E0x7f303f40ca50 v0x7f303f40c850_0))
                (=> v0x7f303f40c990_0 E0x7f303f40ca50)
                (=> v0x7f303f40cd50_0
                    (and v0x7f303f40c010_0
                         E0x7f303f40ce10
                         (not v0x7f303f40c850_0)))
                (=> v0x7f303f40cd50_0 E0x7f303f40ce10)
                (=> v0x7f303f40d590_0
                    (and v0x7f303f40c990_0 E0x7f303f40d650 v0x7f303f40cc10_0))
                (=> v0x7f303f40d590_0 E0x7f303f40d650)
                (=> v0x7f303f40da90_0
                    (and v0x7f303f40cd50_0 E0x7f303f40db50 v0x7f303f40d450_0))
                (=> v0x7f303f40da90_0 E0x7f303f40db50)
                (=> v0x7f303f40dd10_0 a!5)
                a!6
                v0x7f303f40dd10_0
                (not v0x7f303f40f110_0)
                (<= v0x7f303f40a150_0 v0x7f303f40de90_0)
                (>= v0x7f303f40a150_0 v0x7f303f40de90_0)
                (<= v0x7f303f40a250_0 v0x7f303f40c0d0_0)
                (>= v0x7f303f40a250_0 v0x7f303f40c0d0_0)
                (<= v0x7f303f408010_0 v0x7f303f40ddd0_0)
                (>= v0x7f303f408010_0 v0x7f303f40ddd0_0)
                (= v0x7f303f40a8d0_0 (= v0x7f303f40a810_0 0.0))
                (= v0x7f303f40ad10_0 (< v0x7f303f40a190_0 2.0))
                (= v0x7f303f40aed0_0 (ite v0x7f303f40ad10_0 1.0 0.0))
                (= v0x7f303f40b010_0 (+ v0x7f303f40aed0_0 v0x7f303f40a190_0))
                (= v0x7f303f40b990_0 (= v0x7f303f40b8d0_0 0.0))
                (= v0x7f303f40bd90_0 (= v0x7f303f40a090_0 0.0))
                (= v0x7f303f40bed0_0 (ite v0x7f303f40bd90_0 1.0 0.0))
                (= v0x7f303f40c850_0 (= v0x7f303f409f10_0 0.0))
                (= v0x7f303f40cc10_0 (> v0x7f303f40b210_0 1.0))
                (= v0x7f303f40d010_0 (> v0x7f303f40b210_0 0.0))
                (= v0x7f303f40d150_0 (+ v0x7f303f40b210_0 (- 1.0)))
                (= v0x7f303f40d310_0
                   (ite v0x7f303f40d010_0 v0x7f303f40d150_0 v0x7f303f40b210_0))
                (= v0x7f303f40d450_0 (= v0x7f303f40d310_0 0.0))
                (= v0x7f303f40d810_0 (= v0x7f303f40c0d0_0 0.0))
                (= v0x7f303f40d950_0
                   (ite v0x7f303f40d810_0 1.0 v0x7f303f409f10_0))
                (= v0x7f303f40ec50_0 (= v0x7f303f40c0d0_0 0.0))
                (= v0x7f303f40ed50_0 (= v0x7f303f40ddd0_0 2.0))
                (= v0x7f303f40ee90_0 (= v0x7f303f40de90_0 0.0))
                (= v0x7f303f40efd0_0 (and v0x7f303f40ed50_0 v0x7f303f40ec50_0))
                (= v0x7f303f40f110_0 (and v0x7f303f40efd0_0 v0x7f303f40ee90_0)))))
  (=> F0x7f303f40fcd0 a!7))))
(assert (=> F0x7f303f40fcd0 F0x7f303f40fe50))
(assert (let ((a!1 (=> v0x7f303f40b150_0
               (or (and v0x7f303f40aa10_0
                        E0x7f303f40b2d0
                        (<= v0x7f303f40b210_0 v0x7f303f40b010_0)
                        (>= v0x7f303f40b210_0 v0x7f303f40b010_0))
                   (and v0x7f303f40a750_0
                        E0x7f303f40b490
                        v0x7f303f40a8d0_0
                        (<= v0x7f303f40b210_0 v0x7f303f40a190_0)
                        (>= v0x7f303f40b210_0 v0x7f303f40a190_0)))))
      (a!2 (=> v0x7f303f40b150_0
               (or (and E0x7f303f40b2d0 (not E0x7f303f40b490))
                   (and E0x7f303f40b490 (not E0x7f303f40b2d0)))))
      (a!3 (=> v0x7f303f40c010_0
               (or (and v0x7f303f40bad0_0
                        E0x7f303f40c190
                        (<= v0x7f303f40c0d0_0 v0x7f303f40bed0_0)
                        (>= v0x7f303f40c0d0_0 v0x7f303f40bed0_0))
                   (and v0x7f303f40b150_0
                        E0x7f303f40c350
                        v0x7f303f40b990_0
                        (<= v0x7f303f40c0d0_0 v0x7f303f40a090_0)
                        (>= v0x7f303f40c0d0_0 v0x7f303f40a090_0)))))
      (a!4 (=> v0x7f303f40c010_0
               (or (and E0x7f303f40c190 (not E0x7f303f40c350))
                   (and E0x7f303f40c350 (not E0x7f303f40c190)))))
      (a!5 (or (and v0x7f303f40d590_0
                    E0x7f303f40df50
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40b210_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40b210_0))
                    (<= v0x7f303f40de90_0 v0x7f303f40d950_0)
                    (>= v0x7f303f40de90_0 v0x7f303f40d950_0))
               (and v0x7f303f40c990_0
                    E0x7f303f40e210
                    (not v0x7f303f40cc10_0)
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40b210_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40b210_0))
                    (and (<= v0x7f303f40de90_0 v0x7f303f409f10_0)
                         (>= v0x7f303f40de90_0 v0x7f303f409f10_0)))
               (and v0x7f303f40da90_0
                    E0x7f303f40e490
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40d310_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40d310_0))
                    (and (<= v0x7f303f40de90_0 v0x7f303f409f10_0)
                         (>= v0x7f303f40de90_0 v0x7f303f409f10_0)))
               (and v0x7f303f40cd50_0
                    E0x7f303f40e690
                    (not v0x7f303f40d450_0)
                    (and (<= v0x7f303f40ddd0_0 v0x7f303f40d310_0)
                         (>= v0x7f303f40ddd0_0 v0x7f303f40d310_0))
                    (<= v0x7f303f40de90_0 0.0)
                    (>= v0x7f303f40de90_0 0.0))))
      (a!6 (=> v0x7f303f40dd10_0
               (or (and E0x7f303f40df50
                        (not E0x7f303f40e210)
                        (not E0x7f303f40e490)
                        (not E0x7f303f40e690))
                   (and E0x7f303f40e210
                        (not E0x7f303f40df50)
                        (not E0x7f303f40e490)
                        (not E0x7f303f40e690))
                   (and E0x7f303f40e490
                        (not E0x7f303f40df50)
                        (not E0x7f303f40e210)
                        (not E0x7f303f40e690))
                   (and E0x7f303f40e690
                        (not E0x7f303f40df50)
                        (not E0x7f303f40e210)
                        (not E0x7f303f40e490))))))
(let ((a!7 (and (=> v0x7f303f40aa10_0
                    (and v0x7f303f40a750_0
                         E0x7f303f40aad0
                         (not v0x7f303f40a8d0_0)))
                (=> v0x7f303f40aa10_0 E0x7f303f40aad0)
                a!1
                a!2
                (=> v0x7f303f40bad0_0
                    (and v0x7f303f40b150_0
                         E0x7f303f40bb90
                         (not v0x7f303f40b990_0)))
                (=> v0x7f303f40bad0_0 E0x7f303f40bb90)
                a!3
                a!4
                (=> v0x7f303f40c990_0
                    (and v0x7f303f40c010_0 E0x7f303f40ca50 v0x7f303f40c850_0))
                (=> v0x7f303f40c990_0 E0x7f303f40ca50)
                (=> v0x7f303f40cd50_0
                    (and v0x7f303f40c010_0
                         E0x7f303f40ce10
                         (not v0x7f303f40c850_0)))
                (=> v0x7f303f40cd50_0 E0x7f303f40ce10)
                (=> v0x7f303f40d590_0
                    (and v0x7f303f40c990_0 E0x7f303f40d650 v0x7f303f40cc10_0))
                (=> v0x7f303f40d590_0 E0x7f303f40d650)
                (=> v0x7f303f40da90_0
                    (and v0x7f303f40cd50_0 E0x7f303f40db50 v0x7f303f40d450_0))
                (=> v0x7f303f40da90_0 E0x7f303f40db50)
                (=> v0x7f303f40dd10_0 a!5)
                a!6
                v0x7f303f40dd10_0
                v0x7f303f40f110_0
                (= v0x7f303f40a8d0_0 (= v0x7f303f40a810_0 0.0))
                (= v0x7f303f40ad10_0 (< v0x7f303f40a190_0 2.0))
                (= v0x7f303f40aed0_0 (ite v0x7f303f40ad10_0 1.0 0.0))
                (= v0x7f303f40b010_0 (+ v0x7f303f40aed0_0 v0x7f303f40a190_0))
                (= v0x7f303f40b990_0 (= v0x7f303f40b8d0_0 0.0))
                (= v0x7f303f40bd90_0 (= v0x7f303f40a090_0 0.0))
                (= v0x7f303f40bed0_0 (ite v0x7f303f40bd90_0 1.0 0.0))
                (= v0x7f303f40c850_0 (= v0x7f303f409f10_0 0.0))
                (= v0x7f303f40cc10_0 (> v0x7f303f40b210_0 1.0))
                (= v0x7f303f40d010_0 (> v0x7f303f40b210_0 0.0))
                (= v0x7f303f40d150_0 (+ v0x7f303f40b210_0 (- 1.0)))
                (= v0x7f303f40d310_0
                   (ite v0x7f303f40d010_0 v0x7f303f40d150_0 v0x7f303f40b210_0))
                (= v0x7f303f40d450_0 (= v0x7f303f40d310_0 0.0))
                (= v0x7f303f40d810_0 (= v0x7f303f40c0d0_0 0.0))
                (= v0x7f303f40d950_0
                   (ite v0x7f303f40d810_0 1.0 v0x7f303f409f10_0))
                (= v0x7f303f40ec50_0 (= v0x7f303f40c0d0_0 0.0))
                (= v0x7f303f40ed50_0 (= v0x7f303f40ddd0_0 2.0))
                (= v0x7f303f40ee90_0 (= v0x7f303f40de90_0 0.0))
                (= v0x7f303f40efd0_0 (and v0x7f303f40ed50_0 v0x7f303f40ec50_0))
                (= v0x7f303f40f110_0 (and v0x7f303f40efd0_0 v0x7f303f40ee90_0)))))
  (=> F0x7f303f40ff10 a!7))))
(assert (=> F0x7f303f40ff10 F0x7f303f40fe50))
(assert (=> F0x7f303f410050 (or F0x7f303f40fd90 F0x7f303f40fcd0)))
(assert (=> F0x7f303f410010 F0x7f303f40ff10))
(assert (=> pre!entry!0 (=> F0x7f303f40fc90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f303f40fe50 (>= v0x7f303f409f10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f303f40fe50 (>= v0x7f303f40a090_0 0.0))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f303f40fe50 (not (<= 3.0 v0x7f303f40a190_0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f303f410050 (>= v0x7f303f40a150_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f303f410050 (>= v0x7f303f40a250_0 0.0))))
(assert (= lemma!bb1.i.i!2 (=> F0x7f303f410050 (not (<= 3.0 v0x7f303f408010_0)))))
(assert (= lemma!bb2.i.i34.i.i!0 (=> F0x7f303f410010 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb2.i.i34.i.i!0) (not lemma!bb2.i.i34.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
; (init: F0x7f303f40fc90)
; (error: F0x7f303f410010)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb2.i.i34.i.i!0)
