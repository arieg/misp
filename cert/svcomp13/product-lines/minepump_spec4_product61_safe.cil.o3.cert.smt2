(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i43.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7fd65d651510 () Bool)
(declare-fun F0x7fd65d651550 () Bool)
(declare-fun F0x7fd65d651410 () Bool)
(declare-fun v0x7fd65d6504d0_0 () Bool)
(declare-fun v0x7fd65d650390_0 () Bool)
(declare-fun v0x7fd65d64ea90_0 () Bool)
(declare-fun v0x7fd65d64e650_0 () Real)
(declare-fun v0x7fd65d64e510_0 () Bool)
(declare-fun v0x7fd65d64d290_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fd65d64cdd0_0 () Real)
(declare-fun v0x7fd65d64c3d0_0 () Real)
(declare-fun v0x7fd65d650610_0 () Bool)
(declare-fun v0x7fd65d64e810_0 () Real)
(declare-fun E0x7fd65d64fbd0 () Bool)
(declare-fun v0x7fd65d64b690_0 () Real)
(declare-fun E0x7fd65d64f950 () Bool)
(declare-fun v0x7fd65d64e950_0 () Bool)
(declare-fun v0x7fd65d64f090_0 () Real)
(declare-fun v0x7fd65d64f450_0 () Bool)
(declare-fun E0x7fd65d64f690 () Bool)
(declare-fun v0x7fd65d64ebd0_0 () Bool)
(declare-fun E0x7fd65d64f290 () Bool)
(declare-fun v0x7fd65d64c210_0 () Bool)
(declare-fun v0x7fd65d64f1d0_0 () Bool)
(declare-fun v0x7fd65d64e110_0 () Bool)
(declare-fun v0x7fd65d64ed10_0 () Bool)
(declare-fun E0x7fd65d64e310 () Bool)
(declare-fun v0x7fd65d64e250_0 () Bool)
(declare-fun v0x7fd65d64de90_0 () Bool)
(declare-fun v0x7fd65d64f510_0 () Real)
(declare-fun v0x7fd65d64b410_0 () Real)
(declare-fun E0x7fd65d64fdd0 () Bool)
(declare-fun v0x7fd65d64d5d0_0 () Real)
(declare-fun v0x7fd65d64d510_0 () Bool)
(declare-fun v0x7fd65d64ce90_0 () Bool)
(declare-fun v0x7fd65d64b590_0 () Real)
(declare-fun E0x7fd65d64c990 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fd65d64c710_0 () Real)
(declare-fun F0x7fd65d651350 () Bool)
(declare-fun v0x7fd65d64d3d0_0 () Real)
(declare-fun v0x7fd65d64ef90_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7fd65d64d850 () Bool)
(declare-fun E0x7fd65d64bfd0 () Bool)
(declare-fun v0x7fd65d64cfd0_0 () Bool)
(declare-fun E0x7fd65d64c7d0 () Bool)
(declare-fun v0x7fd65d64bc50_0 () Bool)
(declare-fun E0x7fd65d64d090 () Bool)
(declare-fun v0x7fd65d64f5d0_0 () Real)
(declare-fun F0x7fd65d6511d0 () Bool)
(declare-fun v0x7fd65d64b750_0 () Real)
(declare-fun v0x7fd65d648010_0 () Real)
(declare-fun v0x7fd65d64c650_0 () Bool)
(declare-fun v0x7fd65d64dd50_0 () Bool)
(declare-fun v0x7fd65d64b650_0 () Real)
(declare-fun F0x7fd65d651110 () Bool)
(declare-fun post!bb1.i.i43.i.i!0 () Bool)
(declare-fun E0x7fd65d64d690 () Bool)
(declare-fun v0x7fd65d64c510_0 () Real)
(declare-fun v0x7fd65d64bd10_0 () Real)
(declare-fun E0x7fd65d64edd0 () Bool)
(declare-fun v0x7fd65d648110_0 () Bool)
(declare-fun E0x7fd65d64df50 () Bool)
(declare-fun v0x7fd65d64bf10_0 () Bool)
(declare-fun F0x7fd65d651290 () Bool)
(declare-fun v0x7fd65d64bdd0_0 () Bool)

(assert (=> F0x7fd65d651290
    (and v0x7fd65d648110_0
         (<= v0x7fd65d64b650_0 0.0)
         (>= v0x7fd65d64b650_0 0.0)
         (<= v0x7fd65d64b750_0 1.0)
         (>= v0x7fd65d64b750_0 1.0)
         (<= v0x7fd65d648010_0 0.0)
         (>= v0x7fd65d648010_0 0.0))))
(assert (=> F0x7fd65d651290 F0x7fd65d6511d0))
(assert (let ((a!1 (=> v0x7fd65d64c650_0
               (or (and v0x7fd65d64bf10_0
                        E0x7fd65d64c7d0
                        (<= v0x7fd65d64c710_0 v0x7fd65d64c510_0)
                        (>= v0x7fd65d64c710_0 v0x7fd65d64c510_0))
                   (and v0x7fd65d64bc50_0
                        E0x7fd65d64c990
                        v0x7fd65d64bdd0_0
                        (<= v0x7fd65d64c710_0 v0x7fd65d64b590_0)
                        (>= v0x7fd65d64c710_0 v0x7fd65d64b590_0)))))
      (a!2 (=> v0x7fd65d64c650_0
               (or (and E0x7fd65d64c7d0 (not E0x7fd65d64c990))
                   (and E0x7fd65d64c990 (not E0x7fd65d64c7d0)))))
      (a!3 (=> v0x7fd65d64d510_0
               (or (and v0x7fd65d64cfd0_0
                        E0x7fd65d64d690
                        (<= v0x7fd65d64d5d0_0 v0x7fd65d64d3d0_0)
                        (>= v0x7fd65d64d5d0_0 v0x7fd65d64d3d0_0))
                   (and v0x7fd65d64c650_0
                        E0x7fd65d64d850
                        v0x7fd65d64ce90_0
                        (<= v0x7fd65d64d5d0_0 v0x7fd65d64b410_0)
                        (>= v0x7fd65d64d5d0_0 v0x7fd65d64b410_0)))))
      (a!4 (=> v0x7fd65d64d510_0
               (or (and E0x7fd65d64d690 (not E0x7fd65d64d850))
                   (and E0x7fd65d64d850 (not E0x7fd65d64d690)))))
      (a!5 (or (and v0x7fd65d64ed10_0
                    E0x7fd65d64f690
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64c710_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64c710_0))
                    (<= v0x7fd65d64f5d0_0 v0x7fd65d64f090_0)
                    (>= v0x7fd65d64f5d0_0 v0x7fd65d64f090_0))
               (and v0x7fd65d64de90_0
                    E0x7fd65d64f950
                    (not v0x7fd65d64e110_0)
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64c710_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64c710_0))
                    (and (<= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)
                         (>= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)))
               (and v0x7fd65d64f1d0_0
                    E0x7fd65d64fbd0
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64e810_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64e810_0))
                    (and (<= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)
                         (>= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)))
               (and v0x7fd65d64e250_0
                    E0x7fd65d64fdd0
                    (not v0x7fd65d64ebd0_0)
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64e810_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64e810_0))
                    (<= v0x7fd65d64f5d0_0 0.0)
                    (>= v0x7fd65d64f5d0_0 0.0))))
      (a!6 (=> v0x7fd65d64f450_0
               (or (and E0x7fd65d64f690
                        (not E0x7fd65d64f950)
                        (not E0x7fd65d64fbd0)
                        (not E0x7fd65d64fdd0))
                   (and E0x7fd65d64f950
                        (not E0x7fd65d64f690)
                        (not E0x7fd65d64fbd0)
                        (not E0x7fd65d64fdd0))
                   (and E0x7fd65d64fbd0
                        (not E0x7fd65d64f690)
                        (not E0x7fd65d64f950)
                        (not E0x7fd65d64fdd0))
                   (and E0x7fd65d64fdd0
                        (not E0x7fd65d64f690)
                        (not E0x7fd65d64f950)
                        (not E0x7fd65d64fbd0))))))
(let ((a!7 (and (=> v0x7fd65d64bf10_0
                    (and v0x7fd65d64bc50_0
                         E0x7fd65d64bfd0
                         (not v0x7fd65d64bdd0_0)))
                (=> v0x7fd65d64bf10_0 E0x7fd65d64bfd0)
                a!1
                a!2
                (=> v0x7fd65d64cfd0_0
                    (and v0x7fd65d64c650_0
                         E0x7fd65d64d090
                         (not v0x7fd65d64ce90_0)))
                (=> v0x7fd65d64cfd0_0 E0x7fd65d64d090)
                a!3
                a!4
                (=> v0x7fd65d64de90_0
                    (and v0x7fd65d64d510_0 E0x7fd65d64df50 v0x7fd65d64dd50_0))
                (=> v0x7fd65d64de90_0 E0x7fd65d64df50)
                (=> v0x7fd65d64e250_0
                    (and v0x7fd65d64d510_0
                         E0x7fd65d64e310
                         (not v0x7fd65d64dd50_0)))
                (=> v0x7fd65d64e250_0 E0x7fd65d64e310)
                (=> v0x7fd65d64ed10_0
                    (and v0x7fd65d64de90_0 E0x7fd65d64edd0 v0x7fd65d64e110_0))
                (=> v0x7fd65d64ed10_0 E0x7fd65d64edd0)
                (=> v0x7fd65d64f1d0_0
                    (and v0x7fd65d64e250_0 E0x7fd65d64f290 v0x7fd65d64ebd0_0))
                (=> v0x7fd65d64f1d0_0 E0x7fd65d64f290)
                (=> v0x7fd65d64f450_0 a!5)
                a!6
                v0x7fd65d64f450_0
                v0x7fd65d650610_0
                (<= v0x7fd65d64b650_0 v0x7fd65d64d5d0_0)
                (>= v0x7fd65d64b650_0 v0x7fd65d64d5d0_0)
                (<= v0x7fd65d64b750_0 v0x7fd65d64f510_0)
                (>= v0x7fd65d64b750_0 v0x7fd65d64f510_0)
                (<= v0x7fd65d648010_0 v0x7fd65d64f5d0_0)
                (>= v0x7fd65d648010_0 v0x7fd65d64f5d0_0)
                (= v0x7fd65d64bdd0_0 (= v0x7fd65d64bd10_0 0.0))
                (= v0x7fd65d64c210_0 (< v0x7fd65d64b590_0 2.0))
                (= v0x7fd65d64c3d0_0 (ite v0x7fd65d64c210_0 1.0 0.0))
                (= v0x7fd65d64c510_0 (+ v0x7fd65d64c3d0_0 v0x7fd65d64b590_0))
                (= v0x7fd65d64ce90_0 (= v0x7fd65d64cdd0_0 0.0))
                (= v0x7fd65d64d290_0 (= v0x7fd65d64b410_0 0.0))
                (= v0x7fd65d64d3d0_0 (ite v0x7fd65d64d290_0 1.0 0.0))
                (= v0x7fd65d64dd50_0 (= v0x7fd65d64b690_0 0.0))
                (= v0x7fd65d64e110_0 (> v0x7fd65d64c710_0 1.0))
                (= v0x7fd65d64e510_0 (> v0x7fd65d64c710_0 0.0))
                (= v0x7fd65d64e650_0 (+ v0x7fd65d64c710_0 (- 1.0)))
                (= v0x7fd65d64e810_0
                   (ite v0x7fd65d64e510_0 v0x7fd65d64e650_0 v0x7fd65d64c710_0))
                (= v0x7fd65d64e950_0 (= v0x7fd65d64d5d0_0 0.0))
                (= v0x7fd65d64ea90_0 (= v0x7fd65d64e810_0 0.0))
                (= v0x7fd65d64ebd0_0 (and v0x7fd65d64e950_0 v0x7fd65d64ea90_0))
                (= v0x7fd65d64ef90_0 (= v0x7fd65d64d5d0_0 0.0))
                (= v0x7fd65d64f090_0
                   (ite v0x7fd65d64ef90_0 1.0 v0x7fd65d64b690_0))
                (= v0x7fd65d650390_0 (not (= v0x7fd65d64f510_0 0.0)))
                (= v0x7fd65d6504d0_0 (= v0x7fd65d64f5d0_0 0.0))
                (= v0x7fd65d650610_0 (or v0x7fd65d6504d0_0 v0x7fd65d650390_0)))))
  (=> F0x7fd65d651110 a!7))))
(assert (=> F0x7fd65d651110 F0x7fd65d651350))
(assert (let ((a!1 (=> v0x7fd65d64c650_0
               (or (and v0x7fd65d64bf10_0
                        E0x7fd65d64c7d0
                        (<= v0x7fd65d64c710_0 v0x7fd65d64c510_0)
                        (>= v0x7fd65d64c710_0 v0x7fd65d64c510_0))
                   (and v0x7fd65d64bc50_0
                        E0x7fd65d64c990
                        v0x7fd65d64bdd0_0
                        (<= v0x7fd65d64c710_0 v0x7fd65d64b590_0)
                        (>= v0x7fd65d64c710_0 v0x7fd65d64b590_0)))))
      (a!2 (=> v0x7fd65d64c650_0
               (or (and E0x7fd65d64c7d0 (not E0x7fd65d64c990))
                   (and E0x7fd65d64c990 (not E0x7fd65d64c7d0)))))
      (a!3 (=> v0x7fd65d64d510_0
               (or (and v0x7fd65d64cfd0_0
                        E0x7fd65d64d690
                        (<= v0x7fd65d64d5d0_0 v0x7fd65d64d3d0_0)
                        (>= v0x7fd65d64d5d0_0 v0x7fd65d64d3d0_0))
                   (and v0x7fd65d64c650_0
                        E0x7fd65d64d850
                        v0x7fd65d64ce90_0
                        (<= v0x7fd65d64d5d0_0 v0x7fd65d64b410_0)
                        (>= v0x7fd65d64d5d0_0 v0x7fd65d64b410_0)))))
      (a!4 (=> v0x7fd65d64d510_0
               (or (and E0x7fd65d64d690 (not E0x7fd65d64d850))
                   (and E0x7fd65d64d850 (not E0x7fd65d64d690)))))
      (a!5 (or (and v0x7fd65d64ed10_0
                    E0x7fd65d64f690
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64c710_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64c710_0))
                    (<= v0x7fd65d64f5d0_0 v0x7fd65d64f090_0)
                    (>= v0x7fd65d64f5d0_0 v0x7fd65d64f090_0))
               (and v0x7fd65d64de90_0
                    E0x7fd65d64f950
                    (not v0x7fd65d64e110_0)
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64c710_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64c710_0))
                    (and (<= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)
                         (>= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)))
               (and v0x7fd65d64f1d0_0
                    E0x7fd65d64fbd0
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64e810_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64e810_0))
                    (and (<= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)
                         (>= v0x7fd65d64f5d0_0 v0x7fd65d64b690_0)))
               (and v0x7fd65d64e250_0
                    E0x7fd65d64fdd0
                    (not v0x7fd65d64ebd0_0)
                    (and (<= v0x7fd65d64f510_0 v0x7fd65d64e810_0)
                         (>= v0x7fd65d64f510_0 v0x7fd65d64e810_0))
                    (<= v0x7fd65d64f5d0_0 0.0)
                    (>= v0x7fd65d64f5d0_0 0.0))))
      (a!6 (=> v0x7fd65d64f450_0
               (or (and E0x7fd65d64f690
                        (not E0x7fd65d64f950)
                        (not E0x7fd65d64fbd0)
                        (not E0x7fd65d64fdd0))
                   (and E0x7fd65d64f950
                        (not E0x7fd65d64f690)
                        (not E0x7fd65d64fbd0)
                        (not E0x7fd65d64fdd0))
                   (and E0x7fd65d64fbd0
                        (not E0x7fd65d64f690)
                        (not E0x7fd65d64f950)
                        (not E0x7fd65d64fdd0))
                   (and E0x7fd65d64fdd0
                        (not E0x7fd65d64f690)
                        (not E0x7fd65d64f950)
                        (not E0x7fd65d64fbd0))))))
(let ((a!7 (and (=> v0x7fd65d64bf10_0
                    (and v0x7fd65d64bc50_0
                         E0x7fd65d64bfd0
                         (not v0x7fd65d64bdd0_0)))
                (=> v0x7fd65d64bf10_0 E0x7fd65d64bfd0)
                a!1
                a!2
                (=> v0x7fd65d64cfd0_0
                    (and v0x7fd65d64c650_0
                         E0x7fd65d64d090
                         (not v0x7fd65d64ce90_0)))
                (=> v0x7fd65d64cfd0_0 E0x7fd65d64d090)
                a!3
                a!4
                (=> v0x7fd65d64de90_0
                    (and v0x7fd65d64d510_0 E0x7fd65d64df50 v0x7fd65d64dd50_0))
                (=> v0x7fd65d64de90_0 E0x7fd65d64df50)
                (=> v0x7fd65d64e250_0
                    (and v0x7fd65d64d510_0
                         E0x7fd65d64e310
                         (not v0x7fd65d64dd50_0)))
                (=> v0x7fd65d64e250_0 E0x7fd65d64e310)
                (=> v0x7fd65d64ed10_0
                    (and v0x7fd65d64de90_0 E0x7fd65d64edd0 v0x7fd65d64e110_0))
                (=> v0x7fd65d64ed10_0 E0x7fd65d64edd0)
                (=> v0x7fd65d64f1d0_0
                    (and v0x7fd65d64e250_0 E0x7fd65d64f290 v0x7fd65d64ebd0_0))
                (=> v0x7fd65d64f1d0_0 E0x7fd65d64f290)
                (=> v0x7fd65d64f450_0 a!5)
                a!6
                v0x7fd65d64f450_0
                (not v0x7fd65d650610_0)
                (= v0x7fd65d64bdd0_0 (= v0x7fd65d64bd10_0 0.0))
                (= v0x7fd65d64c210_0 (< v0x7fd65d64b590_0 2.0))
                (= v0x7fd65d64c3d0_0 (ite v0x7fd65d64c210_0 1.0 0.0))
                (= v0x7fd65d64c510_0 (+ v0x7fd65d64c3d0_0 v0x7fd65d64b590_0))
                (= v0x7fd65d64ce90_0 (= v0x7fd65d64cdd0_0 0.0))
                (= v0x7fd65d64d290_0 (= v0x7fd65d64b410_0 0.0))
                (= v0x7fd65d64d3d0_0 (ite v0x7fd65d64d290_0 1.0 0.0))
                (= v0x7fd65d64dd50_0 (= v0x7fd65d64b690_0 0.0))
                (= v0x7fd65d64e110_0 (> v0x7fd65d64c710_0 1.0))
                (= v0x7fd65d64e510_0 (> v0x7fd65d64c710_0 0.0))
                (= v0x7fd65d64e650_0 (+ v0x7fd65d64c710_0 (- 1.0)))
                (= v0x7fd65d64e810_0
                   (ite v0x7fd65d64e510_0 v0x7fd65d64e650_0 v0x7fd65d64c710_0))
                (= v0x7fd65d64e950_0 (= v0x7fd65d64d5d0_0 0.0))
                (= v0x7fd65d64ea90_0 (= v0x7fd65d64e810_0 0.0))
                (= v0x7fd65d64ebd0_0 (and v0x7fd65d64e950_0 v0x7fd65d64ea90_0))
                (= v0x7fd65d64ef90_0 (= v0x7fd65d64d5d0_0 0.0))
                (= v0x7fd65d64f090_0
                   (ite v0x7fd65d64ef90_0 1.0 v0x7fd65d64b690_0))
                (= v0x7fd65d650390_0 (not (= v0x7fd65d64f510_0 0.0)))
                (= v0x7fd65d6504d0_0 (= v0x7fd65d64f5d0_0 0.0))
                (= v0x7fd65d650610_0 (or v0x7fd65d6504d0_0 v0x7fd65d650390_0)))))
  (=> F0x7fd65d651410 a!7))))
(assert (=> F0x7fd65d651410 F0x7fd65d651350))
(assert (=> F0x7fd65d651550 (or F0x7fd65d651290 F0x7fd65d651110)))
(assert (=> F0x7fd65d651510 F0x7fd65d651410))
(assert (=> pre!entry!0 (=> F0x7fd65d6511d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd65d651350 (>= v0x7fd65d64b410_0 0.0))))
(assert (let ((a!1 (=> F0x7fd65d651350
               (or (not (<= v0x7fd65d64b590_0 1.0)) (<= v0x7fd65d64b690_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2 (=> F0x7fd65d651350 (>= v0x7fd65d64b690_0 0.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fd65d651550 (>= v0x7fd65d64b650_0 0.0))))
(assert (let ((a!1 (=> F0x7fd65d651550
               (or (not (<= v0x7fd65d64b750_0 1.0)) (<= v0x7fd65d648010_0 0.0)))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!2 (=> F0x7fd65d651550 (>= v0x7fd65d648010_0 0.0))))
(assert (= lemma!bb1.i.i43.i.i!0 (=> F0x7fd65d651510 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i43.i.i!0) (not lemma!bb1.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
; (init: F0x7fd65d6511d0)
; (error: F0x7fd65d651510)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i43.i.i!0)
