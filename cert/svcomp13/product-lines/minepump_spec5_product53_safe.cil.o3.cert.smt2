(declare-fun post!bb2.i.i37.i.i!0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun F0x7fbbb2efad50 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun F0x7fbbb2efad90 () Bool)
(declare-fun F0x7fbbb2efa990 () Bool)
(declare-fun F0x7fbbb2efaa50 () Bool)
(declare-fun v0x7fbbb2ef9d10_0 () Bool)
(declare-fun v0x7fbbb2ef9bd0_0 () Bool)
(declare-fun v0x7fbbb2ef9a90_0 () Bool)
(declare-fun v0x7fbbb2ef9950_0 () Bool)
(declare-fun v0x7fbbb2ef6950_0 () Real)
(declare-fun v0x7fbbb2ef5890_0 () Real)
(declare-fun E0x7fbbb2ef9410 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7fbbb2ef84d0_0 () Real)
(declare-fun E0x7fbbb2ef9150 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fbbb2ef7dd0_0 () Real)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7fbbb2ef8d10_0 () Real)
(declare-fun E0x7fbbb2ef8e90 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fbbb2ef8310_0 () Real)
(declare-fun E0x7fbbb2ef7ad0 () Bool)
(declare-fun v0x7fbbb2ef7a10_0 () Bool)
(declare-fun E0x7fbbb2ef73d0 () Bool)
(declare-fun v0x7fbbb2ef7f10_0 () Bool)
(declare-fun v0x7fbbb2ef7150_0 () Real)
(declare-fun E0x7fbbb2ef7210 () Bool)
(declare-fun v0x7fbbb2ef5210_0 () Real)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun E0x7fbbb2ef7fd0 () Bool)
(declare-fun v0x7fbbb2ef6a10_0 () Bool)
(declare-fun v0x7fbbb2ef5f50_0 () Real)
(declare-fun E0x7fbbb2ef6c10 () Bool)
(declare-fun v0x7fbbb2ef4f90_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fbbb2ef6b50_0 () Bool)
(declare-fun lemma!bb2.i.i37.i.i!0 () Bool)
(declare-fun v0x7fbbb2ef7090_0 () Bool)
(declare-fun v0x7fbbb2ef5d90_0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun v0x7fbbb2ef5110_0 () Real)
(declare-fun E0x7fbbb2ef6510 () Bool)
(declare-fun v0x7fbbb2ef7c90_0 () Bool)
(declare-fun v0x7fbbb2ef8610_0 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun E0x7fbbb2ef6350 () Bool)
(declare-fun v0x7fbbb2ef61d0_0 () Bool)
(declare-fun v0x7fbbb2ef8dd0_0 () Real)
(declare-fun v0x7fbbb2ef6290_0 () Real)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun v0x7fbbb2ef5950_0 () Bool)
(declare-fun v0x7fbbb2ef8c50_0 () Bool)
(declare-fun v0x7fbbb2ef6e10_0 () Bool)
(declare-fun F0x7fbbb2efabd0 () Bool)
(declare-fun v0x7fbbb2ef2010_0 () Real)
(declare-fun E0x7fbbb2ef5b50 () Bool)
(declare-fun v0x7fbbb2ef6090_0 () Real)
(declare-fun v0x7fbbb2ef9e50_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fbbb2ef8890_0 () Bool)
(declare-fun v0x7fbbb2ef89d0_0 () Bool)
(declare-fun v0x7fbbb2ef5a90_0 () Bool)
(declare-fun v0x7fbbb2ef8750_0 () Bool)
(declare-fun v0x7fbbb2ef51d0_0 () Real)
(declare-fun F0x7fbbb2efab10 () Bool)
(declare-fun v0x7fbbb2ef57d0_0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun E0x7fbbb2ef8a90 () Bool)
(declare-fun v0x7fbbb2ef78d0_0 () Bool)
(declare-fun v0x7fbbb2ef52d0_0 () Real)
(declare-fun v0x7fbbb2ef2110_0 () Bool)
(declare-fun v0x7fbbb2ef81d0_0 () Bool)
(declare-fun v0x7fbbb2ef6f50_0 () Real)
(declare-fun F0x7fbbb2efac90 () Bool)

(assert (=> F0x7fbbb2efac90
    (and v0x7fbbb2ef2110_0
         (<= v0x7fbbb2ef51d0_0 0.0)
         (>= v0x7fbbb2ef51d0_0 0.0)
         (<= v0x7fbbb2ef52d0_0 1.0)
         (>= v0x7fbbb2ef52d0_0 1.0)
         (<= v0x7fbbb2ef2010_0 0.0)
         (>= v0x7fbbb2ef2010_0 0.0))))
(assert (=> F0x7fbbb2efac90 F0x7fbbb2efabd0))
(assert (let ((a!1 (=> v0x7fbbb2ef61d0_0
               (or (and v0x7fbbb2ef5a90_0
                        E0x7fbbb2ef6350
                        (<= v0x7fbbb2ef6290_0 v0x7fbbb2ef6090_0)
                        (>= v0x7fbbb2ef6290_0 v0x7fbbb2ef6090_0))
                   (and v0x7fbbb2ef57d0_0
                        E0x7fbbb2ef6510
                        v0x7fbbb2ef5950_0
                        (<= v0x7fbbb2ef6290_0 v0x7fbbb2ef5110_0)
                        (>= v0x7fbbb2ef6290_0 v0x7fbbb2ef5110_0)))))
      (a!2 (=> v0x7fbbb2ef61d0_0
               (or (and E0x7fbbb2ef6350 (not E0x7fbbb2ef6510))
                   (and E0x7fbbb2ef6510 (not E0x7fbbb2ef6350)))))
      (a!3 (=> v0x7fbbb2ef7090_0
               (or (and v0x7fbbb2ef6b50_0
                        E0x7fbbb2ef7210
                        (<= v0x7fbbb2ef7150_0 v0x7fbbb2ef6f50_0)
                        (>= v0x7fbbb2ef7150_0 v0x7fbbb2ef6f50_0))
                   (and v0x7fbbb2ef61d0_0
                        E0x7fbbb2ef73d0
                        v0x7fbbb2ef6a10_0
                        (<= v0x7fbbb2ef7150_0 v0x7fbbb2ef4f90_0)
                        (>= v0x7fbbb2ef7150_0 v0x7fbbb2ef4f90_0)))))
      (a!4 (=> v0x7fbbb2ef7090_0
               (or (and E0x7fbbb2ef7210 (not E0x7fbbb2ef73d0))
                   (and E0x7fbbb2ef73d0 (not E0x7fbbb2ef7210)))))
      (a!5 (or (and v0x7fbbb2ef7a10_0
                    E0x7fbbb2ef8e90
                    (<= v0x7fbbb2ef8d10_0 v0x7fbbb2ef6290_0)
                    (>= v0x7fbbb2ef8d10_0 v0x7fbbb2ef6290_0)
                    (<= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef7dd0_0)
                    (>= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef7dd0_0))
               (and v0x7fbbb2ef89d0_0
                    E0x7fbbb2ef9150
                    (and (<= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0)
                         (>= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0))
                    (<= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef5210_0)
                    (>= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef5210_0))
               (and v0x7fbbb2ef7f10_0
                    E0x7fbbb2ef9410
                    (not v0x7fbbb2ef8890_0)
                    (and (<= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0)
                         (>= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0))
                    (<= v0x7fbbb2ef8dd0_0 0.0)
                    (>= v0x7fbbb2ef8dd0_0 0.0))))
      (a!6 (=> v0x7fbbb2ef8c50_0
               (or (and E0x7fbbb2ef8e90
                        (not E0x7fbbb2ef9150)
                        (not E0x7fbbb2ef9410))
                   (and E0x7fbbb2ef9150
                        (not E0x7fbbb2ef8e90)
                        (not E0x7fbbb2ef9410))
                   (and E0x7fbbb2ef9410
                        (not E0x7fbbb2ef8e90)
                        (not E0x7fbbb2ef9150))))))
(let ((a!7 (and (=> v0x7fbbb2ef5a90_0
                    (and v0x7fbbb2ef57d0_0
                         E0x7fbbb2ef5b50
                         (not v0x7fbbb2ef5950_0)))
                (=> v0x7fbbb2ef5a90_0 E0x7fbbb2ef5b50)
                a!1
                a!2
                (=> v0x7fbbb2ef6b50_0
                    (and v0x7fbbb2ef61d0_0
                         E0x7fbbb2ef6c10
                         (not v0x7fbbb2ef6a10_0)))
                (=> v0x7fbbb2ef6b50_0 E0x7fbbb2ef6c10)
                a!3
                a!4
                (=> v0x7fbbb2ef7a10_0
                    (and v0x7fbbb2ef7090_0 E0x7fbbb2ef7ad0 v0x7fbbb2ef78d0_0))
                (=> v0x7fbbb2ef7a10_0 E0x7fbbb2ef7ad0)
                (=> v0x7fbbb2ef7f10_0
                    (and v0x7fbbb2ef7090_0
                         E0x7fbbb2ef7fd0
                         (not v0x7fbbb2ef78d0_0)))
                (=> v0x7fbbb2ef7f10_0 E0x7fbbb2ef7fd0)
                (=> v0x7fbbb2ef89d0_0
                    (and v0x7fbbb2ef7f10_0 E0x7fbbb2ef8a90 v0x7fbbb2ef8890_0))
                (=> v0x7fbbb2ef89d0_0 E0x7fbbb2ef8a90)
                (=> v0x7fbbb2ef8c50_0 a!5)
                a!6
                v0x7fbbb2ef8c50_0
                (not v0x7fbbb2ef9e50_0)
                (<= v0x7fbbb2ef51d0_0 v0x7fbbb2ef7150_0)
                (>= v0x7fbbb2ef51d0_0 v0x7fbbb2ef7150_0)
                (<= v0x7fbbb2ef52d0_0 v0x7fbbb2ef8d10_0)
                (>= v0x7fbbb2ef52d0_0 v0x7fbbb2ef8d10_0)
                (<= v0x7fbbb2ef2010_0 v0x7fbbb2ef8dd0_0)
                (>= v0x7fbbb2ef2010_0 v0x7fbbb2ef8dd0_0)
                (= v0x7fbbb2ef5950_0 (= v0x7fbbb2ef5890_0 0.0))
                (= v0x7fbbb2ef5d90_0 (< v0x7fbbb2ef5110_0 2.0))
                (= v0x7fbbb2ef5f50_0 (ite v0x7fbbb2ef5d90_0 1.0 0.0))
                (= v0x7fbbb2ef6090_0 (+ v0x7fbbb2ef5f50_0 v0x7fbbb2ef5110_0))
                (= v0x7fbbb2ef6a10_0 (= v0x7fbbb2ef6950_0 0.0))
                (= v0x7fbbb2ef6e10_0 (= v0x7fbbb2ef4f90_0 0.0))
                (= v0x7fbbb2ef6f50_0 (ite v0x7fbbb2ef6e10_0 1.0 0.0))
                (= v0x7fbbb2ef78d0_0 (= v0x7fbbb2ef5210_0 0.0))
                (= v0x7fbbb2ef7c90_0 (> v0x7fbbb2ef6290_0 1.0))
                (= v0x7fbbb2ef7dd0_0
                   (ite v0x7fbbb2ef7c90_0 1.0 v0x7fbbb2ef5210_0))
                (= v0x7fbbb2ef81d0_0 (> v0x7fbbb2ef6290_0 0.0))
                (= v0x7fbbb2ef8310_0 (+ v0x7fbbb2ef6290_0 (- 1.0)))
                (= v0x7fbbb2ef84d0_0
                   (ite v0x7fbbb2ef81d0_0 v0x7fbbb2ef8310_0 v0x7fbbb2ef6290_0))
                (= v0x7fbbb2ef8610_0 (= v0x7fbbb2ef7150_0 0.0))
                (= v0x7fbbb2ef8750_0 (= v0x7fbbb2ef84d0_0 0.0))
                (= v0x7fbbb2ef8890_0 (and v0x7fbbb2ef8610_0 v0x7fbbb2ef8750_0))
                (= v0x7fbbb2ef9950_0 (= v0x7fbbb2ef8d10_0 2.0))
                (= v0x7fbbb2ef9a90_0 (= v0x7fbbb2ef8dd0_0 0.0))
                (= v0x7fbbb2ef9bd0_0 (or v0x7fbbb2ef9a90_0 v0x7fbbb2ef9950_0))
                (= v0x7fbbb2ef9d10_0 (xor v0x7fbbb2ef9bd0_0 true))
                (= v0x7fbbb2ef9e50_0 (and v0x7fbbb2ef78d0_0 v0x7fbbb2ef9d10_0)))))
  (=> F0x7fbbb2efab10 a!7))))
(assert (=> F0x7fbbb2efab10 F0x7fbbb2efaa50))
(assert (let ((a!1 (=> v0x7fbbb2ef61d0_0
               (or (and v0x7fbbb2ef5a90_0
                        E0x7fbbb2ef6350
                        (<= v0x7fbbb2ef6290_0 v0x7fbbb2ef6090_0)
                        (>= v0x7fbbb2ef6290_0 v0x7fbbb2ef6090_0))
                   (and v0x7fbbb2ef57d0_0
                        E0x7fbbb2ef6510
                        v0x7fbbb2ef5950_0
                        (<= v0x7fbbb2ef6290_0 v0x7fbbb2ef5110_0)
                        (>= v0x7fbbb2ef6290_0 v0x7fbbb2ef5110_0)))))
      (a!2 (=> v0x7fbbb2ef61d0_0
               (or (and E0x7fbbb2ef6350 (not E0x7fbbb2ef6510))
                   (and E0x7fbbb2ef6510 (not E0x7fbbb2ef6350)))))
      (a!3 (=> v0x7fbbb2ef7090_0
               (or (and v0x7fbbb2ef6b50_0
                        E0x7fbbb2ef7210
                        (<= v0x7fbbb2ef7150_0 v0x7fbbb2ef6f50_0)
                        (>= v0x7fbbb2ef7150_0 v0x7fbbb2ef6f50_0))
                   (and v0x7fbbb2ef61d0_0
                        E0x7fbbb2ef73d0
                        v0x7fbbb2ef6a10_0
                        (<= v0x7fbbb2ef7150_0 v0x7fbbb2ef4f90_0)
                        (>= v0x7fbbb2ef7150_0 v0x7fbbb2ef4f90_0)))))
      (a!4 (=> v0x7fbbb2ef7090_0
               (or (and E0x7fbbb2ef7210 (not E0x7fbbb2ef73d0))
                   (and E0x7fbbb2ef73d0 (not E0x7fbbb2ef7210)))))
      (a!5 (or (and v0x7fbbb2ef7a10_0
                    E0x7fbbb2ef8e90
                    (<= v0x7fbbb2ef8d10_0 v0x7fbbb2ef6290_0)
                    (>= v0x7fbbb2ef8d10_0 v0x7fbbb2ef6290_0)
                    (<= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef7dd0_0)
                    (>= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef7dd0_0))
               (and v0x7fbbb2ef89d0_0
                    E0x7fbbb2ef9150
                    (and (<= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0)
                         (>= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0))
                    (<= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef5210_0)
                    (>= v0x7fbbb2ef8dd0_0 v0x7fbbb2ef5210_0))
               (and v0x7fbbb2ef7f10_0
                    E0x7fbbb2ef9410
                    (not v0x7fbbb2ef8890_0)
                    (and (<= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0)
                         (>= v0x7fbbb2ef8d10_0 v0x7fbbb2ef84d0_0))
                    (<= v0x7fbbb2ef8dd0_0 0.0)
                    (>= v0x7fbbb2ef8dd0_0 0.0))))
      (a!6 (=> v0x7fbbb2ef8c50_0
               (or (and E0x7fbbb2ef8e90
                        (not E0x7fbbb2ef9150)
                        (not E0x7fbbb2ef9410))
                   (and E0x7fbbb2ef9150
                        (not E0x7fbbb2ef8e90)
                        (not E0x7fbbb2ef9410))
                   (and E0x7fbbb2ef9410
                        (not E0x7fbbb2ef8e90)
                        (not E0x7fbbb2ef9150))))))
(let ((a!7 (and (=> v0x7fbbb2ef5a90_0
                    (and v0x7fbbb2ef57d0_0
                         E0x7fbbb2ef5b50
                         (not v0x7fbbb2ef5950_0)))
                (=> v0x7fbbb2ef5a90_0 E0x7fbbb2ef5b50)
                a!1
                a!2
                (=> v0x7fbbb2ef6b50_0
                    (and v0x7fbbb2ef61d0_0
                         E0x7fbbb2ef6c10
                         (not v0x7fbbb2ef6a10_0)))
                (=> v0x7fbbb2ef6b50_0 E0x7fbbb2ef6c10)
                a!3
                a!4
                (=> v0x7fbbb2ef7a10_0
                    (and v0x7fbbb2ef7090_0 E0x7fbbb2ef7ad0 v0x7fbbb2ef78d0_0))
                (=> v0x7fbbb2ef7a10_0 E0x7fbbb2ef7ad0)
                (=> v0x7fbbb2ef7f10_0
                    (and v0x7fbbb2ef7090_0
                         E0x7fbbb2ef7fd0
                         (not v0x7fbbb2ef78d0_0)))
                (=> v0x7fbbb2ef7f10_0 E0x7fbbb2ef7fd0)
                (=> v0x7fbbb2ef89d0_0
                    (and v0x7fbbb2ef7f10_0 E0x7fbbb2ef8a90 v0x7fbbb2ef8890_0))
                (=> v0x7fbbb2ef89d0_0 E0x7fbbb2ef8a90)
                (=> v0x7fbbb2ef8c50_0 a!5)
                a!6
                v0x7fbbb2ef8c50_0
                v0x7fbbb2ef9e50_0
                (= v0x7fbbb2ef5950_0 (= v0x7fbbb2ef5890_0 0.0))
                (= v0x7fbbb2ef5d90_0 (< v0x7fbbb2ef5110_0 2.0))
                (= v0x7fbbb2ef5f50_0 (ite v0x7fbbb2ef5d90_0 1.0 0.0))
                (= v0x7fbbb2ef6090_0 (+ v0x7fbbb2ef5f50_0 v0x7fbbb2ef5110_0))
                (= v0x7fbbb2ef6a10_0 (= v0x7fbbb2ef6950_0 0.0))
                (= v0x7fbbb2ef6e10_0 (= v0x7fbbb2ef4f90_0 0.0))
                (= v0x7fbbb2ef6f50_0 (ite v0x7fbbb2ef6e10_0 1.0 0.0))
                (= v0x7fbbb2ef78d0_0 (= v0x7fbbb2ef5210_0 0.0))
                (= v0x7fbbb2ef7c90_0 (> v0x7fbbb2ef6290_0 1.0))
                (= v0x7fbbb2ef7dd0_0
                   (ite v0x7fbbb2ef7c90_0 1.0 v0x7fbbb2ef5210_0))
                (= v0x7fbbb2ef81d0_0 (> v0x7fbbb2ef6290_0 0.0))
                (= v0x7fbbb2ef8310_0 (+ v0x7fbbb2ef6290_0 (- 1.0)))
                (= v0x7fbbb2ef84d0_0
                   (ite v0x7fbbb2ef81d0_0 v0x7fbbb2ef8310_0 v0x7fbbb2ef6290_0))
                (= v0x7fbbb2ef8610_0 (= v0x7fbbb2ef7150_0 0.0))
                (= v0x7fbbb2ef8750_0 (= v0x7fbbb2ef84d0_0 0.0))
                (= v0x7fbbb2ef8890_0 (and v0x7fbbb2ef8610_0 v0x7fbbb2ef8750_0))
                (= v0x7fbbb2ef9950_0 (= v0x7fbbb2ef8d10_0 2.0))
                (= v0x7fbbb2ef9a90_0 (= v0x7fbbb2ef8dd0_0 0.0))
                (= v0x7fbbb2ef9bd0_0 (or v0x7fbbb2ef9a90_0 v0x7fbbb2ef9950_0))
                (= v0x7fbbb2ef9d10_0 (xor v0x7fbbb2ef9bd0_0 true))
                (= v0x7fbbb2ef9e50_0 (and v0x7fbbb2ef78d0_0 v0x7fbbb2ef9d10_0)))))
  (=> F0x7fbbb2efa990 a!7))))
(assert (=> F0x7fbbb2efa990 F0x7fbbb2efaa50))
(assert (=> F0x7fbbb2efad90 (or F0x7fbbb2efac90 F0x7fbbb2efab10)))
(assert (=> F0x7fbbb2efad50 F0x7fbbb2efa990))
(assert (=> pre!entry!0 (=> F0x7fbbb2efabd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fbbb2efaa50 (>= v0x7fbbb2ef4f90_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fbbb2efaa50
        (or (>= v0x7fbbb2ef5110_0 2.0) (<= v0x7fbbb2ef5210_0 0.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fbbb2efaa50 (>= v0x7fbbb2ef5110_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fbbb2efaa50
        (or (<= v0x7fbbb2ef5110_0 1.0) (>= v0x7fbbb2ef5210_0 1.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7fbbb2efaa50 (<= v0x7fbbb2ef5110_0 2.0))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7fbbb2efaa50
        (or (>= v0x7fbbb2ef5110_0 1.0) (<= v0x7fbbb2ef5110_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fbbb2efad90 (>= v0x7fbbb2ef51d0_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fbbb2efad90
       (or (>= v0x7fbbb2ef52d0_0 2.0) (<= v0x7fbbb2ef2010_0 0.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fbbb2efad90 (>= v0x7fbbb2ef52d0_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7fbbb2efad90
       (or (<= v0x7fbbb2ef52d0_0 1.0) (>= v0x7fbbb2ef2010_0 1.0)))))
(assert (= lemma!bb1.i.i!4 (=> F0x7fbbb2efad90 (<= v0x7fbbb2ef52d0_0 2.0))))
(assert (= lemma!bb1.i.i!5
   (=> F0x7fbbb2efad90
       (or (>= v0x7fbbb2ef52d0_0 1.0) (<= v0x7fbbb2ef52d0_0 0.0)))))
(assert (= lemma!bb2.i.i37.i.i!0 (=> F0x7fbbb2efad50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb2.i.i37.i.i!0) (not lemma!bb2.i.i37.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
; (init: F0x7fbbb2efabd0)
; (error: F0x7fbbb2efad50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i37.i.i!0)
