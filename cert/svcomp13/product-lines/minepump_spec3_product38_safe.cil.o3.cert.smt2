(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fabba9255d0 () Bool)
(declare-fun v0x7fabba921910_0 () Bool)
(declare-fun F0x7fabba9253d0 () Bool)
(declare-fun v0x7fabba921450_0 () Real)
(declare-fun v0x7fabba920390_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fabba924690_0 () Bool)
(declare-fun v0x7fabba922fd0_0 () Real)
(declare-fun E0x7fabba9239d0 () Bool)
(declare-fun v0x7fabba9228d0_0 () Real)
(declare-fun v0x7fabba922cd0_0 () Bool)
(declare-fun v0x7fabba923650_0 () Real)
(declare-fun v0x7fabba923590_0 () Real)
(declare-fun v0x7fabba9234d0_0 () Bool)
(declare-fun E0x7fabba923310 () Bool)
(declare-fun v0x7fabba923110_0 () Bool)
(declare-fun v0x7fabba923250_0 () Bool)
(declare-fun E0x7fabba922ad0 () Bool)
(declare-fun v0x7fabba922e10_0 () Real)
(declare-fun v0x7fabba9223d0_0 () Bool)
(declare-fun E0x7fabba923c90 () Bool)
(declare-fun v0x7fabba924550_0 () Bool)
(declare-fun E0x7fabba9225d0 () Bool)
(declare-fun v0x7fabba922510_0 () Bool)
(declare-fun F0x7fabba925490 () Bool)
(declare-fun v0x7fabba921a50_0 () Real)
(declare-fun v0x7fabba922a10_0 () Bool)
(declare-fun v0x7fabba924410_0 () Bool)
(declare-fun F0x7fabba925590 () Bool)
(declare-fun E0x7fabba921d10 () Bool)
(declare-fun v0x7fabba921b90_0 () Bool)
(declare-fun v0x7fabba91fc10_0 () Real)
(declare-fun E0x7fabba920650 () Bool)
(declare-fun v0x7fabba922790_0 () Bool)
(declare-fun v0x7fabba9241d0_0 () Bool)
(declare-fun v0x7fabba920b90_0 () Real)
(declare-fun E0x7fabba923710 () Bool)
(declare-fun v0x7fabba9202d0_0 () Bool)
(declare-fun v0x7fabba920cd0_0 () Bool)
(declare-fun v0x7fabba91fa90_0 () Real)
(declare-fun v0x7fabba920590_0 () Bool)
(declare-fun v0x7fabba920a50_0 () Real)
(declare-fun v0x7fabba920450_0 () Bool)
(declare-fun E0x7fabba920e50 () Bool)
(declare-fun F0x7fabba925310 () Bool)
(declare-fun v0x7fabba91fd10_0 () Real)
(declare-fun v0x7fabba91fdd0_0 () Real)
(declare-fun E0x7fabba921010 () Bool)
(declare-fun v0x7fabba921510_0 () Bool)
(declare-fun v0x7fabba91fcd0_0 () Real)
(declare-fun v0x7fabba921c50_0 () Real)
(declare-fun E0x7fabba921710 () Bool)
(declare-fun v0x7fabba9242d0_0 () Bool)
(declare-fun v0x7fabba921650_0 () Bool)
(declare-fun v0x7fabba920890_0 () Bool)
(declare-fun v0x7fabba920d90_0 () Real)
(declare-fun v0x7fabba91e110_0 () Bool)
(declare-fun v0x7fabba91e010_0 () Real)
(declare-fun F0x7fabba925250 () Bool)
(declare-fun E0x7fabba921ed0 () Bool)
(declare-fun F0x7fabba925190 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)

(assert (=> F0x7fabba925190
    (and v0x7fabba91e110_0
         (<= v0x7fabba91fcd0_0 0.0)
         (>= v0x7fabba91fcd0_0 0.0)
         (<= v0x7fabba91fdd0_0 1.0)
         (>= v0x7fabba91fdd0_0 1.0)
         (<= v0x7fabba91e010_0 0.0)
         (>= v0x7fabba91e010_0 0.0))))
(assert (=> F0x7fabba925190 F0x7fabba925250))
(assert (let ((a!1 (=> v0x7fabba920cd0_0
               (or (and v0x7fabba920590_0
                        E0x7fabba920e50
                        (<= v0x7fabba920d90_0 v0x7fabba920b90_0)
                        (>= v0x7fabba920d90_0 v0x7fabba920b90_0))
                   (and v0x7fabba9202d0_0
                        E0x7fabba921010
                        v0x7fabba920450_0
                        (<= v0x7fabba920d90_0 v0x7fabba91fc10_0)
                        (>= v0x7fabba920d90_0 v0x7fabba91fc10_0)))))
      (a!2 (=> v0x7fabba920cd0_0
               (or (and E0x7fabba920e50 (not E0x7fabba921010))
                   (and E0x7fabba921010 (not E0x7fabba920e50)))))
      (a!3 (=> v0x7fabba921b90_0
               (or (and v0x7fabba921650_0
                        E0x7fabba921d10
                        (<= v0x7fabba921c50_0 v0x7fabba921a50_0)
                        (>= v0x7fabba921c50_0 v0x7fabba921a50_0))
                   (and v0x7fabba920cd0_0
                        E0x7fabba921ed0
                        v0x7fabba921510_0
                        (<= v0x7fabba921c50_0 v0x7fabba91fa90_0)
                        (>= v0x7fabba921c50_0 v0x7fabba91fa90_0)))))
      (a!4 (=> v0x7fabba921b90_0
               (or (and E0x7fabba921d10 (not E0x7fabba921ed0))
                   (and E0x7fabba921ed0 (not E0x7fabba921d10)))))
      (a!5 (or (and v0x7fabba922510_0
                    E0x7fabba923710
                    (<= v0x7fabba923590_0 v0x7fabba920d90_0)
                    (>= v0x7fabba923590_0 v0x7fabba920d90_0)
                    (<= v0x7fabba923650_0 v0x7fabba9228d0_0)
                    (>= v0x7fabba923650_0 v0x7fabba9228d0_0))
               (and v0x7fabba923250_0
                    E0x7fabba9239d0
                    (and (<= v0x7fabba923590_0 v0x7fabba922fd0_0)
                         (>= v0x7fabba923590_0 v0x7fabba922fd0_0))
                    (<= v0x7fabba923650_0 v0x7fabba91fd10_0)
                    (>= v0x7fabba923650_0 v0x7fabba91fd10_0))
               (and v0x7fabba922a10_0
                    E0x7fabba923c90
                    (not v0x7fabba923110_0)
                    (and (<= v0x7fabba923590_0 v0x7fabba922fd0_0)
                         (>= v0x7fabba923590_0 v0x7fabba922fd0_0))
                    (<= v0x7fabba923650_0 0.0)
                    (>= v0x7fabba923650_0 0.0))))
      (a!6 (=> v0x7fabba9234d0_0
               (or (and E0x7fabba923710
                        (not E0x7fabba9239d0)
                        (not E0x7fabba923c90))
                   (and E0x7fabba9239d0
                        (not E0x7fabba923710)
                        (not E0x7fabba923c90))
                   (and E0x7fabba923c90
                        (not E0x7fabba923710)
                        (not E0x7fabba9239d0))))))
(let ((a!7 (and (=> v0x7fabba920590_0
                    (and v0x7fabba9202d0_0
                         E0x7fabba920650
                         (not v0x7fabba920450_0)))
                (=> v0x7fabba920590_0 E0x7fabba920650)
                a!1
                a!2
                (=> v0x7fabba921650_0
                    (and v0x7fabba920cd0_0
                         E0x7fabba921710
                         (not v0x7fabba921510_0)))
                (=> v0x7fabba921650_0 E0x7fabba921710)
                a!3
                a!4
                (=> v0x7fabba922510_0
                    (and v0x7fabba921b90_0 E0x7fabba9225d0 v0x7fabba9223d0_0))
                (=> v0x7fabba922510_0 E0x7fabba9225d0)
                (=> v0x7fabba922a10_0
                    (and v0x7fabba921b90_0
                         E0x7fabba922ad0
                         (not v0x7fabba9223d0_0)))
                (=> v0x7fabba922a10_0 E0x7fabba922ad0)
                (=> v0x7fabba923250_0
                    (and v0x7fabba922a10_0 E0x7fabba923310 v0x7fabba923110_0))
                (=> v0x7fabba923250_0 E0x7fabba923310)
                (=> v0x7fabba9234d0_0 a!5)
                a!6
                v0x7fabba9234d0_0
                (not v0x7fabba924690_0)
                (<= v0x7fabba91fcd0_0 v0x7fabba921c50_0)
                (>= v0x7fabba91fcd0_0 v0x7fabba921c50_0)
                (<= v0x7fabba91fdd0_0 v0x7fabba923590_0)
                (>= v0x7fabba91fdd0_0 v0x7fabba923590_0)
                (<= v0x7fabba91e010_0 v0x7fabba923650_0)
                (>= v0x7fabba91e010_0 v0x7fabba923650_0)
                (= v0x7fabba920450_0 (= v0x7fabba920390_0 0.0))
                (= v0x7fabba920890_0 (< v0x7fabba91fc10_0 2.0))
                (= v0x7fabba920a50_0 (ite v0x7fabba920890_0 1.0 0.0))
                (= v0x7fabba920b90_0 (+ v0x7fabba920a50_0 v0x7fabba91fc10_0))
                (= v0x7fabba921510_0 (= v0x7fabba921450_0 0.0))
                (= v0x7fabba921910_0 (= v0x7fabba91fa90_0 0.0))
                (= v0x7fabba921a50_0 (ite v0x7fabba921910_0 1.0 0.0))
                (= v0x7fabba9223d0_0 (= v0x7fabba91fd10_0 0.0))
                (= v0x7fabba922790_0 (> v0x7fabba920d90_0 1.0))
                (= v0x7fabba9228d0_0
                   (ite v0x7fabba922790_0 1.0 v0x7fabba91fd10_0))
                (= v0x7fabba922cd0_0 (> v0x7fabba920d90_0 0.0))
                (= v0x7fabba922e10_0 (+ v0x7fabba920d90_0 (- 1.0)))
                (= v0x7fabba922fd0_0
                   (ite v0x7fabba922cd0_0 v0x7fabba922e10_0 v0x7fabba920d90_0))
                (= v0x7fabba923110_0 (= v0x7fabba921c50_0 0.0))
                (= v0x7fabba9241d0_0 (= v0x7fabba921c50_0 0.0))
                (= v0x7fabba9242d0_0 (= v0x7fabba923590_0 2.0))
                (= v0x7fabba924410_0 (= v0x7fabba923650_0 0.0))
                (= v0x7fabba924550_0 (and v0x7fabba9242d0_0 v0x7fabba9241d0_0))
                (= v0x7fabba924690_0 (and v0x7fabba924550_0 v0x7fabba924410_0)))))
  (=> F0x7fabba925310 a!7))))
(assert (=> F0x7fabba925310 F0x7fabba9253d0))
(assert (let ((a!1 (=> v0x7fabba920cd0_0
               (or (and v0x7fabba920590_0
                        E0x7fabba920e50
                        (<= v0x7fabba920d90_0 v0x7fabba920b90_0)
                        (>= v0x7fabba920d90_0 v0x7fabba920b90_0))
                   (and v0x7fabba9202d0_0
                        E0x7fabba921010
                        v0x7fabba920450_0
                        (<= v0x7fabba920d90_0 v0x7fabba91fc10_0)
                        (>= v0x7fabba920d90_0 v0x7fabba91fc10_0)))))
      (a!2 (=> v0x7fabba920cd0_0
               (or (and E0x7fabba920e50 (not E0x7fabba921010))
                   (and E0x7fabba921010 (not E0x7fabba920e50)))))
      (a!3 (=> v0x7fabba921b90_0
               (or (and v0x7fabba921650_0
                        E0x7fabba921d10
                        (<= v0x7fabba921c50_0 v0x7fabba921a50_0)
                        (>= v0x7fabba921c50_0 v0x7fabba921a50_0))
                   (and v0x7fabba920cd0_0
                        E0x7fabba921ed0
                        v0x7fabba921510_0
                        (<= v0x7fabba921c50_0 v0x7fabba91fa90_0)
                        (>= v0x7fabba921c50_0 v0x7fabba91fa90_0)))))
      (a!4 (=> v0x7fabba921b90_0
               (or (and E0x7fabba921d10 (not E0x7fabba921ed0))
                   (and E0x7fabba921ed0 (not E0x7fabba921d10)))))
      (a!5 (or (and v0x7fabba922510_0
                    E0x7fabba923710
                    (<= v0x7fabba923590_0 v0x7fabba920d90_0)
                    (>= v0x7fabba923590_0 v0x7fabba920d90_0)
                    (<= v0x7fabba923650_0 v0x7fabba9228d0_0)
                    (>= v0x7fabba923650_0 v0x7fabba9228d0_0))
               (and v0x7fabba923250_0
                    E0x7fabba9239d0
                    (and (<= v0x7fabba923590_0 v0x7fabba922fd0_0)
                         (>= v0x7fabba923590_0 v0x7fabba922fd0_0))
                    (<= v0x7fabba923650_0 v0x7fabba91fd10_0)
                    (>= v0x7fabba923650_0 v0x7fabba91fd10_0))
               (and v0x7fabba922a10_0
                    E0x7fabba923c90
                    (not v0x7fabba923110_0)
                    (and (<= v0x7fabba923590_0 v0x7fabba922fd0_0)
                         (>= v0x7fabba923590_0 v0x7fabba922fd0_0))
                    (<= v0x7fabba923650_0 0.0)
                    (>= v0x7fabba923650_0 0.0))))
      (a!6 (=> v0x7fabba9234d0_0
               (or (and E0x7fabba923710
                        (not E0x7fabba9239d0)
                        (not E0x7fabba923c90))
                   (and E0x7fabba9239d0
                        (not E0x7fabba923710)
                        (not E0x7fabba923c90))
                   (and E0x7fabba923c90
                        (not E0x7fabba923710)
                        (not E0x7fabba9239d0))))))
(let ((a!7 (and (=> v0x7fabba920590_0
                    (and v0x7fabba9202d0_0
                         E0x7fabba920650
                         (not v0x7fabba920450_0)))
                (=> v0x7fabba920590_0 E0x7fabba920650)
                a!1
                a!2
                (=> v0x7fabba921650_0
                    (and v0x7fabba920cd0_0
                         E0x7fabba921710
                         (not v0x7fabba921510_0)))
                (=> v0x7fabba921650_0 E0x7fabba921710)
                a!3
                a!4
                (=> v0x7fabba922510_0
                    (and v0x7fabba921b90_0 E0x7fabba9225d0 v0x7fabba9223d0_0))
                (=> v0x7fabba922510_0 E0x7fabba9225d0)
                (=> v0x7fabba922a10_0
                    (and v0x7fabba921b90_0
                         E0x7fabba922ad0
                         (not v0x7fabba9223d0_0)))
                (=> v0x7fabba922a10_0 E0x7fabba922ad0)
                (=> v0x7fabba923250_0
                    (and v0x7fabba922a10_0 E0x7fabba923310 v0x7fabba923110_0))
                (=> v0x7fabba923250_0 E0x7fabba923310)
                (=> v0x7fabba9234d0_0 a!5)
                a!6
                v0x7fabba9234d0_0
                v0x7fabba924690_0
                (= v0x7fabba920450_0 (= v0x7fabba920390_0 0.0))
                (= v0x7fabba920890_0 (< v0x7fabba91fc10_0 2.0))
                (= v0x7fabba920a50_0 (ite v0x7fabba920890_0 1.0 0.0))
                (= v0x7fabba920b90_0 (+ v0x7fabba920a50_0 v0x7fabba91fc10_0))
                (= v0x7fabba921510_0 (= v0x7fabba921450_0 0.0))
                (= v0x7fabba921910_0 (= v0x7fabba91fa90_0 0.0))
                (= v0x7fabba921a50_0 (ite v0x7fabba921910_0 1.0 0.0))
                (= v0x7fabba9223d0_0 (= v0x7fabba91fd10_0 0.0))
                (= v0x7fabba922790_0 (> v0x7fabba920d90_0 1.0))
                (= v0x7fabba9228d0_0
                   (ite v0x7fabba922790_0 1.0 v0x7fabba91fd10_0))
                (= v0x7fabba922cd0_0 (> v0x7fabba920d90_0 0.0))
                (= v0x7fabba922e10_0 (+ v0x7fabba920d90_0 (- 1.0)))
                (= v0x7fabba922fd0_0
                   (ite v0x7fabba922cd0_0 v0x7fabba922e10_0 v0x7fabba920d90_0))
                (= v0x7fabba923110_0 (= v0x7fabba921c50_0 0.0))
                (= v0x7fabba9241d0_0 (= v0x7fabba921c50_0 0.0))
                (= v0x7fabba9242d0_0 (= v0x7fabba923590_0 2.0))
                (= v0x7fabba924410_0 (= v0x7fabba923650_0 0.0))
                (= v0x7fabba924550_0 (and v0x7fabba9242d0_0 v0x7fabba9241d0_0))
                (= v0x7fabba924690_0 (and v0x7fabba924550_0 v0x7fabba924410_0)))))
  (=> F0x7fabba925490 a!7))))
(assert (=> F0x7fabba925490 F0x7fabba9253d0))
(assert (=> F0x7fabba9255d0 (or F0x7fabba925190 F0x7fabba925310)))
(assert (=> F0x7fabba925590 F0x7fabba925490))
(assert (=> pre!entry!0 (=> F0x7fabba925250 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fabba9253d0 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fabba9255d0 true)))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7fabba925590 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7fabba925250)
; (error: F0x7fabba925590)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i23.i.i!0)
