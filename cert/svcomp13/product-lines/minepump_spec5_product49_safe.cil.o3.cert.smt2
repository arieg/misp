(declare-fun lemma!bb2.i.i28.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7feb366be610 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7feb366be650 () Bool)
(declare-fun F0x7feb366be290 () Bool)
(declare-fun F0x7feb366be350 () Bool)
(declare-fun v0x7feb366bd6d0_0 () Bool)
(declare-fun v0x7feb366bd590_0 () Bool)
(declare-fun v0x7feb366bbe10_0 () Bool)
(declare-fun v0x7feb366ba7d0_0 () Bool)
(declare-fun v0x7feb366ba2d0_0 () Real)
(declare-fun v0x7feb366bc110_0 () Real)
(declare-fun v0x7feb366bba10_0 () Real)
(declare-fun E0x7feb366bc850 () Bool)
(declare-fun E0x7feb366bc450 () Bool)
(declare-fun v0x7feb366b9d90_0 () Real)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7feb366bbc10 () Bool)
(declare-fun v0x7feb366bc610_0 () Bool)
(declare-fun v0x7feb366bc390_0 () Bool)
(declare-fun v0x7feb366bc250_0 () Bool)
(declare-fun v0x7feb366bbf50_0 () Real)
(declare-fun v0x7feb366bbb50_0 () Bool)
(declare-fun v0x7feb366bb510_0 () Bool)
(declare-fun E0x7feb366bcb10 () Bool)
(declare-fun v0x7feb366bb650_0 () Bool)
(declare-fun v0x7feb366ba990_0 () Real)
(declare-fun v0x7feb366b9c10_0 () Real)
(declare-fun v0x7feb366bac10_0 () Bool)
(declare-fun v0x7feb366bb8d0_0 () Bool)
(declare-fun v0x7feb366ba390_0 () Bool)
(declare-fun E0x7feb366ba590 () Bool)
(declare-fun v0x7feb366ba210_0 () Bool)
(declare-fun v0x7feb366ba4d0_0 () Bool)
(declare-fun E0x7feb366bb710 () Bool)
(declare-fun F0x7feb366be410 () Bool)
(declare-fun v0x7feb366bc6d0_0 () Real)
(declare-fun F0x7feb366be4d0 () Bool)
(declare-fun post!bb2.i.i28.i.i!0 () Bool)
(declare-fun v0x7feb366b9e50_0 () Real)
(declare-fun v0x7feb366baad0_0 () Real)
(declare-fun v0x7feb366b8110_0 () Bool)
(declare-fun E0x7feb366bcdd0 () Bool)
(declare-fun E0x7feb366baf50 () Bool)
(declare-fun v0x7feb366b8010_0 () Real)
(declare-fun v0x7feb366bd450_0 () Bool)
(declare-fun v0x7feb366bd310_0 () Bool)
(declare-fun v0x7feb366bacd0_0 () Real)
(declare-fun F0x7feb366be590 () Bool)
(declare-fun v0x7feb366bd810_0 () Bool)
(declare-fun v0x7feb366bc790_0 () Real)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7feb366bad90 () Bool)

(assert (=> F0x7feb366be590
    (and v0x7feb366b8110_0
         (<= v0x7feb366b9e50_0 1.0)
         (>= v0x7feb366b9e50_0 1.0)
         (<= v0x7feb366b8010_0 0.0)
         (>= v0x7feb366b8010_0 0.0))))
(assert (=> F0x7feb366be590 F0x7feb366be4d0))
(assert (let ((a!1 (=> v0x7feb366bac10_0
               (or (and v0x7feb366ba4d0_0
                        E0x7feb366bad90
                        (<= v0x7feb366bacd0_0 v0x7feb366baad0_0)
                        (>= v0x7feb366bacd0_0 v0x7feb366baad0_0))
                   (and v0x7feb366ba210_0
                        E0x7feb366baf50
                        v0x7feb366ba390_0
                        (<= v0x7feb366bacd0_0 v0x7feb366b9c10_0)
                        (>= v0x7feb366bacd0_0 v0x7feb366b9c10_0)))))
      (a!2 (=> v0x7feb366bac10_0
               (or (and E0x7feb366bad90 (not E0x7feb366baf50))
                   (and E0x7feb366baf50 (not E0x7feb366bad90)))))
      (a!3 (or (and v0x7feb366bb650_0
                    E0x7feb366bc850
                    (<= v0x7feb366bc6d0_0 v0x7feb366bacd0_0)
                    (>= v0x7feb366bc6d0_0 v0x7feb366bacd0_0)
                    (<= v0x7feb366bc790_0 v0x7feb366bba10_0)
                    (>= v0x7feb366bc790_0 v0x7feb366bba10_0))
               (and v0x7feb366bc390_0
                    E0x7feb366bcb10
                    (and (<= v0x7feb366bc6d0_0 v0x7feb366bc110_0)
                         (>= v0x7feb366bc6d0_0 v0x7feb366bc110_0))
                    (<= v0x7feb366bc790_0 v0x7feb366b9d90_0)
                    (>= v0x7feb366bc790_0 v0x7feb366b9d90_0))
               (and v0x7feb366bbb50_0
                    E0x7feb366bcdd0
                    (not v0x7feb366bc250_0)
                    (and (<= v0x7feb366bc6d0_0 v0x7feb366bc110_0)
                         (>= v0x7feb366bc6d0_0 v0x7feb366bc110_0))
                    (<= v0x7feb366bc790_0 0.0)
                    (>= v0x7feb366bc790_0 0.0))))
      (a!4 (=> v0x7feb366bc610_0
               (or (and E0x7feb366bc850
                        (not E0x7feb366bcb10)
                        (not E0x7feb366bcdd0))
                   (and E0x7feb366bcb10
                        (not E0x7feb366bc850)
                        (not E0x7feb366bcdd0))
                   (and E0x7feb366bcdd0
                        (not E0x7feb366bc850)
                        (not E0x7feb366bcb10))))))
(let ((a!5 (and (=> v0x7feb366ba4d0_0
                    (and v0x7feb366ba210_0
                         E0x7feb366ba590
                         (not v0x7feb366ba390_0)))
                (=> v0x7feb366ba4d0_0 E0x7feb366ba590)
                a!1
                a!2
                (=> v0x7feb366bb650_0
                    (and v0x7feb366bac10_0 E0x7feb366bb710 v0x7feb366bb510_0))
                (=> v0x7feb366bb650_0 E0x7feb366bb710)
                (=> v0x7feb366bbb50_0
                    (and v0x7feb366bac10_0
                         E0x7feb366bbc10
                         (not v0x7feb366bb510_0)))
                (=> v0x7feb366bbb50_0 E0x7feb366bbc10)
                (=> v0x7feb366bc390_0
                    (and v0x7feb366bbb50_0 E0x7feb366bc450 v0x7feb366bc250_0))
                (=> v0x7feb366bc390_0 E0x7feb366bc450)
                (=> v0x7feb366bc610_0 a!3)
                a!4
                v0x7feb366bc610_0
                (not v0x7feb366bd810_0)
                (<= v0x7feb366b9e50_0 v0x7feb366bc6d0_0)
                (>= v0x7feb366b9e50_0 v0x7feb366bc6d0_0)
                (<= v0x7feb366b8010_0 v0x7feb366bc790_0)
                (>= v0x7feb366b8010_0 v0x7feb366bc790_0)
                (= v0x7feb366ba390_0 (= v0x7feb366ba2d0_0 0.0))
                (= v0x7feb366ba7d0_0 (< v0x7feb366b9c10_0 2.0))
                (= v0x7feb366ba990_0 (ite v0x7feb366ba7d0_0 1.0 0.0))
                (= v0x7feb366baad0_0 (+ v0x7feb366ba990_0 v0x7feb366b9c10_0))
                (= v0x7feb366bb510_0 (= v0x7feb366b9d90_0 0.0))
                (= v0x7feb366bb8d0_0 (> v0x7feb366bacd0_0 1.0))
                (= v0x7feb366bba10_0
                   (ite v0x7feb366bb8d0_0 1.0 v0x7feb366b9d90_0))
                (= v0x7feb366bbe10_0 (> v0x7feb366bacd0_0 0.0))
                (= v0x7feb366bbf50_0 (+ v0x7feb366bacd0_0 (- 1.0)))
                (= v0x7feb366bc110_0
                   (ite v0x7feb366bbe10_0 v0x7feb366bbf50_0 v0x7feb366bacd0_0))
                (= v0x7feb366bc250_0 (= v0x7feb366bc110_0 0.0))
                (= v0x7feb366bd310_0 (= v0x7feb366bc6d0_0 2.0))
                (= v0x7feb366bd450_0 (= v0x7feb366bc790_0 0.0))
                (= v0x7feb366bd590_0 (or v0x7feb366bd450_0 v0x7feb366bd310_0))
                (= v0x7feb366bd6d0_0 (xor v0x7feb366bd590_0 true))
                (= v0x7feb366bd810_0 (and v0x7feb366bb510_0 v0x7feb366bd6d0_0)))))
  (=> F0x7feb366be410 a!5))))
(assert (=> F0x7feb366be410 F0x7feb366be350))
(assert (let ((a!1 (=> v0x7feb366bac10_0
               (or (and v0x7feb366ba4d0_0
                        E0x7feb366bad90
                        (<= v0x7feb366bacd0_0 v0x7feb366baad0_0)
                        (>= v0x7feb366bacd0_0 v0x7feb366baad0_0))
                   (and v0x7feb366ba210_0
                        E0x7feb366baf50
                        v0x7feb366ba390_0
                        (<= v0x7feb366bacd0_0 v0x7feb366b9c10_0)
                        (>= v0x7feb366bacd0_0 v0x7feb366b9c10_0)))))
      (a!2 (=> v0x7feb366bac10_0
               (or (and E0x7feb366bad90 (not E0x7feb366baf50))
                   (and E0x7feb366baf50 (not E0x7feb366bad90)))))
      (a!3 (or (and v0x7feb366bb650_0
                    E0x7feb366bc850
                    (<= v0x7feb366bc6d0_0 v0x7feb366bacd0_0)
                    (>= v0x7feb366bc6d0_0 v0x7feb366bacd0_0)
                    (<= v0x7feb366bc790_0 v0x7feb366bba10_0)
                    (>= v0x7feb366bc790_0 v0x7feb366bba10_0))
               (and v0x7feb366bc390_0
                    E0x7feb366bcb10
                    (and (<= v0x7feb366bc6d0_0 v0x7feb366bc110_0)
                         (>= v0x7feb366bc6d0_0 v0x7feb366bc110_0))
                    (<= v0x7feb366bc790_0 v0x7feb366b9d90_0)
                    (>= v0x7feb366bc790_0 v0x7feb366b9d90_0))
               (and v0x7feb366bbb50_0
                    E0x7feb366bcdd0
                    (not v0x7feb366bc250_0)
                    (and (<= v0x7feb366bc6d0_0 v0x7feb366bc110_0)
                         (>= v0x7feb366bc6d0_0 v0x7feb366bc110_0))
                    (<= v0x7feb366bc790_0 0.0)
                    (>= v0x7feb366bc790_0 0.0))))
      (a!4 (=> v0x7feb366bc610_0
               (or (and E0x7feb366bc850
                        (not E0x7feb366bcb10)
                        (not E0x7feb366bcdd0))
                   (and E0x7feb366bcb10
                        (not E0x7feb366bc850)
                        (not E0x7feb366bcdd0))
                   (and E0x7feb366bcdd0
                        (not E0x7feb366bc850)
                        (not E0x7feb366bcb10))))))
(let ((a!5 (and (=> v0x7feb366ba4d0_0
                    (and v0x7feb366ba210_0
                         E0x7feb366ba590
                         (not v0x7feb366ba390_0)))
                (=> v0x7feb366ba4d0_0 E0x7feb366ba590)
                a!1
                a!2
                (=> v0x7feb366bb650_0
                    (and v0x7feb366bac10_0 E0x7feb366bb710 v0x7feb366bb510_0))
                (=> v0x7feb366bb650_0 E0x7feb366bb710)
                (=> v0x7feb366bbb50_0
                    (and v0x7feb366bac10_0
                         E0x7feb366bbc10
                         (not v0x7feb366bb510_0)))
                (=> v0x7feb366bbb50_0 E0x7feb366bbc10)
                (=> v0x7feb366bc390_0
                    (and v0x7feb366bbb50_0 E0x7feb366bc450 v0x7feb366bc250_0))
                (=> v0x7feb366bc390_0 E0x7feb366bc450)
                (=> v0x7feb366bc610_0 a!3)
                a!4
                v0x7feb366bc610_0
                v0x7feb366bd810_0
                (= v0x7feb366ba390_0 (= v0x7feb366ba2d0_0 0.0))
                (= v0x7feb366ba7d0_0 (< v0x7feb366b9c10_0 2.0))
                (= v0x7feb366ba990_0 (ite v0x7feb366ba7d0_0 1.0 0.0))
                (= v0x7feb366baad0_0 (+ v0x7feb366ba990_0 v0x7feb366b9c10_0))
                (= v0x7feb366bb510_0 (= v0x7feb366b9d90_0 0.0))
                (= v0x7feb366bb8d0_0 (> v0x7feb366bacd0_0 1.0))
                (= v0x7feb366bba10_0
                   (ite v0x7feb366bb8d0_0 1.0 v0x7feb366b9d90_0))
                (= v0x7feb366bbe10_0 (> v0x7feb366bacd0_0 0.0))
                (= v0x7feb366bbf50_0 (+ v0x7feb366bacd0_0 (- 1.0)))
                (= v0x7feb366bc110_0
                   (ite v0x7feb366bbe10_0 v0x7feb366bbf50_0 v0x7feb366bacd0_0))
                (= v0x7feb366bc250_0 (= v0x7feb366bc110_0 0.0))
                (= v0x7feb366bd310_0 (= v0x7feb366bc6d0_0 2.0))
                (= v0x7feb366bd450_0 (= v0x7feb366bc790_0 0.0))
                (= v0x7feb366bd590_0 (or v0x7feb366bd450_0 v0x7feb366bd310_0))
                (= v0x7feb366bd6d0_0 (xor v0x7feb366bd590_0 true))
                (= v0x7feb366bd810_0 (and v0x7feb366bb510_0 v0x7feb366bd6d0_0)))))
  (=> F0x7feb366be290 a!5))))
(assert (=> F0x7feb366be290 F0x7feb366be350))
(assert (=> F0x7feb366be650 (or F0x7feb366be590 F0x7feb366be410)))
(assert (=> F0x7feb366be610 F0x7feb366be290))
(assert (=> pre!entry!0 (=> F0x7feb366be4d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7feb366be350 (<= v0x7feb366b9c10_0 2.0))))
(assert (let ((a!1 (not (or (not (>= v0x7feb366b9d90_0 0.0))
                    (not (<= v0x7feb366b9c10_0 1.0))
                    (not (<= v0x7feb366b9d90_0 0.0))
                    (not (>= v0x7feb366b9c10_0 1.0))))))
  (=> pre!bb1.i.i!1 (=> F0x7feb366be350 (or (>= v0x7feb366b9c10_0 2.0) a!1)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7feb366be650 (<= v0x7feb366b9e50_0 2.0))))
(assert (let ((a!1 (not (or (not (>= v0x7feb366b8010_0 0.0))
                    (not (<= v0x7feb366b9e50_0 1.0))
                    (not (<= v0x7feb366b8010_0 0.0))
                    (not (>= v0x7feb366b9e50_0 1.0))))))
  (= lemma!bb1.i.i!1 (=> F0x7feb366be650 (or (>= v0x7feb366b9e50_0 2.0) a!1)))))
(assert (= lemma!bb2.i.i28.i.i!0 (=> F0x7feb366be610 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i28.i.i!0) (not lemma!bb2.i.i28.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7feb366be4d0)
; (error: F0x7feb366be610)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i28.i.i!0)
