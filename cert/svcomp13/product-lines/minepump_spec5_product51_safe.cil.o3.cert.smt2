(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb2.i.i28.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f5204526b10 () Bool)
(declare-fun F0x7f5204526bd0 () Bool)
(declare-fun v0x7f5204525d50_0 () Bool)
(declare-fun v0x7f5204525c10_0 () Bool)
(declare-fun v0x7f5204525ad0_0 () Bool)
(declare-fun v0x7f5204525990_0 () Bool)
(declare-fun F0x7f5204526b50 () Bool)
(declare-fun v0x7f5204523890_0 () Bool)
(declare-fun v0x7f5204522650_0 () Bool)
(declare-fun v0x7f5204520a10_0 () Bool)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun v0x7f5204525fd0_0 () Bool)
(declare-fun E0x7f5204525150 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f5204524f50 () Bool)
(declare-fun E0x7f52045249d0 () Bool)
(declare-fun v0x7f5204522790_0 () Real)
(declare-fun v0x7f520451fd90_0 () Real)
(declare-fun v0x7f5204520bd0_0 () Real)
(declare-fun v0x7f5204523ed0_0 () Bool)
(declare-fun v0x7f52045239d0_0 () Real)
(declare-fun E0x7f5204523bd0 () Bool)
(declare-fun v0x7f5204524150_0 () Bool)
(declare-fun v0x7f5204523610_0 () Bool)
(declare-fun E0x7f5204522dd0 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun E0x7f5204523410 () Bool)
(declare-fun v0x7f5204522950_0 () Real)
(declare-fun E0x7f5204522c10 () Bool)
(declare-fun post!bb2.i.i28.i.i!0 () Bool)
(declare-fun v0x7f5204521690_0 () Real)
(declare-fun v0x7f5204522250_0 () Bool)
(declare-fun E0x7f5204521ed0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f5204522b50_0 () Real)
(declare-fun v0x7f5204521890_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f520451fc10_0 () Real)
(declare-fun v0x7f5204520d10_0 () Real)
(declare-fun v0x7f5204524450_0 () Real)
(declare-fun v0x7f520451fe90_0 () Real)
(declare-fun v0x7f5204520f10_0 () Real)
(declare-fun E0x7f5204521950 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7f5204520e50_0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun v0x7f52045205d0_0 () Bool)
(declare-fun E0x7f5204524510 () Bool)
(declare-fun v0x7f5204524390_0 () Real)
(declare-fun E0x7f5204520fd0 () Bool)
(declare-fun v0x7f5204522390_0 () Bool)
(declare-fun v0x7f5204521b10_0 () Real)
(declare-fun v0x7f5204521d10_0 () Bool)
(declare-fun E0x7f52045207d0 () Bool)
(declare-fun v0x7f5204523b10_0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun F0x7f5204526dd0 () Bool)
(declare-fun v0x7f5204524210_0 () Real)
(declare-fun v0x7f5204522a90_0 () Bool)
(declare-fun F0x7f5204526e90 () Bool)
(declare-fun v0x7f520451e010_0 () Real)
(declare-fun v0x7f5204523d90_0 () Bool)
(declare-fun v0x7f520451ff50_0 () Real)
(declare-fun v0x7f5204521bd0_0 () Bool)
(declare-fun E0x7f5204523f90 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f5204523210_0 () Bool)
(declare-fun E0x7f52045236d0 () Bool)
(declare-fun F0x7f5204526c90 () Bool)
(declare-fun v0x7f520451fe50_0 () Real)
(declare-fun v0x7f5204521750_0 () Bool)
(declare-fun E0x7f5204522450 () Bool)
(declare-fun v0x7f5204523350_0 () Bool)
(declare-fun v0x7f5204520710_0 () Bool)
(declare-fun v0x7f5204520450_0 () Bool)
(declare-fun v0x7f520451e110_0 () Bool)
(declare-fun v0x7f5204520510_0 () Real)
(declare-fun E0x7f5204521190 () Bool)
(declare-fun E0x7f5204524c50 () Bool)
(declare-fun v0x7f52045242d0_0 () Real)
(declare-fun v0x7f5204525e90_0 () Bool)
(declare-fun E0x7f5204521dd0 () Bool)
(declare-fun F0x7f5204526f10 () Bool)

(assert (=> F0x7f5204526f10
    (and v0x7f520451e110_0
         (<= v0x7f520451fe50_0 1.0)
         (>= v0x7f520451fe50_0 1.0)
         (<= v0x7f520451ff50_0 1.0)
         (>= v0x7f520451ff50_0 1.0)
         (<= v0x7f520451e010_0 0.0)
         (>= v0x7f520451e010_0 0.0))))
(assert (=> F0x7f5204526f10 F0x7f5204526e90))
(assert (let ((a!1 (=> v0x7f5204520e50_0
               (or (and v0x7f5204520710_0
                        E0x7f5204520fd0
                        (<= v0x7f5204520f10_0 v0x7f5204520d10_0)
                        (>= v0x7f5204520f10_0 v0x7f5204520d10_0))
                   (and v0x7f5204520450_0
                        E0x7f5204521190
                        v0x7f52045205d0_0
                        (<= v0x7f5204520f10_0 v0x7f520451fc10_0)
                        (>= v0x7f5204520f10_0 v0x7f520451fc10_0)))))
      (a!2 (=> v0x7f5204520e50_0
               (or (and E0x7f5204520fd0 (not E0x7f5204521190))
                   (and E0x7f5204521190 (not E0x7f5204520fd0)))))
      (a!3 (=> v0x7f5204521d10_0
               (or (and v0x7f5204521890_0 E0x7f5204521dd0 v0x7f5204521bd0_0)
                   (and v0x7f5204520e50_0
                        E0x7f5204521ed0
                        (not v0x7f5204521750_0)))))
      (a!4 (=> v0x7f5204521d10_0
               (or (and E0x7f5204521dd0 (not E0x7f5204521ed0))
                   (and E0x7f5204521ed0 (not E0x7f5204521dd0)))))
      (a!5 (=> v0x7f5204522a90_0
               (or (and v0x7f5204522390_0
                        E0x7f5204522c10
                        (<= v0x7f5204522b50_0 v0x7f5204522950_0)
                        (>= v0x7f5204522b50_0 v0x7f5204522950_0))
                   (and v0x7f5204521d10_0
                        E0x7f5204522dd0
                        v0x7f5204522250_0
                        (<= v0x7f5204522b50_0 v0x7f5204520f10_0)
                        (>= v0x7f5204522b50_0 v0x7f5204520f10_0)))))
      (a!6 (=> v0x7f5204522a90_0
               (or (and E0x7f5204522c10 (not E0x7f5204522dd0))
                   (and E0x7f5204522dd0 (not E0x7f5204522c10)))))
      (a!7 (or (and v0x7f5204523610_0
                    E0x7f5204524510
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (<= v0x7f5204524450_0 v0x7f52045239d0_0)
                    (>= v0x7f5204524450_0 v0x7f52045239d0_0))
               (and v0x7f5204523ed0_0
                    E0x7f52045249d0
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (and (<= v0x7f5204524450_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524450_0 v0x7f520451fe90_0)))
               (and v0x7f5204523b10_0
                    E0x7f5204524c50
                    (not v0x7f5204523d90_0)
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (and (<= v0x7f5204524450_0 0.0) (>= v0x7f5204524450_0 0.0)))
               (and v0x7f5204522a90_0
                    E0x7f5204524f50
                    v0x7f5204523210_0
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (and (<= v0x7f5204524450_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524450_0 v0x7f520451fe90_0)))
               (and v0x7f5204521890_0
                    E0x7f5204525150
                    (not v0x7f5204521bd0_0)
                    (<= v0x7f5204524210_0 0.0)
                    (>= v0x7f5204524210_0 0.0)
                    (<= v0x7f52045242d0_0 0.0)
                    (>= v0x7f52045242d0_0 0.0)
                    (<= v0x7f5204524390_0 v0x7f5204520f10_0)
                    (>= v0x7f5204524390_0 v0x7f5204520f10_0)
                    (and (<= v0x7f5204524450_0 0.0) (>= v0x7f5204524450_0 0.0)))))
      (a!8 (=> v0x7f5204524150_0
               (or (and E0x7f5204524510
                        (not E0x7f52045249d0)
                        (not E0x7f5204524c50)
                        (not E0x7f5204524f50)
                        (not E0x7f5204525150))
                   (and E0x7f52045249d0
                        (not E0x7f5204524510)
                        (not E0x7f5204524c50)
                        (not E0x7f5204524f50)
                        (not E0x7f5204525150))
                   (and E0x7f5204524c50
                        (not E0x7f5204524510)
                        (not E0x7f52045249d0)
                        (not E0x7f5204524f50)
                        (not E0x7f5204525150))
                   (and E0x7f5204524f50
                        (not E0x7f5204524510)
                        (not E0x7f52045249d0)
                        (not E0x7f5204524c50)
                        (not E0x7f5204525150))
                   (and E0x7f5204525150
                        (not E0x7f5204524510)
                        (not E0x7f52045249d0)
                        (not E0x7f5204524c50)
                        (not E0x7f5204524f50))))))
(let ((a!9 (and (=> v0x7f5204520710_0
                    (and v0x7f5204520450_0
                         E0x7f52045207d0
                         (not v0x7f52045205d0_0)))
                (=> v0x7f5204520710_0 E0x7f52045207d0)
                a!1
                a!2
                (=> v0x7f5204521890_0
                    (and v0x7f5204520e50_0 E0x7f5204521950 v0x7f5204521750_0))
                (=> v0x7f5204521890_0 E0x7f5204521950)
                a!3
                a!4
                (=> v0x7f5204522390_0
                    (and v0x7f5204521d10_0
                         E0x7f5204522450
                         (not v0x7f5204522250_0)))
                (=> v0x7f5204522390_0 E0x7f5204522450)
                a!5
                a!6
                (=> v0x7f5204523350_0
                    (and v0x7f5204522a90_0
                         E0x7f5204523410
                         (not v0x7f5204523210_0)))
                (=> v0x7f5204523350_0 E0x7f5204523410)
                (=> v0x7f5204523610_0
                    (and v0x7f5204523350_0 E0x7f52045236d0 v0x7f5204522250_0))
                (=> v0x7f5204523610_0 E0x7f52045236d0)
                (=> v0x7f5204523b10_0
                    (and v0x7f5204523350_0
                         E0x7f5204523bd0
                         (not v0x7f5204522250_0)))
                (=> v0x7f5204523b10_0 E0x7f5204523bd0)
                (=> v0x7f5204523ed0_0
                    (and v0x7f5204523b10_0 E0x7f5204523f90 v0x7f5204523d90_0))
                (=> v0x7f5204523ed0_0 E0x7f5204523f90)
                (=> v0x7f5204524150_0 a!7)
                a!8
                v0x7f5204524150_0
                (not v0x7f5204525fd0_0)
                (<= v0x7f520451fe50_0 v0x7f5204524390_0)
                (>= v0x7f520451fe50_0 v0x7f5204524390_0)
                (<= v0x7f520451ff50_0 v0x7f52045242d0_0)
                (>= v0x7f520451ff50_0 v0x7f52045242d0_0)
                (<= v0x7f520451e010_0 v0x7f5204524450_0)
                (>= v0x7f520451e010_0 v0x7f5204524450_0)
                (= v0x7f52045205d0_0 (= v0x7f5204520510_0 0.0))
                (= v0x7f5204520a10_0 (< v0x7f520451fc10_0 2.0))
                (= v0x7f5204520bd0_0 (ite v0x7f5204520a10_0 1.0 0.0))
                (= v0x7f5204520d10_0 (+ v0x7f5204520bd0_0 v0x7f520451fc10_0))
                (= v0x7f5204521750_0 (= v0x7f5204521690_0 0.0))
                (= v0x7f5204521bd0_0 (= v0x7f5204521b10_0 0.0))
                (= v0x7f5204522250_0 (= v0x7f520451fe90_0 0.0))
                (= v0x7f5204522650_0 (> v0x7f5204520f10_0 0.0))
                (= v0x7f5204522790_0 (+ v0x7f5204520f10_0 (- 1.0)))
                (= v0x7f5204522950_0
                   (ite v0x7f5204522650_0 v0x7f5204522790_0 v0x7f5204520f10_0))
                (= v0x7f5204523210_0 (= v0x7f520451fd90_0 0.0))
                (= v0x7f5204523890_0 (> v0x7f5204522b50_0 1.0))
                (= v0x7f52045239d0_0
                   (ite v0x7f5204523890_0 1.0 v0x7f520451fe90_0))
                (= v0x7f5204523d90_0 (= v0x7f5204522b50_0 0.0))
                (= v0x7f5204525990_0 (= v0x7f5204524390_0 2.0))
                (= v0x7f5204525ad0_0 (= v0x7f5204524450_0 0.0))
                (= v0x7f5204525c10_0 (or v0x7f5204525ad0_0 v0x7f5204525990_0))
                (= v0x7f5204525d50_0 (xor v0x7f5204525c10_0 true))
                (= v0x7f5204525e90_0 (= v0x7f5204524210_0 0.0))
                (= v0x7f5204525fd0_0 (and v0x7f5204525e90_0 v0x7f5204525d50_0)))))
  (=> F0x7f5204526dd0 a!9))))
(assert (=> F0x7f5204526dd0 F0x7f5204526bd0))
(assert (let ((a!1 (=> v0x7f5204520e50_0
               (or (and v0x7f5204520710_0
                        E0x7f5204520fd0
                        (<= v0x7f5204520f10_0 v0x7f5204520d10_0)
                        (>= v0x7f5204520f10_0 v0x7f5204520d10_0))
                   (and v0x7f5204520450_0
                        E0x7f5204521190
                        v0x7f52045205d0_0
                        (<= v0x7f5204520f10_0 v0x7f520451fc10_0)
                        (>= v0x7f5204520f10_0 v0x7f520451fc10_0)))))
      (a!2 (=> v0x7f5204520e50_0
               (or (and E0x7f5204520fd0 (not E0x7f5204521190))
                   (and E0x7f5204521190 (not E0x7f5204520fd0)))))
      (a!3 (=> v0x7f5204521d10_0
               (or (and v0x7f5204521890_0 E0x7f5204521dd0 v0x7f5204521bd0_0)
                   (and v0x7f5204520e50_0
                        E0x7f5204521ed0
                        (not v0x7f5204521750_0)))))
      (a!4 (=> v0x7f5204521d10_0
               (or (and E0x7f5204521dd0 (not E0x7f5204521ed0))
                   (and E0x7f5204521ed0 (not E0x7f5204521dd0)))))
      (a!5 (=> v0x7f5204522a90_0
               (or (and v0x7f5204522390_0
                        E0x7f5204522c10
                        (<= v0x7f5204522b50_0 v0x7f5204522950_0)
                        (>= v0x7f5204522b50_0 v0x7f5204522950_0))
                   (and v0x7f5204521d10_0
                        E0x7f5204522dd0
                        v0x7f5204522250_0
                        (<= v0x7f5204522b50_0 v0x7f5204520f10_0)
                        (>= v0x7f5204522b50_0 v0x7f5204520f10_0)))))
      (a!6 (=> v0x7f5204522a90_0
               (or (and E0x7f5204522c10 (not E0x7f5204522dd0))
                   (and E0x7f5204522dd0 (not E0x7f5204522c10)))))
      (a!7 (or (and v0x7f5204523610_0
                    E0x7f5204524510
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (<= v0x7f5204524450_0 v0x7f52045239d0_0)
                    (>= v0x7f5204524450_0 v0x7f52045239d0_0))
               (and v0x7f5204523ed0_0
                    E0x7f52045249d0
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (and (<= v0x7f5204524450_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524450_0 v0x7f520451fe90_0)))
               (and v0x7f5204523b10_0
                    E0x7f5204524c50
                    (not v0x7f5204523d90_0)
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (and (<= v0x7f5204524450_0 0.0) (>= v0x7f5204524450_0 0.0)))
               (and v0x7f5204522a90_0
                    E0x7f5204524f50
                    v0x7f5204523210_0
                    (and (<= v0x7f5204524210_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524210_0 v0x7f520451fe90_0))
                    (and (<= v0x7f52045242d0_0 v0x7f520451fd90_0)
                         (>= v0x7f52045242d0_0 v0x7f520451fd90_0))
                    (and (<= v0x7f5204524390_0 v0x7f5204522b50_0)
                         (>= v0x7f5204524390_0 v0x7f5204522b50_0))
                    (and (<= v0x7f5204524450_0 v0x7f520451fe90_0)
                         (>= v0x7f5204524450_0 v0x7f520451fe90_0)))
               (and v0x7f5204521890_0
                    E0x7f5204525150
                    (not v0x7f5204521bd0_0)
                    (<= v0x7f5204524210_0 0.0)
                    (>= v0x7f5204524210_0 0.0)
                    (<= v0x7f52045242d0_0 0.0)
                    (>= v0x7f52045242d0_0 0.0)
                    (<= v0x7f5204524390_0 v0x7f5204520f10_0)
                    (>= v0x7f5204524390_0 v0x7f5204520f10_0)
                    (and (<= v0x7f5204524450_0 0.0) (>= v0x7f5204524450_0 0.0)))))
      (a!8 (=> v0x7f5204524150_0
               (or (and E0x7f5204524510
                        (not E0x7f52045249d0)
                        (not E0x7f5204524c50)
                        (not E0x7f5204524f50)
                        (not E0x7f5204525150))
                   (and E0x7f52045249d0
                        (not E0x7f5204524510)
                        (not E0x7f5204524c50)
                        (not E0x7f5204524f50)
                        (not E0x7f5204525150))
                   (and E0x7f5204524c50
                        (not E0x7f5204524510)
                        (not E0x7f52045249d0)
                        (not E0x7f5204524f50)
                        (not E0x7f5204525150))
                   (and E0x7f5204524f50
                        (not E0x7f5204524510)
                        (not E0x7f52045249d0)
                        (not E0x7f5204524c50)
                        (not E0x7f5204525150))
                   (and E0x7f5204525150
                        (not E0x7f5204524510)
                        (not E0x7f52045249d0)
                        (not E0x7f5204524c50)
                        (not E0x7f5204524f50))))))
(let ((a!9 (and (=> v0x7f5204520710_0
                    (and v0x7f5204520450_0
                         E0x7f52045207d0
                         (not v0x7f52045205d0_0)))
                (=> v0x7f5204520710_0 E0x7f52045207d0)
                a!1
                a!2
                (=> v0x7f5204521890_0
                    (and v0x7f5204520e50_0 E0x7f5204521950 v0x7f5204521750_0))
                (=> v0x7f5204521890_0 E0x7f5204521950)
                a!3
                a!4
                (=> v0x7f5204522390_0
                    (and v0x7f5204521d10_0
                         E0x7f5204522450
                         (not v0x7f5204522250_0)))
                (=> v0x7f5204522390_0 E0x7f5204522450)
                a!5
                a!6
                (=> v0x7f5204523350_0
                    (and v0x7f5204522a90_0
                         E0x7f5204523410
                         (not v0x7f5204523210_0)))
                (=> v0x7f5204523350_0 E0x7f5204523410)
                (=> v0x7f5204523610_0
                    (and v0x7f5204523350_0 E0x7f52045236d0 v0x7f5204522250_0))
                (=> v0x7f5204523610_0 E0x7f52045236d0)
                (=> v0x7f5204523b10_0
                    (and v0x7f5204523350_0
                         E0x7f5204523bd0
                         (not v0x7f5204522250_0)))
                (=> v0x7f5204523b10_0 E0x7f5204523bd0)
                (=> v0x7f5204523ed0_0
                    (and v0x7f5204523b10_0 E0x7f5204523f90 v0x7f5204523d90_0))
                (=> v0x7f5204523ed0_0 E0x7f5204523f90)
                (=> v0x7f5204524150_0 a!7)
                a!8
                v0x7f5204524150_0
                v0x7f5204525fd0_0
                (= v0x7f52045205d0_0 (= v0x7f5204520510_0 0.0))
                (= v0x7f5204520a10_0 (< v0x7f520451fc10_0 2.0))
                (= v0x7f5204520bd0_0 (ite v0x7f5204520a10_0 1.0 0.0))
                (= v0x7f5204520d10_0 (+ v0x7f5204520bd0_0 v0x7f520451fc10_0))
                (= v0x7f5204521750_0 (= v0x7f5204521690_0 0.0))
                (= v0x7f5204521bd0_0 (= v0x7f5204521b10_0 0.0))
                (= v0x7f5204522250_0 (= v0x7f520451fe90_0 0.0))
                (= v0x7f5204522650_0 (> v0x7f5204520f10_0 0.0))
                (= v0x7f5204522790_0 (+ v0x7f5204520f10_0 (- 1.0)))
                (= v0x7f5204522950_0
                   (ite v0x7f5204522650_0 v0x7f5204522790_0 v0x7f5204520f10_0))
                (= v0x7f5204523210_0 (= v0x7f520451fd90_0 0.0))
                (= v0x7f5204523890_0 (> v0x7f5204522b50_0 1.0))
                (= v0x7f52045239d0_0
                   (ite v0x7f5204523890_0 1.0 v0x7f520451fe90_0))
                (= v0x7f5204523d90_0 (= v0x7f5204522b50_0 0.0))
                (= v0x7f5204525990_0 (= v0x7f5204524390_0 2.0))
                (= v0x7f5204525ad0_0 (= v0x7f5204524450_0 0.0))
                (= v0x7f5204525c10_0 (or v0x7f5204525ad0_0 v0x7f5204525990_0))
                (= v0x7f5204525d50_0 (xor v0x7f5204525c10_0 true))
                (= v0x7f5204525e90_0 (= v0x7f5204524210_0 0.0))
                (= v0x7f5204525fd0_0 (and v0x7f5204525e90_0 v0x7f5204525d50_0)))))
  (=> F0x7f5204526c90 a!9))))
(assert (=> F0x7f5204526c90 F0x7f5204526bd0))
(assert (=> F0x7f5204526b10 (or F0x7f5204526f10 F0x7f5204526dd0)))
(assert (=> F0x7f5204526b50 F0x7f5204526c90))
(assert (=> pre!entry!0 (=> F0x7f5204526e90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f5204526bd0 (>= v0x7f520451fd90_0 0.0))))
(assert (let ((a!1 (=> F0x7f5204526bd0
               (or (>= v0x7f520451fc10_0 0.0) (not (<= 1.0 v0x7f520451fd90_0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f5204526bd0
        (or (>= v0x7f520451fc10_0 1.0)
            (<= v0x7f520451fc10_0 0.0)
            (<= v0x7f520451fd90_0 0.0)))))
(assert (let ((a!1 (=> F0x7f5204526bd0
               (or (not (<= 1.0 v0x7f520451fd90_0)) (<= v0x7f520451fc10_0 2.0)))))
  (=> pre!bb1.i.i!3 a!1)))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f5204526bd0
        (or (<= v0x7f520451fc10_0 1.0)
            (<= v0x7f520451fd90_0 0.0)
            (>= v0x7f520451fc10_0 2.0)))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7f5204526bd0
        (or (>= v0x7f520451fd90_0 1.0) (<= v0x7f520451fd90_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f5204526b10 (>= v0x7f520451ff50_0 0.0))))
(assert (let ((a!1 (=> F0x7f5204526b10
               (or (>= v0x7f520451fe50_0 0.0) (not (<= 1.0 v0x7f520451ff50_0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!2
   (=> F0x7f5204526b10
       (or (>= v0x7f520451fe50_0 1.0)
           (<= v0x7f520451fe50_0 0.0)
           (<= v0x7f520451ff50_0 0.0)))))
(assert (let ((a!1 (=> F0x7f5204526b10
               (or (not (<= 1.0 v0x7f520451ff50_0)) (<= v0x7f520451fe50_0 2.0)))))
  (= lemma!bb1.i.i!3 a!1)))
(assert (= lemma!bb1.i.i!4
   (=> F0x7f5204526b10
       (or (<= v0x7f520451fe50_0 1.0)
           (<= v0x7f520451ff50_0 0.0)
           (>= v0x7f520451fe50_0 2.0)))))
(assert (= lemma!bb1.i.i!5
   (=> F0x7f5204526b10
       (or (>= v0x7f520451ff50_0 1.0) (<= v0x7f520451ff50_0 0.0)))))
(assert (= lemma!bb2.i.i28.i.i!0 (=> F0x7f5204526b50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb2.i.i28.i.i!0) (not lemma!bb2.i.i28.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
; (init: F0x7f5204526e90)
; (error: F0x7f5204526b50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i28.i.i!0)
