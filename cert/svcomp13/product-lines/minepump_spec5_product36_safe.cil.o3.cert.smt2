(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fb62f1d6a50 () Bool)
(declare-fun v0x7fb62f1d5890_0 () Bool)
(declare-fun v0x7fb62f1d4250_0 () Bool)
(declare-fun v0x7fb62f1d1c10_0 () Real)
(declare-fun v0x7fb62f1d1150_0 () Real)
(declare-fun v0x7fb62f1d5750_0 () Bool)
(declare-fun v0x7fb62f1d0f90_0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun F0x7fb62f1d6a10 () Bool)
(declare-fun v0x7fb62f1d5b10_0 () Bool)
(declare-fun E0x7fb62f1d4e10 () Bool)
(declare-fun F0x7fb62f1d6890 () Bool)
(declare-fun F0x7fb62f1d6950 () Bool)
(declare-fun v0x7fb62f1d4390_0 () Bool)
(declare-fun v0x7fb62f1d59d0_0 () Bool)
(declare-fun v0x7fb62f1d4890_0 () Bool)
(declare-fun v0x7fb62f1d49d0_0 () Bool)
(declare-fun v0x7fb62f1d4c90_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fb62f1d44d0_0 () Bool)
(declare-fun E0x7fb62f1d46d0 () Bool)
(declare-fun lemma!bb2.i.i16.i.i!0 () Bool)
(declare-fun v0x7fb62f1d4610_0 () Bool)
(declare-fun v0x7fb62f1d3510_0 () Real)
(declare-fun v0x7fb62f1d3190_0 () Real)
(declare-fun v0x7fb62f1d3450_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fb62f1d0410_0 () Real)
(declare-fun v0x7fb62f1d3390_0 () Real)
(declare-fun E0x7fb62f1d35d0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7fb62f1d32d0_0 () Bool)
(declare-fun v0x7fb62f1d2a90_0 () Bool)
(declare-fun v0x7fb62f1d4d50_0 () Real)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun E0x7fb62f1d2c90 () Bool)
(declare-fun v0x7fb62f1d2bd0_0 () Bool)
(declare-fun v0x7fb62f1d2fd0_0 () Real)
(declare-fun E0x7fb62f1d2610 () Bool)
(declare-fun v0x7fb62f1d2290_0 () Bool)
(declare-fun E0x7fb62f1d4fd0 () Bool)
(declare-fun E0x7fb62f1d1ed0 () Bool)
(declare-fun v0x7fb62f1d2090_0 () Real)
(declare-fun v0x7fb62f1d1e10_0 () Bool)
(declare-fun v0x7fb62f1d1290_0 () Real)
(declare-fun v0x7fb62f1d1490_0 () Real)
(declare-fun v0x7fb62f1d0310_0 () Real)
(declare-fun E0x7fb62f1d1550 () Bool)
(declare-fun v0x7fb62f1d13d0_0 () Bool)
(declare-fun v0x7fb62f1d1cd0_0 () Bool)
(declare-fun v0x7fb62f1d5610_0 () Bool)
(declare-fun v0x7fb62f1d2e90_0 () Bool)
(declare-fun v0x7fb62f1d0b50_0 () Bool)
(declare-fun v0x7fb62f1d0a90_0 () Real)
(declare-fun F0x7fb62f1d66d0 () Bool)
(declare-fun F0x7fb62f1d6790 () Bool)
(declare-fun v0x7fb62f1d0c90_0 () Bool)
(declare-fun E0x7fb62f1d51d0 () Bool)
(declare-fun E0x7fb62f1d4a90 () Bool)
(declare-fun v0x7fb62f1d09d0_0 () Bool)
(declare-fun E0x7fb62f1d2410 () Bool)
(declare-fun v0x7fb62f1cf010_0 () Real)
(declare-fun v0x7fb62f1d2350_0 () Real)
(declare-fun v0x7fb62f1d03d0_0 () Real)
(declare-fun v0x7fb62f1d04d0_0 () Real)
(declare-fun E0x7fb62f1d1710 () Bool)
(declare-fun v0x7fb62f1d0190_0 () Real)
(declare-fun E0x7fb62f1d3c10 () Bool)
(declare-fun v0x7fb62f1cf110_0 () Bool)
(declare-fun E0x7fb62f1d3990 () Bool)
(declare-fun v0x7fb62f1d2150_0 () Bool)
(declare-fun E0x7fb62f1d0d50 () Bool)
(declare-fun F0x7fb62f1d6850 () Bool)

(assert (=> F0x7fb62f1d6850
    (and v0x7fb62f1cf110_0
         (<= v0x7fb62f1d03d0_0 1.0)
         (>= v0x7fb62f1d03d0_0 1.0)
         (<= v0x7fb62f1d04d0_0 1.0)
         (>= v0x7fb62f1d04d0_0 1.0)
         (<= v0x7fb62f1cf010_0 0.0)
         (>= v0x7fb62f1cf010_0 0.0))))
(assert (=> F0x7fb62f1d6850 F0x7fb62f1d6790))
(assert (let ((a!1 (=> v0x7fb62f1d13d0_0
               (or (and v0x7fb62f1d0c90_0
                        E0x7fb62f1d1550
                        (<= v0x7fb62f1d1490_0 v0x7fb62f1d1290_0)
                        (>= v0x7fb62f1d1490_0 v0x7fb62f1d1290_0))
                   (and v0x7fb62f1d09d0_0
                        E0x7fb62f1d1710
                        v0x7fb62f1d0b50_0
                        (<= v0x7fb62f1d1490_0 v0x7fb62f1d0190_0)
                        (>= v0x7fb62f1d1490_0 v0x7fb62f1d0190_0)))))
      (a!2 (=> v0x7fb62f1d13d0_0
               (or (and E0x7fb62f1d1550 (not E0x7fb62f1d1710))
                   (and E0x7fb62f1d1710 (not E0x7fb62f1d1550)))))
      (a!3 (=> v0x7fb62f1d2290_0
               (or (and v0x7fb62f1d1e10_0
                        E0x7fb62f1d2410
                        v0x7fb62f1d2150_0
                        (<= v0x7fb62f1d2350_0 v0x7fb62f1d0310_0)
                        (>= v0x7fb62f1d2350_0 v0x7fb62f1d0310_0))
                   (and v0x7fb62f1d13d0_0
                        E0x7fb62f1d2610
                        (not v0x7fb62f1d1cd0_0)
                        (<= v0x7fb62f1d2350_0 1.0)
                        (>= v0x7fb62f1d2350_0 1.0)))))
      (a!4 (=> v0x7fb62f1d2290_0
               (or (and E0x7fb62f1d2410 (not E0x7fb62f1d2610))
                   (and E0x7fb62f1d2610 (not E0x7fb62f1d2410)))))
      (a!5 (or (and v0x7fb62f1d2bd0_0
                    E0x7fb62f1d35d0
                    (and (<= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0)
                         (>= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0))
                    (and (<= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0)
                         (>= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0))
                    (<= v0x7fb62f1d3510_0 v0x7fb62f1d3190_0)
                    (>= v0x7fb62f1d3510_0 v0x7fb62f1d3190_0))
               (and v0x7fb62f1d2290_0
                    E0x7fb62f1d3990
                    v0x7fb62f1d2a90_0
                    (and (<= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0)
                         (>= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0))
                    (and (<= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0)
                         (>= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0))
                    (and (<= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)
                         (>= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)))
               (and v0x7fb62f1d1e10_0
                    E0x7fb62f1d3c10
                    (not v0x7fb62f1d2150_0)
                    (<= v0x7fb62f1d3390_0 0.0)
                    (>= v0x7fb62f1d3390_0 0.0)
                    (<= v0x7fb62f1d3450_0 0.0)
                    (>= v0x7fb62f1d3450_0 0.0)
                    (and (<= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)
                         (>= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)))))
      (a!6 (=> v0x7fb62f1d32d0_0
               (or (and E0x7fb62f1d35d0
                        (not E0x7fb62f1d3990)
                        (not E0x7fb62f1d3c10))
                   (and E0x7fb62f1d3990
                        (not E0x7fb62f1d35d0)
                        (not E0x7fb62f1d3c10))
                   (and E0x7fb62f1d3c10
                        (not E0x7fb62f1d35d0)
                        (not E0x7fb62f1d3990)))))
      (a!7 (or (and v0x7fb62f1d49d0_0
                    E0x7fb62f1d4e10
                    (and (<= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)
                         (>= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)))
               (and v0x7fb62f1d4610_0
                    E0x7fb62f1d4fd0
                    v0x7fb62f1d4890_0
                    (<= v0x7fb62f1d4d50_0 1.0)
                    (>= v0x7fb62f1d4d50_0 1.0))
               (and v0x7fb62f1d32d0_0
                    E0x7fb62f1d51d0
                    (not v0x7fb62f1d44d0_0)
                    (and (<= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)
                         (>= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)))))
      (a!8 (=> v0x7fb62f1d4c90_0
               (or (and E0x7fb62f1d4e10
                        (not E0x7fb62f1d4fd0)
                        (not E0x7fb62f1d51d0))
                   (and E0x7fb62f1d4fd0
                        (not E0x7fb62f1d4e10)
                        (not E0x7fb62f1d51d0))
                   (and E0x7fb62f1d51d0
                        (not E0x7fb62f1d4e10)
                        (not E0x7fb62f1d4fd0))))))
(let ((a!9 (and (=> v0x7fb62f1d0c90_0
                    (and v0x7fb62f1d09d0_0
                         E0x7fb62f1d0d50
                         (not v0x7fb62f1d0b50_0)))
                (=> v0x7fb62f1d0c90_0 E0x7fb62f1d0d50)
                a!1
                a!2
                (=> v0x7fb62f1d1e10_0
                    (and v0x7fb62f1d13d0_0 E0x7fb62f1d1ed0 v0x7fb62f1d1cd0_0))
                (=> v0x7fb62f1d1e10_0 E0x7fb62f1d1ed0)
                a!3
                a!4
                (=> v0x7fb62f1d2bd0_0
                    (and v0x7fb62f1d2290_0
                         E0x7fb62f1d2c90
                         (not v0x7fb62f1d2a90_0)))
                (=> v0x7fb62f1d2bd0_0 E0x7fb62f1d2c90)
                (=> v0x7fb62f1d32d0_0 a!5)
                a!6
                (=> v0x7fb62f1d4610_0
                    (and v0x7fb62f1d32d0_0 E0x7fb62f1d46d0 v0x7fb62f1d44d0_0))
                (=> v0x7fb62f1d4610_0 E0x7fb62f1d46d0)
                (=> v0x7fb62f1d49d0_0
                    (and v0x7fb62f1d4610_0
                         E0x7fb62f1d4a90
                         (not v0x7fb62f1d4890_0)))
                (=> v0x7fb62f1d49d0_0 E0x7fb62f1d4a90)
                (=> v0x7fb62f1d4c90_0 a!7)
                a!8
                v0x7fb62f1d4c90_0
                (not v0x7fb62f1d5b10_0)
                (<= v0x7fb62f1d03d0_0 v0x7fb62f1d3510_0)
                (>= v0x7fb62f1d03d0_0 v0x7fb62f1d3510_0)
                (<= v0x7fb62f1d04d0_0 v0x7fb62f1d3450_0)
                (>= v0x7fb62f1d04d0_0 v0x7fb62f1d3450_0)
                (<= v0x7fb62f1cf010_0 v0x7fb62f1d4d50_0)
                (>= v0x7fb62f1cf010_0 v0x7fb62f1d4d50_0)
                (= v0x7fb62f1d0b50_0 (= v0x7fb62f1d0a90_0 0.0))
                (= v0x7fb62f1d0f90_0 (< v0x7fb62f1d0190_0 2.0))
                (= v0x7fb62f1d1150_0 (ite v0x7fb62f1d0f90_0 1.0 0.0))
                (= v0x7fb62f1d1290_0 (+ v0x7fb62f1d1150_0 v0x7fb62f1d0190_0))
                (= v0x7fb62f1d1cd0_0 (= v0x7fb62f1d1c10_0 0.0))
                (= v0x7fb62f1d2150_0 (= v0x7fb62f1d2090_0 0.0))
                (= v0x7fb62f1d2a90_0 (= v0x7fb62f1d0410_0 0.0))
                (= v0x7fb62f1d2e90_0 (> v0x7fb62f1d1490_0 0.0))
                (= v0x7fb62f1d2fd0_0 (+ v0x7fb62f1d1490_0 (- 1.0)))
                (= v0x7fb62f1d3190_0
                   (ite v0x7fb62f1d2e90_0 v0x7fb62f1d2fd0_0 v0x7fb62f1d1490_0))
                (= v0x7fb62f1d4250_0 (not (= v0x7fb62f1d3450_0 0.0)))
                (= v0x7fb62f1d4390_0 (= v0x7fb62f1d3390_0 0.0))
                (= v0x7fb62f1d44d0_0 (and v0x7fb62f1d4250_0 v0x7fb62f1d4390_0))
                (= v0x7fb62f1d4890_0 (> v0x7fb62f1d3510_0 1.0))
                (= v0x7fb62f1d5610_0 (= v0x7fb62f1d3510_0 2.0))
                (= v0x7fb62f1d5750_0 (= v0x7fb62f1d4d50_0 0.0))
                (= v0x7fb62f1d5890_0 (or v0x7fb62f1d5750_0 v0x7fb62f1d5610_0))
                (= v0x7fb62f1d59d0_0 (xor v0x7fb62f1d5890_0 true))
                (= v0x7fb62f1d5b10_0 (and v0x7fb62f1d4390_0 v0x7fb62f1d59d0_0)))))
  (=> F0x7fb62f1d66d0 a!9))))
(assert (=> F0x7fb62f1d66d0 F0x7fb62f1d6890))
(assert (let ((a!1 (=> v0x7fb62f1d13d0_0
               (or (and v0x7fb62f1d0c90_0
                        E0x7fb62f1d1550
                        (<= v0x7fb62f1d1490_0 v0x7fb62f1d1290_0)
                        (>= v0x7fb62f1d1490_0 v0x7fb62f1d1290_0))
                   (and v0x7fb62f1d09d0_0
                        E0x7fb62f1d1710
                        v0x7fb62f1d0b50_0
                        (<= v0x7fb62f1d1490_0 v0x7fb62f1d0190_0)
                        (>= v0x7fb62f1d1490_0 v0x7fb62f1d0190_0)))))
      (a!2 (=> v0x7fb62f1d13d0_0
               (or (and E0x7fb62f1d1550 (not E0x7fb62f1d1710))
                   (and E0x7fb62f1d1710 (not E0x7fb62f1d1550)))))
      (a!3 (=> v0x7fb62f1d2290_0
               (or (and v0x7fb62f1d1e10_0
                        E0x7fb62f1d2410
                        v0x7fb62f1d2150_0
                        (<= v0x7fb62f1d2350_0 v0x7fb62f1d0310_0)
                        (>= v0x7fb62f1d2350_0 v0x7fb62f1d0310_0))
                   (and v0x7fb62f1d13d0_0
                        E0x7fb62f1d2610
                        (not v0x7fb62f1d1cd0_0)
                        (<= v0x7fb62f1d2350_0 1.0)
                        (>= v0x7fb62f1d2350_0 1.0)))))
      (a!4 (=> v0x7fb62f1d2290_0
               (or (and E0x7fb62f1d2410 (not E0x7fb62f1d2610))
                   (and E0x7fb62f1d2610 (not E0x7fb62f1d2410)))))
      (a!5 (or (and v0x7fb62f1d2bd0_0
                    E0x7fb62f1d35d0
                    (and (<= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0)
                         (>= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0))
                    (and (<= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0)
                         (>= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0))
                    (<= v0x7fb62f1d3510_0 v0x7fb62f1d3190_0)
                    (>= v0x7fb62f1d3510_0 v0x7fb62f1d3190_0))
               (and v0x7fb62f1d2290_0
                    E0x7fb62f1d3990
                    v0x7fb62f1d2a90_0
                    (and (<= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0)
                         (>= v0x7fb62f1d3390_0 v0x7fb62f1d0410_0))
                    (and (<= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0)
                         (>= v0x7fb62f1d3450_0 v0x7fb62f1d2350_0))
                    (and (<= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)
                         (>= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)))
               (and v0x7fb62f1d1e10_0
                    E0x7fb62f1d3c10
                    (not v0x7fb62f1d2150_0)
                    (<= v0x7fb62f1d3390_0 0.0)
                    (>= v0x7fb62f1d3390_0 0.0)
                    (<= v0x7fb62f1d3450_0 0.0)
                    (>= v0x7fb62f1d3450_0 0.0)
                    (and (<= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)
                         (>= v0x7fb62f1d3510_0 v0x7fb62f1d1490_0)))))
      (a!6 (=> v0x7fb62f1d32d0_0
               (or (and E0x7fb62f1d35d0
                        (not E0x7fb62f1d3990)
                        (not E0x7fb62f1d3c10))
                   (and E0x7fb62f1d3990
                        (not E0x7fb62f1d35d0)
                        (not E0x7fb62f1d3c10))
                   (and E0x7fb62f1d3c10
                        (not E0x7fb62f1d35d0)
                        (not E0x7fb62f1d3990)))))
      (a!7 (or (and v0x7fb62f1d49d0_0
                    E0x7fb62f1d4e10
                    (and (<= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)
                         (>= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)))
               (and v0x7fb62f1d4610_0
                    E0x7fb62f1d4fd0
                    v0x7fb62f1d4890_0
                    (<= v0x7fb62f1d4d50_0 1.0)
                    (>= v0x7fb62f1d4d50_0 1.0))
               (and v0x7fb62f1d32d0_0
                    E0x7fb62f1d51d0
                    (not v0x7fb62f1d44d0_0)
                    (and (<= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)
                         (>= v0x7fb62f1d4d50_0 v0x7fb62f1d3390_0)))))
      (a!8 (=> v0x7fb62f1d4c90_0
               (or (and E0x7fb62f1d4e10
                        (not E0x7fb62f1d4fd0)
                        (not E0x7fb62f1d51d0))
                   (and E0x7fb62f1d4fd0
                        (not E0x7fb62f1d4e10)
                        (not E0x7fb62f1d51d0))
                   (and E0x7fb62f1d51d0
                        (not E0x7fb62f1d4e10)
                        (not E0x7fb62f1d4fd0))))))
(let ((a!9 (and (=> v0x7fb62f1d0c90_0
                    (and v0x7fb62f1d09d0_0
                         E0x7fb62f1d0d50
                         (not v0x7fb62f1d0b50_0)))
                (=> v0x7fb62f1d0c90_0 E0x7fb62f1d0d50)
                a!1
                a!2
                (=> v0x7fb62f1d1e10_0
                    (and v0x7fb62f1d13d0_0 E0x7fb62f1d1ed0 v0x7fb62f1d1cd0_0))
                (=> v0x7fb62f1d1e10_0 E0x7fb62f1d1ed0)
                a!3
                a!4
                (=> v0x7fb62f1d2bd0_0
                    (and v0x7fb62f1d2290_0
                         E0x7fb62f1d2c90
                         (not v0x7fb62f1d2a90_0)))
                (=> v0x7fb62f1d2bd0_0 E0x7fb62f1d2c90)
                (=> v0x7fb62f1d32d0_0 a!5)
                a!6
                (=> v0x7fb62f1d4610_0
                    (and v0x7fb62f1d32d0_0 E0x7fb62f1d46d0 v0x7fb62f1d44d0_0))
                (=> v0x7fb62f1d4610_0 E0x7fb62f1d46d0)
                (=> v0x7fb62f1d49d0_0
                    (and v0x7fb62f1d4610_0
                         E0x7fb62f1d4a90
                         (not v0x7fb62f1d4890_0)))
                (=> v0x7fb62f1d49d0_0 E0x7fb62f1d4a90)
                (=> v0x7fb62f1d4c90_0 a!7)
                a!8
                v0x7fb62f1d4c90_0
                v0x7fb62f1d5b10_0
                (= v0x7fb62f1d0b50_0 (= v0x7fb62f1d0a90_0 0.0))
                (= v0x7fb62f1d0f90_0 (< v0x7fb62f1d0190_0 2.0))
                (= v0x7fb62f1d1150_0 (ite v0x7fb62f1d0f90_0 1.0 0.0))
                (= v0x7fb62f1d1290_0 (+ v0x7fb62f1d1150_0 v0x7fb62f1d0190_0))
                (= v0x7fb62f1d1cd0_0 (= v0x7fb62f1d1c10_0 0.0))
                (= v0x7fb62f1d2150_0 (= v0x7fb62f1d2090_0 0.0))
                (= v0x7fb62f1d2a90_0 (= v0x7fb62f1d0410_0 0.0))
                (= v0x7fb62f1d2e90_0 (> v0x7fb62f1d1490_0 0.0))
                (= v0x7fb62f1d2fd0_0 (+ v0x7fb62f1d1490_0 (- 1.0)))
                (= v0x7fb62f1d3190_0
                   (ite v0x7fb62f1d2e90_0 v0x7fb62f1d2fd0_0 v0x7fb62f1d1490_0))
                (= v0x7fb62f1d4250_0 (not (= v0x7fb62f1d3450_0 0.0)))
                (= v0x7fb62f1d4390_0 (= v0x7fb62f1d3390_0 0.0))
                (= v0x7fb62f1d44d0_0 (and v0x7fb62f1d4250_0 v0x7fb62f1d4390_0))
                (= v0x7fb62f1d4890_0 (> v0x7fb62f1d3510_0 1.0))
                (= v0x7fb62f1d5610_0 (= v0x7fb62f1d3510_0 2.0))
                (= v0x7fb62f1d5750_0 (= v0x7fb62f1d4d50_0 0.0))
                (= v0x7fb62f1d5890_0 (or v0x7fb62f1d5750_0 v0x7fb62f1d5610_0))
                (= v0x7fb62f1d59d0_0 (xor v0x7fb62f1d5890_0 true))
                (= v0x7fb62f1d5b10_0 (and v0x7fb62f1d4390_0 v0x7fb62f1d59d0_0)))))
  (=> F0x7fb62f1d6950 a!9))))
(assert (=> F0x7fb62f1d6950 F0x7fb62f1d6890))
(assert (=> F0x7fb62f1d6a50 (or F0x7fb62f1d6850 F0x7fb62f1d66d0)))
(assert (=> F0x7fb62f1d6a10 F0x7fb62f1d6950))
(assert (=> pre!entry!0 (=> F0x7fb62f1d6790 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fb62f1d6890 (<= v0x7fb62f1d0190_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fb62f1d6890
        (or (<= v0x7fb62f1d0190_0 1.0) (>= v0x7fb62f1d0190_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fb62f1d6890 (>= v0x7fb62f1d0190_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fb62f1d6890
        (or (>= v0x7fb62f1d0190_0 1.0) (<= v0x7fb62f1d0190_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fb62f1d6a50 (<= v0x7fb62f1d03d0_0 2.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fb62f1d6a50
       (or (<= v0x7fb62f1d03d0_0 1.0) (>= v0x7fb62f1d03d0_0 2.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fb62f1d6a50 (>= v0x7fb62f1d03d0_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7fb62f1d6a50
       (or (>= v0x7fb62f1d03d0_0 1.0) (<= v0x7fb62f1d03d0_0 0.0)))))
(assert (= lemma!bb2.i.i16.i.i!0 (=> F0x7fb62f1d6a10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb2.i.i16.i.i!0) (not lemma!bb2.i.i16.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7fb62f1d6790)
; (error: F0x7fb62f1d6a10)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i16.i.i!0)
