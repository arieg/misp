(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i28.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun F0x7f8c376eb610 () Bool)
(declare-fun F0x7f8c376eb350 () Bool)
(declare-fun v0x7f8c376ea6d0_0 () Bool)
(declare-fun v0x7f8c376ea450_0 () Bool)
(declare-fun v0x7f8c376e8f50_0 () Real)
(declare-fun v0x7f8c376e8e10_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun F0x7f8c376eb650 () Bool)
(declare-fun v0x7f8c376e88d0_0 () Bool)
(declare-fun v0x7f8c376ea310_0 () Bool)
(declare-fun v0x7f8c376e7990_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f8c376e77d0_0 () Bool)
(declare-fun v0x7f8c376e72d0_0 () Real)
(declare-fun v0x7f8c376ea810_0 () Bool)
(declare-fun v0x7f8c376e6d90_0 () Real)
(declare-fun v0x7f8c376e8a10_0 () Real)
(declare-fun v0x7f8c376e9610_0 () Bool)
(declare-fun v0x7f8c376e9390_0 () Bool)
(declare-fun v0x7f8c376e9790_0 () Real)
(declare-fun v0x7f8c376e9250_0 () Bool)
(declare-fun E0x7f8c376e9b10 () Bool)
(declare-fun E0x7f8c376e8c10 () Bool)
(declare-fun v0x7f8c376e8510_0 () Bool)
(declare-fun E0x7f8c376e9850 () Bool)
(declare-fun F0x7f8c376eb290 () Bool)
(declare-fun v0x7f8c376e6c10_0 () Real)
(declare-fun E0x7f8c376e7f50 () Bool)
(declare-fun v0x7f8c376e7ad0_0 () Real)
(declare-fun v0x7f8c376e7c10_0 () Bool)
(declare-fun v0x7f8c376e9110_0 () Real)
(declare-fun post!bb2.i.i28.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f8c376e74d0_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f8c376e7d90 () Bool)
(declare-fun F0x7f8c376eb410 () Bool)
(declare-fun v0x7f8c376e96d0_0 () Real)
(declare-fun v0x7f8c376e7210_0 () Bool)
(declare-fun F0x7f8c376eb4d0 () Bool)
(declare-fun v0x7f8c376e5010_0 () Real)
(declare-fun v0x7f8c376e7390_0 () Bool)
(declare-fun E0x7f8c376e7590 () Bool)
(declare-fun v0x7f8c376e8b50_0 () Bool)
(declare-fun v0x7f8c376e6e50_0 () Real)
(declare-fun E0x7f8c376e8710 () Bool)
(declare-fun E0x7f8c376e9dd0 () Bool)
(declare-fun v0x7f8c376e7cd0_0 () Real)
(declare-fun E0x7f8c376e9450 () Bool)
(declare-fun v0x7f8c376e5110_0 () Bool)
(declare-fun v0x7f8c376ea590_0 () Bool)
(declare-fun v0x7f8c376e8650_0 () Bool)
(declare-fun F0x7f8c376eb590 () Bool)

(assert (=> F0x7f8c376eb590
    (and v0x7f8c376e5110_0
         (<= v0x7f8c376e6e50_0 1.0)
         (>= v0x7f8c376e6e50_0 1.0)
         (<= v0x7f8c376e5010_0 0.0)
         (>= v0x7f8c376e5010_0 0.0))))
(assert (=> F0x7f8c376eb590 F0x7f8c376eb4d0))
(assert (let ((a!1 (=> v0x7f8c376e7c10_0
               (or (and v0x7f8c376e74d0_0
                        E0x7f8c376e7d90
                        (<= v0x7f8c376e7cd0_0 v0x7f8c376e7ad0_0)
                        (>= v0x7f8c376e7cd0_0 v0x7f8c376e7ad0_0))
                   (and v0x7f8c376e7210_0
                        E0x7f8c376e7f50
                        v0x7f8c376e7390_0
                        (<= v0x7f8c376e7cd0_0 v0x7f8c376e6c10_0)
                        (>= v0x7f8c376e7cd0_0 v0x7f8c376e6c10_0)))))
      (a!2 (=> v0x7f8c376e7c10_0
               (or (and E0x7f8c376e7d90 (not E0x7f8c376e7f50))
                   (and E0x7f8c376e7f50 (not E0x7f8c376e7d90)))))
      (a!3 (or (and v0x7f8c376e8650_0
                    E0x7f8c376e9850
                    (<= v0x7f8c376e96d0_0 v0x7f8c376e7cd0_0)
                    (>= v0x7f8c376e96d0_0 v0x7f8c376e7cd0_0)
                    (<= v0x7f8c376e9790_0 v0x7f8c376e8a10_0)
                    (>= v0x7f8c376e9790_0 v0x7f8c376e8a10_0))
               (and v0x7f8c376e9390_0
                    E0x7f8c376e9b10
                    (and (<= v0x7f8c376e96d0_0 v0x7f8c376e9110_0)
                         (>= v0x7f8c376e96d0_0 v0x7f8c376e9110_0))
                    (<= v0x7f8c376e9790_0 v0x7f8c376e6d90_0)
                    (>= v0x7f8c376e9790_0 v0x7f8c376e6d90_0))
               (and v0x7f8c376e8b50_0
                    E0x7f8c376e9dd0
                    (not v0x7f8c376e9250_0)
                    (and (<= v0x7f8c376e96d0_0 v0x7f8c376e9110_0)
                         (>= v0x7f8c376e96d0_0 v0x7f8c376e9110_0))
                    (<= v0x7f8c376e9790_0 0.0)
                    (>= v0x7f8c376e9790_0 0.0))))
      (a!4 (=> v0x7f8c376e9610_0
               (or (and E0x7f8c376e9850
                        (not E0x7f8c376e9b10)
                        (not E0x7f8c376e9dd0))
                   (and E0x7f8c376e9b10
                        (not E0x7f8c376e9850)
                        (not E0x7f8c376e9dd0))
                   (and E0x7f8c376e9dd0
                        (not E0x7f8c376e9850)
                        (not E0x7f8c376e9b10))))))
(let ((a!5 (and (=> v0x7f8c376e74d0_0
                    (and v0x7f8c376e7210_0
                         E0x7f8c376e7590
                         (not v0x7f8c376e7390_0)))
                (=> v0x7f8c376e74d0_0 E0x7f8c376e7590)
                a!1
                a!2
                (=> v0x7f8c376e8650_0
                    (and v0x7f8c376e7c10_0 E0x7f8c376e8710 v0x7f8c376e8510_0))
                (=> v0x7f8c376e8650_0 E0x7f8c376e8710)
                (=> v0x7f8c376e8b50_0
                    (and v0x7f8c376e7c10_0
                         E0x7f8c376e8c10
                         (not v0x7f8c376e8510_0)))
                (=> v0x7f8c376e8b50_0 E0x7f8c376e8c10)
                (=> v0x7f8c376e9390_0
                    (and v0x7f8c376e8b50_0 E0x7f8c376e9450 v0x7f8c376e9250_0))
                (=> v0x7f8c376e9390_0 E0x7f8c376e9450)
                (=> v0x7f8c376e9610_0 a!3)
                a!4
                v0x7f8c376e9610_0
                (not v0x7f8c376ea810_0)
                (<= v0x7f8c376e6e50_0 v0x7f8c376e96d0_0)
                (>= v0x7f8c376e6e50_0 v0x7f8c376e96d0_0)
                (<= v0x7f8c376e5010_0 v0x7f8c376e9790_0)
                (>= v0x7f8c376e5010_0 v0x7f8c376e9790_0)
                (= v0x7f8c376e7390_0 (= v0x7f8c376e72d0_0 0.0))
                (= v0x7f8c376e77d0_0 (< v0x7f8c376e6c10_0 2.0))
                (= v0x7f8c376e7990_0 (ite v0x7f8c376e77d0_0 1.0 0.0))
                (= v0x7f8c376e7ad0_0 (+ v0x7f8c376e7990_0 v0x7f8c376e6c10_0))
                (= v0x7f8c376e8510_0 (= v0x7f8c376e6d90_0 0.0))
                (= v0x7f8c376e88d0_0 (> v0x7f8c376e7cd0_0 1.0))
                (= v0x7f8c376e8a10_0
                   (ite v0x7f8c376e88d0_0 1.0 v0x7f8c376e6d90_0))
                (= v0x7f8c376e8e10_0 (> v0x7f8c376e7cd0_0 0.0))
                (= v0x7f8c376e8f50_0 (+ v0x7f8c376e7cd0_0 (- 1.0)))
                (= v0x7f8c376e9110_0
                   (ite v0x7f8c376e8e10_0 v0x7f8c376e8f50_0 v0x7f8c376e7cd0_0))
                (= v0x7f8c376e9250_0 (= v0x7f8c376e9110_0 0.0))
                (= v0x7f8c376ea310_0 (= v0x7f8c376e96d0_0 2.0))
                (= v0x7f8c376ea450_0 (= v0x7f8c376e9790_0 0.0))
                (= v0x7f8c376ea590_0 (or v0x7f8c376ea450_0 v0x7f8c376ea310_0))
                (= v0x7f8c376ea6d0_0 (xor v0x7f8c376ea590_0 true))
                (= v0x7f8c376ea810_0 (and v0x7f8c376e8510_0 v0x7f8c376ea6d0_0)))))
  (=> F0x7f8c376eb410 a!5))))
(assert (=> F0x7f8c376eb410 F0x7f8c376eb350))
(assert (let ((a!1 (=> v0x7f8c376e7c10_0
               (or (and v0x7f8c376e74d0_0
                        E0x7f8c376e7d90
                        (<= v0x7f8c376e7cd0_0 v0x7f8c376e7ad0_0)
                        (>= v0x7f8c376e7cd0_0 v0x7f8c376e7ad0_0))
                   (and v0x7f8c376e7210_0
                        E0x7f8c376e7f50
                        v0x7f8c376e7390_0
                        (<= v0x7f8c376e7cd0_0 v0x7f8c376e6c10_0)
                        (>= v0x7f8c376e7cd0_0 v0x7f8c376e6c10_0)))))
      (a!2 (=> v0x7f8c376e7c10_0
               (or (and E0x7f8c376e7d90 (not E0x7f8c376e7f50))
                   (and E0x7f8c376e7f50 (not E0x7f8c376e7d90)))))
      (a!3 (or (and v0x7f8c376e8650_0
                    E0x7f8c376e9850
                    (<= v0x7f8c376e96d0_0 v0x7f8c376e7cd0_0)
                    (>= v0x7f8c376e96d0_0 v0x7f8c376e7cd0_0)
                    (<= v0x7f8c376e9790_0 v0x7f8c376e8a10_0)
                    (>= v0x7f8c376e9790_0 v0x7f8c376e8a10_0))
               (and v0x7f8c376e9390_0
                    E0x7f8c376e9b10
                    (and (<= v0x7f8c376e96d0_0 v0x7f8c376e9110_0)
                         (>= v0x7f8c376e96d0_0 v0x7f8c376e9110_0))
                    (<= v0x7f8c376e9790_0 v0x7f8c376e6d90_0)
                    (>= v0x7f8c376e9790_0 v0x7f8c376e6d90_0))
               (and v0x7f8c376e8b50_0
                    E0x7f8c376e9dd0
                    (not v0x7f8c376e9250_0)
                    (and (<= v0x7f8c376e96d0_0 v0x7f8c376e9110_0)
                         (>= v0x7f8c376e96d0_0 v0x7f8c376e9110_0))
                    (<= v0x7f8c376e9790_0 0.0)
                    (>= v0x7f8c376e9790_0 0.0))))
      (a!4 (=> v0x7f8c376e9610_0
               (or (and E0x7f8c376e9850
                        (not E0x7f8c376e9b10)
                        (not E0x7f8c376e9dd0))
                   (and E0x7f8c376e9b10
                        (not E0x7f8c376e9850)
                        (not E0x7f8c376e9dd0))
                   (and E0x7f8c376e9dd0
                        (not E0x7f8c376e9850)
                        (not E0x7f8c376e9b10))))))
(let ((a!5 (and (=> v0x7f8c376e74d0_0
                    (and v0x7f8c376e7210_0
                         E0x7f8c376e7590
                         (not v0x7f8c376e7390_0)))
                (=> v0x7f8c376e74d0_0 E0x7f8c376e7590)
                a!1
                a!2
                (=> v0x7f8c376e8650_0
                    (and v0x7f8c376e7c10_0 E0x7f8c376e8710 v0x7f8c376e8510_0))
                (=> v0x7f8c376e8650_0 E0x7f8c376e8710)
                (=> v0x7f8c376e8b50_0
                    (and v0x7f8c376e7c10_0
                         E0x7f8c376e8c10
                         (not v0x7f8c376e8510_0)))
                (=> v0x7f8c376e8b50_0 E0x7f8c376e8c10)
                (=> v0x7f8c376e9390_0
                    (and v0x7f8c376e8b50_0 E0x7f8c376e9450 v0x7f8c376e9250_0))
                (=> v0x7f8c376e9390_0 E0x7f8c376e9450)
                (=> v0x7f8c376e9610_0 a!3)
                a!4
                v0x7f8c376e9610_0
                v0x7f8c376ea810_0
                (= v0x7f8c376e7390_0 (= v0x7f8c376e72d0_0 0.0))
                (= v0x7f8c376e77d0_0 (< v0x7f8c376e6c10_0 2.0))
                (= v0x7f8c376e7990_0 (ite v0x7f8c376e77d0_0 1.0 0.0))
                (= v0x7f8c376e7ad0_0 (+ v0x7f8c376e7990_0 v0x7f8c376e6c10_0))
                (= v0x7f8c376e8510_0 (= v0x7f8c376e6d90_0 0.0))
                (= v0x7f8c376e88d0_0 (> v0x7f8c376e7cd0_0 1.0))
                (= v0x7f8c376e8a10_0
                   (ite v0x7f8c376e88d0_0 1.0 v0x7f8c376e6d90_0))
                (= v0x7f8c376e8e10_0 (> v0x7f8c376e7cd0_0 0.0))
                (= v0x7f8c376e8f50_0 (+ v0x7f8c376e7cd0_0 (- 1.0)))
                (= v0x7f8c376e9110_0
                   (ite v0x7f8c376e8e10_0 v0x7f8c376e8f50_0 v0x7f8c376e7cd0_0))
                (= v0x7f8c376e9250_0 (= v0x7f8c376e9110_0 0.0))
                (= v0x7f8c376ea310_0 (= v0x7f8c376e96d0_0 2.0))
                (= v0x7f8c376ea450_0 (= v0x7f8c376e9790_0 0.0))
                (= v0x7f8c376ea590_0 (or v0x7f8c376ea450_0 v0x7f8c376ea310_0))
                (= v0x7f8c376ea6d0_0 (xor v0x7f8c376ea590_0 true))
                (= v0x7f8c376ea810_0 (and v0x7f8c376e8510_0 v0x7f8c376ea6d0_0)))))
  (=> F0x7f8c376eb290 a!5))))
(assert (=> F0x7f8c376eb290 F0x7f8c376eb350))
(assert (=> F0x7f8c376eb650 (or F0x7f8c376eb590 F0x7f8c376eb410)))
(assert (=> F0x7f8c376eb610 F0x7f8c376eb290))
(assert (=> pre!entry!0 (=> F0x7f8c376eb4d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f8c376eb350 (<= v0x7f8c376e6c10_0 2.0))))
(assert (let ((a!1 (not (or (not (>= v0x7f8c376e6d90_0 0.0))
                    (not (<= v0x7f8c376e6c10_0 1.0))
                    (not (<= v0x7f8c376e6d90_0 0.0))
                    (not (>= v0x7f8c376e6c10_0 1.0))))))
  (=> pre!bb1.i.i!1 (=> F0x7f8c376eb350 (or (>= v0x7f8c376e6c10_0 2.0) a!1)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f8c376eb650 (<= v0x7f8c376e6e50_0 2.0))))
(assert (let ((a!1 (not (or (not (>= v0x7f8c376e5010_0 0.0))
                    (not (<= v0x7f8c376e6e50_0 1.0))
                    (not (<= v0x7f8c376e5010_0 0.0))
                    (not (>= v0x7f8c376e6e50_0 1.0))))))
  (= lemma!bb1.i.i!1 (=> F0x7f8c376eb650 (or (>= v0x7f8c376e6e50_0 2.0) a!1)))))
(assert (= lemma!bb2.i.i28.i.i!0 (=> F0x7f8c376eb610 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i28.i.i!0) (not lemma!bb2.i.i28.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f8c376eb4d0)
; (error: F0x7f8c376eb610)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i28.i.i!0)
