(declare-fun post!bb1.i.i29.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f4f0763a950 () Bool)
(declare-fun v0x7f4f07639a10_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f4f07638510_0 () Bool)
(declare-fun v0x7f4f07635510_0 () Real)
(declare-fun v0x7f4f07638010_0 () Real)
(declare-fun v0x7f4f07635a10_0 () Bool)
(declare-fun v0x7f4f07637e50_0 () Real)
(declare-fun E0x7f4f07639150 () Bool)
(declare-fun v0x7f4f07634c10_0 () Real)
(declare-fun F0x7f4f0763a890 () Bool)
(declare-fun E0x7f4f07638ed0 () Bool)
(declare-fun v0x7f4f076389d0_0 () Bool)
(declare-fun E0x7f4f07639350 () Bool)
(declare-fun E0x7f4f07638810 () Bool)
(declare-fun v0x7f4f07638750_0 () Bool)
(declare-fun v0x7f4f07639b50_0 () Bool)
(declare-fun E0x7f4f07638350 () Bool)
(declare-fun v0x7f4f07636a90_0 () Bool)
(declare-fun v0x7f4f07638290_0 () Bool)
(declare-fun v0x7f4f07635bd0_0 () Real)
(declare-fun v0x7f4f07638150_0 () Bool)
(declare-fun E0x7f4f07637b10 () Bool)
(declare-fun v0x7f4f07637910_0 () Bool)
(declare-fun v0x7f4f07637d10_0 () Bool)
(declare-fun E0x7f4f07637750 () Bool)
(declare-fun v0x7f4f07638610_0 () Real)
(declare-fun v0x7f4f07634d90_0 () Real)
(declare-fun v0x7f4f07636bd0_0 () Real)
(declare-fun v0x7f4f07638b50_0 () Real)
(declare-fun v0x7f4f07636dd0_0 () Real)
(declare-fun E0x7f4f07636890 () Bool)
(declare-fun E0x7f4f07636e90 () Bool)
(declare-fun E0x7f4f07635fd0 () Bool)
(declare-fun v0x7f4f07637690_0 () Bool)
(declare-fun lemma!bb1.i.i29.i.i!0 () Bool)
(declare-fun v0x7f4f07635e50_0 () Bool)
(declare-fun v0x7f4f07636690_0 () Bool)
(declare-fun F0x7f4f0763a7d0 () Bool)
(declare-fun v0x7f4f07637550_0 () Bool)
(declare-fun v0x7f4f07638a90_0 () Real)
(declare-fun v0x7f4f07635450_0 () Bool)
(declare-fun v0x7f4f07635d10_0 () Real)
(declare-fun v0x7f4f07637a50_0 () Bool)
(declare-fun F0x7f4f0763a710 () Bool)
(declare-fun v0x7f4f076365d0_0 () Real)
(declare-fun v0x7f4f07634e90_0 () Real)
(declare-fun E0x7f4f07637050 () Bool)
(declare-fun v0x7f4f07633010_0 () Real)
(declare-fun E0x7f4f07638c10 () Bool)
(declare-fun F0x7f4f0763aa90 () Bool)
(declare-fun v0x7f4f076355d0_0 () Bool)
(declare-fun v0x7f4f076367d0_0 () Bool)
(declare-fun F0x7f4f0763aa50 () Bool)
(declare-fun v0x7f4f07635710_0 () Bool)
(declare-fun E0x7f4f07636190 () Bool)
(declare-fun v0x7f4f07639910_0 () Bool)
(declare-fun v0x7f4f07634e50_0 () Real)
(declare-fun E0x7f4f076357d0 () Bool)
(declare-fun v0x7f4f07636d10_0 () Bool)
(declare-fun v0x7f4f07634f50_0 () Real)
(declare-fun v0x7f4f07633110_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f4f07635f10_0 () Real)
(declare-fun F0x7f4f0763a650 () Bool)

(assert (=> F0x7f4f0763a650
    (and v0x7f4f07633110_0
         (<= v0x7f4f07634e50_0 0.0)
         (>= v0x7f4f07634e50_0 0.0)
         (<= v0x7f4f07634f50_0 0.0)
         (>= v0x7f4f07634f50_0 0.0)
         (<= v0x7f4f07633010_0 1.0)
         (>= v0x7f4f07633010_0 1.0))))
(assert (=> F0x7f4f0763a650 F0x7f4f0763a710))
(assert (let ((a!1 (=> v0x7f4f07635e50_0
               (or (and v0x7f4f07635710_0
                        E0x7f4f07635fd0
                        (<= v0x7f4f07635f10_0 v0x7f4f07635d10_0)
                        (>= v0x7f4f07635f10_0 v0x7f4f07635d10_0))
                   (and v0x7f4f07635450_0
                        E0x7f4f07636190
                        v0x7f4f076355d0_0
                        (<= v0x7f4f07635f10_0 v0x7f4f07634e90_0)
                        (>= v0x7f4f07635f10_0 v0x7f4f07634e90_0)))))
      (a!2 (=> v0x7f4f07635e50_0
               (or (and E0x7f4f07635fd0 (not E0x7f4f07636190))
                   (and E0x7f4f07636190 (not E0x7f4f07635fd0)))))
      (a!3 (=> v0x7f4f07636d10_0
               (or (and v0x7f4f076367d0_0
                        E0x7f4f07636e90
                        (<= v0x7f4f07636dd0_0 v0x7f4f07636bd0_0)
                        (>= v0x7f4f07636dd0_0 v0x7f4f07636bd0_0))
                   (and v0x7f4f07635e50_0
                        E0x7f4f07637050
                        v0x7f4f07636690_0
                        (<= v0x7f4f07636dd0_0 v0x7f4f07634d90_0)
                        (>= v0x7f4f07636dd0_0 v0x7f4f07634d90_0)))))
      (a!4 (=> v0x7f4f07636d10_0
               (or (and E0x7f4f07636e90 (not E0x7f4f07637050))
                   (and E0x7f4f07637050 (not E0x7f4f07636e90)))))
      (a!5 (or (and v0x7f4f07638290_0
                    E0x7f4f07638c10
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07635f10_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07635f10_0))
                    (<= v0x7f4f07638b50_0 v0x7f4f07638610_0)
                    (>= v0x7f4f07638b50_0 v0x7f4f07638610_0))
               (and v0x7f4f07637690_0
                    E0x7f4f07638ed0
                    (not v0x7f4f07637910_0)
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07635f10_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07635f10_0))
                    (and (<= v0x7f4f07638b50_0 v0x7f4f07634c10_0)
                         (>= v0x7f4f07638b50_0 v0x7f4f07634c10_0)))
               (and v0x7f4f07638750_0
                    E0x7f4f07639150
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07638010_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07638010_0))
                    (and (<= v0x7f4f07638b50_0 v0x7f4f07634c10_0)
                         (>= v0x7f4f07638b50_0 v0x7f4f07634c10_0)))
               (and v0x7f4f07637a50_0
                    E0x7f4f07639350
                    (not v0x7f4f07638150_0)
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07638010_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07638010_0))
                    (<= v0x7f4f07638b50_0 0.0)
                    (>= v0x7f4f07638b50_0 0.0))))
      (a!6 (=> v0x7f4f076389d0_0
               (or (and E0x7f4f07638c10
                        (not E0x7f4f07638ed0)
                        (not E0x7f4f07639150)
                        (not E0x7f4f07639350))
                   (and E0x7f4f07638ed0
                        (not E0x7f4f07638c10)
                        (not E0x7f4f07639150)
                        (not E0x7f4f07639350))
                   (and E0x7f4f07639150
                        (not E0x7f4f07638c10)
                        (not E0x7f4f07638ed0)
                        (not E0x7f4f07639350))
                   (and E0x7f4f07639350
                        (not E0x7f4f07638c10)
                        (not E0x7f4f07638ed0)
                        (not E0x7f4f07639150))))))
(let ((a!7 (and (=> v0x7f4f07635710_0
                    (and v0x7f4f07635450_0
                         E0x7f4f076357d0
                         (not v0x7f4f076355d0_0)))
                (=> v0x7f4f07635710_0 E0x7f4f076357d0)
                a!1
                a!2
                (=> v0x7f4f076367d0_0
                    (and v0x7f4f07635e50_0
                         E0x7f4f07636890
                         (not v0x7f4f07636690_0)))
                (=> v0x7f4f076367d0_0 E0x7f4f07636890)
                a!3
                a!4
                (=> v0x7f4f07637690_0
                    (and v0x7f4f07636d10_0 E0x7f4f07637750 v0x7f4f07637550_0))
                (=> v0x7f4f07637690_0 E0x7f4f07637750)
                (=> v0x7f4f07637a50_0
                    (and v0x7f4f07636d10_0
                         E0x7f4f07637b10
                         (not v0x7f4f07637550_0)))
                (=> v0x7f4f07637a50_0 E0x7f4f07637b10)
                (=> v0x7f4f07638290_0
                    (and v0x7f4f07637690_0 E0x7f4f07638350 v0x7f4f07637910_0))
                (=> v0x7f4f07638290_0 E0x7f4f07638350)
                (=> v0x7f4f07638750_0
                    (and v0x7f4f07637a50_0 E0x7f4f07638810 v0x7f4f07638150_0))
                (=> v0x7f4f07638750_0 E0x7f4f07638810)
                (=> v0x7f4f076389d0_0 a!5)
                a!6
                v0x7f4f076389d0_0
                v0x7f4f07639b50_0
                (<= v0x7f4f07634e50_0 v0x7f4f07638b50_0)
                (>= v0x7f4f07634e50_0 v0x7f4f07638b50_0)
                (<= v0x7f4f07634f50_0 v0x7f4f07636dd0_0)
                (>= v0x7f4f07634f50_0 v0x7f4f07636dd0_0)
                (<= v0x7f4f07633010_0 v0x7f4f07638a90_0)
                (>= v0x7f4f07633010_0 v0x7f4f07638a90_0)
                (= v0x7f4f076355d0_0 (= v0x7f4f07635510_0 0.0))
                (= v0x7f4f07635a10_0 (< v0x7f4f07634e90_0 2.0))
                (= v0x7f4f07635bd0_0 (ite v0x7f4f07635a10_0 1.0 0.0))
                (= v0x7f4f07635d10_0 (+ v0x7f4f07635bd0_0 v0x7f4f07634e90_0))
                (= v0x7f4f07636690_0 (= v0x7f4f076365d0_0 0.0))
                (= v0x7f4f07636a90_0 (= v0x7f4f07634d90_0 0.0))
                (= v0x7f4f07636bd0_0 (ite v0x7f4f07636a90_0 1.0 0.0))
                (= v0x7f4f07637550_0 (= v0x7f4f07634c10_0 0.0))
                (= v0x7f4f07637910_0 (> v0x7f4f07635f10_0 1.0))
                (= v0x7f4f07637d10_0 (> v0x7f4f07635f10_0 0.0))
                (= v0x7f4f07637e50_0 (+ v0x7f4f07635f10_0 (- 1.0)))
                (= v0x7f4f07638010_0
                   (ite v0x7f4f07637d10_0 v0x7f4f07637e50_0 v0x7f4f07635f10_0))
                (= v0x7f4f07638150_0 (= v0x7f4f07636dd0_0 0.0))
                (= v0x7f4f07638510_0 (= v0x7f4f07636dd0_0 0.0))
                (= v0x7f4f07638610_0
                   (ite v0x7f4f07638510_0 1.0 v0x7f4f07634c10_0))
                (= v0x7f4f07639910_0 (= v0x7f4f07636dd0_0 0.0))
                (= v0x7f4f07639a10_0 (= v0x7f4f07638b50_0 0.0))
                (= v0x7f4f07639b50_0 (or v0x7f4f07639a10_0 v0x7f4f07639910_0)))))
  (=> F0x7f4f0763a7d0 a!7))))
(assert (=> F0x7f4f0763a7d0 F0x7f4f0763a890))
(assert (let ((a!1 (=> v0x7f4f07635e50_0
               (or (and v0x7f4f07635710_0
                        E0x7f4f07635fd0
                        (<= v0x7f4f07635f10_0 v0x7f4f07635d10_0)
                        (>= v0x7f4f07635f10_0 v0x7f4f07635d10_0))
                   (and v0x7f4f07635450_0
                        E0x7f4f07636190
                        v0x7f4f076355d0_0
                        (<= v0x7f4f07635f10_0 v0x7f4f07634e90_0)
                        (>= v0x7f4f07635f10_0 v0x7f4f07634e90_0)))))
      (a!2 (=> v0x7f4f07635e50_0
               (or (and E0x7f4f07635fd0 (not E0x7f4f07636190))
                   (and E0x7f4f07636190 (not E0x7f4f07635fd0)))))
      (a!3 (=> v0x7f4f07636d10_0
               (or (and v0x7f4f076367d0_0
                        E0x7f4f07636e90
                        (<= v0x7f4f07636dd0_0 v0x7f4f07636bd0_0)
                        (>= v0x7f4f07636dd0_0 v0x7f4f07636bd0_0))
                   (and v0x7f4f07635e50_0
                        E0x7f4f07637050
                        v0x7f4f07636690_0
                        (<= v0x7f4f07636dd0_0 v0x7f4f07634d90_0)
                        (>= v0x7f4f07636dd0_0 v0x7f4f07634d90_0)))))
      (a!4 (=> v0x7f4f07636d10_0
               (or (and E0x7f4f07636e90 (not E0x7f4f07637050))
                   (and E0x7f4f07637050 (not E0x7f4f07636e90)))))
      (a!5 (or (and v0x7f4f07638290_0
                    E0x7f4f07638c10
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07635f10_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07635f10_0))
                    (<= v0x7f4f07638b50_0 v0x7f4f07638610_0)
                    (>= v0x7f4f07638b50_0 v0x7f4f07638610_0))
               (and v0x7f4f07637690_0
                    E0x7f4f07638ed0
                    (not v0x7f4f07637910_0)
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07635f10_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07635f10_0))
                    (and (<= v0x7f4f07638b50_0 v0x7f4f07634c10_0)
                         (>= v0x7f4f07638b50_0 v0x7f4f07634c10_0)))
               (and v0x7f4f07638750_0
                    E0x7f4f07639150
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07638010_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07638010_0))
                    (and (<= v0x7f4f07638b50_0 v0x7f4f07634c10_0)
                         (>= v0x7f4f07638b50_0 v0x7f4f07634c10_0)))
               (and v0x7f4f07637a50_0
                    E0x7f4f07639350
                    (not v0x7f4f07638150_0)
                    (and (<= v0x7f4f07638a90_0 v0x7f4f07638010_0)
                         (>= v0x7f4f07638a90_0 v0x7f4f07638010_0))
                    (<= v0x7f4f07638b50_0 0.0)
                    (>= v0x7f4f07638b50_0 0.0))))
      (a!6 (=> v0x7f4f076389d0_0
               (or (and E0x7f4f07638c10
                        (not E0x7f4f07638ed0)
                        (not E0x7f4f07639150)
                        (not E0x7f4f07639350))
                   (and E0x7f4f07638ed0
                        (not E0x7f4f07638c10)
                        (not E0x7f4f07639150)
                        (not E0x7f4f07639350))
                   (and E0x7f4f07639150
                        (not E0x7f4f07638c10)
                        (not E0x7f4f07638ed0)
                        (not E0x7f4f07639350))
                   (and E0x7f4f07639350
                        (not E0x7f4f07638c10)
                        (not E0x7f4f07638ed0)
                        (not E0x7f4f07639150))))))
(let ((a!7 (and (=> v0x7f4f07635710_0
                    (and v0x7f4f07635450_0
                         E0x7f4f076357d0
                         (not v0x7f4f076355d0_0)))
                (=> v0x7f4f07635710_0 E0x7f4f076357d0)
                a!1
                a!2
                (=> v0x7f4f076367d0_0
                    (and v0x7f4f07635e50_0
                         E0x7f4f07636890
                         (not v0x7f4f07636690_0)))
                (=> v0x7f4f076367d0_0 E0x7f4f07636890)
                a!3
                a!4
                (=> v0x7f4f07637690_0
                    (and v0x7f4f07636d10_0 E0x7f4f07637750 v0x7f4f07637550_0))
                (=> v0x7f4f07637690_0 E0x7f4f07637750)
                (=> v0x7f4f07637a50_0
                    (and v0x7f4f07636d10_0
                         E0x7f4f07637b10
                         (not v0x7f4f07637550_0)))
                (=> v0x7f4f07637a50_0 E0x7f4f07637b10)
                (=> v0x7f4f07638290_0
                    (and v0x7f4f07637690_0 E0x7f4f07638350 v0x7f4f07637910_0))
                (=> v0x7f4f07638290_0 E0x7f4f07638350)
                (=> v0x7f4f07638750_0
                    (and v0x7f4f07637a50_0 E0x7f4f07638810 v0x7f4f07638150_0))
                (=> v0x7f4f07638750_0 E0x7f4f07638810)
                (=> v0x7f4f076389d0_0 a!5)
                a!6
                v0x7f4f076389d0_0
                (not v0x7f4f07639b50_0)
                (= v0x7f4f076355d0_0 (= v0x7f4f07635510_0 0.0))
                (= v0x7f4f07635a10_0 (< v0x7f4f07634e90_0 2.0))
                (= v0x7f4f07635bd0_0 (ite v0x7f4f07635a10_0 1.0 0.0))
                (= v0x7f4f07635d10_0 (+ v0x7f4f07635bd0_0 v0x7f4f07634e90_0))
                (= v0x7f4f07636690_0 (= v0x7f4f076365d0_0 0.0))
                (= v0x7f4f07636a90_0 (= v0x7f4f07634d90_0 0.0))
                (= v0x7f4f07636bd0_0 (ite v0x7f4f07636a90_0 1.0 0.0))
                (= v0x7f4f07637550_0 (= v0x7f4f07634c10_0 0.0))
                (= v0x7f4f07637910_0 (> v0x7f4f07635f10_0 1.0))
                (= v0x7f4f07637d10_0 (> v0x7f4f07635f10_0 0.0))
                (= v0x7f4f07637e50_0 (+ v0x7f4f07635f10_0 (- 1.0)))
                (= v0x7f4f07638010_0
                   (ite v0x7f4f07637d10_0 v0x7f4f07637e50_0 v0x7f4f07635f10_0))
                (= v0x7f4f07638150_0 (= v0x7f4f07636dd0_0 0.0))
                (= v0x7f4f07638510_0 (= v0x7f4f07636dd0_0 0.0))
                (= v0x7f4f07638610_0
                   (ite v0x7f4f07638510_0 1.0 v0x7f4f07634c10_0))
                (= v0x7f4f07639910_0 (= v0x7f4f07636dd0_0 0.0))
                (= v0x7f4f07639a10_0 (= v0x7f4f07638b50_0 0.0))
                (= v0x7f4f07639b50_0 (or v0x7f4f07639a10_0 v0x7f4f07639910_0)))))
  (=> F0x7f4f0763a950 a!7))))
(assert (=> F0x7f4f0763a950 F0x7f4f0763a890))
(assert (=> F0x7f4f0763aa90 (or F0x7f4f0763a650 F0x7f4f0763a7d0)))
(assert (=> F0x7f4f0763aa50 F0x7f4f0763a950))
(assert (=> pre!entry!0 (=> F0x7f4f0763a710 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f4f0763a890 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f4f0763aa90 true)))
(assert (= lemma!bb1.i.i29.i.i!0 (=> F0x7f4f0763aa50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i29.i.i!0) (not lemma!bb1.i.i29.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f4f0763a710)
; (error: F0x7f4f0763aa50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i29.i.i!0)
