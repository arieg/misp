(declare-fun post!bb2.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i43.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f4f117f38d0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun F0x7f4f117f3910 () Bool)
(declare-fun F0x7f4f117f37d0 () Bool)
(declare-fun F0x7f4f117f3610 () Bool)
(declare-fun v0x7f4f117f2890_0 () Bool)
(declare-fun v0x7f4f117f2750_0 () Bool)
(declare-fun v0x7f4f117f0c10_0 () Bool)
(declare-fun v0x7f4f117f0ad0_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f4f117ef410_0 () Bool)
(declare-fun v0x7f4f117eef50_0 () Real)
(declare-fun v0x7f4f117ee550_0 () Real)
(declare-fun v0x7f4f117f29d0_0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun E0x7f4f117f1f50 () Bool)
(declare-fun E0x7f4f117f1d50 () Bool)
(declare-fun v0x7f4f117ede90_0 () Real)
(declare-fun v0x7f4f117ed810_0 () Real)
(declare-fun E0x7f4f117f1ad0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f4f117f1210_0 () Real)
(declare-fun v0x7f4f117f2510_0 () Bool)
(declare-fun v0x7f4f117f1750_0 () Real)
(declare-fun v0x7f4f117f1690_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun E0x7f4f117f1810 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7f4f117f15d0_0 () Bool)
(declare-fun E0x7f4f117f1410 () Bool)
(declare-fun v0x7f4f117f1350_0 () Bool)
(declare-fun E0x7f4f117f0f50 () Bool)
(declare-fun v0x7f4f117f0690_0 () Bool)
(declare-fun v0x7f4f117f07d0_0 () Real)
(declare-fun v0x7f4f117f0e90_0 () Bool)
(declare-fun E0x7f4f117f0490 () Bool)
(declare-fun v0x7f4f117f0990_0 () Real)
(declare-fun v0x7f4f117f03d0_0 () Bool)
(declare-fun v0x7f4f117f0010_0 () Bool)
(declare-fun v0x7f4f117efed0_0 () Bool)
(declare-fun E0x7f4f117f00d0 () Bool)
(declare-fun v0x7f4f117ef550_0 () Real)
(declare-fun v0x7f4f117f2610_0 () Bool)
(declare-fun v0x7f4f117ef010_0 () Bool)
(declare-fun v0x7f4f117ef750_0 () Real)
(declare-fun v0x7f4f117f0290_0 () Bool)
(declare-fun v0x7f4f117ef690_0 () Bool)
(declare-fun v0x7f4f117ef150_0 () Bool)
(declare-fun v0x7f4f117ed710_0 () Real)
(declare-fun E0x7f4f117ef210 () Bool)
(declare-fun v0x7f4f117ee690_0 () Real)
(declare-fun E0x7f4f117ef810 () Bool)
(declare-fun v0x7f4f117ee390_0 () Bool)
(declare-fun v0x7f4f117ee7d0_0 () Bool)
(declare-fun v0x7f4f117edf50_0 () Bool)
(declare-fun E0x7f4f117ef9d0 () Bool)
(declare-fun E0x7f4f117ee150 () Bool)
(declare-fun v0x7f4f117eddd0_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f4f117ee090_0 () Bool)
(declare-fun F0x7f4f117f3550 () Bool)
(declare-fun v0x7f4f117ee890_0 () Real)
(declare-fun v0x7f4f117ed590_0 () Real)
(declare-fun E0x7f4f117eeb10 () Bool)
(declare-fun E0x7f4f117ee950 () Bool)
(declare-fun v0x7f4f117f1110_0 () Bool)
(declare-fun F0x7f4f117f3650 () Bool)
(declare-fun v0x7f4f117f0d50_0 () Bool)
(declare-fun v0x7f4f117ed8d0_0 () Real)
(declare-fun v0x7f4f117ea010_0 () Real)
(declare-fun v0x7f4f117ed7d0_0 () Real)
(declare-fun v0x7f4f117ea110_0 () Bool)
(declare-fun F0x7f4f117f3710 () Bool)

(assert (=> F0x7f4f117f3710
    (and v0x7f4f117ea110_0
         (<= v0x7f4f117ed7d0_0 0.0)
         (>= v0x7f4f117ed7d0_0 0.0)
         (<= v0x7f4f117ed8d0_0 1.0)
         (>= v0x7f4f117ed8d0_0 1.0)
         (<= v0x7f4f117ea010_0 0.0)
         (>= v0x7f4f117ea010_0 0.0))))
(assert (=> F0x7f4f117f3710 F0x7f4f117f3650))
(assert (let ((a!1 (=> v0x7f4f117ee7d0_0
               (or (and v0x7f4f117ee090_0
                        E0x7f4f117ee950
                        (<= v0x7f4f117ee890_0 v0x7f4f117ee690_0)
                        (>= v0x7f4f117ee890_0 v0x7f4f117ee690_0))
                   (and v0x7f4f117eddd0_0
                        E0x7f4f117eeb10
                        v0x7f4f117edf50_0
                        (<= v0x7f4f117ee890_0 v0x7f4f117ed710_0)
                        (>= v0x7f4f117ee890_0 v0x7f4f117ed710_0)))))
      (a!2 (=> v0x7f4f117ee7d0_0
               (or (and E0x7f4f117ee950 (not E0x7f4f117eeb10))
                   (and E0x7f4f117eeb10 (not E0x7f4f117ee950)))))
      (a!3 (=> v0x7f4f117ef690_0
               (or (and v0x7f4f117ef150_0
                        E0x7f4f117ef810
                        (<= v0x7f4f117ef750_0 v0x7f4f117ef550_0)
                        (>= v0x7f4f117ef750_0 v0x7f4f117ef550_0))
                   (and v0x7f4f117ee7d0_0
                        E0x7f4f117ef9d0
                        v0x7f4f117ef010_0
                        (<= v0x7f4f117ef750_0 v0x7f4f117ed590_0)
                        (>= v0x7f4f117ef750_0 v0x7f4f117ed590_0)))))
      (a!4 (=> v0x7f4f117ef690_0
               (or (and E0x7f4f117ef810 (not E0x7f4f117ef9d0))
                   (and E0x7f4f117ef9d0 (not E0x7f4f117ef810)))))
      (a!5 (or (and v0x7f4f117f0e90_0
                    E0x7f4f117f1810
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117ee890_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117ee890_0))
                    (<= v0x7f4f117f1750_0 v0x7f4f117f1210_0)
                    (>= v0x7f4f117f1750_0 v0x7f4f117f1210_0))
               (and v0x7f4f117f0010_0
                    E0x7f4f117f1ad0
                    (not v0x7f4f117f0290_0)
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117ee890_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117ee890_0))
                    (and (<= v0x7f4f117f1750_0 v0x7f4f117ed810_0)
                         (>= v0x7f4f117f1750_0 v0x7f4f117ed810_0)))
               (and v0x7f4f117f1350_0
                    E0x7f4f117f1d50
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117f0990_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117f0990_0))
                    (and (<= v0x7f4f117f1750_0 v0x7f4f117ed810_0)
                         (>= v0x7f4f117f1750_0 v0x7f4f117ed810_0)))
               (and v0x7f4f117f03d0_0
                    E0x7f4f117f1f50
                    (not v0x7f4f117f0d50_0)
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117f0990_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117f0990_0))
                    (<= v0x7f4f117f1750_0 0.0)
                    (>= v0x7f4f117f1750_0 0.0))))
      (a!6 (=> v0x7f4f117f15d0_0
               (or (and E0x7f4f117f1810
                        (not E0x7f4f117f1ad0)
                        (not E0x7f4f117f1d50)
                        (not E0x7f4f117f1f50))
                   (and E0x7f4f117f1ad0
                        (not E0x7f4f117f1810)
                        (not E0x7f4f117f1d50)
                        (not E0x7f4f117f1f50))
                   (and E0x7f4f117f1d50
                        (not E0x7f4f117f1810)
                        (not E0x7f4f117f1ad0)
                        (not E0x7f4f117f1f50))
                   (and E0x7f4f117f1f50
                        (not E0x7f4f117f1810)
                        (not E0x7f4f117f1ad0)
                        (not E0x7f4f117f1d50))))))
(let ((a!7 (and (=> v0x7f4f117ee090_0
                    (and v0x7f4f117eddd0_0
                         E0x7f4f117ee150
                         (not v0x7f4f117edf50_0)))
                (=> v0x7f4f117ee090_0 E0x7f4f117ee150)
                a!1
                a!2
                (=> v0x7f4f117ef150_0
                    (and v0x7f4f117ee7d0_0
                         E0x7f4f117ef210
                         (not v0x7f4f117ef010_0)))
                (=> v0x7f4f117ef150_0 E0x7f4f117ef210)
                a!3
                a!4
                (=> v0x7f4f117f0010_0
                    (and v0x7f4f117ef690_0 E0x7f4f117f00d0 v0x7f4f117efed0_0))
                (=> v0x7f4f117f0010_0 E0x7f4f117f00d0)
                (=> v0x7f4f117f03d0_0
                    (and v0x7f4f117ef690_0
                         E0x7f4f117f0490
                         (not v0x7f4f117efed0_0)))
                (=> v0x7f4f117f03d0_0 E0x7f4f117f0490)
                (=> v0x7f4f117f0e90_0
                    (and v0x7f4f117f0010_0 E0x7f4f117f0f50 v0x7f4f117f0290_0))
                (=> v0x7f4f117f0e90_0 E0x7f4f117f0f50)
                (=> v0x7f4f117f1350_0
                    (and v0x7f4f117f03d0_0 E0x7f4f117f1410 v0x7f4f117f0d50_0))
                (=> v0x7f4f117f1350_0 E0x7f4f117f1410)
                (=> v0x7f4f117f15d0_0 a!5)
                a!6
                v0x7f4f117f15d0_0
                (not v0x7f4f117f29d0_0)
                (<= v0x7f4f117ed7d0_0 v0x7f4f117ef750_0)
                (>= v0x7f4f117ed7d0_0 v0x7f4f117ef750_0)
                (<= v0x7f4f117ed8d0_0 v0x7f4f117f1690_0)
                (>= v0x7f4f117ed8d0_0 v0x7f4f117f1690_0)
                (<= v0x7f4f117ea010_0 v0x7f4f117f1750_0)
                (>= v0x7f4f117ea010_0 v0x7f4f117f1750_0)
                (= v0x7f4f117edf50_0 (= v0x7f4f117ede90_0 0.0))
                (= v0x7f4f117ee390_0 (< v0x7f4f117ed710_0 2.0))
                (= v0x7f4f117ee550_0 (ite v0x7f4f117ee390_0 1.0 0.0))
                (= v0x7f4f117ee690_0 (+ v0x7f4f117ee550_0 v0x7f4f117ed710_0))
                (= v0x7f4f117ef010_0 (= v0x7f4f117eef50_0 0.0))
                (= v0x7f4f117ef410_0 (= v0x7f4f117ed590_0 0.0))
                (= v0x7f4f117ef550_0 (ite v0x7f4f117ef410_0 1.0 0.0))
                (= v0x7f4f117efed0_0 (= v0x7f4f117ed810_0 0.0))
                (= v0x7f4f117f0290_0 (> v0x7f4f117ee890_0 1.0))
                (= v0x7f4f117f0690_0 (> v0x7f4f117ee890_0 0.0))
                (= v0x7f4f117f07d0_0 (+ v0x7f4f117ee890_0 (- 1.0)))
                (= v0x7f4f117f0990_0
                   (ite v0x7f4f117f0690_0 v0x7f4f117f07d0_0 v0x7f4f117ee890_0))
                (= v0x7f4f117f0ad0_0 (= v0x7f4f117ef750_0 0.0))
                (= v0x7f4f117f0c10_0 (= v0x7f4f117f0990_0 0.0))
                (= v0x7f4f117f0d50_0 (and v0x7f4f117f0ad0_0 v0x7f4f117f0c10_0))
                (= v0x7f4f117f1110_0 (= v0x7f4f117ef750_0 0.0))
                (= v0x7f4f117f1210_0
                   (ite v0x7f4f117f1110_0 1.0 v0x7f4f117ed810_0))
                (= v0x7f4f117f2510_0 (= v0x7f4f117ef750_0 0.0))
                (= v0x7f4f117f2610_0 (= v0x7f4f117f1690_0 2.0))
                (= v0x7f4f117f2750_0 (= v0x7f4f117f1750_0 0.0))
                (= v0x7f4f117f2890_0 (and v0x7f4f117f2610_0 v0x7f4f117f2510_0))
                (= v0x7f4f117f29d0_0 (and v0x7f4f117f2890_0 v0x7f4f117f2750_0)))))
  (=> F0x7f4f117f3550 a!7))))
(assert (=> F0x7f4f117f3550 F0x7f4f117f3610))
(assert (let ((a!1 (=> v0x7f4f117ee7d0_0
               (or (and v0x7f4f117ee090_0
                        E0x7f4f117ee950
                        (<= v0x7f4f117ee890_0 v0x7f4f117ee690_0)
                        (>= v0x7f4f117ee890_0 v0x7f4f117ee690_0))
                   (and v0x7f4f117eddd0_0
                        E0x7f4f117eeb10
                        v0x7f4f117edf50_0
                        (<= v0x7f4f117ee890_0 v0x7f4f117ed710_0)
                        (>= v0x7f4f117ee890_0 v0x7f4f117ed710_0)))))
      (a!2 (=> v0x7f4f117ee7d0_0
               (or (and E0x7f4f117ee950 (not E0x7f4f117eeb10))
                   (and E0x7f4f117eeb10 (not E0x7f4f117ee950)))))
      (a!3 (=> v0x7f4f117ef690_0
               (or (and v0x7f4f117ef150_0
                        E0x7f4f117ef810
                        (<= v0x7f4f117ef750_0 v0x7f4f117ef550_0)
                        (>= v0x7f4f117ef750_0 v0x7f4f117ef550_0))
                   (and v0x7f4f117ee7d0_0
                        E0x7f4f117ef9d0
                        v0x7f4f117ef010_0
                        (<= v0x7f4f117ef750_0 v0x7f4f117ed590_0)
                        (>= v0x7f4f117ef750_0 v0x7f4f117ed590_0)))))
      (a!4 (=> v0x7f4f117ef690_0
               (or (and E0x7f4f117ef810 (not E0x7f4f117ef9d0))
                   (and E0x7f4f117ef9d0 (not E0x7f4f117ef810)))))
      (a!5 (or (and v0x7f4f117f0e90_0
                    E0x7f4f117f1810
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117ee890_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117ee890_0))
                    (<= v0x7f4f117f1750_0 v0x7f4f117f1210_0)
                    (>= v0x7f4f117f1750_0 v0x7f4f117f1210_0))
               (and v0x7f4f117f0010_0
                    E0x7f4f117f1ad0
                    (not v0x7f4f117f0290_0)
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117ee890_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117ee890_0))
                    (and (<= v0x7f4f117f1750_0 v0x7f4f117ed810_0)
                         (>= v0x7f4f117f1750_0 v0x7f4f117ed810_0)))
               (and v0x7f4f117f1350_0
                    E0x7f4f117f1d50
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117f0990_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117f0990_0))
                    (and (<= v0x7f4f117f1750_0 v0x7f4f117ed810_0)
                         (>= v0x7f4f117f1750_0 v0x7f4f117ed810_0)))
               (and v0x7f4f117f03d0_0
                    E0x7f4f117f1f50
                    (not v0x7f4f117f0d50_0)
                    (and (<= v0x7f4f117f1690_0 v0x7f4f117f0990_0)
                         (>= v0x7f4f117f1690_0 v0x7f4f117f0990_0))
                    (<= v0x7f4f117f1750_0 0.0)
                    (>= v0x7f4f117f1750_0 0.0))))
      (a!6 (=> v0x7f4f117f15d0_0
               (or (and E0x7f4f117f1810
                        (not E0x7f4f117f1ad0)
                        (not E0x7f4f117f1d50)
                        (not E0x7f4f117f1f50))
                   (and E0x7f4f117f1ad0
                        (not E0x7f4f117f1810)
                        (not E0x7f4f117f1d50)
                        (not E0x7f4f117f1f50))
                   (and E0x7f4f117f1d50
                        (not E0x7f4f117f1810)
                        (not E0x7f4f117f1ad0)
                        (not E0x7f4f117f1f50))
                   (and E0x7f4f117f1f50
                        (not E0x7f4f117f1810)
                        (not E0x7f4f117f1ad0)
                        (not E0x7f4f117f1d50))))))
(let ((a!7 (and (=> v0x7f4f117ee090_0
                    (and v0x7f4f117eddd0_0
                         E0x7f4f117ee150
                         (not v0x7f4f117edf50_0)))
                (=> v0x7f4f117ee090_0 E0x7f4f117ee150)
                a!1
                a!2
                (=> v0x7f4f117ef150_0
                    (and v0x7f4f117ee7d0_0
                         E0x7f4f117ef210
                         (not v0x7f4f117ef010_0)))
                (=> v0x7f4f117ef150_0 E0x7f4f117ef210)
                a!3
                a!4
                (=> v0x7f4f117f0010_0
                    (and v0x7f4f117ef690_0 E0x7f4f117f00d0 v0x7f4f117efed0_0))
                (=> v0x7f4f117f0010_0 E0x7f4f117f00d0)
                (=> v0x7f4f117f03d0_0
                    (and v0x7f4f117ef690_0
                         E0x7f4f117f0490
                         (not v0x7f4f117efed0_0)))
                (=> v0x7f4f117f03d0_0 E0x7f4f117f0490)
                (=> v0x7f4f117f0e90_0
                    (and v0x7f4f117f0010_0 E0x7f4f117f0f50 v0x7f4f117f0290_0))
                (=> v0x7f4f117f0e90_0 E0x7f4f117f0f50)
                (=> v0x7f4f117f1350_0
                    (and v0x7f4f117f03d0_0 E0x7f4f117f1410 v0x7f4f117f0d50_0))
                (=> v0x7f4f117f1350_0 E0x7f4f117f1410)
                (=> v0x7f4f117f15d0_0 a!5)
                a!6
                v0x7f4f117f15d0_0
                v0x7f4f117f29d0_0
                (= v0x7f4f117edf50_0 (= v0x7f4f117ede90_0 0.0))
                (= v0x7f4f117ee390_0 (< v0x7f4f117ed710_0 2.0))
                (= v0x7f4f117ee550_0 (ite v0x7f4f117ee390_0 1.0 0.0))
                (= v0x7f4f117ee690_0 (+ v0x7f4f117ee550_0 v0x7f4f117ed710_0))
                (= v0x7f4f117ef010_0 (= v0x7f4f117eef50_0 0.0))
                (= v0x7f4f117ef410_0 (= v0x7f4f117ed590_0 0.0))
                (= v0x7f4f117ef550_0 (ite v0x7f4f117ef410_0 1.0 0.0))
                (= v0x7f4f117efed0_0 (= v0x7f4f117ed810_0 0.0))
                (= v0x7f4f117f0290_0 (> v0x7f4f117ee890_0 1.0))
                (= v0x7f4f117f0690_0 (> v0x7f4f117ee890_0 0.0))
                (= v0x7f4f117f07d0_0 (+ v0x7f4f117ee890_0 (- 1.0)))
                (= v0x7f4f117f0990_0
                   (ite v0x7f4f117f0690_0 v0x7f4f117f07d0_0 v0x7f4f117ee890_0))
                (= v0x7f4f117f0ad0_0 (= v0x7f4f117ef750_0 0.0))
                (= v0x7f4f117f0c10_0 (= v0x7f4f117f0990_0 0.0))
                (= v0x7f4f117f0d50_0 (and v0x7f4f117f0ad0_0 v0x7f4f117f0c10_0))
                (= v0x7f4f117f1110_0 (= v0x7f4f117ef750_0 0.0))
                (= v0x7f4f117f1210_0
                   (ite v0x7f4f117f1110_0 1.0 v0x7f4f117ed810_0))
                (= v0x7f4f117f2510_0 (= v0x7f4f117ef750_0 0.0))
                (= v0x7f4f117f2610_0 (= v0x7f4f117f1690_0 2.0))
                (= v0x7f4f117f2750_0 (= v0x7f4f117f1750_0 0.0))
                (= v0x7f4f117f2890_0 (and v0x7f4f117f2610_0 v0x7f4f117f2510_0))
                (= v0x7f4f117f29d0_0 (and v0x7f4f117f2890_0 v0x7f4f117f2750_0)))))
  (=> F0x7f4f117f37d0 a!7))))
(assert (=> F0x7f4f117f37d0 F0x7f4f117f3610))
(assert (=> F0x7f4f117f3910 (or F0x7f4f117f3710 F0x7f4f117f3550)))
(assert (=> F0x7f4f117f38d0 F0x7f4f117f37d0))
(assert (=> pre!entry!0 (=> F0x7f4f117f3650 true)))
(assert (=> pre!bb1.i.i!0
    (=> F0x7f4f117f3610
        (or (<= v0x7f4f117ed590_0 0.0) (<= v0x7f4f117ed810_0 0.0)))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f4f117f3610 (>= v0x7f4f117ed810_0 0.0))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f4f117f3610 (not (<= 3.0 v0x7f4f117ed710_0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f4f117f3610 (>= v0x7f4f117ed590_0 0.0))))
(assert (= lemma!bb1.i.i!0
   (=> F0x7f4f117f3910
       (or (<= v0x7f4f117ed7d0_0 0.0) (<= v0x7f4f117ea010_0 0.0)))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f4f117f3910 (>= v0x7f4f117ea010_0 0.0))))
(assert (= lemma!bb1.i.i!2 (=> F0x7f4f117f3910 (not (<= 3.0 v0x7f4f117ed8d0_0)))))
(assert (= lemma!bb1.i.i!3 (=> F0x7f4f117f3910 (>= v0x7f4f117ed7d0_0 0.0))))
(assert (= lemma!bb2.i.i43.i.i!0 (=> F0x7f4f117f38d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb2.i.i43.i.i!0) (not lemma!bb2.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7f4f117f3650)
; (error: F0x7f4f117f38d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i43.i.i!0)
