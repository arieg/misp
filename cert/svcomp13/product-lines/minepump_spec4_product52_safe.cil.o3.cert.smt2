(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f3ac45cd8d0 () Bool)
(declare-fun F0x7f3ac45cd910 () Bool)
(declare-fun F0x7f3ac45cd590 () Bool)
(declare-fun F0x7f3ac45cd650 () Bool)
(declare-fun v0x7f3ac45cc890_0 () Bool)
(declare-fun v0x7f3ac45cc750_0 () Bool)
(declare-fun v0x7f3ac45ca9d0_0 () Bool)
(declare-fun v0x7f3ac45c9790_0 () Bool)
(declare-fun v0x7f3ac45c8990_0 () Real)
(declare-fun v0x7f3ac45c8510_0 () Real)
(declare-fun v0x7f3ac45c7890_0 () Bool)
(declare-fun post!bb1.i.i26.i.i!0 () Bool)
(declare-fun v0x7f3ac45c7390_0 () Real)
(declare-fun E0x7f3ac45cc010 () Bool)
(declare-fun v0x7f3ac45c6c10_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7f3ac45cab10_0 () Real)
(declare-fun v0x7f3ac45cb290_0 () Bool)
(declare-fun v0x7f3ac45caed0_0 () Bool)
(declare-fun v0x7f3ac45cb010_0 () Bool)
(declare-fun E0x7f3ac45cad10 () Bool)
(declare-fun v0x7f3ac45c98d0_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7f3ac45cac50_0 () Bool)
(declare-fun E0x7f3ac45cb950 () Bool)
(declare-fun E0x7f3ac45ca810 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f3ac45ca350_0 () Bool)
(declare-fun E0x7f3ac45ca550 () Bool)
(declare-fun E0x7f3ac45cbe50 () Bool)
(declare-fun v0x7f3ac45ca490_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f3ac45c9a90_0 () Real)
(declare-fun v0x7f3ac45ca750_0 () Bool)
(declare-fun v0x7f3ac45cc9d0_0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun v0x7f3ac45c9c90_0 () Real)
(declare-fun E0x7f3ac45c9d50 () Bool)
(declare-fun v0x7f3ac45cb4d0_0 () Real)
(declare-fun v0x7f3ac45c6a90_0 () Real)
(declare-fun v0x7f3ac45c8c50_0 () Real)
(declare-fun v0x7f3ac45c7a50_0 () Real)
(declare-fun v0x7f3ac45c8a50_0 () Bool)
(declare-fun v0x7f3ac45c8b90_0 () Bool)
(declare-fun lemma!bb1.i.i26.i.i!0 () Bool)
(declare-fun v0x7f3ac45c85d0_0 () Bool)
(declare-fun E0x7f3ac45cb0d0 () Bool)
(declare-fun E0x7f3ac45c87d0 () Bool)
(declare-fun v0x7f3ac45c6d10_0 () Real)
(declare-fun E0x7f3ac45cbb90 () Bool)
(declare-fun v0x7f3ac45c8710_0 () Bool)
(declare-fun v0x7f3ac45c9bd0_0 () Bool)
(declare-fun v0x7f3ac45c7b90_0 () Real)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f3ac45c7d90_0 () Real)
(declare-fun v0x7f3ac45c9390_0 () Bool)
(declare-fun v0x7f3ac45c7cd0_0 () Bool)
(declare-fun E0x7f3ac45c7650 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f3ac45c94d0_0 () Bool)
(declare-fun E0x7f3ac45c8d10 () Bool)
(declare-fun v0x7f3ac45c72d0_0 () Bool)
(declare-fun E0x7f3ac45cb590 () Bool)
(declare-fun E0x7f3ac45c8f10 () Bool)
(declare-fun F0x7f3ac45cd710 () Bool)
(declare-fun v0x7f3ac45c5010_0 () Real)
(declare-fun v0x7f3ac45c7450_0 () Bool)
(declare-fun E0x7f3ac45c9f10 () Bool)
(declare-fun v0x7f3ac45cb350_0 () Real)
(declare-fun E0x7f3ac45c7e50 () Bool)
(declare-fun v0x7f3ac45c6dd0_0 () Real)
(declare-fun F0x7f3ac45cd510 () Bool)
(declare-fun E0x7f3ac45c9590 () Bool)
(declare-fun v0x7f3ac45cb410_0 () Real)
(declare-fun v0x7f3ac45c6cd0_0 () Real)
(declare-fun E0x7f3ac45c8010 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun v0x7f3ac45c5110_0 () Bool)
(declare-fun v0x7f3ac45c7590_0 () Bool)
(declare-fun F0x7f3ac45cd850 () Bool)

(assert (=> F0x7f3ac45cd850
    (and v0x7f3ac45c5110_0
         (<= v0x7f3ac45c6cd0_0 1.0)
         (>= v0x7f3ac45c6cd0_0 1.0)
         (<= v0x7f3ac45c6dd0_0 0.0)
         (>= v0x7f3ac45c6dd0_0 0.0)
         (<= v0x7f3ac45c5010_0 1.0)
         (>= v0x7f3ac45c5010_0 1.0))))
(assert (=> F0x7f3ac45cd850 F0x7f3ac45cd510))
(assert (let ((a!1 (=> v0x7f3ac45c7cd0_0
               (or (and v0x7f3ac45c7590_0
                        E0x7f3ac45c7e50
                        (<= v0x7f3ac45c7d90_0 v0x7f3ac45c7b90_0)
                        (>= v0x7f3ac45c7d90_0 v0x7f3ac45c7b90_0))
                   (and v0x7f3ac45c72d0_0
                        E0x7f3ac45c8010
                        v0x7f3ac45c7450_0
                        (<= v0x7f3ac45c7d90_0 v0x7f3ac45c6d10_0)
                        (>= v0x7f3ac45c7d90_0 v0x7f3ac45c6d10_0)))))
      (a!2 (=> v0x7f3ac45c7cd0_0
               (or (and E0x7f3ac45c7e50 (not E0x7f3ac45c8010))
                   (and E0x7f3ac45c8010 (not E0x7f3ac45c7e50)))))
      (a!3 (=> v0x7f3ac45c8b90_0
               (or (and v0x7f3ac45c8710_0
                        E0x7f3ac45c8d10
                        v0x7f3ac45c8a50_0
                        (<= v0x7f3ac45c8c50_0 v0x7f3ac45c6a90_0)
                        (>= v0x7f3ac45c8c50_0 v0x7f3ac45c6a90_0))
                   (and v0x7f3ac45c7cd0_0
                        E0x7f3ac45c8f10
                        (not v0x7f3ac45c85d0_0)
                        (<= v0x7f3ac45c8c50_0 1.0)
                        (>= v0x7f3ac45c8c50_0 1.0)))))
      (a!4 (=> v0x7f3ac45c8b90_0
               (or (and E0x7f3ac45c8d10 (not E0x7f3ac45c8f10))
                   (and E0x7f3ac45c8f10 (not E0x7f3ac45c8d10)))))
      (a!5 (=> v0x7f3ac45c9bd0_0
               (or (and v0x7f3ac45c94d0_0
                        E0x7f3ac45c9d50
                        (<= v0x7f3ac45c9c90_0 v0x7f3ac45c9a90_0)
                        (>= v0x7f3ac45c9c90_0 v0x7f3ac45c9a90_0))
                   (and v0x7f3ac45c8b90_0
                        E0x7f3ac45c9f10
                        v0x7f3ac45c9390_0
                        (<= v0x7f3ac45c9c90_0 v0x7f3ac45c7d90_0)
                        (>= v0x7f3ac45c9c90_0 v0x7f3ac45c7d90_0)))))
      (a!6 (=> v0x7f3ac45c9bd0_0
               (or (and E0x7f3ac45c9d50 (not E0x7f3ac45c9f10))
                   (and E0x7f3ac45c9f10 (not E0x7f3ac45c9d50)))))
      (a!7 (or (and v0x7f3ac45ca750_0
                    E0x7f3ac45cb590
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (<= v0x7f3ac45cb4d0_0 v0x7f3ac45cab10_0)
                    (>= v0x7f3ac45cb4d0_0 v0x7f3ac45cab10_0))
               (and v0x7f3ac45cb010_0
                    E0x7f3ac45cb950
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (and (<= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)
                         (>= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)))
               (and v0x7f3ac45cac50_0
                    E0x7f3ac45cbb90
                    (not v0x7f3ac45caed0_0)
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (and (<= v0x7f3ac45cb4d0_0 0.0) (>= v0x7f3ac45cb4d0_0 0.0)))
               (and v0x7f3ac45c9bd0_0
                    E0x7f3ac45cbe50
                    v0x7f3ac45ca350_0
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (and (<= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)
                         (>= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)))
               (and v0x7f3ac45c8710_0
                    E0x7f3ac45cc010
                    (not v0x7f3ac45c8a50_0)
                    (<= v0x7f3ac45cb350_0 0.0)
                    (>= v0x7f3ac45cb350_0 0.0)
                    (<= v0x7f3ac45cb410_0 v0x7f3ac45c7d90_0)
                    (>= v0x7f3ac45cb410_0 v0x7f3ac45c7d90_0)
                    (and (<= v0x7f3ac45cb4d0_0 0.0) (>= v0x7f3ac45cb4d0_0 0.0)))))
      (a!8 (=> v0x7f3ac45cb290_0
               (or (and E0x7f3ac45cb590
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cbe50)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cb950
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cbe50)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cbb90
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbe50)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cbe50
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cc010
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cbe50))))))
(let ((a!9 (and (=> v0x7f3ac45c7590_0
                    (and v0x7f3ac45c72d0_0
                         E0x7f3ac45c7650
                         (not v0x7f3ac45c7450_0)))
                (=> v0x7f3ac45c7590_0 E0x7f3ac45c7650)
                a!1
                a!2
                (=> v0x7f3ac45c8710_0
                    (and v0x7f3ac45c7cd0_0 E0x7f3ac45c87d0 v0x7f3ac45c85d0_0))
                (=> v0x7f3ac45c8710_0 E0x7f3ac45c87d0)
                a!3
                a!4
                (=> v0x7f3ac45c94d0_0
                    (and v0x7f3ac45c8b90_0
                         E0x7f3ac45c9590
                         (not v0x7f3ac45c9390_0)))
                (=> v0x7f3ac45c94d0_0 E0x7f3ac45c9590)
                a!5
                a!6
                (=> v0x7f3ac45ca490_0
                    (and v0x7f3ac45c9bd0_0
                         E0x7f3ac45ca550
                         (not v0x7f3ac45ca350_0)))
                (=> v0x7f3ac45ca490_0 E0x7f3ac45ca550)
                (=> v0x7f3ac45ca750_0
                    (and v0x7f3ac45ca490_0 E0x7f3ac45ca810 v0x7f3ac45c9390_0))
                (=> v0x7f3ac45ca750_0 E0x7f3ac45ca810)
                (=> v0x7f3ac45cac50_0
                    (and v0x7f3ac45ca490_0
                         E0x7f3ac45cad10
                         (not v0x7f3ac45c9390_0)))
                (=> v0x7f3ac45cac50_0 E0x7f3ac45cad10)
                (=> v0x7f3ac45cb010_0
                    (and v0x7f3ac45cac50_0 E0x7f3ac45cb0d0 v0x7f3ac45caed0_0))
                (=> v0x7f3ac45cb010_0 E0x7f3ac45cb0d0)
                (=> v0x7f3ac45cb290_0 a!7)
                a!8
                v0x7f3ac45cb290_0
                v0x7f3ac45cc9d0_0
                (<= v0x7f3ac45c6cd0_0 v0x7f3ac45cb350_0)
                (>= v0x7f3ac45c6cd0_0 v0x7f3ac45cb350_0)
                (<= v0x7f3ac45c6dd0_0 v0x7f3ac45cb4d0_0)
                (>= v0x7f3ac45c6dd0_0 v0x7f3ac45cb4d0_0)
                (<= v0x7f3ac45c5010_0 v0x7f3ac45cb410_0)
                (>= v0x7f3ac45c5010_0 v0x7f3ac45cb410_0)
                (= v0x7f3ac45c7450_0 (= v0x7f3ac45c7390_0 0.0))
                (= v0x7f3ac45c7890_0 (< v0x7f3ac45c6d10_0 2.0))
                (= v0x7f3ac45c7a50_0 (ite v0x7f3ac45c7890_0 1.0 0.0))
                (= v0x7f3ac45c7b90_0 (+ v0x7f3ac45c7a50_0 v0x7f3ac45c6d10_0))
                (= v0x7f3ac45c85d0_0 (= v0x7f3ac45c8510_0 0.0))
                (= v0x7f3ac45c8a50_0 (= v0x7f3ac45c8990_0 0.0))
                (= v0x7f3ac45c9390_0 (= v0x7f3ac45c6c10_0 0.0))
                (= v0x7f3ac45c9790_0 (> v0x7f3ac45c7d90_0 0.0))
                (= v0x7f3ac45c98d0_0 (+ v0x7f3ac45c7d90_0 (- 1.0)))
                (= v0x7f3ac45c9a90_0
                   (ite v0x7f3ac45c9790_0 v0x7f3ac45c98d0_0 v0x7f3ac45c7d90_0))
                (= v0x7f3ac45ca350_0 (= v0x7f3ac45c8c50_0 0.0))
                (= v0x7f3ac45ca9d0_0 (> v0x7f3ac45c9c90_0 1.0))
                (= v0x7f3ac45cab10_0
                   (ite v0x7f3ac45ca9d0_0 1.0 v0x7f3ac45c6c10_0))
                (= v0x7f3ac45caed0_0 (= v0x7f3ac45c9c90_0 0.0))
                (= v0x7f3ac45cc750_0 (not (= v0x7f3ac45cb410_0 0.0)))
                (= v0x7f3ac45cc890_0 (= v0x7f3ac45cb4d0_0 0.0))
                (= v0x7f3ac45cc9d0_0 (or v0x7f3ac45cc890_0 v0x7f3ac45cc750_0)))))
  (=> F0x7f3ac45cd710 a!9))))
(assert (=> F0x7f3ac45cd710 F0x7f3ac45cd650))
(assert (let ((a!1 (=> v0x7f3ac45c7cd0_0
               (or (and v0x7f3ac45c7590_0
                        E0x7f3ac45c7e50
                        (<= v0x7f3ac45c7d90_0 v0x7f3ac45c7b90_0)
                        (>= v0x7f3ac45c7d90_0 v0x7f3ac45c7b90_0))
                   (and v0x7f3ac45c72d0_0
                        E0x7f3ac45c8010
                        v0x7f3ac45c7450_0
                        (<= v0x7f3ac45c7d90_0 v0x7f3ac45c6d10_0)
                        (>= v0x7f3ac45c7d90_0 v0x7f3ac45c6d10_0)))))
      (a!2 (=> v0x7f3ac45c7cd0_0
               (or (and E0x7f3ac45c7e50 (not E0x7f3ac45c8010))
                   (and E0x7f3ac45c8010 (not E0x7f3ac45c7e50)))))
      (a!3 (=> v0x7f3ac45c8b90_0
               (or (and v0x7f3ac45c8710_0
                        E0x7f3ac45c8d10
                        v0x7f3ac45c8a50_0
                        (<= v0x7f3ac45c8c50_0 v0x7f3ac45c6a90_0)
                        (>= v0x7f3ac45c8c50_0 v0x7f3ac45c6a90_0))
                   (and v0x7f3ac45c7cd0_0
                        E0x7f3ac45c8f10
                        (not v0x7f3ac45c85d0_0)
                        (<= v0x7f3ac45c8c50_0 1.0)
                        (>= v0x7f3ac45c8c50_0 1.0)))))
      (a!4 (=> v0x7f3ac45c8b90_0
               (or (and E0x7f3ac45c8d10 (not E0x7f3ac45c8f10))
                   (and E0x7f3ac45c8f10 (not E0x7f3ac45c8d10)))))
      (a!5 (=> v0x7f3ac45c9bd0_0
               (or (and v0x7f3ac45c94d0_0
                        E0x7f3ac45c9d50
                        (<= v0x7f3ac45c9c90_0 v0x7f3ac45c9a90_0)
                        (>= v0x7f3ac45c9c90_0 v0x7f3ac45c9a90_0))
                   (and v0x7f3ac45c8b90_0
                        E0x7f3ac45c9f10
                        v0x7f3ac45c9390_0
                        (<= v0x7f3ac45c9c90_0 v0x7f3ac45c7d90_0)
                        (>= v0x7f3ac45c9c90_0 v0x7f3ac45c7d90_0)))))
      (a!6 (=> v0x7f3ac45c9bd0_0
               (or (and E0x7f3ac45c9d50 (not E0x7f3ac45c9f10))
                   (and E0x7f3ac45c9f10 (not E0x7f3ac45c9d50)))))
      (a!7 (or (and v0x7f3ac45ca750_0
                    E0x7f3ac45cb590
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (<= v0x7f3ac45cb4d0_0 v0x7f3ac45cab10_0)
                    (>= v0x7f3ac45cb4d0_0 v0x7f3ac45cab10_0))
               (and v0x7f3ac45cb010_0
                    E0x7f3ac45cb950
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (and (<= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)
                         (>= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)))
               (and v0x7f3ac45cac50_0
                    E0x7f3ac45cbb90
                    (not v0x7f3ac45caed0_0)
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (and (<= v0x7f3ac45cb4d0_0 0.0) (>= v0x7f3ac45cb4d0_0 0.0)))
               (and v0x7f3ac45c9bd0_0
                    E0x7f3ac45cbe50
                    v0x7f3ac45ca350_0
                    (and (<= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0)
                         (>= v0x7f3ac45cb350_0 v0x7f3ac45c8c50_0))
                    (and (<= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0)
                         (>= v0x7f3ac45cb410_0 v0x7f3ac45c9c90_0))
                    (and (<= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)
                         (>= v0x7f3ac45cb4d0_0 v0x7f3ac45c6c10_0)))
               (and v0x7f3ac45c8710_0
                    E0x7f3ac45cc010
                    (not v0x7f3ac45c8a50_0)
                    (<= v0x7f3ac45cb350_0 0.0)
                    (>= v0x7f3ac45cb350_0 0.0)
                    (<= v0x7f3ac45cb410_0 v0x7f3ac45c7d90_0)
                    (>= v0x7f3ac45cb410_0 v0x7f3ac45c7d90_0)
                    (and (<= v0x7f3ac45cb4d0_0 0.0) (>= v0x7f3ac45cb4d0_0 0.0)))))
      (a!8 (=> v0x7f3ac45cb290_0
               (or (and E0x7f3ac45cb590
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cbe50)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cb950
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cbe50)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cbb90
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbe50)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cbe50
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cc010))
                   (and E0x7f3ac45cc010
                        (not E0x7f3ac45cb590)
                        (not E0x7f3ac45cb950)
                        (not E0x7f3ac45cbb90)
                        (not E0x7f3ac45cbe50))))))
(let ((a!9 (and (=> v0x7f3ac45c7590_0
                    (and v0x7f3ac45c72d0_0
                         E0x7f3ac45c7650
                         (not v0x7f3ac45c7450_0)))
                (=> v0x7f3ac45c7590_0 E0x7f3ac45c7650)
                a!1
                a!2
                (=> v0x7f3ac45c8710_0
                    (and v0x7f3ac45c7cd0_0 E0x7f3ac45c87d0 v0x7f3ac45c85d0_0))
                (=> v0x7f3ac45c8710_0 E0x7f3ac45c87d0)
                a!3
                a!4
                (=> v0x7f3ac45c94d0_0
                    (and v0x7f3ac45c8b90_0
                         E0x7f3ac45c9590
                         (not v0x7f3ac45c9390_0)))
                (=> v0x7f3ac45c94d0_0 E0x7f3ac45c9590)
                a!5
                a!6
                (=> v0x7f3ac45ca490_0
                    (and v0x7f3ac45c9bd0_0
                         E0x7f3ac45ca550
                         (not v0x7f3ac45ca350_0)))
                (=> v0x7f3ac45ca490_0 E0x7f3ac45ca550)
                (=> v0x7f3ac45ca750_0
                    (and v0x7f3ac45ca490_0 E0x7f3ac45ca810 v0x7f3ac45c9390_0))
                (=> v0x7f3ac45ca750_0 E0x7f3ac45ca810)
                (=> v0x7f3ac45cac50_0
                    (and v0x7f3ac45ca490_0
                         E0x7f3ac45cad10
                         (not v0x7f3ac45c9390_0)))
                (=> v0x7f3ac45cac50_0 E0x7f3ac45cad10)
                (=> v0x7f3ac45cb010_0
                    (and v0x7f3ac45cac50_0 E0x7f3ac45cb0d0 v0x7f3ac45caed0_0))
                (=> v0x7f3ac45cb010_0 E0x7f3ac45cb0d0)
                (=> v0x7f3ac45cb290_0 a!7)
                a!8
                v0x7f3ac45cb290_0
                (not v0x7f3ac45cc9d0_0)
                (= v0x7f3ac45c7450_0 (= v0x7f3ac45c7390_0 0.0))
                (= v0x7f3ac45c7890_0 (< v0x7f3ac45c6d10_0 2.0))
                (= v0x7f3ac45c7a50_0 (ite v0x7f3ac45c7890_0 1.0 0.0))
                (= v0x7f3ac45c7b90_0 (+ v0x7f3ac45c7a50_0 v0x7f3ac45c6d10_0))
                (= v0x7f3ac45c85d0_0 (= v0x7f3ac45c8510_0 0.0))
                (= v0x7f3ac45c8a50_0 (= v0x7f3ac45c8990_0 0.0))
                (= v0x7f3ac45c9390_0 (= v0x7f3ac45c6c10_0 0.0))
                (= v0x7f3ac45c9790_0 (> v0x7f3ac45c7d90_0 0.0))
                (= v0x7f3ac45c98d0_0 (+ v0x7f3ac45c7d90_0 (- 1.0)))
                (= v0x7f3ac45c9a90_0
                   (ite v0x7f3ac45c9790_0 v0x7f3ac45c98d0_0 v0x7f3ac45c7d90_0))
                (= v0x7f3ac45ca350_0 (= v0x7f3ac45c8c50_0 0.0))
                (= v0x7f3ac45ca9d0_0 (> v0x7f3ac45c9c90_0 1.0))
                (= v0x7f3ac45cab10_0
                   (ite v0x7f3ac45ca9d0_0 1.0 v0x7f3ac45c6c10_0))
                (= v0x7f3ac45caed0_0 (= v0x7f3ac45c9c90_0 0.0))
                (= v0x7f3ac45cc750_0 (not (= v0x7f3ac45cb410_0 0.0)))
                (= v0x7f3ac45cc890_0 (= v0x7f3ac45cb4d0_0 0.0))
                (= v0x7f3ac45cc9d0_0 (or v0x7f3ac45cc890_0 v0x7f3ac45cc750_0)))))
  (=> F0x7f3ac45cd590 a!9))))
(assert (=> F0x7f3ac45cd590 F0x7f3ac45cd650))
(assert (=> F0x7f3ac45cd910 (or F0x7f3ac45cd850 F0x7f3ac45cd710)))
(assert (=> F0x7f3ac45cd8d0 F0x7f3ac45cd590))
(assert (=> pre!entry!0 (=> F0x7f3ac45cd510 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f3ac45cd650 (>= v0x7f3ac45c6c10_0 0.0))))
(assert (let ((a!1 (=> F0x7f3ac45cd650
               (or (<= v0x7f3ac45c6c10_0 0.0)
                   (not (<= 0.0 v0x7f3ac45c6a90_0))
                   (not (<= v0x7f3ac45c6a90_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (let ((a!1 (=> F0x7f3ac45cd650
               (or (not (<= 1.0 v0x7f3ac45c6c10_0))
                   (not (<= v0x7f3ac45c6d10_0 1.0))))))
  (=> pre!bb1.i.i!2 a!1)))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f3ac45cd650
        (or (<= v0x7f3ac45c6c10_0 0.0) (>= v0x7f3ac45c6c10_0 1.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f3ac45cd910 (>= v0x7f3ac45c6dd0_0 0.0))))
(assert (let ((a!1 (=> F0x7f3ac45cd910
               (or (<= v0x7f3ac45c6dd0_0 0.0)
                   (not (<= 0.0 v0x7f3ac45c6cd0_0))
                   (not (<= v0x7f3ac45c6cd0_0 0.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (let ((a!1 (=> F0x7f3ac45cd910
               (or (not (<= 1.0 v0x7f3ac45c6dd0_0))
                   (not (<= v0x7f3ac45c5010_0 1.0))))))
  (= lemma!bb1.i.i!2 a!1)))
(assert (= lemma!bb1.i.i!3
   (=> F0x7f3ac45cd910
       (or (<= v0x7f3ac45c6dd0_0 0.0) (>= v0x7f3ac45c6dd0_0 1.0)))))
(assert (= lemma!bb1.i.i26.i.i!0 (=> F0x7f3ac45cd8d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i26.i.i!0) (not lemma!bb1.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7f3ac45cd510)
; (error: F0x7f3ac45cd8d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i26.i.i!0)
