(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f5a21b22510 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f5a21b21910_0 () Bool)
(declare-fun v0x7f5a21b217d0_0 () Bool)
(declare-fun v0x7f5a21b21550_0 () Bool)
(declare-fun F0x7f5a21b22890 () Bool)
(declare-fun v0x7f5a21b1ef10_0 () Real)
(declare-fun v0x7f5a21b1ed50_0 () Bool)
(declare-fun v0x7f5a21b1e850_0 () Real)
(declare-fun F0x7f5a21b22790 () Bool)
(declare-fun v0x7f5a21b21a50_0 () Bool)
(declare-fun v0x7f5a21b21690_0 () Bool)
(declare-fun v0x7f5a21b20550_0 () Real)
(declare-fun E0x7f5a21b21090 () Bool)
(declare-fun v0x7f5a21b1e190_0 () Real)
(declare-fun v0x7f5a21b20a10_0 () Real)
(declare-fun v0x7f5a21b1fe50_0 () Bool)
(declare-fun E0x7f5a21b20750 () Bool)
(declare-fun v0x7f5a21b1fa90_0 () Bool)
(declare-fun v0x7f5a21b20690_0 () Bool)
(declare-fun v0x7f5a21b20950_0 () Bool)
(declare-fun v0x7f5a21b20390_0 () Real)
(declare-fun v0x7f5a21b1e310_0 () Real)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun E0x7f5a21b1f310 () Bool)
(declare-fun v0x7f5a21b1e910_0 () Bool)
(declare-fun v0x7f5a21b1e790_0 () Bool)
(declare-fun v0x7f5a21b1f250_0 () Real)
(declare-fun E0x7f5a21b20b90 () Bool)
(declare-fun v0x7f5a21b1f190_0 () Bool)
(declare-fun v0x7f5a21b1fbd0_0 () Bool)
(declare-fun F0x7f5a21b225d0 () Bool)
(declare-fun v0x7f5a21b1f050_0 () Real)
(declare-fun E0x7f5a21b1eb10 () Bool)
(declare-fun F0x7f5a21b22690 () Bool)
(declare-fun F0x7f5a21b22850 () Bool)
(declare-fun v0x7f5a21b1d010_0 () Real)
(declare-fun v0x7f5a21b1e3d0_0 () Real)
(declare-fun v0x7f5a21b20250_0 () Bool)
(declare-fun E0x7f5a21b20e50 () Bool)
(declare-fun lemma!bb2.i.i16.i.i!0 () Bool)
(declare-fun v0x7f5a21b1d110_0 () Bool)
(declare-fun E0x7f5a21b1fc90 () Bool)
(declare-fun v0x7f5a21b1ff90_0 () Bool)
(declare-fun E0x7f5a21b1f4d0 () Bool)
(declare-fun v0x7f5a21b1ea50_0 () Bool)
(declare-fun F0x7f5a21b22750 () Bool)
(declare-fun E0x7f5a21b20050 () Bool)
(declare-fun v0x7f5a21b20ad0_0 () Real)

(assert (=> F0x7f5a21b22750
    (and v0x7f5a21b1d110_0
         (<= v0x7f5a21b1e3d0_0 0.0)
         (>= v0x7f5a21b1e3d0_0 0.0)
         (<= v0x7f5a21b1d010_0 1.0)
         (>= v0x7f5a21b1d010_0 1.0))))
(assert (=> F0x7f5a21b22750 F0x7f5a21b22690))
(assert (let ((a!1 (=> v0x7f5a21b1f190_0
               (or (and v0x7f5a21b1ea50_0
                        E0x7f5a21b1f310
                        (<= v0x7f5a21b1f250_0 v0x7f5a21b1f050_0)
                        (>= v0x7f5a21b1f250_0 v0x7f5a21b1f050_0))
                   (and v0x7f5a21b1e790_0
                        E0x7f5a21b1f4d0
                        v0x7f5a21b1e910_0
                        (<= v0x7f5a21b1f250_0 v0x7f5a21b1e310_0)
                        (>= v0x7f5a21b1f250_0 v0x7f5a21b1e310_0)))))
      (a!2 (=> v0x7f5a21b1f190_0
               (or (and E0x7f5a21b1f310 (not E0x7f5a21b1f4d0))
                   (and E0x7f5a21b1f4d0 (not E0x7f5a21b1f310)))))
      (a!3 (or (and v0x7f5a21b20690_0
                    E0x7f5a21b20b90
                    (and (<= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0)
                         (>= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0))
                    (and (<= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)
                         (>= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)))
               (and v0x7f5a21b1fbd0_0
                    E0x7f5a21b20e50
                    v0x7f5a21b1fe50_0
                    (and (<= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0)
                         (>= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0))
                    (<= v0x7f5a21b20ad0_0 1.0)
                    (>= v0x7f5a21b20ad0_0 1.0))
               (and v0x7f5a21b1ff90_0
                    E0x7f5a21b21090
                    (<= v0x7f5a21b20a10_0 v0x7f5a21b20550_0)
                    (>= v0x7f5a21b20a10_0 v0x7f5a21b20550_0)
                    (and (<= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)
                         (>= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)))))
      (a!4 (=> v0x7f5a21b20950_0
               (or (and E0x7f5a21b20b90
                        (not E0x7f5a21b20e50)
                        (not E0x7f5a21b21090))
                   (and E0x7f5a21b20e50
                        (not E0x7f5a21b20b90)
                        (not E0x7f5a21b21090))
                   (and E0x7f5a21b21090
                        (not E0x7f5a21b20b90)
                        (not E0x7f5a21b20e50))))))
(let ((a!5 (and (=> v0x7f5a21b1ea50_0
                    (and v0x7f5a21b1e790_0
                         E0x7f5a21b1eb10
                         (not v0x7f5a21b1e910_0)))
                (=> v0x7f5a21b1ea50_0 E0x7f5a21b1eb10)
                a!1
                a!2
                (=> v0x7f5a21b1fbd0_0
                    (and v0x7f5a21b1f190_0 E0x7f5a21b1fc90 v0x7f5a21b1fa90_0))
                (=> v0x7f5a21b1fbd0_0 E0x7f5a21b1fc90)
                (=> v0x7f5a21b1ff90_0
                    (and v0x7f5a21b1f190_0
                         E0x7f5a21b20050
                         (not v0x7f5a21b1fa90_0)))
                (=> v0x7f5a21b1ff90_0 E0x7f5a21b20050)
                (=> v0x7f5a21b20690_0
                    (and v0x7f5a21b1fbd0_0
                         E0x7f5a21b20750
                         (not v0x7f5a21b1fe50_0)))
                (=> v0x7f5a21b20690_0 E0x7f5a21b20750)
                (=> v0x7f5a21b20950_0 a!3)
                a!4
                v0x7f5a21b20950_0
                (not v0x7f5a21b21a50_0)
                (<= v0x7f5a21b1e3d0_0 v0x7f5a21b20ad0_0)
                (>= v0x7f5a21b1e3d0_0 v0x7f5a21b20ad0_0)
                (<= v0x7f5a21b1d010_0 v0x7f5a21b20a10_0)
                (>= v0x7f5a21b1d010_0 v0x7f5a21b20a10_0)
                (= v0x7f5a21b1e910_0 (= v0x7f5a21b1e850_0 0.0))
                (= v0x7f5a21b1ed50_0 (< v0x7f5a21b1e310_0 2.0))
                (= v0x7f5a21b1ef10_0 (ite v0x7f5a21b1ed50_0 1.0 0.0))
                (= v0x7f5a21b1f050_0 (+ v0x7f5a21b1ef10_0 v0x7f5a21b1e310_0))
                (= v0x7f5a21b1fa90_0 (= v0x7f5a21b1e190_0 0.0))
                (= v0x7f5a21b1fe50_0 (> v0x7f5a21b1f250_0 1.0))
                (= v0x7f5a21b20250_0 (> v0x7f5a21b1f250_0 0.0))
                (= v0x7f5a21b20390_0 (+ v0x7f5a21b1f250_0 (- 1.0)))
                (= v0x7f5a21b20550_0
                   (ite v0x7f5a21b20250_0 v0x7f5a21b20390_0 v0x7f5a21b1f250_0))
                (= v0x7f5a21b21550_0 (= v0x7f5a21b20a10_0 2.0))
                (= v0x7f5a21b21690_0 (= v0x7f5a21b20ad0_0 0.0))
                (= v0x7f5a21b217d0_0 (or v0x7f5a21b21690_0 v0x7f5a21b21550_0))
                (= v0x7f5a21b21910_0 (xor v0x7f5a21b217d0_0 true))
                (= v0x7f5a21b21a50_0 (and v0x7f5a21b1fa90_0 v0x7f5a21b21910_0)))))
  (=> F0x7f5a21b225d0 a!5))))
(assert (=> F0x7f5a21b225d0 F0x7f5a21b22510))
(assert (let ((a!1 (=> v0x7f5a21b1f190_0
               (or (and v0x7f5a21b1ea50_0
                        E0x7f5a21b1f310
                        (<= v0x7f5a21b1f250_0 v0x7f5a21b1f050_0)
                        (>= v0x7f5a21b1f250_0 v0x7f5a21b1f050_0))
                   (and v0x7f5a21b1e790_0
                        E0x7f5a21b1f4d0
                        v0x7f5a21b1e910_0
                        (<= v0x7f5a21b1f250_0 v0x7f5a21b1e310_0)
                        (>= v0x7f5a21b1f250_0 v0x7f5a21b1e310_0)))))
      (a!2 (=> v0x7f5a21b1f190_0
               (or (and E0x7f5a21b1f310 (not E0x7f5a21b1f4d0))
                   (and E0x7f5a21b1f4d0 (not E0x7f5a21b1f310)))))
      (a!3 (or (and v0x7f5a21b20690_0
                    E0x7f5a21b20b90
                    (and (<= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0)
                         (>= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0))
                    (and (<= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)
                         (>= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)))
               (and v0x7f5a21b1fbd0_0
                    E0x7f5a21b20e50
                    v0x7f5a21b1fe50_0
                    (and (<= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0)
                         (>= v0x7f5a21b20a10_0 v0x7f5a21b1f250_0))
                    (<= v0x7f5a21b20ad0_0 1.0)
                    (>= v0x7f5a21b20ad0_0 1.0))
               (and v0x7f5a21b1ff90_0
                    E0x7f5a21b21090
                    (<= v0x7f5a21b20a10_0 v0x7f5a21b20550_0)
                    (>= v0x7f5a21b20a10_0 v0x7f5a21b20550_0)
                    (and (<= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)
                         (>= v0x7f5a21b20ad0_0 v0x7f5a21b1e190_0)))))
      (a!4 (=> v0x7f5a21b20950_0
               (or (and E0x7f5a21b20b90
                        (not E0x7f5a21b20e50)
                        (not E0x7f5a21b21090))
                   (and E0x7f5a21b20e50
                        (not E0x7f5a21b20b90)
                        (not E0x7f5a21b21090))
                   (and E0x7f5a21b21090
                        (not E0x7f5a21b20b90)
                        (not E0x7f5a21b20e50))))))
(let ((a!5 (and (=> v0x7f5a21b1ea50_0
                    (and v0x7f5a21b1e790_0
                         E0x7f5a21b1eb10
                         (not v0x7f5a21b1e910_0)))
                (=> v0x7f5a21b1ea50_0 E0x7f5a21b1eb10)
                a!1
                a!2
                (=> v0x7f5a21b1fbd0_0
                    (and v0x7f5a21b1f190_0 E0x7f5a21b1fc90 v0x7f5a21b1fa90_0))
                (=> v0x7f5a21b1fbd0_0 E0x7f5a21b1fc90)
                (=> v0x7f5a21b1ff90_0
                    (and v0x7f5a21b1f190_0
                         E0x7f5a21b20050
                         (not v0x7f5a21b1fa90_0)))
                (=> v0x7f5a21b1ff90_0 E0x7f5a21b20050)
                (=> v0x7f5a21b20690_0
                    (and v0x7f5a21b1fbd0_0
                         E0x7f5a21b20750
                         (not v0x7f5a21b1fe50_0)))
                (=> v0x7f5a21b20690_0 E0x7f5a21b20750)
                (=> v0x7f5a21b20950_0 a!3)
                a!4
                v0x7f5a21b20950_0
                v0x7f5a21b21a50_0
                (= v0x7f5a21b1e910_0 (= v0x7f5a21b1e850_0 0.0))
                (= v0x7f5a21b1ed50_0 (< v0x7f5a21b1e310_0 2.0))
                (= v0x7f5a21b1ef10_0 (ite v0x7f5a21b1ed50_0 1.0 0.0))
                (= v0x7f5a21b1f050_0 (+ v0x7f5a21b1ef10_0 v0x7f5a21b1e310_0))
                (= v0x7f5a21b1fa90_0 (= v0x7f5a21b1e190_0 0.0))
                (= v0x7f5a21b1fe50_0 (> v0x7f5a21b1f250_0 1.0))
                (= v0x7f5a21b20250_0 (> v0x7f5a21b1f250_0 0.0))
                (= v0x7f5a21b20390_0 (+ v0x7f5a21b1f250_0 (- 1.0)))
                (= v0x7f5a21b20550_0
                   (ite v0x7f5a21b20250_0 v0x7f5a21b20390_0 v0x7f5a21b1f250_0))
                (= v0x7f5a21b21550_0 (= v0x7f5a21b20a10_0 2.0))
                (= v0x7f5a21b21690_0 (= v0x7f5a21b20ad0_0 0.0))
                (= v0x7f5a21b217d0_0 (or v0x7f5a21b21690_0 v0x7f5a21b21550_0))
                (= v0x7f5a21b21910_0 (xor v0x7f5a21b217d0_0 true))
                (= v0x7f5a21b21a50_0 (and v0x7f5a21b1fa90_0 v0x7f5a21b21910_0)))))
  (=> F0x7f5a21b22790 a!5))))
(assert (=> F0x7f5a21b22790 F0x7f5a21b22510))
(assert (=> F0x7f5a21b22890 (or F0x7f5a21b22750 F0x7f5a21b225d0)))
(assert (=> F0x7f5a21b22850 F0x7f5a21b22790))
(assert (=> pre!entry!0 (=> F0x7f5a21b22690 true)))
(assert (let ((a!1 (not (or (not (>= v0x7f5a21b1e310_0 1.0))
                    (not (<= v0x7f5a21b1e190_0 0.0))
                    (not (<= v0x7f5a21b1e310_0 1.0))
                    (not (>= v0x7f5a21b1e190_0 0.0))))))
  (=> pre!bb1.i.i!0 (=> F0x7f5a21b22510 (or (>= v0x7f5a21b1e190_0 1.0) a!1)))))
(assert (let ((a!1 (not (or (not (>= v0x7f5a21b1d010_0 1.0))
                    (not (<= v0x7f5a21b1e3d0_0 0.0))
                    (not (<= v0x7f5a21b1d010_0 1.0))
                    (not (>= v0x7f5a21b1e3d0_0 0.0))))))
  (= lemma!bb1.i.i!0 (=> F0x7f5a21b22890 (or (>= v0x7f5a21b1e3d0_0 1.0) a!1)))))
(assert (= lemma!bb2.i.i16.i.i!0 (=> F0x7f5a21b22850 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i16.i.i!0) (not lemma!bb2.i.i16.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f5a21b22690)
; (error: F0x7f5a21b22850)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i16.i.i!0)
