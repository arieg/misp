(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f3cc1781950 () Bool)
(declare-fun F0x7f3cc1781590 () Bool)
(declare-fun v0x7f3cc1780590_0 () Bool)
(declare-fun v0x7f3cc177f510_0 () Bool)
(declare-fun v0x7f3cc177f290_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f3cc177f150_0 () Bool)
(declare-fun v0x7f3cc177d250_0 () Real)
(declare-fun v0x7f3cc177c3d0_0 () Bool)
(declare-fun v0x7f3cc177bf10_0 () Real)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun v0x7f3cc177fa10_0 () Bool)
(declare-fun v0x7f3cc177b510_0 () Real)
(declare-fun E0x7f3cc177ffd0 () Bool)
(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun E0x7f3cc177fe10 () Bool)
(declare-fun v0x7f3cc177ded0_0 () Real)
(declare-fun v0x7f3cc177f790_0 () Bool)
(declare-fun E0x7f3cc177eb10 () Bool)
(declare-fun E0x7f3cc177e890 () Bool)
(declare-fun v0x7f3cc177a590_0 () Real)
(declare-fun v0x7f3cc177e350_0 () Real)
(declare-fun v0x7f3cc177a690_0 () Real)
(declare-fun v0x7f3cc177e290_0 () Real)
(declare-fun E0x7f3cc177e4d0 () Bool)
(declare-fun v0x7f3cc177d990_0 () Bool)
(declare-fun v0x7f3cc177cdd0_0 () Real)
(declare-fun v0x7f3cc177e1d0_0 () Bool)
(declare-fun v0x7f3cc177dad0_0 () Bool)
(declare-fun E0x7f3cc177d610 () Bool)
(declare-fun v0x7f3cc177fc90_0 () Bool)
(declare-fun v0x7f3cc177d450_0 () Bool)
(declare-fun E0x7f3cc177d090 () Bool)
(declare-fun E0x7f3cc177c990 () Bool)
(declare-fun v0x7f3cc17806d0_0 () Bool)
(declare-fun v0x7f3cc177ce90_0 () Bool)
(declare-fun E0x7f3cc177c7d0 () Bool)
(declare-fun v0x7f3cc177fb50_0 () Real)
(declare-fun v0x7f3cc177bfd0_0 () Bool)
(declare-fun E0x7f3cc177c1d0 () Bool)
(declare-fun E0x7f3cc177f850 () Bool)
(declare-fun v0x7f3cc1780950_0 () Bool)
(declare-fun v0x7f3cc177c110_0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun E0x7f3cc177bad0 () Bool)
(declare-fun v0x7f3cc177b650_0 () Real)
(declare-fun v0x7f3cc177b850_0 () Real)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7f3cc177b790_0 () Bool)
(declare-fun F0x7f3cc1781890 () Bool)
(declare-fun E0x7f3cc177b910 () Bool)
(declare-fun v0x7f3cc177f650_0 () Bool)
(declare-fun v0x7f3cc177dd90_0 () Bool)
(declare-fun E0x7f3cc177b110 () Bool)
(declare-fun v0x7f3cc177ae50_0 () Real)
(declare-fun v0x7f3cc177c650_0 () Bool)
(declare-fun v0x7f3cc177ad90_0 () Bool)
(declare-fun v0x7f3cc177f3d0_0 () Bool)
(declare-fun v0x7f3cc177b050_0 () Bool)
(declare-fun E0x7f3cc177d510 () Bool)
(declare-fun F0x7f3cc1781650 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f3cc177a490_0 () Real)
(declare-fun v0x7f3cc177af10_0 () Bool)
(declare-fun v0x7f3cc177b350_0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun F0x7f3cc1781710 () Bool)
(declare-fun v0x7f3cc177e090_0 () Real)
(declare-fun v0x7f3cc177a650_0 () Real)
(declare-fun v0x7f3cc1779010_0 () Real)
(declare-fun E0x7f3cc177db90 () Bool)
(declare-fun v0x7f3cc177e410_0 () Real)
(declare-fun v0x7f3cc177cfd0_0 () Bool)
(declare-fun v0x7f3cc177d310_0 () Bool)
(declare-fun v0x7f3cc1780810_0 () Bool)
(declare-fun v0x7f3cc177a310_0 () Real)
(declare-fun v0x7f3cc1780450_0 () Bool)
(declare-fun v0x7f3cc177c510_0 () Real)
(declare-fun v0x7f3cc177a550_0 () Real)
(declare-fun v0x7f3cc177c710_0 () Real)
(declare-fun v0x7f3cc1779110_0 () Bool)
(declare-fun v0x7f3cc177a750_0 () Real)
(declare-fun v0x7f3cc177fd50_0 () Real)
(declare-fun F0x7f3cc1781990 () Bool)
(declare-fun F0x7f3cc17817d0 () Bool)

(assert (=> F0x7f3cc17817d0
    (and v0x7f3cc1779110_0
         (<= v0x7f3cc177a550_0 0.0)
         (>= v0x7f3cc177a550_0 0.0)
         (<= v0x7f3cc177a650_0 1.0)
         (>= v0x7f3cc177a650_0 1.0)
         (<= v0x7f3cc177a750_0 1.0)
         (>= v0x7f3cc177a750_0 1.0)
         (<= v0x7f3cc1779010_0 0.0)
         (>= v0x7f3cc1779010_0 0.0))))
(assert (=> F0x7f3cc17817d0 F0x7f3cc1781710))
(assert (let ((a!1 (=> v0x7f3cc177b790_0
               (or (and v0x7f3cc177b050_0
                        E0x7f3cc177b910
                        (<= v0x7f3cc177b850_0 v0x7f3cc177b650_0)
                        (>= v0x7f3cc177b850_0 v0x7f3cc177b650_0))
                   (and v0x7f3cc177ad90_0
                        E0x7f3cc177bad0
                        v0x7f3cc177af10_0
                        (<= v0x7f3cc177b850_0 v0x7f3cc177a490_0)
                        (>= v0x7f3cc177b850_0 v0x7f3cc177a490_0)))))
      (a!2 (=> v0x7f3cc177b790_0
               (or (and E0x7f3cc177b910 (not E0x7f3cc177bad0))
                   (and E0x7f3cc177bad0 (not E0x7f3cc177b910)))))
      (a!3 (=> v0x7f3cc177c650_0
               (or (and v0x7f3cc177c110_0
                        E0x7f3cc177c7d0
                        (<= v0x7f3cc177c710_0 v0x7f3cc177c510_0)
                        (>= v0x7f3cc177c710_0 v0x7f3cc177c510_0))
                   (and v0x7f3cc177b790_0
                        E0x7f3cc177c990
                        v0x7f3cc177bfd0_0
                        (<= v0x7f3cc177c710_0 v0x7f3cc177a310_0)
                        (>= v0x7f3cc177c710_0 v0x7f3cc177a310_0)))))
      (a!4 (=> v0x7f3cc177c650_0
               (or (and E0x7f3cc177c7d0 (not E0x7f3cc177c990))
                   (and E0x7f3cc177c990 (not E0x7f3cc177c7d0)))))
      (a!5 (=> v0x7f3cc177d450_0
               (or (and v0x7f3cc177cfd0_0 E0x7f3cc177d510 v0x7f3cc177d310_0)
                   (and v0x7f3cc177c650_0
                        E0x7f3cc177d610
                        (not v0x7f3cc177ce90_0)))))
      (a!6 (=> v0x7f3cc177d450_0
               (or (and E0x7f3cc177d510 (not E0x7f3cc177d610))
                   (and E0x7f3cc177d610 (not E0x7f3cc177d510)))))
      (a!7 (or (and v0x7f3cc177dad0_0
                    E0x7f3cc177e4d0
                    (and (<= v0x7f3cc177e290_0 v0x7f3cc177a690_0)
                         (>= v0x7f3cc177e290_0 v0x7f3cc177a690_0))
                    (and (<= v0x7f3cc177e350_0 v0x7f3cc177a590_0)
                         (>= v0x7f3cc177e350_0 v0x7f3cc177a590_0))
                    (<= v0x7f3cc177e410_0 v0x7f3cc177e090_0)
                    (>= v0x7f3cc177e410_0 v0x7f3cc177e090_0))
               (and v0x7f3cc177d450_0
                    E0x7f3cc177e890
                    v0x7f3cc177d990_0
                    (and (<= v0x7f3cc177e290_0 v0x7f3cc177a690_0)
                         (>= v0x7f3cc177e290_0 v0x7f3cc177a690_0))
                    (and (<= v0x7f3cc177e350_0 v0x7f3cc177a590_0)
                         (>= v0x7f3cc177e350_0 v0x7f3cc177a590_0))
                    (and (<= v0x7f3cc177e410_0 v0x7f3cc177b850_0)
                         (>= v0x7f3cc177e410_0 v0x7f3cc177b850_0)))
               (and v0x7f3cc177cfd0_0
                    E0x7f3cc177eb10
                    (not v0x7f3cc177d310_0)
                    (<= v0x7f3cc177e290_0 0.0)
                    (>= v0x7f3cc177e290_0 0.0)
                    (<= v0x7f3cc177e350_0 0.0)
                    (>= v0x7f3cc177e350_0 0.0)
                    (and (<= v0x7f3cc177e410_0 v0x7f3cc177b850_0)
                         (>= v0x7f3cc177e410_0 v0x7f3cc177b850_0)))))
      (a!8 (=> v0x7f3cc177e1d0_0
               (or (and E0x7f3cc177e4d0
                        (not E0x7f3cc177e890)
                        (not E0x7f3cc177eb10))
                   (and E0x7f3cc177e890
                        (not E0x7f3cc177e4d0)
                        (not E0x7f3cc177eb10))
                   (and E0x7f3cc177eb10
                        (not E0x7f3cc177e4d0)
                        (not E0x7f3cc177e890)))))
      (a!9 (=> v0x7f3cc177fc90_0
               (or (and v0x7f3cc177f790_0
                        E0x7f3cc177fe10
                        (<= v0x7f3cc177fd50_0 v0x7f3cc177fb50_0)
                        (>= v0x7f3cc177fd50_0 v0x7f3cc177fb50_0))
                   (and v0x7f3cc177e1d0_0
                        E0x7f3cc177ffd0
                        (not v0x7f3cc177f650_0)
                        (<= v0x7f3cc177fd50_0 v0x7f3cc177e290_0)
                        (>= v0x7f3cc177fd50_0 v0x7f3cc177e290_0)))))
      (a!10 (=> v0x7f3cc177fc90_0
                (or (and E0x7f3cc177fe10 (not E0x7f3cc177ffd0))
                    (and E0x7f3cc177ffd0 (not E0x7f3cc177fe10))))))
(let ((a!11 (and (=> v0x7f3cc177b050_0
                     (and v0x7f3cc177ad90_0
                          E0x7f3cc177b110
                          (not v0x7f3cc177af10_0)))
                 (=> v0x7f3cc177b050_0 E0x7f3cc177b110)
                 a!1
                 a!2
                 (=> v0x7f3cc177c110_0
                     (and v0x7f3cc177b790_0
                          E0x7f3cc177c1d0
                          (not v0x7f3cc177bfd0_0)))
                 (=> v0x7f3cc177c110_0 E0x7f3cc177c1d0)
                 a!3
                 a!4
                 (=> v0x7f3cc177cfd0_0
                     (and v0x7f3cc177c650_0 E0x7f3cc177d090 v0x7f3cc177ce90_0))
                 (=> v0x7f3cc177cfd0_0 E0x7f3cc177d090)
                 a!5
                 a!6
                 (=> v0x7f3cc177dad0_0
                     (and v0x7f3cc177d450_0
                          E0x7f3cc177db90
                          (not v0x7f3cc177d990_0)))
                 (=> v0x7f3cc177dad0_0 E0x7f3cc177db90)
                 (=> v0x7f3cc177e1d0_0 a!7)
                 a!8
                 (=> v0x7f3cc177f790_0
                     (and v0x7f3cc177e1d0_0 E0x7f3cc177f850 v0x7f3cc177f650_0))
                 (=> v0x7f3cc177f790_0 E0x7f3cc177f850)
                 a!9
                 a!10
                 v0x7f3cc177fc90_0
                 (not v0x7f3cc1780950_0)
                 (<= v0x7f3cc177a550_0 v0x7f3cc177c710_0)
                 (>= v0x7f3cc177a550_0 v0x7f3cc177c710_0)
                 (<= v0x7f3cc177a650_0 v0x7f3cc177e410_0)
                 (>= v0x7f3cc177a650_0 v0x7f3cc177e410_0)
                 (<= v0x7f3cc177a750_0 v0x7f3cc177e350_0)
                 (>= v0x7f3cc177a750_0 v0x7f3cc177e350_0)
                 (<= v0x7f3cc1779010_0 v0x7f3cc177fd50_0)
                 (>= v0x7f3cc1779010_0 v0x7f3cc177fd50_0)
                 (= v0x7f3cc177af10_0 (= v0x7f3cc177ae50_0 0.0))
                 (= v0x7f3cc177b350_0 (< v0x7f3cc177a490_0 2.0))
                 (= v0x7f3cc177b510_0 (ite v0x7f3cc177b350_0 1.0 0.0))
                 (= v0x7f3cc177b650_0 (+ v0x7f3cc177b510_0 v0x7f3cc177a490_0))
                 (= v0x7f3cc177bfd0_0 (= v0x7f3cc177bf10_0 0.0))
                 (= v0x7f3cc177c3d0_0 (= v0x7f3cc177a310_0 0.0))
                 (= v0x7f3cc177c510_0 (ite v0x7f3cc177c3d0_0 1.0 0.0))
                 (= v0x7f3cc177ce90_0 (= v0x7f3cc177cdd0_0 0.0))
                 (= v0x7f3cc177d310_0 (= v0x7f3cc177d250_0 0.0))
                 (= v0x7f3cc177d990_0 (= v0x7f3cc177a690_0 0.0))
                 (= v0x7f3cc177dd90_0 (> v0x7f3cc177b850_0 0.0))
                 (= v0x7f3cc177ded0_0 (+ v0x7f3cc177b850_0 (- 1.0)))
                 (= v0x7f3cc177e090_0
                    (ite v0x7f3cc177dd90_0 v0x7f3cc177ded0_0 v0x7f3cc177b850_0))
                 (= v0x7f3cc177f150_0 (not (= v0x7f3cc177e350_0 0.0)))
                 (= v0x7f3cc177f290_0 (= v0x7f3cc177e290_0 0.0))
                 (= v0x7f3cc177f3d0_0 (> v0x7f3cc177e410_0 1.0))
                 (= v0x7f3cc177f510_0 (and v0x7f3cc177f150_0 v0x7f3cc177f290_0))
                 (= v0x7f3cc177f650_0 (and v0x7f3cc177f510_0 v0x7f3cc177f3d0_0))
                 (= v0x7f3cc177fa10_0 (= v0x7f3cc177c710_0 0.0))
                 (= v0x7f3cc177fb50_0
                    (ite v0x7f3cc177fa10_0 1.0 v0x7f3cc177e290_0))
                 (= v0x7f3cc1780450_0 (= v0x7f3cc177e410_0 2.0))
                 (= v0x7f3cc1780590_0 (= v0x7f3cc177fd50_0 0.0))
                 (= v0x7f3cc17806d0_0 (or v0x7f3cc1780590_0 v0x7f3cc1780450_0))
                 (= v0x7f3cc1780810_0 (xor v0x7f3cc17806d0_0 true))
                 (= v0x7f3cc1780950_0 (and v0x7f3cc177f290_0 v0x7f3cc1780810_0)))))
  (=> F0x7f3cc1781650 a!11))))
(assert (=> F0x7f3cc1781650 F0x7f3cc1781590))
(assert (let ((a!1 (=> v0x7f3cc177b790_0
               (or (and v0x7f3cc177b050_0
                        E0x7f3cc177b910
                        (<= v0x7f3cc177b850_0 v0x7f3cc177b650_0)
                        (>= v0x7f3cc177b850_0 v0x7f3cc177b650_0))
                   (and v0x7f3cc177ad90_0
                        E0x7f3cc177bad0
                        v0x7f3cc177af10_0
                        (<= v0x7f3cc177b850_0 v0x7f3cc177a490_0)
                        (>= v0x7f3cc177b850_0 v0x7f3cc177a490_0)))))
      (a!2 (=> v0x7f3cc177b790_0
               (or (and E0x7f3cc177b910 (not E0x7f3cc177bad0))
                   (and E0x7f3cc177bad0 (not E0x7f3cc177b910)))))
      (a!3 (=> v0x7f3cc177c650_0
               (or (and v0x7f3cc177c110_0
                        E0x7f3cc177c7d0
                        (<= v0x7f3cc177c710_0 v0x7f3cc177c510_0)
                        (>= v0x7f3cc177c710_0 v0x7f3cc177c510_0))
                   (and v0x7f3cc177b790_0
                        E0x7f3cc177c990
                        v0x7f3cc177bfd0_0
                        (<= v0x7f3cc177c710_0 v0x7f3cc177a310_0)
                        (>= v0x7f3cc177c710_0 v0x7f3cc177a310_0)))))
      (a!4 (=> v0x7f3cc177c650_0
               (or (and E0x7f3cc177c7d0 (not E0x7f3cc177c990))
                   (and E0x7f3cc177c990 (not E0x7f3cc177c7d0)))))
      (a!5 (=> v0x7f3cc177d450_0
               (or (and v0x7f3cc177cfd0_0 E0x7f3cc177d510 v0x7f3cc177d310_0)
                   (and v0x7f3cc177c650_0
                        E0x7f3cc177d610
                        (not v0x7f3cc177ce90_0)))))
      (a!6 (=> v0x7f3cc177d450_0
               (or (and E0x7f3cc177d510 (not E0x7f3cc177d610))
                   (and E0x7f3cc177d610 (not E0x7f3cc177d510)))))
      (a!7 (or (and v0x7f3cc177dad0_0
                    E0x7f3cc177e4d0
                    (and (<= v0x7f3cc177e290_0 v0x7f3cc177a690_0)
                         (>= v0x7f3cc177e290_0 v0x7f3cc177a690_0))
                    (and (<= v0x7f3cc177e350_0 v0x7f3cc177a590_0)
                         (>= v0x7f3cc177e350_0 v0x7f3cc177a590_0))
                    (<= v0x7f3cc177e410_0 v0x7f3cc177e090_0)
                    (>= v0x7f3cc177e410_0 v0x7f3cc177e090_0))
               (and v0x7f3cc177d450_0
                    E0x7f3cc177e890
                    v0x7f3cc177d990_0
                    (and (<= v0x7f3cc177e290_0 v0x7f3cc177a690_0)
                         (>= v0x7f3cc177e290_0 v0x7f3cc177a690_0))
                    (and (<= v0x7f3cc177e350_0 v0x7f3cc177a590_0)
                         (>= v0x7f3cc177e350_0 v0x7f3cc177a590_0))
                    (and (<= v0x7f3cc177e410_0 v0x7f3cc177b850_0)
                         (>= v0x7f3cc177e410_0 v0x7f3cc177b850_0)))
               (and v0x7f3cc177cfd0_0
                    E0x7f3cc177eb10
                    (not v0x7f3cc177d310_0)
                    (<= v0x7f3cc177e290_0 0.0)
                    (>= v0x7f3cc177e290_0 0.0)
                    (<= v0x7f3cc177e350_0 0.0)
                    (>= v0x7f3cc177e350_0 0.0)
                    (and (<= v0x7f3cc177e410_0 v0x7f3cc177b850_0)
                         (>= v0x7f3cc177e410_0 v0x7f3cc177b850_0)))))
      (a!8 (=> v0x7f3cc177e1d0_0
               (or (and E0x7f3cc177e4d0
                        (not E0x7f3cc177e890)
                        (not E0x7f3cc177eb10))
                   (and E0x7f3cc177e890
                        (not E0x7f3cc177e4d0)
                        (not E0x7f3cc177eb10))
                   (and E0x7f3cc177eb10
                        (not E0x7f3cc177e4d0)
                        (not E0x7f3cc177e890)))))
      (a!9 (=> v0x7f3cc177fc90_0
               (or (and v0x7f3cc177f790_0
                        E0x7f3cc177fe10
                        (<= v0x7f3cc177fd50_0 v0x7f3cc177fb50_0)
                        (>= v0x7f3cc177fd50_0 v0x7f3cc177fb50_0))
                   (and v0x7f3cc177e1d0_0
                        E0x7f3cc177ffd0
                        (not v0x7f3cc177f650_0)
                        (<= v0x7f3cc177fd50_0 v0x7f3cc177e290_0)
                        (>= v0x7f3cc177fd50_0 v0x7f3cc177e290_0)))))
      (a!10 (=> v0x7f3cc177fc90_0
                (or (and E0x7f3cc177fe10 (not E0x7f3cc177ffd0))
                    (and E0x7f3cc177ffd0 (not E0x7f3cc177fe10))))))
(let ((a!11 (and (=> v0x7f3cc177b050_0
                     (and v0x7f3cc177ad90_0
                          E0x7f3cc177b110
                          (not v0x7f3cc177af10_0)))
                 (=> v0x7f3cc177b050_0 E0x7f3cc177b110)
                 a!1
                 a!2
                 (=> v0x7f3cc177c110_0
                     (and v0x7f3cc177b790_0
                          E0x7f3cc177c1d0
                          (not v0x7f3cc177bfd0_0)))
                 (=> v0x7f3cc177c110_0 E0x7f3cc177c1d0)
                 a!3
                 a!4
                 (=> v0x7f3cc177cfd0_0
                     (and v0x7f3cc177c650_0 E0x7f3cc177d090 v0x7f3cc177ce90_0))
                 (=> v0x7f3cc177cfd0_0 E0x7f3cc177d090)
                 a!5
                 a!6
                 (=> v0x7f3cc177dad0_0
                     (and v0x7f3cc177d450_0
                          E0x7f3cc177db90
                          (not v0x7f3cc177d990_0)))
                 (=> v0x7f3cc177dad0_0 E0x7f3cc177db90)
                 (=> v0x7f3cc177e1d0_0 a!7)
                 a!8
                 (=> v0x7f3cc177f790_0
                     (and v0x7f3cc177e1d0_0 E0x7f3cc177f850 v0x7f3cc177f650_0))
                 (=> v0x7f3cc177f790_0 E0x7f3cc177f850)
                 a!9
                 a!10
                 v0x7f3cc177fc90_0
                 v0x7f3cc1780950_0
                 (= v0x7f3cc177af10_0 (= v0x7f3cc177ae50_0 0.0))
                 (= v0x7f3cc177b350_0 (< v0x7f3cc177a490_0 2.0))
                 (= v0x7f3cc177b510_0 (ite v0x7f3cc177b350_0 1.0 0.0))
                 (= v0x7f3cc177b650_0 (+ v0x7f3cc177b510_0 v0x7f3cc177a490_0))
                 (= v0x7f3cc177bfd0_0 (= v0x7f3cc177bf10_0 0.0))
                 (= v0x7f3cc177c3d0_0 (= v0x7f3cc177a310_0 0.0))
                 (= v0x7f3cc177c510_0 (ite v0x7f3cc177c3d0_0 1.0 0.0))
                 (= v0x7f3cc177ce90_0 (= v0x7f3cc177cdd0_0 0.0))
                 (= v0x7f3cc177d310_0 (= v0x7f3cc177d250_0 0.0))
                 (= v0x7f3cc177d990_0 (= v0x7f3cc177a690_0 0.0))
                 (= v0x7f3cc177dd90_0 (> v0x7f3cc177b850_0 0.0))
                 (= v0x7f3cc177ded0_0 (+ v0x7f3cc177b850_0 (- 1.0)))
                 (= v0x7f3cc177e090_0
                    (ite v0x7f3cc177dd90_0 v0x7f3cc177ded0_0 v0x7f3cc177b850_0))
                 (= v0x7f3cc177f150_0 (not (= v0x7f3cc177e350_0 0.0)))
                 (= v0x7f3cc177f290_0 (= v0x7f3cc177e290_0 0.0))
                 (= v0x7f3cc177f3d0_0 (> v0x7f3cc177e410_0 1.0))
                 (= v0x7f3cc177f510_0 (and v0x7f3cc177f150_0 v0x7f3cc177f290_0))
                 (= v0x7f3cc177f650_0 (and v0x7f3cc177f510_0 v0x7f3cc177f3d0_0))
                 (= v0x7f3cc177fa10_0 (= v0x7f3cc177c710_0 0.0))
                 (= v0x7f3cc177fb50_0
                    (ite v0x7f3cc177fa10_0 1.0 v0x7f3cc177e290_0))
                 (= v0x7f3cc1780450_0 (= v0x7f3cc177e410_0 2.0))
                 (= v0x7f3cc1780590_0 (= v0x7f3cc177fd50_0 0.0))
                 (= v0x7f3cc17806d0_0 (or v0x7f3cc1780590_0 v0x7f3cc1780450_0))
                 (= v0x7f3cc1780810_0 (xor v0x7f3cc17806d0_0 true))
                 (= v0x7f3cc1780950_0 (and v0x7f3cc177f290_0 v0x7f3cc1780810_0)))))
  (=> F0x7f3cc1781890 a!11))))
(assert (=> F0x7f3cc1781890 F0x7f3cc1781590))
(assert (=> F0x7f3cc1781990 (or F0x7f3cc17817d0 F0x7f3cc1781650)))
(assert (=> F0x7f3cc1781950 F0x7f3cc1781890))
(assert (=> pre!entry!0 (=> F0x7f3cc1781710 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f3cc1781590 (>= v0x7f3cc177a590_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f3cc1781590 (<= v0x7f3cc177a490_0 2.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f3cc1781590
        (or (<= v0x7f3cc177a490_0 1.0) (>= v0x7f3cc177a490_0 2.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f3cc1781590 (>= v0x7f3cc177a490_0 0.0))))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f3cc1781590
        (or (>= v0x7f3cc177a490_0 1.0) (<= v0x7f3cc177a490_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f3cc1781990 (>= v0x7f3cc177a750_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f3cc1781990 (<= v0x7f3cc177a650_0 2.0))))
(assert (= lemma!bb1.i.i!2
   (=> F0x7f3cc1781990
       (or (<= v0x7f3cc177a650_0 1.0) (>= v0x7f3cc177a650_0 2.0)))))
(assert (= lemma!bb1.i.i!3 (=> F0x7f3cc1781990 (>= v0x7f3cc177a650_0 0.0))))
(assert (= lemma!bb1.i.i!4
   (=> F0x7f3cc1781990
       (or (>= v0x7f3cc177a650_0 1.0) (<= v0x7f3cc177a650_0 0.0)))))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7f3cc1781950 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
; (init: F0x7f3cc1781710)
; (error: F0x7f3cc1781950)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i23.i.i!0)
