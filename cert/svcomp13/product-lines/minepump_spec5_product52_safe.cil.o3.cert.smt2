(declare-fun post!bb2.i.i28.i.i!0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i28.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f2336470190 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f23364701d0 () Bool)
(declare-fun F0x7f233646fdd0 () Bool)
(declare-fun v0x7f233646f010_0 () Bool)
(declare-fun v0x7f233646ed90_0 () Bool)
(declare-fun v0x7f233646ec50_0 () Bool)
(declare-fun v0x7f233646cb50_0 () Bool)
(declare-fun v0x7f233646ba50_0 () Real)
(declare-fun v0x7f2336469510_0 () Real)
(declare-fun v0x7f233646f150_0 () Bool)
(declare-fun E0x7f233646e410 () Bool)
(declare-fun E0x7f233646dc90 () Bool)
(declare-fun v0x7f2336468d90_0 () Real)
(declare-fun v0x7f233646d4d0_0 () Real)
(declare-fun E0x7f233646d7d0 () Bool)
(declare-fun v0x7f233646d410_0 () Bool)
(declare-fun v0x7f233646c8d0_0 () Bool)
(declare-fun E0x7f233646c090 () Bool)
(declare-fun v0x7f233646cdd0_0 () Bool)
(declare-fun v0x7f233646be10_0 () Real)
(declare-fun v0x7f233646cc90_0 () Real)
(declare-fun E0x7f233646bed0 () Bool)
(declare-fun v0x7f2336469bd0_0 () Real)
(declare-fun v0x7f233646bd50_0 () Bool)
(declare-fun v0x7f233646d050_0 () Bool)
(declare-fun v0x7f233646b510_0 () Bool)
(declare-fun E0x7f233646b710 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f233646b650_0 () Bool)
(declare-fun E0x7f233646b090 () Bool)
(declare-fun E0x7f233646df10 () Bool)
(declare-fun v0x7f2336468c10_0 () Real)
(declare-fun E0x7f233646c6d0 () Bool)
(declare-fun v0x7f233646add0_0 () Real)
(declare-fun v0x7f233646eed0_0 () Bool)
(declare-fun v0x7f233646abd0_0 () Bool)
(declare-fun v0x7f233646ad10_0 () Bool)
(declare-fun E0x7f233646ce90 () Bool)
(declare-fun E0x7f233646c990 () Bool)
(declare-fun v0x7f233646a750_0 () Bool)
(declare-fun v0x7f2336469a10_0 () Bool)
(declare-fun E0x7f233646a950 () Bool)
(declare-fun v0x7f233646a690_0 () Real)
(declare-fun v0x7f233646c4d0_0 () Bool)
(declare-fun E0x7f233646d250 () Bool)
(declare-fun v0x7f2336468e90_0 () Real)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun E0x7f233646a190 () Bool)
(declare-fun E0x7f233646ae90 () Bool)
(declare-fun v0x7f233646d710_0 () Real)
(declare-fun v0x7f2336469e50_0 () Bool)
(declare-fun F0x7f233646ff90 () Bool)
(declare-fun v0x7f233646bc10_0 () Real)
(declare-fun E0x7f233646e210 () Bool)
(declare-fun v0x7f2336469f10_0 () Real)
(declare-fun v0x7f233646d190_0 () Bool)
(declare-fun v0x7f2336469450_0 () Bool)
(declare-fun v0x7f233646c610_0 () Bool)
(declare-fun v0x7f233646ab10_0 () Real)
(declare-fun F0x7f2336470010 () Bool)
(declare-fun v0x7f233646a890_0 () Bool)
(declare-fun v0x7f233646d590_0 () Real)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7f2336468f50_0 () Real)
(declare-fun F0x7f233646fed0 () Bool)
(declare-fun E0x7f2336469fd0 () Bool)
(declare-fun v0x7f2336469710_0 () Bool)
(declare-fun v0x7f233646f290_0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun E0x7f23364697d0 () Bool)
(declare-fun v0x7f23364695d0_0 () Bool)
(declare-fun v0x7f2336468e50_0 () Real)
(declare-fun v0x7f2336469d10_0 () Real)
(declare-fun v0x7f2336467110_0 () Bool)
(declare-fun v0x7f233646d650_0 () Real)
(declare-fun v0x7f233646b910_0 () Bool)
(declare-fun v0x7f2336467010_0 () Real)
(declare-fun F0x7f23364700d0 () Bool)

(assert (=> F0x7f23364700d0
    (and v0x7f2336467110_0
         (<= v0x7f2336468e50_0 1.0)
         (>= v0x7f2336468e50_0 1.0)
         (<= v0x7f2336468f50_0 0.0)
         (>= v0x7f2336468f50_0 0.0)
         (<= v0x7f2336467010_0 1.0)
         (>= v0x7f2336467010_0 1.0))))
(assert (=> F0x7f23364700d0 F0x7f2336470010))
(assert (let ((a!1 (=> v0x7f2336469e50_0
               (or (and v0x7f2336469710_0
                        E0x7f2336469fd0
                        (<= v0x7f2336469f10_0 v0x7f2336469d10_0)
                        (>= v0x7f2336469f10_0 v0x7f2336469d10_0))
                   (and v0x7f2336469450_0
                        E0x7f233646a190
                        v0x7f23364695d0_0
                        (<= v0x7f2336469f10_0 v0x7f2336468e90_0)
                        (>= v0x7f2336469f10_0 v0x7f2336468e90_0)))))
      (a!2 (=> v0x7f2336469e50_0
               (or (and E0x7f2336469fd0 (not E0x7f233646a190))
                   (and E0x7f233646a190 (not E0x7f2336469fd0)))))
      (a!3 (=> v0x7f233646ad10_0
               (or (and v0x7f233646a890_0
                        E0x7f233646ae90
                        v0x7f233646abd0_0
                        (<= v0x7f233646add0_0 v0x7f2336468c10_0)
                        (>= v0x7f233646add0_0 v0x7f2336468c10_0))
                   (and v0x7f2336469e50_0
                        E0x7f233646b090
                        (not v0x7f233646a750_0)
                        (<= v0x7f233646add0_0 1.0)
                        (>= v0x7f233646add0_0 1.0)))))
      (a!4 (=> v0x7f233646ad10_0
               (or (and E0x7f233646ae90 (not E0x7f233646b090))
                   (and E0x7f233646b090 (not E0x7f233646ae90)))))
      (a!5 (=> v0x7f233646bd50_0
               (or (and v0x7f233646b650_0
                        E0x7f233646bed0
                        (<= v0x7f233646be10_0 v0x7f233646bc10_0)
                        (>= v0x7f233646be10_0 v0x7f233646bc10_0))
                   (and v0x7f233646ad10_0
                        E0x7f233646c090
                        v0x7f233646b510_0
                        (<= v0x7f233646be10_0 v0x7f2336469f10_0)
                        (>= v0x7f233646be10_0 v0x7f2336469f10_0)))))
      (a!6 (=> v0x7f233646bd50_0
               (or (and E0x7f233646bed0 (not E0x7f233646c090))
                   (and E0x7f233646c090 (not E0x7f233646bed0)))))
      (a!7 (or (and v0x7f233646c8d0_0
                    E0x7f233646d7d0
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (<= v0x7f233646d710_0 v0x7f233646cc90_0)
                    (>= v0x7f233646d710_0 v0x7f233646cc90_0))
               (and v0x7f233646d190_0
                    E0x7f233646dc90
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (and (<= v0x7f233646d710_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d710_0 v0x7f2336468d90_0)))
               (and v0x7f233646cdd0_0
                    E0x7f233646df10
                    (not v0x7f233646d050_0)
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (and (<= v0x7f233646d710_0 0.0) (>= v0x7f233646d710_0 0.0)))
               (and v0x7f233646bd50_0
                    E0x7f233646e210
                    v0x7f233646c4d0_0
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (and (<= v0x7f233646d710_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d710_0 v0x7f2336468d90_0)))
               (and v0x7f233646a890_0
                    E0x7f233646e410
                    (not v0x7f233646abd0_0)
                    (<= v0x7f233646d4d0_0 0.0)
                    (>= v0x7f233646d4d0_0 0.0)
                    (<= v0x7f233646d590_0 0.0)
                    (>= v0x7f233646d590_0 0.0)
                    (<= v0x7f233646d650_0 v0x7f2336469f10_0)
                    (>= v0x7f233646d650_0 v0x7f2336469f10_0)
                    (and (<= v0x7f233646d710_0 0.0) (>= v0x7f233646d710_0 0.0)))))
      (a!8 (=> v0x7f233646d410_0
               (or (and E0x7f233646d7d0
                        (not E0x7f233646dc90)
                        (not E0x7f233646df10)
                        (not E0x7f233646e210)
                        (not E0x7f233646e410))
                   (and E0x7f233646dc90
                        (not E0x7f233646d7d0)
                        (not E0x7f233646df10)
                        (not E0x7f233646e210)
                        (not E0x7f233646e410))
                   (and E0x7f233646df10
                        (not E0x7f233646d7d0)
                        (not E0x7f233646dc90)
                        (not E0x7f233646e210)
                        (not E0x7f233646e410))
                   (and E0x7f233646e210
                        (not E0x7f233646d7d0)
                        (not E0x7f233646dc90)
                        (not E0x7f233646df10)
                        (not E0x7f233646e410))
                   (and E0x7f233646e410
                        (not E0x7f233646d7d0)
                        (not E0x7f233646dc90)
                        (not E0x7f233646df10)
                        (not E0x7f233646e210))))))
(let ((a!9 (and (=> v0x7f2336469710_0
                    (and v0x7f2336469450_0
                         E0x7f23364697d0
                         (not v0x7f23364695d0_0)))
                (=> v0x7f2336469710_0 E0x7f23364697d0)
                a!1
                a!2
                (=> v0x7f233646a890_0
                    (and v0x7f2336469e50_0 E0x7f233646a950 v0x7f233646a750_0))
                (=> v0x7f233646a890_0 E0x7f233646a950)
                a!3
                a!4
                (=> v0x7f233646b650_0
                    (and v0x7f233646ad10_0
                         E0x7f233646b710
                         (not v0x7f233646b510_0)))
                (=> v0x7f233646b650_0 E0x7f233646b710)
                a!5
                a!6
                (=> v0x7f233646c610_0
                    (and v0x7f233646bd50_0
                         E0x7f233646c6d0
                         (not v0x7f233646c4d0_0)))
                (=> v0x7f233646c610_0 E0x7f233646c6d0)
                (=> v0x7f233646c8d0_0
                    (and v0x7f233646c610_0 E0x7f233646c990 v0x7f233646b510_0))
                (=> v0x7f233646c8d0_0 E0x7f233646c990)
                (=> v0x7f233646cdd0_0
                    (and v0x7f233646c610_0
                         E0x7f233646ce90
                         (not v0x7f233646b510_0)))
                (=> v0x7f233646cdd0_0 E0x7f233646ce90)
                (=> v0x7f233646d190_0
                    (and v0x7f233646cdd0_0 E0x7f233646d250 v0x7f233646d050_0))
                (=> v0x7f233646d190_0 E0x7f233646d250)
                (=> v0x7f233646d410_0 a!7)
                a!8
                v0x7f233646d410_0
                (not v0x7f233646f290_0)
                (<= v0x7f2336468e50_0 v0x7f233646d590_0)
                (>= v0x7f2336468e50_0 v0x7f233646d590_0)
                (<= v0x7f2336468f50_0 v0x7f233646d710_0)
                (>= v0x7f2336468f50_0 v0x7f233646d710_0)
                (<= v0x7f2336467010_0 v0x7f233646d650_0)
                (>= v0x7f2336467010_0 v0x7f233646d650_0)
                (= v0x7f23364695d0_0 (= v0x7f2336469510_0 0.0))
                (= v0x7f2336469a10_0 (< v0x7f2336468e90_0 2.0))
                (= v0x7f2336469bd0_0 (ite v0x7f2336469a10_0 1.0 0.0))
                (= v0x7f2336469d10_0 (+ v0x7f2336469bd0_0 v0x7f2336468e90_0))
                (= v0x7f233646a750_0 (= v0x7f233646a690_0 0.0))
                (= v0x7f233646abd0_0 (= v0x7f233646ab10_0 0.0))
                (= v0x7f233646b510_0 (= v0x7f2336468d90_0 0.0))
                (= v0x7f233646b910_0 (> v0x7f2336469f10_0 0.0))
                (= v0x7f233646ba50_0 (+ v0x7f2336469f10_0 (- 1.0)))
                (= v0x7f233646bc10_0
                   (ite v0x7f233646b910_0 v0x7f233646ba50_0 v0x7f2336469f10_0))
                (= v0x7f233646c4d0_0 (= v0x7f233646add0_0 0.0))
                (= v0x7f233646cb50_0 (> v0x7f233646be10_0 1.0))
                (= v0x7f233646cc90_0
                   (ite v0x7f233646cb50_0 1.0 v0x7f2336468d90_0))
                (= v0x7f233646d050_0 (= v0x7f233646be10_0 0.0))
                (= v0x7f233646ec50_0 (= v0x7f233646d650_0 2.0))
                (= v0x7f233646ed90_0 (= v0x7f233646d710_0 0.0))
                (= v0x7f233646eed0_0 (or v0x7f233646ed90_0 v0x7f233646ec50_0))
                (= v0x7f233646f010_0 (xor v0x7f233646eed0_0 true))
                (= v0x7f233646f150_0 (= v0x7f233646d4d0_0 0.0))
                (= v0x7f233646f290_0 (and v0x7f233646f150_0 v0x7f233646f010_0)))))
  (=> F0x7f233646ff90 a!9))))
(assert (=> F0x7f233646ff90 F0x7f233646fed0))
(assert (let ((a!1 (=> v0x7f2336469e50_0
               (or (and v0x7f2336469710_0
                        E0x7f2336469fd0
                        (<= v0x7f2336469f10_0 v0x7f2336469d10_0)
                        (>= v0x7f2336469f10_0 v0x7f2336469d10_0))
                   (and v0x7f2336469450_0
                        E0x7f233646a190
                        v0x7f23364695d0_0
                        (<= v0x7f2336469f10_0 v0x7f2336468e90_0)
                        (>= v0x7f2336469f10_0 v0x7f2336468e90_0)))))
      (a!2 (=> v0x7f2336469e50_0
               (or (and E0x7f2336469fd0 (not E0x7f233646a190))
                   (and E0x7f233646a190 (not E0x7f2336469fd0)))))
      (a!3 (=> v0x7f233646ad10_0
               (or (and v0x7f233646a890_0
                        E0x7f233646ae90
                        v0x7f233646abd0_0
                        (<= v0x7f233646add0_0 v0x7f2336468c10_0)
                        (>= v0x7f233646add0_0 v0x7f2336468c10_0))
                   (and v0x7f2336469e50_0
                        E0x7f233646b090
                        (not v0x7f233646a750_0)
                        (<= v0x7f233646add0_0 1.0)
                        (>= v0x7f233646add0_0 1.0)))))
      (a!4 (=> v0x7f233646ad10_0
               (or (and E0x7f233646ae90 (not E0x7f233646b090))
                   (and E0x7f233646b090 (not E0x7f233646ae90)))))
      (a!5 (=> v0x7f233646bd50_0
               (or (and v0x7f233646b650_0
                        E0x7f233646bed0
                        (<= v0x7f233646be10_0 v0x7f233646bc10_0)
                        (>= v0x7f233646be10_0 v0x7f233646bc10_0))
                   (and v0x7f233646ad10_0
                        E0x7f233646c090
                        v0x7f233646b510_0
                        (<= v0x7f233646be10_0 v0x7f2336469f10_0)
                        (>= v0x7f233646be10_0 v0x7f2336469f10_0)))))
      (a!6 (=> v0x7f233646bd50_0
               (or (and E0x7f233646bed0 (not E0x7f233646c090))
                   (and E0x7f233646c090 (not E0x7f233646bed0)))))
      (a!7 (or (and v0x7f233646c8d0_0
                    E0x7f233646d7d0
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (<= v0x7f233646d710_0 v0x7f233646cc90_0)
                    (>= v0x7f233646d710_0 v0x7f233646cc90_0))
               (and v0x7f233646d190_0
                    E0x7f233646dc90
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (and (<= v0x7f233646d710_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d710_0 v0x7f2336468d90_0)))
               (and v0x7f233646cdd0_0
                    E0x7f233646df10
                    (not v0x7f233646d050_0)
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (and (<= v0x7f233646d710_0 0.0) (>= v0x7f233646d710_0 0.0)))
               (and v0x7f233646bd50_0
                    E0x7f233646e210
                    v0x7f233646c4d0_0
                    (and (<= v0x7f233646d4d0_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d4d0_0 v0x7f2336468d90_0))
                    (and (<= v0x7f233646d590_0 v0x7f233646add0_0)
                         (>= v0x7f233646d590_0 v0x7f233646add0_0))
                    (and (<= v0x7f233646d650_0 v0x7f233646be10_0)
                         (>= v0x7f233646d650_0 v0x7f233646be10_0))
                    (and (<= v0x7f233646d710_0 v0x7f2336468d90_0)
                         (>= v0x7f233646d710_0 v0x7f2336468d90_0)))
               (and v0x7f233646a890_0
                    E0x7f233646e410
                    (not v0x7f233646abd0_0)
                    (<= v0x7f233646d4d0_0 0.0)
                    (>= v0x7f233646d4d0_0 0.0)
                    (<= v0x7f233646d590_0 0.0)
                    (>= v0x7f233646d590_0 0.0)
                    (<= v0x7f233646d650_0 v0x7f2336469f10_0)
                    (>= v0x7f233646d650_0 v0x7f2336469f10_0)
                    (and (<= v0x7f233646d710_0 0.0) (>= v0x7f233646d710_0 0.0)))))
      (a!8 (=> v0x7f233646d410_0
               (or (and E0x7f233646d7d0
                        (not E0x7f233646dc90)
                        (not E0x7f233646df10)
                        (not E0x7f233646e210)
                        (not E0x7f233646e410))
                   (and E0x7f233646dc90
                        (not E0x7f233646d7d0)
                        (not E0x7f233646df10)
                        (not E0x7f233646e210)
                        (not E0x7f233646e410))
                   (and E0x7f233646df10
                        (not E0x7f233646d7d0)
                        (not E0x7f233646dc90)
                        (not E0x7f233646e210)
                        (not E0x7f233646e410))
                   (and E0x7f233646e210
                        (not E0x7f233646d7d0)
                        (not E0x7f233646dc90)
                        (not E0x7f233646df10)
                        (not E0x7f233646e410))
                   (and E0x7f233646e410
                        (not E0x7f233646d7d0)
                        (not E0x7f233646dc90)
                        (not E0x7f233646df10)
                        (not E0x7f233646e210))))))
(let ((a!9 (and (=> v0x7f2336469710_0
                    (and v0x7f2336469450_0
                         E0x7f23364697d0
                         (not v0x7f23364695d0_0)))
                (=> v0x7f2336469710_0 E0x7f23364697d0)
                a!1
                a!2
                (=> v0x7f233646a890_0
                    (and v0x7f2336469e50_0 E0x7f233646a950 v0x7f233646a750_0))
                (=> v0x7f233646a890_0 E0x7f233646a950)
                a!3
                a!4
                (=> v0x7f233646b650_0
                    (and v0x7f233646ad10_0
                         E0x7f233646b710
                         (not v0x7f233646b510_0)))
                (=> v0x7f233646b650_0 E0x7f233646b710)
                a!5
                a!6
                (=> v0x7f233646c610_0
                    (and v0x7f233646bd50_0
                         E0x7f233646c6d0
                         (not v0x7f233646c4d0_0)))
                (=> v0x7f233646c610_0 E0x7f233646c6d0)
                (=> v0x7f233646c8d0_0
                    (and v0x7f233646c610_0 E0x7f233646c990 v0x7f233646b510_0))
                (=> v0x7f233646c8d0_0 E0x7f233646c990)
                (=> v0x7f233646cdd0_0
                    (and v0x7f233646c610_0
                         E0x7f233646ce90
                         (not v0x7f233646b510_0)))
                (=> v0x7f233646cdd0_0 E0x7f233646ce90)
                (=> v0x7f233646d190_0
                    (and v0x7f233646cdd0_0 E0x7f233646d250 v0x7f233646d050_0))
                (=> v0x7f233646d190_0 E0x7f233646d250)
                (=> v0x7f233646d410_0 a!7)
                a!8
                v0x7f233646d410_0
                v0x7f233646f290_0
                (= v0x7f23364695d0_0 (= v0x7f2336469510_0 0.0))
                (= v0x7f2336469a10_0 (< v0x7f2336468e90_0 2.0))
                (= v0x7f2336469bd0_0 (ite v0x7f2336469a10_0 1.0 0.0))
                (= v0x7f2336469d10_0 (+ v0x7f2336469bd0_0 v0x7f2336468e90_0))
                (= v0x7f233646a750_0 (= v0x7f233646a690_0 0.0))
                (= v0x7f233646abd0_0 (= v0x7f233646ab10_0 0.0))
                (= v0x7f233646b510_0 (= v0x7f2336468d90_0 0.0))
                (= v0x7f233646b910_0 (> v0x7f2336469f10_0 0.0))
                (= v0x7f233646ba50_0 (+ v0x7f2336469f10_0 (- 1.0)))
                (= v0x7f233646bc10_0
                   (ite v0x7f233646b910_0 v0x7f233646ba50_0 v0x7f2336469f10_0))
                (= v0x7f233646c4d0_0 (= v0x7f233646add0_0 0.0))
                (= v0x7f233646cb50_0 (> v0x7f233646be10_0 1.0))
                (= v0x7f233646cc90_0
                   (ite v0x7f233646cb50_0 1.0 v0x7f2336468d90_0))
                (= v0x7f233646d050_0 (= v0x7f233646be10_0 0.0))
                (= v0x7f233646ec50_0 (= v0x7f233646d650_0 2.0))
                (= v0x7f233646ed90_0 (= v0x7f233646d710_0 0.0))
                (= v0x7f233646eed0_0 (or v0x7f233646ed90_0 v0x7f233646ec50_0))
                (= v0x7f233646f010_0 (xor v0x7f233646eed0_0 true))
                (= v0x7f233646f150_0 (= v0x7f233646d4d0_0 0.0))
                (= v0x7f233646f290_0 (and v0x7f233646f150_0 v0x7f233646f010_0)))))
  (=> F0x7f233646fdd0 a!9))))
(assert (=> F0x7f233646fdd0 F0x7f233646fed0))
(assert (=> F0x7f23364701d0 (or F0x7f23364700d0 F0x7f233646ff90)))
(assert (=> F0x7f2336470190 F0x7f233646fdd0))
(assert (=> pre!entry!0 (=> F0x7f2336470010 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f233646fed0 (>= v0x7f2336468c10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f233646fed0 (<= v0x7f2336468e90_0 2.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f233646fed0
        (or (<= v0x7f2336468e90_0 1.0) (>= v0x7f2336468e90_0 2.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7f233646fed0 (>= v0x7f2336468e90_0 0.0))))
(assert (=> pre!bb1.i.i!4
    (=> F0x7f233646fed0
        (or (>= v0x7f2336468e90_0 1.0) (<= v0x7f2336468e90_0 0.0)))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7f233646fed0
        (or (<= v0x7f2336468e90_0 1.0)
            (>= v0x7f2336468d90_0 1.0)
            (<= v0x7f2336468c10_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f23364701d0 (>= v0x7f2336468e50_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f23364701d0 (<= v0x7f2336467010_0 2.0))))
(assert (= lemma!bb1.i.i!2
   (=> F0x7f23364701d0
       (or (<= v0x7f2336467010_0 1.0) (>= v0x7f2336467010_0 2.0)))))
(assert (= lemma!bb1.i.i!3 (=> F0x7f23364701d0 (>= v0x7f2336467010_0 0.0))))
(assert (= lemma!bb1.i.i!4
   (=> F0x7f23364701d0
       (or (>= v0x7f2336467010_0 1.0) (<= v0x7f2336467010_0 0.0)))))
(assert (= lemma!bb1.i.i!5
   (=> F0x7f23364701d0
       (or (<= v0x7f2336467010_0 1.0)
           (>= v0x7f2336468f50_0 1.0)
           (<= v0x7f2336468e50_0 0.0)))))
(assert (= lemma!bb2.i.i28.i.i!0 (=> F0x7f2336470190 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb2.i.i28.i.i!0) (not lemma!bb2.i.i28.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
; (init: F0x7f2336470010)
; (error: F0x7f2336470190)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i28.i.i!0)
