(declare-fun post!bb2.i.i25.i.i!0 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i25.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fcaba57f5d0 () Bool)
(declare-fun F0x7fcaba57f610 () Bool)
(declare-fun F0x7fcaba57f510 () Bool)
(declare-fun F0x7fcaba57f2d0 () Bool)
(declare-fun v0x7fcaba57e590_0 () Bool)
(declare-fun v0x7fcaba57e450_0 () Bool)
(declare-fun v0x7fcaba57e1d0_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7fcaba57c790_0 () Bool)
(declare-fun v0x7fcaba57e310_0 () Bool)
(declare-fun v0x7fcaba57b910_0 () Bool)
(declare-fun v0x7fcaba57aa50_0 () Real)
(declare-fun v0x7fcaba57a890_0 () Bool)
(declare-fun v0x7fcaba57a390_0 () Real)
(declare-fun v0x7fcaba579a90_0 () Real)
(declare-fun v0x7fcaba57ccd0_0 () Bool)
(declare-fun v0x7fcaba57cfd0_0 () Real)
(declare-fun v0x7fcaba57ce10_0 () Real)
(declare-fun v0x7fcaba57d650_0 () Real)
(declare-fun E0x7fcaba57d710 () Bool)
(declare-fun v0x7fcaba57d110_0 () Bool)
(declare-fun v0x7fcaba57d590_0 () Real)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun E0x7fcaba57d310 () Bool)
(declare-fun E0x7fcaba57cad0 () Bool)
(declare-fun v0x7fcaba57ca10_0 () Bool)
(declare-fun v0x7fcaba57d250_0 () Bool)
(declare-fun v0x7fcaba57c510_0 () Bool)
(declare-fun E0x7fcaba57bed0 () Bool)
(declare-fun E0x7fcaba57bd10 () Bool)
(declare-fun v0x7fcaba57b650_0 () Bool)
(declare-fun E0x7fcaba57b010 () Bool)
(declare-fun v0x7fcaba57ab90_0 () Real)
(declare-fun E0x7fcaba57ae50 () Bool)
(declare-fun v0x7fcaba57bb90_0 () Bool)
(declare-fun v0x7fcaba579d10_0 () Real)
(declare-fun v0x7fcaba57acd0_0 () Bool)
(declare-fun v0x7fcaba57c3d0_0 () Bool)
(declare-fun v0x7fcaba57e6d0_0 () Bool)
(declare-fun E0x7fcaba57c5d0 () Bool)
(declare-fun E0x7fcaba57a650 () Bool)
(declare-fun v0x7fcaba57a450_0 () Bool)
(declare-fun v0x7fcaba579c10_0 () Real)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun v0x7fcaba57ba50_0 () Real)
(declare-fun v0x7fcaba57c8d0_0 () Real)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun v0x7fcaba57ad90_0 () Real)
(declare-fun v0x7fcaba57d4d0_0 () Bool)
(declare-fun E0x7fcaba57dc90 () Bool)
(declare-fun v0x7fcaba57a2d0_0 () Bool)
(declare-fun v0x7fcaba57a590_0 () Bool)
(declare-fun F0x7fcaba57f390 () Bool)
(declare-fun v0x7fcaba57b510_0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun F0x7fcaba57f290 () Bool)
(declare-fun v0x7fcaba57b450_0 () Real)
(declare-fun v0x7fcaba57bc50_0 () Real)
(declare-fun v0x7fcaba579dd0_0 () Real)
(declare-fun v0x7fcaba579cd0_0 () Real)
(declare-fun v0x7fcaba578110_0 () Bool)
(declare-fun E0x7fcaba57b710 () Bool)
(declare-fun v0x7fcaba578010_0 () Real)
(declare-fun E0x7fcaba57d9d0 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun F0x7fcaba57f4d0 () Bool)

(assert (=> F0x7fcaba57f4d0
    (and v0x7fcaba578110_0
         (<= v0x7fcaba579cd0_0 0.0)
         (>= v0x7fcaba579cd0_0 0.0)
         (<= v0x7fcaba579dd0_0 0.0)
         (>= v0x7fcaba579dd0_0 0.0)
         (<= v0x7fcaba578010_0 1.0)
         (>= v0x7fcaba578010_0 1.0))))
(assert (=> F0x7fcaba57f4d0 F0x7fcaba57f290))
(assert (let ((a!1 (=> v0x7fcaba57acd0_0
               (or (and v0x7fcaba57a590_0
                        E0x7fcaba57ae50
                        (<= v0x7fcaba57ad90_0 v0x7fcaba57ab90_0)
                        (>= v0x7fcaba57ad90_0 v0x7fcaba57ab90_0))
                   (and v0x7fcaba57a2d0_0
                        E0x7fcaba57b010
                        v0x7fcaba57a450_0
                        (<= v0x7fcaba57ad90_0 v0x7fcaba579d10_0)
                        (>= v0x7fcaba57ad90_0 v0x7fcaba579d10_0)))))
      (a!2 (=> v0x7fcaba57acd0_0
               (or (and E0x7fcaba57ae50 (not E0x7fcaba57b010))
                   (and E0x7fcaba57b010 (not E0x7fcaba57ae50)))))
      (a!3 (=> v0x7fcaba57bb90_0
               (or (and v0x7fcaba57b650_0
                        E0x7fcaba57bd10
                        (<= v0x7fcaba57bc50_0 v0x7fcaba57ba50_0)
                        (>= v0x7fcaba57bc50_0 v0x7fcaba57ba50_0))
                   (and v0x7fcaba57acd0_0
                        E0x7fcaba57bed0
                        v0x7fcaba57b510_0
                        (<= v0x7fcaba57bc50_0 v0x7fcaba579c10_0)
                        (>= v0x7fcaba57bc50_0 v0x7fcaba579c10_0)))))
      (a!4 (=> v0x7fcaba57bb90_0
               (or (and E0x7fcaba57bd10 (not E0x7fcaba57bed0))
                   (and E0x7fcaba57bed0 (not E0x7fcaba57bd10)))))
      (a!5 (or (and v0x7fcaba57c510_0
                    E0x7fcaba57d710
                    (<= v0x7fcaba57d590_0 v0x7fcaba57ad90_0)
                    (>= v0x7fcaba57d590_0 v0x7fcaba57ad90_0)
                    (<= v0x7fcaba57d650_0 v0x7fcaba57c8d0_0)
                    (>= v0x7fcaba57d650_0 v0x7fcaba57c8d0_0))
               (and v0x7fcaba57d250_0
                    E0x7fcaba57d9d0
                    (and (<= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0)
                         (>= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0))
                    (<= v0x7fcaba57d650_0 v0x7fcaba579a90_0)
                    (>= v0x7fcaba57d650_0 v0x7fcaba579a90_0))
               (and v0x7fcaba57ca10_0
                    E0x7fcaba57dc90
                    (not v0x7fcaba57d110_0)
                    (and (<= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0)
                         (>= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0))
                    (<= v0x7fcaba57d650_0 0.0)
                    (>= v0x7fcaba57d650_0 0.0))))
      (a!6 (=> v0x7fcaba57d4d0_0
               (or (and E0x7fcaba57d710
                        (not E0x7fcaba57d9d0)
                        (not E0x7fcaba57dc90))
                   (and E0x7fcaba57d9d0
                        (not E0x7fcaba57d710)
                        (not E0x7fcaba57dc90))
                   (and E0x7fcaba57dc90
                        (not E0x7fcaba57d710)
                        (not E0x7fcaba57d9d0))))))
(let ((a!7 (and (=> v0x7fcaba57a590_0
                    (and v0x7fcaba57a2d0_0
                         E0x7fcaba57a650
                         (not v0x7fcaba57a450_0)))
                (=> v0x7fcaba57a590_0 E0x7fcaba57a650)
                a!1
                a!2
                (=> v0x7fcaba57b650_0
                    (and v0x7fcaba57acd0_0
                         E0x7fcaba57b710
                         (not v0x7fcaba57b510_0)))
                (=> v0x7fcaba57b650_0 E0x7fcaba57b710)
                a!3
                a!4
                (=> v0x7fcaba57c510_0
                    (and v0x7fcaba57bb90_0 E0x7fcaba57c5d0 v0x7fcaba57c3d0_0))
                (=> v0x7fcaba57c510_0 E0x7fcaba57c5d0)
                (=> v0x7fcaba57ca10_0
                    (and v0x7fcaba57bb90_0
                         E0x7fcaba57cad0
                         (not v0x7fcaba57c3d0_0)))
                (=> v0x7fcaba57ca10_0 E0x7fcaba57cad0)
                (=> v0x7fcaba57d250_0
                    (and v0x7fcaba57ca10_0 E0x7fcaba57d310 v0x7fcaba57d110_0))
                (=> v0x7fcaba57d250_0 E0x7fcaba57d310)
                (=> v0x7fcaba57d4d0_0 a!5)
                a!6
                v0x7fcaba57d4d0_0
                (not v0x7fcaba57e6d0_0)
                (<= v0x7fcaba579cd0_0 v0x7fcaba57d650_0)
                (>= v0x7fcaba579cd0_0 v0x7fcaba57d650_0)
                (<= v0x7fcaba579dd0_0 v0x7fcaba57bc50_0)
                (>= v0x7fcaba579dd0_0 v0x7fcaba57bc50_0)
                (<= v0x7fcaba578010_0 v0x7fcaba57d590_0)
                (>= v0x7fcaba578010_0 v0x7fcaba57d590_0)
                (= v0x7fcaba57a450_0 (= v0x7fcaba57a390_0 0.0))
                (= v0x7fcaba57a890_0 (< v0x7fcaba579d10_0 2.0))
                (= v0x7fcaba57aa50_0 (ite v0x7fcaba57a890_0 1.0 0.0))
                (= v0x7fcaba57ab90_0 (+ v0x7fcaba57aa50_0 v0x7fcaba579d10_0))
                (= v0x7fcaba57b510_0 (= v0x7fcaba57b450_0 0.0))
                (= v0x7fcaba57b910_0 (= v0x7fcaba579c10_0 0.0))
                (= v0x7fcaba57ba50_0 (ite v0x7fcaba57b910_0 1.0 0.0))
                (= v0x7fcaba57c3d0_0 (= v0x7fcaba579a90_0 0.0))
                (= v0x7fcaba57c790_0 (> v0x7fcaba57ad90_0 1.0))
                (= v0x7fcaba57c8d0_0
                   (ite v0x7fcaba57c790_0 1.0 v0x7fcaba579a90_0))
                (= v0x7fcaba57ccd0_0 (> v0x7fcaba57ad90_0 0.0))
                (= v0x7fcaba57ce10_0 (+ v0x7fcaba57ad90_0 (- 1.0)))
                (= v0x7fcaba57cfd0_0
                   (ite v0x7fcaba57ccd0_0 v0x7fcaba57ce10_0 v0x7fcaba57ad90_0))
                (= v0x7fcaba57d110_0 (= v0x7fcaba57bc50_0 0.0))
                (= v0x7fcaba57e1d0_0 (= v0x7fcaba57d590_0 2.0))
                (= v0x7fcaba57e310_0 (= v0x7fcaba57d650_0 0.0))
                (= v0x7fcaba57e450_0 (or v0x7fcaba57e310_0 v0x7fcaba57e1d0_0))
                (= v0x7fcaba57e590_0 (xor v0x7fcaba57e450_0 true))
                (= v0x7fcaba57e6d0_0 (and v0x7fcaba57c3d0_0 v0x7fcaba57e590_0)))))
  (=> F0x7fcaba57f390 a!7))))
(assert (=> F0x7fcaba57f390 F0x7fcaba57f2d0))
(assert (let ((a!1 (=> v0x7fcaba57acd0_0
               (or (and v0x7fcaba57a590_0
                        E0x7fcaba57ae50
                        (<= v0x7fcaba57ad90_0 v0x7fcaba57ab90_0)
                        (>= v0x7fcaba57ad90_0 v0x7fcaba57ab90_0))
                   (and v0x7fcaba57a2d0_0
                        E0x7fcaba57b010
                        v0x7fcaba57a450_0
                        (<= v0x7fcaba57ad90_0 v0x7fcaba579d10_0)
                        (>= v0x7fcaba57ad90_0 v0x7fcaba579d10_0)))))
      (a!2 (=> v0x7fcaba57acd0_0
               (or (and E0x7fcaba57ae50 (not E0x7fcaba57b010))
                   (and E0x7fcaba57b010 (not E0x7fcaba57ae50)))))
      (a!3 (=> v0x7fcaba57bb90_0
               (or (and v0x7fcaba57b650_0
                        E0x7fcaba57bd10
                        (<= v0x7fcaba57bc50_0 v0x7fcaba57ba50_0)
                        (>= v0x7fcaba57bc50_0 v0x7fcaba57ba50_0))
                   (and v0x7fcaba57acd0_0
                        E0x7fcaba57bed0
                        v0x7fcaba57b510_0
                        (<= v0x7fcaba57bc50_0 v0x7fcaba579c10_0)
                        (>= v0x7fcaba57bc50_0 v0x7fcaba579c10_0)))))
      (a!4 (=> v0x7fcaba57bb90_0
               (or (and E0x7fcaba57bd10 (not E0x7fcaba57bed0))
                   (and E0x7fcaba57bed0 (not E0x7fcaba57bd10)))))
      (a!5 (or (and v0x7fcaba57c510_0
                    E0x7fcaba57d710
                    (<= v0x7fcaba57d590_0 v0x7fcaba57ad90_0)
                    (>= v0x7fcaba57d590_0 v0x7fcaba57ad90_0)
                    (<= v0x7fcaba57d650_0 v0x7fcaba57c8d0_0)
                    (>= v0x7fcaba57d650_0 v0x7fcaba57c8d0_0))
               (and v0x7fcaba57d250_0
                    E0x7fcaba57d9d0
                    (and (<= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0)
                         (>= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0))
                    (<= v0x7fcaba57d650_0 v0x7fcaba579a90_0)
                    (>= v0x7fcaba57d650_0 v0x7fcaba579a90_0))
               (and v0x7fcaba57ca10_0
                    E0x7fcaba57dc90
                    (not v0x7fcaba57d110_0)
                    (and (<= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0)
                         (>= v0x7fcaba57d590_0 v0x7fcaba57cfd0_0))
                    (<= v0x7fcaba57d650_0 0.0)
                    (>= v0x7fcaba57d650_0 0.0))))
      (a!6 (=> v0x7fcaba57d4d0_0
               (or (and E0x7fcaba57d710
                        (not E0x7fcaba57d9d0)
                        (not E0x7fcaba57dc90))
                   (and E0x7fcaba57d9d0
                        (not E0x7fcaba57d710)
                        (not E0x7fcaba57dc90))
                   (and E0x7fcaba57dc90
                        (not E0x7fcaba57d710)
                        (not E0x7fcaba57d9d0))))))
(let ((a!7 (and (=> v0x7fcaba57a590_0
                    (and v0x7fcaba57a2d0_0
                         E0x7fcaba57a650
                         (not v0x7fcaba57a450_0)))
                (=> v0x7fcaba57a590_0 E0x7fcaba57a650)
                a!1
                a!2
                (=> v0x7fcaba57b650_0
                    (and v0x7fcaba57acd0_0
                         E0x7fcaba57b710
                         (not v0x7fcaba57b510_0)))
                (=> v0x7fcaba57b650_0 E0x7fcaba57b710)
                a!3
                a!4
                (=> v0x7fcaba57c510_0
                    (and v0x7fcaba57bb90_0 E0x7fcaba57c5d0 v0x7fcaba57c3d0_0))
                (=> v0x7fcaba57c510_0 E0x7fcaba57c5d0)
                (=> v0x7fcaba57ca10_0
                    (and v0x7fcaba57bb90_0
                         E0x7fcaba57cad0
                         (not v0x7fcaba57c3d0_0)))
                (=> v0x7fcaba57ca10_0 E0x7fcaba57cad0)
                (=> v0x7fcaba57d250_0
                    (and v0x7fcaba57ca10_0 E0x7fcaba57d310 v0x7fcaba57d110_0))
                (=> v0x7fcaba57d250_0 E0x7fcaba57d310)
                (=> v0x7fcaba57d4d0_0 a!5)
                a!6
                v0x7fcaba57d4d0_0
                v0x7fcaba57e6d0_0
                (= v0x7fcaba57a450_0 (= v0x7fcaba57a390_0 0.0))
                (= v0x7fcaba57a890_0 (< v0x7fcaba579d10_0 2.0))
                (= v0x7fcaba57aa50_0 (ite v0x7fcaba57a890_0 1.0 0.0))
                (= v0x7fcaba57ab90_0 (+ v0x7fcaba57aa50_0 v0x7fcaba579d10_0))
                (= v0x7fcaba57b510_0 (= v0x7fcaba57b450_0 0.0))
                (= v0x7fcaba57b910_0 (= v0x7fcaba579c10_0 0.0))
                (= v0x7fcaba57ba50_0 (ite v0x7fcaba57b910_0 1.0 0.0))
                (= v0x7fcaba57c3d0_0 (= v0x7fcaba579a90_0 0.0))
                (= v0x7fcaba57c790_0 (> v0x7fcaba57ad90_0 1.0))
                (= v0x7fcaba57c8d0_0
                   (ite v0x7fcaba57c790_0 1.0 v0x7fcaba579a90_0))
                (= v0x7fcaba57ccd0_0 (> v0x7fcaba57ad90_0 0.0))
                (= v0x7fcaba57ce10_0 (+ v0x7fcaba57ad90_0 (- 1.0)))
                (= v0x7fcaba57cfd0_0
                   (ite v0x7fcaba57ccd0_0 v0x7fcaba57ce10_0 v0x7fcaba57ad90_0))
                (= v0x7fcaba57d110_0 (= v0x7fcaba57bc50_0 0.0))
                (= v0x7fcaba57e1d0_0 (= v0x7fcaba57d590_0 2.0))
                (= v0x7fcaba57e310_0 (= v0x7fcaba57d650_0 0.0))
                (= v0x7fcaba57e450_0 (or v0x7fcaba57e310_0 v0x7fcaba57e1d0_0))
                (= v0x7fcaba57e590_0 (xor v0x7fcaba57e450_0 true))
                (= v0x7fcaba57e6d0_0 (and v0x7fcaba57c3d0_0 v0x7fcaba57e590_0)))))
  (=> F0x7fcaba57f510 a!7))))
(assert (=> F0x7fcaba57f510 F0x7fcaba57f2d0))
(assert (=> F0x7fcaba57f610 (or F0x7fcaba57f4d0 F0x7fcaba57f390)))
(assert (=> F0x7fcaba57f5d0 F0x7fcaba57f510))
(assert (=> pre!entry!0 (=> F0x7fcaba57f290 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fcaba57f2d0 (>= v0x7fcaba579c10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7fcaba57f2d0 (>= v0x7fcaba579d10_0 0.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7fcaba57f2d0
        (or (<= v0x7fcaba579d10_0 1.0) (>= v0x7fcaba579d10_0 2.0)))))
(assert (=> pre!bb1.i.i!3 (=> F0x7fcaba57f2d0 (<= v0x7fcaba579d10_0 2.0))))
(assert (=> pre!bb1.i.i!4
    (=> F0x7fcaba57f2d0
        (or (<= v0x7fcaba579d10_0 0.0) (>= v0x7fcaba579d10_0 1.0)))))
(assert (=> pre!bb1.i.i!5
    (=> F0x7fcaba57f2d0
        (or (<= v0x7fcaba579d10_0 1.0) (>= v0x7fcaba579a90_0 1.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fcaba57f610 (>= v0x7fcaba579dd0_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7fcaba57f610 (>= v0x7fcaba578010_0 0.0))))
(assert (= lemma!bb1.i.i!2
   (=> F0x7fcaba57f610
       (or (<= v0x7fcaba578010_0 1.0) (>= v0x7fcaba578010_0 2.0)))))
(assert (= lemma!bb1.i.i!3 (=> F0x7fcaba57f610 (<= v0x7fcaba578010_0 2.0))))
(assert (= lemma!bb1.i.i!4
   (=> F0x7fcaba57f610
       (or (<= v0x7fcaba578010_0 0.0) (>= v0x7fcaba578010_0 1.0)))))
(assert (= lemma!bb1.i.i!5
   (=> F0x7fcaba57f610
       (or (<= v0x7fcaba578010_0 1.0) (>= v0x7fcaba579cd0_0 1.0)))))
(assert (= lemma!bb2.i.i25.i.i!0 (=> F0x7fcaba57f5d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb2.i.i25.i.i!0) (not lemma!bb2.i.i25.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
; (init: F0x7fcaba57f290)
; (error: F0x7fcaba57f5d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i25.i.i!0)
