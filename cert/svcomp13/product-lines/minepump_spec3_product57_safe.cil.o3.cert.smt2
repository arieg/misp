(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i34.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fc641103010 () Bool)
(declare-fun F0x7fc641102f10 () Bool)
(declare-fun v0x7fc641101e90_0 () Bool)
(declare-fun v0x7fc641100810_0 () Bool)
(declare-fun v0x7fc6410fed90_0 () Bool)
(declare-fun v0x7fc6410fe8d0_0 () Real)
(declare-fun v0x7fc6410fdd10_0 () Bool)
(declare-fun v0x7fc641102110_0 () Bool)
(declare-fun E0x7fc641101690 () Bool)
(declare-fun E0x7fc641101210 () Bool)
(declare-fun v0x7fc641100dd0_0 () Real)
(declare-fun E0x7fc641100f50 () Bool)
(declare-fun E0x7fc641101490 () Bool)
(declare-fun v0x7fc641100d10_0 () Bool)
(declare-fun v0x7fc641100450_0 () Bool)
(declare-fun v0x7fc641100950_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fc641100a90_0 () Bool)
(declare-fun v0x7fc6410ffc10_0 () Bool)
(declare-fun E0x7fc641100650 () Bool)
(declare-fun v0x7fc641100150_0 () Real)
(declare-fun v0x7fc6410ffd50_0 () Bool)
(declare-fun v0x7fc6410ff850_0 () Bool)
(declare-fun v0x7fc6410ff990_0 () Bool)
(declare-fun E0x7fc6410ff350 () Bool)
(declare-fun v0x7fc6410fcf10_0 () Real)
(declare-fun v0x7fc6410feed0_0 () Real)
(declare-fun F0x7fc641103050 () Bool)
(declare-fun v0x7fc6410fe990_0 () Bool)
(declare-fun E0x7fc641100b50 () Bool)
(declare-fun v0x7fc6410fd810_0 () Real)
(declare-fun v0x7fc6410fead0_0 () Bool)
(declare-fun v0x7fc6410ff010_0 () Bool)
(declare-fun v0x7fc6410fd190_0 () Real)
(declare-fun v0x7fc641100590_0 () Bool)
(declare-fun v0x7fc641101fd0_0 () Bool)
(declare-fun F0x7fc641102e50 () Bool)
(declare-fun E0x7fc6410ffe10 () Bool)
(declare-fun v0x7fc6410fe010_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fc6410fe210_0 () Real)
(declare-fun E0x7fc6410ffa50 () Bool)
(declare-fun E0x7fc6410fe2d0 () Bool)
(declare-fun v0x7fc641101d50_0 () Bool)
(declare-fun E0x7fc6410fe490 () Bool)
(declare-fun v0x7fc6410fded0_0 () Real)
(declare-fun v0x7fc641100e90_0 () Real)
(declare-fun E0x7fc6410ff190 () Bool)
(declare-fun v0x7fc6410fe150_0 () Bool)
(declare-fun v0x7fc6410fd090_0 () Real)
(declare-fun E0x7fc6410fdad0 () Bool)
(declare-fun post!bb2.i.i34.i.i!0 () Bool)
(declare-fun F0x7fc641102cd0 () Bool)
(declare-fun F0x7fc641102c90 () Bool)
(declare-fun v0x7fc6410fb010_0 () Real)
(declare-fun v0x7fc6410fd250_0 () Real)
(declare-fun E0x7fc6410feb90 () Bool)
(declare-fun v0x7fc641100010_0 () Bool)
(declare-fun v0x7fc6410fd150_0 () Real)
(declare-fun v0x7fc641100310_0 () Real)
(declare-fun v0x7fc6410fb110_0 () Bool)
(declare-fun v0x7fc6410fd750_0 () Bool)
(declare-fun v0x7fc6410fd8d0_0 () Bool)
(declare-fun v0x7fc6410ff0d0_0 () Real)
(declare-fun F0x7fc641102d90 () Bool)
(declare-fun v0x7fc6410fda10_0 () Bool)
(declare-fun v0x7fc641101c50_0 () Bool)

(assert (=> F0x7fc641102d90
    (and v0x7fc6410fb110_0
         (<= v0x7fc6410fd150_0 0.0)
         (>= v0x7fc6410fd150_0 0.0)
         (<= v0x7fc6410fd250_0 0.0)
         (>= v0x7fc6410fd250_0 0.0)
         (<= v0x7fc6410fb010_0 1.0)
         (>= v0x7fc6410fb010_0 1.0))))
(assert (=> F0x7fc641102d90 F0x7fc641102c90))
(assert (let ((a!1 (=> v0x7fc6410fe150_0
               (or (and v0x7fc6410fda10_0
                        E0x7fc6410fe2d0
                        (<= v0x7fc6410fe210_0 v0x7fc6410fe010_0)
                        (>= v0x7fc6410fe210_0 v0x7fc6410fe010_0))
                   (and v0x7fc6410fd750_0
                        E0x7fc6410fe490
                        v0x7fc6410fd8d0_0
                        (<= v0x7fc6410fe210_0 v0x7fc6410fd190_0)
                        (>= v0x7fc6410fe210_0 v0x7fc6410fd190_0)))))
      (a!2 (=> v0x7fc6410fe150_0
               (or (and E0x7fc6410fe2d0 (not E0x7fc6410fe490))
                   (and E0x7fc6410fe490 (not E0x7fc6410fe2d0)))))
      (a!3 (=> v0x7fc6410ff010_0
               (or (and v0x7fc6410fead0_0
                        E0x7fc6410ff190
                        (<= v0x7fc6410ff0d0_0 v0x7fc6410feed0_0)
                        (>= v0x7fc6410ff0d0_0 v0x7fc6410feed0_0))
                   (and v0x7fc6410fe150_0
                        E0x7fc6410ff350
                        v0x7fc6410fe990_0
                        (<= v0x7fc6410ff0d0_0 v0x7fc6410fd090_0)
                        (>= v0x7fc6410ff0d0_0 v0x7fc6410fd090_0)))))
      (a!4 (=> v0x7fc6410ff010_0
               (or (and E0x7fc6410ff190 (not E0x7fc6410ff350))
                   (and E0x7fc6410ff350 (not E0x7fc6410ff190)))))
      (a!5 (or (and v0x7fc641100590_0
                    E0x7fc641100f50
                    (and (<= v0x7fc641100dd0_0 v0x7fc6410fe210_0)
                         (>= v0x7fc641100dd0_0 v0x7fc6410fe210_0))
                    (<= v0x7fc641100e90_0 v0x7fc641100950_0)
                    (>= v0x7fc641100e90_0 v0x7fc641100950_0))
               (and v0x7fc6410ff990_0
                    E0x7fc641101210
                    (not v0x7fc6410ffc10_0)
                    (and (<= v0x7fc641100dd0_0 v0x7fc6410fe210_0)
                         (>= v0x7fc641100dd0_0 v0x7fc6410fe210_0))
                    (and (<= v0x7fc641100e90_0 v0x7fc6410fcf10_0)
                         (>= v0x7fc641100e90_0 v0x7fc6410fcf10_0)))
               (and v0x7fc641100a90_0
                    E0x7fc641101490
                    (and (<= v0x7fc641100dd0_0 v0x7fc641100310_0)
                         (>= v0x7fc641100dd0_0 v0x7fc641100310_0))
                    (and (<= v0x7fc641100e90_0 v0x7fc6410fcf10_0)
                         (>= v0x7fc641100e90_0 v0x7fc6410fcf10_0)))
               (and v0x7fc6410ffd50_0
                    E0x7fc641101690
                    (not v0x7fc641100450_0)
                    (and (<= v0x7fc641100dd0_0 v0x7fc641100310_0)
                         (>= v0x7fc641100dd0_0 v0x7fc641100310_0))
                    (<= v0x7fc641100e90_0 0.0)
                    (>= v0x7fc641100e90_0 0.0))))
      (a!6 (=> v0x7fc641100d10_0
               (or (and E0x7fc641100f50
                        (not E0x7fc641101210)
                        (not E0x7fc641101490)
                        (not E0x7fc641101690))
                   (and E0x7fc641101210
                        (not E0x7fc641100f50)
                        (not E0x7fc641101490)
                        (not E0x7fc641101690))
                   (and E0x7fc641101490
                        (not E0x7fc641100f50)
                        (not E0x7fc641101210)
                        (not E0x7fc641101690))
                   (and E0x7fc641101690
                        (not E0x7fc641100f50)
                        (not E0x7fc641101210)
                        (not E0x7fc641101490))))))
(let ((a!7 (and (=> v0x7fc6410fda10_0
                    (and v0x7fc6410fd750_0
                         E0x7fc6410fdad0
                         (not v0x7fc6410fd8d0_0)))
                (=> v0x7fc6410fda10_0 E0x7fc6410fdad0)
                a!1
                a!2
                (=> v0x7fc6410fead0_0
                    (and v0x7fc6410fe150_0
                         E0x7fc6410feb90
                         (not v0x7fc6410fe990_0)))
                (=> v0x7fc6410fead0_0 E0x7fc6410feb90)
                a!3
                a!4
                (=> v0x7fc6410ff990_0
                    (and v0x7fc6410ff010_0 E0x7fc6410ffa50 v0x7fc6410ff850_0))
                (=> v0x7fc6410ff990_0 E0x7fc6410ffa50)
                (=> v0x7fc6410ffd50_0
                    (and v0x7fc6410ff010_0
                         E0x7fc6410ffe10
                         (not v0x7fc6410ff850_0)))
                (=> v0x7fc6410ffd50_0 E0x7fc6410ffe10)
                (=> v0x7fc641100590_0
                    (and v0x7fc6410ff990_0 E0x7fc641100650 v0x7fc6410ffc10_0))
                (=> v0x7fc641100590_0 E0x7fc641100650)
                (=> v0x7fc641100a90_0
                    (and v0x7fc6410ffd50_0 E0x7fc641100b50 v0x7fc641100450_0))
                (=> v0x7fc641100a90_0 E0x7fc641100b50)
                (=> v0x7fc641100d10_0 a!5)
                a!6
                v0x7fc641100d10_0
                (not v0x7fc641102110_0)
                (<= v0x7fc6410fd150_0 v0x7fc641100e90_0)
                (>= v0x7fc6410fd150_0 v0x7fc641100e90_0)
                (<= v0x7fc6410fd250_0 v0x7fc6410ff0d0_0)
                (>= v0x7fc6410fd250_0 v0x7fc6410ff0d0_0)
                (<= v0x7fc6410fb010_0 v0x7fc641100dd0_0)
                (>= v0x7fc6410fb010_0 v0x7fc641100dd0_0)
                (= v0x7fc6410fd8d0_0 (= v0x7fc6410fd810_0 0.0))
                (= v0x7fc6410fdd10_0 (< v0x7fc6410fd190_0 2.0))
                (= v0x7fc6410fded0_0 (ite v0x7fc6410fdd10_0 1.0 0.0))
                (= v0x7fc6410fe010_0 (+ v0x7fc6410fded0_0 v0x7fc6410fd190_0))
                (= v0x7fc6410fe990_0 (= v0x7fc6410fe8d0_0 0.0))
                (= v0x7fc6410fed90_0 (= v0x7fc6410fd090_0 0.0))
                (= v0x7fc6410feed0_0 (ite v0x7fc6410fed90_0 1.0 0.0))
                (= v0x7fc6410ff850_0 (= v0x7fc6410fcf10_0 0.0))
                (= v0x7fc6410ffc10_0 (> v0x7fc6410fe210_0 1.0))
                (= v0x7fc641100010_0 (> v0x7fc6410fe210_0 0.0))
                (= v0x7fc641100150_0 (+ v0x7fc6410fe210_0 (- 1.0)))
                (= v0x7fc641100310_0
                   (ite v0x7fc641100010_0 v0x7fc641100150_0 v0x7fc6410fe210_0))
                (= v0x7fc641100450_0 (= v0x7fc641100310_0 0.0))
                (= v0x7fc641100810_0 (= v0x7fc6410ff0d0_0 0.0))
                (= v0x7fc641100950_0
                   (ite v0x7fc641100810_0 1.0 v0x7fc6410fcf10_0))
                (= v0x7fc641101c50_0 (= v0x7fc6410ff0d0_0 0.0))
                (= v0x7fc641101d50_0 (= v0x7fc641100dd0_0 2.0))
                (= v0x7fc641101e90_0 (= v0x7fc641100e90_0 0.0))
                (= v0x7fc641101fd0_0 (and v0x7fc641101d50_0 v0x7fc641101c50_0))
                (= v0x7fc641102110_0 (and v0x7fc641101fd0_0 v0x7fc641101e90_0)))))
  (=> F0x7fc641102cd0 a!7))))
(assert (=> F0x7fc641102cd0 F0x7fc641102e50))
(assert (let ((a!1 (=> v0x7fc6410fe150_0
               (or (and v0x7fc6410fda10_0
                        E0x7fc6410fe2d0
                        (<= v0x7fc6410fe210_0 v0x7fc6410fe010_0)
                        (>= v0x7fc6410fe210_0 v0x7fc6410fe010_0))
                   (and v0x7fc6410fd750_0
                        E0x7fc6410fe490
                        v0x7fc6410fd8d0_0
                        (<= v0x7fc6410fe210_0 v0x7fc6410fd190_0)
                        (>= v0x7fc6410fe210_0 v0x7fc6410fd190_0)))))
      (a!2 (=> v0x7fc6410fe150_0
               (or (and E0x7fc6410fe2d0 (not E0x7fc6410fe490))
                   (and E0x7fc6410fe490 (not E0x7fc6410fe2d0)))))
      (a!3 (=> v0x7fc6410ff010_0
               (or (and v0x7fc6410fead0_0
                        E0x7fc6410ff190
                        (<= v0x7fc6410ff0d0_0 v0x7fc6410feed0_0)
                        (>= v0x7fc6410ff0d0_0 v0x7fc6410feed0_0))
                   (and v0x7fc6410fe150_0
                        E0x7fc6410ff350
                        v0x7fc6410fe990_0
                        (<= v0x7fc6410ff0d0_0 v0x7fc6410fd090_0)
                        (>= v0x7fc6410ff0d0_0 v0x7fc6410fd090_0)))))
      (a!4 (=> v0x7fc6410ff010_0
               (or (and E0x7fc6410ff190 (not E0x7fc6410ff350))
                   (and E0x7fc6410ff350 (not E0x7fc6410ff190)))))
      (a!5 (or (and v0x7fc641100590_0
                    E0x7fc641100f50
                    (and (<= v0x7fc641100dd0_0 v0x7fc6410fe210_0)
                         (>= v0x7fc641100dd0_0 v0x7fc6410fe210_0))
                    (<= v0x7fc641100e90_0 v0x7fc641100950_0)
                    (>= v0x7fc641100e90_0 v0x7fc641100950_0))
               (and v0x7fc6410ff990_0
                    E0x7fc641101210
                    (not v0x7fc6410ffc10_0)
                    (and (<= v0x7fc641100dd0_0 v0x7fc6410fe210_0)
                         (>= v0x7fc641100dd0_0 v0x7fc6410fe210_0))
                    (and (<= v0x7fc641100e90_0 v0x7fc6410fcf10_0)
                         (>= v0x7fc641100e90_0 v0x7fc6410fcf10_0)))
               (and v0x7fc641100a90_0
                    E0x7fc641101490
                    (and (<= v0x7fc641100dd0_0 v0x7fc641100310_0)
                         (>= v0x7fc641100dd0_0 v0x7fc641100310_0))
                    (and (<= v0x7fc641100e90_0 v0x7fc6410fcf10_0)
                         (>= v0x7fc641100e90_0 v0x7fc6410fcf10_0)))
               (and v0x7fc6410ffd50_0
                    E0x7fc641101690
                    (not v0x7fc641100450_0)
                    (and (<= v0x7fc641100dd0_0 v0x7fc641100310_0)
                         (>= v0x7fc641100dd0_0 v0x7fc641100310_0))
                    (<= v0x7fc641100e90_0 0.0)
                    (>= v0x7fc641100e90_0 0.0))))
      (a!6 (=> v0x7fc641100d10_0
               (or (and E0x7fc641100f50
                        (not E0x7fc641101210)
                        (not E0x7fc641101490)
                        (not E0x7fc641101690))
                   (and E0x7fc641101210
                        (not E0x7fc641100f50)
                        (not E0x7fc641101490)
                        (not E0x7fc641101690))
                   (and E0x7fc641101490
                        (not E0x7fc641100f50)
                        (not E0x7fc641101210)
                        (not E0x7fc641101690))
                   (and E0x7fc641101690
                        (not E0x7fc641100f50)
                        (not E0x7fc641101210)
                        (not E0x7fc641101490))))))
(let ((a!7 (and (=> v0x7fc6410fda10_0
                    (and v0x7fc6410fd750_0
                         E0x7fc6410fdad0
                         (not v0x7fc6410fd8d0_0)))
                (=> v0x7fc6410fda10_0 E0x7fc6410fdad0)
                a!1
                a!2
                (=> v0x7fc6410fead0_0
                    (and v0x7fc6410fe150_0
                         E0x7fc6410feb90
                         (not v0x7fc6410fe990_0)))
                (=> v0x7fc6410fead0_0 E0x7fc6410feb90)
                a!3
                a!4
                (=> v0x7fc6410ff990_0
                    (and v0x7fc6410ff010_0 E0x7fc6410ffa50 v0x7fc6410ff850_0))
                (=> v0x7fc6410ff990_0 E0x7fc6410ffa50)
                (=> v0x7fc6410ffd50_0
                    (and v0x7fc6410ff010_0
                         E0x7fc6410ffe10
                         (not v0x7fc6410ff850_0)))
                (=> v0x7fc6410ffd50_0 E0x7fc6410ffe10)
                (=> v0x7fc641100590_0
                    (and v0x7fc6410ff990_0 E0x7fc641100650 v0x7fc6410ffc10_0))
                (=> v0x7fc641100590_0 E0x7fc641100650)
                (=> v0x7fc641100a90_0
                    (and v0x7fc6410ffd50_0 E0x7fc641100b50 v0x7fc641100450_0))
                (=> v0x7fc641100a90_0 E0x7fc641100b50)
                (=> v0x7fc641100d10_0 a!5)
                a!6
                v0x7fc641100d10_0
                v0x7fc641102110_0
                (= v0x7fc6410fd8d0_0 (= v0x7fc6410fd810_0 0.0))
                (= v0x7fc6410fdd10_0 (< v0x7fc6410fd190_0 2.0))
                (= v0x7fc6410fded0_0 (ite v0x7fc6410fdd10_0 1.0 0.0))
                (= v0x7fc6410fe010_0 (+ v0x7fc6410fded0_0 v0x7fc6410fd190_0))
                (= v0x7fc6410fe990_0 (= v0x7fc6410fe8d0_0 0.0))
                (= v0x7fc6410fed90_0 (= v0x7fc6410fd090_0 0.0))
                (= v0x7fc6410feed0_0 (ite v0x7fc6410fed90_0 1.0 0.0))
                (= v0x7fc6410ff850_0 (= v0x7fc6410fcf10_0 0.0))
                (= v0x7fc6410ffc10_0 (> v0x7fc6410fe210_0 1.0))
                (= v0x7fc641100010_0 (> v0x7fc6410fe210_0 0.0))
                (= v0x7fc641100150_0 (+ v0x7fc6410fe210_0 (- 1.0)))
                (= v0x7fc641100310_0
                   (ite v0x7fc641100010_0 v0x7fc641100150_0 v0x7fc6410fe210_0))
                (= v0x7fc641100450_0 (= v0x7fc641100310_0 0.0))
                (= v0x7fc641100810_0 (= v0x7fc6410ff0d0_0 0.0))
                (= v0x7fc641100950_0
                   (ite v0x7fc641100810_0 1.0 v0x7fc6410fcf10_0))
                (= v0x7fc641101c50_0 (= v0x7fc6410ff0d0_0 0.0))
                (= v0x7fc641101d50_0 (= v0x7fc641100dd0_0 2.0))
                (= v0x7fc641101e90_0 (= v0x7fc641100e90_0 0.0))
                (= v0x7fc641101fd0_0 (and v0x7fc641101d50_0 v0x7fc641101c50_0))
                (= v0x7fc641102110_0 (and v0x7fc641101fd0_0 v0x7fc641101e90_0)))))
  (=> F0x7fc641102f10 a!7))))
(assert (=> F0x7fc641102f10 F0x7fc641102e50))
(assert (=> F0x7fc641103050 (or F0x7fc641102d90 F0x7fc641102cd0)))
(assert (=> F0x7fc641103010 F0x7fc641102f10))
(assert (=> pre!entry!0 (=> F0x7fc641102c90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fc641102e50 (>= v0x7fc6410fcf10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7fc641102e50 (>= v0x7fc6410fd090_0 0.0))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fc641102e50 (not (<= 3.0 v0x7fc6410fd190_0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fc641103050 (>= v0x7fc6410fd150_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7fc641103050 (>= v0x7fc6410fd250_0 0.0))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fc641103050 (not (<= 3.0 v0x7fc6410fb010_0)))))
(assert (= lemma!bb2.i.i34.i.i!0 (=> F0x7fc641103010 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb2.i.i34.i.i!0) (not lemma!bb2.i.i34.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
; (init: F0x7fc641102c90)
; (error: F0x7fc641103010)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb2.i.i34.i.i!0)
