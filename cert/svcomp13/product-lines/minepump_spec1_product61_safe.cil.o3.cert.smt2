(declare-fun post!bb1.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i43.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fccd50a3510 () Bool)
(declare-fun v0x7fccd50a0f90_0 () Bool)
(declare-fun v0x7fccd50a0a90_0 () Bool)
(declare-fun v0x7fccd50a0650_0 () Real)
(declare-fun v0x7fccd50a0510_0 () Bool)
(declare-fun v0x7fccd509f290_0 () Bool)
(declare-fun v0x7fccd509dd10_0 () Real)
(declare-fun v0x7fccd50a2490_0 () Bool)
(declare-fun E0x7fccd50a1dd0 () Bool)
(declare-fun F0x7fccd50a33d0 () Bool)
(declare-fun v0x7fccd50a0810_0 () Real)
(declare-fun v0x7fccd509d410_0 () Real)
(declare-fun v0x7fccd50a25d0_0 () Bool)
(declare-fun E0x7fccd50a1950 () Bool)
(declare-fun v0x7fccd50a1090_0 () Real)
(declare-fun v0x7fccd50a1510_0 () Real)
(declare-fun v0x7fccd50a0bd0_0 () Bool)
(declare-fun E0x7fccd50a1290 () Bool)
(declare-fun v0x7fccd50a11d0_0 () Bool)
(declare-fun v0x7fccd50a0110_0 () Bool)
(declare-fun F0x7fccd50a3310 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7fccd50a0dd0 () Bool)
(declare-fun E0x7fccd50a1bd0 () Bool)
(declare-fun v0x7fccd50a0d10_0 () Bool)
(declare-fun E0x7fccd50a0310 () Bool)
(declare-fun v0x7fccd50a0250_0 () Bool)
(declare-fun v0x7fccd50a15d0_0 () Real)
(declare-fun v0x7fccd509d590_0 () Real)
(declare-fun v0x7fccd509f3d0_0 () Real)
(declare-fun v0x7fccd509fd50_0 () Bool)
(declare-fun v0x7fccd509e210_0 () Bool)
(declare-fun v0x7fccd509f5d0_0 () Real)
(declare-fun E0x7fccd50a1690 () Bool)
(declare-fun E0x7fccd509f690 () Bool)
(declare-fun E0x7fccd509f090 () Bool)
(declare-fun v0x7fccd50a0950_0 () Bool)
(declare-fun E0x7fccd509e990 () Bool)
(declare-fun v0x7fccd509e650_0 () Bool)
(declare-fun E0x7fccd509e7d0 () Bool)
(declare-fun v0x7fccd509efd0_0 () Bool)
(declare-fun v0x7fccd509e710_0 () Real)
(declare-fun v0x7fccd509dc50_0 () Bool)
(declare-fun E0x7fccd509f850 () Bool)
(declare-fun v0x7fccd509d690_0 () Real)
(declare-fun F0x7fccd50a34d0 () Bool)
(declare-fun E0x7fccd509dfd0 () Bool)
(declare-fun F0x7fccd50a3190 () Bool)
(declare-fun v0x7fccd509f510_0 () Bool)
(declare-fun F0x7fccd50a3250 () Bool)
(declare-fun v0x7fccd509a010_0 () Real)
(declare-fun v0x7fccd509ddd0_0 () Bool)
(declare-fun E0x7fccd509ff50 () Bool)
(declare-fun v0x7fccd509d750_0 () Real)
(declare-fun v0x7fccd50a1450_0 () Bool)
(declare-fun v0x7fccd50a2390_0 () Bool)
(declare-fun v0x7fccd509ee90_0 () Bool)
(declare-fun v0x7fccd509e3d0_0 () Real)
(declare-fun v0x7fccd509df10_0 () Bool)
(declare-fun v0x7fccd509edd0_0 () Real)
(declare-fun v0x7fccd509d650_0 () Real)
(declare-fun v0x7fccd509a110_0 () Bool)
(declare-fun v0x7fccd509e510_0 () Real)
(declare-fun v0x7fccd509fe90_0 () Bool)
(declare-fun F0x7fccd50a30d0 () Bool)

(assert (=> F0x7fccd50a30d0
    (and v0x7fccd509a110_0
         (<= v0x7fccd509d650_0 0.0)
         (>= v0x7fccd509d650_0 0.0)
         (<= v0x7fccd509d750_0 0.0)
         (>= v0x7fccd509d750_0 0.0)
         (<= v0x7fccd509a010_0 1.0)
         (>= v0x7fccd509a010_0 1.0))))
(assert (=> F0x7fccd50a30d0 F0x7fccd50a3190))
(assert (let ((a!1 (=> v0x7fccd509e650_0
               (or (and v0x7fccd509df10_0
                        E0x7fccd509e7d0
                        (<= v0x7fccd509e710_0 v0x7fccd509e510_0)
                        (>= v0x7fccd509e710_0 v0x7fccd509e510_0))
                   (and v0x7fccd509dc50_0
                        E0x7fccd509e990
                        v0x7fccd509ddd0_0
                        (<= v0x7fccd509e710_0 v0x7fccd509d690_0)
                        (>= v0x7fccd509e710_0 v0x7fccd509d690_0)))))
      (a!2 (=> v0x7fccd509e650_0
               (or (and E0x7fccd509e7d0 (not E0x7fccd509e990))
                   (and E0x7fccd509e990 (not E0x7fccd509e7d0)))))
      (a!3 (=> v0x7fccd509f510_0
               (or (and v0x7fccd509efd0_0
                        E0x7fccd509f690
                        (<= v0x7fccd509f5d0_0 v0x7fccd509f3d0_0)
                        (>= v0x7fccd509f5d0_0 v0x7fccd509f3d0_0))
                   (and v0x7fccd509e650_0
                        E0x7fccd509f850
                        v0x7fccd509ee90_0
                        (<= v0x7fccd509f5d0_0 v0x7fccd509d590_0)
                        (>= v0x7fccd509f5d0_0 v0x7fccd509d590_0)))))
      (a!4 (=> v0x7fccd509f510_0
               (or (and E0x7fccd509f690 (not E0x7fccd509f850))
                   (and E0x7fccd509f850 (not E0x7fccd509f690)))))
      (a!5 (or (and v0x7fccd50a0d10_0
                    E0x7fccd50a1690
                    (and (<= v0x7fccd50a1510_0 v0x7fccd509e710_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd509e710_0))
                    (<= v0x7fccd50a15d0_0 v0x7fccd50a1090_0)
                    (>= v0x7fccd50a15d0_0 v0x7fccd50a1090_0))
               (and v0x7fccd509fe90_0
                    E0x7fccd50a1950
                    (not v0x7fccd50a0110_0)
                    (and (<= v0x7fccd50a1510_0 v0x7fccd509e710_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd509e710_0))
                    (and (<= v0x7fccd50a15d0_0 v0x7fccd509d410_0)
                         (>= v0x7fccd50a15d0_0 v0x7fccd509d410_0)))
               (and v0x7fccd50a11d0_0
                    E0x7fccd50a1bd0
                    (and (<= v0x7fccd50a1510_0 v0x7fccd50a0810_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd50a0810_0))
                    (and (<= v0x7fccd50a15d0_0 v0x7fccd509d410_0)
                         (>= v0x7fccd50a15d0_0 v0x7fccd509d410_0)))
               (and v0x7fccd50a0250_0
                    E0x7fccd50a1dd0
                    (not v0x7fccd50a0bd0_0)
                    (and (<= v0x7fccd50a1510_0 v0x7fccd50a0810_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd50a0810_0))
                    (<= v0x7fccd50a15d0_0 0.0)
                    (>= v0x7fccd50a15d0_0 0.0))))
      (a!6 (=> v0x7fccd50a1450_0
               (or (and E0x7fccd50a1690
                        (not E0x7fccd50a1950)
                        (not E0x7fccd50a1bd0)
                        (not E0x7fccd50a1dd0))
                   (and E0x7fccd50a1950
                        (not E0x7fccd50a1690)
                        (not E0x7fccd50a1bd0)
                        (not E0x7fccd50a1dd0))
                   (and E0x7fccd50a1bd0
                        (not E0x7fccd50a1690)
                        (not E0x7fccd50a1950)
                        (not E0x7fccd50a1dd0))
                   (and E0x7fccd50a1dd0
                        (not E0x7fccd50a1690)
                        (not E0x7fccd50a1950)
                        (not E0x7fccd50a1bd0))))))
(let ((a!7 (and (=> v0x7fccd509df10_0
                    (and v0x7fccd509dc50_0
                         E0x7fccd509dfd0
                         (not v0x7fccd509ddd0_0)))
                (=> v0x7fccd509df10_0 E0x7fccd509dfd0)
                a!1
                a!2
                (=> v0x7fccd509efd0_0
                    (and v0x7fccd509e650_0
                         E0x7fccd509f090
                         (not v0x7fccd509ee90_0)))
                (=> v0x7fccd509efd0_0 E0x7fccd509f090)
                a!3
                a!4
                (=> v0x7fccd509fe90_0
                    (and v0x7fccd509f510_0 E0x7fccd509ff50 v0x7fccd509fd50_0))
                (=> v0x7fccd509fe90_0 E0x7fccd509ff50)
                (=> v0x7fccd50a0250_0
                    (and v0x7fccd509f510_0
                         E0x7fccd50a0310
                         (not v0x7fccd509fd50_0)))
                (=> v0x7fccd50a0250_0 E0x7fccd50a0310)
                (=> v0x7fccd50a0d10_0
                    (and v0x7fccd509fe90_0 E0x7fccd50a0dd0 v0x7fccd50a0110_0))
                (=> v0x7fccd50a0d10_0 E0x7fccd50a0dd0)
                (=> v0x7fccd50a11d0_0
                    (and v0x7fccd50a0250_0 E0x7fccd50a1290 v0x7fccd50a0bd0_0))
                (=> v0x7fccd50a11d0_0 E0x7fccd50a1290)
                (=> v0x7fccd50a1450_0 a!5)
                a!6
                v0x7fccd50a1450_0
                v0x7fccd50a25d0_0
                (<= v0x7fccd509d650_0 v0x7fccd50a15d0_0)
                (>= v0x7fccd509d650_0 v0x7fccd50a15d0_0)
                (<= v0x7fccd509d750_0 v0x7fccd509f5d0_0)
                (>= v0x7fccd509d750_0 v0x7fccd509f5d0_0)
                (<= v0x7fccd509a010_0 v0x7fccd50a1510_0)
                (>= v0x7fccd509a010_0 v0x7fccd50a1510_0)
                (= v0x7fccd509ddd0_0 (= v0x7fccd509dd10_0 0.0))
                (= v0x7fccd509e210_0 (< v0x7fccd509d690_0 2.0))
                (= v0x7fccd509e3d0_0 (ite v0x7fccd509e210_0 1.0 0.0))
                (= v0x7fccd509e510_0 (+ v0x7fccd509e3d0_0 v0x7fccd509d690_0))
                (= v0x7fccd509ee90_0 (= v0x7fccd509edd0_0 0.0))
                (= v0x7fccd509f290_0 (= v0x7fccd509d590_0 0.0))
                (= v0x7fccd509f3d0_0 (ite v0x7fccd509f290_0 1.0 0.0))
                (= v0x7fccd509fd50_0 (= v0x7fccd509d410_0 0.0))
                (= v0x7fccd50a0110_0 (> v0x7fccd509e710_0 1.0))
                (= v0x7fccd50a0510_0 (> v0x7fccd509e710_0 0.0))
                (= v0x7fccd50a0650_0 (+ v0x7fccd509e710_0 (- 1.0)))
                (= v0x7fccd50a0810_0
                   (ite v0x7fccd50a0510_0 v0x7fccd50a0650_0 v0x7fccd509e710_0))
                (= v0x7fccd50a0950_0 (= v0x7fccd509f5d0_0 0.0))
                (= v0x7fccd50a0a90_0 (= v0x7fccd50a0810_0 0.0))
                (= v0x7fccd50a0bd0_0 (and v0x7fccd50a0950_0 v0x7fccd50a0a90_0))
                (= v0x7fccd50a0f90_0 (= v0x7fccd509f5d0_0 0.0))
                (= v0x7fccd50a1090_0
                   (ite v0x7fccd50a0f90_0 1.0 v0x7fccd509d410_0))
                (= v0x7fccd50a2390_0 (= v0x7fccd509f5d0_0 0.0))
                (= v0x7fccd50a2490_0 (= v0x7fccd50a15d0_0 0.0))
                (= v0x7fccd50a25d0_0 (or v0x7fccd50a2490_0 v0x7fccd50a2390_0)))))
  (=> F0x7fccd50a3250 a!7))))
(assert (=> F0x7fccd50a3250 F0x7fccd50a3310))
(assert (let ((a!1 (=> v0x7fccd509e650_0
               (or (and v0x7fccd509df10_0
                        E0x7fccd509e7d0
                        (<= v0x7fccd509e710_0 v0x7fccd509e510_0)
                        (>= v0x7fccd509e710_0 v0x7fccd509e510_0))
                   (and v0x7fccd509dc50_0
                        E0x7fccd509e990
                        v0x7fccd509ddd0_0
                        (<= v0x7fccd509e710_0 v0x7fccd509d690_0)
                        (>= v0x7fccd509e710_0 v0x7fccd509d690_0)))))
      (a!2 (=> v0x7fccd509e650_0
               (or (and E0x7fccd509e7d0 (not E0x7fccd509e990))
                   (and E0x7fccd509e990 (not E0x7fccd509e7d0)))))
      (a!3 (=> v0x7fccd509f510_0
               (or (and v0x7fccd509efd0_0
                        E0x7fccd509f690
                        (<= v0x7fccd509f5d0_0 v0x7fccd509f3d0_0)
                        (>= v0x7fccd509f5d0_0 v0x7fccd509f3d0_0))
                   (and v0x7fccd509e650_0
                        E0x7fccd509f850
                        v0x7fccd509ee90_0
                        (<= v0x7fccd509f5d0_0 v0x7fccd509d590_0)
                        (>= v0x7fccd509f5d0_0 v0x7fccd509d590_0)))))
      (a!4 (=> v0x7fccd509f510_0
               (or (and E0x7fccd509f690 (not E0x7fccd509f850))
                   (and E0x7fccd509f850 (not E0x7fccd509f690)))))
      (a!5 (or (and v0x7fccd50a0d10_0
                    E0x7fccd50a1690
                    (and (<= v0x7fccd50a1510_0 v0x7fccd509e710_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd509e710_0))
                    (<= v0x7fccd50a15d0_0 v0x7fccd50a1090_0)
                    (>= v0x7fccd50a15d0_0 v0x7fccd50a1090_0))
               (and v0x7fccd509fe90_0
                    E0x7fccd50a1950
                    (not v0x7fccd50a0110_0)
                    (and (<= v0x7fccd50a1510_0 v0x7fccd509e710_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd509e710_0))
                    (and (<= v0x7fccd50a15d0_0 v0x7fccd509d410_0)
                         (>= v0x7fccd50a15d0_0 v0x7fccd509d410_0)))
               (and v0x7fccd50a11d0_0
                    E0x7fccd50a1bd0
                    (and (<= v0x7fccd50a1510_0 v0x7fccd50a0810_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd50a0810_0))
                    (and (<= v0x7fccd50a15d0_0 v0x7fccd509d410_0)
                         (>= v0x7fccd50a15d0_0 v0x7fccd509d410_0)))
               (and v0x7fccd50a0250_0
                    E0x7fccd50a1dd0
                    (not v0x7fccd50a0bd0_0)
                    (and (<= v0x7fccd50a1510_0 v0x7fccd50a0810_0)
                         (>= v0x7fccd50a1510_0 v0x7fccd50a0810_0))
                    (<= v0x7fccd50a15d0_0 0.0)
                    (>= v0x7fccd50a15d0_0 0.0))))
      (a!6 (=> v0x7fccd50a1450_0
               (or (and E0x7fccd50a1690
                        (not E0x7fccd50a1950)
                        (not E0x7fccd50a1bd0)
                        (not E0x7fccd50a1dd0))
                   (and E0x7fccd50a1950
                        (not E0x7fccd50a1690)
                        (not E0x7fccd50a1bd0)
                        (not E0x7fccd50a1dd0))
                   (and E0x7fccd50a1bd0
                        (not E0x7fccd50a1690)
                        (not E0x7fccd50a1950)
                        (not E0x7fccd50a1dd0))
                   (and E0x7fccd50a1dd0
                        (not E0x7fccd50a1690)
                        (not E0x7fccd50a1950)
                        (not E0x7fccd50a1bd0))))))
(let ((a!7 (and (=> v0x7fccd509df10_0
                    (and v0x7fccd509dc50_0
                         E0x7fccd509dfd0
                         (not v0x7fccd509ddd0_0)))
                (=> v0x7fccd509df10_0 E0x7fccd509dfd0)
                a!1
                a!2
                (=> v0x7fccd509efd0_0
                    (and v0x7fccd509e650_0
                         E0x7fccd509f090
                         (not v0x7fccd509ee90_0)))
                (=> v0x7fccd509efd0_0 E0x7fccd509f090)
                a!3
                a!4
                (=> v0x7fccd509fe90_0
                    (and v0x7fccd509f510_0 E0x7fccd509ff50 v0x7fccd509fd50_0))
                (=> v0x7fccd509fe90_0 E0x7fccd509ff50)
                (=> v0x7fccd50a0250_0
                    (and v0x7fccd509f510_0
                         E0x7fccd50a0310
                         (not v0x7fccd509fd50_0)))
                (=> v0x7fccd50a0250_0 E0x7fccd50a0310)
                (=> v0x7fccd50a0d10_0
                    (and v0x7fccd509fe90_0 E0x7fccd50a0dd0 v0x7fccd50a0110_0))
                (=> v0x7fccd50a0d10_0 E0x7fccd50a0dd0)
                (=> v0x7fccd50a11d0_0
                    (and v0x7fccd50a0250_0 E0x7fccd50a1290 v0x7fccd50a0bd0_0))
                (=> v0x7fccd50a11d0_0 E0x7fccd50a1290)
                (=> v0x7fccd50a1450_0 a!5)
                a!6
                v0x7fccd50a1450_0
                (not v0x7fccd50a25d0_0)
                (= v0x7fccd509ddd0_0 (= v0x7fccd509dd10_0 0.0))
                (= v0x7fccd509e210_0 (< v0x7fccd509d690_0 2.0))
                (= v0x7fccd509e3d0_0 (ite v0x7fccd509e210_0 1.0 0.0))
                (= v0x7fccd509e510_0 (+ v0x7fccd509e3d0_0 v0x7fccd509d690_0))
                (= v0x7fccd509ee90_0 (= v0x7fccd509edd0_0 0.0))
                (= v0x7fccd509f290_0 (= v0x7fccd509d590_0 0.0))
                (= v0x7fccd509f3d0_0 (ite v0x7fccd509f290_0 1.0 0.0))
                (= v0x7fccd509fd50_0 (= v0x7fccd509d410_0 0.0))
                (= v0x7fccd50a0110_0 (> v0x7fccd509e710_0 1.0))
                (= v0x7fccd50a0510_0 (> v0x7fccd509e710_0 0.0))
                (= v0x7fccd50a0650_0 (+ v0x7fccd509e710_0 (- 1.0)))
                (= v0x7fccd50a0810_0
                   (ite v0x7fccd50a0510_0 v0x7fccd50a0650_0 v0x7fccd509e710_0))
                (= v0x7fccd50a0950_0 (= v0x7fccd509f5d0_0 0.0))
                (= v0x7fccd50a0a90_0 (= v0x7fccd50a0810_0 0.0))
                (= v0x7fccd50a0bd0_0 (and v0x7fccd50a0950_0 v0x7fccd50a0a90_0))
                (= v0x7fccd50a0f90_0 (= v0x7fccd509f5d0_0 0.0))
                (= v0x7fccd50a1090_0
                   (ite v0x7fccd50a0f90_0 1.0 v0x7fccd509d410_0))
                (= v0x7fccd50a2390_0 (= v0x7fccd509f5d0_0 0.0))
                (= v0x7fccd50a2490_0 (= v0x7fccd50a15d0_0 0.0))
                (= v0x7fccd50a25d0_0 (or v0x7fccd50a2490_0 v0x7fccd50a2390_0)))))
  (=> F0x7fccd50a33d0 a!7))))
(assert (=> F0x7fccd50a33d0 F0x7fccd50a3310))
(assert (=> F0x7fccd50a3510 (or F0x7fccd50a30d0 F0x7fccd50a3250)))
(assert (=> F0x7fccd50a34d0 F0x7fccd50a33d0))
(assert (=> pre!entry!0 (=> F0x7fccd50a3190 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fccd50a3310 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fccd50a3510 true)))
(assert (= lemma!bb1.i.i43.i.i!0 (=> F0x7fccd50a34d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i43.i.i!0) (not lemma!bb1.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7fccd50a3190)
; (error: F0x7fccd50a34d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i43.i.i!0)
