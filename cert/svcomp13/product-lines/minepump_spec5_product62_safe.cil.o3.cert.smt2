(declare-fun post!bb2.i.i45.i.i!0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i45.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun F0x7f4a27eaa550 () Bool)
(declare-fun F0x7f4a27eaa850 () Bool)
(declare-fun v0x7f4a27ea9790_0 () Bool)
(declare-fun v0x7f4a27ea7690_0 () Bool)
(declare-fun v0x7f4a27ea5f50_0 () Real)
(declare-fun E0x7f4a27ea8f50 () Bool)
(declare-fun v0x7f4a27ea7990_0 () Real)
(declare-fun F0x7f4a27eaa910 () Bool)
(declare-fun v0x7f4a27ea5550_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f4a27ea8ad0 () Bool)
(declare-fun v0x7f4a27ea8210_0 () Real)
(declare-fun v0x7f4a27ea8690_0 () Real)
(declare-fun v0x7f4a27ea4810_0 () Real)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7f4a27ea85d0_0 () Bool)
(declare-fun v0x7f4a27ea7d50_0 () Bool)
(declare-fun E0x7f4a27ea8410 () Bool)
(declare-fun E0x7f4a27ea8810 () Bool)
(declare-fun v0x7f4a27ea6410_0 () Bool)
(declare-fun E0x7f4a27ea8d50 () Bool)
(declare-fun v0x7f4a27ea8110_0 () Bool)
(declare-fun v0x7f4a27ea8350_0 () Bool)
(declare-fun v0x7f4a27ea9510_0 () Bool)
(declare-fun v0x7f4a27ea7290_0 () Bool)
(declare-fun v0x7f4a27ea98d0_0 () Bool)
(declare-fun E0x7f4a27ea7f50 () Bool)
(declare-fun v0x7f4a27ea7e90_0 () Bool)
(declare-fun v0x7f4a27ea7010_0 () Bool)
(declare-fun v0x7f4a27ea4590_0 () Real)
(declare-fun E0x7f4a27ea69d0 () Bool)
(declare-fun v0x7f4a27ea5390_0 () Bool)
(declare-fun v0x7f4a27ea7ad0_0 () Bool)
(declare-fun v0x7f4a27ea6ed0_0 () Bool)
(declare-fun v0x7f4a27ea6010_0 () Bool)
(declare-fun v0x7f4a27ea7c10_0 () Bool)
(declare-fun v0x7f4a27ea77d0_0 () Real)
(declare-fun E0x7f4a27ea7490 () Bool)
(declare-fun E0x7f4a27ea6210 () Bool)
(declare-fun v0x7f4a27ea9a10_0 () Bool)
(declare-fun v0x7f4a27ea9650_0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7f4a27ea4710_0 () Real)
(declare-fun v0x7f4a27ea5690_0 () Real)
(declare-fun F0x7f4a27eaa950 () Bool)
(declare-fun v0x7f4a27ea6750_0 () Real)
(declare-fun v0x7f4a27ea5890_0 () Real)
(declare-fun v0x7f4a27ea4f50_0 () Bool)
(declare-fun E0x7f4a27ea5b10 () Bool)
(declare-fun v0x7f4a27ea6150_0 () Bool)
(declare-fun v0x7f4a27ea4dd0_0 () Bool)
(declare-fun F0x7f4a27eaa610 () Bool)
(declare-fun F0x7f4a27eaa6d0 () Bool)
(declare-fun v0x7f4a27ea1010_0 () Real)
(declare-fun v0x7f4a27ea73d0_0 () Bool)
(declare-fun v0x7f4a27ea48d0_0 () Real)
(declare-fun v0x7f4a27ea6550_0 () Real)
(declare-fun v0x7f4a27ea47d0_0 () Real)
(declare-fun E0x7f4a27ea5150 () Bool)
(declare-fun v0x7f4a27ea57d0_0 () Bool)
(declare-fun v0x7f4a27ea4e90_0 () Real)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f4a27ea70d0 () Bool)
(declare-fun v0x7f4a27ea5090_0 () Bool)
(declare-fun v0x7f4a27ea6690_0 () Bool)
(declare-fun E0x7f4a27ea6810 () Bool)
(declare-fun v0x7f4a27ea8750_0 () Real)
(declare-fun v0x7f4a27ea1110_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7f4a27ea5950 () Bool)
(declare-fun F0x7f4a27eaa790 () Bool)

(assert (=> F0x7f4a27eaa790
    (and v0x7f4a27ea1110_0
         (<= v0x7f4a27ea47d0_0 0.0)
         (>= v0x7f4a27ea47d0_0 0.0)
         (<= v0x7f4a27ea48d0_0 1.0)
         (>= v0x7f4a27ea48d0_0 1.0)
         (<= v0x7f4a27ea1010_0 0.0)
         (>= v0x7f4a27ea1010_0 0.0))))
(assert (=> F0x7f4a27eaa790 F0x7f4a27eaa6d0))
(assert (let ((a!1 (=> v0x7f4a27ea57d0_0
               (or (and v0x7f4a27ea5090_0
                        E0x7f4a27ea5950
                        (<= v0x7f4a27ea5890_0 v0x7f4a27ea5690_0)
                        (>= v0x7f4a27ea5890_0 v0x7f4a27ea5690_0))
                   (and v0x7f4a27ea4dd0_0
                        E0x7f4a27ea5b10
                        v0x7f4a27ea4f50_0
                        (<= v0x7f4a27ea5890_0 v0x7f4a27ea4710_0)
                        (>= v0x7f4a27ea5890_0 v0x7f4a27ea4710_0)))))
      (a!2 (=> v0x7f4a27ea57d0_0
               (or (and E0x7f4a27ea5950 (not E0x7f4a27ea5b10))
                   (and E0x7f4a27ea5b10 (not E0x7f4a27ea5950)))))
      (a!3 (=> v0x7f4a27ea6690_0
               (or (and v0x7f4a27ea6150_0
                        E0x7f4a27ea6810
                        (<= v0x7f4a27ea6750_0 v0x7f4a27ea6550_0)
                        (>= v0x7f4a27ea6750_0 v0x7f4a27ea6550_0))
                   (and v0x7f4a27ea57d0_0
                        E0x7f4a27ea69d0
                        v0x7f4a27ea6010_0
                        (<= v0x7f4a27ea6750_0 v0x7f4a27ea4590_0)
                        (>= v0x7f4a27ea6750_0 v0x7f4a27ea4590_0)))))
      (a!4 (=> v0x7f4a27ea6690_0
               (or (and E0x7f4a27ea6810 (not E0x7f4a27ea69d0))
                   (and E0x7f4a27ea69d0 (not E0x7f4a27ea6810)))))
      (a!5 (or (and v0x7f4a27ea7e90_0
                    E0x7f4a27ea8810
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0))
                    (<= v0x7f4a27ea8750_0 v0x7f4a27ea8210_0)
                    (>= v0x7f4a27ea8750_0 v0x7f4a27ea8210_0))
               (and v0x7f4a27ea7010_0
                    E0x7f4a27ea8ad0
                    (not v0x7f4a27ea7290_0)
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0))
                    (and (<= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)
                         (>= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)))
               (and v0x7f4a27ea8350_0
                    E0x7f4a27ea8d50
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0))
                    (and (<= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)
                         (>= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)))
               (and v0x7f4a27ea73d0_0
                    E0x7f4a27ea8f50
                    (not v0x7f4a27ea7d50_0)
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0))
                    (<= v0x7f4a27ea8750_0 0.0)
                    (>= v0x7f4a27ea8750_0 0.0))))
      (a!6 (=> v0x7f4a27ea85d0_0
               (or (and E0x7f4a27ea8810
                        (not E0x7f4a27ea8ad0)
                        (not E0x7f4a27ea8d50)
                        (not E0x7f4a27ea8f50))
                   (and E0x7f4a27ea8ad0
                        (not E0x7f4a27ea8810)
                        (not E0x7f4a27ea8d50)
                        (not E0x7f4a27ea8f50))
                   (and E0x7f4a27ea8d50
                        (not E0x7f4a27ea8810)
                        (not E0x7f4a27ea8ad0)
                        (not E0x7f4a27ea8f50))
                   (and E0x7f4a27ea8f50
                        (not E0x7f4a27ea8810)
                        (not E0x7f4a27ea8ad0)
                        (not E0x7f4a27ea8d50))))))
(let ((a!7 (and (=> v0x7f4a27ea5090_0
                    (and v0x7f4a27ea4dd0_0
                         E0x7f4a27ea5150
                         (not v0x7f4a27ea4f50_0)))
                (=> v0x7f4a27ea5090_0 E0x7f4a27ea5150)
                a!1
                a!2
                (=> v0x7f4a27ea6150_0
                    (and v0x7f4a27ea57d0_0
                         E0x7f4a27ea6210
                         (not v0x7f4a27ea6010_0)))
                (=> v0x7f4a27ea6150_0 E0x7f4a27ea6210)
                a!3
                a!4
                (=> v0x7f4a27ea7010_0
                    (and v0x7f4a27ea6690_0 E0x7f4a27ea70d0 v0x7f4a27ea6ed0_0))
                (=> v0x7f4a27ea7010_0 E0x7f4a27ea70d0)
                (=> v0x7f4a27ea73d0_0
                    (and v0x7f4a27ea6690_0
                         E0x7f4a27ea7490
                         (not v0x7f4a27ea6ed0_0)))
                (=> v0x7f4a27ea73d0_0 E0x7f4a27ea7490)
                (=> v0x7f4a27ea7e90_0
                    (and v0x7f4a27ea7010_0 E0x7f4a27ea7f50 v0x7f4a27ea7290_0))
                (=> v0x7f4a27ea7e90_0 E0x7f4a27ea7f50)
                (=> v0x7f4a27ea8350_0
                    (and v0x7f4a27ea73d0_0 E0x7f4a27ea8410 v0x7f4a27ea7d50_0))
                (=> v0x7f4a27ea8350_0 E0x7f4a27ea8410)
                (=> v0x7f4a27ea85d0_0 a!5)
                a!6
                v0x7f4a27ea85d0_0
                (not v0x7f4a27ea9a10_0)
                (<= v0x7f4a27ea47d0_0 v0x7f4a27ea6750_0)
                (>= v0x7f4a27ea47d0_0 v0x7f4a27ea6750_0)
                (<= v0x7f4a27ea48d0_0 v0x7f4a27ea8690_0)
                (>= v0x7f4a27ea48d0_0 v0x7f4a27ea8690_0)
                (<= v0x7f4a27ea1010_0 v0x7f4a27ea8750_0)
                (>= v0x7f4a27ea1010_0 v0x7f4a27ea8750_0)
                (= v0x7f4a27ea4f50_0 (= v0x7f4a27ea4e90_0 0.0))
                (= v0x7f4a27ea5390_0 (< v0x7f4a27ea4710_0 2.0))
                (= v0x7f4a27ea5550_0 (ite v0x7f4a27ea5390_0 1.0 0.0))
                (= v0x7f4a27ea5690_0 (+ v0x7f4a27ea5550_0 v0x7f4a27ea4710_0))
                (= v0x7f4a27ea6010_0 (= v0x7f4a27ea5f50_0 0.0))
                (= v0x7f4a27ea6410_0 (= v0x7f4a27ea4590_0 0.0))
                (= v0x7f4a27ea6550_0 (ite v0x7f4a27ea6410_0 1.0 0.0))
                (= v0x7f4a27ea6ed0_0 (= v0x7f4a27ea4810_0 0.0))
                (= v0x7f4a27ea7290_0 (> v0x7f4a27ea5890_0 1.0))
                (= v0x7f4a27ea7690_0 (> v0x7f4a27ea5890_0 0.0))
                (= v0x7f4a27ea77d0_0 (+ v0x7f4a27ea5890_0 (- 1.0)))
                (= v0x7f4a27ea7990_0
                   (ite v0x7f4a27ea7690_0 v0x7f4a27ea77d0_0 v0x7f4a27ea5890_0))
                (= v0x7f4a27ea7ad0_0 (= v0x7f4a27ea6750_0 0.0))
                (= v0x7f4a27ea7c10_0 (= v0x7f4a27ea7990_0 0.0))
                (= v0x7f4a27ea7d50_0 (and v0x7f4a27ea7ad0_0 v0x7f4a27ea7c10_0))
                (= v0x7f4a27ea8110_0 (= v0x7f4a27ea6750_0 0.0))
                (= v0x7f4a27ea8210_0
                   (ite v0x7f4a27ea8110_0 1.0 v0x7f4a27ea4810_0))
                (= v0x7f4a27ea9510_0 (= v0x7f4a27ea8690_0 2.0))
                (= v0x7f4a27ea9650_0 (= v0x7f4a27ea8750_0 0.0))
                (= v0x7f4a27ea9790_0 (or v0x7f4a27ea9650_0 v0x7f4a27ea9510_0))
                (= v0x7f4a27ea98d0_0 (xor v0x7f4a27ea9790_0 true))
                (= v0x7f4a27ea9a10_0 (and v0x7f4a27ea6ed0_0 v0x7f4a27ea98d0_0)))))
  (=> F0x7f4a27eaa610 a!7))))
(assert (=> F0x7f4a27eaa610 F0x7f4a27eaa550))
(assert (let ((a!1 (=> v0x7f4a27ea57d0_0
               (or (and v0x7f4a27ea5090_0
                        E0x7f4a27ea5950
                        (<= v0x7f4a27ea5890_0 v0x7f4a27ea5690_0)
                        (>= v0x7f4a27ea5890_0 v0x7f4a27ea5690_0))
                   (and v0x7f4a27ea4dd0_0
                        E0x7f4a27ea5b10
                        v0x7f4a27ea4f50_0
                        (<= v0x7f4a27ea5890_0 v0x7f4a27ea4710_0)
                        (>= v0x7f4a27ea5890_0 v0x7f4a27ea4710_0)))))
      (a!2 (=> v0x7f4a27ea57d0_0
               (or (and E0x7f4a27ea5950 (not E0x7f4a27ea5b10))
                   (and E0x7f4a27ea5b10 (not E0x7f4a27ea5950)))))
      (a!3 (=> v0x7f4a27ea6690_0
               (or (and v0x7f4a27ea6150_0
                        E0x7f4a27ea6810
                        (<= v0x7f4a27ea6750_0 v0x7f4a27ea6550_0)
                        (>= v0x7f4a27ea6750_0 v0x7f4a27ea6550_0))
                   (and v0x7f4a27ea57d0_0
                        E0x7f4a27ea69d0
                        v0x7f4a27ea6010_0
                        (<= v0x7f4a27ea6750_0 v0x7f4a27ea4590_0)
                        (>= v0x7f4a27ea6750_0 v0x7f4a27ea4590_0)))))
      (a!4 (=> v0x7f4a27ea6690_0
               (or (and E0x7f4a27ea6810 (not E0x7f4a27ea69d0))
                   (and E0x7f4a27ea69d0 (not E0x7f4a27ea6810)))))
      (a!5 (or (and v0x7f4a27ea7e90_0
                    E0x7f4a27ea8810
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0))
                    (<= v0x7f4a27ea8750_0 v0x7f4a27ea8210_0)
                    (>= v0x7f4a27ea8750_0 v0x7f4a27ea8210_0))
               (and v0x7f4a27ea7010_0
                    E0x7f4a27ea8ad0
                    (not v0x7f4a27ea7290_0)
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea5890_0))
                    (and (<= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)
                         (>= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)))
               (and v0x7f4a27ea8350_0
                    E0x7f4a27ea8d50
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0))
                    (and (<= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)
                         (>= v0x7f4a27ea8750_0 v0x7f4a27ea4810_0)))
               (and v0x7f4a27ea73d0_0
                    E0x7f4a27ea8f50
                    (not v0x7f4a27ea7d50_0)
                    (and (<= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0)
                         (>= v0x7f4a27ea8690_0 v0x7f4a27ea7990_0))
                    (<= v0x7f4a27ea8750_0 0.0)
                    (>= v0x7f4a27ea8750_0 0.0))))
      (a!6 (=> v0x7f4a27ea85d0_0
               (or (and E0x7f4a27ea8810
                        (not E0x7f4a27ea8ad0)
                        (not E0x7f4a27ea8d50)
                        (not E0x7f4a27ea8f50))
                   (and E0x7f4a27ea8ad0
                        (not E0x7f4a27ea8810)
                        (not E0x7f4a27ea8d50)
                        (not E0x7f4a27ea8f50))
                   (and E0x7f4a27ea8d50
                        (not E0x7f4a27ea8810)
                        (not E0x7f4a27ea8ad0)
                        (not E0x7f4a27ea8f50))
                   (and E0x7f4a27ea8f50
                        (not E0x7f4a27ea8810)
                        (not E0x7f4a27ea8ad0)
                        (not E0x7f4a27ea8d50))))))
(let ((a!7 (and (=> v0x7f4a27ea5090_0
                    (and v0x7f4a27ea4dd0_0
                         E0x7f4a27ea5150
                         (not v0x7f4a27ea4f50_0)))
                (=> v0x7f4a27ea5090_0 E0x7f4a27ea5150)
                a!1
                a!2
                (=> v0x7f4a27ea6150_0
                    (and v0x7f4a27ea57d0_0
                         E0x7f4a27ea6210
                         (not v0x7f4a27ea6010_0)))
                (=> v0x7f4a27ea6150_0 E0x7f4a27ea6210)
                a!3
                a!4
                (=> v0x7f4a27ea7010_0
                    (and v0x7f4a27ea6690_0 E0x7f4a27ea70d0 v0x7f4a27ea6ed0_0))
                (=> v0x7f4a27ea7010_0 E0x7f4a27ea70d0)
                (=> v0x7f4a27ea73d0_0
                    (and v0x7f4a27ea6690_0
                         E0x7f4a27ea7490
                         (not v0x7f4a27ea6ed0_0)))
                (=> v0x7f4a27ea73d0_0 E0x7f4a27ea7490)
                (=> v0x7f4a27ea7e90_0
                    (and v0x7f4a27ea7010_0 E0x7f4a27ea7f50 v0x7f4a27ea7290_0))
                (=> v0x7f4a27ea7e90_0 E0x7f4a27ea7f50)
                (=> v0x7f4a27ea8350_0
                    (and v0x7f4a27ea73d0_0 E0x7f4a27ea8410 v0x7f4a27ea7d50_0))
                (=> v0x7f4a27ea8350_0 E0x7f4a27ea8410)
                (=> v0x7f4a27ea85d0_0 a!5)
                a!6
                v0x7f4a27ea85d0_0
                v0x7f4a27ea9a10_0
                (= v0x7f4a27ea4f50_0 (= v0x7f4a27ea4e90_0 0.0))
                (= v0x7f4a27ea5390_0 (< v0x7f4a27ea4710_0 2.0))
                (= v0x7f4a27ea5550_0 (ite v0x7f4a27ea5390_0 1.0 0.0))
                (= v0x7f4a27ea5690_0 (+ v0x7f4a27ea5550_0 v0x7f4a27ea4710_0))
                (= v0x7f4a27ea6010_0 (= v0x7f4a27ea5f50_0 0.0))
                (= v0x7f4a27ea6410_0 (= v0x7f4a27ea4590_0 0.0))
                (= v0x7f4a27ea6550_0 (ite v0x7f4a27ea6410_0 1.0 0.0))
                (= v0x7f4a27ea6ed0_0 (= v0x7f4a27ea4810_0 0.0))
                (= v0x7f4a27ea7290_0 (> v0x7f4a27ea5890_0 1.0))
                (= v0x7f4a27ea7690_0 (> v0x7f4a27ea5890_0 0.0))
                (= v0x7f4a27ea77d0_0 (+ v0x7f4a27ea5890_0 (- 1.0)))
                (= v0x7f4a27ea7990_0
                   (ite v0x7f4a27ea7690_0 v0x7f4a27ea77d0_0 v0x7f4a27ea5890_0))
                (= v0x7f4a27ea7ad0_0 (= v0x7f4a27ea6750_0 0.0))
                (= v0x7f4a27ea7c10_0 (= v0x7f4a27ea7990_0 0.0))
                (= v0x7f4a27ea7d50_0 (and v0x7f4a27ea7ad0_0 v0x7f4a27ea7c10_0))
                (= v0x7f4a27ea8110_0 (= v0x7f4a27ea6750_0 0.0))
                (= v0x7f4a27ea8210_0
                   (ite v0x7f4a27ea8110_0 1.0 v0x7f4a27ea4810_0))
                (= v0x7f4a27ea9510_0 (= v0x7f4a27ea8690_0 2.0))
                (= v0x7f4a27ea9650_0 (= v0x7f4a27ea8750_0 0.0))
                (= v0x7f4a27ea9790_0 (or v0x7f4a27ea9650_0 v0x7f4a27ea9510_0))
                (= v0x7f4a27ea98d0_0 (xor v0x7f4a27ea9790_0 true))
                (= v0x7f4a27ea9a10_0 (and v0x7f4a27ea6ed0_0 v0x7f4a27ea98d0_0)))))
  (=> F0x7f4a27eaa850 a!7))))
(assert (=> F0x7f4a27eaa850 F0x7f4a27eaa550))
(assert (=> F0x7f4a27eaa950 (or F0x7f4a27eaa790 F0x7f4a27eaa610)))
(assert (=> F0x7f4a27eaa910 F0x7f4a27eaa850))
(assert (=> pre!entry!0 (=> F0x7f4a27eaa6d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f4a27eaa550 (>= v0x7f4a27ea4590_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f4a27eaa550
        (or (>= v0x7f4a27ea4710_0 2.0) (<= v0x7f4a27ea4710_0 1.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f4a27eaa550 (>= v0x7f4a27ea4710_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f4a27eaa550
        (or (>= v0x7f4a27ea4710_0 1.0) (<= v0x7f4a27ea4710_0 0.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f4a27eaa550 (<= v0x7f4a27ea4710_0 2.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f4a27eaa950 (>= v0x7f4a27ea47d0_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7f4a27eaa950
       (or (>= v0x7f4a27ea48d0_0 2.0) (<= v0x7f4a27ea48d0_0 1.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7f4a27eaa950 (>= v0x7f4a27ea48d0_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7f4a27eaa950
       (or (>= v0x7f4a27ea48d0_0 1.0) (<= v0x7f4a27ea48d0_0 0.0)))))
(assert (= lemma!bb1.i.i!4 (=> F0x7f4a27eaa950 (<= v0x7f4a27ea48d0_0 2.0))))
(assert (= lemma!bb2.i.i45.i.i!0 (=> F0x7f4a27eaa910 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb2.i.i45.i.i!0) (not lemma!bb2.i.i45.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
; (init: F0x7f4a27eaa6d0)
; (error: F0x7f4a27eaa910)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i45.i.i!0)
