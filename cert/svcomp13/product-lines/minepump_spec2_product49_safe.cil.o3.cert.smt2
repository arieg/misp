(declare-fun post!bb2.i.i26.i.i!0 () Bool)
(declare-fun post!bb1.i.i!6 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i26.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!6 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!7 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fb0cacff9d0 () Bool)
(declare-fun v0x7fb0cacfe6d0_0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun v0x7fb0cacfb810_0 () Real)
(declare-fun v0x7fb0cacfac50_0 () Bool)
(declare-fun v0x7fb0cacfe810_0 () Bool)
(declare-fun E0x7fb0cacfe050 () Bool)
(declare-fun v0x7fb0cacfa750_0 () Real)
(declare-fun v0x7fb0cacfd390_0 () Real)
(declare-fun pre!bb1.i.i!6 () Bool)
(declare-fun v0x7fb0cacfcc90_0 () Real)
(declare-fun v0x7fb0cacfe950_0 () Bool)
(declare-fun v0x7fb0cacfda10_0 () Real)
(declare-fun E0x7fb0cacfdad0 () Bool)
(declare-fun v0x7fb0cacfd4d0_0 () Bool)
(declare-fun v0x7fb0cacfd610_0 () Bool)
(declare-fun post!bb1.i.i!7 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun E0x7fb0cacfce90 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!7 () Bool)
(declare-fun v0x7fb0cacfcdd0_0 () Bool)
(declare-fun v0x7fb0cacfec10_0 () Bool)
(declare-fun v0x7fb0cacf9d90_0 () Real)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun v0x7fb0cacf9e90_0 () Real)
(declare-fun E0x7fb0cacfc290 () Bool)
(declare-fun E0x7fb0cacfd6d0 () Bool)
(declare-fun v0x7fb0cacfc8d0_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fb0cacfbe10_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7fb0cacfbf50_0 () Bool)
(declare-fun E0x7fb0cacfbad0 () Bool)
(declare-fun v0x7fb0cacfd1d0_0 () Real)
(declare-fun v0x7fb0cacf9f90_0 () Real)
(declare-fun E0x7fb0cacfc0d0 () Bool)
(declare-fun v0x7fb0cacfd090_0 () Bool)
(declare-fun v0x7fb0cacfe590_0 () Bool)
(declare-fun F0x7fb0cacff990 () Bool)
(declare-fun v0x7fb0cacfaf50_0 () Real)
(declare-fun v0x7fb0cacfb150_0 () Real)
(declare-fun F0x7fb0cacffb10 () Bool)
(declare-fun v0x7fb0cacfd950_0 () Real)
(declare-fun E0x7fb0cacfdd90 () Bool)
(declare-fun v0x7fb0cacfc790_0 () Bool)
(declare-fun E0x7fb0cacfb210 () Bool)
(declare-fun v0x7fb0cacfb090_0 () Bool)
(declare-fun v0x7fb0cacfae10_0 () Real)
(declare-fun v0x7fb0cacfa810_0 () Bool)
(declare-fun v0x7fb0cacfa950_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fb0cacfb8d0_0 () Bool)
(declare-fun F0x7fb0cacffc50 () Bool)
(declare-fun E0x7fb0cacfea10 () Bool)
(declare-fun E0x7fb0cacfc990 () Bool)
(declare-fun F0x7fb0cacffd10 () Bool)
(declare-fun v0x7fb0cacf8010_0 () Real)
(declare-fun v0x7fb0cacfbcd0_0 () Bool)
(declare-fun v0x7fb0cacfa690_0 () Bool)
(declare-fun E0x7fb0cacfaa10 () Bool)
(declare-fun v0x7fb0cacfc010_0 () Real)
(declare-fun v0x7fb0cacfba10_0 () Bool)
(declare-fun v0x7fb0cacfa050_0 () Real)
(declare-fun v0x7fb0cacf9f50_0 () Real)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun E0x7fb0cacfb3d0 () Bool)
(declare-fun v0x7fb0cacfd890_0 () Bool)
(declare-fun F0x7fb0cacffbd0 () Bool)
(declare-fun v0x7fb0cacf8110_0 () Bool)
(declare-fun v0x7fb0cacfcb50_0 () Bool)
(declare-fun v0x7fb0cacf9e50_0 () Real)
(declare-fun F0x7fb0cacffdd0 () Bool)
(declare-fun v0x7fb0cacf9c10_0 () Real)

(assert (=> F0x7fb0cacffdd0
    (and v0x7fb0cacf8110_0
         (<= v0x7fb0cacf9e50_0 0.0)
         (>= v0x7fb0cacf9e50_0 0.0)
         (<= v0x7fb0cacf9f50_0 0.0)
         (>= v0x7fb0cacf9f50_0 0.0)
         (<= v0x7fb0cacfa050_0 0.0)
         (>= v0x7fb0cacfa050_0 0.0)
         (<= v0x7fb0cacf8010_0 1.0)
         (>= v0x7fb0cacf8010_0 1.0))))
(assert (=> F0x7fb0cacffdd0 F0x7fb0cacffd10))
(assert (let ((a!1 (=> v0x7fb0cacfb090_0
               (or (and v0x7fb0cacfa950_0
                        E0x7fb0cacfb210
                        (<= v0x7fb0cacfb150_0 v0x7fb0cacfaf50_0)
                        (>= v0x7fb0cacfb150_0 v0x7fb0cacfaf50_0))
                   (and v0x7fb0cacfa690_0
                        E0x7fb0cacfb3d0
                        v0x7fb0cacfa810_0
                        (<= v0x7fb0cacfb150_0 v0x7fb0cacf9f90_0)
                        (>= v0x7fb0cacfb150_0 v0x7fb0cacf9f90_0)))))
      (a!2 (=> v0x7fb0cacfb090_0
               (or (and E0x7fb0cacfb210 (not E0x7fb0cacfb3d0))
                   (and E0x7fb0cacfb3d0 (not E0x7fb0cacfb210)))))
      (a!3 (=> v0x7fb0cacfbf50_0
               (or (and v0x7fb0cacfba10_0
                        E0x7fb0cacfc0d0
                        (<= v0x7fb0cacfc010_0 v0x7fb0cacfbe10_0)
                        (>= v0x7fb0cacfc010_0 v0x7fb0cacfbe10_0))
                   (and v0x7fb0cacfb090_0
                        E0x7fb0cacfc290
                        v0x7fb0cacfb8d0_0
                        (<= v0x7fb0cacfc010_0 v0x7fb0cacf9e90_0)
                        (>= v0x7fb0cacfc010_0 v0x7fb0cacf9e90_0)))))
      (a!4 (=> v0x7fb0cacfbf50_0
               (or (and E0x7fb0cacfc0d0 (not E0x7fb0cacfc290))
                   (and E0x7fb0cacfc290 (not E0x7fb0cacfc0d0)))))
      (a!5 (or (and v0x7fb0cacfc8d0_0
                    E0x7fb0cacfdad0
                    (<= v0x7fb0cacfd950_0 v0x7fb0cacfb150_0)
                    (>= v0x7fb0cacfd950_0 v0x7fb0cacfb150_0)
                    (<= v0x7fb0cacfda10_0 v0x7fb0cacfcc90_0)
                    (>= v0x7fb0cacfda10_0 v0x7fb0cacfcc90_0))
               (and v0x7fb0cacfd610_0
                    E0x7fb0cacfdd90
                    (and (<= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0)
                         (>= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0))
                    (<= v0x7fb0cacfda10_0 v0x7fb0cacf9c10_0)
                    (>= v0x7fb0cacfda10_0 v0x7fb0cacf9c10_0))
               (and v0x7fb0cacfcdd0_0
                    E0x7fb0cacfe050
                    (not v0x7fb0cacfd4d0_0)
                    (and (<= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0)
                         (>= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0))
                    (<= v0x7fb0cacfda10_0 0.0)
                    (>= v0x7fb0cacfda10_0 0.0))))
      (a!6 (=> v0x7fb0cacfd890_0
               (or (and E0x7fb0cacfdad0
                        (not E0x7fb0cacfdd90)
                        (not E0x7fb0cacfe050))
                   (and E0x7fb0cacfdd90
                        (not E0x7fb0cacfdad0)
                        (not E0x7fb0cacfe050))
                   (and E0x7fb0cacfe050
                        (not E0x7fb0cacfdad0)
                        (not E0x7fb0cacfdd90)))))
      (a!7 (or (and v0x7fb0cacfe950_0
                    v0x7fb0cacfec10_0
                    (and (<= v0x7fb0cacf9e50_0 v0x7fb0cacfda10_0)
                         (>= v0x7fb0cacf9e50_0 v0x7fb0cacfda10_0))
                    (<= v0x7fb0cacf9f50_0 1.0)
                    (>= v0x7fb0cacf9f50_0 1.0)
                    (and (<= v0x7fb0cacfa050_0 v0x7fb0cacfc010_0)
                         (>= v0x7fb0cacfa050_0 v0x7fb0cacfc010_0))
                    (and (<= v0x7fb0cacf8010_0 v0x7fb0cacfd950_0)
                         (>= v0x7fb0cacf8010_0 v0x7fb0cacfd950_0)))
               (and v0x7fb0cacfd890_0
                    v0x7fb0cacfe810_0
                    (and (<= v0x7fb0cacf9e50_0 v0x7fb0cacfda10_0)
                         (>= v0x7fb0cacf9e50_0 v0x7fb0cacfda10_0))
                    (<= v0x7fb0cacf9f50_0 0.0)
                    (>= v0x7fb0cacf9f50_0 0.0)
                    (and (<= v0x7fb0cacfa050_0 v0x7fb0cacfc010_0)
                         (>= v0x7fb0cacfa050_0 v0x7fb0cacfc010_0))
                    (and (<= v0x7fb0cacf8010_0 v0x7fb0cacfd950_0)
                         (>= v0x7fb0cacf8010_0 v0x7fb0cacfd950_0))))))
(let ((a!8 (and (=> v0x7fb0cacfa950_0
                    (and v0x7fb0cacfa690_0
                         E0x7fb0cacfaa10
                         (not v0x7fb0cacfa810_0)))
                (=> v0x7fb0cacfa950_0 E0x7fb0cacfaa10)
                a!1
                a!2
                (=> v0x7fb0cacfba10_0
                    (and v0x7fb0cacfb090_0
                         E0x7fb0cacfbad0
                         (not v0x7fb0cacfb8d0_0)))
                (=> v0x7fb0cacfba10_0 E0x7fb0cacfbad0)
                a!3
                a!4
                (=> v0x7fb0cacfc8d0_0
                    (and v0x7fb0cacfbf50_0 E0x7fb0cacfc990 v0x7fb0cacfc790_0))
                (=> v0x7fb0cacfc8d0_0 E0x7fb0cacfc990)
                (=> v0x7fb0cacfcdd0_0
                    (and v0x7fb0cacfbf50_0
                         E0x7fb0cacfce90
                         (not v0x7fb0cacfc790_0)))
                (=> v0x7fb0cacfcdd0_0 E0x7fb0cacfce90)
                (=> v0x7fb0cacfd610_0
                    (and v0x7fb0cacfcdd0_0 E0x7fb0cacfd6d0 v0x7fb0cacfd4d0_0))
                (=> v0x7fb0cacfd610_0 E0x7fb0cacfd6d0)
                (=> v0x7fb0cacfd890_0 a!5)
                a!6
                (=> v0x7fb0cacfe950_0
                    (and v0x7fb0cacfd890_0
                         E0x7fb0cacfea10
                         (not v0x7fb0cacfe810_0)))
                (=> v0x7fb0cacfe950_0 E0x7fb0cacfea10)
                a!7
                (= v0x7fb0cacfa810_0 (= v0x7fb0cacfa750_0 0.0))
                (= v0x7fb0cacfac50_0 (< v0x7fb0cacf9f90_0 2.0))
                (= v0x7fb0cacfae10_0 (ite v0x7fb0cacfac50_0 1.0 0.0))
                (= v0x7fb0cacfaf50_0 (+ v0x7fb0cacfae10_0 v0x7fb0cacf9f90_0))
                (= v0x7fb0cacfb8d0_0 (= v0x7fb0cacfb810_0 0.0))
                (= v0x7fb0cacfbcd0_0 (= v0x7fb0cacf9e90_0 0.0))
                (= v0x7fb0cacfbe10_0 (ite v0x7fb0cacfbcd0_0 1.0 0.0))
                (= v0x7fb0cacfc790_0 (= v0x7fb0cacf9c10_0 0.0))
                (= v0x7fb0cacfcb50_0 (> v0x7fb0cacfb150_0 1.0))
                (= v0x7fb0cacfcc90_0
                   (ite v0x7fb0cacfcb50_0 1.0 v0x7fb0cacf9c10_0))
                (= v0x7fb0cacfd090_0 (> v0x7fb0cacfb150_0 0.0))
                (= v0x7fb0cacfd1d0_0 (+ v0x7fb0cacfb150_0 (- 1.0)))
                (= v0x7fb0cacfd390_0
                   (ite v0x7fb0cacfd090_0 v0x7fb0cacfd1d0_0 v0x7fb0cacfb150_0))
                (= v0x7fb0cacfd4d0_0 (= v0x7fb0cacfd390_0 0.0))
                (= v0x7fb0cacfe590_0 (= v0x7fb0cacfc010_0 0.0))
                (= v0x7fb0cacfe6d0_0 (= v0x7fb0cacfda10_0 0.0))
                (= v0x7fb0cacfe810_0 (or v0x7fb0cacfe6d0_0 v0x7fb0cacfe590_0))
                (= v0x7fb0cacfec10_0 (= v0x7fb0cacf9d90_0 0.0)))))
  (=> F0x7fb0cacffc50 a!8))))
(assert (=> F0x7fb0cacffc50 F0x7fb0cacffbd0))
(assert (let ((a!1 (=> v0x7fb0cacfb090_0
               (or (and v0x7fb0cacfa950_0
                        E0x7fb0cacfb210
                        (<= v0x7fb0cacfb150_0 v0x7fb0cacfaf50_0)
                        (>= v0x7fb0cacfb150_0 v0x7fb0cacfaf50_0))
                   (and v0x7fb0cacfa690_0
                        E0x7fb0cacfb3d0
                        v0x7fb0cacfa810_0
                        (<= v0x7fb0cacfb150_0 v0x7fb0cacf9f90_0)
                        (>= v0x7fb0cacfb150_0 v0x7fb0cacf9f90_0)))))
      (a!2 (=> v0x7fb0cacfb090_0
               (or (and E0x7fb0cacfb210 (not E0x7fb0cacfb3d0))
                   (and E0x7fb0cacfb3d0 (not E0x7fb0cacfb210)))))
      (a!3 (=> v0x7fb0cacfbf50_0
               (or (and v0x7fb0cacfba10_0
                        E0x7fb0cacfc0d0
                        (<= v0x7fb0cacfc010_0 v0x7fb0cacfbe10_0)
                        (>= v0x7fb0cacfc010_0 v0x7fb0cacfbe10_0))
                   (and v0x7fb0cacfb090_0
                        E0x7fb0cacfc290
                        v0x7fb0cacfb8d0_0
                        (<= v0x7fb0cacfc010_0 v0x7fb0cacf9e90_0)
                        (>= v0x7fb0cacfc010_0 v0x7fb0cacf9e90_0)))))
      (a!4 (=> v0x7fb0cacfbf50_0
               (or (and E0x7fb0cacfc0d0 (not E0x7fb0cacfc290))
                   (and E0x7fb0cacfc290 (not E0x7fb0cacfc0d0)))))
      (a!5 (or (and v0x7fb0cacfc8d0_0
                    E0x7fb0cacfdad0
                    (<= v0x7fb0cacfd950_0 v0x7fb0cacfb150_0)
                    (>= v0x7fb0cacfd950_0 v0x7fb0cacfb150_0)
                    (<= v0x7fb0cacfda10_0 v0x7fb0cacfcc90_0)
                    (>= v0x7fb0cacfda10_0 v0x7fb0cacfcc90_0))
               (and v0x7fb0cacfd610_0
                    E0x7fb0cacfdd90
                    (and (<= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0)
                         (>= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0))
                    (<= v0x7fb0cacfda10_0 v0x7fb0cacf9c10_0)
                    (>= v0x7fb0cacfda10_0 v0x7fb0cacf9c10_0))
               (and v0x7fb0cacfcdd0_0
                    E0x7fb0cacfe050
                    (not v0x7fb0cacfd4d0_0)
                    (and (<= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0)
                         (>= v0x7fb0cacfd950_0 v0x7fb0cacfd390_0))
                    (<= v0x7fb0cacfda10_0 0.0)
                    (>= v0x7fb0cacfda10_0 0.0))))
      (a!6 (=> v0x7fb0cacfd890_0
               (or (and E0x7fb0cacfdad0
                        (not E0x7fb0cacfdd90)
                        (not E0x7fb0cacfe050))
                   (and E0x7fb0cacfdd90
                        (not E0x7fb0cacfdad0)
                        (not E0x7fb0cacfe050))
                   (and E0x7fb0cacfe050
                        (not E0x7fb0cacfdad0)
                        (not E0x7fb0cacfdd90))))))
(let ((a!7 (and (=> v0x7fb0cacfa950_0
                    (and v0x7fb0cacfa690_0
                         E0x7fb0cacfaa10
                         (not v0x7fb0cacfa810_0)))
                (=> v0x7fb0cacfa950_0 E0x7fb0cacfaa10)
                a!1
                a!2
                (=> v0x7fb0cacfba10_0
                    (and v0x7fb0cacfb090_0
                         E0x7fb0cacfbad0
                         (not v0x7fb0cacfb8d0_0)))
                (=> v0x7fb0cacfba10_0 E0x7fb0cacfbad0)
                a!3
                a!4
                (=> v0x7fb0cacfc8d0_0
                    (and v0x7fb0cacfbf50_0 E0x7fb0cacfc990 v0x7fb0cacfc790_0))
                (=> v0x7fb0cacfc8d0_0 E0x7fb0cacfc990)
                (=> v0x7fb0cacfcdd0_0
                    (and v0x7fb0cacfbf50_0
                         E0x7fb0cacfce90
                         (not v0x7fb0cacfc790_0)))
                (=> v0x7fb0cacfcdd0_0 E0x7fb0cacfce90)
                (=> v0x7fb0cacfd610_0
                    (and v0x7fb0cacfcdd0_0 E0x7fb0cacfd6d0 v0x7fb0cacfd4d0_0))
                (=> v0x7fb0cacfd610_0 E0x7fb0cacfd6d0)
                (=> v0x7fb0cacfd890_0 a!5)
                a!6
                (=> v0x7fb0cacfe950_0
                    (and v0x7fb0cacfd890_0
                         E0x7fb0cacfea10
                         (not v0x7fb0cacfe810_0)))
                (=> v0x7fb0cacfe950_0 E0x7fb0cacfea10)
                v0x7fb0cacfe950_0
                (not v0x7fb0cacfec10_0)
                (= v0x7fb0cacfa810_0 (= v0x7fb0cacfa750_0 0.0))
                (= v0x7fb0cacfac50_0 (< v0x7fb0cacf9f90_0 2.0))
                (= v0x7fb0cacfae10_0 (ite v0x7fb0cacfac50_0 1.0 0.0))
                (= v0x7fb0cacfaf50_0 (+ v0x7fb0cacfae10_0 v0x7fb0cacf9f90_0))
                (= v0x7fb0cacfb8d0_0 (= v0x7fb0cacfb810_0 0.0))
                (= v0x7fb0cacfbcd0_0 (= v0x7fb0cacf9e90_0 0.0))
                (= v0x7fb0cacfbe10_0 (ite v0x7fb0cacfbcd0_0 1.0 0.0))
                (= v0x7fb0cacfc790_0 (= v0x7fb0cacf9c10_0 0.0))
                (= v0x7fb0cacfcb50_0 (> v0x7fb0cacfb150_0 1.0))
                (= v0x7fb0cacfcc90_0
                   (ite v0x7fb0cacfcb50_0 1.0 v0x7fb0cacf9c10_0))
                (= v0x7fb0cacfd090_0 (> v0x7fb0cacfb150_0 0.0))
                (= v0x7fb0cacfd1d0_0 (+ v0x7fb0cacfb150_0 (- 1.0)))
                (= v0x7fb0cacfd390_0
                   (ite v0x7fb0cacfd090_0 v0x7fb0cacfd1d0_0 v0x7fb0cacfb150_0))
                (= v0x7fb0cacfd4d0_0 (= v0x7fb0cacfd390_0 0.0))
                (= v0x7fb0cacfe590_0 (= v0x7fb0cacfc010_0 0.0))
                (= v0x7fb0cacfe6d0_0 (= v0x7fb0cacfda10_0 0.0))
                (= v0x7fb0cacfe810_0 (or v0x7fb0cacfe6d0_0 v0x7fb0cacfe590_0))
                (= v0x7fb0cacfec10_0 (= v0x7fb0cacf9d90_0 0.0)))))
  (=> F0x7fb0cacffb10 a!7))))
(assert (=> F0x7fb0cacffb10 F0x7fb0cacffbd0))
(assert (=> F0x7fb0cacff990 (or F0x7fb0cacffdd0 F0x7fb0cacffc50)))
(assert (=> F0x7fb0cacff9d0 F0x7fb0cacffb10))
(assert (=> pre!entry!0 (=> F0x7fb0cacffd10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fb0cacffbd0 (>= v0x7fb0cacf9d90_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fb0cacffbd0
        (or (>= v0x7fb0cacf9f90_0 0.0) (<= v0x7fb0cacf9d90_0 0.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fb0cacffbd0 (>= v0x7fb0cacf9e90_0 0.0))))
(assert (let ((a!1 (=> F0x7fb0cacffbd0
               (or (<= v0x7fb0cacf9c10_0 0.0) (not (<= v0x7fb0cacf9f90_0 1.0))))))
  (=> pre!bb1.i.i!3 a!1)))
(assert (=> pre!bb1.i.i!4
    (=> F0x7fb0cacffbd0
        (or (>= v0x7fb0cacf9c10_0 1.0) (<= v0x7fb0cacf9d90_0 0.0)))))
(assert (=> pre!bb1.i.i!5 (=> F0x7fb0cacffbd0 (not (<= v0x7fb0cacf9f90_0 0.0)))))
(assert (=> pre!bb1.i.i!6 (=> F0x7fb0cacffbd0 (>= v0x7fb0cacf9c10_0 0.0))))
(assert (=> pre!bb1.i.i!7
    (=> F0x7fb0cacffbd0
        (or (>= v0x7fb0cacf9e90_0 1.0) (<= v0x7fb0cacf9e90_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fb0cacff990 (>= v0x7fb0cacf9f50_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fb0cacff990
       (or (>= v0x7fb0cacf8010_0 0.0) (<= v0x7fb0cacf9f50_0 0.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fb0cacff990 (>= v0x7fb0cacfa050_0 0.0))))
(assert (let ((a!1 (=> F0x7fb0cacff990
               (or (<= v0x7fb0cacf9e50_0 0.0) (not (<= v0x7fb0cacf8010_0 1.0))))))
  (= lemma!bb1.i.i!3 a!1)))
(assert (= lemma!bb1.i.i!4
   (=> F0x7fb0cacff990
       (or (>= v0x7fb0cacf9e50_0 1.0) (<= v0x7fb0cacf9f50_0 0.0)))))
(assert (= lemma!bb1.i.i!5 (=> F0x7fb0cacff990 (not (<= v0x7fb0cacf8010_0 0.0)))))
(assert (= lemma!bb1.i.i!6 (=> F0x7fb0cacff990 (>= v0x7fb0cacf9e50_0 0.0))))
(assert (= lemma!bb1.i.i!7
   (=> F0x7fb0cacff990
       (or (>= v0x7fb0cacfa050_0 1.0) (<= v0x7fb0cacfa050_0 0.0)))))
(assert (= lemma!bb2.i.i26.i.i!0 (=> F0x7fb0cacff9d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb1.i.i!6) (not lemma!bb1.i.i!6))
    (and (not post!bb1.i.i!7) (not lemma!bb1.i.i!7))
    (and (not post!bb2.i.i26.i.i!0) (not lemma!bb2.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5 pre!bb1.i.i!6 pre!bb1.i.i!7)
; (init: F0x7fb0cacffd10)
; (error: F0x7fb0cacff9d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb1.i.i!6 post!bb1.i.i!7 post!bb2.i.i26.i.i!0)
