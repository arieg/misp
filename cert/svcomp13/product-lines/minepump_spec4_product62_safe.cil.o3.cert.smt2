(declare-fun post!bb1.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f2ca7210510 () Bool)
(declare-fun F0x7f2ca7210410 () Bool)
(declare-fun v0x7f2ca720f390_0 () Bool)
(declare-fun v0x7f2ca720d950_0 () Bool)
(declare-fun v0x7f2ca720da90_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f2ca720d510_0 () Bool)
(declare-fun v0x7f2ca720c290_0 () Bool)
(declare-fun v0x7f2ca720df90_0 () Bool)
(declare-fun v0x7f2ca720bdd0_0 () Real)
(declare-fun E0x7f2ca720edd0 () Bool)
(declare-fun E0x7f2ca720ebd0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7f2ca7210550 () Bool)
(declare-fun v0x7f2ca720a690_0 () Real)
(declare-fun v0x7f2ca720e090_0 () Real)
(declare-fun v0x7f2ca720b210_0 () Bool)
(declare-fun v0x7f2ca720d650_0 () Real)
(declare-fun F0x7f2ca7210350 () Bool)
(declare-fun v0x7f2ca720e510_0 () Real)
(declare-fun v0x7f2ca720d810_0 () Real)
(declare-fun v0x7f2ca720e450_0 () Bool)
(declare-fun v0x7f2ca720d110_0 () Bool)
(declare-fun E0x7f2ca720e690 () Bool)
(declare-fun E0x7f2ca720ddd0 () Bool)
(declare-fun v0x7f2ca720cd50_0 () Bool)
(declare-fun v0x7f2ca720ce90_0 () Bool)
(declare-fun v0x7f2ca720a410_0 () Real)
(declare-fun v0x7f2ca720c3d0_0 () Real)
(declare-fun E0x7f2ca720c690 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f2ca720c510_0 () Bool)
(declare-fun v0x7f2ca720dd10_0 () Bool)
(declare-fun v0x7f2ca720be90_0 () Bool)
(declare-fun v0x7f2ca720d250_0 () Bool)
(declare-fun E0x7f2ca720c090 () Bool)
(declare-fun v0x7f2ca720ad10_0 () Real)
(declare-fun v0x7f2ca720b710_0 () Real)
(declare-fun v0x7f2ca720e1d0_0 () Bool)
(declare-fun E0x7f2ca720b7d0 () Bool)
(declare-fun v0x7f2ca720add0_0 () Bool)
(declare-fun v0x7f2ca720c5d0_0 () Real)
(declare-fun v0x7f2ca720b3d0_0 () Real)
(declare-fun E0x7f2ca720b990 () Bool)
(declare-fun E0x7f2ca720afd0 () Bool)
(declare-fun v0x7f2ca720b510_0 () Real)
(declare-fun F0x7f2ca7210110 () Bool)
(declare-fun v0x7f2ca720f610_0 () Bool)
(declare-fun E0x7f2ca720d310 () Bool)
(declare-fun F0x7f2ca72101d0 () Bool)
(declare-fun v0x7f2ca720bfd0_0 () Bool)
(declare-fun v0x7f2ca720dbd0_0 () Bool)
(declare-fun E0x7f2ca720e950 () Bool)
(declare-fun v0x7f2ca720a590_0 () Real)
(declare-fun v0x7f2ca720a750_0 () Real)
(declare-fun v0x7f2ca720b650_0 () Bool)
(declare-fun E0x7f2ca720cf50 () Bool)
(declare-fun v0x7f2ca720e5d0_0 () Real)
(declare-fun v0x7f2ca720ac50_0 () Bool)
(declare-fun v0x7f2ca720a650_0 () Real)
(declare-fun E0x7f2ca720e290 () Bool)
(declare-fun v0x7f2ca720af10_0 () Bool)
(declare-fun E0x7f2ca720c850 () Bool)
(declare-fun v0x7f2ca720f4d0_0 () Bool)
(declare-fun v0x7f2ca7207110_0 () Bool)
(declare-fun F0x7f2ca7210290 () Bool)
(declare-fun v0x7f2ca7207010_0 () Real)

(assert (=> F0x7f2ca7210290
    (and v0x7f2ca7207110_0
         (<= v0x7f2ca720a650_0 0.0)
         (>= v0x7f2ca720a650_0 0.0)
         (<= v0x7f2ca720a750_0 1.0)
         (>= v0x7f2ca720a750_0 1.0)
         (<= v0x7f2ca7207010_0 0.0)
         (>= v0x7f2ca7207010_0 0.0))))
(assert (=> F0x7f2ca7210290 F0x7f2ca72101d0))
(assert (let ((a!1 (=> v0x7f2ca720b650_0
               (or (and v0x7f2ca720af10_0
                        E0x7f2ca720b7d0
                        (<= v0x7f2ca720b710_0 v0x7f2ca720b510_0)
                        (>= v0x7f2ca720b710_0 v0x7f2ca720b510_0))
                   (and v0x7f2ca720ac50_0
                        E0x7f2ca720b990
                        v0x7f2ca720add0_0
                        (<= v0x7f2ca720b710_0 v0x7f2ca720a590_0)
                        (>= v0x7f2ca720b710_0 v0x7f2ca720a590_0)))))
      (a!2 (=> v0x7f2ca720b650_0
               (or (and E0x7f2ca720b7d0 (not E0x7f2ca720b990))
                   (and E0x7f2ca720b990 (not E0x7f2ca720b7d0)))))
      (a!3 (=> v0x7f2ca720c510_0
               (or (and v0x7f2ca720bfd0_0
                        E0x7f2ca720c690
                        (<= v0x7f2ca720c5d0_0 v0x7f2ca720c3d0_0)
                        (>= v0x7f2ca720c5d0_0 v0x7f2ca720c3d0_0))
                   (and v0x7f2ca720b650_0
                        E0x7f2ca720c850
                        v0x7f2ca720be90_0
                        (<= v0x7f2ca720c5d0_0 v0x7f2ca720a410_0)
                        (>= v0x7f2ca720c5d0_0 v0x7f2ca720a410_0)))))
      (a!4 (=> v0x7f2ca720c510_0
               (or (and E0x7f2ca720c690 (not E0x7f2ca720c850))
                   (and E0x7f2ca720c850 (not E0x7f2ca720c690)))))
      (a!5 (or (and v0x7f2ca720dd10_0
                    E0x7f2ca720e690
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720b710_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720b710_0))
                    (<= v0x7f2ca720e5d0_0 v0x7f2ca720e090_0)
                    (>= v0x7f2ca720e5d0_0 v0x7f2ca720e090_0))
               (and v0x7f2ca720ce90_0
                    E0x7f2ca720e950
                    (not v0x7f2ca720d110_0)
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720b710_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720b710_0))
                    (and (<= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)
                         (>= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)))
               (and v0x7f2ca720e1d0_0
                    E0x7f2ca720ebd0
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720d810_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720d810_0))
                    (and (<= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)
                         (>= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)))
               (and v0x7f2ca720d250_0
                    E0x7f2ca720edd0
                    (not v0x7f2ca720dbd0_0)
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720d810_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720d810_0))
                    (<= v0x7f2ca720e5d0_0 0.0)
                    (>= v0x7f2ca720e5d0_0 0.0))))
      (a!6 (=> v0x7f2ca720e450_0
               (or (and E0x7f2ca720e690
                        (not E0x7f2ca720e950)
                        (not E0x7f2ca720ebd0)
                        (not E0x7f2ca720edd0))
                   (and E0x7f2ca720e950
                        (not E0x7f2ca720e690)
                        (not E0x7f2ca720ebd0)
                        (not E0x7f2ca720edd0))
                   (and E0x7f2ca720ebd0
                        (not E0x7f2ca720e690)
                        (not E0x7f2ca720e950)
                        (not E0x7f2ca720edd0))
                   (and E0x7f2ca720edd0
                        (not E0x7f2ca720e690)
                        (not E0x7f2ca720e950)
                        (not E0x7f2ca720ebd0))))))
(let ((a!7 (and (=> v0x7f2ca720af10_0
                    (and v0x7f2ca720ac50_0
                         E0x7f2ca720afd0
                         (not v0x7f2ca720add0_0)))
                (=> v0x7f2ca720af10_0 E0x7f2ca720afd0)
                a!1
                a!2
                (=> v0x7f2ca720bfd0_0
                    (and v0x7f2ca720b650_0
                         E0x7f2ca720c090
                         (not v0x7f2ca720be90_0)))
                (=> v0x7f2ca720bfd0_0 E0x7f2ca720c090)
                a!3
                a!4
                (=> v0x7f2ca720ce90_0
                    (and v0x7f2ca720c510_0 E0x7f2ca720cf50 v0x7f2ca720cd50_0))
                (=> v0x7f2ca720ce90_0 E0x7f2ca720cf50)
                (=> v0x7f2ca720d250_0
                    (and v0x7f2ca720c510_0
                         E0x7f2ca720d310
                         (not v0x7f2ca720cd50_0)))
                (=> v0x7f2ca720d250_0 E0x7f2ca720d310)
                (=> v0x7f2ca720dd10_0
                    (and v0x7f2ca720ce90_0 E0x7f2ca720ddd0 v0x7f2ca720d110_0))
                (=> v0x7f2ca720dd10_0 E0x7f2ca720ddd0)
                (=> v0x7f2ca720e1d0_0
                    (and v0x7f2ca720d250_0 E0x7f2ca720e290 v0x7f2ca720dbd0_0))
                (=> v0x7f2ca720e1d0_0 E0x7f2ca720e290)
                (=> v0x7f2ca720e450_0 a!5)
                a!6
                v0x7f2ca720e450_0
                v0x7f2ca720f610_0
                (<= v0x7f2ca720a650_0 v0x7f2ca720c5d0_0)
                (>= v0x7f2ca720a650_0 v0x7f2ca720c5d0_0)
                (<= v0x7f2ca720a750_0 v0x7f2ca720e510_0)
                (>= v0x7f2ca720a750_0 v0x7f2ca720e510_0)
                (<= v0x7f2ca7207010_0 v0x7f2ca720e5d0_0)
                (>= v0x7f2ca7207010_0 v0x7f2ca720e5d0_0)
                (= v0x7f2ca720add0_0 (= v0x7f2ca720ad10_0 0.0))
                (= v0x7f2ca720b210_0 (< v0x7f2ca720a590_0 2.0))
                (= v0x7f2ca720b3d0_0 (ite v0x7f2ca720b210_0 1.0 0.0))
                (= v0x7f2ca720b510_0 (+ v0x7f2ca720b3d0_0 v0x7f2ca720a590_0))
                (= v0x7f2ca720be90_0 (= v0x7f2ca720bdd0_0 0.0))
                (= v0x7f2ca720c290_0 (= v0x7f2ca720a410_0 0.0))
                (= v0x7f2ca720c3d0_0 (ite v0x7f2ca720c290_0 1.0 0.0))
                (= v0x7f2ca720cd50_0 (= v0x7f2ca720a690_0 0.0))
                (= v0x7f2ca720d110_0 (> v0x7f2ca720b710_0 1.0))
                (= v0x7f2ca720d510_0 (> v0x7f2ca720b710_0 0.0))
                (= v0x7f2ca720d650_0 (+ v0x7f2ca720b710_0 (- 1.0)))
                (= v0x7f2ca720d810_0
                   (ite v0x7f2ca720d510_0 v0x7f2ca720d650_0 v0x7f2ca720b710_0))
                (= v0x7f2ca720d950_0 (= v0x7f2ca720c5d0_0 0.0))
                (= v0x7f2ca720da90_0 (= v0x7f2ca720d810_0 0.0))
                (= v0x7f2ca720dbd0_0 (and v0x7f2ca720d950_0 v0x7f2ca720da90_0))
                (= v0x7f2ca720df90_0 (= v0x7f2ca720c5d0_0 0.0))
                (= v0x7f2ca720e090_0
                   (ite v0x7f2ca720df90_0 1.0 v0x7f2ca720a690_0))
                (= v0x7f2ca720f390_0 (not (= v0x7f2ca720e510_0 0.0)))
                (= v0x7f2ca720f4d0_0 (= v0x7f2ca720e5d0_0 0.0))
                (= v0x7f2ca720f610_0 (or v0x7f2ca720f4d0_0 v0x7f2ca720f390_0)))))
  (=> F0x7f2ca7210110 a!7))))
(assert (=> F0x7f2ca7210110 F0x7f2ca7210350))
(assert (let ((a!1 (=> v0x7f2ca720b650_0
               (or (and v0x7f2ca720af10_0
                        E0x7f2ca720b7d0
                        (<= v0x7f2ca720b710_0 v0x7f2ca720b510_0)
                        (>= v0x7f2ca720b710_0 v0x7f2ca720b510_0))
                   (and v0x7f2ca720ac50_0
                        E0x7f2ca720b990
                        v0x7f2ca720add0_0
                        (<= v0x7f2ca720b710_0 v0x7f2ca720a590_0)
                        (>= v0x7f2ca720b710_0 v0x7f2ca720a590_0)))))
      (a!2 (=> v0x7f2ca720b650_0
               (or (and E0x7f2ca720b7d0 (not E0x7f2ca720b990))
                   (and E0x7f2ca720b990 (not E0x7f2ca720b7d0)))))
      (a!3 (=> v0x7f2ca720c510_0
               (or (and v0x7f2ca720bfd0_0
                        E0x7f2ca720c690
                        (<= v0x7f2ca720c5d0_0 v0x7f2ca720c3d0_0)
                        (>= v0x7f2ca720c5d0_0 v0x7f2ca720c3d0_0))
                   (and v0x7f2ca720b650_0
                        E0x7f2ca720c850
                        v0x7f2ca720be90_0
                        (<= v0x7f2ca720c5d0_0 v0x7f2ca720a410_0)
                        (>= v0x7f2ca720c5d0_0 v0x7f2ca720a410_0)))))
      (a!4 (=> v0x7f2ca720c510_0
               (or (and E0x7f2ca720c690 (not E0x7f2ca720c850))
                   (and E0x7f2ca720c850 (not E0x7f2ca720c690)))))
      (a!5 (or (and v0x7f2ca720dd10_0
                    E0x7f2ca720e690
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720b710_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720b710_0))
                    (<= v0x7f2ca720e5d0_0 v0x7f2ca720e090_0)
                    (>= v0x7f2ca720e5d0_0 v0x7f2ca720e090_0))
               (and v0x7f2ca720ce90_0
                    E0x7f2ca720e950
                    (not v0x7f2ca720d110_0)
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720b710_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720b710_0))
                    (and (<= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)
                         (>= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)))
               (and v0x7f2ca720e1d0_0
                    E0x7f2ca720ebd0
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720d810_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720d810_0))
                    (and (<= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)
                         (>= v0x7f2ca720e5d0_0 v0x7f2ca720a690_0)))
               (and v0x7f2ca720d250_0
                    E0x7f2ca720edd0
                    (not v0x7f2ca720dbd0_0)
                    (and (<= v0x7f2ca720e510_0 v0x7f2ca720d810_0)
                         (>= v0x7f2ca720e510_0 v0x7f2ca720d810_0))
                    (<= v0x7f2ca720e5d0_0 0.0)
                    (>= v0x7f2ca720e5d0_0 0.0))))
      (a!6 (=> v0x7f2ca720e450_0
               (or (and E0x7f2ca720e690
                        (not E0x7f2ca720e950)
                        (not E0x7f2ca720ebd0)
                        (not E0x7f2ca720edd0))
                   (and E0x7f2ca720e950
                        (not E0x7f2ca720e690)
                        (not E0x7f2ca720ebd0)
                        (not E0x7f2ca720edd0))
                   (and E0x7f2ca720ebd0
                        (not E0x7f2ca720e690)
                        (not E0x7f2ca720e950)
                        (not E0x7f2ca720edd0))
                   (and E0x7f2ca720edd0
                        (not E0x7f2ca720e690)
                        (not E0x7f2ca720e950)
                        (not E0x7f2ca720ebd0))))))
(let ((a!7 (and (=> v0x7f2ca720af10_0
                    (and v0x7f2ca720ac50_0
                         E0x7f2ca720afd0
                         (not v0x7f2ca720add0_0)))
                (=> v0x7f2ca720af10_0 E0x7f2ca720afd0)
                a!1
                a!2
                (=> v0x7f2ca720bfd0_0
                    (and v0x7f2ca720b650_0
                         E0x7f2ca720c090
                         (not v0x7f2ca720be90_0)))
                (=> v0x7f2ca720bfd0_0 E0x7f2ca720c090)
                a!3
                a!4
                (=> v0x7f2ca720ce90_0
                    (and v0x7f2ca720c510_0 E0x7f2ca720cf50 v0x7f2ca720cd50_0))
                (=> v0x7f2ca720ce90_0 E0x7f2ca720cf50)
                (=> v0x7f2ca720d250_0
                    (and v0x7f2ca720c510_0
                         E0x7f2ca720d310
                         (not v0x7f2ca720cd50_0)))
                (=> v0x7f2ca720d250_0 E0x7f2ca720d310)
                (=> v0x7f2ca720dd10_0
                    (and v0x7f2ca720ce90_0 E0x7f2ca720ddd0 v0x7f2ca720d110_0))
                (=> v0x7f2ca720dd10_0 E0x7f2ca720ddd0)
                (=> v0x7f2ca720e1d0_0
                    (and v0x7f2ca720d250_0 E0x7f2ca720e290 v0x7f2ca720dbd0_0))
                (=> v0x7f2ca720e1d0_0 E0x7f2ca720e290)
                (=> v0x7f2ca720e450_0 a!5)
                a!6
                v0x7f2ca720e450_0
                (not v0x7f2ca720f610_0)
                (= v0x7f2ca720add0_0 (= v0x7f2ca720ad10_0 0.0))
                (= v0x7f2ca720b210_0 (< v0x7f2ca720a590_0 2.0))
                (= v0x7f2ca720b3d0_0 (ite v0x7f2ca720b210_0 1.0 0.0))
                (= v0x7f2ca720b510_0 (+ v0x7f2ca720b3d0_0 v0x7f2ca720a590_0))
                (= v0x7f2ca720be90_0 (= v0x7f2ca720bdd0_0 0.0))
                (= v0x7f2ca720c290_0 (= v0x7f2ca720a410_0 0.0))
                (= v0x7f2ca720c3d0_0 (ite v0x7f2ca720c290_0 1.0 0.0))
                (= v0x7f2ca720cd50_0 (= v0x7f2ca720a690_0 0.0))
                (= v0x7f2ca720d110_0 (> v0x7f2ca720b710_0 1.0))
                (= v0x7f2ca720d510_0 (> v0x7f2ca720b710_0 0.0))
                (= v0x7f2ca720d650_0 (+ v0x7f2ca720b710_0 (- 1.0)))
                (= v0x7f2ca720d810_0
                   (ite v0x7f2ca720d510_0 v0x7f2ca720d650_0 v0x7f2ca720b710_0))
                (= v0x7f2ca720d950_0 (= v0x7f2ca720c5d0_0 0.0))
                (= v0x7f2ca720da90_0 (= v0x7f2ca720d810_0 0.0))
                (= v0x7f2ca720dbd0_0 (and v0x7f2ca720d950_0 v0x7f2ca720da90_0))
                (= v0x7f2ca720df90_0 (= v0x7f2ca720c5d0_0 0.0))
                (= v0x7f2ca720e090_0
                   (ite v0x7f2ca720df90_0 1.0 v0x7f2ca720a690_0))
                (= v0x7f2ca720f390_0 (not (= v0x7f2ca720e510_0 0.0)))
                (= v0x7f2ca720f4d0_0 (= v0x7f2ca720e5d0_0 0.0))
                (= v0x7f2ca720f610_0 (or v0x7f2ca720f4d0_0 v0x7f2ca720f390_0)))))
  (=> F0x7f2ca7210410 a!7))))
(assert (=> F0x7f2ca7210410 F0x7f2ca7210350))
(assert (=> F0x7f2ca7210550 (or F0x7f2ca7210290 F0x7f2ca7210110)))
(assert (=> F0x7f2ca7210510 F0x7f2ca7210410))
(assert (=> pre!entry!0 (=> F0x7f2ca72101d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f2ca7210350 (>= v0x7f2ca720a410_0 0.0))))
(assert (let ((a!1 (=> F0x7f2ca7210350
               (or (not (<= v0x7f2ca720a590_0 1.0)) (<= v0x7f2ca720a690_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (=> pre!bb1.i.i!2 (=> F0x7f2ca7210350 (>= v0x7f2ca720a690_0 0.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f2ca7210550 (>= v0x7f2ca720a650_0 0.0))))
(assert (let ((a!1 (=> F0x7f2ca7210550
               (or (not (<= v0x7f2ca720a750_0 1.0)) (<= v0x7f2ca7207010_0 0.0)))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!2 (=> F0x7f2ca7210550 (>= v0x7f2ca7207010_0 0.0))))
(assert (= lemma!bb1.i.i43.i.i!0 (=> F0x7f2ca7210510 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i43.i.i!0) (not lemma!bb1.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2)
; (init: F0x7f2ca72101d0)
; (error: F0x7f2ca7210510)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i43.i.i!0)
