(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i29.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun post!bb1.i.i29.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f707e337a90 () Bool)
(declare-fun F0x7f707e337890 () Bool)
(declare-fun v0x7f707e335510_0 () Bool)
(declare-fun F0x7f707e337a50 () Bool)
(declare-fun v0x7f707e334e50_0 () Real)
(declare-fun v0x7f707e333a90_0 () Bool)
(declare-fun v0x7f707e336b50_0 () Bool)
(declare-fun E0x7f707e336350 () Bool)
(declare-fun v0x7f707e331e90_0 () Real)
(declare-fun v0x7f707e335610_0 () Real)
(declare-fun v0x7f707e335b50_0 () Real)
(declare-fun v0x7f707e3359d0_0 () Bool)
(declare-fun v0x7f707e335150_0 () Bool)
(declare-fun v0x7f707e334910_0 () Bool)
(declare-fun E0x7f707e335350 () Bool)
(declare-fun v0x7f707e335a90_0 () Real)
(declare-fun E0x7f707e334b10 () Bool)
(declare-fun v0x7f707e332bd0_0 () Real)
(declare-fun v0x7f707e334a50_0 () Bool)
(declare-fun v0x7f707e334550_0 () Bool)
(declare-fun E0x7f707e334750 () Bool)
(declare-fun v0x7f707e333dd0_0 () Real)
(declare-fun v0x7f707e332a10_0 () Bool)
(declare-fun E0x7f707e333e90 () Bool)
(declare-fun v0x7f707e333d10_0 () Bool)
(declare-fun v0x7f707e333690_0 () Bool)
(declare-fun v0x7f707e335750_0 () Bool)
(declare-fun E0x7f707e335ed0 () Bool)
(declare-fun v0x7f707e3337d0_0 () Bool)
(declare-fun v0x7f707e331d90_0 () Real)
(declare-fun v0x7f707e334690_0 () Bool)
(declare-fun v0x7f707e332d10_0 () Real)
(declare-fun v0x7f707e334d10_0 () Bool)
(declare-fun v0x7f707e332f10_0 () Real)
(declare-fun E0x7f707e334050 () Bool)
(declare-fun v0x7f707e332e50_0 () Bool)
(declare-fun v0x7f707e3325d0_0 () Bool)
(declare-fun E0x7f707e3327d0 () Bool)
(declare-fun v0x7f707e333bd0_0 () Real)
(declare-fun v0x7f707e332450_0 () Bool)
(declare-fun v0x7f707e336a10_0 () Bool)
(declare-fun F0x7f707e337710 () Bool)
(declare-fun v0x7f707e330010_0 () Real)
(declare-fun F0x7f707e3377d0 () Bool)
(declare-fun v0x7f707e335290_0 () Bool)
(declare-fun v0x7f707e331f50_0 () Real)
(declare-fun v0x7f707e332510_0 () Real)
(declare-fun v0x7f707e330110_0 () Bool)
(declare-fun v0x7f707e336910_0 () Bool)
(declare-fun E0x7f707e333190 () Bool)
(declare-fun v0x7f707e332710_0 () Bool)
(declare-fun E0x7f707e335810 () Bool)
(declare-fun F0x7f707e337950 () Bool)
(declare-fun v0x7f707e331c10_0 () Real)
(declare-fun v0x7f707e335010_0 () Real)
(declare-fun v0x7f707e3335d0_0 () Real)
(declare-fun v0x7f707e331e50_0 () Real)
(declare-fun E0x7f707e332fd0 () Bool)
(declare-fun E0x7f707e336150 () Bool)
(declare-fun E0x7f707e335c10 () Bool)
(declare-fun E0x7f707e333890 () Bool)
(declare-fun F0x7f707e337650 () Bool)

(assert (=> F0x7f707e337650
    (and v0x7f707e330110_0
         (<= v0x7f707e331e50_0 0.0)
         (>= v0x7f707e331e50_0 0.0)
         (<= v0x7f707e331f50_0 1.0)
         (>= v0x7f707e331f50_0 1.0)
         (<= v0x7f707e330010_0 0.0)
         (>= v0x7f707e330010_0 0.0))))
(assert (=> F0x7f707e337650 F0x7f707e337710))
(assert (let ((a!1 (=> v0x7f707e332e50_0
               (or (and v0x7f707e332710_0
                        E0x7f707e332fd0
                        (<= v0x7f707e332f10_0 v0x7f707e332d10_0)
                        (>= v0x7f707e332f10_0 v0x7f707e332d10_0))
                   (and v0x7f707e332450_0
                        E0x7f707e333190
                        v0x7f707e3325d0_0
                        (<= v0x7f707e332f10_0 v0x7f707e331d90_0)
                        (>= v0x7f707e332f10_0 v0x7f707e331d90_0)))))
      (a!2 (=> v0x7f707e332e50_0
               (or (and E0x7f707e332fd0 (not E0x7f707e333190))
                   (and E0x7f707e333190 (not E0x7f707e332fd0)))))
      (a!3 (=> v0x7f707e333d10_0
               (or (and v0x7f707e3337d0_0
                        E0x7f707e333e90
                        (<= v0x7f707e333dd0_0 v0x7f707e333bd0_0)
                        (>= v0x7f707e333dd0_0 v0x7f707e333bd0_0))
                   (and v0x7f707e332e50_0
                        E0x7f707e334050
                        v0x7f707e333690_0
                        (<= v0x7f707e333dd0_0 v0x7f707e331c10_0)
                        (>= v0x7f707e333dd0_0 v0x7f707e331c10_0)))))
      (a!4 (=> v0x7f707e333d10_0
               (or (and E0x7f707e333e90 (not E0x7f707e334050))
                   (and E0x7f707e334050 (not E0x7f707e333e90)))))
      (a!5 (or (and v0x7f707e335290_0
                    E0x7f707e335c10
                    (and (<= v0x7f707e335a90_0 v0x7f707e332f10_0)
                         (>= v0x7f707e335a90_0 v0x7f707e332f10_0))
                    (<= v0x7f707e335b50_0 v0x7f707e335610_0)
                    (>= v0x7f707e335b50_0 v0x7f707e335610_0))
               (and v0x7f707e334690_0
                    E0x7f707e335ed0
                    (not v0x7f707e334910_0)
                    (and (<= v0x7f707e335a90_0 v0x7f707e332f10_0)
                         (>= v0x7f707e335a90_0 v0x7f707e332f10_0))
                    (and (<= v0x7f707e335b50_0 v0x7f707e331e90_0)
                         (>= v0x7f707e335b50_0 v0x7f707e331e90_0)))
               (and v0x7f707e335750_0
                    E0x7f707e336150
                    (and (<= v0x7f707e335a90_0 v0x7f707e335010_0)
                         (>= v0x7f707e335a90_0 v0x7f707e335010_0))
                    (and (<= v0x7f707e335b50_0 v0x7f707e331e90_0)
                         (>= v0x7f707e335b50_0 v0x7f707e331e90_0)))
               (and v0x7f707e334a50_0
                    E0x7f707e336350
                    (not v0x7f707e335150_0)
                    (and (<= v0x7f707e335a90_0 v0x7f707e335010_0)
                         (>= v0x7f707e335a90_0 v0x7f707e335010_0))
                    (<= v0x7f707e335b50_0 0.0)
                    (>= v0x7f707e335b50_0 0.0))))
      (a!6 (=> v0x7f707e3359d0_0
               (or (and E0x7f707e335c10
                        (not E0x7f707e335ed0)
                        (not E0x7f707e336150)
                        (not E0x7f707e336350))
                   (and E0x7f707e335ed0
                        (not E0x7f707e335c10)
                        (not E0x7f707e336150)
                        (not E0x7f707e336350))
                   (and E0x7f707e336150
                        (not E0x7f707e335c10)
                        (not E0x7f707e335ed0)
                        (not E0x7f707e336350))
                   (and E0x7f707e336350
                        (not E0x7f707e335c10)
                        (not E0x7f707e335ed0)
                        (not E0x7f707e336150))))))
(let ((a!7 (and (=> v0x7f707e332710_0
                    (and v0x7f707e332450_0
                         E0x7f707e3327d0
                         (not v0x7f707e3325d0_0)))
                (=> v0x7f707e332710_0 E0x7f707e3327d0)
                a!1
                a!2
                (=> v0x7f707e3337d0_0
                    (and v0x7f707e332e50_0
                         E0x7f707e333890
                         (not v0x7f707e333690_0)))
                (=> v0x7f707e3337d0_0 E0x7f707e333890)
                a!3
                a!4
                (=> v0x7f707e334690_0
                    (and v0x7f707e333d10_0 E0x7f707e334750 v0x7f707e334550_0))
                (=> v0x7f707e334690_0 E0x7f707e334750)
                (=> v0x7f707e334a50_0
                    (and v0x7f707e333d10_0
                         E0x7f707e334b10
                         (not v0x7f707e334550_0)))
                (=> v0x7f707e334a50_0 E0x7f707e334b10)
                (=> v0x7f707e335290_0
                    (and v0x7f707e334690_0 E0x7f707e335350 v0x7f707e334910_0))
                (=> v0x7f707e335290_0 E0x7f707e335350)
                (=> v0x7f707e335750_0
                    (and v0x7f707e334a50_0 E0x7f707e335810 v0x7f707e335150_0))
                (=> v0x7f707e335750_0 E0x7f707e335810)
                (=> v0x7f707e3359d0_0 a!5)
                a!6
                v0x7f707e3359d0_0
                v0x7f707e336b50_0
                (<= v0x7f707e331e50_0 v0x7f707e333dd0_0)
                (>= v0x7f707e331e50_0 v0x7f707e333dd0_0)
                (<= v0x7f707e331f50_0 v0x7f707e335a90_0)
                (>= v0x7f707e331f50_0 v0x7f707e335a90_0)
                (<= v0x7f707e330010_0 v0x7f707e335b50_0)
                (>= v0x7f707e330010_0 v0x7f707e335b50_0)
                (= v0x7f707e3325d0_0 (= v0x7f707e332510_0 0.0))
                (= v0x7f707e332a10_0 (< v0x7f707e331d90_0 2.0))
                (= v0x7f707e332bd0_0 (ite v0x7f707e332a10_0 1.0 0.0))
                (= v0x7f707e332d10_0 (+ v0x7f707e332bd0_0 v0x7f707e331d90_0))
                (= v0x7f707e333690_0 (= v0x7f707e3335d0_0 0.0))
                (= v0x7f707e333a90_0 (= v0x7f707e331c10_0 0.0))
                (= v0x7f707e333bd0_0 (ite v0x7f707e333a90_0 1.0 0.0))
                (= v0x7f707e334550_0 (= v0x7f707e331e90_0 0.0))
                (= v0x7f707e334910_0 (> v0x7f707e332f10_0 1.0))
                (= v0x7f707e334d10_0 (> v0x7f707e332f10_0 0.0))
                (= v0x7f707e334e50_0 (+ v0x7f707e332f10_0 (- 1.0)))
                (= v0x7f707e335010_0
                   (ite v0x7f707e334d10_0 v0x7f707e334e50_0 v0x7f707e332f10_0))
                (= v0x7f707e335150_0 (= v0x7f707e333dd0_0 0.0))
                (= v0x7f707e335510_0 (= v0x7f707e333dd0_0 0.0))
                (= v0x7f707e335610_0
                   (ite v0x7f707e335510_0 1.0 v0x7f707e331e90_0))
                (= v0x7f707e336910_0 (= v0x7f707e333dd0_0 0.0))
                (= v0x7f707e336a10_0 (= v0x7f707e335b50_0 0.0))
                (= v0x7f707e336b50_0 (or v0x7f707e336a10_0 v0x7f707e336910_0)))))
  (=> F0x7f707e3377d0 a!7))))
(assert (=> F0x7f707e3377d0 F0x7f707e337890))
(assert (let ((a!1 (=> v0x7f707e332e50_0
               (or (and v0x7f707e332710_0
                        E0x7f707e332fd0
                        (<= v0x7f707e332f10_0 v0x7f707e332d10_0)
                        (>= v0x7f707e332f10_0 v0x7f707e332d10_0))
                   (and v0x7f707e332450_0
                        E0x7f707e333190
                        v0x7f707e3325d0_0
                        (<= v0x7f707e332f10_0 v0x7f707e331d90_0)
                        (>= v0x7f707e332f10_0 v0x7f707e331d90_0)))))
      (a!2 (=> v0x7f707e332e50_0
               (or (and E0x7f707e332fd0 (not E0x7f707e333190))
                   (and E0x7f707e333190 (not E0x7f707e332fd0)))))
      (a!3 (=> v0x7f707e333d10_0
               (or (and v0x7f707e3337d0_0
                        E0x7f707e333e90
                        (<= v0x7f707e333dd0_0 v0x7f707e333bd0_0)
                        (>= v0x7f707e333dd0_0 v0x7f707e333bd0_0))
                   (and v0x7f707e332e50_0
                        E0x7f707e334050
                        v0x7f707e333690_0
                        (<= v0x7f707e333dd0_0 v0x7f707e331c10_0)
                        (>= v0x7f707e333dd0_0 v0x7f707e331c10_0)))))
      (a!4 (=> v0x7f707e333d10_0
               (or (and E0x7f707e333e90 (not E0x7f707e334050))
                   (and E0x7f707e334050 (not E0x7f707e333e90)))))
      (a!5 (or (and v0x7f707e335290_0
                    E0x7f707e335c10
                    (and (<= v0x7f707e335a90_0 v0x7f707e332f10_0)
                         (>= v0x7f707e335a90_0 v0x7f707e332f10_0))
                    (<= v0x7f707e335b50_0 v0x7f707e335610_0)
                    (>= v0x7f707e335b50_0 v0x7f707e335610_0))
               (and v0x7f707e334690_0
                    E0x7f707e335ed0
                    (not v0x7f707e334910_0)
                    (and (<= v0x7f707e335a90_0 v0x7f707e332f10_0)
                         (>= v0x7f707e335a90_0 v0x7f707e332f10_0))
                    (and (<= v0x7f707e335b50_0 v0x7f707e331e90_0)
                         (>= v0x7f707e335b50_0 v0x7f707e331e90_0)))
               (and v0x7f707e335750_0
                    E0x7f707e336150
                    (and (<= v0x7f707e335a90_0 v0x7f707e335010_0)
                         (>= v0x7f707e335a90_0 v0x7f707e335010_0))
                    (and (<= v0x7f707e335b50_0 v0x7f707e331e90_0)
                         (>= v0x7f707e335b50_0 v0x7f707e331e90_0)))
               (and v0x7f707e334a50_0
                    E0x7f707e336350
                    (not v0x7f707e335150_0)
                    (and (<= v0x7f707e335a90_0 v0x7f707e335010_0)
                         (>= v0x7f707e335a90_0 v0x7f707e335010_0))
                    (<= v0x7f707e335b50_0 0.0)
                    (>= v0x7f707e335b50_0 0.0))))
      (a!6 (=> v0x7f707e3359d0_0
               (or (and E0x7f707e335c10
                        (not E0x7f707e335ed0)
                        (not E0x7f707e336150)
                        (not E0x7f707e336350))
                   (and E0x7f707e335ed0
                        (not E0x7f707e335c10)
                        (not E0x7f707e336150)
                        (not E0x7f707e336350))
                   (and E0x7f707e336150
                        (not E0x7f707e335c10)
                        (not E0x7f707e335ed0)
                        (not E0x7f707e336350))
                   (and E0x7f707e336350
                        (not E0x7f707e335c10)
                        (not E0x7f707e335ed0)
                        (not E0x7f707e336150))))))
(let ((a!7 (and (=> v0x7f707e332710_0
                    (and v0x7f707e332450_0
                         E0x7f707e3327d0
                         (not v0x7f707e3325d0_0)))
                (=> v0x7f707e332710_0 E0x7f707e3327d0)
                a!1
                a!2
                (=> v0x7f707e3337d0_0
                    (and v0x7f707e332e50_0
                         E0x7f707e333890
                         (not v0x7f707e333690_0)))
                (=> v0x7f707e3337d0_0 E0x7f707e333890)
                a!3
                a!4
                (=> v0x7f707e334690_0
                    (and v0x7f707e333d10_0 E0x7f707e334750 v0x7f707e334550_0))
                (=> v0x7f707e334690_0 E0x7f707e334750)
                (=> v0x7f707e334a50_0
                    (and v0x7f707e333d10_0
                         E0x7f707e334b10
                         (not v0x7f707e334550_0)))
                (=> v0x7f707e334a50_0 E0x7f707e334b10)
                (=> v0x7f707e335290_0
                    (and v0x7f707e334690_0 E0x7f707e335350 v0x7f707e334910_0))
                (=> v0x7f707e335290_0 E0x7f707e335350)
                (=> v0x7f707e335750_0
                    (and v0x7f707e334a50_0 E0x7f707e335810 v0x7f707e335150_0))
                (=> v0x7f707e335750_0 E0x7f707e335810)
                (=> v0x7f707e3359d0_0 a!5)
                a!6
                v0x7f707e3359d0_0
                (not v0x7f707e336b50_0)
                (= v0x7f707e3325d0_0 (= v0x7f707e332510_0 0.0))
                (= v0x7f707e332a10_0 (< v0x7f707e331d90_0 2.0))
                (= v0x7f707e332bd0_0 (ite v0x7f707e332a10_0 1.0 0.0))
                (= v0x7f707e332d10_0 (+ v0x7f707e332bd0_0 v0x7f707e331d90_0))
                (= v0x7f707e333690_0 (= v0x7f707e3335d0_0 0.0))
                (= v0x7f707e333a90_0 (= v0x7f707e331c10_0 0.0))
                (= v0x7f707e333bd0_0 (ite v0x7f707e333a90_0 1.0 0.0))
                (= v0x7f707e334550_0 (= v0x7f707e331e90_0 0.0))
                (= v0x7f707e334910_0 (> v0x7f707e332f10_0 1.0))
                (= v0x7f707e334d10_0 (> v0x7f707e332f10_0 0.0))
                (= v0x7f707e334e50_0 (+ v0x7f707e332f10_0 (- 1.0)))
                (= v0x7f707e335010_0
                   (ite v0x7f707e334d10_0 v0x7f707e334e50_0 v0x7f707e332f10_0))
                (= v0x7f707e335150_0 (= v0x7f707e333dd0_0 0.0))
                (= v0x7f707e335510_0 (= v0x7f707e333dd0_0 0.0))
                (= v0x7f707e335610_0
                   (ite v0x7f707e335510_0 1.0 v0x7f707e331e90_0))
                (= v0x7f707e336910_0 (= v0x7f707e333dd0_0 0.0))
                (= v0x7f707e336a10_0 (= v0x7f707e335b50_0 0.0))
                (= v0x7f707e336b50_0 (or v0x7f707e336a10_0 v0x7f707e336910_0)))))
  (=> F0x7f707e337950 a!7))))
(assert (=> F0x7f707e337950 F0x7f707e337890))
(assert (=> F0x7f707e337a90 (or F0x7f707e337650 F0x7f707e3377d0)))
(assert (=> F0x7f707e337a50 F0x7f707e337950))
(assert (=> pre!entry!0 (=> F0x7f707e337710 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f707e337890 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f707e337a90 true)))
(assert (= lemma!bb1.i.i29.i.i!0 (=> F0x7f707e337a50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i29.i.i!0) (not lemma!bb1.i.i29.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f707e337710)
; (error: F0x7f707e337a50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i29.i.i!0)
