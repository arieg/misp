(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fc2a95a8510_0 () Bool)
(declare-fun v0x7fc2a95a3d10_0 () Real)
(declare-fun v0x7fc2a95a7050_0 () Real)
(declare-fun v0x7fc2a95a6f10_0 () Bool)
(declare-fun v0x7fc2a95a69d0_0 () Bool)
(declare-fun v0x7fc2a95a8790_0 () Bool)
(declare-fun v0x7fc2a95a3e10_0 () Real)
(declare-fun E0x7fc2a95a7c10 () Bool)
(declare-fun v0x7fc2a95a6b10_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fc2a95a9ad0 () Bool)
(declare-fun v0x7fc2a95a77d0_0 () Real)
(declare-fun v0x7fc2a95a7350_0 () Bool)
(declare-fun v0x7fc2a95a4c90_0 () Real)
(declare-fun v0x7fc2a95a7490_0 () Bool)
(declare-fun v0x7fc2a95a45d0_0 () Real)
(declare-fun E0x7fc2a95a6d10 () Bool)
(declare-fun E0x7fc2a95a7550 () Bool)
(declare-fun v0x7fc2a95a6c50_0 () Bool)
(declare-fun E0x7fc2a95a7950 () Bool)
(declare-fun E0x7fc2a95a6810 () Bool)
(declare-fun v0x7fc2a95a3a90_0 () Real)
(declare-fun E0x7fc2a95a5f50 () Bool)
(declare-fun v0x7fc2a95a5dd0_0 () Bool)
(declare-fun F0x7fc2a95a9c10 () Bool)
(declare-fun E0x7fc2a95a7ed0 () Bool)
(declare-fun v0x7fc2a95a7710_0 () Bool)
(declare-fun v0x7fc2a95a5750_0 () Bool)
(declare-fun E0x7fc2a95a8850 () Bool)
(declare-fun v0x7fc2a95a8650_0 () Bool)
(declare-fun v0x7fc2a95a5e90_0 () Real)
(declare-fun E0x7fc2a95a5950 () Bool)
(declare-fun v0x7fc2a95a6750_0 () Bool)
(declare-fun v0x7fc2a95a5890_0 () Bool)
(declare-fun v0x7fc2a95a3c10_0 () Real)
(declare-fun v0x7fc2a95a8a50_0 () Bool)
(declare-fun v0x7fc2a95a7210_0 () Real)
(declare-fun v0x7fc2a95a4fd0_0 () Real)
(declare-fun v0x7fc2a95a4690_0 () Bool)
(declare-fun F0x7fc2a95a9a10 () Bool)
(declare-fun v0x7fc2a95a4dd0_0 () Real)
(declare-fun v0x7fc2a95a8410_0 () Bool)
(declare-fun F0x7fc2a95a9950 () Bool)
(declare-fun v0x7fc2a95a5c90_0 () Real)
(declare-fun v0x7fc2a95a4510_0 () Bool)
(declare-fun F0x7fc2a95a9850 () Bool)
(declare-fun v0x7fc2a95a6610_0 () Bool)
(declare-fun v0x7fc2a95a5b50_0 () Bool)
(declare-fun v0x7fc2a95a3dd0_0 () Real)
(declare-fun v0x7fc2a95a5690_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fc2a95a3cd0_0 () Real)
(declare-fun v0x7fc2a95a3ed0_0 () Real)
(declare-fun E0x7fc2a95a4890 () Bool)
(declare-fun v0x7fc2a95a7890_0 () Real)
(declare-fun E0x7fc2a95a5250 () Bool)
(declare-fun E0x7fc2a95a5090 () Bool)
(declare-fun v0x7fc2a95a2110_0 () Bool)
(declare-fun v0x7fc2a95a4ad0_0 () Bool)
(declare-fun v0x7fc2a95a2010_0 () Real)
(declare-fun F0x7fc2a95a9bd0 () Bool)
(declare-fun v0x7fc2a95a47d0_0 () Bool)
(declare-fun v0x7fc2a95a4f10_0 () Bool)
(declare-fun E0x7fc2a95a6110 () Bool)
(declare-fun F0x7fc2a95a9910 () Bool)

(assert (=> F0x7fc2a95a9910
    (and v0x7fc2a95a2110_0
         (<= v0x7fc2a95a3cd0_0 0.0)
         (>= v0x7fc2a95a3cd0_0 0.0)
         (<= v0x7fc2a95a3dd0_0 1.0)
         (>= v0x7fc2a95a3dd0_0 1.0)
         (<= v0x7fc2a95a3ed0_0 0.0)
         (>= v0x7fc2a95a3ed0_0 0.0)
         (<= v0x7fc2a95a2010_0 0.0)
         (>= v0x7fc2a95a2010_0 0.0))))
(assert (=> F0x7fc2a95a9910 F0x7fc2a95a9850))
(assert (let ((a!1 (=> v0x7fc2a95a4f10_0
               (or (and v0x7fc2a95a47d0_0
                        E0x7fc2a95a5090
                        (<= v0x7fc2a95a4fd0_0 v0x7fc2a95a4dd0_0)
                        (>= v0x7fc2a95a4fd0_0 v0x7fc2a95a4dd0_0))
                   (and v0x7fc2a95a4510_0
                        E0x7fc2a95a5250
                        v0x7fc2a95a4690_0
                        (<= v0x7fc2a95a4fd0_0 v0x7fc2a95a3c10_0)
                        (>= v0x7fc2a95a4fd0_0 v0x7fc2a95a3c10_0)))))
      (a!2 (=> v0x7fc2a95a4f10_0
               (or (and E0x7fc2a95a5090 (not E0x7fc2a95a5250))
                   (and E0x7fc2a95a5250 (not E0x7fc2a95a5090)))))
      (a!3 (=> v0x7fc2a95a5dd0_0
               (or (and v0x7fc2a95a5890_0
                        E0x7fc2a95a5f50
                        (<= v0x7fc2a95a5e90_0 v0x7fc2a95a5c90_0)
                        (>= v0x7fc2a95a5e90_0 v0x7fc2a95a5c90_0))
                   (and v0x7fc2a95a4f10_0
                        E0x7fc2a95a6110
                        v0x7fc2a95a5750_0
                        (<= v0x7fc2a95a5e90_0 v0x7fc2a95a3a90_0)
                        (>= v0x7fc2a95a5e90_0 v0x7fc2a95a3a90_0)))))
      (a!4 (=> v0x7fc2a95a5dd0_0
               (or (and E0x7fc2a95a5f50 (not E0x7fc2a95a6110))
                   (and E0x7fc2a95a6110 (not E0x7fc2a95a5f50)))))
      (a!5 (or (and v0x7fc2a95a6750_0
                    E0x7fc2a95a7950
                    (<= v0x7fc2a95a77d0_0 v0x7fc2a95a4fd0_0)
                    (>= v0x7fc2a95a77d0_0 v0x7fc2a95a4fd0_0)
                    (<= v0x7fc2a95a7890_0 v0x7fc2a95a6b10_0)
                    (>= v0x7fc2a95a7890_0 v0x7fc2a95a6b10_0))
               (and v0x7fc2a95a7490_0
                    E0x7fc2a95a7c10
                    (and (<= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0)
                         (>= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0))
                    (<= v0x7fc2a95a7890_0 v0x7fc2a95a3e10_0)
                    (>= v0x7fc2a95a7890_0 v0x7fc2a95a3e10_0))
               (and v0x7fc2a95a6c50_0
                    E0x7fc2a95a7ed0
                    (not v0x7fc2a95a7350_0)
                    (and (<= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0)
                         (>= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0))
                    (<= v0x7fc2a95a7890_0 0.0)
                    (>= v0x7fc2a95a7890_0 0.0))))
      (a!6 (=> v0x7fc2a95a7710_0
               (or (and E0x7fc2a95a7950
                        (not E0x7fc2a95a7c10)
                        (not E0x7fc2a95a7ed0))
                   (and E0x7fc2a95a7c10
                        (not E0x7fc2a95a7950)
                        (not E0x7fc2a95a7ed0))
                   (and E0x7fc2a95a7ed0
                        (not E0x7fc2a95a7950)
                        (not E0x7fc2a95a7c10)))))
      (a!7 (or (and v0x7fc2a95a8790_0
                    v0x7fc2a95a8a50_0
                    (and (<= v0x7fc2a95a3cd0_0 v0x7fc2a95a5e90_0)
                         (>= v0x7fc2a95a3cd0_0 v0x7fc2a95a5e90_0))
                    (and (<= v0x7fc2a95a3dd0_0 v0x7fc2a95a77d0_0)
                         (>= v0x7fc2a95a3dd0_0 v0x7fc2a95a77d0_0))
                    (<= v0x7fc2a95a3ed0_0 1.0)
                    (>= v0x7fc2a95a3ed0_0 1.0)
                    (and (<= v0x7fc2a95a2010_0 v0x7fc2a95a7890_0)
                         (>= v0x7fc2a95a2010_0 v0x7fc2a95a7890_0)))
               (and v0x7fc2a95a7710_0
                    v0x7fc2a95a8650_0
                    (and (<= v0x7fc2a95a3cd0_0 v0x7fc2a95a5e90_0)
                         (>= v0x7fc2a95a3cd0_0 v0x7fc2a95a5e90_0))
                    (and (<= v0x7fc2a95a3dd0_0 v0x7fc2a95a77d0_0)
                         (>= v0x7fc2a95a3dd0_0 v0x7fc2a95a77d0_0))
                    (<= v0x7fc2a95a3ed0_0 0.0)
                    (>= v0x7fc2a95a3ed0_0 0.0)
                    (and (<= v0x7fc2a95a2010_0 v0x7fc2a95a7890_0)
                         (>= v0x7fc2a95a2010_0 v0x7fc2a95a7890_0))))))
(let ((a!8 (and (=> v0x7fc2a95a47d0_0
                    (and v0x7fc2a95a4510_0
                         E0x7fc2a95a4890
                         (not v0x7fc2a95a4690_0)))
                (=> v0x7fc2a95a47d0_0 E0x7fc2a95a4890)
                a!1
                a!2
                (=> v0x7fc2a95a5890_0
                    (and v0x7fc2a95a4f10_0
                         E0x7fc2a95a5950
                         (not v0x7fc2a95a5750_0)))
                (=> v0x7fc2a95a5890_0 E0x7fc2a95a5950)
                a!3
                a!4
                (=> v0x7fc2a95a6750_0
                    (and v0x7fc2a95a5dd0_0 E0x7fc2a95a6810 v0x7fc2a95a6610_0))
                (=> v0x7fc2a95a6750_0 E0x7fc2a95a6810)
                (=> v0x7fc2a95a6c50_0
                    (and v0x7fc2a95a5dd0_0
                         E0x7fc2a95a6d10
                         (not v0x7fc2a95a6610_0)))
                (=> v0x7fc2a95a6c50_0 E0x7fc2a95a6d10)
                (=> v0x7fc2a95a7490_0
                    (and v0x7fc2a95a6c50_0 E0x7fc2a95a7550 v0x7fc2a95a7350_0))
                (=> v0x7fc2a95a7490_0 E0x7fc2a95a7550)
                (=> v0x7fc2a95a7710_0 a!5)
                a!6
                (=> v0x7fc2a95a8790_0
                    (and v0x7fc2a95a7710_0
                         E0x7fc2a95a8850
                         (not v0x7fc2a95a8650_0)))
                (=> v0x7fc2a95a8790_0 E0x7fc2a95a8850)
                a!7
                (= v0x7fc2a95a4690_0 (= v0x7fc2a95a45d0_0 0.0))
                (= v0x7fc2a95a4ad0_0 (< v0x7fc2a95a3c10_0 2.0))
                (= v0x7fc2a95a4c90_0 (ite v0x7fc2a95a4ad0_0 1.0 0.0))
                (= v0x7fc2a95a4dd0_0 (+ v0x7fc2a95a4c90_0 v0x7fc2a95a3c10_0))
                (= v0x7fc2a95a5750_0 (= v0x7fc2a95a5690_0 0.0))
                (= v0x7fc2a95a5b50_0 (= v0x7fc2a95a3a90_0 0.0))
                (= v0x7fc2a95a5c90_0 (ite v0x7fc2a95a5b50_0 1.0 0.0))
                (= v0x7fc2a95a6610_0 (= v0x7fc2a95a3e10_0 0.0))
                (= v0x7fc2a95a69d0_0 (> v0x7fc2a95a4fd0_0 1.0))
                (= v0x7fc2a95a6b10_0
                   (ite v0x7fc2a95a69d0_0 1.0 v0x7fc2a95a3e10_0))
                (= v0x7fc2a95a6f10_0 (> v0x7fc2a95a4fd0_0 0.0))
                (= v0x7fc2a95a7050_0 (+ v0x7fc2a95a4fd0_0 (- 1.0)))
                (= v0x7fc2a95a7210_0
                   (ite v0x7fc2a95a6f10_0 v0x7fc2a95a7050_0 v0x7fc2a95a4fd0_0))
                (= v0x7fc2a95a7350_0 (= v0x7fc2a95a5e90_0 0.0))
                (= v0x7fc2a95a8410_0 (= v0x7fc2a95a5e90_0 0.0))
                (= v0x7fc2a95a8510_0 (= v0x7fc2a95a7890_0 0.0))
                (= v0x7fc2a95a8650_0 (or v0x7fc2a95a8510_0 v0x7fc2a95a8410_0))
                (= v0x7fc2a95a8a50_0 (= v0x7fc2a95a3d10_0 0.0)))))
  (=> F0x7fc2a95a9950 a!8))))
(assert (=> F0x7fc2a95a9950 F0x7fc2a95a9a10))
(assert (let ((a!1 (=> v0x7fc2a95a4f10_0
               (or (and v0x7fc2a95a47d0_0
                        E0x7fc2a95a5090
                        (<= v0x7fc2a95a4fd0_0 v0x7fc2a95a4dd0_0)
                        (>= v0x7fc2a95a4fd0_0 v0x7fc2a95a4dd0_0))
                   (and v0x7fc2a95a4510_0
                        E0x7fc2a95a5250
                        v0x7fc2a95a4690_0
                        (<= v0x7fc2a95a4fd0_0 v0x7fc2a95a3c10_0)
                        (>= v0x7fc2a95a4fd0_0 v0x7fc2a95a3c10_0)))))
      (a!2 (=> v0x7fc2a95a4f10_0
               (or (and E0x7fc2a95a5090 (not E0x7fc2a95a5250))
                   (and E0x7fc2a95a5250 (not E0x7fc2a95a5090)))))
      (a!3 (=> v0x7fc2a95a5dd0_0
               (or (and v0x7fc2a95a5890_0
                        E0x7fc2a95a5f50
                        (<= v0x7fc2a95a5e90_0 v0x7fc2a95a5c90_0)
                        (>= v0x7fc2a95a5e90_0 v0x7fc2a95a5c90_0))
                   (and v0x7fc2a95a4f10_0
                        E0x7fc2a95a6110
                        v0x7fc2a95a5750_0
                        (<= v0x7fc2a95a5e90_0 v0x7fc2a95a3a90_0)
                        (>= v0x7fc2a95a5e90_0 v0x7fc2a95a3a90_0)))))
      (a!4 (=> v0x7fc2a95a5dd0_0
               (or (and E0x7fc2a95a5f50 (not E0x7fc2a95a6110))
                   (and E0x7fc2a95a6110 (not E0x7fc2a95a5f50)))))
      (a!5 (or (and v0x7fc2a95a6750_0
                    E0x7fc2a95a7950
                    (<= v0x7fc2a95a77d0_0 v0x7fc2a95a4fd0_0)
                    (>= v0x7fc2a95a77d0_0 v0x7fc2a95a4fd0_0)
                    (<= v0x7fc2a95a7890_0 v0x7fc2a95a6b10_0)
                    (>= v0x7fc2a95a7890_0 v0x7fc2a95a6b10_0))
               (and v0x7fc2a95a7490_0
                    E0x7fc2a95a7c10
                    (and (<= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0)
                         (>= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0))
                    (<= v0x7fc2a95a7890_0 v0x7fc2a95a3e10_0)
                    (>= v0x7fc2a95a7890_0 v0x7fc2a95a3e10_0))
               (and v0x7fc2a95a6c50_0
                    E0x7fc2a95a7ed0
                    (not v0x7fc2a95a7350_0)
                    (and (<= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0)
                         (>= v0x7fc2a95a77d0_0 v0x7fc2a95a7210_0))
                    (<= v0x7fc2a95a7890_0 0.0)
                    (>= v0x7fc2a95a7890_0 0.0))))
      (a!6 (=> v0x7fc2a95a7710_0
               (or (and E0x7fc2a95a7950
                        (not E0x7fc2a95a7c10)
                        (not E0x7fc2a95a7ed0))
                   (and E0x7fc2a95a7c10
                        (not E0x7fc2a95a7950)
                        (not E0x7fc2a95a7ed0))
                   (and E0x7fc2a95a7ed0
                        (not E0x7fc2a95a7950)
                        (not E0x7fc2a95a7c10))))))
(let ((a!7 (and (=> v0x7fc2a95a47d0_0
                    (and v0x7fc2a95a4510_0
                         E0x7fc2a95a4890
                         (not v0x7fc2a95a4690_0)))
                (=> v0x7fc2a95a47d0_0 E0x7fc2a95a4890)
                a!1
                a!2
                (=> v0x7fc2a95a5890_0
                    (and v0x7fc2a95a4f10_0
                         E0x7fc2a95a5950
                         (not v0x7fc2a95a5750_0)))
                (=> v0x7fc2a95a5890_0 E0x7fc2a95a5950)
                a!3
                a!4
                (=> v0x7fc2a95a6750_0
                    (and v0x7fc2a95a5dd0_0 E0x7fc2a95a6810 v0x7fc2a95a6610_0))
                (=> v0x7fc2a95a6750_0 E0x7fc2a95a6810)
                (=> v0x7fc2a95a6c50_0
                    (and v0x7fc2a95a5dd0_0
                         E0x7fc2a95a6d10
                         (not v0x7fc2a95a6610_0)))
                (=> v0x7fc2a95a6c50_0 E0x7fc2a95a6d10)
                (=> v0x7fc2a95a7490_0
                    (and v0x7fc2a95a6c50_0 E0x7fc2a95a7550 v0x7fc2a95a7350_0))
                (=> v0x7fc2a95a7490_0 E0x7fc2a95a7550)
                (=> v0x7fc2a95a7710_0 a!5)
                a!6
                (=> v0x7fc2a95a8790_0
                    (and v0x7fc2a95a7710_0
                         E0x7fc2a95a8850
                         (not v0x7fc2a95a8650_0)))
                (=> v0x7fc2a95a8790_0 E0x7fc2a95a8850)
                v0x7fc2a95a8790_0
                (not v0x7fc2a95a8a50_0)
                (= v0x7fc2a95a4690_0 (= v0x7fc2a95a45d0_0 0.0))
                (= v0x7fc2a95a4ad0_0 (< v0x7fc2a95a3c10_0 2.0))
                (= v0x7fc2a95a4c90_0 (ite v0x7fc2a95a4ad0_0 1.0 0.0))
                (= v0x7fc2a95a4dd0_0 (+ v0x7fc2a95a4c90_0 v0x7fc2a95a3c10_0))
                (= v0x7fc2a95a5750_0 (= v0x7fc2a95a5690_0 0.0))
                (= v0x7fc2a95a5b50_0 (= v0x7fc2a95a3a90_0 0.0))
                (= v0x7fc2a95a5c90_0 (ite v0x7fc2a95a5b50_0 1.0 0.0))
                (= v0x7fc2a95a6610_0 (= v0x7fc2a95a3e10_0 0.0))
                (= v0x7fc2a95a69d0_0 (> v0x7fc2a95a4fd0_0 1.0))
                (= v0x7fc2a95a6b10_0
                   (ite v0x7fc2a95a69d0_0 1.0 v0x7fc2a95a3e10_0))
                (= v0x7fc2a95a6f10_0 (> v0x7fc2a95a4fd0_0 0.0))
                (= v0x7fc2a95a7050_0 (+ v0x7fc2a95a4fd0_0 (- 1.0)))
                (= v0x7fc2a95a7210_0
                   (ite v0x7fc2a95a6f10_0 v0x7fc2a95a7050_0 v0x7fc2a95a4fd0_0))
                (= v0x7fc2a95a7350_0 (= v0x7fc2a95a5e90_0 0.0))
                (= v0x7fc2a95a8410_0 (= v0x7fc2a95a5e90_0 0.0))
                (= v0x7fc2a95a8510_0 (= v0x7fc2a95a7890_0 0.0))
                (= v0x7fc2a95a8650_0 (or v0x7fc2a95a8510_0 v0x7fc2a95a8410_0))
                (= v0x7fc2a95a8a50_0 (= v0x7fc2a95a3d10_0 0.0)))))
  (=> F0x7fc2a95a9ad0 a!7))))
(assert (=> F0x7fc2a95a9ad0 F0x7fc2a95a9a10))
(assert (=> F0x7fc2a95a9c10 (or F0x7fc2a95a9910 F0x7fc2a95a9950)))
(assert (=> F0x7fc2a95a9bd0 F0x7fc2a95a9ad0))
(assert (=> pre!entry!0 (=> F0x7fc2a95a9850 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fc2a95a9a10 (>= v0x7fc2a95a3d10_0 0.0))))
(assert (let ((a!1 (=> F0x7fc2a95a9a10
               (or (<= v0x7fc2a95a3d10_0 0.0) (not (<= v0x7fc2a95a3e10_0 0.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fc2a95a9c10 (>= v0x7fc2a95a3ed0_0 0.0))))
(assert (let ((a!1 (=> F0x7fc2a95a9c10
               (or (<= v0x7fc2a95a3ed0_0 0.0) (not (<= v0x7fc2a95a2010_0 0.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7fc2a95a9bd0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7fc2a95a9850)
; (error: F0x7fc2a95a9bd0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i23.i.i!0)
