(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f897ee3d610 () Bool)
(declare-fun F0x7f897ee3d290 () Bool)
(declare-fun F0x7f897ee3d350 () Bool)
(declare-fun v0x7f897ee3c5d0_0 () Bool)
(declare-fun v0x7f897ee3c490_0 () Bool)
(declare-fun v0x7f897ee39610_0 () Real)
(declare-fun post!bb1.i.i26.i.i!0 () Bool)
(declare-fun v0x7f897ee37890_0 () Bool)
(declare-fun v0x7f897ee37390_0 () Real)
(declare-fun v0x7f897ee38510_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun E0x7f897ee3bd50 () Bool)
(declare-fun lemma!bb1.i.i26.i.i!0 () Bool)
(declare-fun v0x7f897ee37a50_0 () Real)
(declare-fun E0x7f897ee3b8d0 () Bool)
(declare-fun v0x7f897ee36c10_0 () Real)
(declare-fun v0x7f897ee3a850_0 () Real)
(declare-fun v0x7f897ee3a710_0 () Bool)
(declare-fun v0x7f897ee3b090_0 () Real)
(declare-fun v0x7f897ee3c710_0 () Bool)
(declare-fun v0x7f897ee3ac10_0 () Bool)
(declare-fun E0x7f897ee3ae10 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f897ee3ad50_0 () Bool)
(declare-fun E0x7f897ee3aa50 () Bool)
(declare-fun v0x7f897ee3a990_0 () Bool)
(declare-fun E0x7f897ee3a550 () Bool)
(declare-fun v0x7f897ee3a490_0 () Bool)
(declare-fun v0x7f897ee3a090_0 () Bool)
(declare-fun v0x7f897ee397d0_0 () Real)
(declare-fun v0x7f897ee399d0_0 () Real)
(declare-fun v0x7f897ee39910_0 () Bool)
(declare-fun v0x7f897ee390d0_0 () Bool)
(declare-fun v0x7f897ee3b150_0 () Real)
(declare-fun E0x7f897ee39c50 () Bool)
(declare-fun v0x7f897ee36a90_0 () Real)
(declare-fun F0x7f897ee3d650 () Bool)
(declare-fun E0x7f897ee38d50 () Bool)
(declare-fun v0x7f897ee385d0_0 () Bool)
(declare-fun v0x7f897ee38a50_0 () Bool)
(declare-fun E0x7f897ee387d0 () Bool)
(declare-fun v0x7f897ee38710_0 () Bool)
(declare-fun E0x7f897ee39a90 () Bool)
(declare-fun v0x7f897ee3b210_0 () Real)
(declare-fun E0x7f897ee38c50 () Bool)
(declare-fun E0x7f897ee37e50 () Bool)
(declare-fun v0x7f897ee36d10_0 () Real)
(declare-fun v0x7f897ee37cd0_0 () Bool)
(declare-fun v0x7f897ee38990_0 () Real)
(declare-fun E0x7f897ee37650 () Bool)
(declare-fun v0x7f897ee37590_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7f897ee3bb90 () Bool)
(declare-fun F0x7f897ee3d410 () Bool)
(declare-fun E0x7f897ee3a290 () Bool)
(declare-fun v0x7f897ee37d90_0 () Real)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun v0x7f897ee38b90_0 () Bool)
(declare-fun F0x7f897ee3d510 () Bool)
(declare-fun v0x7f897ee3a1d0_0 () Bool)
(declare-fun v0x7f897ee394d0_0 () Bool)
(declare-fun v0x7f897ee372d0_0 () Bool)
(declare-fun E0x7f897ee3b690 () Bool)
(declare-fun v0x7f897ee37450_0 () Bool)
(declare-fun v0x7f897ee36dd0_0 () Real)
(declare-fun E0x7f897ee392d0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7f897ee38010 () Bool)
(declare-fun v0x7f897ee39210_0 () Bool)
(declare-fun v0x7f897ee3afd0_0 () Bool)
(declare-fun E0x7f897ee3b2d0 () Bool)
(declare-fun v0x7f897ee36cd0_0 () Real)
(declare-fun v0x7f897ee35010_0 () Real)
(declare-fun v0x7f897ee37b90_0 () Real)
(declare-fun v0x7f897ee35110_0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun F0x7f897ee3d590 () Bool)

(assert (=> F0x7f897ee3d590
    (and v0x7f897ee35110_0
         (<= v0x7f897ee36cd0_0 1.0)
         (>= v0x7f897ee36cd0_0 1.0)
         (<= v0x7f897ee36dd0_0 0.0)
         (>= v0x7f897ee36dd0_0 0.0)
         (<= v0x7f897ee35010_0 1.0)
         (>= v0x7f897ee35010_0 1.0))))
(assert (=> F0x7f897ee3d590 F0x7f897ee3d510))
(assert (let ((a!1 (=> v0x7f897ee37cd0_0
               (or (and v0x7f897ee37590_0
                        E0x7f897ee37e50
                        (<= v0x7f897ee37d90_0 v0x7f897ee37b90_0)
                        (>= v0x7f897ee37d90_0 v0x7f897ee37b90_0))
                   (and v0x7f897ee372d0_0
                        E0x7f897ee38010
                        v0x7f897ee37450_0
                        (<= v0x7f897ee37d90_0 v0x7f897ee36d10_0)
                        (>= v0x7f897ee37d90_0 v0x7f897ee36d10_0)))))
      (a!2 (=> v0x7f897ee37cd0_0
               (or (and E0x7f897ee37e50 (not E0x7f897ee38010))
                   (and E0x7f897ee38010 (not E0x7f897ee37e50)))))
      (a!3 (=> v0x7f897ee38b90_0
               (or (and v0x7f897ee38710_0 E0x7f897ee38c50 v0x7f897ee38a50_0)
                   (and v0x7f897ee37cd0_0
                        E0x7f897ee38d50
                        (not v0x7f897ee385d0_0)))))
      (a!4 (=> v0x7f897ee38b90_0
               (or (and E0x7f897ee38c50 (not E0x7f897ee38d50))
                   (and E0x7f897ee38d50 (not E0x7f897ee38c50)))))
      (a!5 (=> v0x7f897ee39910_0
               (or (and v0x7f897ee39210_0
                        E0x7f897ee39a90
                        (<= v0x7f897ee399d0_0 v0x7f897ee397d0_0)
                        (>= v0x7f897ee399d0_0 v0x7f897ee397d0_0))
                   (and v0x7f897ee38b90_0
                        E0x7f897ee39c50
                        v0x7f897ee390d0_0
                        (<= v0x7f897ee399d0_0 v0x7f897ee37d90_0)
                        (>= v0x7f897ee399d0_0 v0x7f897ee37d90_0)))))
      (a!6 (=> v0x7f897ee39910_0
               (or (and E0x7f897ee39a90 (not E0x7f897ee39c50))
                   (and E0x7f897ee39c50 (not E0x7f897ee39a90)))))
      (a!7 (or (and v0x7f897ee3a490_0
                    E0x7f897ee3b2d0
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (<= v0x7f897ee3b210_0 v0x7f897ee3a850_0)
                    (>= v0x7f897ee3b210_0 v0x7f897ee3a850_0))
               (and v0x7f897ee3ad50_0
                    E0x7f897ee3b690
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (and (<= v0x7f897ee3b210_0 v0x7f897ee36c10_0)
                         (>= v0x7f897ee3b210_0 v0x7f897ee36c10_0)))
               (and v0x7f897ee3a990_0
                    E0x7f897ee3b8d0
                    (not v0x7f897ee3ac10_0)
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (and (<= v0x7f897ee3b210_0 0.0) (>= v0x7f897ee3b210_0 0.0)))
               (and v0x7f897ee39910_0
                    E0x7f897ee3bb90
                    v0x7f897ee3a090_0
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (and (<= v0x7f897ee3b210_0 v0x7f897ee36c10_0)
                         (>= v0x7f897ee3b210_0 v0x7f897ee36c10_0)))
               (and v0x7f897ee38710_0
                    E0x7f897ee3bd50
                    (not v0x7f897ee38a50_0)
                    (<= v0x7f897ee3b090_0 0.0)
                    (>= v0x7f897ee3b090_0 0.0)
                    (<= v0x7f897ee3b150_0 v0x7f897ee37d90_0)
                    (>= v0x7f897ee3b150_0 v0x7f897ee37d90_0)
                    (and (<= v0x7f897ee3b210_0 0.0) (>= v0x7f897ee3b210_0 0.0)))))
      (a!8 (=> v0x7f897ee3afd0_0
               (or (and E0x7f897ee3b2d0
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bb90)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3b690
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bb90)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3b8d0
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3bb90)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3bb90
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3bd50
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bb90))))))
(let ((a!9 (and (=> v0x7f897ee37590_0
                    (and v0x7f897ee372d0_0
                         E0x7f897ee37650
                         (not v0x7f897ee37450_0)))
                (=> v0x7f897ee37590_0 E0x7f897ee37650)
                a!1
                a!2
                (=> v0x7f897ee38710_0
                    (and v0x7f897ee37cd0_0 E0x7f897ee387d0 v0x7f897ee385d0_0))
                (=> v0x7f897ee38710_0 E0x7f897ee387d0)
                a!3
                a!4
                (=> v0x7f897ee39210_0
                    (and v0x7f897ee38b90_0
                         E0x7f897ee392d0
                         (not v0x7f897ee390d0_0)))
                (=> v0x7f897ee39210_0 E0x7f897ee392d0)
                a!5
                a!6
                (=> v0x7f897ee3a1d0_0
                    (and v0x7f897ee39910_0
                         E0x7f897ee3a290
                         (not v0x7f897ee3a090_0)))
                (=> v0x7f897ee3a1d0_0 E0x7f897ee3a290)
                (=> v0x7f897ee3a490_0
                    (and v0x7f897ee3a1d0_0 E0x7f897ee3a550 v0x7f897ee390d0_0))
                (=> v0x7f897ee3a490_0 E0x7f897ee3a550)
                (=> v0x7f897ee3a990_0
                    (and v0x7f897ee3a1d0_0
                         E0x7f897ee3aa50
                         (not v0x7f897ee390d0_0)))
                (=> v0x7f897ee3a990_0 E0x7f897ee3aa50)
                (=> v0x7f897ee3ad50_0
                    (and v0x7f897ee3a990_0 E0x7f897ee3ae10 v0x7f897ee3ac10_0))
                (=> v0x7f897ee3ad50_0 E0x7f897ee3ae10)
                (=> v0x7f897ee3afd0_0 a!7)
                a!8
                v0x7f897ee3afd0_0
                v0x7f897ee3c710_0
                (<= v0x7f897ee36cd0_0 v0x7f897ee3b090_0)
                (>= v0x7f897ee36cd0_0 v0x7f897ee3b090_0)
                (<= v0x7f897ee36dd0_0 v0x7f897ee3b210_0)
                (>= v0x7f897ee36dd0_0 v0x7f897ee3b210_0)
                (<= v0x7f897ee35010_0 v0x7f897ee3b150_0)
                (>= v0x7f897ee35010_0 v0x7f897ee3b150_0)
                (= v0x7f897ee37450_0 (= v0x7f897ee37390_0 0.0))
                (= v0x7f897ee37890_0 (< v0x7f897ee36d10_0 2.0))
                (= v0x7f897ee37a50_0 (ite v0x7f897ee37890_0 1.0 0.0))
                (= v0x7f897ee37b90_0 (+ v0x7f897ee37a50_0 v0x7f897ee36d10_0))
                (= v0x7f897ee385d0_0 (= v0x7f897ee38510_0 0.0))
                (= v0x7f897ee38a50_0 (= v0x7f897ee38990_0 0.0))
                (= v0x7f897ee390d0_0 (= v0x7f897ee36c10_0 0.0))
                (= v0x7f897ee394d0_0 (> v0x7f897ee37d90_0 0.0))
                (= v0x7f897ee39610_0 (+ v0x7f897ee37d90_0 (- 1.0)))
                (= v0x7f897ee397d0_0
                   (ite v0x7f897ee394d0_0 v0x7f897ee39610_0 v0x7f897ee37d90_0))
                (= v0x7f897ee3a090_0 (= v0x7f897ee36a90_0 0.0))
                (= v0x7f897ee3a710_0 (> v0x7f897ee399d0_0 1.0))
                (= v0x7f897ee3a850_0
                   (ite v0x7f897ee3a710_0 1.0 v0x7f897ee36c10_0))
                (= v0x7f897ee3ac10_0 (= v0x7f897ee399d0_0 0.0))
                (= v0x7f897ee3c490_0 (not (= v0x7f897ee3b150_0 0.0)))
                (= v0x7f897ee3c5d0_0 (= v0x7f897ee3b210_0 0.0))
                (= v0x7f897ee3c710_0 (or v0x7f897ee3c5d0_0 v0x7f897ee3c490_0)))))
  (=> F0x7f897ee3d410 a!9))))
(assert (=> F0x7f897ee3d410 F0x7f897ee3d350))
(assert (let ((a!1 (=> v0x7f897ee37cd0_0
               (or (and v0x7f897ee37590_0
                        E0x7f897ee37e50
                        (<= v0x7f897ee37d90_0 v0x7f897ee37b90_0)
                        (>= v0x7f897ee37d90_0 v0x7f897ee37b90_0))
                   (and v0x7f897ee372d0_0
                        E0x7f897ee38010
                        v0x7f897ee37450_0
                        (<= v0x7f897ee37d90_0 v0x7f897ee36d10_0)
                        (>= v0x7f897ee37d90_0 v0x7f897ee36d10_0)))))
      (a!2 (=> v0x7f897ee37cd0_0
               (or (and E0x7f897ee37e50 (not E0x7f897ee38010))
                   (and E0x7f897ee38010 (not E0x7f897ee37e50)))))
      (a!3 (=> v0x7f897ee38b90_0
               (or (and v0x7f897ee38710_0 E0x7f897ee38c50 v0x7f897ee38a50_0)
                   (and v0x7f897ee37cd0_0
                        E0x7f897ee38d50
                        (not v0x7f897ee385d0_0)))))
      (a!4 (=> v0x7f897ee38b90_0
               (or (and E0x7f897ee38c50 (not E0x7f897ee38d50))
                   (and E0x7f897ee38d50 (not E0x7f897ee38c50)))))
      (a!5 (=> v0x7f897ee39910_0
               (or (and v0x7f897ee39210_0
                        E0x7f897ee39a90
                        (<= v0x7f897ee399d0_0 v0x7f897ee397d0_0)
                        (>= v0x7f897ee399d0_0 v0x7f897ee397d0_0))
                   (and v0x7f897ee38b90_0
                        E0x7f897ee39c50
                        v0x7f897ee390d0_0
                        (<= v0x7f897ee399d0_0 v0x7f897ee37d90_0)
                        (>= v0x7f897ee399d0_0 v0x7f897ee37d90_0)))))
      (a!6 (=> v0x7f897ee39910_0
               (or (and E0x7f897ee39a90 (not E0x7f897ee39c50))
                   (and E0x7f897ee39c50 (not E0x7f897ee39a90)))))
      (a!7 (or (and v0x7f897ee3a490_0
                    E0x7f897ee3b2d0
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (<= v0x7f897ee3b210_0 v0x7f897ee3a850_0)
                    (>= v0x7f897ee3b210_0 v0x7f897ee3a850_0))
               (and v0x7f897ee3ad50_0
                    E0x7f897ee3b690
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (and (<= v0x7f897ee3b210_0 v0x7f897ee36c10_0)
                         (>= v0x7f897ee3b210_0 v0x7f897ee36c10_0)))
               (and v0x7f897ee3a990_0
                    E0x7f897ee3b8d0
                    (not v0x7f897ee3ac10_0)
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (and (<= v0x7f897ee3b210_0 0.0) (>= v0x7f897ee3b210_0 0.0)))
               (and v0x7f897ee39910_0
                    E0x7f897ee3bb90
                    v0x7f897ee3a090_0
                    (and (<= v0x7f897ee3b090_0 v0x7f897ee36a90_0)
                         (>= v0x7f897ee3b090_0 v0x7f897ee36a90_0))
                    (and (<= v0x7f897ee3b150_0 v0x7f897ee399d0_0)
                         (>= v0x7f897ee3b150_0 v0x7f897ee399d0_0))
                    (and (<= v0x7f897ee3b210_0 v0x7f897ee36c10_0)
                         (>= v0x7f897ee3b210_0 v0x7f897ee36c10_0)))
               (and v0x7f897ee38710_0
                    E0x7f897ee3bd50
                    (not v0x7f897ee38a50_0)
                    (<= v0x7f897ee3b090_0 0.0)
                    (>= v0x7f897ee3b090_0 0.0)
                    (<= v0x7f897ee3b150_0 v0x7f897ee37d90_0)
                    (>= v0x7f897ee3b150_0 v0x7f897ee37d90_0)
                    (and (<= v0x7f897ee3b210_0 0.0) (>= v0x7f897ee3b210_0 0.0)))))
      (a!8 (=> v0x7f897ee3afd0_0
               (or (and E0x7f897ee3b2d0
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bb90)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3b690
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bb90)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3b8d0
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3bb90)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3bb90
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bd50))
                   (and E0x7f897ee3bd50
                        (not E0x7f897ee3b2d0)
                        (not E0x7f897ee3b690)
                        (not E0x7f897ee3b8d0)
                        (not E0x7f897ee3bb90))))))
(let ((a!9 (and (=> v0x7f897ee37590_0
                    (and v0x7f897ee372d0_0
                         E0x7f897ee37650
                         (not v0x7f897ee37450_0)))
                (=> v0x7f897ee37590_0 E0x7f897ee37650)
                a!1
                a!2
                (=> v0x7f897ee38710_0
                    (and v0x7f897ee37cd0_0 E0x7f897ee387d0 v0x7f897ee385d0_0))
                (=> v0x7f897ee38710_0 E0x7f897ee387d0)
                a!3
                a!4
                (=> v0x7f897ee39210_0
                    (and v0x7f897ee38b90_0
                         E0x7f897ee392d0
                         (not v0x7f897ee390d0_0)))
                (=> v0x7f897ee39210_0 E0x7f897ee392d0)
                a!5
                a!6
                (=> v0x7f897ee3a1d0_0
                    (and v0x7f897ee39910_0
                         E0x7f897ee3a290
                         (not v0x7f897ee3a090_0)))
                (=> v0x7f897ee3a1d0_0 E0x7f897ee3a290)
                (=> v0x7f897ee3a490_0
                    (and v0x7f897ee3a1d0_0 E0x7f897ee3a550 v0x7f897ee390d0_0))
                (=> v0x7f897ee3a490_0 E0x7f897ee3a550)
                (=> v0x7f897ee3a990_0
                    (and v0x7f897ee3a1d0_0
                         E0x7f897ee3aa50
                         (not v0x7f897ee390d0_0)))
                (=> v0x7f897ee3a990_0 E0x7f897ee3aa50)
                (=> v0x7f897ee3ad50_0
                    (and v0x7f897ee3a990_0 E0x7f897ee3ae10 v0x7f897ee3ac10_0))
                (=> v0x7f897ee3ad50_0 E0x7f897ee3ae10)
                (=> v0x7f897ee3afd0_0 a!7)
                a!8
                v0x7f897ee3afd0_0
                (not v0x7f897ee3c710_0)
                (= v0x7f897ee37450_0 (= v0x7f897ee37390_0 0.0))
                (= v0x7f897ee37890_0 (< v0x7f897ee36d10_0 2.0))
                (= v0x7f897ee37a50_0 (ite v0x7f897ee37890_0 1.0 0.0))
                (= v0x7f897ee37b90_0 (+ v0x7f897ee37a50_0 v0x7f897ee36d10_0))
                (= v0x7f897ee385d0_0 (= v0x7f897ee38510_0 0.0))
                (= v0x7f897ee38a50_0 (= v0x7f897ee38990_0 0.0))
                (= v0x7f897ee390d0_0 (= v0x7f897ee36c10_0 0.0))
                (= v0x7f897ee394d0_0 (> v0x7f897ee37d90_0 0.0))
                (= v0x7f897ee39610_0 (+ v0x7f897ee37d90_0 (- 1.0)))
                (= v0x7f897ee397d0_0
                   (ite v0x7f897ee394d0_0 v0x7f897ee39610_0 v0x7f897ee37d90_0))
                (= v0x7f897ee3a090_0 (= v0x7f897ee36a90_0 0.0))
                (= v0x7f897ee3a710_0 (> v0x7f897ee399d0_0 1.0))
                (= v0x7f897ee3a850_0
                   (ite v0x7f897ee3a710_0 1.0 v0x7f897ee36c10_0))
                (= v0x7f897ee3ac10_0 (= v0x7f897ee399d0_0 0.0))
                (= v0x7f897ee3c490_0 (not (= v0x7f897ee3b150_0 0.0)))
                (= v0x7f897ee3c5d0_0 (= v0x7f897ee3b210_0 0.0))
                (= v0x7f897ee3c710_0 (or v0x7f897ee3c5d0_0 v0x7f897ee3c490_0)))))
  (=> F0x7f897ee3d290 a!9))))
(assert (=> F0x7f897ee3d290 F0x7f897ee3d350))
(assert (=> F0x7f897ee3d650 (or F0x7f897ee3d590 F0x7f897ee3d410)))
(assert (=> F0x7f897ee3d610 F0x7f897ee3d290))
(assert (=> pre!entry!0 (=> F0x7f897ee3d510 true)))
(assert (let ((a!1 (=> F0x7f897ee3d350
               (or (not (<= 1.0 v0x7f897ee36c10_0))
                   (not (<= 0.0 v0x7f897ee36a90_0))
                   (not (<= v0x7f897ee36a90_0 0.0))))))
  (=> pre!bb1.i.i!0 a!1)))
(assert (=> pre!bb1.i.i!1 (=> F0x7f897ee3d350 (>= v0x7f897ee36c10_0 0.0))))
(assert (let ((a!1 (=> F0x7f897ee3d350
               (or (not (<= v0x7f897ee36d10_0 1.0)) (<= v0x7f897ee36c10_0 0.0)))))
  (=> pre!bb1.i.i!2 a!1)))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f897ee3d350
        (or (>= v0x7f897ee36c10_0 1.0) (<= v0x7f897ee36c10_0 0.0)))))
(assert (let ((a!1 (=> F0x7f897ee3d650
               (or (not (<= 1.0 v0x7f897ee36dd0_0))
                   (not (<= 0.0 v0x7f897ee36cd0_0))
                   (not (<= v0x7f897ee36cd0_0 0.0))))))
  (= lemma!bb1.i.i!0 a!1)))
(assert (= lemma!bb1.i.i!1 (=> F0x7f897ee3d650 (>= v0x7f897ee36dd0_0 0.0))))
(assert (let ((a!1 (=> F0x7f897ee3d650
               (or (not (<= v0x7f897ee35010_0 1.0)) (<= v0x7f897ee36dd0_0 0.0)))))
  (= lemma!bb1.i.i!2 a!1)))
(assert (= lemma!bb1.i.i!3
   (=> F0x7f897ee3d650
       (or (>= v0x7f897ee36dd0_0 1.0) (<= v0x7f897ee36dd0_0 0.0)))))
(assert (= lemma!bb1.i.i26.i.i!0 (=> F0x7f897ee3d610 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i26.i.i!0) (not lemma!bb1.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7f897ee3d510)
; (error: F0x7f897ee3d610)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i26.i.i!0)
