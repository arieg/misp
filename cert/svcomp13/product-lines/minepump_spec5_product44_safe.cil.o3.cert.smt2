(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fd29d0c1c10 () Bool)
(declare-fun F0x7fd29d0c1b50 () Bool)
(declare-fun F0x7fd29d0c1a90 () Bool)
(declare-fun v0x7fd29d0c0ad0_0 () Bool)
(declare-fun v0x7fd29d0c0710_0 () Bool)
(declare-fun v0x7fd29d0bfcd0_0 () Bool)
(declare-fun v0x7fd29d0bf690_0 () Bool)
(declare-fun v0x7fd29d0bf550_0 () Bool)
(declare-fun v0x7fd29d0bf410_0 () Bool)
(declare-fun v0x7fd29d0be190_0 () Real)
(declare-fun v0x7fd29d0be050_0 () Bool)
(declare-fun v0x7fd29d0bc3d0_0 () Bool)
(declare-fun v0x7fd29d0bbf10_0 () Real)
(declare-fun v0x7fd29d0bb510_0 () Real)
(declare-fun v0x7fd29d0bae50_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7fd29d0c0c10_0 () Bool)
(declare-fun v0x7fd29d0bd250_0 () Real)
(declare-fun E0x7fd29d0c0290 () Bool)
(declare-fun v0x7fd29d0bfe10_0 () Real)
(declare-fun v0x7fd29d0c0010_0 () Real)
(declare-fun E0x7fd29d0c00d0 () Bool)
(declare-fun v0x7fd29d0bff50_0 () Bool)
(declare-fun v0x7fd29d0bfa50_0 () Bool)
(declare-fun v0x7fd29d0bf910_0 () Bool)
(declare-fun v0x7fd29d0be610_0 () Real)
(declare-fun v0x7fd29d0be550_0 () Real)
(declare-fun v0x7fd29d0bdc50_0 () Bool)
(declare-fun v0x7fd29d0bdd90_0 () Bool)
(declare-fun E0x7fd29d0bd090 () Bool)
(declare-fun E0x7fd29d0bedd0 () Bool)
(declare-fun v0x7fd29d0bcfd0_0 () Bool)
(declare-fun E0x7fd29d0bc990 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7fd29d0bd5d0 () Bool)
(declare-fun v0x7fd29d0bc710_0 () Real)
(declare-fun E0x7fd29d0bc7d0 () Bool)
(declare-fun v0x7fd29d0bc650_0 () Bool)
(declare-fun v0x7fd29d0ba490_0 () Real)
(declare-fun v0x7fd29d0bc510_0 () Real)
(declare-fun v0x7fd29d0bbfd0_0 () Bool)
(declare-fun v0x7fd29d0bcdd0_0 () Real)
(declare-fun F0x7fd29d0c1c50 () Bool)
(declare-fun E0x7fd29d0beb50 () Bool)
(declare-fun v0x7fd29d0bc110_0 () Bool)
(declare-fun v0x7fd29d0bd310_0 () Bool)
(declare-fun E0x7fd29d0bbad0 () Bool)
(declare-fun v0x7fd29d0bb650_0 () Real)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7fd29d0bb850_0 () Real)
(declare-fun v0x7fd29d0bd450_0 () Bool)
(declare-fun E0x7fd29d0bb910 () Bool)
(declare-fun v0x7fd29d0ba590_0 () Real)
(declare-fun E0x7fd29d0bc1d0 () Bool)
(declare-fun v0x7fd29d0bb790_0 () Bool)
(declare-fun E0x7fd29d0be790 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun v0x7fd29d0baf10_0 () Bool)
(declare-fun v0x7fd29d0be350_0 () Real)
(declare-fun v0x7fd29d0bd510_0 () Real)
(declare-fun v0x7fd29d0ba310_0 () Real)
(declare-fun v0x7fd29d0ba690_0 () Real)
(declare-fun v0x7fd29d0c0990_0 () Bool)
(declare-fun v0x7fd29d0bad90_0 () Bool)
(declare-fun E0x7fd29d0bb110 () Bool)
(declare-fun v0x7fd29d0be490_0 () Bool)
(declare-fun F0x7fd29d0c18d0 () Bool)
(declare-fun v0x7fd29d0b9010_0 () Real)
(declare-fun E0x7fd29d0bfb10 () Bool)
(declare-fun E0x7fd29d0bd7d0 () Bool)
(declare-fun E0x7fd29d0bde50 () Bool)
(declare-fun v0x7fd29d0ba750_0 () Real)
(declare-fun v0x7fd29d0ba550_0 () Real)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7fd29d0c0850_0 () Bool)
(declare-fun v0x7fd29d0bf7d0_0 () Bool)
(declare-fun v0x7fd29d0ba650_0 () Real)
(declare-fun v0x7fd29d0b9110_0 () Bool)
(declare-fun v0x7fd29d0bb350_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fd29d0bce90_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun F0x7fd29d0c1990 () Bool)
(declare-fun v0x7fd29d0bb050_0 () Bool)
(declare-fun v0x7fd29d0be6d0_0 () Real)
(declare-fun F0x7fd29d0c1a50 () Bool)

(assert (=> F0x7fd29d0c1a50
    (and v0x7fd29d0b9110_0
         (<= v0x7fd29d0ba550_0 1.0)
         (>= v0x7fd29d0ba550_0 1.0)
         (<= v0x7fd29d0ba650_0 0.0)
         (>= v0x7fd29d0ba650_0 0.0)
         (<= v0x7fd29d0ba750_0 0.0)
         (>= v0x7fd29d0ba750_0 0.0)
         (<= v0x7fd29d0b9010_0 1.0)
         (>= v0x7fd29d0b9010_0 1.0))))
(assert (=> F0x7fd29d0c1a50 F0x7fd29d0c1990))
(assert (let ((a!1 (=> v0x7fd29d0bb790_0
               (or (and v0x7fd29d0bb050_0
                        E0x7fd29d0bb910
                        (<= v0x7fd29d0bb850_0 v0x7fd29d0bb650_0)
                        (>= v0x7fd29d0bb850_0 v0x7fd29d0bb650_0))
                   (and v0x7fd29d0bad90_0
                        E0x7fd29d0bbad0
                        v0x7fd29d0baf10_0
                        (<= v0x7fd29d0bb850_0 v0x7fd29d0ba690_0)
                        (>= v0x7fd29d0bb850_0 v0x7fd29d0ba690_0)))))
      (a!2 (=> v0x7fd29d0bb790_0
               (or (and E0x7fd29d0bb910 (not E0x7fd29d0bbad0))
                   (and E0x7fd29d0bbad0 (not E0x7fd29d0bb910)))))
      (a!3 (=> v0x7fd29d0bc650_0
               (or (and v0x7fd29d0bc110_0
                        E0x7fd29d0bc7d0
                        (<= v0x7fd29d0bc710_0 v0x7fd29d0bc510_0)
                        (>= v0x7fd29d0bc710_0 v0x7fd29d0bc510_0))
                   (and v0x7fd29d0bb790_0
                        E0x7fd29d0bc990
                        v0x7fd29d0bbfd0_0
                        (<= v0x7fd29d0bc710_0 v0x7fd29d0ba590_0)
                        (>= v0x7fd29d0bc710_0 v0x7fd29d0ba590_0)))))
      (a!4 (=> v0x7fd29d0bc650_0
               (or (and E0x7fd29d0bc7d0 (not E0x7fd29d0bc990))
                   (and E0x7fd29d0bc990 (not E0x7fd29d0bc7d0)))))
      (a!5 (=> v0x7fd29d0bd450_0
               (or (and v0x7fd29d0bcfd0_0
                        E0x7fd29d0bd5d0
                        v0x7fd29d0bd310_0
                        (<= v0x7fd29d0bd510_0 v0x7fd29d0ba310_0)
                        (>= v0x7fd29d0bd510_0 v0x7fd29d0ba310_0))
                   (and v0x7fd29d0bc650_0
                        E0x7fd29d0bd7d0
                        (not v0x7fd29d0bce90_0)
                        (<= v0x7fd29d0bd510_0 1.0)
                        (>= v0x7fd29d0bd510_0 1.0)))))
      (a!6 (=> v0x7fd29d0bd450_0
               (or (and E0x7fd29d0bd5d0 (not E0x7fd29d0bd7d0))
                   (and E0x7fd29d0bd7d0 (not E0x7fd29d0bd5d0)))))
      (a!7 (or (and v0x7fd29d0bdd90_0
                    E0x7fd29d0be790
                    (and (<= v0x7fd29d0be550_0 v0x7fd29d0ba490_0)
                         (>= v0x7fd29d0be550_0 v0x7fd29d0ba490_0))
                    (and (<= v0x7fd29d0be610_0 v0x7fd29d0bd510_0)
                         (>= v0x7fd29d0be610_0 v0x7fd29d0bd510_0))
                    (<= v0x7fd29d0be6d0_0 v0x7fd29d0be350_0)
                    (>= v0x7fd29d0be6d0_0 v0x7fd29d0be350_0))
               (and v0x7fd29d0bd450_0
                    E0x7fd29d0beb50
                    v0x7fd29d0bdc50_0
                    (and (<= v0x7fd29d0be550_0 v0x7fd29d0ba490_0)
                         (>= v0x7fd29d0be550_0 v0x7fd29d0ba490_0))
                    (and (<= v0x7fd29d0be610_0 v0x7fd29d0bd510_0)
                         (>= v0x7fd29d0be610_0 v0x7fd29d0bd510_0))
                    (and (<= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)
                         (>= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)))
               (and v0x7fd29d0bcfd0_0
                    E0x7fd29d0bedd0
                    (not v0x7fd29d0bd310_0)
                    (<= v0x7fd29d0be550_0 0.0)
                    (>= v0x7fd29d0be550_0 0.0)
                    (<= v0x7fd29d0be610_0 0.0)
                    (>= v0x7fd29d0be610_0 0.0)
                    (and (<= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)
                         (>= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)))))
      (a!8 (=> v0x7fd29d0be490_0
               (or (and E0x7fd29d0be790
                        (not E0x7fd29d0beb50)
                        (not E0x7fd29d0bedd0))
                   (and E0x7fd29d0beb50
                        (not E0x7fd29d0be790)
                        (not E0x7fd29d0bedd0))
                   (and E0x7fd29d0bedd0
                        (not E0x7fd29d0be790)
                        (not E0x7fd29d0beb50)))))
      (a!9 (=> v0x7fd29d0bff50_0
               (or (and v0x7fd29d0bfa50_0
                        E0x7fd29d0c00d0
                        (<= v0x7fd29d0c0010_0 v0x7fd29d0bfe10_0)
                        (>= v0x7fd29d0c0010_0 v0x7fd29d0bfe10_0))
                   (and v0x7fd29d0be490_0
                        E0x7fd29d0c0290
                        (not v0x7fd29d0bf910_0)
                        (<= v0x7fd29d0c0010_0 v0x7fd29d0be550_0)
                        (>= v0x7fd29d0c0010_0 v0x7fd29d0be550_0)))))
      (a!10 (=> v0x7fd29d0bff50_0
                (or (and E0x7fd29d0c00d0 (not E0x7fd29d0c0290))
                    (and E0x7fd29d0c0290 (not E0x7fd29d0c00d0))))))
(let ((a!11 (and (=> v0x7fd29d0bb050_0
                     (and v0x7fd29d0bad90_0
                          E0x7fd29d0bb110
                          (not v0x7fd29d0baf10_0)))
                 (=> v0x7fd29d0bb050_0 E0x7fd29d0bb110)
                 a!1
                 a!2
                 (=> v0x7fd29d0bc110_0
                     (and v0x7fd29d0bb790_0
                          E0x7fd29d0bc1d0
                          (not v0x7fd29d0bbfd0_0)))
                 (=> v0x7fd29d0bc110_0 E0x7fd29d0bc1d0)
                 a!3
                 a!4
                 (=> v0x7fd29d0bcfd0_0
                     (and v0x7fd29d0bc650_0 E0x7fd29d0bd090 v0x7fd29d0bce90_0))
                 (=> v0x7fd29d0bcfd0_0 E0x7fd29d0bd090)
                 a!5
                 a!6
                 (=> v0x7fd29d0bdd90_0
                     (and v0x7fd29d0bd450_0
                          E0x7fd29d0bde50
                          (not v0x7fd29d0bdc50_0)))
                 (=> v0x7fd29d0bdd90_0 E0x7fd29d0bde50)
                 (=> v0x7fd29d0be490_0 a!7)
                 a!8
                 (=> v0x7fd29d0bfa50_0
                     (and v0x7fd29d0be490_0 E0x7fd29d0bfb10 v0x7fd29d0bf910_0))
                 (=> v0x7fd29d0bfa50_0 E0x7fd29d0bfb10)
                 a!9
                 a!10
                 v0x7fd29d0bff50_0
                 (not v0x7fd29d0c0c10_0)
                 (<= v0x7fd29d0ba550_0 v0x7fd29d0be610_0)
                 (>= v0x7fd29d0ba550_0 v0x7fd29d0be610_0)
                 (<= v0x7fd29d0ba650_0 v0x7fd29d0c0010_0)
                 (>= v0x7fd29d0ba650_0 v0x7fd29d0c0010_0)
                 (<= v0x7fd29d0ba750_0 v0x7fd29d0bc710_0)
                 (>= v0x7fd29d0ba750_0 v0x7fd29d0bc710_0)
                 (<= v0x7fd29d0b9010_0 v0x7fd29d0be6d0_0)
                 (>= v0x7fd29d0b9010_0 v0x7fd29d0be6d0_0)
                 (= v0x7fd29d0baf10_0 (= v0x7fd29d0bae50_0 0.0))
                 (= v0x7fd29d0bb350_0 (< v0x7fd29d0ba690_0 2.0))
                 (= v0x7fd29d0bb510_0 (ite v0x7fd29d0bb350_0 1.0 0.0))
                 (= v0x7fd29d0bb650_0 (+ v0x7fd29d0bb510_0 v0x7fd29d0ba690_0))
                 (= v0x7fd29d0bbfd0_0 (= v0x7fd29d0bbf10_0 0.0))
                 (= v0x7fd29d0bc3d0_0 (= v0x7fd29d0ba590_0 0.0))
                 (= v0x7fd29d0bc510_0 (ite v0x7fd29d0bc3d0_0 1.0 0.0))
                 (= v0x7fd29d0bce90_0 (= v0x7fd29d0bcdd0_0 0.0))
                 (= v0x7fd29d0bd310_0 (= v0x7fd29d0bd250_0 0.0))
                 (= v0x7fd29d0bdc50_0 (= v0x7fd29d0ba490_0 0.0))
                 (= v0x7fd29d0be050_0 (> v0x7fd29d0bb850_0 0.0))
                 (= v0x7fd29d0be190_0 (+ v0x7fd29d0bb850_0 (- 1.0)))
                 (= v0x7fd29d0be350_0
                    (ite v0x7fd29d0be050_0 v0x7fd29d0be190_0 v0x7fd29d0bb850_0))
                 (= v0x7fd29d0bf410_0 (not (= v0x7fd29d0be610_0 0.0)))
                 (= v0x7fd29d0bf550_0 (= v0x7fd29d0be550_0 0.0))
                 (= v0x7fd29d0bf690_0 (> v0x7fd29d0be6d0_0 1.0))
                 (= v0x7fd29d0bf7d0_0 (and v0x7fd29d0bf410_0 v0x7fd29d0bf550_0))
                 (= v0x7fd29d0bf910_0 (and v0x7fd29d0bf7d0_0 v0x7fd29d0bf690_0))
                 (= v0x7fd29d0bfcd0_0 (= v0x7fd29d0bc710_0 0.0))
                 (= v0x7fd29d0bfe10_0
                    (ite v0x7fd29d0bfcd0_0 1.0 v0x7fd29d0be550_0))
                 (= v0x7fd29d0c0710_0 (= v0x7fd29d0be6d0_0 2.0))
                 (= v0x7fd29d0c0850_0 (= v0x7fd29d0c0010_0 0.0))
                 (= v0x7fd29d0c0990_0 (or v0x7fd29d0c0850_0 v0x7fd29d0c0710_0))
                 (= v0x7fd29d0c0ad0_0 (xor v0x7fd29d0c0990_0 true))
                 (= v0x7fd29d0c0c10_0 (and v0x7fd29d0bf550_0 v0x7fd29d0c0ad0_0)))))
  (=> F0x7fd29d0c18d0 a!11))))
(assert (=> F0x7fd29d0c18d0 F0x7fd29d0c1a90))
(assert (let ((a!1 (=> v0x7fd29d0bb790_0
               (or (and v0x7fd29d0bb050_0
                        E0x7fd29d0bb910
                        (<= v0x7fd29d0bb850_0 v0x7fd29d0bb650_0)
                        (>= v0x7fd29d0bb850_0 v0x7fd29d0bb650_0))
                   (and v0x7fd29d0bad90_0
                        E0x7fd29d0bbad0
                        v0x7fd29d0baf10_0
                        (<= v0x7fd29d0bb850_0 v0x7fd29d0ba690_0)
                        (>= v0x7fd29d0bb850_0 v0x7fd29d0ba690_0)))))
      (a!2 (=> v0x7fd29d0bb790_0
               (or (and E0x7fd29d0bb910 (not E0x7fd29d0bbad0))
                   (and E0x7fd29d0bbad0 (not E0x7fd29d0bb910)))))
      (a!3 (=> v0x7fd29d0bc650_0
               (or (and v0x7fd29d0bc110_0
                        E0x7fd29d0bc7d0
                        (<= v0x7fd29d0bc710_0 v0x7fd29d0bc510_0)
                        (>= v0x7fd29d0bc710_0 v0x7fd29d0bc510_0))
                   (and v0x7fd29d0bb790_0
                        E0x7fd29d0bc990
                        v0x7fd29d0bbfd0_0
                        (<= v0x7fd29d0bc710_0 v0x7fd29d0ba590_0)
                        (>= v0x7fd29d0bc710_0 v0x7fd29d0ba590_0)))))
      (a!4 (=> v0x7fd29d0bc650_0
               (or (and E0x7fd29d0bc7d0 (not E0x7fd29d0bc990))
                   (and E0x7fd29d0bc990 (not E0x7fd29d0bc7d0)))))
      (a!5 (=> v0x7fd29d0bd450_0
               (or (and v0x7fd29d0bcfd0_0
                        E0x7fd29d0bd5d0
                        v0x7fd29d0bd310_0
                        (<= v0x7fd29d0bd510_0 v0x7fd29d0ba310_0)
                        (>= v0x7fd29d0bd510_0 v0x7fd29d0ba310_0))
                   (and v0x7fd29d0bc650_0
                        E0x7fd29d0bd7d0
                        (not v0x7fd29d0bce90_0)
                        (<= v0x7fd29d0bd510_0 1.0)
                        (>= v0x7fd29d0bd510_0 1.0)))))
      (a!6 (=> v0x7fd29d0bd450_0
               (or (and E0x7fd29d0bd5d0 (not E0x7fd29d0bd7d0))
                   (and E0x7fd29d0bd7d0 (not E0x7fd29d0bd5d0)))))
      (a!7 (or (and v0x7fd29d0bdd90_0
                    E0x7fd29d0be790
                    (and (<= v0x7fd29d0be550_0 v0x7fd29d0ba490_0)
                         (>= v0x7fd29d0be550_0 v0x7fd29d0ba490_0))
                    (and (<= v0x7fd29d0be610_0 v0x7fd29d0bd510_0)
                         (>= v0x7fd29d0be610_0 v0x7fd29d0bd510_0))
                    (<= v0x7fd29d0be6d0_0 v0x7fd29d0be350_0)
                    (>= v0x7fd29d0be6d0_0 v0x7fd29d0be350_0))
               (and v0x7fd29d0bd450_0
                    E0x7fd29d0beb50
                    v0x7fd29d0bdc50_0
                    (and (<= v0x7fd29d0be550_0 v0x7fd29d0ba490_0)
                         (>= v0x7fd29d0be550_0 v0x7fd29d0ba490_0))
                    (and (<= v0x7fd29d0be610_0 v0x7fd29d0bd510_0)
                         (>= v0x7fd29d0be610_0 v0x7fd29d0bd510_0))
                    (and (<= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)
                         (>= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)))
               (and v0x7fd29d0bcfd0_0
                    E0x7fd29d0bedd0
                    (not v0x7fd29d0bd310_0)
                    (<= v0x7fd29d0be550_0 0.0)
                    (>= v0x7fd29d0be550_0 0.0)
                    (<= v0x7fd29d0be610_0 0.0)
                    (>= v0x7fd29d0be610_0 0.0)
                    (and (<= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)
                         (>= v0x7fd29d0be6d0_0 v0x7fd29d0bb850_0)))))
      (a!8 (=> v0x7fd29d0be490_0
               (or (and E0x7fd29d0be790
                        (not E0x7fd29d0beb50)
                        (not E0x7fd29d0bedd0))
                   (and E0x7fd29d0beb50
                        (not E0x7fd29d0be790)
                        (not E0x7fd29d0bedd0))
                   (and E0x7fd29d0bedd0
                        (not E0x7fd29d0be790)
                        (not E0x7fd29d0beb50)))))
      (a!9 (=> v0x7fd29d0bff50_0
               (or (and v0x7fd29d0bfa50_0
                        E0x7fd29d0c00d0
                        (<= v0x7fd29d0c0010_0 v0x7fd29d0bfe10_0)
                        (>= v0x7fd29d0c0010_0 v0x7fd29d0bfe10_0))
                   (and v0x7fd29d0be490_0
                        E0x7fd29d0c0290
                        (not v0x7fd29d0bf910_0)
                        (<= v0x7fd29d0c0010_0 v0x7fd29d0be550_0)
                        (>= v0x7fd29d0c0010_0 v0x7fd29d0be550_0)))))
      (a!10 (=> v0x7fd29d0bff50_0
                (or (and E0x7fd29d0c00d0 (not E0x7fd29d0c0290))
                    (and E0x7fd29d0c0290 (not E0x7fd29d0c00d0))))))
(let ((a!11 (and (=> v0x7fd29d0bb050_0
                     (and v0x7fd29d0bad90_0
                          E0x7fd29d0bb110
                          (not v0x7fd29d0baf10_0)))
                 (=> v0x7fd29d0bb050_0 E0x7fd29d0bb110)
                 a!1
                 a!2
                 (=> v0x7fd29d0bc110_0
                     (and v0x7fd29d0bb790_0
                          E0x7fd29d0bc1d0
                          (not v0x7fd29d0bbfd0_0)))
                 (=> v0x7fd29d0bc110_0 E0x7fd29d0bc1d0)
                 a!3
                 a!4
                 (=> v0x7fd29d0bcfd0_0
                     (and v0x7fd29d0bc650_0 E0x7fd29d0bd090 v0x7fd29d0bce90_0))
                 (=> v0x7fd29d0bcfd0_0 E0x7fd29d0bd090)
                 a!5
                 a!6
                 (=> v0x7fd29d0bdd90_0
                     (and v0x7fd29d0bd450_0
                          E0x7fd29d0bde50
                          (not v0x7fd29d0bdc50_0)))
                 (=> v0x7fd29d0bdd90_0 E0x7fd29d0bde50)
                 (=> v0x7fd29d0be490_0 a!7)
                 a!8
                 (=> v0x7fd29d0bfa50_0
                     (and v0x7fd29d0be490_0 E0x7fd29d0bfb10 v0x7fd29d0bf910_0))
                 (=> v0x7fd29d0bfa50_0 E0x7fd29d0bfb10)
                 a!9
                 a!10
                 v0x7fd29d0bff50_0
                 v0x7fd29d0c0c10_0
                 (= v0x7fd29d0baf10_0 (= v0x7fd29d0bae50_0 0.0))
                 (= v0x7fd29d0bb350_0 (< v0x7fd29d0ba690_0 2.0))
                 (= v0x7fd29d0bb510_0 (ite v0x7fd29d0bb350_0 1.0 0.0))
                 (= v0x7fd29d0bb650_0 (+ v0x7fd29d0bb510_0 v0x7fd29d0ba690_0))
                 (= v0x7fd29d0bbfd0_0 (= v0x7fd29d0bbf10_0 0.0))
                 (= v0x7fd29d0bc3d0_0 (= v0x7fd29d0ba590_0 0.0))
                 (= v0x7fd29d0bc510_0 (ite v0x7fd29d0bc3d0_0 1.0 0.0))
                 (= v0x7fd29d0bce90_0 (= v0x7fd29d0bcdd0_0 0.0))
                 (= v0x7fd29d0bd310_0 (= v0x7fd29d0bd250_0 0.0))
                 (= v0x7fd29d0bdc50_0 (= v0x7fd29d0ba490_0 0.0))
                 (= v0x7fd29d0be050_0 (> v0x7fd29d0bb850_0 0.0))
                 (= v0x7fd29d0be190_0 (+ v0x7fd29d0bb850_0 (- 1.0)))
                 (= v0x7fd29d0be350_0
                    (ite v0x7fd29d0be050_0 v0x7fd29d0be190_0 v0x7fd29d0bb850_0))
                 (= v0x7fd29d0bf410_0 (not (= v0x7fd29d0be610_0 0.0)))
                 (= v0x7fd29d0bf550_0 (= v0x7fd29d0be550_0 0.0))
                 (= v0x7fd29d0bf690_0 (> v0x7fd29d0be6d0_0 1.0))
                 (= v0x7fd29d0bf7d0_0 (and v0x7fd29d0bf410_0 v0x7fd29d0bf550_0))
                 (= v0x7fd29d0bf910_0 (and v0x7fd29d0bf7d0_0 v0x7fd29d0bf690_0))
                 (= v0x7fd29d0bfcd0_0 (= v0x7fd29d0bc710_0 0.0))
                 (= v0x7fd29d0bfe10_0
                    (ite v0x7fd29d0bfcd0_0 1.0 v0x7fd29d0be550_0))
                 (= v0x7fd29d0c0710_0 (= v0x7fd29d0be6d0_0 2.0))
                 (= v0x7fd29d0c0850_0 (= v0x7fd29d0c0010_0 0.0))
                 (= v0x7fd29d0c0990_0 (or v0x7fd29d0c0850_0 v0x7fd29d0c0710_0))
                 (= v0x7fd29d0c0ad0_0 (xor v0x7fd29d0c0990_0 true))
                 (= v0x7fd29d0c0c10_0 (and v0x7fd29d0bf550_0 v0x7fd29d0c0ad0_0)))))
  (=> F0x7fd29d0c1b50 a!11))))
(assert (=> F0x7fd29d0c1b50 F0x7fd29d0c1a90))
(assert (=> F0x7fd29d0c1c50 (or F0x7fd29d0c1a50 F0x7fd29d0c18d0)))
(assert (=> F0x7fd29d0c1c10 F0x7fd29d0c1b50))
(assert (=> pre!entry!0 (=> F0x7fd29d0c1990 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd29d0c1a90 (<= v0x7fd29d0ba690_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fd29d0c1a90
        (or (<= v0x7fd29d0ba690_0 1.0) (>= v0x7fd29d0ba690_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fd29d0c1a90 (>= v0x7fd29d0ba690_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fd29d0c1a90
        (or (>= v0x7fd29d0ba690_0 1.0) (<= v0x7fd29d0ba690_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fd29d0c1c50 (<= v0x7fd29d0b9010_0 2.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fd29d0c1c50
       (or (<= v0x7fd29d0b9010_0 1.0) (>= v0x7fd29d0b9010_0 2.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fd29d0c1c50 (>= v0x7fd29d0b9010_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7fd29d0c1c50
       (or (>= v0x7fd29d0b9010_0 1.0) (<= v0x7fd29d0b9010_0 0.0)))))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7fd29d0c1c10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7fd29d0c1990)
; (error: F0x7fd29d0c1c10)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i23.i.i!0)
