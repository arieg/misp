(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun F0x7fbae8620f50 () Bool)
(declare-fun F0x7fbae8620e90 () Bool)
(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fbae861ff10_0 () Bool)
(declare-fun v0x7fbae861fdd0_0 () Bool)
(declare-fun v0x7fbae861fc90_0 () Bool)
(declare-fun F0x7fbae8620f90 () Bool)
(declare-fun v0x7fbae861ec10_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7fbae861e190_0 () Real)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun v0x7fbae861e050_0 () Bool)
(declare-fun v0x7fbae861ccd0_0 () Real)
(declare-fun v0x7fbae861c2d0_0 () Real)
(declare-fun v0x7fbae8620050_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7fbae861f6d0 () Bool)
(declare-fun v0x7fbae861bc10_0 () Real)
(declare-fun v0x7fbae861f250_0 () Real)
(declare-fun E0x7fbae861f510 () Bool)
(declare-fun v0x7fbae861ee90_0 () Bool)
(declare-fun v0x7fbae861f390_0 () Bool)
(declare-fun v0x7fbae861e350_0 () Real)
(declare-fun v0x7fbae861dd90_0 () Bool)
(declare-fun E0x7fbae861d750 () Bool)
(declare-fun v0x7fbae861d4d0_0 () Real)
(declare-fun E0x7fbae861ef50 () Bool)
(declare-fun v0x7fbae861dc50_0 () Bool)
(declare-fun E0x7fbae861e7d0 () Bool)
(declare-fun v0x7fbae861b590_0 () Real)
(declare-fun E0x7fbae861de50 () Bool)
(declare-fun v0x7fbae861c610_0 () Real)
(declare-fun v0x7fbae861cd90_0 () Bool)
(declare-fun v0x7fbae861c550_0 () Bool)
(declare-fun v0x7fbae861d2d0_0 () Real)
(declare-fun F0x7fbae8620dd0 () Bool)
(declare-fun v0x7fbae861bcd0_0 () Bool)
(declare-fun v0x7fbae861d190_0 () Bool)
(declare-fun E0x7fbae861bed0 () Bool)
(declare-fun v0x7fbae861b310_0 () Real)
(declare-fun v0x7fbae861f450_0 () Real)
(declare-fun v0x7fbae861be10_0 () Bool)
(declare-fun v0x7fbae861c110_0 () Bool)
(declare-fun v0x7fbae861bb50_0 () Bool)
(declare-fun E0x7fbae861d590 () Bool)
(declare-fun F0x7fbae8620cd0 () Bool)
(declare-fun v0x7fbae861f110_0 () Bool)
(declare-fun v0x7fbae861ed50_0 () Bool)
(declare-fun v0x7fbae861e550_0 () Real)
(declare-fun v0x7fbae861c410_0 () Real)
(declare-fun v0x7fbae861b650_0 () Real)
(declare-fun v0x7fbae861b490_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7fbae861c6d0 () Bool)
(declare-fun v0x7fbae861fb50_0 () Bool)
(declare-fun v0x7fbae861a110_0 () Bool)
(declare-fun E0x7fbae861c890 () Bool)
(declare-fun F0x7fbae8620c10 () Bool)
(declare-fun E0x7fbae861e610 () Bool)
(declare-fun v0x7fbae861d410_0 () Bool)
(declare-fun E0x7fbae861cf90 () Bool)
(declare-fun v0x7fbae861e490_0 () Bool)
(declare-fun v0x7fbae861ced0_0 () Bool)
(declare-fun F0x7fbae8620d90 () Bool)
(declare-fun v0x7fbae861b550_0 () Real)
(declare-fun v0x7fbae861a010_0 () Real)

(assert (=> F0x7fbae8620d90
    (and v0x7fbae861a110_0
         (<= v0x7fbae861b550_0 0.0)
         (>= v0x7fbae861b550_0 0.0)
         (<= v0x7fbae861b650_0 0.0)
         (>= v0x7fbae861b650_0 0.0)
         (<= v0x7fbae861a010_0 1.0)
         (>= v0x7fbae861a010_0 1.0))))
(assert (=> F0x7fbae8620d90 F0x7fbae8620cd0))
(assert (let ((a!1 (=> v0x7fbae861c550_0
               (or (and v0x7fbae861be10_0
                        E0x7fbae861c6d0
                        (<= v0x7fbae861c610_0 v0x7fbae861c410_0)
                        (>= v0x7fbae861c610_0 v0x7fbae861c410_0))
                   (and v0x7fbae861bb50_0
                        E0x7fbae861c890
                        v0x7fbae861bcd0_0
                        (<= v0x7fbae861c610_0 v0x7fbae861b590_0)
                        (>= v0x7fbae861c610_0 v0x7fbae861b590_0)))))
      (a!2 (=> v0x7fbae861c550_0
               (or (and E0x7fbae861c6d0 (not E0x7fbae861c890))
                   (and E0x7fbae861c890 (not E0x7fbae861c6d0)))))
      (a!3 (=> v0x7fbae861d410_0
               (or (and v0x7fbae861ced0_0
                        E0x7fbae861d590
                        (<= v0x7fbae861d4d0_0 v0x7fbae861d2d0_0)
                        (>= v0x7fbae861d4d0_0 v0x7fbae861d2d0_0))
                   (and v0x7fbae861c550_0
                        E0x7fbae861d750
                        v0x7fbae861cd90_0
                        (<= v0x7fbae861d4d0_0 v0x7fbae861b490_0)
                        (>= v0x7fbae861d4d0_0 v0x7fbae861b490_0)))))
      (a!4 (=> v0x7fbae861d410_0
               (or (and E0x7fbae861d590 (not E0x7fbae861d750))
                   (and E0x7fbae861d750 (not E0x7fbae861d590)))))
      (a!5 (=> v0x7fbae861e490_0
               (or (and v0x7fbae861dd90_0
                        E0x7fbae861e610
                        (<= v0x7fbae861e550_0 v0x7fbae861e350_0)
                        (>= v0x7fbae861e550_0 v0x7fbae861e350_0))
                   (and v0x7fbae861d410_0
                        E0x7fbae861e7d0
                        v0x7fbae861dc50_0
                        (<= v0x7fbae861e550_0 v0x7fbae861c610_0)
                        (>= v0x7fbae861e550_0 v0x7fbae861c610_0)))))
      (a!6 (=> v0x7fbae861e490_0
               (or (and E0x7fbae861e610 (not E0x7fbae861e7d0))
                   (and E0x7fbae861e7d0 (not E0x7fbae861e610)))))
      (a!7 (=> v0x7fbae861f390_0
               (or (and v0x7fbae861ee90_0
                        E0x7fbae861f510
                        (<= v0x7fbae861f450_0 v0x7fbae861f250_0)
                        (>= v0x7fbae861f450_0 v0x7fbae861f250_0))
                   (and v0x7fbae861e490_0
                        E0x7fbae861f6d0
                        (not v0x7fbae861ed50_0)
                        (<= v0x7fbae861f450_0 v0x7fbae861b310_0)
                        (>= v0x7fbae861f450_0 v0x7fbae861b310_0)))))
      (a!8 (=> v0x7fbae861f390_0
               (or (and E0x7fbae861f510 (not E0x7fbae861f6d0))
                   (and E0x7fbae861f6d0 (not E0x7fbae861f510))))))
(let ((a!9 (and (=> v0x7fbae861be10_0
                    (and v0x7fbae861bb50_0
                         E0x7fbae861bed0
                         (not v0x7fbae861bcd0_0)))
                (=> v0x7fbae861be10_0 E0x7fbae861bed0)
                a!1
                a!2
                (=> v0x7fbae861ced0_0
                    (and v0x7fbae861c550_0
                         E0x7fbae861cf90
                         (not v0x7fbae861cd90_0)))
                (=> v0x7fbae861ced0_0 E0x7fbae861cf90)
                a!3
                a!4
                (=> v0x7fbae861dd90_0
                    (and v0x7fbae861d410_0
                         E0x7fbae861de50
                         (not v0x7fbae861dc50_0)))
                (=> v0x7fbae861dd90_0 E0x7fbae861de50)
                a!5
                a!6
                (=> v0x7fbae861ee90_0
                    (and v0x7fbae861e490_0 E0x7fbae861ef50 v0x7fbae861ed50_0))
                (=> v0x7fbae861ee90_0 E0x7fbae861ef50)
                a!7
                a!8
                v0x7fbae861f390_0
                (not v0x7fbae8620050_0)
                (<= v0x7fbae861b550_0 v0x7fbae861f450_0)
                (>= v0x7fbae861b550_0 v0x7fbae861f450_0)
                (<= v0x7fbae861b650_0 v0x7fbae861d4d0_0)
                (>= v0x7fbae861b650_0 v0x7fbae861d4d0_0)
                (<= v0x7fbae861a010_0 v0x7fbae861e550_0)
                (>= v0x7fbae861a010_0 v0x7fbae861e550_0)
                (= v0x7fbae861bcd0_0 (= v0x7fbae861bc10_0 0.0))
                (= v0x7fbae861c110_0 (< v0x7fbae861b590_0 2.0))
                (= v0x7fbae861c2d0_0 (ite v0x7fbae861c110_0 1.0 0.0))
                (= v0x7fbae861c410_0 (+ v0x7fbae861c2d0_0 v0x7fbae861b590_0))
                (= v0x7fbae861cd90_0 (= v0x7fbae861ccd0_0 0.0))
                (= v0x7fbae861d190_0 (= v0x7fbae861b490_0 0.0))
                (= v0x7fbae861d2d0_0 (ite v0x7fbae861d190_0 1.0 0.0))
                (= v0x7fbae861dc50_0 (= v0x7fbae861b310_0 0.0))
                (= v0x7fbae861e050_0 (> v0x7fbae861c610_0 0.0))
                (= v0x7fbae861e190_0 (+ v0x7fbae861c610_0 (- 1.0)))
                (= v0x7fbae861e350_0
                   (ite v0x7fbae861e050_0 v0x7fbae861e190_0 v0x7fbae861c610_0))
                (= v0x7fbae861ec10_0 (> v0x7fbae861e550_0 1.0))
                (= v0x7fbae861ed50_0 (and v0x7fbae861ec10_0 v0x7fbae861dc50_0))
                (= v0x7fbae861f110_0 (= v0x7fbae861d4d0_0 0.0))
                (= v0x7fbae861f250_0
                   (ite v0x7fbae861f110_0 1.0 v0x7fbae861b310_0))
                (= v0x7fbae861fb50_0 (= v0x7fbae861e550_0 2.0))
                (= v0x7fbae861fc90_0 (= v0x7fbae861f450_0 0.0))
                (= v0x7fbae861fdd0_0 (or v0x7fbae861fc90_0 v0x7fbae861fb50_0))
                (= v0x7fbae861ff10_0 (xor v0x7fbae861fdd0_0 true))
                (= v0x7fbae8620050_0 (and v0x7fbae861dc50_0 v0x7fbae861ff10_0)))))
  (=> F0x7fbae8620c10 a!9))))
(assert (=> F0x7fbae8620c10 F0x7fbae8620dd0))
(assert (let ((a!1 (=> v0x7fbae861c550_0
               (or (and v0x7fbae861be10_0
                        E0x7fbae861c6d0
                        (<= v0x7fbae861c610_0 v0x7fbae861c410_0)
                        (>= v0x7fbae861c610_0 v0x7fbae861c410_0))
                   (and v0x7fbae861bb50_0
                        E0x7fbae861c890
                        v0x7fbae861bcd0_0
                        (<= v0x7fbae861c610_0 v0x7fbae861b590_0)
                        (>= v0x7fbae861c610_0 v0x7fbae861b590_0)))))
      (a!2 (=> v0x7fbae861c550_0
               (or (and E0x7fbae861c6d0 (not E0x7fbae861c890))
                   (and E0x7fbae861c890 (not E0x7fbae861c6d0)))))
      (a!3 (=> v0x7fbae861d410_0
               (or (and v0x7fbae861ced0_0
                        E0x7fbae861d590
                        (<= v0x7fbae861d4d0_0 v0x7fbae861d2d0_0)
                        (>= v0x7fbae861d4d0_0 v0x7fbae861d2d0_0))
                   (and v0x7fbae861c550_0
                        E0x7fbae861d750
                        v0x7fbae861cd90_0
                        (<= v0x7fbae861d4d0_0 v0x7fbae861b490_0)
                        (>= v0x7fbae861d4d0_0 v0x7fbae861b490_0)))))
      (a!4 (=> v0x7fbae861d410_0
               (or (and E0x7fbae861d590 (not E0x7fbae861d750))
                   (and E0x7fbae861d750 (not E0x7fbae861d590)))))
      (a!5 (=> v0x7fbae861e490_0
               (or (and v0x7fbae861dd90_0
                        E0x7fbae861e610
                        (<= v0x7fbae861e550_0 v0x7fbae861e350_0)
                        (>= v0x7fbae861e550_0 v0x7fbae861e350_0))
                   (and v0x7fbae861d410_0
                        E0x7fbae861e7d0
                        v0x7fbae861dc50_0
                        (<= v0x7fbae861e550_0 v0x7fbae861c610_0)
                        (>= v0x7fbae861e550_0 v0x7fbae861c610_0)))))
      (a!6 (=> v0x7fbae861e490_0
               (or (and E0x7fbae861e610 (not E0x7fbae861e7d0))
                   (and E0x7fbae861e7d0 (not E0x7fbae861e610)))))
      (a!7 (=> v0x7fbae861f390_0
               (or (and v0x7fbae861ee90_0
                        E0x7fbae861f510
                        (<= v0x7fbae861f450_0 v0x7fbae861f250_0)
                        (>= v0x7fbae861f450_0 v0x7fbae861f250_0))
                   (and v0x7fbae861e490_0
                        E0x7fbae861f6d0
                        (not v0x7fbae861ed50_0)
                        (<= v0x7fbae861f450_0 v0x7fbae861b310_0)
                        (>= v0x7fbae861f450_0 v0x7fbae861b310_0)))))
      (a!8 (=> v0x7fbae861f390_0
               (or (and E0x7fbae861f510 (not E0x7fbae861f6d0))
                   (and E0x7fbae861f6d0 (not E0x7fbae861f510))))))
(let ((a!9 (and (=> v0x7fbae861be10_0
                    (and v0x7fbae861bb50_0
                         E0x7fbae861bed0
                         (not v0x7fbae861bcd0_0)))
                (=> v0x7fbae861be10_0 E0x7fbae861bed0)
                a!1
                a!2
                (=> v0x7fbae861ced0_0
                    (and v0x7fbae861c550_0
                         E0x7fbae861cf90
                         (not v0x7fbae861cd90_0)))
                (=> v0x7fbae861ced0_0 E0x7fbae861cf90)
                a!3
                a!4
                (=> v0x7fbae861dd90_0
                    (and v0x7fbae861d410_0
                         E0x7fbae861de50
                         (not v0x7fbae861dc50_0)))
                (=> v0x7fbae861dd90_0 E0x7fbae861de50)
                a!5
                a!6
                (=> v0x7fbae861ee90_0
                    (and v0x7fbae861e490_0 E0x7fbae861ef50 v0x7fbae861ed50_0))
                (=> v0x7fbae861ee90_0 E0x7fbae861ef50)
                a!7
                a!8
                v0x7fbae861f390_0
                v0x7fbae8620050_0
                (= v0x7fbae861bcd0_0 (= v0x7fbae861bc10_0 0.0))
                (= v0x7fbae861c110_0 (< v0x7fbae861b590_0 2.0))
                (= v0x7fbae861c2d0_0 (ite v0x7fbae861c110_0 1.0 0.0))
                (= v0x7fbae861c410_0 (+ v0x7fbae861c2d0_0 v0x7fbae861b590_0))
                (= v0x7fbae861cd90_0 (= v0x7fbae861ccd0_0 0.0))
                (= v0x7fbae861d190_0 (= v0x7fbae861b490_0 0.0))
                (= v0x7fbae861d2d0_0 (ite v0x7fbae861d190_0 1.0 0.0))
                (= v0x7fbae861dc50_0 (= v0x7fbae861b310_0 0.0))
                (= v0x7fbae861e050_0 (> v0x7fbae861c610_0 0.0))
                (= v0x7fbae861e190_0 (+ v0x7fbae861c610_0 (- 1.0)))
                (= v0x7fbae861e350_0
                   (ite v0x7fbae861e050_0 v0x7fbae861e190_0 v0x7fbae861c610_0))
                (= v0x7fbae861ec10_0 (> v0x7fbae861e550_0 1.0))
                (= v0x7fbae861ed50_0 (and v0x7fbae861ec10_0 v0x7fbae861dc50_0))
                (= v0x7fbae861f110_0 (= v0x7fbae861d4d0_0 0.0))
                (= v0x7fbae861f250_0
                   (ite v0x7fbae861f110_0 1.0 v0x7fbae861b310_0))
                (= v0x7fbae861fb50_0 (= v0x7fbae861e550_0 2.0))
                (= v0x7fbae861fc90_0 (= v0x7fbae861f450_0 0.0))
                (= v0x7fbae861fdd0_0 (or v0x7fbae861fc90_0 v0x7fbae861fb50_0))
                (= v0x7fbae861ff10_0 (xor v0x7fbae861fdd0_0 true))
                (= v0x7fbae8620050_0 (and v0x7fbae861dc50_0 v0x7fbae861ff10_0)))))
  (=> F0x7fbae8620e90 a!9))))
(assert (=> F0x7fbae8620e90 F0x7fbae8620dd0))
(assert (=> F0x7fbae8620f90 (or F0x7fbae8620d90 F0x7fbae8620c10)))
(assert (=> F0x7fbae8620f50 F0x7fbae8620e90))
(assert (=> pre!entry!0 (=> F0x7fbae8620cd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fbae8620dd0 (<= v0x7fbae861b590_0 2.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fbae8620dd0
        (or (<= v0x7fbae861b590_0 1.0) (>= v0x7fbae861b590_0 2.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fbae8620dd0 (>= v0x7fbae861b590_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7fbae8620dd0
        (or (>= v0x7fbae861b590_0 1.0) (<= v0x7fbae861b590_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fbae8620f90 (<= v0x7fbae861a010_0 2.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fbae8620f90
       (or (<= v0x7fbae861a010_0 1.0) (>= v0x7fbae861a010_0 2.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fbae8620f90 (>= v0x7fbae861a010_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7fbae8620f90
       (or (>= v0x7fbae861a010_0 1.0) (<= v0x7fbae861a010_0 0.0)))))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7fbae8620f50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3)
; (init: F0x7fbae8620cd0)
; (error: F0x7fbae8620f50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb2.i.i23.i.i!0)
