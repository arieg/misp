(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f80ac843e90 () Bool)
(declare-fun post!bb2.i.i29.i.i!0 () Bool)
(declare-fun F0x7f80ac843d50 () Bool)
(declare-fun v0x7f80ac842e10_0 () Bool)
(declare-fun v0x7f80ac842cd0_0 () Bool)
(declare-fun v0x7f80ac83e690_0 () Real)
(declare-fun v0x7f80ac840e90_0 () Bool)
(declare-fun v0x7f80ac842f50_0 () Bool)
(declare-fun E0x7f80ac8424d0 () Bool)
(declare-fun F0x7f80ac843c90 () Bool)
(declare-fun v0x7f80ac841190_0 () Real)
(declare-fun E0x7f80ac8422d0 () Bool)
(declare-fun v0x7f80ac841c10_0 () Real)
(declare-fun lemma!bb2.i.i29.i.i!0 () Bool)
(declare-fun E0x7f80ac841d90 () Bool)
(declare-fun F0x7f80ac843e50 () Bool)
(declare-fun v0x7f80ac841b50_0 () Bool)
(declare-fun v0x7f80ac8418d0_0 () Bool)
(declare-fun v0x7f80ac840a90_0 () Bool)
(declare-fun v0x7f80ac841410_0 () Bool)
(declare-fun E0x7f80ac840c90 () Bool)
(declare-fun v0x7f80ac8406d0_0 () Bool)
(declare-fun v0x7f80ac840bd0_0 () Bool)
(declare-fun v0x7f80ac83f810_0 () Bool)
(declare-fun v0x7f80ac83f950_0 () Bool)
(declare-fun v0x7f80ac83e010_0 () Real)
(declare-fun v0x7f80ac83ee90_0 () Real)
(declare-fun v0x7f80ac841690_0 () Bool)
(declare-fun v0x7f80ac83df10_0 () Real)
(declare-fun v0x7f80ac83f090_0 () Real)
(declare-fun v0x7f80ac840810_0 () Bool)
(declare-fun E0x7f80ac842050 () Bool)
(declare-fun E0x7f80ac83f150 () Bool)
(declare-fun v0x7f80ac83e750_0 () Bool)
(declare-fun v0x7f80ac83f750_0 () Real)
(declare-fun v0x7f80ac83fc10_0 () Bool)
(declare-fun v0x7f80ac83fe90_0 () Bool)
(declare-fun v0x7f80ac840fd0_0 () Real)
(declare-fun v0x7f80ac83efd0_0 () Bool)
(declare-fun v0x7f80ac841790_0 () Real)
(declare-fun v0x7f80ac83e5d0_0 () Bool)
(declare-fun v0x7f80ac842b90_0 () Bool)
(declare-fun F0x7f80ac843bd0 () Bool)
(declare-fun E0x7f80ac8401d0 () Bool)
(declare-fun v0x7f80ac83dd90_0 () Real)
(declare-fun v0x7f80ac83ed50_0 () Real)
(declare-fun v0x7f80ac83fd50_0 () Real)
(declare-fun F0x7f80ac843b10 () Bool)
(declare-fun E0x7f80ac83f310 () Bool)
(declare-fun E0x7f80ac841990 () Bool)
(declare-fun v0x7f80ac83e890_0 () Bool)
(declare-fun v0x7f80ac83c010_0 () Real)
(declare-fun v0x7f80ac83ff50_0 () Real)
(declare-fun v0x7f80ac83e0d0_0 () Real)
(declare-fun E0x7f80ac840010 () Bool)
(declare-fun E0x7f80ac83fa10 () Bool)
(declare-fun E0x7f80ac8414d0 () Bool)
(declare-fun E0x7f80ac83e950 () Bool)
(declare-fun v0x7f80ac83dfd0_0 () Real)
(declare-fun v0x7f80ac842a90_0 () Bool)
(declare-fun E0x7f80ac8408d0 () Bool)
(declare-fun v0x7f80ac83c110_0 () Bool)
(declare-fun v0x7f80ac8412d0_0 () Bool)
(declare-fun F0x7f80ac843a50 () Bool)
(declare-fun v0x7f80ac83eb90_0 () Bool)
(declare-fun v0x7f80ac841cd0_0 () Real)

(assert (=> F0x7f80ac843a50
    (and v0x7f80ac83c110_0
         (<= v0x7f80ac83dfd0_0 0.0)
         (>= v0x7f80ac83dfd0_0 0.0)
         (<= v0x7f80ac83e0d0_0 0.0)
         (>= v0x7f80ac83e0d0_0 0.0)
         (<= v0x7f80ac83c010_0 1.0)
         (>= v0x7f80ac83c010_0 1.0))))
(assert (=> F0x7f80ac843a50 F0x7f80ac843b10))
(assert (let ((a!1 (=> v0x7f80ac83efd0_0
               (or (and v0x7f80ac83e890_0
                        E0x7f80ac83f150
                        (<= v0x7f80ac83f090_0 v0x7f80ac83ee90_0)
                        (>= v0x7f80ac83f090_0 v0x7f80ac83ee90_0))
                   (and v0x7f80ac83e5d0_0
                        E0x7f80ac83f310
                        v0x7f80ac83e750_0
                        (<= v0x7f80ac83f090_0 v0x7f80ac83e010_0)
                        (>= v0x7f80ac83f090_0 v0x7f80ac83e010_0)))))
      (a!2 (=> v0x7f80ac83efd0_0
               (or (and E0x7f80ac83f150 (not E0x7f80ac83f310))
                   (and E0x7f80ac83f310 (not E0x7f80ac83f150)))))
      (a!3 (=> v0x7f80ac83fe90_0
               (or (and v0x7f80ac83f950_0
                        E0x7f80ac840010
                        (<= v0x7f80ac83ff50_0 v0x7f80ac83fd50_0)
                        (>= v0x7f80ac83ff50_0 v0x7f80ac83fd50_0))
                   (and v0x7f80ac83efd0_0
                        E0x7f80ac8401d0
                        v0x7f80ac83f810_0
                        (<= v0x7f80ac83ff50_0 v0x7f80ac83df10_0)
                        (>= v0x7f80ac83ff50_0 v0x7f80ac83df10_0)))))
      (a!4 (=> v0x7f80ac83fe90_0
               (or (and E0x7f80ac840010 (not E0x7f80ac8401d0))
                   (and E0x7f80ac8401d0 (not E0x7f80ac840010)))))
      (a!5 (or (and v0x7f80ac841410_0
                    E0x7f80ac841d90
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac83f090_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac83f090_0))
                    (<= v0x7f80ac841cd0_0 v0x7f80ac841790_0)
                    (>= v0x7f80ac841cd0_0 v0x7f80ac841790_0))
               (and v0x7f80ac840810_0
                    E0x7f80ac842050
                    (not v0x7f80ac840a90_0)
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac83f090_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac83f090_0))
                    (and (<= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)
                         (>= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)))
               (and v0x7f80ac8418d0_0
                    E0x7f80ac8422d0
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac841190_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac841190_0))
                    (and (<= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)
                         (>= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)))
               (and v0x7f80ac840bd0_0
                    E0x7f80ac8424d0
                    (not v0x7f80ac8412d0_0)
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac841190_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac841190_0))
                    (<= v0x7f80ac841cd0_0 0.0)
                    (>= v0x7f80ac841cd0_0 0.0))))
      (a!6 (=> v0x7f80ac841b50_0
               (or (and E0x7f80ac841d90
                        (not E0x7f80ac842050)
                        (not E0x7f80ac8422d0)
                        (not E0x7f80ac8424d0))
                   (and E0x7f80ac842050
                        (not E0x7f80ac841d90)
                        (not E0x7f80ac8422d0)
                        (not E0x7f80ac8424d0))
                   (and E0x7f80ac8422d0
                        (not E0x7f80ac841d90)
                        (not E0x7f80ac842050)
                        (not E0x7f80ac8424d0))
                   (and E0x7f80ac8424d0
                        (not E0x7f80ac841d90)
                        (not E0x7f80ac842050)
                        (not E0x7f80ac8422d0))))))
(let ((a!7 (and (=> v0x7f80ac83e890_0
                    (and v0x7f80ac83e5d0_0
                         E0x7f80ac83e950
                         (not v0x7f80ac83e750_0)))
                (=> v0x7f80ac83e890_0 E0x7f80ac83e950)
                a!1
                a!2
                (=> v0x7f80ac83f950_0
                    (and v0x7f80ac83efd0_0
                         E0x7f80ac83fa10
                         (not v0x7f80ac83f810_0)))
                (=> v0x7f80ac83f950_0 E0x7f80ac83fa10)
                a!3
                a!4
                (=> v0x7f80ac840810_0
                    (and v0x7f80ac83fe90_0 E0x7f80ac8408d0 v0x7f80ac8406d0_0))
                (=> v0x7f80ac840810_0 E0x7f80ac8408d0)
                (=> v0x7f80ac840bd0_0
                    (and v0x7f80ac83fe90_0
                         E0x7f80ac840c90
                         (not v0x7f80ac8406d0_0)))
                (=> v0x7f80ac840bd0_0 E0x7f80ac840c90)
                (=> v0x7f80ac841410_0
                    (and v0x7f80ac840810_0 E0x7f80ac8414d0 v0x7f80ac840a90_0))
                (=> v0x7f80ac841410_0 E0x7f80ac8414d0)
                (=> v0x7f80ac8418d0_0
                    (and v0x7f80ac840bd0_0 E0x7f80ac841990 v0x7f80ac8412d0_0))
                (=> v0x7f80ac8418d0_0 E0x7f80ac841990)
                (=> v0x7f80ac841b50_0 a!5)
                a!6
                v0x7f80ac841b50_0
                (not v0x7f80ac842f50_0)
                (<= v0x7f80ac83dfd0_0 v0x7f80ac841cd0_0)
                (>= v0x7f80ac83dfd0_0 v0x7f80ac841cd0_0)
                (<= v0x7f80ac83e0d0_0 v0x7f80ac83ff50_0)
                (>= v0x7f80ac83e0d0_0 v0x7f80ac83ff50_0)
                (<= v0x7f80ac83c010_0 v0x7f80ac841c10_0)
                (>= v0x7f80ac83c010_0 v0x7f80ac841c10_0)
                (= v0x7f80ac83e750_0 (= v0x7f80ac83e690_0 0.0))
                (= v0x7f80ac83eb90_0 (< v0x7f80ac83e010_0 2.0))
                (= v0x7f80ac83ed50_0 (ite v0x7f80ac83eb90_0 1.0 0.0))
                (= v0x7f80ac83ee90_0 (+ v0x7f80ac83ed50_0 v0x7f80ac83e010_0))
                (= v0x7f80ac83f810_0 (= v0x7f80ac83f750_0 0.0))
                (= v0x7f80ac83fc10_0 (= v0x7f80ac83df10_0 0.0))
                (= v0x7f80ac83fd50_0 (ite v0x7f80ac83fc10_0 1.0 0.0))
                (= v0x7f80ac8406d0_0 (= v0x7f80ac83dd90_0 0.0))
                (= v0x7f80ac840a90_0 (> v0x7f80ac83f090_0 1.0))
                (= v0x7f80ac840e90_0 (> v0x7f80ac83f090_0 0.0))
                (= v0x7f80ac840fd0_0 (+ v0x7f80ac83f090_0 (- 1.0)))
                (= v0x7f80ac841190_0
                   (ite v0x7f80ac840e90_0 v0x7f80ac840fd0_0 v0x7f80ac83f090_0))
                (= v0x7f80ac8412d0_0 (= v0x7f80ac83ff50_0 0.0))
                (= v0x7f80ac841690_0 (= v0x7f80ac83ff50_0 0.0))
                (= v0x7f80ac841790_0
                   (ite v0x7f80ac841690_0 1.0 v0x7f80ac83dd90_0))
                (= v0x7f80ac842a90_0 (= v0x7f80ac83ff50_0 0.0))
                (= v0x7f80ac842b90_0 (= v0x7f80ac841c10_0 2.0))
                (= v0x7f80ac842cd0_0 (= v0x7f80ac841cd0_0 0.0))
                (= v0x7f80ac842e10_0 (and v0x7f80ac842b90_0 v0x7f80ac842a90_0))
                (= v0x7f80ac842f50_0 (and v0x7f80ac842e10_0 v0x7f80ac842cd0_0)))))
  (=> F0x7f80ac843bd0 a!7))))
(assert (=> F0x7f80ac843bd0 F0x7f80ac843c90))
(assert (let ((a!1 (=> v0x7f80ac83efd0_0
               (or (and v0x7f80ac83e890_0
                        E0x7f80ac83f150
                        (<= v0x7f80ac83f090_0 v0x7f80ac83ee90_0)
                        (>= v0x7f80ac83f090_0 v0x7f80ac83ee90_0))
                   (and v0x7f80ac83e5d0_0
                        E0x7f80ac83f310
                        v0x7f80ac83e750_0
                        (<= v0x7f80ac83f090_0 v0x7f80ac83e010_0)
                        (>= v0x7f80ac83f090_0 v0x7f80ac83e010_0)))))
      (a!2 (=> v0x7f80ac83efd0_0
               (or (and E0x7f80ac83f150 (not E0x7f80ac83f310))
                   (and E0x7f80ac83f310 (not E0x7f80ac83f150)))))
      (a!3 (=> v0x7f80ac83fe90_0
               (or (and v0x7f80ac83f950_0
                        E0x7f80ac840010
                        (<= v0x7f80ac83ff50_0 v0x7f80ac83fd50_0)
                        (>= v0x7f80ac83ff50_0 v0x7f80ac83fd50_0))
                   (and v0x7f80ac83efd0_0
                        E0x7f80ac8401d0
                        v0x7f80ac83f810_0
                        (<= v0x7f80ac83ff50_0 v0x7f80ac83df10_0)
                        (>= v0x7f80ac83ff50_0 v0x7f80ac83df10_0)))))
      (a!4 (=> v0x7f80ac83fe90_0
               (or (and E0x7f80ac840010 (not E0x7f80ac8401d0))
                   (and E0x7f80ac8401d0 (not E0x7f80ac840010)))))
      (a!5 (or (and v0x7f80ac841410_0
                    E0x7f80ac841d90
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac83f090_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac83f090_0))
                    (<= v0x7f80ac841cd0_0 v0x7f80ac841790_0)
                    (>= v0x7f80ac841cd0_0 v0x7f80ac841790_0))
               (and v0x7f80ac840810_0
                    E0x7f80ac842050
                    (not v0x7f80ac840a90_0)
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac83f090_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac83f090_0))
                    (and (<= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)
                         (>= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)))
               (and v0x7f80ac8418d0_0
                    E0x7f80ac8422d0
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac841190_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac841190_0))
                    (and (<= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)
                         (>= v0x7f80ac841cd0_0 v0x7f80ac83dd90_0)))
               (and v0x7f80ac840bd0_0
                    E0x7f80ac8424d0
                    (not v0x7f80ac8412d0_0)
                    (and (<= v0x7f80ac841c10_0 v0x7f80ac841190_0)
                         (>= v0x7f80ac841c10_0 v0x7f80ac841190_0))
                    (<= v0x7f80ac841cd0_0 0.0)
                    (>= v0x7f80ac841cd0_0 0.0))))
      (a!6 (=> v0x7f80ac841b50_0
               (or (and E0x7f80ac841d90
                        (not E0x7f80ac842050)
                        (not E0x7f80ac8422d0)
                        (not E0x7f80ac8424d0))
                   (and E0x7f80ac842050
                        (not E0x7f80ac841d90)
                        (not E0x7f80ac8422d0)
                        (not E0x7f80ac8424d0))
                   (and E0x7f80ac8422d0
                        (not E0x7f80ac841d90)
                        (not E0x7f80ac842050)
                        (not E0x7f80ac8424d0))
                   (and E0x7f80ac8424d0
                        (not E0x7f80ac841d90)
                        (not E0x7f80ac842050)
                        (not E0x7f80ac8422d0))))))
(let ((a!7 (and (=> v0x7f80ac83e890_0
                    (and v0x7f80ac83e5d0_0
                         E0x7f80ac83e950
                         (not v0x7f80ac83e750_0)))
                (=> v0x7f80ac83e890_0 E0x7f80ac83e950)
                a!1
                a!2
                (=> v0x7f80ac83f950_0
                    (and v0x7f80ac83efd0_0
                         E0x7f80ac83fa10
                         (not v0x7f80ac83f810_0)))
                (=> v0x7f80ac83f950_0 E0x7f80ac83fa10)
                a!3
                a!4
                (=> v0x7f80ac840810_0
                    (and v0x7f80ac83fe90_0 E0x7f80ac8408d0 v0x7f80ac8406d0_0))
                (=> v0x7f80ac840810_0 E0x7f80ac8408d0)
                (=> v0x7f80ac840bd0_0
                    (and v0x7f80ac83fe90_0
                         E0x7f80ac840c90
                         (not v0x7f80ac8406d0_0)))
                (=> v0x7f80ac840bd0_0 E0x7f80ac840c90)
                (=> v0x7f80ac841410_0
                    (and v0x7f80ac840810_0 E0x7f80ac8414d0 v0x7f80ac840a90_0))
                (=> v0x7f80ac841410_0 E0x7f80ac8414d0)
                (=> v0x7f80ac8418d0_0
                    (and v0x7f80ac840bd0_0 E0x7f80ac841990 v0x7f80ac8412d0_0))
                (=> v0x7f80ac8418d0_0 E0x7f80ac841990)
                (=> v0x7f80ac841b50_0 a!5)
                a!6
                v0x7f80ac841b50_0
                v0x7f80ac842f50_0
                (= v0x7f80ac83e750_0 (= v0x7f80ac83e690_0 0.0))
                (= v0x7f80ac83eb90_0 (< v0x7f80ac83e010_0 2.0))
                (= v0x7f80ac83ed50_0 (ite v0x7f80ac83eb90_0 1.0 0.0))
                (= v0x7f80ac83ee90_0 (+ v0x7f80ac83ed50_0 v0x7f80ac83e010_0))
                (= v0x7f80ac83f810_0 (= v0x7f80ac83f750_0 0.0))
                (= v0x7f80ac83fc10_0 (= v0x7f80ac83df10_0 0.0))
                (= v0x7f80ac83fd50_0 (ite v0x7f80ac83fc10_0 1.0 0.0))
                (= v0x7f80ac8406d0_0 (= v0x7f80ac83dd90_0 0.0))
                (= v0x7f80ac840a90_0 (> v0x7f80ac83f090_0 1.0))
                (= v0x7f80ac840e90_0 (> v0x7f80ac83f090_0 0.0))
                (= v0x7f80ac840fd0_0 (+ v0x7f80ac83f090_0 (- 1.0)))
                (= v0x7f80ac841190_0
                   (ite v0x7f80ac840e90_0 v0x7f80ac840fd0_0 v0x7f80ac83f090_0))
                (= v0x7f80ac8412d0_0 (= v0x7f80ac83ff50_0 0.0))
                (= v0x7f80ac841690_0 (= v0x7f80ac83ff50_0 0.0))
                (= v0x7f80ac841790_0
                   (ite v0x7f80ac841690_0 1.0 v0x7f80ac83dd90_0))
                (= v0x7f80ac842a90_0 (= v0x7f80ac83ff50_0 0.0))
                (= v0x7f80ac842b90_0 (= v0x7f80ac841c10_0 2.0))
                (= v0x7f80ac842cd0_0 (= v0x7f80ac841cd0_0 0.0))
                (= v0x7f80ac842e10_0 (and v0x7f80ac842b90_0 v0x7f80ac842a90_0))
                (= v0x7f80ac842f50_0 (and v0x7f80ac842e10_0 v0x7f80ac842cd0_0)))))
  (=> F0x7f80ac843d50 a!7))))
(assert (=> F0x7f80ac843d50 F0x7f80ac843c90))
(assert (=> F0x7f80ac843e90 (or F0x7f80ac843a50 F0x7f80ac843bd0)))
(assert (=> F0x7f80ac843e50 F0x7f80ac843d50))
(assert (=> pre!entry!0 (=> F0x7f80ac843b10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f80ac843c90 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f80ac843e90 true)))
(assert (= lemma!bb2.i.i29.i.i!0 (=> F0x7f80ac843e50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i29.i.i!0) (not lemma!bb2.i.i29.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f80ac843b10)
; (error: F0x7f80ac843e50)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i29.i.i!0)
