(declare-fun post!bb1.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i34.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7fc7ab4c5ad0_0 () Bool)
(declare-fun v0x7fc7ab4c4690_0 () Bool)
(declare-fun v0x7fc7ab4c3fd0_0 () Real)
(declare-fun v0x7fc7ab4c3e90_0 () Bool)
(declare-fun v0x7fc7ab4c2c10_0 () Bool)
(declare-fun v0x7fc7ab4c2750_0 () Real)
(declare-fun v0x7fc7ab4c1d50_0 () Real)
(declare-fun v0x7fc7ab4c5d10_0 () Bool)
(declare-fun v0x7fc7ab4c4190_0 () Real)
(declare-fun E0x7fc7ab4c5090 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fc7ab4c47d0_0 () Real)
(declare-fun v0x7fc7ab4c1b90_0 () Bool)
(declare-fun v0x7fc7ab4c4d10_0 () Real)
(declare-fun E0x7fc7ab4c4dd0 () Bool)
(declare-fun E0x7fc7ab4c49d0 () Bool)
(declare-fun v0x7fc7ab4c3bd0_0 () Bool)
(declare-fun v0x7fc7ab4c0f10_0 () Real)
(declare-fun E0x7fc7ab4c31d0 () Bool)
(declare-fun F0x7fc7ab4c6c50 () Bool)
(declare-fun v0x7fc7ab4c3a90_0 () Bool)
(declare-fun E0x7fc7ab4c3c90 () Bool)
(declare-fun v0x7fc7ab4c2d50_0 () Real)
(declare-fun v0x7fc7ab4c4c50_0 () Real)
(declare-fun v0x7fc7ab4c2950_0 () Bool)
(declare-fun v0x7fc7ab4c4410_0 () Bool)
(declare-fun v0x7fc7ab4c36d0_0 () Bool)
(declare-fun v0x7fc7ab4c1010_0 () Real)
(declare-fun F0x7fc7ab4c6a50 () Bool)
(declare-fun F0x7fc7ab4c6c10 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fc7ab4c2090_0 () Real)
(declare-fun v0x7fc7ab4c1fd0_0 () Bool)
(declare-fun E0x7fc7ab4c2a10 () Bool)
(declare-fun E0x7fc7ab4c5310 () Bool)
(declare-fun E0x7fc7ab4c2150 () Bool)
(declare-fun F0x7fc7ab4c6b10 () Bool)
(declare-fun E0x7fc7ab4c1950 () Bool)
(declare-fun v0x7fc7ab4c2e90_0 () Bool)
(declare-fun v0x7fc7ab4c1e90_0 () Real)
(declare-fun v0x7fc7ab4c1890_0 () Bool)
(declare-fun v0x7fc7ab4c3810_0 () Bool)
(declare-fun E0x7fc7ab4c2310 () Bool)
(declare-fun v0x7fc7ab4c4910_0 () Bool)
(declare-fun E0x7fc7ab4c38d0 () Bool)
(declare-fun F0x7fc7ab4c6990 () Bool)
(declare-fun v0x7fc7ab4c15d0_0 () Bool)
(declare-fun F0x7fc7ab4c6890 () Bool)
(declare-fun v0x7fc7ab4c42d0_0 () Bool)
(declare-fun E0x7fc7ab4c5510 () Bool)
(declare-fun v0x7fc7ab4c1690_0 () Real)
(declare-fun E0x7fc7ab4c3010 () Bool)
(declare-fun v0x7fc7ab4c2810_0 () Bool)
(declare-fun v0x7fc7ab4c1750_0 () Bool)
(declare-fun v0x7fc7ab4c5bd0_0 () Bool)
(declare-fun v0x7fc7ab4c0fd0_0 () Real)
(declare-fun v0x7fc7ab4c2f50_0 () Real)
(declare-fun v0x7fc7ab4c4b90_0 () Bool)
(declare-fun v0x7fc7ab4bf110_0 () Bool)
(declare-fun F0x7fc7ab4c6950 () Bool)
(declare-fun v0x7fc7ab4bf010_0 () Real)
(declare-fun E0x7fc7ab4c44d0 () Bool)
(declare-fun v0x7fc7ab4c0d90_0 () Real)
(declare-fun v0x7fc7ab4c10d0_0 () Real)

(assert (=> F0x7fc7ab4c6950
    (and v0x7fc7ab4bf110_0
         (<= v0x7fc7ab4c0fd0_0 0.0)
         (>= v0x7fc7ab4c0fd0_0 0.0)
         (<= v0x7fc7ab4c10d0_0 0.0)
         (>= v0x7fc7ab4c10d0_0 0.0)
         (<= v0x7fc7ab4bf010_0 1.0)
         (>= v0x7fc7ab4bf010_0 1.0))))
(assert (=> F0x7fc7ab4c6950 F0x7fc7ab4c6890))
(assert (let ((a!1 (=> v0x7fc7ab4c1fd0_0
               (or (and v0x7fc7ab4c1890_0
                        E0x7fc7ab4c2150
                        (<= v0x7fc7ab4c2090_0 v0x7fc7ab4c1e90_0)
                        (>= v0x7fc7ab4c2090_0 v0x7fc7ab4c1e90_0))
                   (and v0x7fc7ab4c15d0_0
                        E0x7fc7ab4c2310
                        v0x7fc7ab4c1750_0
                        (<= v0x7fc7ab4c2090_0 v0x7fc7ab4c1010_0)
                        (>= v0x7fc7ab4c2090_0 v0x7fc7ab4c1010_0)))))
      (a!2 (=> v0x7fc7ab4c1fd0_0
               (or (and E0x7fc7ab4c2150 (not E0x7fc7ab4c2310))
                   (and E0x7fc7ab4c2310 (not E0x7fc7ab4c2150)))))
      (a!3 (=> v0x7fc7ab4c2e90_0
               (or (and v0x7fc7ab4c2950_0
                        E0x7fc7ab4c3010
                        (<= v0x7fc7ab4c2f50_0 v0x7fc7ab4c2d50_0)
                        (>= v0x7fc7ab4c2f50_0 v0x7fc7ab4c2d50_0))
                   (and v0x7fc7ab4c1fd0_0
                        E0x7fc7ab4c31d0
                        v0x7fc7ab4c2810_0
                        (<= v0x7fc7ab4c2f50_0 v0x7fc7ab4c0f10_0)
                        (>= v0x7fc7ab4c2f50_0 v0x7fc7ab4c0f10_0)))))
      (a!4 (=> v0x7fc7ab4c2e90_0
               (or (and E0x7fc7ab4c3010 (not E0x7fc7ab4c31d0))
                   (and E0x7fc7ab4c31d0 (not E0x7fc7ab4c3010)))))
      (a!5 (or (and v0x7fc7ab4c4410_0
                    E0x7fc7ab4c4dd0
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0))
                    (<= v0x7fc7ab4c4d10_0 v0x7fc7ab4c47d0_0)
                    (>= v0x7fc7ab4c4d10_0 v0x7fc7ab4c47d0_0))
               (and v0x7fc7ab4c3810_0
                    E0x7fc7ab4c5090
                    (not v0x7fc7ab4c3a90_0)
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0))
                    (and (<= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)
                         (>= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)))
               (and v0x7fc7ab4c4910_0
                    E0x7fc7ab4c5310
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0))
                    (and (<= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)
                         (>= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)))
               (and v0x7fc7ab4c3bd0_0
                    E0x7fc7ab4c5510
                    (not v0x7fc7ab4c42d0_0)
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0))
                    (<= v0x7fc7ab4c4d10_0 0.0)
                    (>= v0x7fc7ab4c4d10_0 0.0))))
      (a!6 (=> v0x7fc7ab4c4b90_0
               (or (and E0x7fc7ab4c4dd0
                        (not E0x7fc7ab4c5090)
                        (not E0x7fc7ab4c5310)
                        (not E0x7fc7ab4c5510))
                   (and E0x7fc7ab4c5090
                        (not E0x7fc7ab4c4dd0)
                        (not E0x7fc7ab4c5310)
                        (not E0x7fc7ab4c5510))
                   (and E0x7fc7ab4c5310
                        (not E0x7fc7ab4c4dd0)
                        (not E0x7fc7ab4c5090)
                        (not E0x7fc7ab4c5510))
                   (and E0x7fc7ab4c5510
                        (not E0x7fc7ab4c4dd0)
                        (not E0x7fc7ab4c5090)
                        (not E0x7fc7ab4c5310))))))
(let ((a!7 (and (=> v0x7fc7ab4c1890_0
                    (and v0x7fc7ab4c15d0_0
                         E0x7fc7ab4c1950
                         (not v0x7fc7ab4c1750_0)))
                (=> v0x7fc7ab4c1890_0 E0x7fc7ab4c1950)
                a!1
                a!2
                (=> v0x7fc7ab4c2950_0
                    (and v0x7fc7ab4c1fd0_0
                         E0x7fc7ab4c2a10
                         (not v0x7fc7ab4c2810_0)))
                (=> v0x7fc7ab4c2950_0 E0x7fc7ab4c2a10)
                a!3
                a!4
                (=> v0x7fc7ab4c3810_0
                    (and v0x7fc7ab4c2e90_0 E0x7fc7ab4c38d0 v0x7fc7ab4c36d0_0))
                (=> v0x7fc7ab4c3810_0 E0x7fc7ab4c38d0)
                (=> v0x7fc7ab4c3bd0_0
                    (and v0x7fc7ab4c2e90_0
                         E0x7fc7ab4c3c90
                         (not v0x7fc7ab4c36d0_0)))
                (=> v0x7fc7ab4c3bd0_0 E0x7fc7ab4c3c90)
                (=> v0x7fc7ab4c4410_0
                    (and v0x7fc7ab4c3810_0 E0x7fc7ab4c44d0 v0x7fc7ab4c3a90_0))
                (=> v0x7fc7ab4c4410_0 E0x7fc7ab4c44d0)
                (=> v0x7fc7ab4c4910_0
                    (and v0x7fc7ab4c3bd0_0 E0x7fc7ab4c49d0 v0x7fc7ab4c42d0_0))
                (=> v0x7fc7ab4c4910_0 E0x7fc7ab4c49d0)
                (=> v0x7fc7ab4c4b90_0 a!5)
                a!6
                v0x7fc7ab4c4b90_0
                v0x7fc7ab4c5d10_0
                (<= v0x7fc7ab4c0fd0_0 v0x7fc7ab4c4d10_0)
                (>= v0x7fc7ab4c0fd0_0 v0x7fc7ab4c4d10_0)
                (<= v0x7fc7ab4c10d0_0 v0x7fc7ab4c2f50_0)
                (>= v0x7fc7ab4c10d0_0 v0x7fc7ab4c2f50_0)
                (<= v0x7fc7ab4bf010_0 v0x7fc7ab4c4c50_0)
                (>= v0x7fc7ab4bf010_0 v0x7fc7ab4c4c50_0)
                (= v0x7fc7ab4c1750_0 (= v0x7fc7ab4c1690_0 0.0))
                (= v0x7fc7ab4c1b90_0 (< v0x7fc7ab4c1010_0 2.0))
                (= v0x7fc7ab4c1d50_0 (ite v0x7fc7ab4c1b90_0 1.0 0.0))
                (= v0x7fc7ab4c1e90_0 (+ v0x7fc7ab4c1d50_0 v0x7fc7ab4c1010_0))
                (= v0x7fc7ab4c2810_0 (= v0x7fc7ab4c2750_0 0.0))
                (= v0x7fc7ab4c2c10_0 (= v0x7fc7ab4c0f10_0 0.0))
                (= v0x7fc7ab4c2d50_0 (ite v0x7fc7ab4c2c10_0 1.0 0.0))
                (= v0x7fc7ab4c36d0_0 (= v0x7fc7ab4c0d90_0 0.0))
                (= v0x7fc7ab4c3a90_0 (> v0x7fc7ab4c2090_0 1.0))
                (= v0x7fc7ab4c3e90_0 (> v0x7fc7ab4c2090_0 0.0))
                (= v0x7fc7ab4c3fd0_0 (+ v0x7fc7ab4c2090_0 (- 1.0)))
                (= v0x7fc7ab4c4190_0
                   (ite v0x7fc7ab4c3e90_0 v0x7fc7ab4c3fd0_0 v0x7fc7ab4c2090_0))
                (= v0x7fc7ab4c42d0_0 (= v0x7fc7ab4c4190_0 0.0))
                (= v0x7fc7ab4c4690_0 (= v0x7fc7ab4c2f50_0 0.0))
                (= v0x7fc7ab4c47d0_0
                   (ite v0x7fc7ab4c4690_0 1.0 v0x7fc7ab4c0d90_0))
                (= v0x7fc7ab4c5ad0_0 (= v0x7fc7ab4c2f50_0 0.0))
                (= v0x7fc7ab4c5bd0_0 (= v0x7fc7ab4c4d10_0 0.0))
                (= v0x7fc7ab4c5d10_0 (or v0x7fc7ab4c5bd0_0 v0x7fc7ab4c5ad0_0)))))
  (=> F0x7fc7ab4c6990 a!7))))
(assert (=> F0x7fc7ab4c6990 F0x7fc7ab4c6a50))
(assert (let ((a!1 (=> v0x7fc7ab4c1fd0_0
               (or (and v0x7fc7ab4c1890_0
                        E0x7fc7ab4c2150
                        (<= v0x7fc7ab4c2090_0 v0x7fc7ab4c1e90_0)
                        (>= v0x7fc7ab4c2090_0 v0x7fc7ab4c1e90_0))
                   (and v0x7fc7ab4c15d0_0
                        E0x7fc7ab4c2310
                        v0x7fc7ab4c1750_0
                        (<= v0x7fc7ab4c2090_0 v0x7fc7ab4c1010_0)
                        (>= v0x7fc7ab4c2090_0 v0x7fc7ab4c1010_0)))))
      (a!2 (=> v0x7fc7ab4c1fd0_0
               (or (and E0x7fc7ab4c2150 (not E0x7fc7ab4c2310))
                   (and E0x7fc7ab4c2310 (not E0x7fc7ab4c2150)))))
      (a!3 (=> v0x7fc7ab4c2e90_0
               (or (and v0x7fc7ab4c2950_0
                        E0x7fc7ab4c3010
                        (<= v0x7fc7ab4c2f50_0 v0x7fc7ab4c2d50_0)
                        (>= v0x7fc7ab4c2f50_0 v0x7fc7ab4c2d50_0))
                   (and v0x7fc7ab4c1fd0_0
                        E0x7fc7ab4c31d0
                        v0x7fc7ab4c2810_0
                        (<= v0x7fc7ab4c2f50_0 v0x7fc7ab4c0f10_0)
                        (>= v0x7fc7ab4c2f50_0 v0x7fc7ab4c0f10_0)))))
      (a!4 (=> v0x7fc7ab4c2e90_0
               (or (and E0x7fc7ab4c3010 (not E0x7fc7ab4c31d0))
                   (and E0x7fc7ab4c31d0 (not E0x7fc7ab4c3010)))))
      (a!5 (or (and v0x7fc7ab4c4410_0
                    E0x7fc7ab4c4dd0
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0))
                    (<= v0x7fc7ab4c4d10_0 v0x7fc7ab4c47d0_0)
                    (>= v0x7fc7ab4c4d10_0 v0x7fc7ab4c47d0_0))
               (and v0x7fc7ab4c3810_0
                    E0x7fc7ab4c5090
                    (not v0x7fc7ab4c3a90_0)
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c2090_0))
                    (and (<= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)
                         (>= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)))
               (and v0x7fc7ab4c4910_0
                    E0x7fc7ab4c5310
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0))
                    (and (<= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)
                         (>= v0x7fc7ab4c4d10_0 v0x7fc7ab4c0d90_0)))
               (and v0x7fc7ab4c3bd0_0
                    E0x7fc7ab4c5510
                    (not v0x7fc7ab4c42d0_0)
                    (and (<= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0)
                         (>= v0x7fc7ab4c4c50_0 v0x7fc7ab4c4190_0))
                    (<= v0x7fc7ab4c4d10_0 0.0)
                    (>= v0x7fc7ab4c4d10_0 0.0))))
      (a!6 (=> v0x7fc7ab4c4b90_0
               (or (and E0x7fc7ab4c4dd0
                        (not E0x7fc7ab4c5090)
                        (not E0x7fc7ab4c5310)
                        (not E0x7fc7ab4c5510))
                   (and E0x7fc7ab4c5090
                        (not E0x7fc7ab4c4dd0)
                        (not E0x7fc7ab4c5310)
                        (not E0x7fc7ab4c5510))
                   (and E0x7fc7ab4c5310
                        (not E0x7fc7ab4c4dd0)
                        (not E0x7fc7ab4c5090)
                        (not E0x7fc7ab4c5510))
                   (and E0x7fc7ab4c5510
                        (not E0x7fc7ab4c4dd0)
                        (not E0x7fc7ab4c5090)
                        (not E0x7fc7ab4c5310))))))
(let ((a!7 (and (=> v0x7fc7ab4c1890_0
                    (and v0x7fc7ab4c15d0_0
                         E0x7fc7ab4c1950
                         (not v0x7fc7ab4c1750_0)))
                (=> v0x7fc7ab4c1890_0 E0x7fc7ab4c1950)
                a!1
                a!2
                (=> v0x7fc7ab4c2950_0
                    (and v0x7fc7ab4c1fd0_0
                         E0x7fc7ab4c2a10
                         (not v0x7fc7ab4c2810_0)))
                (=> v0x7fc7ab4c2950_0 E0x7fc7ab4c2a10)
                a!3
                a!4
                (=> v0x7fc7ab4c3810_0
                    (and v0x7fc7ab4c2e90_0 E0x7fc7ab4c38d0 v0x7fc7ab4c36d0_0))
                (=> v0x7fc7ab4c3810_0 E0x7fc7ab4c38d0)
                (=> v0x7fc7ab4c3bd0_0
                    (and v0x7fc7ab4c2e90_0
                         E0x7fc7ab4c3c90
                         (not v0x7fc7ab4c36d0_0)))
                (=> v0x7fc7ab4c3bd0_0 E0x7fc7ab4c3c90)
                (=> v0x7fc7ab4c4410_0
                    (and v0x7fc7ab4c3810_0 E0x7fc7ab4c44d0 v0x7fc7ab4c3a90_0))
                (=> v0x7fc7ab4c4410_0 E0x7fc7ab4c44d0)
                (=> v0x7fc7ab4c4910_0
                    (and v0x7fc7ab4c3bd0_0 E0x7fc7ab4c49d0 v0x7fc7ab4c42d0_0))
                (=> v0x7fc7ab4c4910_0 E0x7fc7ab4c49d0)
                (=> v0x7fc7ab4c4b90_0 a!5)
                a!6
                v0x7fc7ab4c4b90_0
                (not v0x7fc7ab4c5d10_0)
                (= v0x7fc7ab4c1750_0 (= v0x7fc7ab4c1690_0 0.0))
                (= v0x7fc7ab4c1b90_0 (< v0x7fc7ab4c1010_0 2.0))
                (= v0x7fc7ab4c1d50_0 (ite v0x7fc7ab4c1b90_0 1.0 0.0))
                (= v0x7fc7ab4c1e90_0 (+ v0x7fc7ab4c1d50_0 v0x7fc7ab4c1010_0))
                (= v0x7fc7ab4c2810_0 (= v0x7fc7ab4c2750_0 0.0))
                (= v0x7fc7ab4c2c10_0 (= v0x7fc7ab4c0f10_0 0.0))
                (= v0x7fc7ab4c2d50_0 (ite v0x7fc7ab4c2c10_0 1.0 0.0))
                (= v0x7fc7ab4c36d0_0 (= v0x7fc7ab4c0d90_0 0.0))
                (= v0x7fc7ab4c3a90_0 (> v0x7fc7ab4c2090_0 1.0))
                (= v0x7fc7ab4c3e90_0 (> v0x7fc7ab4c2090_0 0.0))
                (= v0x7fc7ab4c3fd0_0 (+ v0x7fc7ab4c2090_0 (- 1.0)))
                (= v0x7fc7ab4c4190_0
                   (ite v0x7fc7ab4c3e90_0 v0x7fc7ab4c3fd0_0 v0x7fc7ab4c2090_0))
                (= v0x7fc7ab4c42d0_0 (= v0x7fc7ab4c4190_0 0.0))
                (= v0x7fc7ab4c4690_0 (= v0x7fc7ab4c2f50_0 0.0))
                (= v0x7fc7ab4c47d0_0
                   (ite v0x7fc7ab4c4690_0 1.0 v0x7fc7ab4c0d90_0))
                (= v0x7fc7ab4c5ad0_0 (= v0x7fc7ab4c2f50_0 0.0))
                (= v0x7fc7ab4c5bd0_0 (= v0x7fc7ab4c4d10_0 0.0))
                (= v0x7fc7ab4c5d10_0 (or v0x7fc7ab4c5bd0_0 v0x7fc7ab4c5ad0_0)))))
  (=> F0x7fc7ab4c6b10 a!7))))
(assert (=> F0x7fc7ab4c6b10 F0x7fc7ab4c6a50))
(assert (=> F0x7fc7ab4c6c50 (or F0x7fc7ab4c6950 F0x7fc7ab4c6990)))
(assert (=> F0x7fc7ab4c6c10 F0x7fc7ab4c6b10))
(assert (=> pre!entry!0 (=> F0x7fc7ab4c6890 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fc7ab4c6a50 (>= v0x7fc7ab4c0d90_0 0.0))))
(assert (let ((a!1 (=> F0x7fc7ab4c6a50
               (or (<= v0x7fc7ab4c0d90_0 0.0) (not (<= v0x7fc7ab4c1010_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fc7ab4c6c50 (>= v0x7fc7ab4c0fd0_0 0.0))))
(assert (let ((a!1 (=> F0x7fc7ab4c6c50
               (or (<= v0x7fc7ab4c0fd0_0 0.0) (not (<= v0x7fc7ab4bf010_0 1.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i34.i.i!0 (=> F0x7fc7ab4c6c10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i34.i.i!0) (not lemma!bb1.i.i34.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7fc7ab4c6890)
; (error: F0x7fc7ab4c6c10)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i34.i.i!0)
