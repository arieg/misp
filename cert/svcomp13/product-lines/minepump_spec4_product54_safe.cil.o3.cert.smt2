(declare-fun post!bb1.i.i35.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i35.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fea75a28950 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun F0x7fea75a28990 () Bool)
(declare-fun v0x7fea75a27910_0 () Bool)
(declare-fun v0x7fea75a277d0_0 () Bool)
(declare-fun v0x7fea75a26490_0 () Bool)
(declare-fun v0x7fea75a26190_0 () Real)
(declare-fun v0x7fea75a26050_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7fea75a247d0_0 () Real)
(declare-fun v0x7fea75a23090_0 () Real)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7fea75a26350_0 () Real)
(declare-fun v0x7fea75a26b90_0 () Real)
(declare-fun v0x7fea75a26c50_0 () Real)
(declare-fun v0x7fea75a25d90_0 () Bool)
(declare-fun v0x7fea75a25890_0 () Bool)
(declare-fun F0x7fea75a28850 () Bool)
(declare-fun v0x7fea75a25750_0 () Bool)
(declare-fun v0x7fea75a26710_0 () Bool)
(declare-fun v0x7fea75a22e10_0 () Real)
(declare-fun v0x7fea75a24fd0_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7fea75a24f10_0 () Bool)
(declare-fun E0x7fea75a27290 () Bool)
(declare-fun E0x7fea75a25e50 () Bool)
(declare-fun v0x7fea75a26ad0_0 () Bool)
(declare-fun v0x7fea75a249d0_0 () Bool)
(declare-fun E0x7fea75a25250 () Bool)
(declare-fun v0x7fea75a22f90_0 () Real)
(declare-fun v0x7fea75a26850_0 () Bool)
(declare-fun v0x7fea75a23f10_0 () Real)
(declare-fun v0x7fea75a25c50_0 () Real)
(declare-fun v0x7fea75a24c90_0 () Bool)
(declare-fun E0x7fea75a24390 () Bool)
(declare-fun E0x7fea75a241d0 () Bool)
(declare-fun v0x7fea75a24110_0 () Real)
(declare-fun v0x7fea75a25b10_0 () Bool)
(declare-fun E0x7fea75a24a90 () Bool)
(declare-fun E0x7fea75a239d0 () Bool)
(declare-fun E0x7fea75a26fd0 () Bool)
(declare-fun v0x7fea75a23710_0 () Real)
(declare-fun v0x7fea75a23650_0 () Bool)
(declare-fun E0x7fea75a26910 () Bool)
(declare-fun v0x7fea75a24890_0 () Bool)
(declare-fun E0x7fea75a26d10 () Bool)
(declare-fun v0x7fea75a23910_0 () Bool)
(declare-fun F0x7fea75a286d0 () Bool)
(declare-fun v0x7fea75a20010_0 () Real)
(declare-fun v0x7fea75a23150_0 () Real)
(declare-fun v0x7fea75a27a50_0 () Bool)
(declare-fun v0x7fea75a23c10_0 () Bool)
(declare-fun v0x7fea75a23dd0_0 () Real)
(declare-fun v0x7fea75a24dd0_0 () Real)
(declare-fun v0x7fea75a23050_0 () Real)
(declare-fun v0x7fea75a237d0_0 () Bool)
(declare-fun v0x7fea75a265d0_0 () Bool)
(declare-fun F0x7fea75a28790 () Bool)
(declare-fun v0x7fea75a20110_0 () Bool)
(declare-fun v0x7fea75a24050_0 () Bool)
(declare-fun F0x7fea75a285d0 () Bool)
(declare-fun F0x7fea75a28690 () Bool)
(declare-fun E0x7fea75a25950 () Bool)
(declare-fun E0x7fea75a25090 () Bool)

(assert (=> F0x7fea75a28690
    (and v0x7fea75a20110_0
         (<= v0x7fea75a23050_0 0.0)
         (>= v0x7fea75a23050_0 0.0)
         (<= v0x7fea75a23150_0 1.0)
         (>= v0x7fea75a23150_0 1.0)
         (<= v0x7fea75a20010_0 0.0)
         (>= v0x7fea75a20010_0 0.0))))
(assert (=> F0x7fea75a28690 F0x7fea75a285d0))
(assert (let ((a!1 (=> v0x7fea75a24050_0
               (or (and v0x7fea75a23910_0
                        E0x7fea75a241d0
                        (<= v0x7fea75a24110_0 v0x7fea75a23f10_0)
                        (>= v0x7fea75a24110_0 v0x7fea75a23f10_0))
                   (and v0x7fea75a23650_0
                        E0x7fea75a24390
                        v0x7fea75a237d0_0
                        (<= v0x7fea75a24110_0 v0x7fea75a22f90_0)
                        (>= v0x7fea75a24110_0 v0x7fea75a22f90_0)))))
      (a!2 (=> v0x7fea75a24050_0
               (or (and E0x7fea75a241d0 (not E0x7fea75a24390))
                   (and E0x7fea75a24390 (not E0x7fea75a241d0)))))
      (a!3 (=> v0x7fea75a24f10_0
               (or (and v0x7fea75a249d0_0
                        E0x7fea75a25090
                        (<= v0x7fea75a24fd0_0 v0x7fea75a24dd0_0)
                        (>= v0x7fea75a24fd0_0 v0x7fea75a24dd0_0))
                   (and v0x7fea75a24050_0
                        E0x7fea75a25250
                        v0x7fea75a24890_0
                        (<= v0x7fea75a24fd0_0 v0x7fea75a22e10_0)
                        (>= v0x7fea75a24fd0_0 v0x7fea75a22e10_0)))))
      (a!4 (=> v0x7fea75a24f10_0
               (or (and E0x7fea75a25090 (not E0x7fea75a25250))
                   (and E0x7fea75a25250 (not E0x7fea75a25090)))))
      (a!5 (or (and v0x7fea75a25890_0
                    E0x7fea75a26d10
                    (<= v0x7fea75a26b90_0 v0x7fea75a24110_0)
                    (>= v0x7fea75a26b90_0 v0x7fea75a24110_0)
                    (<= v0x7fea75a26c50_0 v0x7fea75a25c50_0)
                    (>= v0x7fea75a26c50_0 v0x7fea75a25c50_0))
               (and v0x7fea75a26850_0
                    E0x7fea75a26fd0
                    (and (<= v0x7fea75a26b90_0 v0x7fea75a26350_0)
                         (>= v0x7fea75a26b90_0 v0x7fea75a26350_0))
                    (<= v0x7fea75a26c50_0 v0x7fea75a23090_0)
                    (>= v0x7fea75a26c50_0 v0x7fea75a23090_0))
               (and v0x7fea75a25d90_0
                    E0x7fea75a27290
                    (not v0x7fea75a26710_0)
                    (and (<= v0x7fea75a26b90_0 v0x7fea75a26350_0)
                         (>= v0x7fea75a26b90_0 v0x7fea75a26350_0))
                    (<= v0x7fea75a26c50_0 0.0)
                    (>= v0x7fea75a26c50_0 0.0))))
      (a!6 (=> v0x7fea75a26ad0_0
               (or (and E0x7fea75a26d10
                        (not E0x7fea75a26fd0)
                        (not E0x7fea75a27290))
                   (and E0x7fea75a26fd0
                        (not E0x7fea75a26d10)
                        (not E0x7fea75a27290))
                   (and E0x7fea75a27290
                        (not E0x7fea75a26d10)
                        (not E0x7fea75a26fd0))))))
(let ((a!7 (and (=> v0x7fea75a23910_0
                    (and v0x7fea75a23650_0
                         E0x7fea75a239d0
                         (not v0x7fea75a237d0_0)))
                (=> v0x7fea75a23910_0 E0x7fea75a239d0)
                a!1
                a!2
                (=> v0x7fea75a249d0_0
                    (and v0x7fea75a24050_0
                         E0x7fea75a24a90
                         (not v0x7fea75a24890_0)))
                (=> v0x7fea75a249d0_0 E0x7fea75a24a90)
                a!3
                a!4
                (=> v0x7fea75a25890_0
                    (and v0x7fea75a24f10_0 E0x7fea75a25950 v0x7fea75a25750_0))
                (=> v0x7fea75a25890_0 E0x7fea75a25950)
                (=> v0x7fea75a25d90_0
                    (and v0x7fea75a24f10_0
                         E0x7fea75a25e50
                         (not v0x7fea75a25750_0)))
                (=> v0x7fea75a25d90_0 E0x7fea75a25e50)
                (=> v0x7fea75a26850_0
                    (and v0x7fea75a25d90_0 E0x7fea75a26910 v0x7fea75a26710_0))
                (=> v0x7fea75a26850_0 E0x7fea75a26910)
                (=> v0x7fea75a26ad0_0 a!5)
                a!6
                v0x7fea75a26ad0_0
                v0x7fea75a27a50_0
                (<= v0x7fea75a23050_0 v0x7fea75a24fd0_0)
                (>= v0x7fea75a23050_0 v0x7fea75a24fd0_0)
                (<= v0x7fea75a23150_0 v0x7fea75a26b90_0)
                (>= v0x7fea75a23150_0 v0x7fea75a26b90_0)
                (<= v0x7fea75a20010_0 v0x7fea75a26c50_0)
                (>= v0x7fea75a20010_0 v0x7fea75a26c50_0)
                (= v0x7fea75a237d0_0 (= v0x7fea75a23710_0 0.0))
                (= v0x7fea75a23c10_0 (< v0x7fea75a22f90_0 2.0))
                (= v0x7fea75a23dd0_0 (ite v0x7fea75a23c10_0 1.0 0.0))
                (= v0x7fea75a23f10_0 (+ v0x7fea75a23dd0_0 v0x7fea75a22f90_0))
                (= v0x7fea75a24890_0 (= v0x7fea75a247d0_0 0.0))
                (= v0x7fea75a24c90_0 (= v0x7fea75a22e10_0 0.0))
                (= v0x7fea75a24dd0_0 (ite v0x7fea75a24c90_0 1.0 0.0))
                (= v0x7fea75a25750_0 (= v0x7fea75a23090_0 0.0))
                (= v0x7fea75a25b10_0 (> v0x7fea75a24110_0 1.0))
                (= v0x7fea75a25c50_0
                   (ite v0x7fea75a25b10_0 1.0 v0x7fea75a23090_0))
                (= v0x7fea75a26050_0 (> v0x7fea75a24110_0 0.0))
                (= v0x7fea75a26190_0 (+ v0x7fea75a24110_0 (- 1.0)))
                (= v0x7fea75a26350_0
                   (ite v0x7fea75a26050_0 v0x7fea75a26190_0 v0x7fea75a24110_0))
                (= v0x7fea75a26490_0 (= v0x7fea75a24fd0_0 0.0))
                (= v0x7fea75a265d0_0 (= v0x7fea75a26350_0 0.0))
                (= v0x7fea75a26710_0 (and v0x7fea75a26490_0 v0x7fea75a265d0_0))
                (= v0x7fea75a277d0_0 (not (= v0x7fea75a26b90_0 0.0)))
                (= v0x7fea75a27910_0 (= v0x7fea75a26c50_0 0.0))
                (= v0x7fea75a27a50_0 (or v0x7fea75a27910_0 v0x7fea75a277d0_0)))))
  (=> F0x7fea75a286d0 a!7))))
(assert (=> F0x7fea75a286d0 F0x7fea75a28790))
(assert (let ((a!1 (=> v0x7fea75a24050_0
               (or (and v0x7fea75a23910_0
                        E0x7fea75a241d0
                        (<= v0x7fea75a24110_0 v0x7fea75a23f10_0)
                        (>= v0x7fea75a24110_0 v0x7fea75a23f10_0))
                   (and v0x7fea75a23650_0
                        E0x7fea75a24390
                        v0x7fea75a237d0_0
                        (<= v0x7fea75a24110_0 v0x7fea75a22f90_0)
                        (>= v0x7fea75a24110_0 v0x7fea75a22f90_0)))))
      (a!2 (=> v0x7fea75a24050_0
               (or (and E0x7fea75a241d0 (not E0x7fea75a24390))
                   (and E0x7fea75a24390 (not E0x7fea75a241d0)))))
      (a!3 (=> v0x7fea75a24f10_0
               (or (and v0x7fea75a249d0_0
                        E0x7fea75a25090
                        (<= v0x7fea75a24fd0_0 v0x7fea75a24dd0_0)
                        (>= v0x7fea75a24fd0_0 v0x7fea75a24dd0_0))
                   (and v0x7fea75a24050_0
                        E0x7fea75a25250
                        v0x7fea75a24890_0
                        (<= v0x7fea75a24fd0_0 v0x7fea75a22e10_0)
                        (>= v0x7fea75a24fd0_0 v0x7fea75a22e10_0)))))
      (a!4 (=> v0x7fea75a24f10_0
               (or (and E0x7fea75a25090 (not E0x7fea75a25250))
                   (and E0x7fea75a25250 (not E0x7fea75a25090)))))
      (a!5 (or (and v0x7fea75a25890_0
                    E0x7fea75a26d10
                    (<= v0x7fea75a26b90_0 v0x7fea75a24110_0)
                    (>= v0x7fea75a26b90_0 v0x7fea75a24110_0)
                    (<= v0x7fea75a26c50_0 v0x7fea75a25c50_0)
                    (>= v0x7fea75a26c50_0 v0x7fea75a25c50_0))
               (and v0x7fea75a26850_0
                    E0x7fea75a26fd0
                    (and (<= v0x7fea75a26b90_0 v0x7fea75a26350_0)
                         (>= v0x7fea75a26b90_0 v0x7fea75a26350_0))
                    (<= v0x7fea75a26c50_0 v0x7fea75a23090_0)
                    (>= v0x7fea75a26c50_0 v0x7fea75a23090_0))
               (and v0x7fea75a25d90_0
                    E0x7fea75a27290
                    (not v0x7fea75a26710_0)
                    (and (<= v0x7fea75a26b90_0 v0x7fea75a26350_0)
                         (>= v0x7fea75a26b90_0 v0x7fea75a26350_0))
                    (<= v0x7fea75a26c50_0 0.0)
                    (>= v0x7fea75a26c50_0 0.0))))
      (a!6 (=> v0x7fea75a26ad0_0
               (or (and E0x7fea75a26d10
                        (not E0x7fea75a26fd0)
                        (not E0x7fea75a27290))
                   (and E0x7fea75a26fd0
                        (not E0x7fea75a26d10)
                        (not E0x7fea75a27290))
                   (and E0x7fea75a27290
                        (not E0x7fea75a26d10)
                        (not E0x7fea75a26fd0))))))
(let ((a!7 (and (=> v0x7fea75a23910_0
                    (and v0x7fea75a23650_0
                         E0x7fea75a239d0
                         (not v0x7fea75a237d0_0)))
                (=> v0x7fea75a23910_0 E0x7fea75a239d0)
                a!1
                a!2
                (=> v0x7fea75a249d0_0
                    (and v0x7fea75a24050_0
                         E0x7fea75a24a90
                         (not v0x7fea75a24890_0)))
                (=> v0x7fea75a249d0_0 E0x7fea75a24a90)
                a!3
                a!4
                (=> v0x7fea75a25890_0
                    (and v0x7fea75a24f10_0 E0x7fea75a25950 v0x7fea75a25750_0))
                (=> v0x7fea75a25890_0 E0x7fea75a25950)
                (=> v0x7fea75a25d90_0
                    (and v0x7fea75a24f10_0
                         E0x7fea75a25e50
                         (not v0x7fea75a25750_0)))
                (=> v0x7fea75a25d90_0 E0x7fea75a25e50)
                (=> v0x7fea75a26850_0
                    (and v0x7fea75a25d90_0 E0x7fea75a26910 v0x7fea75a26710_0))
                (=> v0x7fea75a26850_0 E0x7fea75a26910)
                (=> v0x7fea75a26ad0_0 a!5)
                a!6
                v0x7fea75a26ad0_0
                (not v0x7fea75a27a50_0)
                (= v0x7fea75a237d0_0 (= v0x7fea75a23710_0 0.0))
                (= v0x7fea75a23c10_0 (< v0x7fea75a22f90_0 2.0))
                (= v0x7fea75a23dd0_0 (ite v0x7fea75a23c10_0 1.0 0.0))
                (= v0x7fea75a23f10_0 (+ v0x7fea75a23dd0_0 v0x7fea75a22f90_0))
                (= v0x7fea75a24890_0 (= v0x7fea75a247d0_0 0.0))
                (= v0x7fea75a24c90_0 (= v0x7fea75a22e10_0 0.0))
                (= v0x7fea75a24dd0_0 (ite v0x7fea75a24c90_0 1.0 0.0))
                (= v0x7fea75a25750_0 (= v0x7fea75a23090_0 0.0))
                (= v0x7fea75a25b10_0 (> v0x7fea75a24110_0 1.0))
                (= v0x7fea75a25c50_0
                   (ite v0x7fea75a25b10_0 1.0 v0x7fea75a23090_0))
                (= v0x7fea75a26050_0 (> v0x7fea75a24110_0 0.0))
                (= v0x7fea75a26190_0 (+ v0x7fea75a24110_0 (- 1.0)))
                (= v0x7fea75a26350_0
                   (ite v0x7fea75a26050_0 v0x7fea75a26190_0 v0x7fea75a24110_0))
                (= v0x7fea75a26490_0 (= v0x7fea75a24fd0_0 0.0))
                (= v0x7fea75a265d0_0 (= v0x7fea75a26350_0 0.0))
                (= v0x7fea75a26710_0 (and v0x7fea75a26490_0 v0x7fea75a265d0_0))
                (= v0x7fea75a277d0_0 (not (= v0x7fea75a26b90_0 0.0)))
                (= v0x7fea75a27910_0 (= v0x7fea75a26c50_0 0.0))
                (= v0x7fea75a27a50_0 (or v0x7fea75a27910_0 v0x7fea75a277d0_0)))))
  (=> F0x7fea75a28850 a!7))))
(assert (=> F0x7fea75a28850 F0x7fea75a28790))
(assert (=> F0x7fea75a28990 (or F0x7fea75a28690 F0x7fea75a286d0)))
(assert (=> F0x7fea75a28950 F0x7fea75a28850))
(assert (=> pre!entry!0 (=> F0x7fea75a285d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fea75a28790 (>= v0x7fea75a23090_0 0.0))))
(assert (let ((a!1 (=> F0x7fea75a28790
               (or (<= v0x7fea75a23090_0 0.0) (not (<= v0x7fea75a22f90_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fea75a28990 (>= v0x7fea75a20010_0 0.0))))
(assert (let ((a!1 (=> F0x7fea75a28990
               (or (<= v0x7fea75a20010_0 0.0) (not (<= v0x7fea75a23150_0 1.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i35.i.i!0 (=> F0x7fea75a28950 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i35.i.i!0) (not lemma!bb1.i.i35.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7fea75a285d0)
; (error: F0x7fea75a28950)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i35.i.i!0)
