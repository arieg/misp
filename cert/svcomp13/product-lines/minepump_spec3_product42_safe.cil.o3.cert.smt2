(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fdec98c8f50 () Bool)
(declare-fun v0x7fdec98c7ed0_0 () Bool)
(declare-fun v0x7fdec98c7110_0 () Bool)
(declare-fun v0x7fdec98c6050_0 () Bool)
(declare-fun v0x7fdec98c5190_0 () Bool)
(declare-fun v0x7fdec98c4cd0_0 () Real)
(declare-fun v0x7fdec98c6190_0 () Real)
(declare-fun v0x7fdec98c7d90_0 () Bool)
(declare-fun v0x7fdec98c3c10_0 () Real)
(declare-fun E0x7fdec98c76d0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun E0x7fdec98c7510 () Bool)
(declare-fun v0x7fdec98c6e90_0 () Bool)
(declare-fun E0x7fdec98c67d0 () Bool)
(declare-fun v0x7fdec98c7b50_0 () Bool)
(declare-fun F0x7fdec98c8e10 () Bool)
(declare-fun v0x7fdec98c6550_0 () Real)
(declare-fun E0x7fdec98c6610 () Bool)
(declare-fun v0x7fdec98c6490_0 () Bool)
(declare-fun v0x7fdec98c5c50_0 () Bool)
(declare-fun v0x7fdec98c7450_0 () Real)
(declare-fun v0x7fdec98c3310_0 () Real)
(declare-fun E0x7fdec98c5750 () Bool)
(declare-fun v0x7fdec98c6d50_0 () Bool)
(declare-fun v0x7fdec98c7390_0 () Bool)
(declare-fun E0x7fdec98c4f90 () Bool)
(declare-fun v0x7fdec98c4ed0_0 () Bool)
(declare-fun v0x7fdec98c3490_0 () Real)
(declare-fun v0x7fdec98c4550_0 () Bool)
(declare-fun v0x7fdec98c7250_0 () Real)
(declare-fun v0x7fdec98c54d0_0 () Real)
(declare-fun E0x7fdec98c3ed0 () Bool)
(declare-fun v0x7fdec98c3b50_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fdec98c3590_0 () Real)
(declare-fun v0x7fdec98c5d90_0 () Bool)
(declare-fun v0x7fdec98c3e10_0 () Bool)
(declare-fun v0x7fdec98c4610_0 () Real)
(declare-fun F0x7fdec98c8c90 () Bool)
(declare-fun E0x7fdec98c6f50 () Bool)
(declare-fun v0x7fdec98c4110_0 () Bool)
(declare-fun lemma!bb2.i.i21.i.i!0 () Bool)
(declare-fun F0x7fdec98c8bd0 () Bool)
(declare-fun v0x7fdec98c6c10_0 () Bool)
(declare-fun v0x7fdec98c5410_0 () Bool)
(declare-fun v0x7fdec98c3cd0_0 () Bool)
(declare-fun F0x7fdec98c8d50 () Bool)
(declare-fun v0x7fdec98c7c50_0 () Bool)
(declare-fun E0x7fdec98c5590 () Bool)
(declare-fun v0x7fdec98c8010_0 () Bool)
(declare-fun v0x7fdec98c2010_0 () Real)
(declare-fun E0x7fdec98c5e50 () Bool)
(declare-fun v0x7fdec98c42d0_0 () Real)
(declare-fun v0x7fdec98c3550_0 () Real)
(declare-fun v0x7fdec98c3650_0 () Real)
(declare-fun v0x7fdec98c4410_0 () Real)
(declare-fun v0x7fdec98c6350_0 () Real)
(declare-fun v0x7fdec98c2110_0 () Bool)
(declare-fun E0x7fdec98c46d0 () Bool)
(declare-fun post!bb2.i.i21.i.i!0 () Bool)
(declare-fun v0x7fdec98c52d0_0 () Real)
(declare-fun F0x7fdec98c8f10 () Bool)
(declare-fun E0x7fdec98c4890 () Bool)
(declare-fun v0x7fdec98c4d90_0 () Bool)
(declare-fun F0x7fdec98c8b10 () Bool)

(assert (=> F0x7fdec98c8b10
    (and v0x7fdec98c2110_0
         (<= v0x7fdec98c3550_0 0.0)
         (>= v0x7fdec98c3550_0 0.0)
         (<= v0x7fdec98c3650_0 1.0)
         (>= v0x7fdec98c3650_0 1.0)
         (<= v0x7fdec98c2010_0 0.0)
         (>= v0x7fdec98c2010_0 0.0))))
(assert (=> F0x7fdec98c8b10 F0x7fdec98c8bd0))
(assert (let ((a!1 (=> v0x7fdec98c4550_0
               (or (and v0x7fdec98c3e10_0
                        E0x7fdec98c46d0
                        (<= v0x7fdec98c4610_0 v0x7fdec98c4410_0)
                        (>= v0x7fdec98c4610_0 v0x7fdec98c4410_0))
                   (and v0x7fdec98c3b50_0
                        E0x7fdec98c4890
                        v0x7fdec98c3cd0_0
                        (<= v0x7fdec98c4610_0 v0x7fdec98c3490_0)
                        (>= v0x7fdec98c4610_0 v0x7fdec98c3490_0)))))
      (a!2 (=> v0x7fdec98c4550_0
               (or (and E0x7fdec98c46d0 (not E0x7fdec98c4890))
                   (and E0x7fdec98c4890 (not E0x7fdec98c46d0)))))
      (a!3 (=> v0x7fdec98c5410_0
               (or (and v0x7fdec98c4ed0_0
                        E0x7fdec98c5590
                        (<= v0x7fdec98c54d0_0 v0x7fdec98c52d0_0)
                        (>= v0x7fdec98c54d0_0 v0x7fdec98c52d0_0))
                   (and v0x7fdec98c4550_0
                        E0x7fdec98c5750
                        v0x7fdec98c4d90_0
                        (<= v0x7fdec98c54d0_0 v0x7fdec98c3310_0)
                        (>= v0x7fdec98c54d0_0 v0x7fdec98c3310_0)))))
      (a!4 (=> v0x7fdec98c5410_0
               (or (and E0x7fdec98c5590 (not E0x7fdec98c5750))
                   (and E0x7fdec98c5750 (not E0x7fdec98c5590)))))
      (a!5 (=> v0x7fdec98c6490_0
               (or (and v0x7fdec98c5d90_0
                        E0x7fdec98c6610
                        (<= v0x7fdec98c6550_0 v0x7fdec98c6350_0)
                        (>= v0x7fdec98c6550_0 v0x7fdec98c6350_0))
                   (and v0x7fdec98c5410_0
                        E0x7fdec98c67d0
                        v0x7fdec98c5c50_0
                        (<= v0x7fdec98c6550_0 v0x7fdec98c4610_0)
                        (>= v0x7fdec98c6550_0 v0x7fdec98c4610_0)))))
      (a!6 (=> v0x7fdec98c6490_0
               (or (and E0x7fdec98c6610 (not E0x7fdec98c67d0))
                   (and E0x7fdec98c67d0 (not E0x7fdec98c6610)))))
      (a!7 (=> v0x7fdec98c7390_0
               (or (and v0x7fdec98c6e90_0
                        E0x7fdec98c7510
                        (<= v0x7fdec98c7450_0 v0x7fdec98c7250_0)
                        (>= v0x7fdec98c7450_0 v0x7fdec98c7250_0))
                   (and v0x7fdec98c6490_0
                        E0x7fdec98c76d0
                        (not v0x7fdec98c6d50_0)
                        (<= v0x7fdec98c7450_0 v0x7fdec98c3590_0)
                        (>= v0x7fdec98c7450_0 v0x7fdec98c3590_0)))))
      (a!8 (=> v0x7fdec98c7390_0
               (or (and E0x7fdec98c7510 (not E0x7fdec98c76d0))
                   (and E0x7fdec98c76d0 (not E0x7fdec98c7510))))))
(let ((a!9 (and (=> v0x7fdec98c3e10_0
                    (and v0x7fdec98c3b50_0
                         E0x7fdec98c3ed0
                         (not v0x7fdec98c3cd0_0)))
                (=> v0x7fdec98c3e10_0 E0x7fdec98c3ed0)
                a!1
                a!2
                (=> v0x7fdec98c4ed0_0
                    (and v0x7fdec98c4550_0
                         E0x7fdec98c4f90
                         (not v0x7fdec98c4d90_0)))
                (=> v0x7fdec98c4ed0_0 E0x7fdec98c4f90)
                a!3
                a!4
                (=> v0x7fdec98c5d90_0
                    (and v0x7fdec98c5410_0
                         E0x7fdec98c5e50
                         (not v0x7fdec98c5c50_0)))
                (=> v0x7fdec98c5d90_0 E0x7fdec98c5e50)
                a!5
                a!6
                (=> v0x7fdec98c6e90_0
                    (and v0x7fdec98c6490_0 E0x7fdec98c6f50 v0x7fdec98c6d50_0))
                (=> v0x7fdec98c6e90_0 E0x7fdec98c6f50)
                a!7
                a!8
                v0x7fdec98c7390_0
                (not v0x7fdec98c8010_0)
                (<= v0x7fdec98c3550_0 v0x7fdec98c54d0_0)
                (>= v0x7fdec98c3550_0 v0x7fdec98c54d0_0)
                (<= v0x7fdec98c3650_0 v0x7fdec98c6550_0)
                (>= v0x7fdec98c3650_0 v0x7fdec98c6550_0)
                (<= v0x7fdec98c2010_0 v0x7fdec98c7450_0)
                (>= v0x7fdec98c2010_0 v0x7fdec98c7450_0)
                (= v0x7fdec98c3cd0_0 (= v0x7fdec98c3c10_0 0.0))
                (= v0x7fdec98c4110_0 (< v0x7fdec98c3490_0 2.0))
                (= v0x7fdec98c42d0_0 (ite v0x7fdec98c4110_0 1.0 0.0))
                (= v0x7fdec98c4410_0 (+ v0x7fdec98c42d0_0 v0x7fdec98c3490_0))
                (= v0x7fdec98c4d90_0 (= v0x7fdec98c4cd0_0 0.0))
                (= v0x7fdec98c5190_0 (= v0x7fdec98c3310_0 0.0))
                (= v0x7fdec98c52d0_0 (ite v0x7fdec98c5190_0 1.0 0.0))
                (= v0x7fdec98c5c50_0 (= v0x7fdec98c3590_0 0.0))
                (= v0x7fdec98c6050_0 (> v0x7fdec98c4610_0 0.0))
                (= v0x7fdec98c6190_0 (+ v0x7fdec98c4610_0 (- 1.0)))
                (= v0x7fdec98c6350_0
                   (ite v0x7fdec98c6050_0 v0x7fdec98c6190_0 v0x7fdec98c4610_0))
                (= v0x7fdec98c6c10_0 (> v0x7fdec98c6550_0 1.0))
                (= v0x7fdec98c6d50_0 (and v0x7fdec98c6c10_0 v0x7fdec98c5c50_0))
                (= v0x7fdec98c7110_0 (= v0x7fdec98c54d0_0 0.0))
                (= v0x7fdec98c7250_0
                   (ite v0x7fdec98c7110_0 1.0 v0x7fdec98c3590_0))
                (= v0x7fdec98c7b50_0 (= v0x7fdec98c54d0_0 0.0))
                (= v0x7fdec98c7c50_0 (= v0x7fdec98c6550_0 2.0))
                (= v0x7fdec98c7d90_0 (= v0x7fdec98c7450_0 0.0))
                (= v0x7fdec98c7ed0_0 (and v0x7fdec98c7c50_0 v0x7fdec98c7b50_0))
                (= v0x7fdec98c8010_0 (and v0x7fdec98c7ed0_0 v0x7fdec98c7d90_0)))))
  (=> F0x7fdec98c8c90 a!9))))
(assert (=> F0x7fdec98c8c90 F0x7fdec98c8d50))
(assert (let ((a!1 (=> v0x7fdec98c4550_0
               (or (and v0x7fdec98c3e10_0
                        E0x7fdec98c46d0
                        (<= v0x7fdec98c4610_0 v0x7fdec98c4410_0)
                        (>= v0x7fdec98c4610_0 v0x7fdec98c4410_0))
                   (and v0x7fdec98c3b50_0
                        E0x7fdec98c4890
                        v0x7fdec98c3cd0_0
                        (<= v0x7fdec98c4610_0 v0x7fdec98c3490_0)
                        (>= v0x7fdec98c4610_0 v0x7fdec98c3490_0)))))
      (a!2 (=> v0x7fdec98c4550_0
               (or (and E0x7fdec98c46d0 (not E0x7fdec98c4890))
                   (and E0x7fdec98c4890 (not E0x7fdec98c46d0)))))
      (a!3 (=> v0x7fdec98c5410_0
               (or (and v0x7fdec98c4ed0_0
                        E0x7fdec98c5590
                        (<= v0x7fdec98c54d0_0 v0x7fdec98c52d0_0)
                        (>= v0x7fdec98c54d0_0 v0x7fdec98c52d0_0))
                   (and v0x7fdec98c4550_0
                        E0x7fdec98c5750
                        v0x7fdec98c4d90_0
                        (<= v0x7fdec98c54d0_0 v0x7fdec98c3310_0)
                        (>= v0x7fdec98c54d0_0 v0x7fdec98c3310_0)))))
      (a!4 (=> v0x7fdec98c5410_0
               (or (and E0x7fdec98c5590 (not E0x7fdec98c5750))
                   (and E0x7fdec98c5750 (not E0x7fdec98c5590)))))
      (a!5 (=> v0x7fdec98c6490_0
               (or (and v0x7fdec98c5d90_0
                        E0x7fdec98c6610
                        (<= v0x7fdec98c6550_0 v0x7fdec98c6350_0)
                        (>= v0x7fdec98c6550_0 v0x7fdec98c6350_0))
                   (and v0x7fdec98c5410_0
                        E0x7fdec98c67d0
                        v0x7fdec98c5c50_0
                        (<= v0x7fdec98c6550_0 v0x7fdec98c4610_0)
                        (>= v0x7fdec98c6550_0 v0x7fdec98c4610_0)))))
      (a!6 (=> v0x7fdec98c6490_0
               (or (and E0x7fdec98c6610 (not E0x7fdec98c67d0))
                   (and E0x7fdec98c67d0 (not E0x7fdec98c6610)))))
      (a!7 (=> v0x7fdec98c7390_0
               (or (and v0x7fdec98c6e90_0
                        E0x7fdec98c7510
                        (<= v0x7fdec98c7450_0 v0x7fdec98c7250_0)
                        (>= v0x7fdec98c7450_0 v0x7fdec98c7250_0))
                   (and v0x7fdec98c6490_0
                        E0x7fdec98c76d0
                        (not v0x7fdec98c6d50_0)
                        (<= v0x7fdec98c7450_0 v0x7fdec98c3590_0)
                        (>= v0x7fdec98c7450_0 v0x7fdec98c3590_0)))))
      (a!8 (=> v0x7fdec98c7390_0
               (or (and E0x7fdec98c7510 (not E0x7fdec98c76d0))
                   (and E0x7fdec98c76d0 (not E0x7fdec98c7510))))))
(let ((a!9 (and (=> v0x7fdec98c3e10_0
                    (and v0x7fdec98c3b50_0
                         E0x7fdec98c3ed0
                         (not v0x7fdec98c3cd0_0)))
                (=> v0x7fdec98c3e10_0 E0x7fdec98c3ed0)
                a!1
                a!2
                (=> v0x7fdec98c4ed0_0
                    (and v0x7fdec98c4550_0
                         E0x7fdec98c4f90
                         (not v0x7fdec98c4d90_0)))
                (=> v0x7fdec98c4ed0_0 E0x7fdec98c4f90)
                a!3
                a!4
                (=> v0x7fdec98c5d90_0
                    (and v0x7fdec98c5410_0
                         E0x7fdec98c5e50
                         (not v0x7fdec98c5c50_0)))
                (=> v0x7fdec98c5d90_0 E0x7fdec98c5e50)
                a!5
                a!6
                (=> v0x7fdec98c6e90_0
                    (and v0x7fdec98c6490_0 E0x7fdec98c6f50 v0x7fdec98c6d50_0))
                (=> v0x7fdec98c6e90_0 E0x7fdec98c6f50)
                a!7
                a!8
                v0x7fdec98c7390_0
                v0x7fdec98c8010_0
                (= v0x7fdec98c3cd0_0 (= v0x7fdec98c3c10_0 0.0))
                (= v0x7fdec98c4110_0 (< v0x7fdec98c3490_0 2.0))
                (= v0x7fdec98c42d0_0 (ite v0x7fdec98c4110_0 1.0 0.0))
                (= v0x7fdec98c4410_0 (+ v0x7fdec98c42d0_0 v0x7fdec98c3490_0))
                (= v0x7fdec98c4d90_0 (= v0x7fdec98c4cd0_0 0.0))
                (= v0x7fdec98c5190_0 (= v0x7fdec98c3310_0 0.0))
                (= v0x7fdec98c52d0_0 (ite v0x7fdec98c5190_0 1.0 0.0))
                (= v0x7fdec98c5c50_0 (= v0x7fdec98c3590_0 0.0))
                (= v0x7fdec98c6050_0 (> v0x7fdec98c4610_0 0.0))
                (= v0x7fdec98c6190_0 (+ v0x7fdec98c4610_0 (- 1.0)))
                (= v0x7fdec98c6350_0
                   (ite v0x7fdec98c6050_0 v0x7fdec98c6190_0 v0x7fdec98c4610_0))
                (= v0x7fdec98c6c10_0 (> v0x7fdec98c6550_0 1.0))
                (= v0x7fdec98c6d50_0 (and v0x7fdec98c6c10_0 v0x7fdec98c5c50_0))
                (= v0x7fdec98c7110_0 (= v0x7fdec98c54d0_0 0.0))
                (= v0x7fdec98c7250_0
                   (ite v0x7fdec98c7110_0 1.0 v0x7fdec98c3590_0))
                (= v0x7fdec98c7b50_0 (= v0x7fdec98c54d0_0 0.0))
                (= v0x7fdec98c7c50_0 (= v0x7fdec98c6550_0 2.0))
                (= v0x7fdec98c7d90_0 (= v0x7fdec98c7450_0 0.0))
                (= v0x7fdec98c7ed0_0 (and v0x7fdec98c7c50_0 v0x7fdec98c7b50_0))
                (= v0x7fdec98c8010_0 (and v0x7fdec98c7ed0_0 v0x7fdec98c7d90_0)))))
  (=> F0x7fdec98c8e10 a!9))))
(assert (=> F0x7fdec98c8e10 F0x7fdec98c8d50))
(assert (=> F0x7fdec98c8f50 (or F0x7fdec98c8b10 F0x7fdec98c8c90)))
(assert (=> F0x7fdec98c8f10 F0x7fdec98c8e10))
(assert (=> pre!entry!0 (=> F0x7fdec98c8bd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fdec98c8d50 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fdec98c8f50 true)))
(assert (= lemma!bb2.i.i21.i.i!0 (=> F0x7fdec98c8f10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i21.i.i!0) (not lemma!bb2.i.i21.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7fdec98c8bd0)
; (error: F0x7fdec98c8f10)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i21.i.i!0)
