(declare-fun post!bb2.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f725c404f10 () Bool)
(declare-fun F0x7f725c404d50 () Bool)
(declare-fun lemma!bb2.i.i43.i.i!0 () Bool)
(declare-fun v0x7f725c403850_0 () Bool)
(declare-fun v0x7f725c403750_0 () Bool)
(declare-fun v0x7f725c401e50_0 () Bool)
(declare-fun v0x7f725c401a10_0 () Real)
(declare-fun v0x7f725c3ff790_0 () Real)
(declare-fun v0x7f725c3ff5d0_0 () Bool)
(declare-fun v0x7f725c3ff0d0_0 () Real)
(declare-fun E0x7f725c403b90 () Bool)
(declare-fun v0x7f725c403d90_0 () Bool)
(declare-fun E0x7f725c402f90 () Bool)
(declare-fun v0x7f725c402450_0 () Real)
(declare-fun v0x7f725c402990_0 () Real)
(declare-fun v0x7f725c402810_0 () Bool)
(declare-fun v0x7f725c3fe590_0 () Real)
(declare-fun v0x7f725c401f90_0 () Bool)
(declare-fun v0x7f725c402590_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f725c4014d0_0 () Bool)
(declare-fun E0x7f725c402190 () Bool)
(declare-fun v0x7f725c4020d0_0 () Bool)
(declare-fun v0x7f725c400650_0 () Bool)
(declare-fun v0x7f725c401610_0 () Bool)
(declare-fun v0x7f725c401d10_0 () Bool)
(declare-fun v0x7f725c3fe710_0 () Real)
(declare-fun E0x7f725c402d10 () Bool)
(declare-fun E0x7f725c400a50 () Bool)
(declare-fun v0x7f725c402350_0 () Bool)
(declare-fun v0x7f725c400250_0 () Bool)
(declare-fun E0x7f725c400450 () Bool)
(declare-fun v0x7f725c400390_0 () Bool)
(declare-fun v0x7f725c3fe810_0 () Real)
(declare-fun F0x7f725c404e10 () Bool)
(declare-fun v0x7f725c3ff8d0_0 () Real)
(declare-fun E0x7f725c3ff390 () Bool)
(declare-fun v0x7f725c3fe910_0 () Real)
(declare-fun v0x7f725c401bd0_0 () Real)
(declare-fun v0x7f725c401110_0 () Bool)
(declare-fun v0x7f725c3ff2d0_0 () Bool)
(declare-fun E0x7f725c400c10 () Bool)
(declare-fun v0x7f725c3ffad0_0 () Real)
(declare-fun E0x7f725c402650 () Bool)
(declare-fun v0x7f725c4028d0_0 () Real)
(declare-fun F0x7f725c404bd0 () Bool)
(declare-fun E0x7f725c401310 () Bool)
(declare-fun F0x7f725c404f50 () Bool)
(declare-fun v0x7f725c3ff190_0 () Bool)
(declare-fun E0x7f725c402a50 () Bool)
(declare-fun E0x7f725c4016d0 () Bool)
(declare-fun v0x7f725c3fb010_0 () Real)
(declare-fun v0x7f725c401250_0 () Bool)
(declare-fun E0x7f725c3ffd50 () Bool)
(declare-fun v0x7f725c400790_0 () Real)
(declare-fun E0x7f725c3ffb90 () Bool)
(declare-fun v0x7f725c3fe9d0_0 () Real)
(declare-fun v0x7f725c3fe8d0_0 () Real)
(declare-fun v0x7f725c4008d0_0 () Bool)
(declare-fun E0x7f725c403190 () Bool)
(declare-fun v0x7f725c403ad0_0 () Bool)
(declare-fun v0x7f725c3ff010_0 () Bool)
(declare-fun v0x7f725c3fe7d0_0 () Real)
(declare-fun v0x7f725c3ffa10_0 () Bool)
(declare-fun v0x7f725c400990_0 () Real)
(declare-fun v0x7f725c400190_0 () Real)
(declare-fun F0x7f725c404c90 () Bool)
(declare-fun v0x7f725c403990_0 () Bool)
(declare-fun v0x7f725c3fb110_0 () Bool)
(declare-fun F0x7f725c404b10 () Bool)
(declare-fun v0x7f725c4018d0_0 () Bool)

(assert (=> F0x7f725c404b10
    (and v0x7f725c3fb110_0
         (<= v0x7f725c3fe7d0_0 0.0)
         (>= v0x7f725c3fe7d0_0 0.0)
         (<= v0x7f725c3fe8d0_0 0.0)
         (>= v0x7f725c3fe8d0_0 0.0)
         (<= v0x7f725c3fe9d0_0 1.0)
         (>= v0x7f725c3fe9d0_0 1.0)
         (<= v0x7f725c3fb010_0 0.0)
         (>= v0x7f725c3fb010_0 0.0))))
(assert (=> F0x7f725c404b10 F0x7f725c404bd0))
(assert (let ((a!1 (=> v0x7f725c3ffa10_0
               (or (and v0x7f725c3ff2d0_0
                        E0x7f725c3ffb90
                        (<= v0x7f725c3ffad0_0 v0x7f725c3ff8d0_0)
                        (>= v0x7f725c3ffad0_0 v0x7f725c3ff8d0_0))
                   (and v0x7f725c3ff010_0
                        E0x7f725c3ffd50
                        v0x7f725c3ff190_0
                        (<= v0x7f725c3ffad0_0 v0x7f725c3fe810_0)
                        (>= v0x7f725c3ffad0_0 v0x7f725c3fe810_0)))))
      (a!2 (=> v0x7f725c3ffa10_0
               (or (and E0x7f725c3ffb90 (not E0x7f725c3ffd50))
                   (and E0x7f725c3ffd50 (not E0x7f725c3ffb90)))))
      (a!3 (=> v0x7f725c4008d0_0
               (or (and v0x7f725c400390_0
                        E0x7f725c400a50
                        (<= v0x7f725c400990_0 v0x7f725c400790_0)
                        (>= v0x7f725c400990_0 v0x7f725c400790_0))
                   (and v0x7f725c3ffa10_0
                        E0x7f725c400c10
                        v0x7f725c400250_0
                        (<= v0x7f725c400990_0 v0x7f725c3fe710_0)
                        (>= v0x7f725c400990_0 v0x7f725c3fe710_0)))))
      (a!4 (=> v0x7f725c4008d0_0
               (or (and E0x7f725c400a50 (not E0x7f725c400c10))
                   (and E0x7f725c400c10 (not E0x7f725c400a50)))))
      (a!5 (or (and v0x7f725c4020d0_0
                    E0x7f725c402a50
                    (and (<= v0x7f725c4028d0_0 v0x7f725c3ffad0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c3ffad0_0))
                    (<= v0x7f725c402990_0 v0x7f725c402450_0)
                    (>= v0x7f725c402990_0 v0x7f725c402450_0))
               (and v0x7f725c401250_0
                    E0x7f725c402d10
                    (not v0x7f725c4014d0_0)
                    (and (<= v0x7f725c4028d0_0 v0x7f725c3ffad0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c3ffad0_0))
                    (and (<= v0x7f725c402990_0 v0x7f725c3fe590_0)
                         (>= v0x7f725c402990_0 v0x7f725c3fe590_0)))
               (and v0x7f725c402590_0
                    E0x7f725c402f90
                    (and (<= v0x7f725c4028d0_0 v0x7f725c401bd0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c401bd0_0))
                    (and (<= v0x7f725c402990_0 v0x7f725c3fe590_0)
                         (>= v0x7f725c402990_0 v0x7f725c3fe590_0)))
               (and v0x7f725c401610_0
                    E0x7f725c403190
                    (not v0x7f725c401f90_0)
                    (and (<= v0x7f725c4028d0_0 v0x7f725c401bd0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c401bd0_0))
                    (<= v0x7f725c402990_0 0.0)
                    (>= v0x7f725c402990_0 0.0))))
      (a!6 (=> v0x7f725c402810_0
               (or (and E0x7f725c402a50
                        (not E0x7f725c402d10)
                        (not E0x7f725c402f90)
                        (not E0x7f725c403190))
                   (and E0x7f725c402d10
                        (not E0x7f725c402a50)
                        (not E0x7f725c402f90)
                        (not E0x7f725c403190))
                   (and E0x7f725c402f90
                        (not E0x7f725c402a50)
                        (not E0x7f725c402d10)
                        (not E0x7f725c403190))
                   (and E0x7f725c403190
                        (not E0x7f725c402a50)
                        (not E0x7f725c402d10)
                        (not E0x7f725c402f90)))))
      (a!7 (or (and v0x7f725c403ad0_0
                    v0x7f725c403d90_0
                    (and (<= v0x7f725c3fe7d0_0 v0x7f725c402990_0)
                         (>= v0x7f725c3fe7d0_0 v0x7f725c402990_0))
                    (and (<= v0x7f725c3fe8d0_0 v0x7f725c400990_0)
                         (>= v0x7f725c3fe8d0_0 v0x7f725c400990_0))
                    (and (<= v0x7f725c3fe9d0_0 v0x7f725c4028d0_0)
                         (>= v0x7f725c3fe9d0_0 v0x7f725c4028d0_0))
                    (<= v0x7f725c3fb010_0 1.0)
                    (>= v0x7f725c3fb010_0 1.0))
               (and v0x7f725c402810_0
                    v0x7f725c403990_0
                    (and (<= v0x7f725c3fe7d0_0 v0x7f725c402990_0)
                         (>= v0x7f725c3fe7d0_0 v0x7f725c402990_0))
                    (and (<= v0x7f725c3fe8d0_0 v0x7f725c400990_0)
                         (>= v0x7f725c3fe8d0_0 v0x7f725c400990_0))
                    (and (<= v0x7f725c3fe9d0_0 v0x7f725c4028d0_0)
                         (>= v0x7f725c3fe9d0_0 v0x7f725c4028d0_0))
                    (<= v0x7f725c3fb010_0 0.0)
                    (>= v0x7f725c3fb010_0 0.0)))))
(let ((a!8 (and (=> v0x7f725c3ff2d0_0
                    (and v0x7f725c3ff010_0
                         E0x7f725c3ff390
                         (not v0x7f725c3ff190_0)))
                (=> v0x7f725c3ff2d0_0 E0x7f725c3ff390)
                a!1
                a!2
                (=> v0x7f725c400390_0
                    (and v0x7f725c3ffa10_0
                         E0x7f725c400450
                         (not v0x7f725c400250_0)))
                (=> v0x7f725c400390_0 E0x7f725c400450)
                a!3
                a!4
                (=> v0x7f725c401250_0
                    (and v0x7f725c4008d0_0 E0x7f725c401310 v0x7f725c401110_0))
                (=> v0x7f725c401250_0 E0x7f725c401310)
                (=> v0x7f725c401610_0
                    (and v0x7f725c4008d0_0
                         E0x7f725c4016d0
                         (not v0x7f725c401110_0)))
                (=> v0x7f725c401610_0 E0x7f725c4016d0)
                (=> v0x7f725c4020d0_0
                    (and v0x7f725c401250_0 E0x7f725c402190 v0x7f725c4014d0_0))
                (=> v0x7f725c4020d0_0 E0x7f725c402190)
                (=> v0x7f725c402590_0
                    (and v0x7f725c401610_0 E0x7f725c402650 v0x7f725c401f90_0))
                (=> v0x7f725c402590_0 E0x7f725c402650)
                (=> v0x7f725c402810_0 a!5)
                a!6
                (=> v0x7f725c403ad0_0
                    (and v0x7f725c402810_0
                         E0x7f725c403b90
                         (not v0x7f725c403990_0)))
                (=> v0x7f725c403ad0_0 E0x7f725c403b90)
                a!7
                (= v0x7f725c3ff190_0 (= v0x7f725c3ff0d0_0 0.0))
                (= v0x7f725c3ff5d0_0 (< v0x7f725c3fe810_0 2.0))
                (= v0x7f725c3ff790_0 (ite v0x7f725c3ff5d0_0 1.0 0.0))
                (= v0x7f725c3ff8d0_0 (+ v0x7f725c3ff790_0 v0x7f725c3fe810_0))
                (= v0x7f725c400250_0 (= v0x7f725c400190_0 0.0))
                (= v0x7f725c400650_0 (= v0x7f725c3fe710_0 0.0))
                (= v0x7f725c400790_0 (ite v0x7f725c400650_0 1.0 0.0))
                (= v0x7f725c401110_0 (= v0x7f725c3fe590_0 0.0))
                (= v0x7f725c4014d0_0 (> v0x7f725c3ffad0_0 1.0))
                (= v0x7f725c4018d0_0 (> v0x7f725c3ffad0_0 0.0))
                (= v0x7f725c401a10_0 (+ v0x7f725c3ffad0_0 (- 1.0)))
                (= v0x7f725c401bd0_0
                   (ite v0x7f725c4018d0_0 v0x7f725c401a10_0 v0x7f725c3ffad0_0))
                (= v0x7f725c401d10_0 (= v0x7f725c400990_0 0.0))
                (= v0x7f725c401e50_0 (= v0x7f725c401bd0_0 0.0))
                (= v0x7f725c401f90_0 (and v0x7f725c401d10_0 v0x7f725c401e50_0))
                (= v0x7f725c402350_0 (= v0x7f725c400990_0 0.0))
                (= v0x7f725c402450_0
                   (ite v0x7f725c402350_0 1.0 v0x7f725c3fe590_0))
                (= v0x7f725c403750_0 (= v0x7f725c400990_0 0.0))
                (= v0x7f725c403850_0 (= v0x7f725c402990_0 0.0))
                (= v0x7f725c403990_0 (or v0x7f725c403850_0 v0x7f725c403750_0))
                (= v0x7f725c403d90_0 (= v0x7f725c3fe910_0 0.0)))))
  (=> F0x7f725c404c90 a!8))))
(assert (=> F0x7f725c404c90 F0x7f725c404d50))
(assert (let ((a!1 (=> v0x7f725c3ffa10_0
               (or (and v0x7f725c3ff2d0_0
                        E0x7f725c3ffb90
                        (<= v0x7f725c3ffad0_0 v0x7f725c3ff8d0_0)
                        (>= v0x7f725c3ffad0_0 v0x7f725c3ff8d0_0))
                   (and v0x7f725c3ff010_0
                        E0x7f725c3ffd50
                        v0x7f725c3ff190_0
                        (<= v0x7f725c3ffad0_0 v0x7f725c3fe810_0)
                        (>= v0x7f725c3ffad0_0 v0x7f725c3fe810_0)))))
      (a!2 (=> v0x7f725c3ffa10_0
               (or (and E0x7f725c3ffb90 (not E0x7f725c3ffd50))
                   (and E0x7f725c3ffd50 (not E0x7f725c3ffb90)))))
      (a!3 (=> v0x7f725c4008d0_0
               (or (and v0x7f725c400390_0
                        E0x7f725c400a50
                        (<= v0x7f725c400990_0 v0x7f725c400790_0)
                        (>= v0x7f725c400990_0 v0x7f725c400790_0))
                   (and v0x7f725c3ffa10_0
                        E0x7f725c400c10
                        v0x7f725c400250_0
                        (<= v0x7f725c400990_0 v0x7f725c3fe710_0)
                        (>= v0x7f725c400990_0 v0x7f725c3fe710_0)))))
      (a!4 (=> v0x7f725c4008d0_0
               (or (and E0x7f725c400a50 (not E0x7f725c400c10))
                   (and E0x7f725c400c10 (not E0x7f725c400a50)))))
      (a!5 (or (and v0x7f725c4020d0_0
                    E0x7f725c402a50
                    (and (<= v0x7f725c4028d0_0 v0x7f725c3ffad0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c3ffad0_0))
                    (<= v0x7f725c402990_0 v0x7f725c402450_0)
                    (>= v0x7f725c402990_0 v0x7f725c402450_0))
               (and v0x7f725c401250_0
                    E0x7f725c402d10
                    (not v0x7f725c4014d0_0)
                    (and (<= v0x7f725c4028d0_0 v0x7f725c3ffad0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c3ffad0_0))
                    (and (<= v0x7f725c402990_0 v0x7f725c3fe590_0)
                         (>= v0x7f725c402990_0 v0x7f725c3fe590_0)))
               (and v0x7f725c402590_0
                    E0x7f725c402f90
                    (and (<= v0x7f725c4028d0_0 v0x7f725c401bd0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c401bd0_0))
                    (and (<= v0x7f725c402990_0 v0x7f725c3fe590_0)
                         (>= v0x7f725c402990_0 v0x7f725c3fe590_0)))
               (and v0x7f725c401610_0
                    E0x7f725c403190
                    (not v0x7f725c401f90_0)
                    (and (<= v0x7f725c4028d0_0 v0x7f725c401bd0_0)
                         (>= v0x7f725c4028d0_0 v0x7f725c401bd0_0))
                    (<= v0x7f725c402990_0 0.0)
                    (>= v0x7f725c402990_0 0.0))))
      (a!6 (=> v0x7f725c402810_0
               (or (and E0x7f725c402a50
                        (not E0x7f725c402d10)
                        (not E0x7f725c402f90)
                        (not E0x7f725c403190))
                   (and E0x7f725c402d10
                        (not E0x7f725c402a50)
                        (not E0x7f725c402f90)
                        (not E0x7f725c403190))
                   (and E0x7f725c402f90
                        (not E0x7f725c402a50)
                        (not E0x7f725c402d10)
                        (not E0x7f725c403190))
                   (and E0x7f725c403190
                        (not E0x7f725c402a50)
                        (not E0x7f725c402d10)
                        (not E0x7f725c402f90))))))
(let ((a!7 (and (=> v0x7f725c3ff2d0_0
                    (and v0x7f725c3ff010_0
                         E0x7f725c3ff390
                         (not v0x7f725c3ff190_0)))
                (=> v0x7f725c3ff2d0_0 E0x7f725c3ff390)
                a!1
                a!2
                (=> v0x7f725c400390_0
                    (and v0x7f725c3ffa10_0
                         E0x7f725c400450
                         (not v0x7f725c400250_0)))
                (=> v0x7f725c400390_0 E0x7f725c400450)
                a!3
                a!4
                (=> v0x7f725c401250_0
                    (and v0x7f725c4008d0_0 E0x7f725c401310 v0x7f725c401110_0))
                (=> v0x7f725c401250_0 E0x7f725c401310)
                (=> v0x7f725c401610_0
                    (and v0x7f725c4008d0_0
                         E0x7f725c4016d0
                         (not v0x7f725c401110_0)))
                (=> v0x7f725c401610_0 E0x7f725c4016d0)
                (=> v0x7f725c4020d0_0
                    (and v0x7f725c401250_0 E0x7f725c402190 v0x7f725c4014d0_0))
                (=> v0x7f725c4020d0_0 E0x7f725c402190)
                (=> v0x7f725c402590_0
                    (and v0x7f725c401610_0 E0x7f725c402650 v0x7f725c401f90_0))
                (=> v0x7f725c402590_0 E0x7f725c402650)
                (=> v0x7f725c402810_0 a!5)
                a!6
                (=> v0x7f725c403ad0_0
                    (and v0x7f725c402810_0
                         E0x7f725c403b90
                         (not v0x7f725c403990_0)))
                (=> v0x7f725c403ad0_0 E0x7f725c403b90)
                v0x7f725c403ad0_0
                (not v0x7f725c403d90_0)
                (= v0x7f725c3ff190_0 (= v0x7f725c3ff0d0_0 0.0))
                (= v0x7f725c3ff5d0_0 (< v0x7f725c3fe810_0 2.0))
                (= v0x7f725c3ff790_0 (ite v0x7f725c3ff5d0_0 1.0 0.0))
                (= v0x7f725c3ff8d0_0 (+ v0x7f725c3ff790_0 v0x7f725c3fe810_0))
                (= v0x7f725c400250_0 (= v0x7f725c400190_0 0.0))
                (= v0x7f725c400650_0 (= v0x7f725c3fe710_0 0.0))
                (= v0x7f725c400790_0 (ite v0x7f725c400650_0 1.0 0.0))
                (= v0x7f725c401110_0 (= v0x7f725c3fe590_0 0.0))
                (= v0x7f725c4014d0_0 (> v0x7f725c3ffad0_0 1.0))
                (= v0x7f725c4018d0_0 (> v0x7f725c3ffad0_0 0.0))
                (= v0x7f725c401a10_0 (+ v0x7f725c3ffad0_0 (- 1.0)))
                (= v0x7f725c401bd0_0
                   (ite v0x7f725c4018d0_0 v0x7f725c401a10_0 v0x7f725c3ffad0_0))
                (= v0x7f725c401d10_0 (= v0x7f725c400990_0 0.0))
                (= v0x7f725c401e50_0 (= v0x7f725c401bd0_0 0.0))
                (= v0x7f725c401f90_0 (and v0x7f725c401d10_0 v0x7f725c401e50_0))
                (= v0x7f725c402350_0 (= v0x7f725c400990_0 0.0))
                (= v0x7f725c402450_0
                   (ite v0x7f725c402350_0 1.0 v0x7f725c3fe590_0))
                (= v0x7f725c403750_0 (= v0x7f725c400990_0 0.0))
                (= v0x7f725c403850_0 (= v0x7f725c402990_0 0.0))
                (= v0x7f725c403990_0 (or v0x7f725c403850_0 v0x7f725c403750_0))
                (= v0x7f725c403d90_0 (= v0x7f725c3fe910_0 0.0)))))
  (=> F0x7f725c404e10 a!7))))
(assert (=> F0x7f725c404e10 F0x7f725c404d50))
(assert (=> F0x7f725c404f50 (or F0x7f725c404b10 F0x7f725c404c90)))
(assert (=> F0x7f725c404f10 F0x7f725c404e10))
(assert (=> pre!entry!0 (=> F0x7f725c404bd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f725c404d50 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f725c404f50 true)))
(assert (= lemma!bb2.i.i43.i.i!0 (=> F0x7f725c404f10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i43.i.i!0) (not lemma!bb2.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f725c404bd0)
; (error: F0x7f725c404f10)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i43.i.i!0)
