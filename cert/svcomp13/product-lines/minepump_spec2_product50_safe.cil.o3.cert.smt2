(declare-fun post!bb2.i.i26.i.i!0 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i26.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun F0x7fd9234cdd90 () Bool)
(declare-fun F0x7fd9234cd9d0 () Bool)
(declare-fun v0x7fd9234cc6d0_0 () Bool)
(declare-fun v0x7fd9234cb1d0_0 () Real)
(declare-fun post!bb1.i.i!5 () Bool)
(declare-fun v0x7fd9234c9cd0_0 () Bool)
(declare-fun v0x7fd9234c9810_0 () Real)
(declare-fun v0x7fd9234c8c50_0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun E0x7fd9234cca10 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun v0x7fd9234cc950_0 () Bool)
(declare-fun E0x7fd9234cc050 () Bool)
(declare-fun F0x7fd9234cddd0 () Bool)
(declare-fun v0x7fd9234c7c10_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7fd9234cbd90 () Bool)
(declare-fun v0x7fd9234cba10_0 () Real)
(declare-fun v0x7fd9234cab50_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fd9234cb950_0 () Real)
(declare-fun F0x7fd9234cdad0 () Bool)
(declare-fun E0x7fd9234cbad0 () Bool)
(declare-fun v0x7fd9234cb4d0_0 () Bool)
(declare-fun v0x7fd9234cc590_0 () Bool)
(declare-fun v0x7fd9234c7f90_0 () Real)
(declare-fun E0x7fd9234cb6d0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fd9234cb390_0 () Real)
(declare-fun v0x7fd9234cb610_0 () Bool)
(declare-fun E0x7fd9234cae90 () Bool)
(declare-fun v0x7fd9234cb890_0 () Bool)
(declare-fun E0x7fd9234ca990 () Bool)
(declare-fun v0x7fd9234c7d90_0 () Real)
(declare-fun v0x7fd9234c8750_0 () Real)
(declare-fun E0x7fd9234ca290 () Bool)
(declare-fun v0x7fd9234c9e10_0 () Real)
(declare-fun E0x7fd9234ca0d0 () Bool)
(declare-fun v0x7fd9234c9f50_0 () Bool)
(declare-fun v0x7fd9234cadd0_0 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun E0x7fd9234c9ad0 () Bool)
(declare-fun v0x7fd9234c8e10_0 () Real)
(declare-fun v0x7fd9234cac90_0 () Real)
(declare-fun E0x7fd9234c93d0 () Bool)
(declare-fun v0x7fd9234c8f50_0 () Real)
(declare-fun v0x7fd9234c9150_0 () Real)
(declare-fun E0x7fd9234c9210 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun v0x7fd9234c9090_0 () Bool)
(declare-fun v0x7fd9234c8810_0 () Bool)
(declare-fun v0x7fd9234cb090_0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7fd9234c98d0_0 () Bool)
(declare-fun F0x7fd9234cdb90 () Bool)
(declare-fun F0x7fd9234cdc10 () Bool)
(declare-fun v0x7fd9234ca790_0 () Bool)
(declare-fun v0x7fd9234c8950_0 () Bool)
(declare-fun v0x7fd9234c8690_0 () Bool)
(declare-fun E0x7fd9234c8a10 () Bool)
(declare-fun pre!bb1.i.i!5 () Bool)
(declare-fun v0x7fd9234c6010_0 () Real)
(declare-fun v0x7fd9234ccc10_0 () Bool)
(declare-fun lemma!bb1.i.i!5 () Bool)
(declare-fun v0x7fd9234ca010_0 () Real)
(declare-fun v0x7fd9234c8050_0 () Real)
(declare-fun v0x7fd9234c7f50_0 () Real)
(declare-fun v0x7fd9234c9a10_0 () Bool)
(declare-fun v0x7fd9234c7e50_0 () Real)
(declare-fun v0x7fd9234ca8d0_0 () Bool)
(declare-fun v0x7fd9234c6110_0 () Bool)
(declare-fun v0x7fd9234c7e90_0 () Real)
(declare-fun F0x7fd9234cdcd0 () Bool)
(declare-fun v0x7fd9234cc810_0 () Bool)

(assert (=> F0x7fd9234cdcd0
    (and v0x7fd9234c6110_0
         (<= v0x7fd9234c7e50_0 0.0)
         (>= v0x7fd9234c7e50_0 0.0)
         (<= v0x7fd9234c7f50_0 0.0)
         (>= v0x7fd9234c7f50_0 0.0)
         (<= v0x7fd9234c8050_0 1.0)
         (>= v0x7fd9234c8050_0 1.0)
         (<= v0x7fd9234c6010_0 0.0)
         (>= v0x7fd9234c6010_0 0.0))))
(assert (=> F0x7fd9234cdcd0 F0x7fd9234cdc10))
(assert (let ((a!1 (=> v0x7fd9234c9090_0
               (or (and v0x7fd9234c8950_0
                        E0x7fd9234c9210
                        (<= v0x7fd9234c9150_0 v0x7fd9234c8f50_0)
                        (>= v0x7fd9234c9150_0 v0x7fd9234c8f50_0))
                   (and v0x7fd9234c8690_0
                        E0x7fd9234c93d0
                        v0x7fd9234c8810_0
                        (<= v0x7fd9234c9150_0 v0x7fd9234c7e90_0)
                        (>= v0x7fd9234c9150_0 v0x7fd9234c7e90_0)))))
      (a!2 (=> v0x7fd9234c9090_0
               (or (and E0x7fd9234c9210 (not E0x7fd9234c93d0))
                   (and E0x7fd9234c93d0 (not E0x7fd9234c9210)))))
      (a!3 (=> v0x7fd9234c9f50_0
               (or (and v0x7fd9234c9a10_0
                        E0x7fd9234ca0d0
                        (<= v0x7fd9234ca010_0 v0x7fd9234c9e10_0)
                        (>= v0x7fd9234ca010_0 v0x7fd9234c9e10_0))
                   (and v0x7fd9234c9090_0
                        E0x7fd9234ca290
                        v0x7fd9234c98d0_0
                        (<= v0x7fd9234ca010_0 v0x7fd9234c7d90_0)
                        (>= v0x7fd9234ca010_0 v0x7fd9234c7d90_0)))))
      (a!4 (=> v0x7fd9234c9f50_0
               (or (and E0x7fd9234ca0d0 (not E0x7fd9234ca290))
                   (and E0x7fd9234ca290 (not E0x7fd9234ca0d0)))))
      (a!5 (or (and v0x7fd9234ca8d0_0
                    E0x7fd9234cbad0
                    (<= v0x7fd9234cb950_0 v0x7fd9234c9150_0)
                    (>= v0x7fd9234cb950_0 v0x7fd9234c9150_0)
                    (<= v0x7fd9234cba10_0 v0x7fd9234cac90_0)
                    (>= v0x7fd9234cba10_0 v0x7fd9234cac90_0))
               (and v0x7fd9234cb610_0
                    E0x7fd9234cbd90
                    (and (<= v0x7fd9234cb950_0 v0x7fd9234cb390_0)
                         (>= v0x7fd9234cb950_0 v0x7fd9234cb390_0))
                    (<= v0x7fd9234cba10_0 v0x7fd9234c7c10_0)
                    (>= v0x7fd9234cba10_0 v0x7fd9234c7c10_0))
               (and v0x7fd9234cadd0_0
                    E0x7fd9234cc050
                    (not v0x7fd9234cb4d0_0)
                    (and (<= v0x7fd9234cb950_0 v0x7fd9234cb390_0)
                         (>= v0x7fd9234cb950_0 v0x7fd9234cb390_0))
                    (<= v0x7fd9234cba10_0 0.0)
                    (>= v0x7fd9234cba10_0 0.0))))
      (a!6 (=> v0x7fd9234cb890_0
               (or (and E0x7fd9234cbad0
                        (not E0x7fd9234cbd90)
                        (not E0x7fd9234cc050))
                   (and E0x7fd9234cbd90
                        (not E0x7fd9234cbad0)
                        (not E0x7fd9234cc050))
                   (and E0x7fd9234cc050
                        (not E0x7fd9234cbad0)
                        (not E0x7fd9234cbd90)))))
      (a!7 (or (and v0x7fd9234cc950_0
                    v0x7fd9234ccc10_0
                    (and (<= v0x7fd9234c7e50_0 v0x7fd9234cba10_0)
                         (>= v0x7fd9234c7e50_0 v0x7fd9234cba10_0))
                    (and (<= v0x7fd9234c7f50_0 v0x7fd9234ca010_0)
                         (>= v0x7fd9234c7f50_0 v0x7fd9234ca010_0))
                    (and (<= v0x7fd9234c8050_0 v0x7fd9234cb950_0)
                         (>= v0x7fd9234c8050_0 v0x7fd9234cb950_0))
                    (<= v0x7fd9234c6010_0 1.0)
                    (>= v0x7fd9234c6010_0 1.0))
               (and v0x7fd9234cb890_0
                    v0x7fd9234cc810_0
                    (and (<= v0x7fd9234c7e50_0 v0x7fd9234cba10_0)
                         (>= v0x7fd9234c7e50_0 v0x7fd9234cba10_0))
                    (and (<= v0x7fd9234c7f50_0 v0x7fd9234ca010_0)
                         (>= v0x7fd9234c7f50_0 v0x7fd9234ca010_0))
                    (and (<= v0x7fd9234c8050_0 v0x7fd9234cb950_0)
                         (>= v0x7fd9234c8050_0 v0x7fd9234cb950_0))
                    (<= v0x7fd9234c6010_0 0.0)
                    (>= v0x7fd9234c6010_0 0.0)))))
(let ((a!8 (and (=> v0x7fd9234c8950_0
                    (and v0x7fd9234c8690_0
                         E0x7fd9234c8a10
                         (not v0x7fd9234c8810_0)))
                (=> v0x7fd9234c8950_0 E0x7fd9234c8a10)
                a!1
                a!2
                (=> v0x7fd9234c9a10_0
                    (and v0x7fd9234c9090_0
                         E0x7fd9234c9ad0
                         (not v0x7fd9234c98d0_0)))
                (=> v0x7fd9234c9a10_0 E0x7fd9234c9ad0)
                a!3
                a!4
                (=> v0x7fd9234ca8d0_0
                    (and v0x7fd9234c9f50_0 E0x7fd9234ca990 v0x7fd9234ca790_0))
                (=> v0x7fd9234ca8d0_0 E0x7fd9234ca990)
                (=> v0x7fd9234cadd0_0
                    (and v0x7fd9234c9f50_0
                         E0x7fd9234cae90
                         (not v0x7fd9234ca790_0)))
                (=> v0x7fd9234cadd0_0 E0x7fd9234cae90)
                (=> v0x7fd9234cb610_0
                    (and v0x7fd9234cadd0_0 E0x7fd9234cb6d0 v0x7fd9234cb4d0_0))
                (=> v0x7fd9234cb610_0 E0x7fd9234cb6d0)
                (=> v0x7fd9234cb890_0 a!5)
                a!6
                (=> v0x7fd9234cc950_0
                    (and v0x7fd9234cb890_0
                         E0x7fd9234cca10
                         (not v0x7fd9234cc810_0)))
                (=> v0x7fd9234cc950_0 E0x7fd9234cca10)
                a!7
                (= v0x7fd9234c8810_0 (= v0x7fd9234c8750_0 0.0))
                (= v0x7fd9234c8c50_0 (< v0x7fd9234c7e90_0 2.0))
                (= v0x7fd9234c8e10_0 (ite v0x7fd9234c8c50_0 1.0 0.0))
                (= v0x7fd9234c8f50_0 (+ v0x7fd9234c8e10_0 v0x7fd9234c7e90_0))
                (= v0x7fd9234c98d0_0 (= v0x7fd9234c9810_0 0.0))
                (= v0x7fd9234c9cd0_0 (= v0x7fd9234c7d90_0 0.0))
                (= v0x7fd9234c9e10_0 (ite v0x7fd9234c9cd0_0 1.0 0.0))
                (= v0x7fd9234ca790_0 (= v0x7fd9234c7c10_0 0.0))
                (= v0x7fd9234cab50_0 (> v0x7fd9234c9150_0 1.0))
                (= v0x7fd9234cac90_0
                   (ite v0x7fd9234cab50_0 1.0 v0x7fd9234c7c10_0))
                (= v0x7fd9234cb090_0 (> v0x7fd9234c9150_0 0.0))
                (= v0x7fd9234cb1d0_0 (+ v0x7fd9234c9150_0 (- 1.0)))
                (= v0x7fd9234cb390_0
                   (ite v0x7fd9234cb090_0 v0x7fd9234cb1d0_0 v0x7fd9234c9150_0))
                (= v0x7fd9234cb4d0_0 (= v0x7fd9234cb390_0 0.0))
                (= v0x7fd9234cc590_0 (= v0x7fd9234ca010_0 0.0))
                (= v0x7fd9234cc6d0_0 (= v0x7fd9234cba10_0 0.0))
                (= v0x7fd9234cc810_0 (or v0x7fd9234cc6d0_0 v0x7fd9234cc590_0))
                (= v0x7fd9234ccc10_0 (= v0x7fd9234c7f90_0 0.0)))))
  (=> F0x7fd9234cdb90 a!8))))
(assert (=> F0x7fd9234cdb90 F0x7fd9234cdad0))
(assert (let ((a!1 (=> v0x7fd9234c9090_0
               (or (and v0x7fd9234c8950_0
                        E0x7fd9234c9210
                        (<= v0x7fd9234c9150_0 v0x7fd9234c8f50_0)
                        (>= v0x7fd9234c9150_0 v0x7fd9234c8f50_0))
                   (and v0x7fd9234c8690_0
                        E0x7fd9234c93d0
                        v0x7fd9234c8810_0
                        (<= v0x7fd9234c9150_0 v0x7fd9234c7e90_0)
                        (>= v0x7fd9234c9150_0 v0x7fd9234c7e90_0)))))
      (a!2 (=> v0x7fd9234c9090_0
               (or (and E0x7fd9234c9210 (not E0x7fd9234c93d0))
                   (and E0x7fd9234c93d0 (not E0x7fd9234c9210)))))
      (a!3 (=> v0x7fd9234c9f50_0
               (or (and v0x7fd9234c9a10_0
                        E0x7fd9234ca0d0
                        (<= v0x7fd9234ca010_0 v0x7fd9234c9e10_0)
                        (>= v0x7fd9234ca010_0 v0x7fd9234c9e10_0))
                   (and v0x7fd9234c9090_0
                        E0x7fd9234ca290
                        v0x7fd9234c98d0_0
                        (<= v0x7fd9234ca010_0 v0x7fd9234c7d90_0)
                        (>= v0x7fd9234ca010_0 v0x7fd9234c7d90_0)))))
      (a!4 (=> v0x7fd9234c9f50_0
               (or (and E0x7fd9234ca0d0 (not E0x7fd9234ca290))
                   (and E0x7fd9234ca290 (not E0x7fd9234ca0d0)))))
      (a!5 (or (and v0x7fd9234ca8d0_0
                    E0x7fd9234cbad0
                    (<= v0x7fd9234cb950_0 v0x7fd9234c9150_0)
                    (>= v0x7fd9234cb950_0 v0x7fd9234c9150_0)
                    (<= v0x7fd9234cba10_0 v0x7fd9234cac90_0)
                    (>= v0x7fd9234cba10_0 v0x7fd9234cac90_0))
               (and v0x7fd9234cb610_0
                    E0x7fd9234cbd90
                    (and (<= v0x7fd9234cb950_0 v0x7fd9234cb390_0)
                         (>= v0x7fd9234cb950_0 v0x7fd9234cb390_0))
                    (<= v0x7fd9234cba10_0 v0x7fd9234c7c10_0)
                    (>= v0x7fd9234cba10_0 v0x7fd9234c7c10_0))
               (and v0x7fd9234cadd0_0
                    E0x7fd9234cc050
                    (not v0x7fd9234cb4d0_0)
                    (and (<= v0x7fd9234cb950_0 v0x7fd9234cb390_0)
                         (>= v0x7fd9234cb950_0 v0x7fd9234cb390_0))
                    (<= v0x7fd9234cba10_0 0.0)
                    (>= v0x7fd9234cba10_0 0.0))))
      (a!6 (=> v0x7fd9234cb890_0
               (or (and E0x7fd9234cbad0
                        (not E0x7fd9234cbd90)
                        (not E0x7fd9234cc050))
                   (and E0x7fd9234cbd90
                        (not E0x7fd9234cbad0)
                        (not E0x7fd9234cc050))
                   (and E0x7fd9234cc050
                        (not E0x7fd9234cbad0)
                        (not E0x7fd9234cbd90))))))
(let ((a!7 (and (=> v0x7fd9234c8950_0
                    (and v0x7fd9234c8690_0
                         E0x7fd9234c8a10
                         (not v0x7fd9234c8810_0)))
                (=> v0x7fd9234c8950_0 E0x7fd9234c8a10)
                a!1
                a!2
                (=> v0x7fd9234c9a10_0
                    (and v0x7fd9234c9090_0
                         E0x7fd9234c9ad0
                         (not v0x7fd9234c98d0_0)))
                (=> v0x7fd9234c9a10_0 E0x7fd9234c9ad0)
                a!3
                a!4
                (=> v0x7fd9234ca8d0_0
                    (and v0x7fd9234c9f50_0 E0x7fd9234ca990 v0x7fd9234ca790_0))
                (=> v0x7fd9234ca8d0_0 E0x7fd9234ca990)
                (=> v0x7fd9234cadd0_0
                    (and v0x7fd9234c9f50_0
                         E0x7fd9234cae90
                         (not v0x7fd9234ca790_0)))
                (=> v0x7fd9234cadd0_0 E0x7fd9234cae90)
                (=> v0x7fd9234cb610_0
                    (and v0x7fd9234cadd0_0 E0x7fd9234cb6d0 v0x7fd9234cb4d0_0))
                (=> v0x7fd9234cb610_0 E0x7fd9234cb6d0)
                (=> v0x7fd9234cb890_0 a!5)
                a!6
                (=> v0x7fd9234cc950_0
                    (and v0x7fd9234cb890_0
                         E0x7fd9234cca10
                         (not v0x7fd9234cc810_0)))
                (=> v0x7fd9234cc950_0 E0x7fd9234cca10)
                v0x7fd9234cc950_0
                (not v0x7fd9234ccc10_0)
                (= v0x7fd9234c8810_0 (= v0x7fd9234c8750_0 0.0))
                (= v0x7fd9234c8c50_0 (< v0x7fd9234c7e90_0 2.0))
                (= v0x7fd9234c8e10_0 (ite v0x7fd9234c8c50_0 1.0 0.0))
                (= v0x7fd9234c8f50_0 (+ v0x7fd9234c8e10_0 v0x7fd9234c7e90_0))
                (= v0x7fd9234c98d0_0 (= v0x7fd9234c9810_0 0.0))
                (= v0x7fd9234c9cd0_0 (= v0x7fd9234c7d90_0 0.0))
                (= v0x7fd9234c9e10_0 (ite v0x7fd9234c9cd0_0 1.0 0.0))
                (= v0x7fd9234ca790_0 (= v0x7fd9234c7c10_0 0.0))
                (= v0x7fd9234cab50_0 (> v0x7fd9234c9150_0 1.0))
                (= v0x7fd9234cac90_0
                   (ite v0x7fd9234cab50_0 1.0 v0x7fd9234c7c10_0))
                (= v0x7fd9234cb090_0 (> v0x7fd9234c9150_0 0.0))
                (= v0x7fd9234cb1d0_0 (+ v0x7fd9234c9150_0 (- 1.0)))
                (= v0x7fd9234cb390_0
                   (ite v0x7fd9234cb090_0 v0x7fd9234cb1d0_0 v0x7fd9234c9150_0))
                (= v0x7fd9234cb4d0_0 (= v0x7fd9234cb390_0 0.0))
                (= v0x7fd9234cc590_0 (= v0x7fd9234ca010_0 0.0))
                (= v0x7fd9234cc6d0_0 (= v0x7fd9234cba10_0 0.0))
                (= v0x7fd9234cc810_0 (or v0x7fd9234cc6d0_0 v0x7fd9234cc590_0))
                (= v0x7fd9234ccc10_0 (= v0x7fd9234c7f90_0 0.0)))))
  (=> F0x7fd9234cd9d0 a!7))))
(assert (=> F0x7fd9234cd9d0 F0x7fd9234cdad0))
(assert (=> F0x7fd9234cddd0 (or F0x7fd9234cdcd0 F0x7fd9234cdb90)))
(assert (=> F0x7fd9234cdd90 F0x7fd9234cd9d0))
(assert (=> pre!entry!0 (=> F0x7fd9234cdc10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd9234cdad0 (>= v0x7fd9234c7f90_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7fd9234cdad0
        (or (>= v0x7fd9234c7e90_0 0.0) (<= v0x7fd9234c7f90_0 0.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7fd9234cdad0 (>= v0x7fd9234c7c10_0 0.0))))
(assert (let ((a!1 (=> F0x7fd9234cdad0
               (or (<= v0x7fd9234c7c10_0 0.0) (not (<= v0x7fd9234c7e90_0 1.0))))))
  (=> pre!bb1.i.i!3 a!1)))
(assert (=> pre!bb1.i.i!4
    (=> F0x7fd9234cdad0
        (or (<= v0x7fd9234c7f90_0 0.0) (>= v0x7fd9234c7c10_0 1.0)))))
(assert (=> pre!bb1.i.i!5 (=> F0x7fd9234cdad0 (not (<= v0x7fd9234c7e90_0 0.0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fd9234cddd0 (>= v0x7fd9234c6010_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7fd9234cddd0
       (or (>= v0x7fd9234c8050_0 0.0) (<= v0x7fd9234c6010_0 0.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7fd9234cddd0 (>= v0x7fd9234c7e50_0 0.0))))
(assert (let ((a!1 (=> F0x7fd9234cddd0
               (or (<= v0x7fd9234c7e50_0 0.0) (not (<= v0x7fd9234c8050_0 1.0))))))
  (= lemma!bb1.i.i!3 a!1)))
(assert (= lemma!bb1.i.i!4
   (=> F0x7fd9234cddd0
       (or (<= v0x7fd9234c6010_0 0.0) (>= v0x7fd9234c7e50_0 1.0)))))
(assert (= lemma!bb1.i.i!5 (=> F0x7fd9234cddd0 (not (<= v0x7fd9234c8050_0 0.0)))))
(assert (= lemma!bb2.i.i26.i.i!0 (=> F0x7fd9234cdd90 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb1.i.i!5) (not lemma!bb1.i.i!5))
    (and (not post!bb2.i.i26.i.i!0) (not lemma!bb2.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4 pre!bb1.i.i!5)
; (init: F0x7fd9234cdc10)
; (error: F0x7fd9234cdd90)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb1.i.i!5 post!bb2.i.i26.i.i!0)
