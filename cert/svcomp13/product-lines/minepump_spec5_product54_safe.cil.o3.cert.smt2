(declare-fun post!bb2.i.i37.i.i!0 () Bool)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i37.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun v0x7f39b69f7750_0 () Bool)
(declare-fun v0x7f39b69f7610_0 () Bool)
(declare-fun F0x7f39b69f9d90 () Bool)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun v0x7f39b69f7310_0 () Real)
(declare-fun v0x7f39b69f71d0_0 () Bool)
(declare-fun v0x7f39b69f6c90_0 () Bool)
(declare-fun v0x7f39b69f5e10_0 () Bool)
(declare-fun v0x7f39b69f4d90_0 () Bool)
(declare-fun v0x7f39b69f8e50_0 () Bool)
(declare-fun E0x7f39b69f8410 () Bool)
(declare-fun v0x7f39b69f3f90_0 () Real)
(declare-fun v0x7f39b69f74d0_0 () Real)
(declare-fun E0x7f39b69f7e90 () Bool)
(declare-fun v0x7f39b69f4f50_0 () Real)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun v0x7f39b69f7c50_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun v0x7f39b69f7890_0 () Bool)
(declare-fun v0x7f39b69f6f10_0 () Bool)
(declare-fun v0x7f39b69f68d0_0 () Bool)
(declare-fun v0x7f39b69f8950_0 () Bool)
(declare-fun E0x7f39b69f6fd0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7f39b69f6ad0 () Bool)
(declare-fun v0x7f39b69f6a10_0 () Bool)
(declare-fun E0x7f39b69f63d0 () Bool)
(declare-fun v0x7f39b69f6150_0 () Real)
(declare-fun v0x7f39b69f6090_0 () Bool)
(declare-fun v0x7f39b69f5a10_0 () Bool)
(declare-fun v0x7f39b69f5f50_0 () Real)
(declare-fun v0x7f39b69f4210_0 () Real)
(declare-fun F0x7f39b69f9d50 () Bool)
(declare-fun v0x7f39b69f4890_0 () Real)
(declare-fun v0x7f39b69f7d10_0 () Real)
(declare-fun v0x7f39b69f8d10_0 () Bool)
(declare-fun v0x7f39b69f5090_0 () Real)
(declare-fun v0x7f39b69f4950_0 () Bool)
(declare-fun E0x7f39b69f7a90 () Bool)
(declare-fun E0x7f39b69f4b50 () Bool)
(declare-fun v0x7f39b69f51d0_0 () Bool)
(declare-fun v0x7f39b69f7dd0_0 () Real)
(declare-fun v0x7f39b69f4a90_0 () Bool)
(declare-fun v0x7f39b69f8bd0_0 () Bool)
(declare-fun E0x7f39b69f6210 () Bool)
(declare-fun v0x7f39b69f5290_0 () Real)
(declare-fun F0x7f39b69f9a50 () Bool)
(declare-fun v0x7f39b69f5950_0 () Real)
(declare-fun v0x7f39b69f5b50_0 () Bool)
(declare-fun v0x7f39b69f42d0_0 () Real)
(declare-fun v0x7f39b69f47d0_0 () Bool)
(declare-fun v0x7f39b69f8a90_0 () Bool)
(declare-fun E0x7f39b69f5c10 () Bool)
(declare-fun v0x7f39b69f41d0_0 () Real)
(declare-fun F0x7f39b69f9b10 () Bool)
(declare-fun v0x7f39b69f79d0_0 () Bool)
(declare-fun v0x7f39b69f1110_0 () Bool)
(declare-fun v0x7f39b69f6dd0_0 () Real)
(declare-fun E0x7f39b69f8150 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun v0x7f39b69f4110_0 () Real)
(declare-fun E0x7f39b69f5510 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f39b69f9bd0 () Bool)
(declare-fun E0x7f39b69f5350 () Bool)
(declare-fun F0x7f39b69f9990 () Bool)
(declare-fun F0x7f39b69f9c90 () Bool)
(declare-fun v0x7f39b69f1010_0 () Real)

(assert (=> F0x7f39b69f9bd0
    (and v0x7f39b69f1110_0
         (<= v0x7f39b69f41d0_0 0.0)
         (>= v0x7f39b69f41d0_0 0.0)
         (<= v0x7f39b69f42d0_0 0.0)
         (>= v0x7f39b69f42d0_0 0.0)
         (<= v0x7f39b69f1010_0 1.0)
         (>= v0x7f39b69f1010_0 1.0))))
(assert (=> F0x7f39b69f9bd0 F0x7f39b69f9b10))
(assert (let ((a!1 (=> v0x7f39b69f51d0_0
               (or (and v0x7f39b69f4a90_0
                        E0x7f39b69f5350
                        (<= v0x7f39b69f5290_0 v0x7f39b69f5090_0)
                        (>= v0x7f39b69f5290_0 v0x7f39b69f5090_0))
                   (and v0x7f39b69f47d0_0
                        E0x7f39b69f5510
                        v0x7f39b69f4950_0
                        (<= v0x7f39b69f5290_0 v0x7f39b69f4210_0)
                        (>= v0x7f39b69f5290_0 v0x7f39b69f4210_0)))))
      (a!2 (=> v0x7f39b69f51d0_0
               (or (and E0x7f39b69f5350 (not E0x7f39b69f5510))
                   (and E0x7f39b69f5510 (not E0x7f39b69f5350)))))
      (a!3 (=> v0x7f39b69f6090_0
               (or (and v0x7f39b69f5b50_0
                        E0x7f39b69f6210
                        (<= v0x7f39b69f6150_0 v0x7f39b69f5f50_0)
                        (>= v0x7f39b69f6150_0 v0x7f39b69f5f50_0))
                   (and v0x7f39b69f51d0_0
                        E0x7f39b69f63d0
                        v0x7f39b69f5a10_0
                        (<= v0x7f39b69f6150_0 v0x7f39b69f4110_0)
                        (>= v0x7f39b69f6150_0 v0x7f39b69f4110_0)))))
      (a!4 (=> v0x7f39b69f6090_0
               (or (and E0x7f39b69f6210 (not E0x7f39b69f63d0))
                   (and E0x7f39b69f63d0 (not E0x7f39b69f6210)))))
      (a!5 (or (and v0x7f39b69f6a10_0
                    E0x7f39b69f7e90
                    (<= v0x7f39b69f7d10_0 v0x7f39b69f5290_0)
                    (>= v0x7f39b69f7d10_0 v0x7f39b69f5290_0)
                    (<= v0x7f39b69f7dd0_0 v0x7f39b69f6dd0_0)
                    (>= v0x7f39b69f7dd0_0 v0x7f39b69f6dd0_0))
               (and v0x7f39b69f79d0_0
                    E0x7f39b69f8150
                    (and (<= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0)
                         (>= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0))
                    (<= v0x7f39b69f7dd0_0 v0x7f39b69f3f90_0)
                    (>= v0x7f39b69f7dd0_0 v0x7f39b69f3f90_0))
               (and v0x7f39b69f6f10_0
                    E0x7f39b69f8410
                    (not v0x7f39b69f7890_0)
                    (and (<= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0)
                         (>= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0))
                    (<= v0x7f39b69f7dd0_0 0.0)
                    (>= v0x7f39b69f7dd0_0 0.0))))
      (a!6 (=> v0x7f39b69f7c50_0
               (or (and E0x7f39b69f7e90
                        (not E0x7f39b69f8150)
                        (not E0x7f39b69f8410))
                   (and E0x7f39b69f8150
                        (not E0x7f39b69f7e90)
                        (not E0x7f39b69f8410))
                   (and E0x7f39b69f8410
                        (not E0x7f39b69f7e90)
                        (not E0x7f39b69f8150))))))
(let ((a!7 (and (=> v0x7f39b69f4a90_0
                    (and v0x7f39b69f47d0_0
                         E0x7f39b69f4b50
                         (not v0x7f39b69f4950_0)))
                (=> v0x7f39b69f4a90_0 E0x7f39b69f4b50)
                a!1
                a!2
                (=> v0x7f39b69f5b50_0
                    (and v0x7f39b69f51d0_0
                         E0x7f39b69f5c10
                         (not v0x7f39b69f5a10_0)))
                (=> v0x7f39b69f5b50_0 E0x7f39b69f5c10)
                a!3
                a!4
                (=> v0x7f39b69f6a10_0
                    (and v0x7f39b69f6090_0 E0x7f39b69f6ad0 v0x7f39b69f68d0_0))
                (=> v0x7f39b69f6a10_0 E0x7f39b69f6ad0)
                (=> v0x7f39b69f6f10_0
                    (and v0x7f39b69f6090_0
                         E0x7f39b69f6fd0
                         (not v0x7f39b69f68d0_0)))
                (=> v0x7f39b69f6f10_0 E0x7f39b69f6fd0)
                (=> v0x7f39b69f79d0_0
                    (and v0x7f39b69f6f10_0 E0x7f39b69f7a90 v0x7f39b69f7890_0))
                (=> v0x7f39b69f79d0_0 E0x7f39b69f7a90)
                (=> v0x7f39b69f7c50_0 a!5)
                a!6
                v0x7f39b69f7c50_0
                (not v0x7f39b69f8e50_0)
                (<= v0x7f39b69f41d0_0 v0x7f39b69f7dd0_0)
                (>= v0x7f39b69f41d0_0 v0x7f39b69f7dd0_0)
                (<= v0x7f39b69f42d0_0 v0x7f39b69f6150_0)
                (>= v0x7f39b69f42d0_0 v0x7f39b69f6150_0)
                (<= v0x7f39b69f1010_0 v0x7f39b69f7d10_0)
                (>= v0x7f39b69f1010_0 v0x7f39b69f7d10_0)
                (= v0x7f39b69f4950_0 (= v0x7f39b69f4890_0 0.0))
                (= v0x7f39b69f4d90_0 (< v0x7f39b69f4210_0 2.0))
                (= v0x7f39b69f4f50_0 (ite v0x7f39b69f4d90_0 1.0 0.0))
                (= v0x7f39b69f5090_0 (+ v0x7f39b69f4f50_0 v0x7f39b69f4210_0))
                (= v0x7f39b69f5a10_0 (= v0x7f39b69f5950_0 0.0))
                (= v0x7f39b69f5e10_0 (= v0x7f39b69f4110_0 0.0))
                (= v0x7f39b69f5f50_0 (ite v0x7f39b69f5e10_0 1.0 0.0))
                (= v0x7f39b69f68d0_0 (= v0x7f39b69f3f90_0 0.0))
                (= v0x7f39b69f6c90_0 (> v0x7f39b69f5290_0 1.0))
                (= v0x7f39b69f6dd0_0
                   (ite v0x7f39b69f6c90_0 1.0 v0x7f39b69f3f90_0))
                (= v0x7f39b69f71d0_0 (> v0x7f39b69f5290_0 0.0))
                (= v0x7f39b69f7310_0 (+ v0x7f39b69f5290_0 (- 1.0)))
                (= v0x7f39b69f74d0_0
                   (ite v0x7f39b69f71d0_0 v0x7f39b69f7310_0 v0x7f39b69f5290_0))
                (= v0x7f39b69f7610_0 (= v0x7f39b69f6150_0 0.0))
                (= v0x7f39b69f7750_0 (= v0x7f39b69f74d0_0 0.0))
                (= v0x7f39b69f7890_0 (and v0x7f39b69f7610_0 v0x7f39b69f7750_0))
                (= v0x7f39b69f8950_0 (= v0x7f39b69f7d10_0 2.0))
                (= v0x7f39b69f8a90_0 (= v0x7f39b69f7dd0_0 0.0))
                (= v0x7f39b69f8bd0_0 (or v0x7f39b69f8a90_0 v0x7f39b69f8950_0))
                (= v0x7f39b69f8d10_0 (xor v0x7f39b69f8bd0_0 true))
                (= v0x7f39b69f8e50_0 (and v0x7f39b69f68d0_0 v0x7f39b69f8d10_0)))))
  (=> F0x7f39b69f9a50 a!7))))
(assert (=> F0x7f39b69f9a50 F0x7f39b69f9990))
(assert (let ((a!1 (=> v0x7f39b69f51d0_0
               (or (and v0x7f39b69f4a90_0
                        E0x7f39b69f5350
                        (<= v0x7f39b69f5290_0 v0x7f39b69f5090_0)
                        (>= v0x7f39b69f5290_0 v0x7f39b69f5090_0))
                   (and v0x7f39b69f47d0_0
                        E0x7f39b69f5510
                        v0x7f39b69f4950_0
                        (<= v0x7f39b69f5290_0 v0x7f39b69f4210_0)
                        (>= v0x7f39b69f5290_0 v0x7f39b69f4210_0)))))
      (a!2 (=> v0x7f39b69f51d0_0
               (or (and E0x7f39b69f5350 (not E0x7f39b69f5510))
                   (and E0x7f39b69f5510 (not E0x7f39b69f5350)))))
      (a!3 (=> v0x7f39b69f6090_0
               (or (and v0x7f39b69f5b50_0
                        E0x7f39b69f6210
                        (<= v0x7f39b69f6150_0 v0x7f39b69f5f50_0)
                        (>= v0x7f39b69f6150_0 v0x7f39b69f5f50_0))
                   (and v0x7f39b69f51d0_0
                        E0x7f39b69f63d0
                        v0x7f39b69f5a10_0
                        (<= v0x7f39b69f6150_0 v0x7f39b69f4110_0)
                        (>= v0x7f39b69f6150_0 v0x7f39b69f4110_0)))))
      (a!4 (=> v0x7f39b69f6090_0
               (or (and E0x7f39b69f6210 (not E0x7f39b69f63d0))
                   (and E0x7f39b69f63d0 (not E0x7f39b69f6210)))))
      (a!5 (or (and v0x7f39b69f6a10_0
                    E0x7f39b69f7e90
                    (<= v0x7f39b69f7d10_0 v0x7f39b69f5290_0)
                    (>= v0x7f39b69f7d10_0 v0x7f39b69f5290_0)
                    (<= v0x7f39b69f7dd0_0 v0x7f39b69f6dd0_0)
                    (>= v0x7f39b69f7dd0_0 v0x7f39b69f6dd0_0))
               (and v0x7f39b69f79d0_0
                    E0x7f39b69f8150
                    (and (<= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0)
                         (>= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0))
                    (<= v0x7f39b69f7dd0_0 v0x7f39b69f3f90_0)
                    (>= v0x7f39b69f7dd0_0 v0x7f39b69f3f90_0))
               (and v0x7f39b69f6f10_0
                    E0x7f39b69f8410
                    (not v0x7f39b69f7890_0)
                    (and (<= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0)
                         (>= v0x7f39b69f7d10_0 v0x7f39b69f74d0_0))
                    (<= v0x7f39b69f7dd0_0 0.0)
                    (>= v0x7f39b69f7dd0_0 0.0))))
      (a!6 (=> v0x7f39b69f7c50_0
               (or (and E0x7f39b69f7e90
                        (not E0x7f39b69f8150)
                        (not E0x7f39b69f8410))
                   (and E0x7f39b69f8150
                        (not E0x7f39b69f7e90)
                        (not E0x7f39b69f8410))
                   (and E0x7f39b69f8410
                        (not E0x7f39b69f7e90)
                        (not E0x7f39b69f8150))))))
(let ((a!7 (and (=> v0x7f39b69f4a90_0
                    (and v0x7f39b69f47d0_0
                         E0x7f39b69f4b50
                         (not v0x7f39b69f4950_0)))
                (=> v0x7f39b69f4a90_0 E0x7f39b69f4b50)
                a!1
                a!2
                (=> v0x7f39b69f5b50_0
                    (and v0x7f39b69f51d0_0
                         E0x7f39b69f5c10
                         (not v0x7f39b69f5a10_0)))
                (=> v0x7f39b69f5b50_0 E0x7f39b69f5c10)
                a!3
                a!4
                (=> v0x7f39b69f6a10_0
                    (and v0x7f39b69f6090_0 E0x7f39b69f6ad0 v0x7f39b69f68d0_0))
                (=> v0x7f39b69f6a10_0 E0x7f39b69f6ad0)
                (=> v0x7f39b69f6f10_0
                    (and v0x7f39b69f6090_0
                         E0x7f39b69f6fd0
                         (not v0x7f39b69f68d0_0)))
                (=> v0x7f39b69f6f10_0 E0x7f39b69f6fd0)
                (=> v0x7f39b69f79d0_0
                    (and v0x7f39b69f6f10_0 E0x7f39b69f7a90 v0x7f39b69f7890_0))
                (=> v0x7f39b69f79d0_0 E0x7f39b69f7a90)
                (=> v0x7f39b69f7c50_0 a!5)
                a!6
                v0x7f39b69f7c50_0
                v0x7f39b69f8e50_0
                (= v0x7f39b69f4950_0 (= v0x7f39b69f4890_0 0.0))
                (= v0x7f39b69f4d90_0 (< v0x7f39b69f4210_0 2.0))
                (= v0x7f39b69f4f50_0 (ite v0x7f39b69f4d90_0 1.0 0.0))
                (= v0x7f39b69f5090_0 (+ v0x7f39b69f4f50_0 v0x7f39b69f4210_0))
                (= v0x7f39b69f5a10_0 (= v0x7f39b69f5950_0 0.0))
                (= v0x7f39b69f5e10_0 (= v0x7f39b69f4110_0 0.0))
                (= v0x7f39b69f5f50_0 (ite v0x7f39b69f5e10_0 1.0 0.0))
                (= v0x7f39b69f68d0_0 (= v0x7f39b69f3f90_0 0.0))
                (= v0x7f39b69f6c90_0 (> v0x7f39b69f5290_0 1.0))
                (= v0x7f39b69f6dd0_0
                   (ite v0x7f39b69f6c90_0 1.0 v0x7f39b69f3f90_0))
                (= v0x7f39b69f71d0_0 (> v0x7f39b69f5290_0 0.0))
                (= v0x7f39b69f7310_0 (+ v0x7f39b69f5290_0 (- 1.0)))
                (= v0x7f39b69f74d0_0
                   (ite v0x7f39b69f71d0_0 v0x7f39b69f7310_0 v0x7f39b69f5290_0))
                (= v0x7f39b69f7610_0 (= v0x7f39b69f6150_0 0.0))
                (= v0x7f39b69f7750_0 (= v0x7f39b69f74d0_0 0.0))
                (= v0x7f39b69f7890_0 (and v0x7f39b69f7610_0 v0x7f39b69f7750_0))
                (= v0x7f39b69f8950_0 (= v0x7f39b69f7d10_0 2.0))
                (= v0x7f39b69f8a90_0 (= v0x7f39b69f7dd0_0 0.0))
                (= v0x7f39b69f8bd0_0 (or v0x7f39b69f8a90_0 v0x7f39b69f8950_0))
                (= v0x7f39b69f8d10_0 (xor v0x7f39b69f8bd0_0 true))
                (= v0x7f39b69f8e50_0 (and v0x7f39b69f68d0_0 v0x7f39b69f8d10_0)))))
  (=> F0x7f39b69f9c90 a!7))))
(assert (=> F0x7f39b69f9c90 F0x7f39b69f9990))
(assert (=> F0x7f39b69f9d90 (or F0x7f39b69f9bd0 F0x7f39b69f9a50)))
(assert (=> F0x7f39b69f9d50 F0x7f39b69f9c90))
(assert (=> pre!entry!0 (=> F0x7f39b69f9b10 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f39b69f9990 (>= v0x7f39b69f4110_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f39b69f9990 (>= v0x7f39b69f4210_0 0.0))))
(assert (=> pre!bb1.i.i!2
    (=> F0x7f39b69f9990
        (or (>= v0x7f39b69f4210_0 1.0) (<= v0x7f39b69f4210_0 0.0)))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f39b69f9990
        (or (<= v0x7f39b69f4210_0 1.0) (>= v0x7f39b69f4210_0 2.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f39b69f9990 (<= v0x7f39b69f4210_0 2.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f39b69f9d90 (>= v0x7f39b69f42d0_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f39b69f9d90 (>= v0x7f39b69f1010_0 0.0))))
(assert (= lemma!bb1.i.i!2
   (=> F0x7f39b69f9d90
       (or (>= v0x7f39b69f1010_0 1.0) (<= v0x7f39b69f1010_0 0.0)))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7f39b69f9d90
       (or (<= v0x7f39b69f1010_0 1.0) (>= v0x7f39b69f1010_0 2.0)))))
(assert (= lemma!bb1.i.i!4 (=> F0x7f39b69f9d90 (<= v0x7f39b69f1010_0 2.0))))
(assert (= lemma!bb2.i.i37.i.i!0 (=> F0x7f39b69f9d50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb2.i.i37.i.i!0) (not lemma!bb2.i.i37.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
; (init: F0x7f39b69f9b10)
; (error: F0x7f39b69f9d50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i37.i.i!0)
