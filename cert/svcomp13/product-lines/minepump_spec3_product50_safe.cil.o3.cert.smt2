(declare-fun post!bb2.i.i26.i.i!0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i26.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f547a840710_0 () Bool)
(declare-fun v0x7f547a83e910_0 () Bool)
(declare-fun v0x7f547a83d5d0_0 () Real)
(declare-fun v0x7f547a83ca10_0 () Bool)
(declare-fun v0x7f547a840850_0 () Bool)
(declare-fun E0x7f547a83fe10 () Bool)
(declare-fun E0x7f547a83fb50 () Bool)
(declare-fun v0x7f547a83f150_0 () Real)
(declare-fun v0x7f547a83f7d0_0 () Real)
(declare-fun v0x7f547a83f710_0 () Real)
(declare-fun F0x7f547a841790 () Bool)
(declare-fun v0x7f547a83ee50_0 () Bool)
(declare-fun v0x7f547a83ef90_0 () Real)
(declare-fun F0x7f547a841590 () Bool)
(declare-fun E0x7f547a83f890 () Bool)
(declare-fun v0x7f547a8405d0_0 () Bool)
(declare-fun v0x7f547a83f650_0 () Bool)
(declare-fun E0x7f547a83f490 () Bool)
(declare-fun v0x7f547a83f3d0_0 () Bool)
(declare-fun v0x7f547a83f290_0 () Bool)
(declare-fun v0x7f547a83bc10_0 () Real)
(declare-fun F0x7f547a841750 () Bool)
(declare-fun E0x7f547a83ec50 () Bool)
(declare-fun F0x7f547a841650 () Bool)
(declare-fun v0x7f547a83e550_0 () Bool)
(declare-fun v0x7f547a83ddd0_0 () Real)
(declare-fun v0x7f547a840350_0 () Bool)
(declare-fun v0x7f547a83d7d0_0 () Bool)
(declare-fun v0x7f547a83cbd0_0 () Real)
(declare-fun E0x7f547a83e750 () Bool)
(declare-fun E0x7f547a83de90 () Bool)
(declare-fun v0x7f547a83da90_0 () Bool)
(declare-fun v0x7f547a83be90_0 () Real)
(declare-fun v0x7f547a83cd10_0 () Real)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f547a840490_0 () Bool)
(declare-fun v0x7f547a83ce50_0 () Bool)
(declare-fun v0x7f547a83eb90_0 () Bool)
(declare-fun v0x7f547a83e690_0 () Bool)
(declare-fun v0x7f547a83c5d0_0 () Bool)
(declare-fun E0x7f547a83e050 () Bool)
(declare-fun v0x7f547a83c450_0 () Bool)
(declare-fun E0x7f547a83d890 () Bool)
(declare-fun v0x7f547a83cf10_0 () Real)
(declare-fun v0x7f547a83c710_0 () Bool)
(declare-fun E0x7f547a83cfd0 () Bool)
(declare-fun F0x7f547a8414d0 () Bool)
(declare-fun v0x7f547a83a010_0 () Real)
(declare-fun v0x7f547a83c510_0 () Real)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7f547a83c7d0 () Bool)
(declare-fun v0x7f547a83bf50_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f547a83dd10_0 () Bool)
(declare-fun v0x7f547a83dbd0_0 () Real)
(declare-fun v0x7f547a83ea50_0 () Real)
(declare-fun v0x7f547a83be50_0 () Real)
(declare-fun v0x7f547a83bd90_0 () Real)
(declare-fun v0x7f547a83a110_0 () Bool)
(declare-fun E0x7f547a83d190 () Bool)
(declare-fun F0x7f547a841390 () Bool)
(declare-fun F0x7f547a841490 () Bool)
(declare-fun v0x7f547a83d690_0 () Bool)

(assert (=> F0x7f547a841490
    (and v0x7f547a83a110_0
         (<= v0x7f547a83be50_0 0.0)
         (>= v0x7f547a83be50_0 0.0)
         (<= v0x7f547a83bf50_0 0.0)
         (>= v0x7f547a83bf50_0 0.0)
         (<= v0x7f547a83a010_0 1.0)
         (>= v0x7f547a83a010_0 1.0))))
(assert (=> F0x7f547a841490 F0x7f547a841390))
(assert (let ((a!1 (=> v0x7f547a83ce50_0
               (or (and v0x7f547a83c710_0
                        E0x7f547a83cfd0
                        (<= v0x7f547a83cf10_0 v0x7f547a83cd10_0)
                        (>= v0x7f547a83cf10_0 v0x7f547a83cd10_0))
                   (and v0x7f547a83c450_0
                        E0x7f547a83d190
                        v0x7f547a83c5d0_0
                        (<= v0x7f547a83cf10_0 v0x7f547a83be90_0)
                        (>= v0x7f547a83cf10_0 v0x7f547a83be90_0)))))
      (a!2 (=> v0x7f547a83ce50_0
               (or (and E0x7f547a83cfd0 (not E0x7f547a83d190))
                   (and E0x7f547a83d190 (not E0x7f547a83cfd0)))))
      (a!3 (=> v0x7f547a83dd10_0
               (or (and v0x7f547a83d7d0_0
                        E0x7f547a83de90
                        (<= v0x7f547a83ddd0_0 v0x7f547a83dbd0_0)
                        (>= v0x7f547a83ddd0_0 v0x7f547a83dbd0_0))
                   (and v0x7f547a83ce50_0
                        E0x7f547a83e050
                        v0x7f547a83d690_0
                        (<= v0x7f547a83ddd0_0 v0x7f547a83bd90_0)
                        (>= v0x7f547a83ddd0_0 v0x7f547a83bd90_0)))))
      (a!4 (=> v0x7f547a83dd10_0
               (or (and E0x7f547a83de90 (not E0x7f547a83e050))
                   (and E0x7f547a83e050 (not E0x7f547a83de90)))))
      (a!5 (or (and v0x7f547a83e690_0
                    E0x7f547a83f890
                    (<= v0x7f547a83f710_0 v0x7f547a83cf10_0)
                    (>= v0x7f547a83f710_0 v0x7f547a83cf10_0)
                    (<= v0x7f547a83f7d0_0 v0x7f547a83ea50_0)
                    (>= v0x7f547a83f7d0_0 v0x7f547a83ea50_0))
               (and v0x7f547a83f3d0_0
                    E0x7f547a83fb50
                    (and (<= v0x7f547a83f710_0 v0x7f547a83f150_0)
                         (>= v0x7f547a83f710_0 v0x7f547a83f150_0))
                    (<= v0x7f547a83f7d0_0 v0x7f547a83bc10_0)
                    (>= v0x7f547a83f7d0_0 v0x7f547a83bc10_0))
               (and v0x7f547a83eb90_0
                    E0x7f547a83fe10
                    (not v0x7f547a83f290_0)
                    (and (<= v0x7f547a83f710_0 v0x7f547a83f150_0)
                         (>= v0x7f547a83f710_0 v0x7f547a83f150_0))
                    (<= v0x7f547a83f7d0_0 0.0)
                    (>= v0x7f547a83f7d0_0 0.0))))
      (a!6 (=> v0x7f547a83f650_0
               (or (and E0x7f547a83f890
                        (not E0x7f547a83fb50)
                        (not E0x7f547a83fe10))
                   (and E0x7f547a83fb50
                        (not E0x7f547a83f890)
                        (not E0x7f547a83fe10))
                   (and E0x7f547a83fe10
                        (not E0x7f547a83f890)
                        (not E0x7f547a83fb50))))))
(let ((a!7 (and (=> v0x7f547a83c710_0
                    (and v0x7f547a83c450_0
                         E0x7f547a83c7d0
                         (not v0x7f547a83c5d0_0)))
                (=> v0x7f547a83c710_0 E0x7f547a83c7d0)
                a!1
                a!2
                (=> v0x7f547a83d7d0_0
                    (and v0x7f547a83ce50_0
                         E0x7f547a83d890
                         (not v0x7f547a83d690_0)))
                (=> v0x7f547a83d7d0_0 E0x7f547a83d890)
                a!3
                a!4
                (=> v0x7f547a83e690_0
                    (and v0x7f547a83dd10_0 E0x7f547a83e750 v0x7f547a83e550_0))
                (=> v0x7f547a83e690_0 E0x7f547a83e750)
                (=> v0x7f547a83eb90_0
                    (and v0x7f547a83dd10_0
                         E0x7f547a83ec50
                         (not v0x7f547a83e550_0)))
                (=> v0x7f547a83eb90_0 E0x7f547a83ec50)
                (=> v0x7f547a83f3d0_0
                    (and v0x7f547a83eb90_0 E0x7f547a83f490 v0x7f547a83f290_0))
                (=> v0x7f547a83f3d0_0 E0x7f547a83f490)
                (=> v0x7f547a83f650_0 a!5)
                a!6
                v0x7f547a83f650_0
                (not v0x7f547a840850_0)
                (<= v0x7f547a83be50_0 v0x7f547a83f7d0_0)
                (>= v0x7f547a83be50_0 v0x7f547a83f7d0_0)
                (<= v0x7f547a83bf50_0 v0x7f547a83ddd0_0)
                (>= v0x7f547a83bf50_0 v0x7f547a83ddd0_0)
                (<= v0x7f547a83a010_0 v0x7f547a83f710_0)
                (>= v0x7f547a83a010_0 v0x7f547a83f710_0)
                (= v0x7f547a83c5d0_0 (= v0x7f547a83c510_0 0.0))
                (= v0x7f547a83ca10_0 (< v0x7f547a83be90_0 2.0))
                (= v0x7f547a83cbd0_0 (ite v0x7f547a83ca10_0 1.0 0.0))
                (= v0x7f547a83cd10_0 (+ v0x7f547a83cbd0_0 v0x7f547a83be90_0))
                (= v0x7f547a83d690_0 (= v0x7f547a83d5d0_0 0.0))
                (= v0x7f547a83da90_0 (= v0x7f547a83bd90_0 0.0))
                (= v0x7f547a83dbd0_0 (ite v0x7f547a83da90_0 1.0 0.0))
                (= v0x7f547a83e550_0 (= v0x7f547a83bc10_0 0.0))
                (= v0x7f547a83e910_0 (> v0x7f547a83cf10_0 1.0))
                (= v0x7f547a83ea50_0
                   (ite v0x7f547a83e910_0 1.0 v0x7f547a83bc10_0))
                (= v0x7f547a83ee50_0 (> v0x7f547a83cf10_0 0.0))
                (= v0x7f547a83ef90_0 (+ v0x7f547a83cf10_0 (- 1.0)))
                (= v0x7f547a83f150_0
                   (ite v0x7f547a83ee50_0 v0x7f547a83ef90_0 v0x7f547a83cf10_0))
                (= v0x7f547a83f290_0 (= v0x7f547a83f150_0 0.0))
                (= v0x7f547a840350_0 (= v0x7f547a83ddd0_0 0.0))
                (= v0x7f547a840490_0 (= v0x7f547a83f710_0 2.0))
                (= v0x7f547a8405d0_0 (= v0x7f547a83f7d0_0 0.0))
                (= v0x7f547a840710_0 (and v0x7f547a840490_0 v0x7f547a840350_0))
                (= v0x7f547a840850_0 (and v0x7f547a840710_0 v0x7f547a8405d0_0)))))
  (=> F0x7f547a8414d0 a!7))))
(assert (=> F0x7f547a8414d0 F0x7f547a841590))
(assert (let ((a!1 (=> v0x7f547a83ce50_0
               (or (and v0x7f547a83c710_0
                        E0x7f547a83cfd0
                        (<= v0x7f547a83cf10_0 v0x7f547a83cd10_0)
                        (>= v0x7f547a83cf10_0 v0x7f547a83cd10_0))
                   (and v0x7f547a83c450_0
                        E0x7f547a83d190
                        v0x7f547a83c5d0_0
                        (<= v0x7f547a83cf10_0 v0x7f547a83be90_0)
                        (>= v0x7f547a83cf10_0 v0x7f547a83be90_0)))))
      (a!2 (=> v0x7f547a83ce50_0
               (or (and E0x7f547a83cfd0 (not E0x7f547a83d190))
                   (and E0x7f547a83d190 (not E0x7f547a83cfd0)))))
      (a!3 (=> v0x7f547a83dd10_0
               (or (and v0x7f547a83d7d0_0
                        E0x7f547a83de90
                        (<= v0x7f547a83ddd0_0 v0x7f547a83dbd0_0)
                        (>= v0x7f547a83ddd0_0 v0x7f547a83dbd0_0))
                   (and v0x7f547a83ce50_0
                        E0x7f547a83e050
                        v0x7f547a83d690_0
                        (<= v0x7f547a83ddd0_0 v0x7f547a83bd90_0)
                        (>= v0x7f547a83ddd0_0 v0x7f547a83bd90_0)))))
      (a!4 (=> v0x7f547a83dd10_0
               (or (and E0x7f547a83de90 (not E0x7f547a83e050))
                   (and E0x7f547a83e050 (not E0x7f547a83de90)))))
      (a!5 (or (and v0x7f547a83e690_0
                    E0x7f547a83f890
                    (<= v0x7f547a83f710_0 v0x7f547a83cf10_0)
                    (>= v0x7f547a83f710_0 v0x7f547a83cf10_0)
                    (<= v0x7f547a83f7d0_0 v0x7f547a83ea50_0)
                    (>= v0x7f547a83f7d0_0 v0x7f547a83ea50_0))
               (and v0x7f547a83f3d0_0
                    E0x7f547a83fb50
                    (and (<= v0x7f547a83f710_0 v0x7f547a83f150_0)
                         (>= v0x7f547a83f710_0 v0x7f547a83f150_0))
                    (<= v0x7f547a83f7d0_0 v0x7f547a83bc10_0)
                    (>= v0x7f547a83f7d0_0 v0x7f547a83bc10_0))
               (and v0x7f547a83eb90_0
                    E0x7f547a83fe10
                    (not v0x7f547a83f290_0)
                    (and (<= v0x7f547a83f710_0 v0x7f547a83f150_0)
                         (>= v0x7f547a83f710_0 v0x7f547a83f150_0))
                    (<= v0x7f547a83f7d0_0 0.0)
                    (>= v0x7f547a83f7d0_0 0.0))))
      (a!6 (=> v0x7f547a83f650_0
               (or (and E0x7f547a83f890
                        (not E0x7f547a83fb50)
                        (not E0x7f547a83fe10))
                   (and E0x7f547a83fb50
                        (not E0x7f547a83f890)
                        (not E0x7f547a83fe10))
                   (and E0x7f547a83fe10
                        (not E0x7f547a83f890)
                        (not E0x7f547a83fb50))))))
(let ((a!7 (and (=> v0x7f547a83c710_0
                    (and v0x7f547a83c450_0
                         E0x7f547a83c7d0
                         (not v0x7f547a83c5d0_0)))
                (=> v0x7f547a83c710_0 E0x7f547a83c7d0)
                a!1
                a!2
                (=> v0x7f547a83d7d0_0
                    (and v0x7f547a83ce50_0
                         E0x7f547a83d890
                         (not v0x7f547a83d690_0)))
                (=> v0x7f547a83d7d0_0 E0x7f547a83d890)
                a!3
                a!4
                (=> v0x7f547a83e690_0
                    (and v0x7f547a83dd10_0 E0x7f547a83e750 v0x7f547a83e550_0))
                (=> v0x7f547a83e690_0 E0x7f547a83e750)
                (=> v0x7f547a83eb90_0
                    (and v0x7f547a83dd10_0
                         E0x7f547a83ec50
                         (not v0x7f547a83e550_0)))
                (=> v0x7f547a83eb90_0 E0x7f547a83ec50)
                (=> v0x7f547a83f3d0_0
                    (and v0x7f547a83eb90_0 E0x7f547a83f490 v0x7f547a83f290_0))
                (=> v0x7f547a83f3d0_0 E0x7f547a83f490)
                (=> v0x7f547a83f650_0 a!5)
                a!6
                v0x7f547a83f650_0
                v0x7f547a840850_0
                (= v0x7f547a83c5d0_0 (= v0x7f547a83c510_0 0.0))
                (= v0x7f547a83ca10_0 (< v0x7f547a83be90_0 2.0))
                (= v0x7f547a83cbd0_0 (ite v0x7f547a83ca10_0 1.0 0.0))
                (= v0x7f547a83cd10_0 (+ v0x7f547a83cbd0_0 v0x7f547a83be90_0))
                (= v0x7f547a83d690_0 (= v0x7f547a83d5d0_0 0.0))
                (= v0x7f547a83da90_0 (= v0x7f547a83bd90_0 0.0))
                (= v0x7f547a83dbd0_0 (ite v0x7f547a83da90_0 1.0 0.0))
                (= v0x7f547a83e550_0 (= v0x7f547a83bc10_0 0.0))
                (= v0x7f547a83e910_0 (> v0x7f547a83cf10_0 1.0))
                (= v0x7f547a83ea50_0
                   (ite v0x7f547a83e910_0 1.0 v0x7f547a83bc10_0))
                (= v0x7f547a83ee50_0 (> v0x7f547a83cf10_0 0.0))
                (= v0x7f547a83ef90_0 (+ v0x7f547a83cf10_0 (- 1.0)))
                (= v0x7f547a83f150_0
                   (ite v0x7f547a83ee50_0 v0x7f547a83ef90_0 v0x7f547a83cf10_0))
                (= v0x7f547a83f290_0 (= v0x7f547a83f150_0 0.0))
                (= v0x7f547a840350_0 (= v0x7f547a83ddd0_0 0.0))
                (= v0x7f547a840490_0 (= v0x7f547a83f710_0 2.0))
                (= v0x7f547a8405d0_0 (= v0x7f547a83f7d0_0 0.0))
                (= v0x7f547a840710_0 (and v0x7f547a840490_0 v0x7f547a840350_0))
                (= v0x7f547a840850_0 (and v0x7f547a840710_0 v0x7f547a8405d0_0)))))
  (=> F0x7f547a841650 a!7))))
(assert (=> F0x7f547a841650 F0x7f547a841590))
(assert (=> F0x7f547a841790 (or F0x7f547a841490 F0x7f547a8414d0)))
(assert (=> F0x7f547a841750 F0x7f547a841650))
(assert (=> pre!entry!0 (=> F0x7f547a841390 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f547a841590 (>= v0x7f547a83bc10_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f547a841590 (not (<= 3.0 v0x7f547a83be90_0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f547a841790 (>= v0x7f547a83be50_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f547a841790 (not (<= 3.0 v0x7f547a83a010_0)))))
(assert (= lemma!bb2.i.i26.i.i!0 (=> F0x7f547a841750 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i26.i.i!0) (not lemma!bb2.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f547a841390)
; (error: F0x7f547a841750)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i26.i.i!0)
