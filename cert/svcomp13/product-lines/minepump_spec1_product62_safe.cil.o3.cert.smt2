(declare-fun post!bb1.i.i43.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i43.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f3e765f13d0 () Bool)
(declare-fun v0x7f3e765eef90_0 () Bool)
(declare-fun v0x7f3e765eea90_0 () Bool)
(declare-fun v0x7f3e765ee950_0 () Bool)
(declare-fun F0x7f3e765f14d0 () Bool)
(declare-fun v0x7f3e765ee650_0 () Real)
(declare-fun v0x7f3e765ed290_0 () Bool)
(declare-fun v0x7f3e765ec210_0 () Bool)
(declare-fun v0x7f3e765ee510_0 () Bool)
(declare-fun E0x7f3e765efdd0 () Bool)
(declare-fun v0x7f3e765ee810_0 () Real)
(declare-fun F0x7f3e765f1310 () Bool)
(declare-fun v0x7f3e765eb690_0 () Real)
(declare-fun E0x7f3e765ef950 () Bool)
(declare-fun v0x7f3e765ef450_0 () Bool)
(declare-fun v0x7f3e765ef1d0_0 () Bool)
(declare-fun v0x7f3e765ebd10_0 () Real)
(declare-fun E0x7f3e765efbd0 () Bool)
(declare-fun v0x7f3e765ee110_0 () Bool)
(declare-fun v0x7f3e765eed10_0 () Bool)
(declare-fun v0x7f3e765ee250_0 () Bool)
(declare-fun v0x7f3e765edd50_0 () Bool)
(declare-fun E0x7f3e765edf50 () Bool)
(declare-fun v0x7f3e765ede90_0 () Bool)
(declare-fun F0x7f3e765f1510 () Bool)
(declare-fun v0x7f3e765eb410_0 () Real)
(declare-fun v0x7f3e765ed3d0_0 () Real)
(declare-fun v0x7f3e765ed5d0_0 () Real)
(declare-fun v0x7f3e765ece90_0 () Bool)
(declare-fun E0x7f3e765ed090 () Bool)
(declare-fun v0x7f3e765eb590_0 () Real)
(declare-fun E0x7f3e765ef290 () Bool)
(declare-fun v0x7f3e765ec650_0 () Bool)
(declare-fun v0x7f3e765ef510_0 () Real)
(declare-fun v0x7f3e765ef5d0_0 () Real)
(declare-fun E0x7f3e765ed690 () Bool)
(declare-fun E0x7f3e765ebfd0 () Bool)
(declare-fun v0x7f3e765ec3d0_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f3e765f1190 () Bool)
(declare-fun v0x7f3e765e8010_0 () Real)
(declare-fun v0x7f3e765f0390_0 () Bool)
(declare-fun v0x7f3e765eb750_0 () Real)
(declare-fun v0x7f3e765ebf10_0 () Bool)
(declare-fun E0x7f3e765ec990 () Bool)
(declare-fun v0x7f3e765f05d0_0 () Bool)
(declare-fun E0x7f3e765ed850 () Bool)
(declare-fun v0x7f3e765ebdd0_0 () Bool)
(declare-fun v0x7f3e765ecfd0_0 () Bool)
(declare-fun v0x7f3e765ec510_0 () Real)
(declare-fun v0x7f3e765eb650_0 () Real)
(declare-fun v0x7f3e765e8110_0 () Bool)
(declare-fun v0x7f3e765ec710_0 () Real)
(declare-fun E0x7f3e765eedd0 () Bool)
(declare-fun E0x7f3e765ef690 () Bool)
(declare-fun v0x7f3e765ed510_0 () Bool)
(declare-fun v0x7f3e765eebd0_0 () Bool)
(declare-fun v0x7f3e765ef090_0 () Real)
(declare-fun v0x7f3e765ecdd0_0 () Real)
(declare-fun E0x7f3e765ec7d0 () Bool)
(declare-fun v0x7f3e765f0490_0 () Bool)
(declare-fun v0x7f3e765ebc50_0 () Bool)
(declare-fun E0x7f3e765ee310 () Bool)
(declare-fun F0x7f3e765f1250 () Bool)
(declare-fun F0x7f3e765f10d0 () Bool)

(assert (=> F0x7f3e765f10d0
    (and v0x7f3e765e8110_0
         (<= v0x7f3e765eb650_0 0.0)
         (>= v0x7f3e765eb650_0 0.0)
         (<= v0x7f3e765eb750_0 1.0)
         (>= v0x7f3e765eb750_0 1.0)
         (<= v0x7f3e765e8010_0 0.0)
         (>= v0x7f3e765e8010_0 0.0))))
(assert (=> F0x7f3e765f10d0 F0x7f3e765f1190))
(assert (let ((a!1 (=> v0x7f3e765ec650_0
               (or (and v0x7f3e765ebf10_0
                        E0x7f3e765ec7d0
                        (<= v0x7f3e765ec710_0 v0x7f3e765ec510_0)
                        (>= v0x7f3e765ec710_0 v0x7f3e765ec510_0))
                   (and v0x7f3e765ebc50_0
                        E0x7f3e765ec990
                        v0x7f3e765ebdd0_0
                        (<= v0x7f3e765ec710_0 v0x7f3e765eb590_0)
                        (>= v0x7f3e765ec710_0 v0x7f3e765eb590_0)))))
      (a!2 (=> v0x7f3e765ec650_0
               (or (and E0x7f3e765ec7d0 (not E0x7f3e765ec990))
                   (and E0x7f3e765ec990 (not E0x7f3e765ec7d0)))))
      (a!3 (=> v0x7f3e765ed510_0
               (or (and v0x7f3e765ecfd0_0
                        E0x7f3e765ed690
                        (<= v0x7f3e765ed5d0_0 v0x7f3e765ed3d0_0)
                        (>= v0x7f3e765ed5d0_0 v0x7f3e765ed3d0_0))
                   (and v0x7f3e765ec650_0
                        E0x7f3e765ed850
                        v0x7f3e765ece90_0
                        (<= v0x7f3e765ed5d0_0 v0x7f3e765eb410_0)
                        (>= v0x7f3e765ed5d0_0 v0x7f3e765eb410_0)))))
      (a!4 (=> v0x7f3e765ed510_0
               (or (and E0x7f3e765ed690 (not E0x7f3e765ed850))
                   (and E0x7f3e765ed850 (not E0x7f3e765ed690)))))
      (a!5 (or (and v0x7f3e765eed10_0
                    E0x7f3e765ef690
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ec710_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ec710_0))
                    (<= v0x7f3e765ef5d0_0 v0x7f3e765ef090_0)
                    (>= v0x7f3e765ef5d0_0 v0x7f3e765ef090_0))
               (and v0x7f3e765ede90_0
                    E0x7f3e765ef950
                    (not v0x7f3e765ee110_0)
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ec710_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ec710_0))
                    (and (<= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)
                         (>= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)))
               (and v0x7f3e765ef1d0_0
                    E0x7f3e765efbd0
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ee810_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ee810_0))
                    (and (<= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)
                         (>= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)))
               (and v0x7f3e765ee250_0
                    E0x7f3e765efdd0
                    (not v0x7f3e765eebd0_0)
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ee810_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ee810_0))
                    (<= v0x7f3e765ef5d0_0 0.0)
                    (>= v0x7f3e765ef5d0_0 0.0))))
      (a!6 (=> v0x7f3e765ef450_0
               (or (and E0x7f3e765ef690
                        (not E0x7f3e765ef950)
                        (not E0x7f3e765efbd0)
                        (not E0x7f3e765efdd0))
                   (and E0x7f3e765ef950
                        (not E0x7f3e765ef690)
                        (not E0x7f3e765efbd0)
                        (not E0x7f3e765efdd0))
                   (and E0x7f3e765efbd0
                        (not E0x7f3e765ef690)
                        (not E0x7f3e765ef950)
                        (not E0x7f3e765efdd0))
                   (and E0x7f3e765efdd0
                        (not E0x7f3e765ef690)
                        (not E0x7f3e765ef950)
                        (not E0x7f3e765efbd0))))))
(let ((a!7 (and (=> v0x7f3e765ebf10_0
                    (and v0x7f3e765ebc50_0
                         E0x7f3e765ebfd0
                         (not v0x7f3e765ebdd0_0)))
                (=> v0x7f3e765ebf10_0 E0x7f3e765ebfd0)
                a!1
                a!2
                (=> v0x7f3e765ecfd0_0
                    (and v0x7f3e765ec650_0
                         E0x7f3e765ed090
                         (not v0x7f3e765ece90_0)))
                (=> v0x7f3e765ecfd0_0 E0x7f3e765ed090)
                a!3
                a!4
                (=> v0x7f3e765ede90_0
                    (and v0x7f3e765ed510_0 E0x7f3e765edf50 v0x7f3e765edd50_0))
                (=> v0x7f3e765ede90_0 E0x7f3e765edf50)
                (=> v0x7f3e765ee250_0
                    (and v0x7f3e765ed510_0
                         E0x7f3e765ee310
                         (not v0x7f3e765edd50_0)))
                (=> v0x7f3e765ee250_0 E0x7f3e765ee310)
                (=> v0x7f3e765eed10_0
                    (and v0x7f3e765ede90_0 E0x7f3e765eedd0 v0x7f3e765ee110_0))
                (=> v0x7f3e765eed10_0 E0x7f3e765eedd0)
                (=> v0x7f3e765ef1d0_0
                    (and v0x7f3e765ee250_0 E0x7f3e765ef290 v0x7f3e765eebd0_0))
                (=> v0x7f3e765ef1d0_0 E0x7f3e765ef290)
                (=> v0x7f3e765ef450_0 a!5)
                a!6
                v0x7f3e765ef450_0
                v0x7f3e765f05d0_0
                (<= v0x7f3e765eb650_0 v0x7f3e765ed5d0_0)
                (>= v0x7f3e765eb650_0 v0x7f3e765ed5d0_0)
                (<= v0x7f3e765eb750_0 v0x7f3e765ef510_0)
                (>= v0x7f3e765eb750_0 v0x7f3e765ef510_0)
                (<= v0x7f3e765e8010_0 v0x7f3e765ef5d0_0)
                (>= v0x7f3e765e8010_0 v0x7f3e765ef5d0_0)
                (= v0x7f3e765ebdd0_0 (= v0x7f3e765ebd10_0 0.0))
                (= v0x7f3e765ec210_0 (< v0x7f3e765eb590_0 2.0))
                (= v0x7f3e765ec3d0_0 (ite v0x7f3e765ec210_0 1.0 0.0))
                (= v0x7f3e765ec510_0 (+ v0x7f3e765ec3d0_0 v0x7f3e765eb590_0))
                (= v0x7f3e765ece90_0 (= v0x7f3e765ecdd0_0 0.0))
                (= v0x7f3e765ed290_0 (= v0x7f3e765eb410_0 0.0))
                (= v0x7f3e765ed3d0_0 (ite v0x7f3e765ed290_0 1.0 0.0))
                (= v0x7f3e765edd50_0 (= v0x7f3e765eb690_0 0.0))
                (= v0x7f3e765ee110_0 (> v0x7f3e765ec710_0 1.0))
                (= v0x7f3e765ee510_0 (> v0x7f3e765ec710_0 0.0))
                (= v0x7f3e765ee650_0 (+ v0x7f3e765ec710_0 (- 1.0)))
                (= v0x7f3e765ee810_0
                   (ite v0x7f3e765ee510_0 v0x7f3e765ee650_0 v0x7f3e765ec710_0))
                (= v0x7f3e765ee950_0 (= v0x7f3e765ed5d0_0 0.0))
                (= v0x7f3e765eea90_0 (= v0x7f3e765ee810_0 0.0))
                (= v0x7f3e765eebd0_0 (and v0x7f3e765ee950_0 v0x7f3e765eea90_0))
                (= v0x7f3e765eef90_0 (= v0x7f3e765ed5d0_0 0.0))
                (= v0x7f3e765ef090_0
                   (ite v0x7f3e765eef90_0 1.0 v0x7f3e765eb690_0))
                (= v0x7f3e765f0390_0 (= v0x7f3e765ed5d0_0 0.0))
                (= v0x7f3e765f0490_0 (= v0x7f3e765ef5d0_0 0.0))
                (= v0x7f3e765f05d0_0 (or v0x7f3e765f0490_0 v0x7f3e765f0390_0)))))
  (=> F0x7f3e765f1250 a!7))))
(assert (=> F0x7f3e765f1250 F0x7f3e765f1310))
(assert (let ((a!1 (=> v0x7f3e765ec650_0
               (or (and v0x7f3e765ebf10_0
                        E0x7f3e765ec7d0
                        (<= v0x7f3e765ec710_0 v0x7f3e765ec510_0)
                        (>= v0x7f3e765ec710_0 v0x7f3e765ec510_0))
                   (and v0x7f3e765ebc50_0
                        E0x7f3e765ec990
                        v0x7f3e765ebdd0_0
                        (<= v0x7f3e765ec710_0 v0x7f3e765eb590_0)
                        (>= v0x7f3e765ec710_0 v0x7f3e765eb590_0)))))
      (a!2 (=> v0x7f3e765ec650_0
               (or (and E0x7f3e765ec7d0 (not E0x7f3e765ec990))
                   (and E0x7f3e765ec990 (not E0x7f3e765ec7d0)))))
      (a!3 (=> v0x7f3e765ed510_0
               (or (and v0x7f3e765ecfd0_0
                        E0x7f3e765ed690
                        (<= v0x7f3e765ed5d0_0 v0x7f3e765ed3d0_0)
                        (>= v0x7f3e765ed5d0_0 v0x7f3e765ed3d0_0))
                   (and v0x7f3e765ec650_0
                        E0x7f3e765ed850
                        v0x7f3e765ece90_0
                        (<= v0x7f3e765ed5d0_0 v0x7f3e765eb410_0)
                        (>= v0x7f3e765ed5d0_0 v0x7f3e765eb410_0)))))
      (a!4 (=> v0x7f3e765ed510_0
               (or (and E0x7f3e765ed690 (not E0x7f3e765ed850))
                   (and E0x7f3e765ed850 (not E0x7f3e765ed690)))))
      (a!5 (or (and v0x7f3e765eed10_0
                    E0x7f3e765ef690
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ec710_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ec710_0))
                    (<= v0x7f3e765ef5d0_0 v0x7f3e765ef090_0)
                    (>= v0x7f3e765ef5d0_0 v0x7f3e765ef090_0))
               (and v0x7f3e765ede90_0
                    E0x7f3e765ef950
                    (not v0x7f3e765ee110_0)
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ec710_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ec710_0))
                    (and (<= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)
                         (>= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)))
               (and v0x7f3e765ef1d0_0
                    E0x7f3e765efbd0
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ee810_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ee810_0))
                    (and (<= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)
                         (>= v0x7f3e765ef5d0_0 v0x7f3e765eb690_0)))
               (and v0x7f3e765ee250_0
                    E0x7f3e765efdd0
                    (not v0x7f3e765eebd0_0)
                    (and (<= v0x7f3e765ef510_0 v0x7f3e765ee810_0)
                         (>= v0x7f3e765ef510_0 v0x7f3e765ee810_0))
                    (<= v0x7f3e765ef5d0_0 0.0)
                    (>= v0x7f3e765ef5d0_0 0.0))))
      (a!6 (=> v0x7f3e765ef450_0
               (or (and E0x7f3e765ef690
                        (not E0x7f3e765ef950)
                        (not E0x7f3e765efbd0)
                        (not E0x7f3e765efdd0))
                   (and E0x7f3e765ef950
                        (not E0x7f3e765ef690)
                        (not E0x7f3e765efbd0)
                        (not E0x7f3e765efdd0))
                   (and E0x7f3e765efbd0
                        (not E0x7f3e765ef690)
                        (not E0x7f3e765ef950)
                        (not E0x7f3e765efdd0))
                   (and E0x7f3e765efdd0
                        (not E0x7f3e765ef690)
                        (not E0x7f3e765ef950)
                        (not E0x7f3e765efbd0))))))
(let ((a!7 (and (=> v0x7f3e765ebf10_0
                    (and v0x7f3e765ebc50_0
                         E0x7f3e765ebfd0
                         (not v0x7f3e765ebdd0_0)))
                (=> v0x7f3e765ebf10_0 E0x7f3e765ebfd0)
                a!1
                a!2
                (=> v0x7f3e765ecfd0_0
                    (and v0x7f3e765ec650_0
                         E0x7f3e765ed090
                         (not v0x7f3e765ece90_0)))
                (=> v0x7f3e765ecfd0_0 E0x7f3e765ed090)
                a!3
                a!4
                (=> v0x7f3e765ede90_0
                    (and v0x7f3e765ed510_0 E0x7f3e765edf50 v0x7f3e765edd50_0))
                (=> v0x7f3e765ede90_0 E0x7f3e765edf50)
                (=> v0x7f3e765ee250_0
                    (and v0x7f3e765ed510_0
                         E0x7f3e765ee310
                         (not v0x7f3e765edd50_0)))
                (=> v0x7f3e765ee250_0 E0x7f3e765ee310)
                (=> v0x7f3e765eed10_0
                    (and v0x7f3e765ede90_0 E0x7f3e765eedd0 v0x7f3e765ee110_0))
                (=> v0x7f3e765eed10_0 E0x7f3e765eedd0)
                (=> v0x7f3e765ef1d0_0
                    (and v0x7f3e765ee250_0 E0x7f3e765ef290 v0x7f3e765eebd0_0))
                (=> v0x7f3e765ef1d0_0 E0x7f3e765ef290)
                (=> v0x7f3e765ef450_0 a!5)
                a!6
                v0x7f3e765ef450_0
                (not v0x7f3e765f05d0_0)
                (= v0x7f3e765ebdd0_0 (= v0x7f3e765ebd10_0 0.0))
                (= v0x7f3e765ec210_0 (< v0x7f3e765eb590_0 2.0))
                (= v0x7f3e765ec3d0_0 (ite v0x7f3e765ec210_0 1.0 0.0))
                (= v0x7f3e765ec510_0 (+ v0x7f3e765ec3d0_0 v0x7f3e765eb590_0))
                (= v0x7f3e765ece90_0 (= v0x7f3e765ecdd0_0 0.0))
                (= v0x7f3e765ed290_0 (= v0x7f3e765eb410_0 0.0))
                (= v0x7f3e765ed3d0_0 (ite v0x7f3e765ed290_0 1.0 0.0))
                (= v0x7f3e765edd50_0 (= v0x7f3e765eb690_0 0.0))
                (= v0x7f3e765ee110_0 (> v0x7f3e765ec710_0 1.0))
                (= v0x7f3e765ee510_0 (> v0x7f3e765ec710_0 0.0))
                (= v0x7f3e765ee650_0 (+ v0x7f3e765ec710_0 (- 1.0)))
                (= v0x7f3e765ee810_0
                   (ite v0x7f3e765ee510_0 v0x7f3e765ee650_0 v0x7f3e765ec710_0))
                (= v0x7f3e765ee950_0 (= v0x7f3e765ed5d0_0 0.0))
                (= v0x7f3e765eea90_0 (= v0x7f3e765ee810_0 0.0))
                (= v0x7f3e765eebd0_0 (and v0x7f3e765ee950_0 v0x7f3e765eea90_0))
                (= v0x7f3e765eef90_0 (= v0x7f3e765ed5d0_0 0.0))
                (= v0x7f3e765ef090_0
                   (ite v0x7f3e765eef90_0 1.0 v0x7f3e765eb690_0))
                (= v0x7f3e765f0390_0 (= v0x7f3e765ed5d0_0 0.0))
                (= v0x7f3e765f0490_0 (= v0x7f3e765ef5d0_0 0.0))
                (= v0x7f3e765f05d0_0 (or v0x7f3e765f0490_0 v0x7f3e765f0390_0)))))
  (=> F0x7f3e765f13d0 a!7))))
(assert (=> F0x7f3e765f13d0 F0x7f3e765f1310))
(assert (=> F0x7f3e765f1510 (or F0x7f3e765f10d0 F0x7f3e765f1250)))
(assert (=> F0x7f3e765f14d0 F0x7f3e765f13d0))
(assert (=> pre!entry!0 (=> F0x7f3e765f1190 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f3e765f1310 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f3e765f1510 true)))
(assert (= lemma!bb1.i.i43.i.i!0 (=> F0x7f3e765f14d0 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i43.i.i!0) (not lemma!bb1.i.i43.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f3e765f1190)
; (error: F0x7f3e765f14d0)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i43.i.i!0)
