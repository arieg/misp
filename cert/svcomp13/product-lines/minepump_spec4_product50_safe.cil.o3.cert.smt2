(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i26.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i26.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f6acf700250 () Bool)
(declare-fun v0x7f6acf6ff190_0 () Bool)
(declare-fun v0x7f6acf6fddd0_0 () Real)
(declare-fun v0x7f6acf6fd750_0 () Bool)
(declare-fun v0x7f6acf6fc150_0 () Real)
(declare-fun F0x7f6acf700050 () Bool)
(declare-fun v0x7f6acf6ff410_0 () Bool)
(declare-fun E0x7f6acf6fec50 () Bool)
(declare-fun v0x7f6acf6fc650_0 () Bool)
(declare-fun v0x7f6acf6fba90_0 () Real)
(declare-fun v0x7f6acf6fdf90_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f6acf6fe990 () Bool)
(declare-fun v0x7f6acf6fd890_0 () Real)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f6acf6fe610_0 () Real)
(declare-fun E0x7f6acf6fe6d0 () Bool)
(declare-fun E0x7f6acf6fe2d0 () Bool)
(declare-fun v0x7f6acf6fe210_0 () Bool)
(declare-fun F0x7f6acf700110 () Bool)
(declare-fun E0x7f6acf6fda90 () Bool)
(declare-fun v0x7f6acf6fbc10_0 () Real)
(declare-fun v0x7f6acf6fdc90_0 () Bool)
(declare-fun E0x7f6acf6fc410 () Bool)
(declare-fun E0x7f6acf6fcdd0 () Bool)
(declare-fun F0x7f6acf700210 () Bool)
(declare-fun v0x7f6acf6ff2d0_0 () Bool)
(declare-fun v0x7f6acf6fc090_0 () Bool)
(declare-fun v0x7f6acf6fc950_0 () Real)
(declare-fun E0x7f6acf6fcc10 () Bool)
(declare-fun v0x7f6acf6fcb50_0 () Real)
(declare-fun v0x7f6acf6fc810_0 () Real)
(declare-fun v0x7f6acf6fc350_0 () Bool)
(declare-fun v0x7f6acf6fe490_0 () Bool)
(declare-fun v0x7f6acf6fd390_0 () Bool)
(declare-fun v0x7f6acf6fd9d0_0 () Bool)
(declare-fun F0x7f6acf6ffe90 () Bool)
(declare-fun v0x7f6acf6fa010_0 () Real)
(declare-fun v0x7f6acf6fd4d0_0 () Bool)
(declare-fun v0x7f6acf6fe550_0 () Real)
(declare-fun v0x7f6acf6fe0d0_0 () Bool)
(declare-fun v0x7f6acf6fca90_0 () Bool)
(declare-fun v0x7f6acf6fbcd0_0 () Real)
(declare-fun F0x7f6acf6fff90 () Bool)
(declare-fun v0x7f6acf6fa110_0 () Bool)
(declare-fun F0x7f6acf6fff50 () Bool)
(declare-fun E0x7f6acf6fd590 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f6acf6fc210_0 () Bool)

(assert (=> F0x7f6acf6fff50
    (and v0x7f6acf6fa110_0
         (<= v0x7f6acf6fbcd0_0 0.0)
         (>= v0x7f6acf6fbcd0_0 0.0)
         (<= v0x7f6acf6fa010_0 1.0)
         (>= v0x7f6acf6fa010_0 1.0))))
(assert (=> F0x7f6acf6fff50 F0x7f6acf6ffe90))
(assert (let ((a!1 (=> v0x7f6acf6fca90_0
               (or (and v0x7f6acf6fc350_0
                        E0x7f6acf6fcc10
                        (<= v0x7f6acf6fcb50_0 v0x7f6acf6fc950_0)
                        (>= v0x7f6acf6fcb50_0 v0x7f6acf6fc950_0))
                   (and v0x7f6acf6fc090_0
                        E0x7f6acf6fcdd0
                        v0x7f6acf6fc210_0
                        (<= v0x7f6acf6fcb50_0 v0x7f6acf6fbc10_0)
                        (>= v0x7f6acf6fcb50_0 v0x7f6acf6fbc10_0)))))
      (a!2 (=> v0x7f6acf6fca90_0
               (or (and E0x7f6acf6fcc10 (not E0x7f6acf6fcdd0))
                   (and E0x7f6acf6fcdd0 (not E0x7f6acf6fcc10)))))
      (a!3 (or (and v0x7f6acf6fd4d0_0
                    E0x7f6acf6fe6d0
                    (<= v0x7f6acf6fe550_0 v0x7f6acf6fcb50_0)
                    (>= v0x7f6acf6fe550_0 v0x7f6acf6fcb50_0)
                    (<= v0x7f6acf6fe610_0 v0x7f6acf6fd890_0)
                    (>= v0x7f6acf6fe610_0 v0x7f6acf6fd890_0))
               (and v0x7f6acf6fe210_0
                    E0x7f6acf6fe990
                    (and (<= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0)
                         (>= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0))
                    (<= v0x7f6acf6fe610_0 v0x7f6acf6fba90_0)
                    (>= v0x7f6acf6fe610_0 v0x7f6acf6fba90_0))
               (and v0x7f6acf6fd9d0_0
                    E0x7f6acf6fec50
                    (not v0x7f6acf6fe0d0_0)
                    (and (<= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0)
                         (>= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0))
                    (<= v0x7f6acf6fe610_0 0.0)
                    (>= v0x7f6acf6fe610_0 0.0))))
      (a!4 (=> v0x7f6acf6fe490_0
               (or (and E0x7f6acf6fe6d0
                        (not E0x7f6acf6fe990)
                        (not E0x7f6acf6fec50))
                   (and E0x7f6acf6fe990
                        (not E0x7f6acf6fe6d0)
                        (not E0x7f6acf6fec50))
                   (and E0x7f6acf6fec50
                        (not E0x7f6acf6fe6d0)
                        (not E0x7f6acf6fe990))))))
(let ((a!5 (and (=> v0x7f6acf6fc350_0
                    (and v0x7f6acf6fc090_0
                         E0x7f6acf6fc410
                         (not v0x7f6acf6fc210_0)))
                (=> v0x7f6acf6fc350_0 E0x7f6acf6fc410)
                a!1
                a!2
                (=> v0x7f6acf6fd4d0_0
                    (and v0x7f6acf6fca90_0 E0x7f6acf6fd590 v0x7f6acf6fd390_0))
                (=> v0x7f6acf6fd4d0_0 E0x7f6acf6fd590)
                (=> v0x7f6acf6fd9d0_0
                    (and v0x7f6acf6fca90_0
                         E0x7f6acf6fda90
                         (not v0x7f6acf6fd390_0)))
                (=> v0x7f6acf6fd9d0_0 E0x7f6acf6fda90)
                (=> v0x7f6acf6fe210_0
                    (and v0x7f6acf6fd9d0_0 E0x7f6acf6fe2d0 v0x7f6acf6fe0d0_0))
                (=> v0x7f6acf6fe210_0 E0x7f6acf6fe2d0)
                (=> v0x7f6acf6fe490_0 a!3)
                a!4
                v0x7f6acf6fe490_0
                v0x7f6acf6ff410_0
                (<= v0x7f6acf6fbcd0_0 v0x7f6acf6fe610_0)
                (>= v0x7f6acf6fbcd0_0 v0x7f6acf6fe610_0)
                (<= v0x7f6acf6fa010_0 v0x7f6acf6fe550_0)
                (>= v0x7f6acf6fa010_0 v0x7f6acf6fe550_0)
                (= v0x7f6acf6fc210_0 (= v0x7f6acf6fc150_0 0.0))
                (= v0x7f6acf6fc650_0 (< v0x7f6acf6fbc10_0 2.0))
                (= v0x7f6acf6fc810_0 (ite v0x7f6acf6fc650_0 1.0 0.0))
                (= v0x7f6acf6fc950_0 (+ v0x7f6acf6fc810_0 v0x7f6acf6fbc10_0))
                (= v0x7f6acf6fd390_0 (= v0x7f6acf6fba90_0 0.0))
                (= v0x7f6acf6fd750_0 (> v0x7f6acf6fcb50_0 1.0))
                (= v0x7f6acf6fd890_0
                   (ite v0x7f6acf6fd750_0 1.0 v0x7f6acf6fba90_0))
                (= v0x7f6acf6fdc90_0 (> v0x7f6acf6fcb50_0 0.0))
                (= v0x7f6acf6fddd0_0 (+ v0x7f6acf6fcb50_0 (- 1.0)))
                (= v0x7f6acf6fdf90_0
                   (ite v0x7f6acf6fdc90_0 v0x7f6acf6fddd0_0 v0x7f6acf6fcb50_0))
                (= v0x7f6acf6fe0d0_0 (= v0x7f6acf6fdf90_0 0.0))
                (= v0x7f6acf6ff190_0 (not (= v0x7f6acf6fe550_0 0.0)))
                (= v0x7f6acf6ff2d0_0 (= v0x7f6acf6fe610_0 0.0))
                (= v0x7f6acf6ff410_0 (or v0x7f6acf6ff2d0_0 v0x7f6acf6ff190_0)))))
  (=> F0x7f6acf6fff90 a!5))))
(assert (=> F0x7f6acf6fff90 F0x7f6acf700050))
(assert (let ((a!1 (=> v0x7f6acf6fca90_0
               (or (and v0x7f6acf6fc350_0
                        E0x7f6acf6fcc10
                        (<= v0x7f6acf6fcb50_0 v0x7f6acf6fc950_0)
                        (>= v0x7f6acf6fcb50_0 v0x7f6acf6fc950_0))
                   (and v0x7f6acf6fc090_0
                        E0x7f6acf6fcdd0
                        v0x7f6acf6fc210_0
                        (<= v0x7f6acf6fcb50_0 v0x7f6acf6fbc10_0)
                        (>= v0x7f6acf6fcb50_0 v0x7f6acf6fbc10_0)))))
      (a!2 (=> v0x7f6acf6fca90_0
               (or (and E0x7f6acf6fcc10 (not E0x7f6acf6fcdd0))
                   (and E0x7f6acf6fcdd0 (not E0x7f6acf6fcc10)))))
      (a!3 (or (and v0x7f6acf6fd4d0_0
                    E0x7f6acf6fe6d0
                    (<= v0x7f6acf6fe550_0 v0x7f6acf6fcb50_0)
                    (>= v0x7f6acf6fe550_0 v0x7f6acf6fcb50_0)
                    (<= v0x7f6acf6fe610_0 v0x7f6acf6fd890_0)
                    (>= v0x7f6acf6fe610_0 v0x7f6acf6fd890_0))
               (and v0x7f6acf6fe210_0
                    E0x7f6acf6fe990
                    (and (<= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0)
                         (>= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0))
                    (<= v0x7f6acf6fe610_0 v0x7f6acf6fba90_0)
                    (>= v0x7f6acf6fe610_0 v0x7f6acf6fba90_0))
               (and v0x7f6acf6fd9d0_0
                    E0x7f6acf6fec50
                    (not v0x7f6acf6fe0d0_0)
                    (and (<= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0)
                         (>= v0x7f6acf6fe550_0 v0x7f6acf6fdf90_0))
                    (<= v0x7f6acf6fe610_0 0.0)
                    (>= v0x7f6acf6fe610_0 0.0))))
      (a!4 (=> v0x7f6acf6fe490_0
               (or (and E0x7f6acf6fe6d0
                        (not E0x7f6acf6fe990)
                        (not E0x7f6acf6fec50))
                   (and E0x7f6acf6fe990
                        (not E0x7f6acf6fe6d0)
                        (not E0x7f6acf6fec50))
                   (and E0x7f6acf6fec50
                        (not E0x7f6acf6fe6d0)
                        (not E0x7f6acf6fe990))))))
(let ((a!5 (and (=> v0x7f6acf6fc350_0
                    (and v0x7f6acf6fc090_0
                         E0x7f6acf6fc410
                         (not v0x7f6acf6fc210_0)))
                (=> v0x7f6acf6fc350_0 E0x7f6acf6fc410)
                a!1
                a!2
                (=> v0x7f6acf6fd4d0_0
                    (and v0x7f6acf6fca90_0 E0x7f6acf6fd590 v0x7f6acf6fd390_0))
                (=> v0x7f6acf6fd4d0_0 E0x7f6acf6fd590)
                (=> v0x7f6acf6fd9d0_0
                    (and v0x7f6acf6fca90_0
                         E0x7f6acf6fda90
                         (not v0x7f6acf6fd390_0)))
                (=> v0x7f6acf6fd9d0_0 E0x7f6acf6fda90)
                (=> v0x7f6acf6fe210_0
                    (and v0x7f6acf6fd9d0_0 E0x7f6acf6fe2d0 v0x7f6acf6fe0d0_0))
                (=> v0x7f6acf6fe210_0 E0x7f6acf6fe2d0)
                (=> v0x7f6acf6fe490_0 a!3)
                a!4
                v0x7f6acf6fe490_0
                (not v0x7f6acf6ff410_0)
                (= v0x7f6acf6fc210_0 (= v0x7f6acf6fc150_0 0.0))
                (= v0x7f6acf6fc650_0 (< v0x7f6acf6fbc10_0 2.0))
                (= v0x7f6acf6fc810_0 (ite v0x7f6acf6fc650_0 1.0 0.0))
                (= v0x7f6acf6fc950_0 (+ v0x7f6acf6fc810_0 v0x7f6acf6fbc10_0))
                (= v0x7f6acf6fd390_0 (= v0x7f6acf6fba90_0 0.0))
                (= v0x7f6acf6fd750_0 (> v0x7f6acf6fcb50_0 1.0))
                (= v0x7f6acf6fd890_0
                   (ite v0x7f6acf6fd750_0 1.0 v0x7f6acf6fba90_0))
                (= v0x7f6acf6fdc90_0 (> v0x7f6acf6fcb50_0 0.0))
                (= v0x7f6acf6fddd0_0 (+ v0x7f6acf6fcb50_0 (- 1.0)))
                (= v0x7f6acf6fdf90_0
                   (ite v0x7f6acf6fdc90_0 v0x7f6acf6fddd0_0 v0x7f6acf6fcb50_0))
                (= v0x7f6acf6fe0d0_0 (= v0x7f6acf6fdf90_0 0.0))
                (= v0x7f6acf6ff190_0 (not (= v0x7f6acf6fe550_0 0.0)))
                (= v0x7f6acf6ff2d0_0 (= v0x7f6acf6fe610_0 0.0))
                (= v0x7f6acf6ff410_0 (or v0x7f6acf6ff2d0_0 v0x7f6acf6ff190_0)))))
  (=> F0x7f6acf700110 a!5))))
(assert (=> F0x7f6acf700110 F0x7f6acf700050))
(assert (=> F0x7f6acf700250 (or F0x7f6acf6fff50 F0x7f6acf6fff90)))
(assert (=> F0x7f6acf700210 F0x7f6acf700110))
(assert (=> pre!entry!0 (=> F0x7f6acf6ffe90 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f6acf700050 (>= v0x7f6acf6fba90_0 0.0))))
(assert (let ((a!1 (=> F0x7f6acf700050
               (or (<= v0x7f6acf6fba90_0 0.0) (not (<= v0x7f6acf6fbc10_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f6acf700250 (>= v0x7f6acf6fbcd0_0 0.0))))
(assert (let ((a!1 (=> F0x7f6acf700250
               (or (<= v0x7f6acf6fbcd0_0 0.0) (not (<= v0x7f6acf6fa010_0 1.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i26.i.i!0 (=> F0x7f6acf700210 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i26.i.i!0) (not lemma!bb1.i.i26.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f6acf6ffe90)
; (error: F0x7f6acf700210)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i26.i.i!0)
