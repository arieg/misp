(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i16.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fafa7e82950_0 () Bool)
(declare-fun F0x7fafa7e839d0 () Bool)
(declare-fun v0x7fafa7e82810_0 () Bool)
(declare-fun v0x7fafa7e826d0_0 () Bool)
(declare-fun v0x7fafa7e80010_0 () Bool)
(declare-fun v0x7fafa7e7fb50_0 () Real)
(declare-fun v0x7fafa7e7ef90_0 () Bool)
(declare-fun v0x7fafa7e813d0_0 () Real)
(declare-fun v0x7fafa7e7ea90_0 () Real)
(declare-fun E0x7fafa7e820d0 () Bool)
(declare-fun E0x7fafa7e81e90 () Bool)
(declare-fun v0x7fafa7e81b10_0 () Real)
(declare-fun v0x7fafa7e81590_0 () Real)
(declare-fun v0x7fafa7e81a50_0 () Real)
(declare-fun E0x7fafa7e81bd0 () Bool)
(declare-fun E0x7fafa7e81790 () Bool)
(declare-fun v0x7fafa7e816d0_0 () Bool)
(declare-fun v0x7fafa7e80fd0_0 () Bool)
(declare-fun E0x7fafa7e80cd0 () Bool)
(declare-fun F0x7fafa7e837d0 () Bool)
(declare-fun v0x7fafa7e81290_0 () Bool)
(declare-fun v0x7fafa7e80e90_0 () Bool)
(declare-fun v0x7fafa7e80150_0 () Real)
(declare-fun v0x7fafa7e82590_0 () Bool)
(declare-fun v0x7fafa7e80c10_0 () Bool)
(declare-fun v0x7fafa7e80ad0_0 () Bool)
(declare-fun v0x7fafa7e82a90_0 () Bool)
(declare-fun v0x7fafa7e80290_0 () Bool)
(declare-fun v0x7fafa7e7fc10_0 () Bool)
(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun E0x7fafa7e7fe10 () Bool)
(declare-fun E0x7fafa7e80410 () Bool)
(declare-fun v0x7fafa7e7e310_0 () Real)
(declare-fun v0x7fafa7e7f490_0 () Real)
(declare-fun E0x7fafa7e7f550 () Bool)
(declare-fun v0x7fafa7e7eb50_0 () Bool)
(declare-fun E0x7fafa7e805d0 () Bool)
(declare-fun v0x7fafa7e7f3d0_0 () Bool)
(declare-fun E0x7fafa7e7ed50 () Bool)
(declare-fun E0x7fafa7e81090 () Bool)
(declare-fun v0x7fafa7e7f150_0 () Real)
(declare-fun F0x7fafa7e83710 () Bool)
(declare-fun F0x7fafa7e83650 () Bool)
(declare-fun v0x7fafa7e7e410_0 () Real)
(declare-fun E0x7fafa7e7f710 () Bool)
(declare-fun v0x7fafa7e7d010_0 () Real)
(declare-fun v0x7fafa7e81990_0 () Bool)
(declare-fun v0x7fafa7e7e4d0_0 () Real)
(declare-fun v0x7fafa7e7fd50_0 () Bool)
(declare-fun v0x7fafa7e7f290_0 () Real)
(declare-fun F0x7fafa7e83990 () Bool)
(declare-fun v0x7fafa7e7e9d0_0 () Bool)
(declare-fun F0x7fafa7e83890 () Bool)
(declare-fun v0x7fafa7e7e3d0_0 () Real)
(declare-fun v0x7fafa7e7e190_0 () Real)
(declare-fun v0x7fafa7e7d110_0 () Bool)
(declare-fun F0x7fafa7e83590 () Bool)
(declare-fun v0x7fafa7e80350_0 () Real)
(declare-fun v0x7fafa7e7ec90_0 () Bool)

(assert (=> F0x7fafa7e83590
    (and v0x7fafa7e7d110_0
         (<= v0x7fafa7e7e3d0_0 0.0)
         (>= v0x7fafa7e7e3d0_0 0.0)
         (<= v0x7fafa7e7e4d0_0 1.0)
         (>= v0x7fafa7e7e4d0_0 1.0)
         (<= v0x7fafa7e7d010_0 0.0)
         (>= v0x7fafa7e7d010_0 0.0))))
(assert (=> F0x7fafa7e83590 F0x7fafa7e83650))
(assert (let ((a!1 (=> v0x7fafa7e7f3d0_0
               (or (and v0x7fafa7e7ec90_0
                        E0x7fafa7e7f550
                        (<= v0x7fafa7e7f490_0 v0x7fafa7e7f290_0)
                        (>= v0x7fafa7e7f490_0 v0x7fafa7e7f290_0))
                   (and v0x7fafa7e7e9d0_0
                        E0x7fafa7e7f710
                        v0x7fafa7e7eb50_0
                        (<= v0x7fafa7e7f490_0 v0x7fafa7e7e310_0)
                        (>= v0x7fafa7e7f490_0 v0x7fafa7e7e310_0)))))
      (a!2 (=> v0x7fafa7e7f3d0_0
               (or (and E0x7fafa7e7f550 (not E0x7fafa7e7f710))
                   (and E0x7fafa7e7f710 (not E0x7fafa7e7f550)))))
      (a!3 (=> v0x7fafa7e80290_0
               (or (and v0x7fafa7e7fd50_0
                        E0x7fafa7e80410
                        (<= v0x7fafa7e80350_0 v0x7fafa7e80150_0)
                        (>= v0x7fafa7e80350_0 v0x7fafa7e80150_0))
                   (and v0x7fafa7e7f3d0_0
                        E0x7fafa7e805d0
                        v0x7fafa7e7fc10_0
                        (<= v0x7fafa7e80350_0 v0x7fafa7e7e190_0)
                        (>= v0x7fafa7e80350_0 v0x7fafa7e7e190_0)))))
      (a!4 (=> v0x7fafa7e80290_0
               (or (and E0x7fafa7e80410 (not E0x7fafa7e805d0))
                   (and E0x7fafa7e805d0 (not E0x7fafa7e80410)))))
      (a!5 (or (and v0x7fafa7e816d0_0
                    E0x7fafa7e81bd0
                    (and (<= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0)
                         (>= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0))
                    (and (<= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)
                         (>= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)))
               (and v0x7fafa7e80c10_0
                    E0x7fafa7e81e90
                    v0x7fafa7e80e90_0
                    (and (<= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0)
                         (>= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0))
                    (<= v0x7fafa7e81b10_0 1.0)
                    (>= v0x7fafa7e81b10_0 1.0))
               (and v0x7fafa7e80fd0_0
                    E0x7fafa7e820d0
                    (<= v0x7fafa7e81a50_0 v0x7fafa7e81590_0)
                    (>= v0x7fafa7e81a50_0 v0x7fafa7e81590_0)
                    (and (<= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)
                         (>= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)))))
      (a!6 (=> v0x7fafa7e81990_0
               (or (and E0x7fafa7e81bd0
                        (not E0x7fafa7e81e90)
                        (not E0x7fafa7e820d0))
                   (and E0x7fafa7e81e90
                        (not E0x7fafa7e81bd0)
                        (not E0x7fafa7e820d0))
                   (and E0x7fafa7e820d0
                        (not E0x7fafa7e81bd0)
                        (not E0x7fafa7e81e90))))))
(let ((a!7 (and (=> v0x7fafa7e7ec90_0
                    (and v0x7fafa7e7e9d0_0
                         E0x7fafa7e7ed50
                         (not v0x7fafa7e7eb50_0)))
                (=> v0x7fafa7e7ec90_0 E0x7fafa7e7ed50)
                a!1
                a!2
                (=> v0x7fafa7e7fd50_0
                    (and v0x7fafa7e7f3d0_0
                         E0x7fafa7e7fe10
                         (not v0x7fafa7e7fc10_0)))
                (=> v0x7fafa7e7fd50_0 E0x7fafa7e7fe10)
                a!3
                a!4
                (=> v0x7fafa7e80c10_0
                    (and v0x7fafa7e80290_0 E0x7fafa7e80cd0 v0x7fafa7e80ad0_0))
                (=> v0x7fafa7e80c10_0 E0x7fafa7e80cd0)
                (=> v0x7fafa7e80fd0_0
                    (and v0x7fafa7e80290_0
                         E0x7fafa7e81090
                         (not v0x7fafa7e80ad0_0)))
                (=> v0x7fafa7e80fd0_0 E0x7fafa7e81090)
                (=> v0x7fafa7e816d0_0
                    (and v0x7fafa7e80c10_0
                         E0x7fafa7e81790
                         (not v0x7fafa7e80e90_0)))
                (=> v0x7fafa7e816d0_0 E0x7fafa7e81790)
                (=> v0x7fafa7e81990_0 a!5)
                a!6
                v0x7fafa7e81990_0
                (not v0x7fafa7e82a90_0)
                (<= v0x7fafa7e7e3d0_0 v0x7fafa7e80350_0)
                (>= v0x7fafa7e7e3d0_0 v0x7fafa7e80350_0)
                (<= v0x7fafa7e7e4d0_0 v0x7fafa7e81a50_0)
                (>= v0x7fafa7e7e4d0_0 v0x7fafa7e81a50_0)
                (<= v0x7fafa7e7d010_0 v0x7fafa7e81b10_0)
                (>= v0x7fafa7e7d010_0 v0x7fafa7e81b10_0)
                (= v0x7fafa7e7eb50_0 (= v0x7fafa7e7ea90_0 0.0))
                (= v0x7fafa7e7ef90_0 (< v0x7fafa7e7e310_0 2.0))
                (= v0x7fafa7e7f150_0 (ite v0x7fafa7e7ef90_0 1.0 0.0))
                (= v0x7fafa7e7f290_0 (+ v0x7fafa7e7f150_0 v0x7fafa7e7e310_0))
                (= v0x7fafa7e7fc10_0 (= v0x7fafa7e7fb50_0 0.0))
                (= v0x7fafa7e80010_0 (= v0x7fafa7e7e190_0 0.0))
                (= v0x7fafa7e80150_0 (ite v0x7fafa7e80010_0 1.0 0.0))
                (= v0x7fafa7e80ad0_0 (= v0x7fafa7e7e410_0 0.0))
                (= v0x7fafa7e80e90_0 (> v0x7fafa7e7f490_0 1.0))
                (= v0x7fafa7e81290_0 (> v0x7fafa7e7f490_0 0.0))
                (= v0x7fafa7e813d0_0 (+ v0x7fafa7e7f490_0 (- 1.0)))
                (= v0x7fafa7e81590_0
                   (ite v0x7fafa7e81290_0 v0x7fafa7e813d0_0 v0x7fafa7e7f490_0))
                (= v0x7fafa7e82590_0 (= v0x7fafa7e80350_0 0.0))
                (= v0x7fafa7e826d0_0 (= v0x7fafa7e81a50_0 2.0))
                (= v0x7fafa7e82810_0 (= v0x7fafa7e81b10_0 0.0))
                (= v0x7fafa7e82950_0 (and v0x7fafa7e826d0_0 v0x7fafa7e82590_0))
                (= v0x7fafa7e82a90_0 (and v0x7fafa7e82950_0 v0x7fafa7e82810_0)))))
  (=> F0x7fafa7e83710 a!7))))
(assert (=> F0x7fafa7e83710 F0x7fafa7e837d0))
(assert (let ((a!1 (=> v0x7fafa7e7f3d0_0
               (or (and v0x7fafa7e7ec90_0
                        E0x7fafa7e7f550
                        (<= v0x7fafa7e7f490_0 v0x7fafa7e7f290_0)
                        (>= v0x7fafa7e7f490_0 v0x7fafa7e7f290_0))
                   (and v0x7fafa7e7e9d0_0
                        E0x7fafa7e7f710
                        v0x7fafa7e7eb50_0
                        (<= v0x7fafa7e7f490_0 v0x7fafa7e7e310_0)
                        (>= v0x7fafa7e7f490_0 v0x7fafa7e7e310_0)))))
      (a!2 (=> v0x7fafa7e7f3d0_0
               (or (and E0x7fafa7e7f550 (not E0x7fafa7e7f710))
                   (and E0x7fafa7e7f710 (not E0x7fafa7e7f550)))))
      (a!3 (=> v0x7fafa7e80290_0
               (or (and v0x7fafa7e7fd50_0
                        E0x7fafa7e80410
                        (<= v0x7fafa7e80350_0 v0x7fafa7e80150_0)
                        (>= v0x7fafa7e80350_0 v0x7fafa7e80150_0))
                   (and v0x7fafa7e7f3d0_0
                        E0x7fafa7e805d0
                        v0x7fafa7e7fc10_0
                        (<= v0x7fafa7e80350_0 v0x7fafa7e7e190_0)
                        (>= v0x7fafa7e80350_0 v0x7fafa7e7e190_0)))))
      (a!4 (=> v0x7fafa7e80290_0
               (or (and E0x7fafa7e80410 (not E0x7fafa7e805d0))
                   (and E0x7fafa7e805d0 (not E0x7fafa7e80410)))))
      (a!5 (or (and v0x7fafa7e816d0_0
                    E0x7fafa7e81bd0
                    (and (<= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0)
                         (>= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0))
                    (and (<= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)
                         (>= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)))
               (and v0x7fafa7e80c10_0
                    E0x7fafa7e81e90
                    v0x7fafa7e80e90_0
                    (and (<= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0)
                         (>= v0x7fafa7e81a50_0 v0x7fafa7e7f490_0))
                    (<= v0x7fafa7e81b10_0 1.0)
                    (>= v0x7fafa7e81b10_0 1.0))
               (and v0x7fafa7e80fd0_0
                    E0x7fafa7e820d0
                    (<= v0x7fafa7e81a50_0 v0x7fafa7e81590_0)
                    (>= v0x7fafa7e81a50_0 v0x7fafa7e81590_0)
                    (and (<= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)
                         (>= v0x7fafa7e81b10_0 v0x7fafa7e7e410_0)))))
      (a!6 (=> v0x7fafa7e81990_0
               (or (and E0x7fafa7e81bd0
                        (not E0x7fafa7e81e90)
                        (not E0x7fafa7e820d0))
                   (and E0x7fafa7e81e90
                        (not E0x7fafa7e81bd0)
                        (not E0x7fafa7e820d0))
                   (and E0x7fafa7e820d0
                        (not E0x7fafa7e81bd0)
                        (not E0x7fafa7e81e90))))))
(let ((a!7 (and (=> v0x7fafa7e7ec90_0
                    (and v0x7fafa7e7e9d0_0
                         E0x7fafa7e7ed50
                         (not v0x7fafa7e7eb50_0)))
                (=> v0x7fafa7e7ec90_0 E0x7fafa7e7ed50)
                a!1
                a!2
                (=> v0x7fafa7e7fd50_0
                    (and v0x7fafa7e7f3d0_0
                         E0x7fafa7e7fe10
                         (not v0x7fafa7e7fc10_0)))
                (=> v0x7fafa7e7fd50_0 E0x7fafa7e7fe10)
                a!3
                a!4
                (=> v0x7fafa7e80c10_0
                    (and v0x7fafa7e80290_0 E0x7fafa7e80cd0 v0x7fafa7e80ad0_0))
                (=> v0x7fafa7e80c10_0 E0x7fafa7e80cd0)
                (=> v0x7fafa7e80fd0_0
                    (and v0x7fafa7e80290_0
                         E0x7fafa7e81090
                         (not v0x7fafa7e80ad0_0)))
                (=> v0x7fafa7e80fd0_0 E0x7fafa7e81090)
                (=> v0x7fafa7e816d0_0
                    (and v0x7fafa7e80c10_0
                         E0x7fafa7e81790
                         (not v0x7fafa7e80e90_0)))
                (=> v0x7fafa7e816d0_0 E0x7fafa7e81790)
                (=> v0x7fafa7e81990_0 a!5)
                a!6
                v0x7fafa7e81990_0
                v0x7fafa7e82a90_0
                (= v0x7fafa7e7eb50_0 (= v0x7fafa7e7ea90_0 0.0))
                (= v0x7fafa7e7ef90_0 (< v0x7fafa7e7e310_0 2.0))
                (= v0x7fafa7e7f150_0 (ite v0x7fafa7e7ef90_0 1.0 0.0))
                (= v0x7fafa7e7f290_0 (+ v0x7fafa7e7f150_0 v0x7fafa7e7e310_0))
                (= v0x7fafa7e7fc10_0 (= v0x7fafa7e7fb50_0 0.0))
                (= v0x7fafa7e80010_0 (= v0x7fafa7e7e190_0 0.0))
                (= v0x7fafa7e80150_0 (ite v0x7fafa7e80010_0 1.0 0.0))
                (= v0x7fafa7e80ad0_0 (= v0x7fafa7e7e410_0 0.0))
                (= v0x7fafa7e80e90_0 (> v0x7fafa7e7f490_0 1.0))
                (= v0x7fafa7e81290_0 (> v0x7fafa7e7f490_0 0.0))
                (= v0x7fafa7e813d0_0 (+ v0x7fafa7e7f490_0 (- 1.0)))
                (= v0x7fafa7e81590_0
                   (ite v0x7fafa7e81290_0 v0x7fafa7e813d0_0 v0x7fafa7e7f490_0))
                (= v0x7fafa7e82590_0 (= v0x7fafa7e80350_0 0.0))
                (= v0x7fafa7e826d0_0 (= v0x7fafa7e81a50_0 2.0))
                (= v0x7fafa7e82810_0 (= v0x7fafa7e81b10_0 0.0))
                (= v0x7fafa7e82950_0 (and v0x7fafa7e826d0_0 v0x7fafa7e82590_0))
                (= v0x7fafa7e82a90_0 (and v0x7fafa7e82950_0 v0x7fafa7e82810_0)))))
  (=> F0x7fafa7e83890 a!7))))
(assert (=> F0x7fafa7e83890 F0x7fafa7e837d0))
(assert (=> F0x7fafa7e839d0 (or F0x7fafa7e83590 F0x7fafa7e83710)))
(assert (=> F0x7fafa7e83990 F0x7fafa7e83890))
(assert (=> pre!entry!0 (=> F0x7fafa7e83650 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fafa7e837d0 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fafa7e839d0 true)))
(assert (= lemma!bb2.i.i16.i.i!0 (=> F0x7fafa7e83990 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i16.i.i!0) (not lemma!bb2.i.i16.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7fafa7e83650)
; (error: F0x7fafa7e83990)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i16.i.i!0)
