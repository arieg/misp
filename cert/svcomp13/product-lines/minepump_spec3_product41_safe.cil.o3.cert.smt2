(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i21.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fecf35b4f10 () Bool)
(declare-fun F0x7fecf35b4f50 () Bool)
(declare-fun v0x7fecf35b3d90_0 () Bool)
(declare-fun v0x7fecf35b2c10_0 () Bool)
(declare-fun F0x7fecf35b4e10 () Bool)
(declare-fun v0x7fecf35b2050_0 () Bool)
(declare-fun v0x7fecf35afc10_0 () Real)
(declare-fun v0x7fecf35af590_0 () Real)
(declare-fun v0x7fecf35b3250_0 () Real)
(declare-fun E0x7fecf35b36d0 () Bool)
(declare-fun v0x7fecf35b3450_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fecf35b3390_0 () Bool)
(declare-fun v0x7fecf35b1190_0 () Bool)
(declare-fun v0x7fecf35b2d50_0 () Bool)
(declare-fun v0x7fecf35b0cd0_0 () Real)
(declare-fun E0x7fecf35b2f50 () Bool)
(declare-fun E0x7fecf35b2610 () Bool)
(declare-fun v0x7fecf35b1c50_0 () Bool)
(declare-fun E0x7fecf35b1e50 () Bool)
(declare-fun v0x7fecf35b1d90_0 () Bool)
(declare-fun v0x7fecf35b14d0_0 () Real)
(declare-fun post!bb2.i.i21.i.i!0 () Bool)
(declare-fun E0x7fecf35b1590 () Bool)
(declare-fun v0x7fecf35b2e90_0 () Bool)
(declare-fun v0x7fecf35af490_0 () Real)
(declare-fun v0x7fecf35b0d90_0 () Bool)
(declare-fun v0x7fecf35b0550_0 () Bool)
(declare-fun v0x7fecf35b3ed0_0 () Bool)
(declare-fun v0x7fecf35b0ed0_0 () Bool)
(declare-fun E0x7fecf35b0f90 () Bool)
(declare-fun v0x7fecf35b2550_0 () Real)
(declare-fun E0x7fecf35b06d0 () Bool)
(declare-fun E0x7fecf35b27d0 () Bool)
(declare-fun v0x7fecf35afcd0_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7fecf35b2350_0 () Real)
(declare-fun v0x7fecf35b1410_0 () Bool)
(declare-fun E0x7fecf35afed0 () Bool)
(declare-fun v0x7fecf35b3c50_0 () Bool)
(declare-fun v0x7fecf35afb50_0 () Bool)
(declare-fun v0x7fecf35af310_0 () Real)
(declare-fun F0x7fecf35b4bd0 () Bool)
(declare-fun v0x7fecf35b3110_0 () Bool)
(declare-fun v0x7fecf35b02d0_0 () Real)
(declare-fun v0x7fecf35ae010_0 () Real)
(declare-fun v0x7fecf35b0410_0 () Real)
(declare-fun v0x7fecf35b0110_0 () Bool)
(declare-fun F0x7fecf35b4c90 () Bool)
(declare-fun v0x7fecf35b2190_0 () Real)
(declare-fun E0x7fecf35b3510 () Bool)
(declare-fun v0x7fecf35af650_0 () Real)
(declare-fun v0x7fecf35af550_0 () Real)
(declare-fun v0x7fecf35ae110_0 () Bool)
(declare-fun E0x7fecf35b1750 () Bool)
(declare-fun E0x7fecf35b0890 () Bool)
(declare-fun v0x7fecf35b4010_0 () Bool)
(declare-fun v0x7fecf35b3b50_0 () Bool)
(declare-fun v0x7fecf35afe10_0 () Bool)
(declare-fun v0x7fecf35b2490_0 () Bool)
(declare-fun v0x7fecf35b12d0_0 () Real)
(declare-fun F0x7fecf35b4d50 () Bool)
(declare-fun F0x7fecf35b4b10 () Bool)
(declare-fun v0x7fecf35b0610_0 () Real)

(assert (=> F0x7fecf35b4b10
    (and v0x7fecf35ae110_0
         (<= v0x7fecf35af550_0 0.0)
         (>= v0x7fecf35af550_0 0.0)
         (<= v0x7fecf35af650_0 1.0)
         (>= v0x7fecf35af650_0 1.0)
         (<= v0x7fecf35ae010_0 0.0)
         (>= v0x7fecf35ae010_0 0.0))))
(assert (=> F0x7fecf35b4b10 F0x7fecf35b4bd0))
(assert (let ((a!1 (=> v0x7fecf35b0550_0
               (or (and v0x7fecf35afe10_0
                        E0x7fecf35b06d0
                        (<= v0x7fecf35b0610_0 v0x7fecf35b0410_0)
                        (>= v0x7fecf35b0610_0 v0x7fecf35b0410_0))
                   (and v0x7fecf35afb50_0
                        E0x7fecf35b0890
                        v0x7fecf35afcd0_0
                        (<= v0x7fecf35b0610_0 v0x7fecf35af490_0)
                        (>= v0x7fecf35b0610_0 v0x7fecf35af490_0)))))
      (a!2 (=> v0x7fecf35b0550_0
               (or (and E0x7fecf35b06d0 (not E0x7fecf35b0890))
                   (and E0x7fecf35b0890 (not E0x7fecf35b06d0)))))
      (a!3 (=> v0x7fecf35b1410_0
               (or (and v0x7fecf35b0ed0_0
                        E0x7fecf35b1590
                        (<= v0x7fecf35b14d0_0 v0x7fecf35b12d0_0)
                        (>= v0x7fecf35b14d0_0 v0x7fecf35b12d0_0))
                   (and v0x7fecf35b0550_0
                        E0x7fecf35b1750
                        v0x7fecf35b0d90_0
                        (<= v0x7fecf35b14d0_0 v0x7fecf35af310_0)
                        (>= v0x7fecf35b14d0_0 v0x7fecf35af310_0)))))
      (a!4 (=> v0x7fecf35b1410_0
               (or (and E0x7fecf35b1590 (not E0x7fecf35b1750))
                   (and E0x7fecf35b1750 (not E0x7fecf35b1590)))))
      (a!5 (=> v0x7fecf35b2490_0
               (or (and v0x7fecf35b1d90_0
                        E0x7fecf35b2610
                        (<= v0x7fecf35b2550_0 v0x7fecf35b2350_0)
                        (>= v0x7fecf35b2550_0 v0x7fecf35b2350_0))
                   (and v0x7fecf35b1410_0
                        E0x7fecf35b27d0
                        v0x7fecf35b1c50_0
                        (<= v0x7fecf35b2550_0 v0x7fecf35b0610_0)
                        (>= v0x7fecf35b2550_0 v0x7fecf35b0610_0)))))
      (a!6 (=> v0x7fecf35b2490_0
               (or (and E0x7fecf35b2610 (not E0x7fecf35b27d0))
                   (and E0x7fecf35b27d0 (not E0x7fecf35b2610)))))
      (a!7 (=> v0x7fecf35b3390_0
               (or (and v0x7fecf35b2e90_0
                        E0x7fecf35b3510
                        (<= v0x7fecf35b3450_0 v0x7fecf35b3250_0)
                        (>= v0x7fecf35b3450_0 v0x7fecf35b3250_0))
                   (and v0x7fecf35b2490_0
                        E0x7fecf35b36d0
                        (not v0x7fecf35b2d50_0)
                        (<= v0x7fecf35b3450_0 v0x7fecf35af590_0)
                        (>= v0x7fecf35b3450_0 v0x7fecf35af590_0)))))
      (a!8 (=> v0x7fecf35b3390_0
               (or (and E0x7fecf35b3510 (not E0x7fecf35b36d0))
                   (and E0x7fecf35b36d0 (not E0x7fecf35b3510))))))
(let ((a!9 (and (=> v0x7fecf35afe10_0
                    (and v0x7fecf35afb50_0
                         E0x7fecf35afed0
                         (not v0x7fecf35afcd0_0)))
                (=> v0x7fecf35afe10_0 E0x7fecf35afed0)
                a!1
                a!2
                (=> v0x7fecf35b0ed0_0
                    (and v0x7fecf35b0550_0
                         E0x7fecf35b0f90
                         (not v0x7fecf35b0d90_0)))
                (=> v0x7fecf35b0ed0_0 E0x7fecf35b0f90)
                a!3
                a!4
                (=> v0x7fecf35b1d90_0
                    (and v0x7fecf35b1410_0
                         E0x7fecf35b1e50
                         (not v0x7fecf35b1c50_0)))
                (=> v0x7fecf35b1d90_0 E0x7fecf35b1e50)
                a!5
                a!6
                (=> v0x7fecf35b2e90_0
                    (and v0x7fecf35b2490_0 E0x7fecf35b2f50 v0x7fecf35b2d50_0))
                (=> v0x7fecf35b2e90_0 E0x7fecf35b2f50)
                a!7
                a!8
                v0x7fecf35b3390_0
                (not v0x7fecf35b4010_0)
                (<= v0x7fecf35af550_0 v0x7fecf35b14d0_0)
                (>= v0x7fecf35af550_0 v0x7fecf35b14d0_0)
                (<= v0x7fecf35af650_0 v0x7fecf35b2550_0)
                (>= v0x7fecf35af650_0 v0x7fecf35b2550_0)
                (<= v0x7fecf35ae010_0 v0x7fecf35b3450_0)
                (>= v0x7fecf35ae010_0 v0x7fecf35b3450_0)
                (= v0x7fecf35afcd0_0 (= v0x7fecf35afc10_0 0.0))
                (= v0x7fecf35b0110_0 (< v0x7fecf35af490_0 2.0))
                (= v0x7fecf35b02d0_0 (ite v0x7fecf35b0110_0 1.0 0.0))
                (= v0x7fecf35b0410_0 (+ v0x7fecf35b02d0_0 v0x7fecf35af490_0))
                (= v0x7fecf35b0d90_0 (= v0x7fecf35b0cd0_0 0.0))
                (= v0x7fecf35b1190_0 (= v0x7fecf35af310_0 0.0))
                (= v0x7fecf35b12d0_0 (ite v0x7fecf35b1190_0 1.0 0.0))
                (= v0x7fecf35b1c50_0 (= v0x7fecf35af590_0 0.0))
                (= v0x7fecf35b2050_0 (> v0x7fecf35b0610_0 0.0))
                (= v0x7fecf35b2190_0 (+ v0x7fecf35b0610_0 (- 1.0)))
                (= v0x7fecf35b2350_0
                   (ite v0x7fecf35b2050_0 v0x7fecf35b2190_0 v0x7fecf35b0610_0))
                (= v0x7fecf35b2c10_0 (> v0x7fecf35b2550_0 1.0))
                (= v0x7fecf35b2d50_0 (and v0x7fecf35b2c10_0 v0x7fecf35b1c50_0))
                (= v0x7fecf35b3110_0 (= v0x7fecf35b14d0_0 0.0))
                (= v0x7fecf35b3250_0
                   (ite v0x7fecf35b3110_0 1.0 v0x7fecf35af590_0))
                (= v0x7fecf35b3b50_0 (= v0x7fecf35b14d0_0 0.0))
                (= v0x7fecf35b3c50_0 (= v0x7fecf35b2550_0 2.0))
                (= v0x7fecf35b3d90_0 (= v0x7fecf35b3450_0 0.0))
                (= v0x7fecf35b3ed0_0 (and v0x7fecf35b3c50_0 v0x7fecf35b3b50_0))
                (= v0x7fecf35b4010_0 (and v0x7fecf35b3ed0_0 v0x7fecf35b3d90_0)))))
  (=> F0x7fecf35b4c90 a!9))))
(assert (=> F0x7fecf35b4c90 F0x7fecf35b4d50))
(assert (let ((a!1 (=> v0x7fecf35b0550_0
               (or (and v0x7fecf35afe10_0
                        E0x7fecf35b06d0
                        (<= v0x7fecf35b0610_0 v0x7fecf35b0410_0)
                        (>= v0x7fecf35b0610_0 v0x7fecf35b0410_0))
                   (and v0x7fecf35afb50_0
                        E0x7fecf35b0890
                        v0x7fecf35afcd0_0
                        (<= v0x7fecf35b0610_0 v0x7fecf35af490_0)
                        (>= v0x7fecf35b0610_0 v0x7fecf35af490_0)))))
      (a!2 (=> v0x7fecf35b0550_0
               (or (and E0x7fecf35b06d0 (not E0x7fecf35b0890))
                   (and E0x7fecf35b0890 (not E0x7fecf35b06d0)))))
      (a!3 (=> v0x7fecf35b1410_0
               (or (and v0x7fecf35b0ed0_0
                        E0x7fecf35b1590
                        (<= v0x7fecf35b14d0_0 v0x7fecf35b12d0_0)
                        (>= v0x7fecf35b14d0_0 v0x7fecf35b12d0_0))
                   (and v0x7fecf35b0550_0
                        E0x7fecf35b1750
                        v0x7fecf35b0d90_0
                        (<= v0x7fecf35b14d0_0 v0x7fecf35af310_0)
                        (>= v0x7fecf35b14d0_0 v0x7fecf35af310_0)))))
      (a!4 (=> v0x7fecf35b1410_0
               (or (and E0x7fecf35b1590 (not E0x7fecf35b1750))
                   (and E0x7fecf35b1750 (not E0x7fecf35b1590)))))
      (a!5 (=> v0x7fecf35b2490_0
               (or (and v0x7fecf35b1d90_0
                        E0x7fecf35b2610
                        (<= v0x7fecf35b2550_0 v0x7fecf35b2350_0)
                        (>= v0x7fecf35b2550_0 v0x7fecf35b2350_0))
                   (and v0x7fecf35b1410_0
                        E0x7fecf35b27d0
                        v0x7fecf35b1c50_0
                        (<= v0x7fecf35b2550_0 v0x7fecf35b0610_0)
                        (>= v0x7fecf35b2550_0 v0x7fecf35b0610_0)))))
      (a!6 (=> v0x7fecf35b2490_0
               (or (and E0x7fecf35b2610 (not E0x7fecf35b27d0))
                   (and E0x7fecf35b27d0 (not E0x7fecf35b2610)))))
      (a!7 (=> v0x7fecf35b3390_0
               (or (and v0x7fecf35b2e90_0
                        E0x7fecf35b3510
                        (<= v0x7fecf35b3450_0 v0x7fecf35b3250_0)
                        (>= v0x7fecf35b3450_0 v0x7fecf35b3250_0))
                   (and v0x7fecf35b2490_0
                        E0x7fecf35b36d0
                        (not v0x7fecf35b2d50_0)
                        (<= v0x7fecf35b3450_0 v0x7fecf35af590_0)
                        (>= v0x7fecf35b3450_0 v0x7fecf35af590_0)))))
      (a!8 (=> v0x7fecf35b3390_0
               (or (and E0x7fecf35b3510 (not E0x7fecf35b36d0))
                   (and E0x7fecf35b36d0 (not E0x7fecf35b3510))))))
(let ((a!9 (and (=> v0x7fecf35afe10_0
                    (and v0x7fecf35afb50_0
                         E0x7fecf35afed0
                         (not v0x7fecf35afcd0_0)))
                (=> v0x7fecf35afe10_0 E0x7fecf35afed0)
                a!1
                a!2
                (=> v0x7fecf35b0ed0_0
                    (and v0x7fecf35b0550_0
                         E0x7fecf35b0f90
                         (not v0x7fecf35b0d90_0)))
                (=> v0x7fecf35b0ed0_0 E0x7fecf35b0f90)
                a!3
                a!4
                (=> v0x7fecf35b1d90_0
                    (and v0x7fecf35b1410_0
                         E0x7fecf35b1e50
                         (not v0x7fecf35b1c50_0)))
                (=> v0x7fecf35b1d90_0 E0x7fecf35b1e50)
                a!5
                a!6
                (=> v0x7fecf35b2e90_0
                    (and v0x7fecf35b2490_0 E0x7fecf35b2f50 v0x7fecf35b2d50_0))
                (=> v0x7fecf35b2e90_0 E0x7fecf35b2f50)
                a!7
                a!8
                v0x7fecf35b3390_0
                v0x7fecf35b4010_0
                (= v0x7fecf35afcd0_0 (= v0x7fecf35afc10_0 0.0))
                (= v0x7fecf35b0110_0 (< v0x7fecf35af490_0 2.0))
                (= v0x7fecf35b02d0_0 (ite v0x7fecf35b0110_0 1.0 0.0))
                (= v0x7fecf35b0410_0 (+ v0x7fecf35b02d0_0 v0x7fecf35af490_0))
                (= v0x7fecf35b0d90_0 (= v0x7fecf35b0cd0_0 0.0))
                (= v0x7fecf35b1190_0 (= v0x7fecf35af310_0 0.0))
                (= v0x7fecf35b12d0_0 (ite v0x7fecf35b1190_0 1.0 0.0))
                (= v0x7fecf35b1c50_0 (= v0x7fecf35af590_0 0.0))
                (= v0x7fecf35b2050_0 (> v0x7fecf35b0610_0 0.0))
                (= v0x7fecf35b2190_0 (+ v0x7fecf35b0610_0 (- 1.0)))
                (= v0x7fecf35b2350_0
                   (ite v0x7fecf35b2050_0 v0x7fecf35b2190_0 v0x7fecf35b0610_0))
                (= v0x7fecf35b2c10_0 (> v0x7fecf35b2550_0 1.0))
                (= v0x7fecf35b2d50_0 (and v0x7fecf35b2c10_0 v0x7fecf35b1c50_0))
                (= v0x7fecf35b3110_0 (= v0x7fecf35b14d0_0 0.0))
                (= v0x7fecf35b3250_0
                   (ite v0x7fecf35b3110_0 1.0 v0x7fecf35af590_0))
                (= v0x7fecf35b3b50_0 (= v0x7fecf35b14d0_0 0.0))
                (= v0x7fecf35b3c50_0 (= v0x7fecf35b2550_0 2.0))
                (= v0x7fecf35b3d90_0 (= v0x7fecf35b3450_0 0.0))
                (= v0x7fecf35b3ed0_0 (and v0x7fecf35b3c50_0 v0x7fecf35b3b50_0))
                (= v0x7fecf35b4010_0 (and v0x7fecf35b3ed0_0 v0x7fecf35b3d90_0)))))
  (=> F0x7fecf35b4e10 a!9))))
(assert (=> F0x7fecf35b4e10 F0x7fecf35b4d50))
(assert (=> F0x7fecf35b4f50 (or F0x7fecf35b4b10 F0x7fecf35b4c90)))
(assert (=> F0x7fecf35b4f10 F0x7fecf35b4e10))
(assert (=> pre!entry!0 (=> F0x7fecf35b4bd0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fecf35b4d50 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7fecf35b4f50 true)))
(assert (= lemma!bb2.i.i21.i.i!0 (=> F0x7fecf35b4f10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i21.i.i!0) (not lemma!bb2.i.i21.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7fecf35b4bd0)
; (error: F0x7fecf35b4f10)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i21.i.i!0)
