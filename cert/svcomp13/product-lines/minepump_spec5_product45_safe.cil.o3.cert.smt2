(declare-fun post!bb1.i.i!3 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!2 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb2.i.i31.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!2 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f99fdbc3ed0 () Bool)
(declare-fun lemma!bb1.i.i!3 () Bool)
(declare-fun F0x7f99fdbc3dd0 () Bool)
(declare-fun v0x7f99fdbc2e50_0 () Bool)
(declare-fun F0x7f99fdbc3e90 () Bool)
(declare-fun v0x7f99fdbc2a90_0 () Bool)
(declare-fun v0x7f99fdbc2d10_0 () Bool)
(declare-fun v0x7f99fdbc2bd0_0 () Bool)
(declare-fun v0x7f99fdbc0e90_0 () Bool)
(declare-fun v0x7f99fdbbeb90_0 () Bool)
(declare-fun v0x7f99fdbbe690_0 () Real)
(declare-fun v0x7f99fdbc2f90_0 () Bool)
(declare-fun E0x7f99fdbc22d0 () Bool)
(declare-fun E0x7f99fdbc2050 () Bool)
(declare-fun E0x7f99fdbc1990 () Bool)
(declare-fun v0x7f99fdbc18d0_0 () Bool)
(declare-fun v0x7f99fdbbfc10_0 () Bool)
(declare-fun v0x7f99fdbc1b50_0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f99fdbc0a90_0 () Bool)
(declare-fun v0x7f99fdbc1690_0 () Bool)
(declare-fun v0x7f99fdbc1410_0 () Bool)
(declare-fun v0x7f99fdbc06d0_0 () Bool)
(declare-fun E0x7f99fdbc08d0 () Bool)
(declare-fun v0x7f99fdbbdf10_0 () Real)
(declare-fun pre!bb1.i.i!4 () Bool)
(declare-fun v0x7f99fdbbf750_0 () Real)
(declare-fun E0x7f99fdbc01d0 () Bool)
(declare-fun E0x7f99fdbc24d0 () Bool)
(declare-fun v0x7f99fdbbfd50_0 () Real)
(declare-fun v0x7f99fdbc1c10_0 () Real)
(declare-fun post!bb1.i.i!2 () Bool)
(declare-fun v0x7f99fdbbed50_0 () Real)
(declare-fun E0x7f99fdbbfa10 () Bool)
(declare-fun v0x7f99fdbbf810_0 () Bool)
(declare-fun v0x7f99fdbbe010_0 () Real)
(declare-fun v0x7f99fdbc1190_0 () Real)
(declare-fun pre!bb1.i.i!3 () Bool)
(declare-fun E0x7f99fdbbf150 () Bool)
(declare-fun E0x7f99fdbc1d90 () Bool)
(declare-fun v0x7f99fdbbefd0_0 () Bool)
(declare-fun E0x7f99fdbc0c90 () Bool)
(declare-fun v0x7f99fdbc12d0_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f99fdbbff50_0 () Real)
(declare-fun v0x7f99fdbbf950_0 () Bool)
(declare-fun v0x7f99fdbbe750_0 () Bool)
(declare-fun E0x7f99fdbc0010 () Bool)
(declare-fun E0x7f99fdbbe950 () Bool)
(declare-fun v0x7f99fdbc1790_0 () Real)
(declare-fun lemma!bb1.i.i!4 () Bool)
(declare-fun E0x7f99fdbc14d0 () Bool)
(declare-fun E0x7f99fdbbf310 () Bool)
(declare-fun v0x7f99fdbbdd90_0 () Real)
(declare-fun F0x7f99fdbc3ad0 () Bool)
(declare-fun v0x7f99fdbbe890_0 () Bool)
(declare-fun F0x7f99fdbc3b90 () Bool)
(declare-fun v0x7f99fdbbf090_0 () Real)
(declare-fun F0x7f99fdbc3c50 () Bool)
(declare-fun v0x7f99fdbbc010_0 () Real)
(declare-fun v0x7f99fdbbee90_0 () Real)
(declare-fun v0x7f99fdbc0bd0_0 () Bool)
(declare-fun v0x7f99fdbbfe90_0 () Bool)
(declare-fun v0x7f99fdbbdfd0_0 () Real)
(declare-fun v0x7f99fdbbe0d0_0 () Real)
(declare-fun post!bb2.i.i31.i.i!0 () Bool)
(declare-fun v0x7f99fdbbe5d0_0 () Bool)
(declare-fun v0x7f99fdbc0fd0_0 () Real)
(declare-fun v0x7f99fdbc0810_0 () Bool)
(declare-fun v0x7f99fdbbc110_0 () Bool)
(declare-fun v0x7f99fdbc1cd0_0 () Real)
(declare-fun post!bb1.i.i!4 () Bool)
(declare-fun F0x7f99fdbc3d10 () Bool)

(assert (=> F0x7f99fdbc3d10
    (and v0x7f99fdbbc110_0
         (<= v0x7f99fdbbdfd0_0 0.0)
         (>= v0x7f99fdbbdfd0_0 0.0)
         (<= v0x7f99fdbbe0d0_0 0.0)
         (>= v0x7f99fdbbe0d0_0 0.0)
         (<= v0x7f99fdbbc010_0 1.0)
         (>= v0x7f99fdbbc010_0 1.0))))
(assert (=> F0x7f99fdbc3d10 F0x7f99fdbc3c50))
(assert (let ((a!1 (=> v0x7f99fdbbefd0_0
               (or (and v0x7f99fdbbe890_0
                        E0x7f99fdbbf150
                        (<= v0x7f99fdbbf090_0 v0x7f99fdbbee90_0)
                        (>= v0x7f99fdbbf090_0 v0x7f99fdbbee90_0))
                   (and v0x7f99fdbbe5d0_0
                        E0x7f99fdbbf310
                        v0x7f99fdbbe750_0
                        (<= v0x7f99fdbbf090_0 v0x7f99fdbbe010_0)
                        (>= v0x7f99fdbbf090_0 v0x7f99fdbbe010_0)))))
      (a!2 (=> v0x7f99fdbbefd0_0
               (or (and E0x7f99fdbbf150 (not E0x7f99fdbbf310))
                   (and E0x7f99fdbbf310 (not E0x7f99fdbbf150)))))
      (a!3 (=> v0x7f99fdbbfe90_0
               (or (and v0x7f99fdbbf950_0
                        E0x7f99fdbc0010
                        (<= v0x7f99fdbbff50_0 v0x7f99fdbbfd50_0)
                        (>= v0x7f99fdbbff50_0 v0x7f99fdbbfd50_0))
                   (and v0x7f99fdbbefd0_0
                        E0x7f99fdbc01d0
                        v0x7f99fdbbf810_0
                        (<= v0x7f99fdbbff50_0 v0x7f99fdbbdf10_0)
                        (>= v0x7f99fdbbff50_0 v0x7f99fdbbdf10_0)))))
      (a!4 (=> v0x7f99fdbbfe90_0
               (or (and E0x7f99fdbc0010 (not E0x7f99fdbc01d0))
                   (and E0x7f99fdbc01d0 (not E0x7f99fdbc0010)))))
      (a!5 (or (and v0x7f99fdbc1410_0
                    E0x7f99fdbc1d90
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0))
                    (<= v0x7f99fdbc1cd0_0 v0x7f99fdbc1790_0)
                    (>= v0x7f99fdbc1cd0_0 v0x7f99fdbc1790_0))
               (and v0x7f99fdbc0810_0
                    E0x7f99fdbc2050
                    (not v0x7f99fdbc0a90_0)
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0))
                    (and (<= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)
                         (>= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)))
               (and v0x7f99fdbc18d0_0
                    E0x7f99fdbc22d0
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0))
                    (and (<= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)
                         (>= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)))
               (and v0x7f99fdbc0bd0_0
                    E0x7f99fdbc24d0
                    (not v0x7f99fdbc12d0_0)
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0))
                    (<= v0x7f99fdbc1cd0_0 0.0)
                    (>= v0x7f99fdbc1cd0_0 0.0))))
      (a!6 (=> v0x7f99fdbc1b50_0
               (or (and E0x7f99fdbc1d90
                        (not E0x7f99fdbc2050)
                        (not E0x7f99fdbc22d0)
                        (not E0x7f99fdbc24d0))
                   (and E0x7f99fdbc2050
                        (not E0x7f99fdbc1d90)
                        (not E0x7f99fdbc22d0)
                        (not E0x7f99fdbc24d0))
                   (and E0x7f99fdbc22d0
                        (not E0x7f99fdbc1d90)
                        (not E0x7f99fdbc2050)
                        (not E0x7f99fdbc24d0))
                   (and E0x7f99fdbc24d0
                        (not E0x7f99fdbc1d90)
                        (not E0x7f99fdbc2050)
                        (not E0x7f99fdbc22d0))))))
(let ((a!7 (and (=> v0x7f99fdbbe890_0
                    (and v0x7f99fdbbe5d0_0
                         E0x7f99fdbbe950
                         (not v0x7f99fdbbe750_0)))
                (=> v0x7f99fdbbe890_0 E0x7f99fdbbe950)
                a!1
                a!2
                (=> v0x7f99fdbbf950_0
                    (and v0x7f99fdbbefd0_0
                         E0x7f99fdbbfa10
                         (not v0x7f99fdbbf810_0)))
                (=> v0x7f99fdbbf950_0 E0x7f99fdbbfa10)
                a!3
                a!4
                (=> v0x7f99fdbc0810_0
                    (and v0x7f99fdbbfe90_0 E0x7f99fdbc08d0 v0x7f99fdbc06d0_0))
                (=> v0x7f99fdbc0810_0 E0x7f99fdbc08d0)
                (=> v0x7f99fdbc0bd0_0
                    (and v0x7f99fdbbfe90_0
                         E0x7f99fdbc0c90
                         (not v0x7f99fdbc06d0_0)))
                (=> v0x7f99fdbc0bd0_0 E0x7f99fdbc0c90)
                (=> v0x7f99fdbc1410_0
                    (and v0x7f99fdbc0810_0 E0x7f99fdbc14d0 v0x7f99fdbc0a90_0))
                (=> v0x7f99fdbc1410_0 E0x7f99fdbc14d0)
                (=> v0x7f99fdbc18d0_0
                    (and v0x7f99fdbc0bd0_0 E0x7f99fdbc1990 v0x7f99fdbc12d0_0))
                (=> v0x7f99fdbc18d0_0 E0x7f99fdbc1990)
                (=> v0x7f99fdbc1b50_0 a!5)
                a!6
                v0x7f99fdbc1b50_0
                (not v0x7f99fdbc2f90_0)
                (<= v0x7f99fdbbdfd0_0 v0x7f99fdbc1cd0_0)
                (>= v0x7f99fdbbdfd0_0 v0x7f99fdbc1cd0_0)
                (<= v0x7f99fdbbe0d0_0 v0x7f99fdbbff50_0)
                (>= v0x7f99fdbbe0d0_0 v0x7f99fdbbff50_0)
                (<= v0x7f99fdbbc010_0 v0x7f99fdbc1c10_0)
                (>= v0x7f99fdbbc010_0 v0x7f99fdbc1c10_0)
                (= v0x7f99fdbbe750_0 (= v0x7f99fdbbe690_0 0.0))
                (= v0x7f99fdbbeb90_0 (< v0x7f99fdbbe010_0 2.0))
                (= v0x7f99fdbbed50_0 (ite v0x7f99fdbbeb90_0 1.0 0.0))
                (= v0x7f99fdbbee90_0 (+ v0x7f99fdbbed50_0 v0x7f99fdbbe010_0))
                (= v0x7f99fdbbf810_0 (= v0x7f99fdbbf750_0 0.0))
                (= v0x7f99fdbbfc10_0 (= v0x7f99fdbbdf10_0 0.0))
                (= v0x7f99fdbbfd50_0 (ite v0x7f99fdbbfc10_0 1.0 0.0))
                (= v0x7f99fdbc06d0_0 (= v0x7f99fdbbdd90_0 0.0))
                (= v0x7f99fdbc0a90_0 (> v0x7f99fdbbf090_0 1.0))
                (= v0x7f99fdbc0e90_0 (> v0x7f99fdbbf090_0 0.0))
                (= v0x7f99fdbc0fd0_0 (+ v0x7f99fdbbf090_0 (- 1.0)))
                (= v0x7f99fdbc1190_0
                   (ite v0x7f99fdbc0e90_0 v0x7f99fdbc0fd0_0 v0x7f99fdbbf090_0))
                (= v0x7f99fdbc12d0_0 (= v0x7f99fdbbff50_0 0.0))
                (= v0x7f99fdbc1690_0 (= v0x7f99fdbbff50_0 0.0))
                (= v0x7f99fdbc1790_0
                   (ite v0x7f99fdbc1690_0 1.0 v0x7f99fdbbdd90_0))
                (= v0x7f99fdbc2a90_0 (= v0x7f99fdbc1c10_0 2.0))
                (= v0x7f99fdbc2bd0_0 (= v0x7f99fdbc1cd0_0 0.0))
                (= v0x7f99fdbc2d10_0 (or v0x7f99fdbc2bd0_0 v0x7f99fdbc2a90_0))
                (= v0x7f99fdbc2e50_0 (xor v0x7f99fdbc2d10_0 true))
                (= v0x7f99fdbc2f90_0 (and v0x7f99fdbc06d0_0 v0x7f99fdbc2e50_0)))))
  (=> F0x7f99fdbc3b90 a!7))))
(assert (=> F0x7f99fdbc3b90 F0x7f99fdbc3ad0))
(assert (let ((a!1 (=> v0x7f99fdbbefd0_0
               (or (and v0x7f99fdbbe890_0
                        E0x7f99fdbbf150
                        (<= v0x7f99fdbbf090_0 v0x7f99fdbbee90_0)
                        (>= v0x7f99fdbbf090_0 v0x7f99fdbbee90_0))
                   (and v0x7f99fdbbe5d0_0
                        E0x7f99fdbbf310
                        v0x7f99fdbbe750_0
                        (<= v0x7f99fdbbf090_0 v0x7f99fdbbe010_0)
                        (>= v0x7f99fdbbf090_0 v0x7f99fdbbe010_0)))))
      (a!2 (=> v0x7f99fdbbefd0_0
               (or (and E0x7f99fdbbf150 (not E0x7f99fdbbf310))
                   (and E0x7f99fdbbf310 (not E0x7f99fdbbf150)))))
      (a!3 (=> v0x7f99fdbbfe90_0
               (or (and v0x7f99fdbbf950_0
                        E0x7f99fdbc0010
                        (<= v0x7f99fdbbff50_0 v0x7f99fdbbfd50_0)
                        (>= v0x7f99fdbbff50_0 v0x7f99fdbbfd50_0))
                   (and v0x7f99fdbbefd0_0
                        E0x7f99fdbc01d0
                        v0x7f99fdbbf810_0
                        (<= v0x7f99fdbbff50_0 v0x7f99fdbbdf10_0)
                        (>= v0x7f99fdbbff50_0 v0x7f99fdbbdf10_0)))))
      (a!4 (=> v0x7f99fdbbfe90_0
               (or (and E0x7f99fdbc0010 (not E0x7f99fdbc01d0))
                   (and E0x7f99fdbc01d0 (not E0x7f99fdbc0010)))))
      (a!5 (or (and v0x7f99fdbc1410_0
                    E0x7f99fdbc1d90
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0))
                    (<= v0x7f99fdbc1cd0_0 v0x7f99fdbc1790_0)
                    (>= v0x7f99fdbc1cd0_0 v0x7f99fdbc1790_0))
               (and v0x7f99fdbc0810_0
                    E0x7f99fdbc2050
                    (not v0x7f99fdbc0a90_0)
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbbf090_0))
                    (and (<= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)
                         (>= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)))
               (and v0x7f99fdbc18d0_0
                    E0x7f99fdbc22d0
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0))
                    (and (<= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)
                         (>= v0x7f99fdbc1cd0_0 v0x7f99fdbbdd90_0)))
               (and v0x7f99fdbc0bd0_0
                    E0x7f99fdbc24d0
                    (not v0x7f99fdbc12d0_0)
                    (and (<= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0)
                         (>= v0x7f99fdbc1c10_0 v0x7f99fdbc1190_0))
                    (<= v0x7f99fdbc1cd0_0 0.0)
                    (>= v0x7f99fdbc1cd0_0 0.0))))
      (a!6 (=> v0x7f99fdbc1b50_0
               (or (and E0x7f99fdbc1d90
                        (not E0x7f99fdbc2050)
                        (not E0x7f99fdbc22d0)
                        (not E0x7f99fdbc24d0))
                   (and E0x7f99fdbc2050
                        (not E0x7f99fdbc1d90)
                        (not E0x7f99fdbc22d0)
                        (not E0x7f99fdbc24d0))
                   (and E0x7f99fdbc22d0
                        (not E0x7f99fdbc1d90)
                        (not E0x7f99fdbc2050)
                        (not E0x7f99fdbc24d0))
                   (and E0x7f99fdbc24d0
                        (not E0x7f99fdbc1d90)
                        (not E0x7f99fdbc2050)
                        (not E0x7f99fdbc22d0))))))
(let ((a!7 (and (=> v0x7f99fdbbe890_0
                    (and v0x7f99fdbbe5d0_0
                         E0x7f99fdbbe950
                         (not v0x7f99fdbbe750_0)))
                (=> v0x7f99fdbbe890_0 E0x7f99fdbbe950)
                a!1
                a!2
                (=> v0x7f99fdbbf950_0
                    (and v0x7f99fdbbefd0_0
                         E0x7f99fdbbfa10
                         (not v0x7f99fdbbf810_0)))
                (=> v0x7f99fdbbf950_0 E0x7f99fdbbfa10)
                a!3
                a!4
                (=> v0x7f99fdbc0810_0
                    (and v0x7f99fdbbfe90_0 E0x7f99fdbc08d0 v0x7f99fdbc06d0_0))
                (=> v0x7f99fdbc0810_0 E0x7f99fdbc08d0)
                (=> v0x7f99fdbc0bd0_0
                    (and v0x7f99fdbbfe90_0
                         E0x7f99fdbc0c90
                         (not v0x7f99fdbc06d0_0)))
                (=> v0x7f99fdbc0bd0_0 E0x7f99fdbc0c90)
                (=> v0x7f99fdbc1410_0
                    (and v0x7f99fdbc0810_0 E0x7f99fdbc14d0 v0x7f99fdbc0a90_0))
                (=> v0x7f99fdbc1410_0 E0x7f99fdbc14d0)
                (=> v0x7f99fdbc18d0_0
                    (and v0x7f99fdbc0bd0_0 E0x7f99fdbc1990 v0x7f99fdbc12d0_0))
                (=> v0x7f99fdbc18d0_0 E0x7f99fdbc1990)
                (=> v0x7f99fdbc1b50_0 a!5)
                a!6
                v0x7f99fdbc1b50_0
                v0x7f99fdbc2f90_0
                (= v0x7f99fdbbe750_0 (= v0x7f99fdbbe690_0 0.0))
                (= v0x7f99fdbbeb90_0 (< v0x7f99fdbbe010_0 2.0))
                (= v0x7f99fdbbed50_0 (ite v0x7f99fdbbeb90_0 1.0 0.0))
                (= v0x7f99fdbbee90_0 (+ v0x7f99fdbbed50_0 v0x7f99fdbbe010_0))
                (= v0x7f99fdbbf810_0 (= v0x7f99fdbbf750_0 0.0))
                (= v0x7f99fdbbfc10_0 (= v0x7f99fdbbdf10_0 0.0))
                (= v0x7f99fdbbfd50_0 (ite v0x7f99fdbbfc10_0 1.0 0.0))
                (= v0x7f99fdbc06d0_0 (= v0x7f99fdbbdd90_0 0.0))
                (= v0x7f99fdbc0a90_0 (> v0x7f99fdbbf090_0 1.0))
                (= v0x7f99fdbc0e90_0 (> v0x7f99fdbbf090_0 0.0))
                (= v0x7f99fdbc0fd0_0 (+ v0x7f99fdbbf090_0 (- 1.0)))
                (= v0x7f99fdbc1190_0
                   (ite v0x7f99fdbc0e90_0 v0x7f99fdbc0fd0_0 v0x7f99fdbbf090_0))
                (= v0x7f99fdbc12d0_0 (= v0x7f99fdbbff50_0 0.0))
                (= v0x7f99fdbc1690_0 (= v0x7f99fdbbff50_0 0.0))
                (= v0x7f99fdbc1790_0
                   (ite v0x7f99fdbc1690_0 1.0 v0x7f99fdbbdd90_0))
                (= v0x7f99fdbc2a90_0 (= v0x7f99fdbc1c10_0 2.0))
                (= v0x7f99fdbc2bd0_0 (= v0x7f99fdbc1cd0_0 0.0))
                (= v0x7f99fdbc2d10_0 (or v0x7f99fdbc2bd0_0 v0x7f99fdbc2a90_0))
                (= v0x7f99fdbc2e50_0 (xor v0x7f99fdbc2d10_0 true))
                (= v0x7f99fdbc2f90_0 (and v0x7f99fdbc06d0_0 v0x7f99fdbc2e50_0)))))
  (=> F0x7f99fdbc3dd0 a!7))))
(assert (=> F0x7f99fdbc3dd0 F0x7f99fdbc3ad0))
(assert (=> F0x7f99fdbc3ed0 (or F0x7f99fdbc3d10 F0x7f99fdbc3b90)))
(assert (=> F0x7f99fdbc3e90 F0x7f99fdbc3dd0))
(assert (=> pre!entry!0 (=> F0x7f99fdbc3c50 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f99fdbc3ad0 (>= v0x7f99fdbbdf10_0 0.0))))
(assert (=> pre!bb1.i.i!1
    (=> F0x7f99fdbc3ad0
        (or (>= v0x7f99fdbbe010_0 2.0) (<= v0x7f99fdbbe010_0 1.0)))))
(assert (=> pre!bb1.i.i!2 (=> F0x7f99fdbc3ad0 (>= v0x7f99fdbbe010_0 0.0))))
(assert (=> pre!bb1.i.i!3
    (=> F0x7f99fdbc3ad0
        (or (<= v0x7f99fdbbe010_0 0.0) (>= v0x7f99fdbbe010_0 1.0)))))
(assert (=> pre!bb1.i.i!4 (=> F0x7f99fdbc3ad0 (<= v0x7f99fdbbe010_0 2.0))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f99fdbc3ed0 (>= v0x7f99fdbbe0d0_0 0.0))))
(assert (= lemma!bb1.i.i!1
   (=> F0x7f99fdbc3ed0
       (or (>= v0x7f99fdbbc010_0 2.0) (<= v0x7f99fdbbc010_0 1.0)))))
(assert (= lemma!bb1.i.i!2 (=> F0x7f99fdbc3ed0 (>= v0x7f99fdbbc010_0 0.0))))
(assert (= lemma!bb1.i.i!3
   (=> F0x7f99fdbc3ed0
       (or (<= v0x7f99fdbbc010_0 0.0) (>= v0x7f99fdbbc010_0 1.0)))))
(assert (= lemma!bb1.i.i!4 (=> F0x7f99fdbc3ed0 (<= v0x7f99fdbbc010_0 2.0))))
(assert (= lemma!bb2.i.i31.i.i!0 (=> F0x7f99fdbc3e90 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i!2) (not lemma!bb1.i.i!2))
    (and (not post!bb1.i.i!3) (not lemma!bb1.i.i!3))
    (and (not post!bb1.i.i!4) (not lemma!bb1.i.i!4))
    (and (not post!bb2.i.i31.i.i!0) (not lemma!bb2.i.i31.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1 pre!bb1.i.i!2 pre!bb1.i.i!3 pre!bb1.i.i!4)
; (init: F0x7f99fdbc3c50)
; (error: F0x7f99fdbc3e90)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i!2 post!bb1.i.i!3 post!bb1.i.i!4 post!bb2.i.i31.i.i!0)
