(declare-fun post!bb1.i.i34.i.i!0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f45f9c71c10 () Bool)
(declare-fun F0x7f45f9c71b10 () Bool)
(declare-fun v0x7f45f9c6f690_0 () Bool)
(declare-fun v0x7f45f9c6efd0_0 () Real)
(declare-fun v0x7f45f9c6dc10_0 () Bool)
(declare-fun v0x7f45f9c6cd50_0 () Real)
(declare-fun v0x7f45f9c6cb90_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f45f9c6c690_0 () Real)
(declare-fun v0x7f45f9c70d10_0 () Bool)
(declare-fun v0x7f45f9c6f190_0 () Real)
(declare-fun E0x7f45f9c70310 () Bool)
(declare-fun v0x7f45f9c6c010_0 () Real)
(declare-fun E0x7f45f9c70090 () Bool)
(declare-fun v0x7f45f9c6fd10_0 () Real)
(declare-fun E0x7f45f9c6fdd0 () Bool)
(declare-fun v0x7f45f9c6f7d0_0 () Real)
(declare-fun v0x7f45f9c6fb90_0 () Bool)
(declare-fun v0x7f45f9c6fc50_0 () Real)
(declare-fun F0x7f45f9c71c50 () Bool)
(declare-fun v0x7f45f9c6f2d0_0 () Bool)
(declare-fun v0x7f45f9c70bd0_0 () Bool)
(declare-fun E0x7f45f9c6f9d0 () Bool)
(declare-fun v0x7f45f9c6f910_0 () Bool)
(declare-fun v0x7f45f9c6ea90_0 () Bool)
(declare-fun v0x7f45f9c6e810_0 () Bool)
(declare-fun v0x7f45f9c6dd50_0 () Real)
(declare-fun v0x7f45f9c6d750_0 () Real)
(declare-fun E0x7f45f9c6e010 () Bool)
(declare-fun v0x7f45f9c6de90_0 () Bool)
(declare-fun v0x7f45f9c6ebd0_0 () Bool)
(declare-fun v0x7f45f9c6d810_0 () Bool)
(declare-fun v0x7f45f9c6ee90_0 () Bool)
(declare-fun F0x7f45f9c71a50 () Bool)
(declare-fun E0x7f45f9c6da10 () Bool)
(declare-fun v0x7f45f9c6e6d0_0 () Bool)
(declare-fun v0x7f45f9c6bf10_0 () Real)
(declare-fun E0x7f45f9c6d310 () Bool)
(declare-fun v0x7f45f9c6d090_0 () Real)
(declare-fun v0x7f45f9c6c750_0 () Bool)
(declare-fun E0x7f45f9c70510 () Bool)
(declare-fun v0x7f45f9c6ce90_0 () Real)
(declare-fun v0x7f45f9c6c5d0_0 () Bool)
(declare-fun v0x7f45f9c6bd90_0 () Real)
(declare-fun v0x7f45f9c6d950_0 () Bool)
(declare-fun v0x7f45f9c6c890_0 () Bool)
(declare-fun v0x7f45f9c6f410_0 () Bool)
(declare-fun v0x7f45f9c6cfd0_0 () Bool)
(declare-fun lemma!bb1.i.i34.i.i!0 () Bool)
(declare-fun E0x7f45f9c6e1d0 () Bool)
(declare-fun E0x7f45f9c6c950 () Bool)
(declare-fun F0x7f45f9c71890 () Bool)
(declare-fun v0x7f45f9c6a010_0 () Real)
(declare-fun v0x7f45f9c70ad0_0 () Bool)
(declare-fun E0x7f45f9c6d150 () Bool)
(declare-fun E0x7f45f9c6ec90 () Bool)
(declare-fun v0x7f45f9c6df50_0 () Real)
(declare-fun v0x7f45f9c6c0d0_0 () Real)
(declare-fun v0x7f45f9c6bfd0_0 () Real)
(declare-fun v0x7f45f9c6a110_0 () Bool)
(declare-fun F0x7f45f9c71990 () Bool)
(declare-fun E0x7f45f9c6e8d0 () Bool)
(declare-fun E0x7f45f9c6f4d0 () Bool)
(declare-fun F0x7f45f9c71950 () Bool)

(assert (=> F0x7f45f9c71950
    (and v0x7f45f9c6a110_0
         (<= v0x7f45f9c6bfd0_0 0.0)
         (>= v0x7f45f9c6bfd0_0 0.0)
         (<= v0x7f45f9c6c0d0_0 1.0)
         (>= v0x7f45f9c6c0d0_0 1.0)
         (<= v0x7f45f9c6a010_0 0.0)
         (>= v0x7f45f9c6a010_0 0.0))))
(assert (=> F0x7f45f9c71950 F0x7f45f9c71890))
(assert (let ((a!1 (=> v0x7f45f9c6cfd0_0
               (or (and v0x7f45f9c6c890_0
                        E0x7f45f9c6d150
                        (<= v0x7f45f9c6d090_0 v0x7f45f9c6ce90_0)
                        (>= v0x7f45f9c6d090_0 v0x7f45f9c6ce90_0))
                   (and v0x7f45f9c6c5d0_0
                        E0x7f45f9c6d310
                        v0x7f45f9c6c750_0
                        (<= v0x7f45f9c6d090_0 v0x7f45f9c6bf10_0)
                        (>= v0x7f45f9c6d090_0 v0x7f45f9c6bf10_0)))))
      (a!2 (=> v0x7f45f9c6cfd0_0
               (or (and E0x7f45f9c6d150 (not E0x7f45f9c6d310))
                   (and E0x7f45f9c6d310 (not E0x7f45f9c6d150)))))
      (a!3 (=> v0x7f45f9c6de90_0
               (or (and v0x7f45f9c6d950_0
                        E0x7f45f9c6e010
                        (<= v0x7f45f9c6df50_0 v0x7f45f9c6dd50_0)
                        (>= v0x7f45f9c6df50_0 v0x7f45f9c6dd50_0))
                   (and v0x7f45f9c6cfd0_0
                        E0x7f45f9c6e1d0
                        v0x7f45f9c6d810_0
                        (<= v0x7f45f9c6df50_0 v0x7f45f9c6bd90_0)
                        (>= v0x7f45f9c6df50_0 v0x7f45f9c6bd90_0)))))
      (a!4 (=> v0x7f45f9c6de90_0
               (or (and E0x7f45f9c6e010 (not E0x7f45f9c6e1d0))
                   (and E0x7f45f9c6e1d0 (not E0x7f45f9c6e010)))))
      (a!5 (or (and v0x7f45f9c6f410_0
                    E0x7f45f9c6fdd0
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0))
                    (<= v0x7f45f9c6fd10_0 v0x7f45f9c6f7d0_0)
                    (>= v0x7f45f9c6fd10_0 v0x7f45f9c6f7d0_0))
               (and v0x7f45f9c6e810_0
                    E0x7f45f9c70090
                    (not v0x7f45f9c6ea90_0)
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0))
                    (and (<= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)
                         (>= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)))
               (and v0x7f45f9c6f910_0
                    E0x7f45f9c70310
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0))
                    (and (<= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)
                         (>= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)))
               (and v0x7f45f9c6ebd0_0
                    E0x7f45f9c70510
                    (not v0x7f45f9c6f2d0_0)
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0))
                    (<= v0x7f45f9c6fd10_0 0.0)
                    (>= v0x7f45f9c6fd10_0 0.0))))
      (a!6 (=> v0x7f45f9c6fb90_0
               (or (and E0x7f45f9c6fdd0
                        (not E0x7f45f9c70090)
                        (not E0x7f45f9c70310)
                        (not E0x7f45f9c70510))
                   (and E0x7f45f9c70090
                        (not E0x7f45f9c6fdd0)
                        (not E0x7f45f9c70310)
                        (not E0x7f45f9c70510))
                   (and E0x7f45f9c70310
                        (not E0x7f45f9c6fdd0)
                        (not E0x7f45f9c70090)
                        (not E0x7f45f9c70510))
                   (and E0x7f45f9c70510
                        (not E0x7f45f9c6fdd0)
                        (not E0x7f45f9c70090)
                        (not E0x7f45f9c70310))))))
(let ((a!7 (and (=> v0x7f45f9c6c890_0
                    (and v0x7f45f9c6c5d0_0
                         E0x7f45f9c6c950
                         (not v0x7f45f9c6c750_0)))
                (=> v0x7f45f9c6c890_0 E0x7f45f9c6c950)
                a!1
                a!2
                (=> v0x7f45f9c6d950_0
                    (and v0x7f45f9c6cfd0_0
                         E0x7f45f9c6da10
                         (not v0x7f45f9c6d810_0)))
                (=> v0x7f45f9c6d950_0 E0x7f45f9c6da10)
                a!3
                a!4
                (=> v0x7f45f9c6e810_0
                    (and v0x7f45f9c6de90_0 E0x7f45f9c6e8d0 v0x7f45f9c6e6d0_0))
                (=> v0x7f45f9c6e810_0 E0x7f45f9c6e8d0)
                (=> v0x7f45f9c6ebd0_0
                    (and v0x7f45f9c6de90_0
                         E0x7f45f9c6ec90
                         (not v0x7f45f9c6e6d0_0)))
                (=> v0x7f45f9c6ebd0_0 E0x7f45f9c6ec90)
                (=> v0x7f45f9c6f410_0
                    (and v0x7f45f9c6e810_0 E0x7f45f9c6f4d0 v0x7f45f9c6ea90_0))
                (=> v0x7f45f9c6f410_0 E0x7f45f9c6f4d0)
                (=> v0x7f45f9c6f910_0
                    (and v0x7f45f9c6ebd0_0 E0x7f45f9c6f9d0 v0x7f45f9c6f2d0_0))
                (=> v0x7f45f9c6f910_0 E0x7f45f9c6f9d0)
                (=> v0x7f45f9c6fb90_0 a!5)
                a!6
                v0x7f45f9c6fb90_0
                v0x7f45f9c70d10_0
                (<= v0x7f45f9c6bfd0_0 v0x7f45f9c6df50_0)
                (>= v0x7f45f9c6bfd0_0 v0x7f45f9c6df50_0)
                (<= v0x7f45f9c6c0d0_0 v0x7f45f9c6fc50_0)
                (>= v0x7f45f9c6c0d0_0 v0x7f45f9c6fc50_0)
                (<= v0x7f45f9c6a010_0 v0x7f45f9c6fd10_0)
                (>= v0x7f45f9c6a010_0 v0x7f45f9c6fd10_0)
                (= v0x7f45f9c6c750_0 (= v0x7f45f9c6c690_0 0.0))
                (= v0x7f45f9c6cb90_0 (< v0x7f45f9c6bf10_0 2.0))
                (= v0x7f45f9c6cd50_0 (ite v0x7f45f9c6cb90_0 1.0 0.0))
                (= v0x7f45f9c6ce90_0 (+ v0x7f45f9c6cd50_0 v0x7f45f9c6bf10_0))
                (= v0x7f45f9c6d810_0 (= v0x7f45f9c6d750_0 0.0))
                (= v0x7f45f9c6dc10_0 (= v0x7f45f9c6bd90_0 0.0))
                (= v0x7f45f9c6dd50_0 (ite v0x7f45f9c6dc10_0 1.0 0.0))
                (= v0x7f45f9c6e6d0_0 (= v0x7f45f9c6c010_0 0.0))
                (= v0x7f45f9c6ea90_0 (> v0x7f45f9c6d090_0 1.0))
                (= v0x7f45f9c6ee90_0 (> v0x7f45f9c6d090_0 0.0))
                (= v0x7f45f9c6efd0_0 (+ v0x7f45f9c6d090_0 (- 1.0)))
                (= v0x7f45f9c6f190_0
                   (ite v0x7f45f9c6ee90_0 v0x7f45f9c6efd0_0 v0x7f45f9c6d090_0))
                (= v0x7f45f9c6f2d0_0 (= v0x7f45f9c6f190_0 0.0))
                (= v0x7f45f9c6f690_0 (= v0x7f45f9c6df50_0 0.0))
                (= v0x7f45f9c6f7d0_0
                   (ite v0x7f45f9c6f690_0 1.0 v0x7f45f9c6c010_0))
                (= v0x7f45f9c70ad0_0 (= v0x7f45f9c6df50_0 0.0))
                (= v0x7f45f9c70bd0_0 (= v0x7f45f9c6fd10_0 0.0))
                (= v0x7f45f9c70d10_0 (or v0x7f45f9c70bd0_0 v0x7f45f9c70ad0_0)))))
  (=> F0x7f45f9c71990 a!7))))
(assert (=> F0x7f45f9c71990 F0x7f45f9c71a50))
(assert (let ((a!1 (=> v0x7f45f9c6cfd0_0
               (or (and v0x7f45f9c6c890_0
                        E0x7f45f9c6d150
                        (<= v0x7f45f9c6d090_0 v0x7f45f9c6ce90_0)
                        (>= v0x7f45f9c6d090_0 v0x7f45f9c6ce90_0))
                   (and v0x7f45f9c6c5d0_0
                        E0x7f45f9c6d310
                        v0x7f45f9c6c750_0
                        (<= v0x7f45f9c6d090_0 v0x7f45f9c6bf10_0)
                        (>= v0x7f45f9c6d090_0 v0x7f45f9c6bf10_0)))))
      (a!2 (=> v0x7f45f9c6cfd0_0
               (or (and E0x7f45f9c6d150 (not E0x7f45f9c6d310))
                   (and E0x7f45f9c6d310 (not E0x7f45f9c6d150)))))
      (a!3 (=> v0x7f45f9c6de90_0
               (or (and v0x7f45f9c6d950_0
                        E0x7f45f9c6e010
                        (<= v0x7f45f9c6df50_0 v0x7f45f9c6dd50_0)
                        (>= v0x7f45f9c6df50_0 v0x7f45f9c6dd50_0))
                   (and v0x7f45f9c6cfd0_0
                        E0x7f45f9c6e1d0
                        v0x7f45f9c6d810_0
                        (<= v0x7f45f9c6df50_0 v0x7f45f9c6bd90_0)
                        (>= v0x7f45f9c6df50_0 v0x7f45f9c6bd90_0)))))
      (a!4 (=> v0x7f45f9c6de90_0
               (or (and E0x7f45f9c6e010 (not E0x7f45f9c6e1d0))
                   (and E0x7f45f9c6e1d0 (not E0x7f45f9c6e010)))))
      (a!5 (or (and v0x7f45f9c6f410_0
                    E0x7f45f9c6fdd0
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0))
                    (<= v0x7f45f9c6fd10_0 v0x7f45f9c6f7d0_0)
                    (>= v0x7f45f9c6fd10_0 v0x7f45f9c6f7d0_0))
               (and v0x7f45f9c6e810_0
                    E0x7f45f9c70090
                    (not v0x7f45f9c6ea90_0)
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6d090_0))
                    (and (<= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)
                         (>= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)))
               (and v0x7f45f9c6f910_0
                    E0x7f45f9c70310
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0))
                    (and (<= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)
                         (>= v0x7f45f9c6fd10_0 v0x7f45f9c6c010_0)))
               (and v0x7f45f9c6ebd0_0
                    E0x7f45f9c70510
                    (not v0x7f45f9c6f2d0_0)
                    (and (<= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0)
                         (>= v0x7f45f9c6fc50_0 v0x7f45f9c6f190_0))
                    (<= v0x7f45f9c6fd10_0 0.0)
                    (>= v0x7f45f9c6fd10_0 0.0))))
      (a!6 (=> v0x7f45f9c6fb90_0
               (or (and E0x7f45f9c6fdd0
                        (not E0x7f45f9c70090)
                        (not E0x7f45f9c70310)
                        (not E0x7f45f9c70510))
                   (and E0x7f45f9c70090
                        (not E0x7f45f9c6fdd0)
                        (not E0x7f45f9c70310)
                        (not E0x7f45f9c70510))
                   (and E0x7f45f9c70310
                        (not E0x7f45f9c6fdd0)
                        (not E0x7f45f9c70090)
                        (not E0x7f45f9c70510))
                   (and E0x7f45f9c70510
                        (not E0x7f45f9c6fdd0)
                        (not E0x7f45f9c70090)
                        (not E0x7f45f9c70310))))))
(let ((a!7 (and (=> v0x7f45f9c6c890_0
                    (and v0x7f45f9c6c5d0_0
                         E0x7f45f9c6c950
                         (not v0x7f45f9c6c750_0)))
                (=> v0x7f45f9c6c890_0 E0x7f45f9c6c950)
                a!1
                a!2
                (=> v0x7f45f9c6d950_0
                    (and v0x7f45f9c6cfd0_0
                         E0x7f45f9c6da10
                         (not v0x7f45f9c6d810_0)))
                (=> v0x7f45f9c6d950_0 E0x7f45f9c6da10)
                a!3
                a!4
                (=> v0x7f45f9c6e810_0
                    (and v0x7f45f9c6de90_0 E0x7f45f9c6e8d0 v0x7f45f9c6e6d0_0))
                (=> v0x7f45f9c6e810_0 E0x7f45f9c6e8d0)
                (=> v0x7f45f9c6ebd0_0
                    (and v0x7f45f9c6de90_0
                         E0x7f45f9c6ec90
                         (not v0x7f45f9c6e6d0_0)))
                (=> v0x7f45f9c6ebd0_0 E0x7f45f9c6ec90)
                (=> v0x7f45f9c6f410_0
                    (and v0x7f45f9c6e810_0 E0x7f45f9c6f4d0 v0x7f45f9c6ea90_0))
                (=> v0x7f45f9c6f410_0 E0x7f45f9c6f4d0)
                (=> v0x7f45f9c6f910_0
                    (and v0x7f45f9c6ebd0_0 E0x7f45f9c6f9d0 v0x7f45f9c6f2d0_0))
                (=> v0x7f45f9c6f910_0 E0x7f45f9c6f9d0)
                (=> v0x7f45f9c6fb90_0 a!5)
                a!6
                v0x7f45f9c6fb90_0
                (not v0x7f45f9c70d10_0)
                (= v0x7f45f9c6c750_0 (= v0x7f45f9c6c690_0 0.0))
                (= v0x7f45f9c6cb90_0 (< v0x7f45f9c6bf10_0 2.0))
                (= v0x7f45f9c6cd50_0 (ite v0x7f45f9c6cb90_0 1.0 0.0))
                (= v0x7f45f9c6ce90_0 (+ v0x7f45f9c6cd50_0 v0x7f45f9c6bf10_0))
                (= v0x7f45f9c6d810_0 (= v0x7f45f9c6d750_0 0.0))
                (= v0x7f45f9c6dc10_0 (= v0x7f45f9c6bd90_0 0.0))
                (= v0x7f45f9c6dd50_0 (ite v0x7f45f9c6dc10_0 1.0 0.0))
                (= v0x7f45f9c6e6d0_0 (= v0x7f45f9c6c010_0 0.0))
                (= v0x7f45f9c6ea90_0 (> v0x7f45f9c6d090_0 1.0))
                (= v0x7f45f9c6ee90_0 (> v0x7f45f9c6d090_0 0.0))
                (= v0x7f45f9c6efd0_0 (+ v0x7f45f9c6d090_0 (- 1.0)))
                (= v0x7f45f9c6f190_0
                   (ite v0x7f45f9c6ee90_0 v0x7f45f9c6efd0_0 v0x7f45f9c6d090_0))
                (= v0x7f45f9c6f2d0_0 (= v0x7f45f9c6f190_0 0.0))
                (= v0x7f45f9c6f690_0 (= v0x7f45f9c6df50_0 0.0))
                (= v0x7f45f9c6f7d0_0
                   (ite v0x7f45f9c6f690_0 1.0 v0x7f45f9c6c010_0))
                (= v0x7f45f9c70ad0_0 (= v0x7f45f9c6df50_0 0.0))
                (= v0x7f45f9c70bd0_0 (= v0x7f45f9c6fd10_0 0.0))
                (= v0x7f45f9c70d10_0 (or v0x7f45f9c70bd0_0 v0x7f45f9c70ad0_0)))))
  (=> F0x7f45f9c71b10 a!7))))
(assert (=> F0x7f45f9c71b10 F0x7f45f9c71a50))
(assert (=> F0x7f45f9c71c50 (or F0x7f45f9c71950 F0x7f45f9c71990)))
(assert (=> F0x7f45f9c71c10 F0x7f45f9c71b10))
(assert (=> pre!entry!0 (=> F0x7f45f9c71890 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f45f9c71a50 (>= v0x7f45f9c6c010_0 0.0))))
(assert (let ((a!1 (=> F0x7f45f9c71a50
               (or (<= v0x7f45f9c6c010_0 0.0) (not (<= v0x7f45f9c6bf10_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f45f9c71c50 (>= v0x7f45f9c6a010_0 0.0))))
(assert (let ((a!1 (=> F0x7f45f9c71c50
               (or (<= v0x7f45f9c6a010_0 0.0) (not (<= v0x7f45f9c6c0d0_0 1.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i34.i.i!0 (=> F0x7f45f9c71c10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i34.i.i!0) (not lemma!bb1.i.i34.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f45f9c71890)
; (error: F0x7f45f9c71c10)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i34.i.i!0)
