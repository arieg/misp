(declare-fun post!bb2.i.i35.i.i!0 () Bool)
(declare-fun lemma!bb2.i.i35.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7f71a6ea5c10 () Bool)
(declare-fun v0x7f71a6ea4b90_0 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun v0x7f71a6ea4a50_0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun v0x7f71a6ea4950_0 () Bool)
(declare-fun v0x7f71a6ea3310_0 () Real)
(declare-fun v0x7f71a6ea31d0_0 () Bool)
(declare-fun F0x7f71a6ea5d10 () Bool)
(declare-fun v0x7f71a6ea1e10_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f71a6ea1950_0 () Real)
(declare-fun v0x7f71a6ea0f50_0 () Real)
(declare-fun v0x7f71a6ea34d0_0 () Real)
(declare-fun v0x7f71a6ea2dd0_0 () Real)
(declare-fun v0x7f71a6e9ff90_0 () Real)
(declare-fun v0x7f71a6ea4cd0_0 () Bool)
(declare-fun F0x7f71a6ea5b50 () Bool)
(declare-fun v0x7f71a6ea3d10_0 () Real)
(declare-fun v0x7f71a6ea3c50_0 () Bool)
(declare-fun E0x7f71a6ea3a90 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun E0x7f71a6ea2fd0 () Bool)
(declare-fun v0x7f71a6ea3dd0_0 () Real)
(declare-fun v0x7f71a6ea39d0_0 () Bool)
(declare-fun v0x7f71a6ea2f10_0 () Bool)
(declare-fun v0x7f71a6ea2a10_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7f71a6ea23d0 () Bool)
(declare-fun v0x7f71a6ea2150_0 () Real)
(declare-fun E0x7f71a6ea4150 () Bool)
(declare-fun v0x7f71a6ea0d90_0 () Bool)
(declare-fun E0x7f71a6ea3e90 () Bool)
(declare-fun E0x7f71a6ea2210 () Bool)
(declare-fun v0x7f71a6ea1a10_0 () Bool)
(declare-fun E0x7f71a6ea1c10 () Bool)
(declare-fun v0x7f71a6ea0210_0 () Real)
(declare-fun E0x7f71a6ea1510 () Bool)
(declare-fun v0x7f71a6ea1290_0 () Real)
(declare-fun v0x7f71a6ea11d0_0 () Bool)
(declare-fun v0x7f71a6ea1b50_0 () Bool)
(declare-fun E0x7f71a6ea0b50 () Bool)
(declare-fun v0x7f71a6ea3750_0 () Bool)
(declare-fun v0x7f71a6ea0950_0 () Bool)
(declare-fun v0x7f71a6e9d010_0 () Real)
(declare-fun v0x7f71a6ea0890_0 () Real)
(declare-fun v0x7f71a6ea2090_0 () Bool)
(declare-fun v0x7f71a6ea3890_0 () Bool)
(declare-fun v0x7f71a6ea1090_0 () Real)
(declare-fun v0x7f71a6ea02d0_0 () Real)
(declare-fun F0x7f71a6ea5d50 () Bool)
(declare-fun v0x7f71a6ea0a90_0 () Bool)
(declare-fun v0x7f71a6ea07d0_0 () Bool)
(declare-fun v0x7f71a6ea1f50_0 () Real)
(declare-fun v0x7f71a6ea01d0_0 () Real)
(declare-fun F0x7f71a6ea5950 () Bool)
(declare-fun F0x7f71a6ea5a90 () Bool)
(declare-fun v0x7f71a6ea4e10_0 () Bool)
(declare-fun E0x7f71a6ea4410 () Bool)
(declare-fun E0x7f71a6ea1350 () Bool)
(declare-fun v0x7f71a6e9d110_0 () Bool)
(declare-fun v0x7f71a6ea2c90_0 () Bool)
(declare-fun v0x7f71a6ea28d0_0 () Bool)
(declare-fun v0x7f71a6ea0110_0 () Real)
(declare-fun v0x7f71a6ea3610_0 () Bool)
(declare-fun E0x7f71a6ea2ad0 () Bool)
(declare-fun F0x7f71a6ea5a50 () Bool)

(assert (=> F0x7f71a6ea5a50
    (and v0x7f71a6e9d110_0
         (<= v0x7f71a6ea01d0_0 0.0)
         (>= v0x7f71a6ea01d0_0 0.0)
         (<= v0x7f71a6ea02d0_0 0.0)
         (>= v0x7f71a6ea02d0_0 0.0)
         (<= v0x7f71a6e9d010_0 1.0)
         (>= v0x7f71a6e9d010_0 1.0))))
(assert (=> F0x7f71a6ea5a50 F0x7f71a6ea5950))
(assert (let ((a!1 (=> v0x7f71a6ea11d0_0
               (or (and v0x7f71a6ea0a90_0
                        E0x7f71a6ea1350
                        (<= v0x7f71a6ea1290_0 v0x7f71a6ea1090_0)
                        (>= v0x7f71a6ea1290_0 v0x7f71a6ea1090_0))
                   (and v0x7f71a6ea07d0_0
                        E0x7f71a6ea1510
                        v0x7f71a6ea0950_0
                        (<= v0x7f71a6ea1290_0 v0x7f71a6ea0210_0)
                        (>= v0x7f71a6ea1290_0 v0x7f71a6ea0210_0)))))
      (a!2 (=> v0x7f71a6ea11d0_0
               (or (and E0x7f71a6ea1350 (not E0x7f71a6ea1510))
                   (and E0x7f71a6ea1510 (not E0x7f71a6ea1350)))))
      (a!3 (=> v0x7f71a6ea2090_0
               (or (and v0x7f71a6ea1b50_0
                        E0x7f71a6ea2210
                        (<= v0x7f71a6ea2150_0 v0x7f71a6ea1f50_0)
                        (>= v0x7f71a6ea2150_0 v0x7f71a6ea1f50_0))
                   (and v0x7f71a6ea11d0_0
                        E0x7f71a6ea23d0
                        v0x7f71a6ea1a10_0
                        (<= v0x7f71a6ea2150_0 v0x7f71a6ea0110_0)
                        (>= v0x7f71a6ea2150_0 v0x7f71a6ea0110_0)))))
      (a!4 (=> v0x7f71a6ea2090_0
               (or (and E0x7f71a6ea2210 (not E0x7f71a6ea23d0))
                   (and E0x7f71a6ea23d0 (not E0x7f71a6ea2210)))))
      (a!5 (or (and v0x7f71a6ea2a10_0
                    E0x7f71a6ea3e90
                    (<= v0x7f71a6ea3d10_0 v0x7f71a6ea1290_0)
                    (>= v0x7f71a6ea3d10_0 v0x7f71a6ea1290_0)
                    (<= v0x7f71a6ea3dd0_0 v0x7f71a6ea2dd0_0)
                    (>= v0x7f71a6ea3dd0_0 v0x7f71a6ea2dd0_0))
               (and v0x7f71a6ea39d0_0
                    E0x7f71a6ea4150
                    (and (<= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0)
                         (>= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0))
                    (<= v0x7f71a6ea3dd0_0 v0x7f71a6e9ff90_0)
                    (>= v0x7f71a6ea3dd0_0 v0x7f71a6e9ff90_0))
               (and v0x7f71a6ea2f10_0
                    E0x7f71a6ea4410
                    (not v0x7f71a6ea3890_0)
                    (and (<= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0)
                         (>= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0))
                    (<= v0x7f71a6ea3dd0_0 0.0)
                    (>= v0x7f71a6ea3dd0_0 0.0))))
      (a!6 (=> v0x7f71a6ea3c50_0
               (or (and E0x7f71a6ea3e90
                        (not E0x7f71a6ea4150)
                        (not E0x7f71a6ea4410))
                   (and E0x7f71a6ea4150
                        (not E0x7f71a6ea3e90)
                        (not E0x7f71a6ea4410))
                   (and E0x7f71a6ea4410
                        (not E0x7f71a6ea3e90)
                        (not E0x7f71a6ea4150))))))
(let ((a!7 (and (=> v0x7f71a6ea0a90_0
                    (and v0x7f71a6ea07d0_0
                         E0x7f71a6ea0b50
                         (not v0x7f71a6ea0950_0)))
                (=> v0x7f71a6ea0a90_0 E0x7f71a6ea0b50)
                a!1
                a!2
                (=> v0x7f71a6ea1b50_0
                    (and v0x7f71a6ea11d0_0
                         E0x7f71a6ea1c10
                         (not v0x7f71a6ea1a10_0)))
                (=> v0x7f71a6ea1b50_0 E0x7f71a6ea1c10)
                a!3
                a!4
                (=> v0x7f71a6ea2a10_0
                    (and v0x7f71a6ea2090_0 E0x7f71a6ea2ad0 v0x7f71a6ea28d0_0))
                (=> v0x7f71a6ea2a10_0 E0x7f71a6ea2ad0)
                (=> v0x7f71a6ea2f10_0
                    (and v0x7f71a6ea2090_0
                         E0x7f71a6ea2fd0
                         (not v0x7f71a6ea28d0_0)))
                (=> v0x7f71a6ea2f10_0 E0x7f71a6ea2fd0)
                (=> v0x7f71a6ea39d0_0
                    (and v0x7f71a6ea2f10_0 E0x7f71a6ea3a90 v0x7f71a6ea3890_0))
                (=> v0x7f71a6ea39d0_0 E0x7f71a6ea3a90)
                (=> v0x7f71a6ea3c50_0 a!5)
                a!6
                v0x7f71a6ea3c50_0
                (not v0x7f71a6ea4e10_0)
                (<= v0x7f71a6ea01d0_0 v0x7f71a6ea3dd0_0)
                (>= v0x7f71a6ea01d0_0 v0x7f71a6ea3dd0_0)
                (<= v0x7f71a6ea02d0_0 v0x7f71a6ea2150_0)
                (>= v0x7f71a6ea02d0_0 v0x7f71a6ea2150_0)
                (<= v0x7f71a6e9d010_0 v0x7f71a6ea3d10_0)
                (>= v0x7f71a6e9d010_0 v0x7f71a6ea3d10_0)
                (= v0x7f71a6ea0950_0 (= v0x7f71a6ea0890_0 0.0))
                (= v0x7f71a6ea0d90_0 (< v0x7f71a6ea0210_0 2.0))
                (= v0x7f71a6ea0f50_0 (ite v0x7f71a6ea0d90_0 1.0 0.0))
                (= v0x7f71a6ea1090_0 (+ v0x7f71a6ea0f50_0 v0x7f71a6ea0210_0))
                (= v0x7f71a6ea1a10_0 (= v0x7f71a6ea1950_0 0.0))
                (= v0x7f71a6ea1e10_0 (= v0x7f71a6ea0110_0 0.0))
                (= v0x7f71a6ea1f50_0 (ite v0x7f71a6ea1e10_0 1.0 0.0))
                (= v0x7f71a6ea28d0_0 (= v0x7f71a6e9ff90_0 0.0))
                (= v0x7f71a6ea2c90_0 (> v0x7f71a6ea1290_0 1.0))
                (= v0x7f71a6ea2dd0_0
                   (ite v0x7f71a6ea2c90_0 1.0 v0x7f71a6e9ff90_0))
                (= v0x7f71a6ea31d0_0 (> v0x7f71a6ea1290_0 0.0))
                (= v0x7f71a6ea3310_0 (+ v0x7f71a6ea1290_0 (- 1.0)))
                (= v0x7f71a6ea34d0_0
                   (ite v0x7f71a6ea31d0_0 v0x7f71a6ea3310_0 v0x7f71a6ea1290_0))
                (= v0x7f71a6ea3610_0 (= v0x7f71a6ea2150_0 0.0))
                (= v0x7f71a6ea3750_0 (= v0x7f71a6ea34d0_0 0.0))
                (= v0x7f71a6ea3890_0 (and v0x7f71a6ea3610_0 v0x7f71a6ea3750_0))
                (= v0x7f71a6ea4950_0 (= v0x7f71a6ea2150_0 0.0))
                (= v0x7f71a6ea4a50_0 (= v0x7f71a6ea3d10_0 2.0))
                (= v0x7f71a6ea4b90_0 (= v0x7f71a6ea3dd0_0 0.0))
                (= v0x7f71a6ea4cd0_0 (and v0x7f71a6ea4a50_0 v0x7f71a6ea4950_0))
                (= v0x7f71a6ea4e10_0 (and v0x7f71a6ea4cd0_0 v0x7f71a6ea4b90_0)))))
  (=> F0x7f71a6ea5a90 a!7))))
(assert (=> F0x7f71a6ea5a90 F0x7f71a6ea5b50))
(assert (let ((a!1 (=> v0x7f71a6ea11d0_0
               (or (and v0x7f71a6ea0a90_0
                        E0x7f71a6ea1350
                        (<= v0x7f71a6ea1290_0 v0x7f71a6ea1090_0)
                        (>= v0x7f71a6ea1290_0 v0x7f71a6ea1090_0))
                   (and v0x7f71a6ea07d0_0
                        E0x7f71a6ea1510
                        v0x7f71a6ea0950_0
                        (<= v0x7f71a6ea1290_0 v0x7f71a6ea0210_0)
                        (>= v0x7f71a6ea1290_0 v0x7f71a6ea0210_0)))))
      (a!2 (=> v0x7f71a6ea11d0_0
               (or (and E0x7f71a6ea1350 (not E0x7f71a6ea1510))
                   (and E0x7f71a6ea1510 (not E0x7f71a6ea1350)))))
      (a!3 (=> v0x7f71a6ea2090_0
               (or (and v0x7f71a6ea1b50_0
                        E0x7f71a6ea2210
                        (<= v0x7f71a6ea2150_0 v0x7f71a6ea1f50_0)
                        (>= v0x7f71a6ea2150_0 v0x7f71a6ea1f50_0))
                   (and v0x7f71a6ea11d0_0
                        E0x7f71a6ea23d0
                        v0x7f71a6ea1a10_0
                        (<= v0x7f71a6ea2150_0 v0x7f71a6ea0110_0)
                        (>= v0x7f71a6ea2150_0 v0x7f71a6ea0110_0)))))
      (a!4 (=> v0x7f71a6ea2090_0
               (or (and E0x7f71a6ea2210 (not E0x7f71a6ea23d0))
                   (and E0x7f71a6ea23d0 (not E0x7f71a6ea2210)))))
      (a!5 (or (and v0x7f71a6ea2a10_0
                    E0x7f71a6ea3e90
                    (<= v0x7f71a6ea3d10_0 v0x7f71a6ea1290_0)
                    (>= v0x7f71a6ea3d10_0 v0x7f71a6ea1290_0)
                    (<= v0x7f71a6ea3dd0_0 v0x7f71a6ea2dd0_0)
                    (>= v0x7f71a6ea3dd0_0 v0x7f71a6ea2dd0_0))
               (and v0x7f71a6ea39d0_0
                    E0x7f71a6ea4150
                    (and (<= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0)
                         (>= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0))
                    (<= v0x7f71a6ea3dd0_0 v0x7f71a6e9ff90_0)
                    (>= v0x7f71a6ea3dd0_0 v0x7f71a6e9ff90_0))
               (and v0x7f71a6ea2f10_0
                    E0x7f71a6ea4410
                    (not v0x7f71a6ea3890_0)
                    (and (<= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0)
                         (>= v0x7f71a6ea3d10_0 v0x7f71a6ea34d0_0))
                    (<= v0x7f71a6ea3dd0_0 0.0)
                    (>= v0x7f71a6ea3dd0_0 0.0))))
      (a!6 (=> v0x7f71a6ea3c50_0
               (or (and E0x7f71a6ea3e90
                        (not E0x7f71a6ea4150)
                        (not E0x7f71a6ea4410))
                   (and E0x7f71a6ea4150
                        (not E0x7f71a6ea3e90)
                        (not E0x7f71a6ea4410))
                   (and E0x7f71a6ea4410
                        (not E0x7f71a6ea3e90)
                        (not E0x7f71a6ea4150))))))
(let ((a!7 (and (=> v0x7f71a6ea0a90_0
                    (and v0x7f71a6ea07d0_0
                         E0x7f71a6ea0b50
                         (not v0x7f71a6ea0950_0)))
                (=> v0x7f71a6ea0a90_0 E0x7f71a6ea0b50)
                a!1
                a!2
                (=> v0x7f71a6ea1b50_0
                    (and v0x7f71a6ea11d0_0
                         E0x7f71a6ea1c10
                         (not v0x7f71a6ea1a10_0)))
                (=> v0x7f71a6ea1b50_0 E0x7f71a6ea1c10)
                a!3
                a!4
                (=> v0x7f71a6ea2a10_0
                    (and v0x7f71a6ea2090_0 E0x7f71a6ea2ad0 v0x7f71a6ea28d0_0))
                (=> v0x7f71a6ea2a10_0 E0x7f71a6ea2ad0)
                (=> v0x7f71a6ea2f10_0
                    (and v0x7f71a6ea2090_0
                         E0x7f71a6ea2fd0
                         (not v0x7f71a6ea28d0_0)))
                (=> v0x7f71a6ea2f10_0 E0x7f71a6ea2fd0)
                (=> v0x7f71a6ea39d0_0
                    (and v0x7f71a6ea2f10_0 E0x7f71a6ea3a90 v0x7f71a6ea3890_0))
                (=> v0x7f71a6ea39d0_0 E0x7f71a6ea3a90)
                (=> v0x7f71a6ea3c50_0 a!5)
                a!6
                v0x7f71a6ea3c50_0
                v0x7f71a6ea4e10_0
                (= v0x7f71a6ea0950_0 (= v0x7f71a6ea0890_0 0.0))
                (= v0x7f71a6ea0d90_0 (< v0x7f71a6ea0210_0 2.0))
                (= v0x7f71a6ea0f50_0 (ite v0x7f71a6ea0d90_0 1.0 0.0))
                (= v0x7f71a6ea1090_0 (+ v0x7f71a6ea0f50_0 v0x7f71a6ea0210_0))
                (= v0x7f71a6ea1a10_0 (= v0x7f71a6ea1950_0 0.0))
                (= v0x7f71a6ea1e10_0 (= v0x7f71a6ea0110_0 0.0))
                (= v0x7f71a6ea1f50_0 (ite v0x7f71a6ea1e10_0 1.0 0.0))
                (= v0x7f71a6ea28d0_0 (= v0x7f71a6e9ff90_0 0.0))
                (= v0x7f71a6ea2c90_0 (> v0x7f71a6ea1290_0 1.0))
                (= v0x7f71a6ea2dd0_0
                   (ite v0x7f71a6ea2c90_0 1.0 v0x7f71a6e9ff90_0))
                (= v0x7f71a6ea31d0_0 (> v0x7f71a6ea1290_0 0.0))
                (= v0x7f71a6ea3310_0 (+ v0x7f71a6ea1290_0 (- 1.0)))
                (= v0x7f71a6ea34d0_0
                   (ite v0x7f71a6ea31d0_0 v0x7f71a6ea3310_0 v0x7f71a6ea1290_0))
                (= v0x7f71a6ea3610_0 (= v0x7f71a6ea2150_0 0.0))
                (= v0x7f71a6ea3750_0 (= v0x7f71a6ea34d0_0 0.0))
                (= v0x7f71a6ea3890_0 (and v0x7f71a6ea3610_0 v0x7f71a6ea3750_0))
                (= v0x7f71a6ea4950_0 (= v0x7f71a6ea2150_0 0.0))
                (= v0x7f71a6ea4a50_0 (= v0x7f71a6ea3d10_0 2.0))
                (= v0x7f71a6ea4b90_0 (= v0x7f71a6ea3dd0_0 0.0))
                (= v0x7f71a6ea4cd0_0 (and v0x7f71a6ea4a50_0 v0x7f71a6ea4950_0))
                (= v0x7f71a6ea4e10_0 (and v0x7f71a6ea4cd0_0 v0x7f71a6ea4b90_0)))))
  (=> F0x7f71a6ea5c10 a!7))))
(assert (=> F0x7f71a6ea5c10 F0x7f71a6ea5b50))
(assert (=> F0x7f71a6ea5d50 (or F0x7f71a6ea5a50 F0x7f71a6ea5a90)))
(assert (=> F0x7f71a6ea5d10 F0x7f71a6ea5c10))
(assert (=> pre!entry!0 (=> F0x7f71a6ea5950 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f71a6ea5b50 (>= v0x7f71a6e9ff90_0 0.0))))
(assert (=> pre!bb1.i.i!1 (=> F0x7f71a6ea5b50 (not (<= 3.0 v0x7f71a6ea0210_0)))))
(assert (= lemma!bb1.i.i!0 (=> F0x7f71a6ea5d50 (>= v0x7f71a6ea01d0_0 0.0))))
(assert (= lemma!bb1.i.i!1 (=> F0x7f71a6ea5d50 (not (<= 3.0 v0x7f71a6e9d010_0)))))
(assert (= lemma!bb2.i.i35.i.i!0 (=> F0x7f71a6ea5d10 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i35.i.i!0) (not lemma!bb2.i.i35.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f71a6ea5950)
; (error: F0x7f71a6ea5d10)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i35.i.i!0)
