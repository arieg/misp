(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun post!bb2.i.i23.i.i!0 () Bool)
(declare-fun F0x7f870d2a45d0 () Bool)
(declare-fun v0x7f870d2a3550_0 () Bool)
(declare-fun v0x7f870d2a3410_0 () Bool)
(declare-fun v0x7f870d2a31d0_0 () Bool)
(declare-fun v0x7f870d2a1cd0_0 () Bool)
(declare-fun v0x7f870d2a32d0_0 () Bool)
(declare-fun v0x7f870d2a1790_0 () Bool)
(declare-fun v0x7f870d29fa50_0 () Real)
(declare-fun v0x7f870d2a3690_0 () Bool)
(declare-fun E0x7f870d2a2c90 () Bool)
(declare-fun v0x7f870d29ed10_0 () Real)
(declare-fun E0x7f870d2a29d0 () Bool)
(declare-fun v0x7f870d2a18d0_0 () Real)
(declare-fun v0x7f870d2a0450_0 () Real)
(declare-fun v0x7f870d2a24d0_0 () Bool)
(declare-fun v0x7f870d2a2110_0 () Bool)
(declare-fun F0x7f870d2a4590 () Bool)
(declare-fun E0x7f870d2a2310 () Bool)
(declare-fun F0x7f870d2a43d0 () Bool)
(declare-fun E0x7f870d2a1ad0 () Bool)
(declare-fun E0x7f870d2a15d0 () Bool)
(declare-fun v0x7f870d2a2250_0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f870d29ea90_0 () Real)
(declare-fun E0x7f870d2a0ed0 () Bool)
(declare-fun lemma!bb2.i.i23.i.i!0 () Bool)
(declare-fun v0x7f870d2a0c50_0 () Real)
(declare-fun E0x7f870d2a0d10 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f870d2a0510_0 () Bool)
(declare-fun v0x7f870d2a0650_0 () Bool)
(declare-fun F0x7f870d2a4490 () Bool)
(declare-fun v0x7f870d29ec10_0 () Real)
(declare-fun v0x7f870d2a2650_0 () Real)
(declare-fun E0x7f870d2a0010 () Bool)
(declare-fun v0x7f870d29fd90_0 () Real)
(declare-fun v0x7f870d29f390_0 () Real)
(declare-fun E0x7f870d2a0710 () Bool)
(declare-fun E0x7f870d2a2710 () Bool)
(declare-fun E0x7f870d29fe50 () Bool)
(declare-fun v0x7f870d29f890_0 () Bool)
(declare-fun v0x7f870d29f450_0 () Bool)
(declare-fun v0x7f870d2a0a50_0 () Real)
(declare-fun v0x7f870d2a1e10_0 () Real)
(declare-fun E0x7f870d29f650 () Bool)
(declare-fun v0x7f870d2a0910_0 () Bool)
(declare-fun v0x7f870d29f2d0_0 () Bool)
(declare-fun v0x7f870d2a1510_0 () Bool)
(declare-fun v0x7f870d29f590_0 () Bool)
(declare-fun F0x7f870d2a4310 () Bool)
(declare-fun v0x7f870d29d010_0 () Real)
(declare-fun v0x7f870d2a1a10_0 () Bool)
(declare-fun v0x7f870d2a0b90_0 () Bool)
(declare-fun v0x7f870d29fcd0_0 () Bool)
(declare-fun v0x7f870d2a2590_0 () Real)
(declare-fun v0x7f870d29edd0_0 () Real)
(declare-fun v0x7f870d29ecd0_0 () Real)
(declare-fun v0x7f870d2a1fd0_0 () Real)
(declare-fun v0x7f870d2a13d0_0 () Bool)
(declare-fun F0x7f870d2a4250 () Bool)
(declare-fun v0x7f870d29fb90_0 () Real)
(declare-fun F0x7f870d2a4190 () Bool)
(declare-fun v0x7f870d29d110_0 () Bool)

(assert (=> F0x7f870d2a4190
    (and v0x7f870d29d110_0
         (<= v0x7f870d29ecd0_0 0.0)
         (>= v0x7f870d29ecd0_0 0.0)
         (<= v0x7f870d29edd0_0 1.0)
         (>= v0x7f870d29edd0_0 1.0)
         (<= v0x7f870d29d010_0 0.0)
         (>= v0x7f870d29d010_0 0.0))))
(assert (=> F0x7f870d2a4190 F0x7f870d2a4250))
(assert (let ((a!1 (=> v0x7f870d29fcd0_0
               (or (and v0x7f870d29f590_0
                        E0x7f870d29fe50
                        (<= v0x7f870d29fd90_0 v0x7f870d29fb90_0)
                        (>= v0x7f870d29fd90_0 v0x7f870d29fb90_0))
                   (and v0x7f870d29f2d0_0
                        E0x7f870d2a0010
                        v0x7f870d29f450_0
                        (<= v0x7f870d29fd90_0 v0x7f870d29ec10_0)
                        (>= v0x7f870d29fd90_0 v0x7f870d29ec10_0)))))
      (a!2 (=> v0x7f870d29fcd0_0
               (or (and E0x7f870d29fe50 (not E0x7f870d2a0010))
                   (and E0x7f870d2a0010 (not E0x7f870d29fe50)))))
      (a!3 (=> v0x7f870d2a0b90_0
               (or (and v0x7f870d2a0650_0
                        E0x7f870d2a0d10
                        (<= v0x7f870d2a0c50_0 v0x7f870d2a0a50_0)
                        (>= v0x7f870d2a0c50_0 v0x7f870d2a0a50_0))
                   (and v0x7f870d29fcd0_0
                        E0x7f870d2a0ed0
                        v0x7f870d2a0510_0
                        (<= v0x7f870d2a0c50_0 v0x7f870d29ea90_0)
                        (>= v0x7f870d2a0c50_0 v0x7f870d29ea90_0)))))
      (a!4 (=> v0x7f870d2a0b90_0
               (or (and E0x7f870d2a0d10 (not E0x7f870d2a0ed0))
                   (and E0x7f870d2a0ed0 (not E0x7f870d2a0d10)))))
      (a!5 (or (and v0x7f870d2a1510_0
                    E0x7f870d2a2710
                    (<= v0x7f870d2a2590_0 v0x7f870d29fd90_0)
                    (>= v0x7f870d2a2590_0 v0x7f870d29fd90_0)
                    (<= v0x7f870d2a2650_0 v0x7f870d2a18d0_0)
                    (>= v0x7f870d2a2650_0 v0x7f870d2a18d0_0))
               (and v0x7f870d2a2250_0
                    E0x7f870d2a29d0
                    (and (<= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0)
                         (>= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0))
                    (<= v0x7f870d2a2650_0 v0x7f870d29ed10_0)
                    (>= v0x7f870d2a2650_0 v0x7f870d29ed10_0))
               (and v0x7f870d2a1a10_0
                    E0x7f870d2a2c90
                    (not v0x7f870d2a2110_0)
                    (and (<= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0)
                         (>= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0))
                    (<= v0x7f870d2a2650_0 0.0)
                    (>= v0x7f870d2a2650_0 0.0))))
      (a!6 (=> v0x7f870d2a24d0_0
               (or (and E0x7f870d2a2710
                        (not E0x7f870d2a29d0)
                        (not E0x7f870d2a2c90))
                   (and E0x7f870d2a29d0
                        (not E0x7f870d2a2710)
                        (not E0x7f870d2a2c90))
                   (and E0x7f870d2a2c90
                        (not E0x7f870d2a2710)
                        (not E0x7f870d2a29d0))))))
(let ((a!7 (and (=> v0x7f870d29f590_0
                    (and v0x7f870d29f2d0_0
                         E0x7f870d29f650
                         (not v0x7f870d29f450_0)))
                (=> v0x7f870d29f590_0 E0x7f870d29f650)
                a!1
                a!2
                (=> v0x7f870d2a0650_0
                    (and v0x7f870d29fcd0_0
                         E0x7f870d2a0710
                         (not v0x7f870d2a0510_0)))
                (=> v0x7f870d2a0650_0 E0x7f870d2a0710)
                a!3
                a!4
                (=> v0x7f870d2a1510_0
                    (and v0x7f870d2a0b90_0 E0x7f870d2a15d0 v0x7f870d2a13d0_0))
                (=> v0x7f870d2a1510_0 E0x7f870d2a15d0)
                (=> v0x7f870d2a1a10_0
                    (and v0x7f870d2a0b90_0
                         E0x7f870d2a1ad0
                         (not v0x7f870d2a13d0_0)))
                (=> v0x7f870d2a1a10_0 E0x7f870d2a1ad0)
                (=> v0x7f870d2a2250_0
                    (and v0x7f870d2a1a10_0 E0x7f870d2a2310 v0x7f870d2a2110_0))
                (=> v0x7f870d2a2250_0 E0x7f870d2a2310)
                (=> v0x7f870d2a24d0_0 a!5)
                a!6
                v0x7f870d2a24d0_0
                (not v0x7f870d2a3690_0)
                (<= v0x7f870d29ecd0_0 v0x7f870d2a0c50_0)
                (>= v0x7f870d29ecd0_0 v0x7f870d2a0c50_0)
                (<= v0x7f870d29edd0_0 v0x7f870d2a2590_0)
                (>= v0x7f870d29edd0_0 v0x7f870d2a2590_0)
                (<= v0x7f870d29d010_0 v0x7f870d2a2650_0)
                (>= v0x7f870d29d010_0 v0x7f870d2a2650_0)
                (= v0x7f870d29f450_0 (= v0x7f870d29f390_0 0.0))
                (= v0x7f870d29f890_0 (< v0x7f870d29ec10_0 2.0))
                (= v0x7f870d29fa50_0 (ite v0x7f870d29f890_0 1.0 0.0))
                (= v0x7f870d29fb90_0 (+ v0x7f870d29fa50_0 v0x7f870d29ec10_0))
                (= v0x7f870d2a0510_0 (= v0x7f870d2a0450_0 0.0))
                (= v0x7f870d2a0910_0 (= v0x7f870d29ea90_0 0.0))
                (= v0x7f870d2a0a50_0 (ite v0x7f870d2a0910_0 1.0 0.0))
                (= v0x7f870d2a13d0_0 (= v0x7f870d29ed10_0 0.0))
                (= v0x7f870d2a1790_0 (> v0x7f870d29fd90_0 1.0))
                (= v0x7f870d2a18d0_0
                   (ite v0x7f870d2a1790_0 1.0 v0x7f870d29ed10_0))
                (= v0x7f870d2a1cd0_0 (> v0x7f870d29fd90_0 0.0))
                (= v0x7f870d2a1e10_0 (+ v0x7f870d29fd90_0 (- 1.0)))
                (= v0x7f870d2a1fd0_0
                   (ite v0x7f870d2a1cd0_0 v0x7f870d2a1e10_0 v0x7f870d29fd90_0))
                (= v0x7f870d2a2110_0 (= v0x7f870d2a0c50_0 0.0))
                (= v0x7f870d2a31d0_0 (= v0x7f870d2a0c50_0 0.0))
                (= v0x7f870d2a32d0_0 (= v0x7f870d2a2590_0 2.0))
                (= v0x7f870d2a3410_0 (= v0x7f870d2a2650_0 0.0))
                (= v0x7f870d2a3550_0 (and v0x7f870d2a32d0_0 v0x7f870d2a31d0_0))
                (= v0x7f870d2a3690_0 (and v0x7f870d2a3550_0 v0x7f870d2a3410_0)))))
  (=> F0x7f870d2a4310 a!7))))
(assert (=> F0x7f870d2a4310 F0x7f870d2a43d0))
(assert (let ((a!1 (=> v0x7f870d29fcd0_0
               (or (and v0x7f870d29f590_0
                        E0x7f870d29fe50
                        (<= v0x7f870d29fd90_0 v0x7f870d29fb90_0)
                        (>= v0x7f870d29fd90_0 v0x7f870d29fb90_0))
                   (and v0x7f870d29f2d0_0
                        E0x7f870d2a0010
                        v0x7f870d29f450_0
                        (<= v0x7f870d29fd90_0 v0x7f870d29ec10_0)
                        (>= v0x7f870d29fd90_0 v0x7f870d29ec10_0)))))
      (a!2 (=> v0x7f870d29fcd0_0
               (or (and E0x7f870d29fe50 (not E0x7f870d2a0010))
                   (and E0x7f870d2a0010 (not E0x7f870d29fe50)))))
      (a!3 (=> v0x7f870d2a0b90_0
               (or (and v0x7f870d2a0650_0
                        E0x7f870d2a0d10
                        (<= v0x7f870d2a0c50_0 v0x7f870d2a0a50_0)
                        (>= v0x7f870d2a0c50_0 v0x7f870d2a0a50_0))
                   (and v0x7f870d29fcd0_0
                        E0x7f870d2a0ed0
                        v0x7f870d2a0510_0
                        (<= v0x7f870d2a0c50_0 v0x7f870d29ea90_0)
                        (>= v0x7f870d2a0c50_0 v0x7f870d29ea90_0)))))
      (a!4 (=> v0x7f870d2a0b90_0
               (or (and E0x7f870d2a0d10 (not E0x7f870d2a0ed0))
                   (and E0x7f870d2a0ed0 (not E0x7f870d2a0d10)))))
      (a!5 (or (and v0x7f870d2a1510_0
                    E0x7f870d2a2710
                    (<= v0x7f870d2a2590_0 v0x7f870d29fd90_0)
                    (>= v0x7f870d2a2590_0 v0x7f870d29fd90_0)
                    (<= v0x7f870d2a2650_0 v0x7f870d2a18d0_0)
                    (>= v0x7f870d2a2650_0 v0x7f870d2a18d0_0))
               (and v0x7f870d2a2250_0
                    E0x7f870d2a29d0
                    (and (<= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0)
                         (>= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0))
                    (<= v0x7f870d2a2650_0 v0x7f870d29ed10_0)
                    (>= v0x7f870d2a2650_0 v0x7f870d29ed10_0))
               (and v0x7f870d2a1a10_0
                    E0x7f870d2a2c90
                    (not v0x7f870d2a2110_0)
                    (and (<= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0)
                         (>= v0x7f870d2a2590_0 v0x7f870d2a1fd0_0))
                    (<= v0x7f870d2a2650_0 0.0)
                    (>= v0x7f870d2a2650_0 0.0))))
      (a!6 (=> v0x7f870d2a24d0_0
               (or (and E0x7f870d2a2710
                        (not E0x7f870d2a29d0)
                        (not E0x7f870d2a2c90))
                   (and E0x7f870d2a29d0
                        (not E0x7f870d2a2710)
                        (not E0x7f870d2a2c90))
                   (and E0x7f870d2a2c90
                        (not E0x7f870d2a2710)
                        (not E0x7f870d2a29d0))))))
(let ((a!7 (and (=> v0x7f870d29f590_0
                    (and v0x7f870d29f2d0_0
                         E0x7f870d29f650
                         (not v0x7f870d29f450_0)))
                (=> v0x7f870d29f590_0 E0x7f870d29f650)
                a!1
                a!2
                (=> v0x7f870d2a0650_0
                    (and v0x7f870d29fcd0_0
                         E0x7f870d2a0710
                         (not v0x7f870d2a0510_0)))
                (=> v0x7f870d2a0650_0 E0x7f870d2a0710)
                a!3
                a!4
                (=> v0x7f870d2a1510_0
                    (and v0x7f870d2a0b90_0 E0x7f870d2a15d0 v0x7f870d2a13d0_0))
                (=> v0x7f870d2a1510_0 E0x7f870d2a15d0)
                (=> v0x7f870d2a1a10_0
                    (and v0x7f870d2a0b90_0
                         E0x7f870d2a1ad0
                         (not v0x7f870d2a13d0_0)))
                (=> v0x7f870d2a1a10_0 E0x7f870d2a1ad0)
                (=> v0x7f870d2a2250_0
                    (and v0x7f870d2a1a10_0 E0x7f870d2a2310 v0x7f870d2a2110_0))
                (=> v0x7f870d2a2250_0 E0x7f870d2a2310)
                (=> v0x7f870d2a24d0_0 a!5)
                a!6
                v0x7f870d2a24d0_0
                v0x7f870d2a3690_0
                (= v0x7f870d29f450_0 (= v0x7f870d29f390_0 0.0))
                (= v0x7f870d29f890_0 (< v0x7f870d29ec10_0 2.0))
                (= v0x7f870d29fa50_0 (ite v0x7f870d29f890_0 1.0 0.0))
                (= v0x7f870d29fb90_0 (+ v0x7f870d29fa50_0 v0x7f870d29ec10_0))
                (= v0x7f870d2a0510_0 (= v0x7f870d2a0450_0 0.0))
                (= v0x7f870d2a0910_0 (= v0x7f870d29ea90_0 0.0))
                (= v0x7f870d2a0a50_0 (ite v0x7f870d2a0910_0 1.0 0.0))
                (= v0x7f870d2a13d0_0 (= v0x7f870d29ed10_0 0.0))
                (= v0x7f870d2a1790_0 (> v0x7f870d29fd90_0 1.0))
                (= v0x7f870d2a18d0_0
                   (ite v0x7f870d2a1790_0 1.0 v0x7f870d29ed10_0))
                (= v0x7f870d2a1cd0_0 (> v0x7f870d29fd90_0 0.0))
                (= v0x7f870d2a1e10_0 (+ v0x7f870d29fd90_0 (- 1.0)))
                (= v0x7f870d2a1fd0_0
                   (ite v0x7f870d2a1cd0_0 v0x7f870d2a1e10_0 v0x7f870d29fd90_0))
                (= v0x7f870d2a2110_0 (= v0x7f870d2a0c50_0 0.0))
                (= v0x7f870d2a31d0_0 (= v0x7f870d2a0c50_0 0.0))
                (= v0x7f870d2a32d0_0 (= v0x7f870d2a2590_0 2.0))
                (= v0x7f870d2a3410_0 (= v0x7f870d2a2650_0 0.0))
                (= v0x7f870d2a3550_0 (and v0x7f870d2a32d0_0 v0x7f870d2a31d0_0))
                (= v0x7f870d2a3690_0 (and v0x7f870d2a3550_0 v0x7f870d2a3410_0)))))
  (=> F0x7f870d2a4490 a!7))))
(assert (=> F0x7f870d2a4490 F0x7f870d2a43d0))
(assert (=> F0x7f870d2a45d0 (or F0x7f870d2a4190 F0x7f870d2a4310)))
(assert (=> F0x7f870d2a4590 F0x7f870d2a4490))
(assert (=> pre!entry!0 (=> F0x7f870d2a4250 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f870d2a43d0 true)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f870d2a45d0 true)))
(assert (= lemma!bb2.i.i23.i.i!0 (=> F0x7f870d2a4590 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i23.i.i!0) (not lemma!bb2.i.i23.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7f870d2a4250)
; (error: F0x7f870d2a4590)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i23.i.i!0)
