(declare-fun post!bb1.i.i35.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i35.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7f01a3fdc910_0 () Bool)
(declare-fun v0x7f01a3fdb490_0 () Bool)
(declare-fun v0x7f01a3fdb190_0 () Real)
(declare-fun F0x7f01a3fdd990 () Bool)
(declare-fun v0x7f01a3fdb050_0 () Bool)
(declare-fun E0x7f01a3fdc290 () Bool)
(declare-fun E0x7f01a3fdbfd0 () Bool)
(declare-fun v0x7f01a3fdac50_0 () Real)
(declare-fun v0x7f01a3fdab10_0 () Bool)
(declare-fun v0x7f01a3fdbb90_0 () Real)
(declare-fun v0x7f01a3fdb350_0 () Real)
(declare-fun v0x7f01a3fdbad0_0 () Bool)
(declare-fun v0x7f01a3fdb710_0 () Bool)
(declare-fun v0x7f01a3fd9c90_0 () Bool)
(declare-fun E0x7f01a3fdb910 () Bool)
(declare-fun v0x7f01a3fd7e10_0 () Real)
(declare-fun v0x7f01a3fd8c10_0 () Bool)
(declare-fun F0x7f01a3fdd950 () Bool)
(declare-fun v0x7f01a3fd9dd0_0 () Real)
(declare-fun v0x7f01a3fd9fd0_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun E0x7f01a3fda090 () Bool)
(declare-fun v0x7f01a3fdca50_0 () Bool)
(declare-fun v0x7f01a3fd9f10_0 () Bool)
(declare-fun v0x7f01a3fd9890_0 () Bool)
(declare-fun v0x7f01a3fd8710_0 () Real)
(declare-fun v0x7f01a3fd97d0_0 () Real)
(declare-fun E0x7f01a3fd9390 () Bool)
(declare-fun v0x7f01a3fda890_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f01a3fd8f10_0 () Real)
(declare-fun E0x7f01a3fda250 () Bool)
(declare-fun v0x7f01a3fd9110_0 () Real)
(declare-fun v0x7f01a3fdad90_0 () Bool)
(declare-fun E0x7f01a3fd91d0 () Bool)
(declare-fun v0x7f01a3fdbc50_0 () Real)
(declare-fun v0x7f01a3fd8dd0_0 () Real)
(declare-fun F0x7f01a3fdd790 () Bool)
(declare-fun v0x7f01a3fd9050_0 () Bool)
(declare-fun F0x7f01a3fdd850 () Bool)
(declare-fun E0x7f01a3fd89d0 () Bool)
(declare-fun E0x7f01a3fdae50 () Bool)
(declare-fun v0x7f01a3fd8650_0 () Bool)
(declare-fun v0x7f01a3fdc7d0_0 () Bool)
(declare-fun v0x7f01a3fda750_0 () Bool)
(declare-fun E0x7f01a3fdbd10 () Bool)
(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun E0x7f01a3fd9a90 () Bool)
(declare-fun F0x7f01a3fdd6d0 () Bool)
(declare-fun v0x7f01a3fd8090_0 () Real)
(declare-fun v0x7f01a3fdb850_0 () Bool)
(declare-fun E0x7f01a3fda950 () Bool)
(declare-fun F0x7f01a3fdd5d0 () Bool)
(declare-fun v0x7f01a3fd5010_0 () Real)
(declare-fun v0x7f01a3fd8050_0 () Real)
(declare-fun v0x7f01a3fd99d0_0 () Bool)
(declare-fun v0x7f01a3fd8910_0 () Bool)
(declare-fun v0x7f01a3fdb5d0_0 () Bool)
(declare-fun v0x7f01a3fd5110_0 () Bool)
(declare-fun v0x7f01a3fd8150_0 () Real)
(declare-fun v0x7f01a3fd87d0_0 () Bool)
(declare-fun F0x7f01a3fdd690 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7f01a3fd7f90_0 () Real)

(assert (=> F0x7f01a3fdd690
    (and v0x7f01a3fd5110_0
         (<= v0x7f01a3fd8050_0 0.0)
         (>= v0x7f01a3fd8050_0 0.0)
         (<= v0x7f01a3fd8150_0 1.0)
         (>= v0x7f01a3fd8150_0 1.0)
         (<= v0x7f01a3fd5010_0 0.0)
         (>= v0x7f01a3fd5010_0 0.0))))
(assert (=> F0x7f01a3fdd690 F0x7f01a3fdd5d0))
(assert (let ((a!1 (=> v0x7f01a3fd9050_0
               (or (and v0x7f01a3fd8910_0
                        E0x7f01a3fd91d0
                        (<= v0x7f01a3fd9110_0 v0x7f01a3fd8f10_0)
                        (>= v0x7f01a3fd9110_0 v0x7f01a3fd8f10_0))
                   (and v0x7f01a3fd8650_0
                        E0x7f01a3fd9390
                        v0x7f01a3fd87d0_0
                        (<= v0x7f01a3fd9110_0 v0x7f01a3fd7f90_0)
                        (>= v0x7f01a3fd9110_0 v0x7f01a3fd7f90_0)))))
      (a!2 (=> v0x7f01a3fd9050_0
               (or (and E0x7f01a3fd91d0 (not E0x7f01a3fd9390))
                   (and E0x7f01a3fd9390 (not E0x7f01a3fd91d0)))))
      (a!3 (=> v0x7f01a3fd9f10_0
               (or (and v0x7f01a3fd99d0_0
                        E0x7f01a3fda090
                        (<= v0x7f01a3fd9fd0_0 v0x7f01a3fd9dd0_0)
                        (>= v0x7f01a3fd9fd0_0 v0x7f01a3fd9dd0_0))
                   (and v0x7f01a3fd9050_0
                        E0x7f01a3fda250
                        v0x7f01a3fd9890_0
                        (<= v0x7f01a3fd9fd0_0 v0x7f01a3fd7e10_0)
                        (>= v0x7f01a3fd9fd0_0 v0x7f01a3fd7e10_0)))))
      (a!4 (=> v0x7f01a3fd9f10_0
               (or (and E0x7f01a3fda090 (not E0x7f01a3fda250))
                   (and E0x7f01a3fda250 (not E0x7f01a3fda090)))))
      (a!5 (or (and v0x7f01a3fda890_0
                    E0x7f01a3fdbd10
                    (<= v0x7f01a3fdbb90_0 v0x7f01a3fd9110_0)
                    (>= v0x7f01a3fdbb90_0 v0x7f01a3fd9110_0)
                    (<= v0x7f01a3fdbc50_0 v0x7f01a3fdac50_0)
                    (>= v0x7f01a3fdbc50_0 v0x7f01a3fdac50_0))
               (and v0x7f01a3fdb850_0
                    E0x7f01a3fdbfd0
                    (and (<= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0)
                         (>= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0))
                    (<= v0x7f01a3fdbc50_0 v0x7f01a3fd8090_0)
                    (>= v0x7f01a3fdbc50_0 v0x7f01a3fd8090_0))
               (and v0x7f01a3fdad90_0
                    E0x7f01a3fdc290
                    (not v0x7f01a3fdb710_0)
                    (and (<= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0)
                         (>= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0))
                    (<= v0x7f01a3fdbc50_0 0.0)
                    (>= v0x7f01a3fdbc50_0 0.0))))
      (a!6 (=> v0x7f01a3fdbad0_0
               (or (and E0x7f01a3fdbd10
                        (not E0x7f01a3fdbfd0)
                        (not E0x7f01a3fdc290))
                   (and E0x7f01a3fdbfd0
                        (not E0x7f01a3fdbd10)
                        (not E0x7f01a3fdc290))
                   (and E0x7f01a3fdc290
                        (not E0x7f01a3fdbd10)
                        (not E0x7f01a3fdbfd0))))))
(let ((a!7 (and (=> v0x7f01a3fd8910_0
                    (and v0x7f01a3fd8650_0
                         E0x7f01a3fd89d0
                         (not v0x7f01a3fd87d0_0)))
                (=> v0x7f01a3fd8910_0 E0x7f01a3fd89d0)
                a!1
                a!2
                (=> v0x7f01a3fd99d0_0
                    (and v0x7f01a3fd9050_0
                         E0x7f01a3fd9a90
                         (not v0x7f01a3fd9890_0)))
                (=> v0x7f01a3fd99d0_0 E0x7f01a3fd9a90)
                a!3
                a!4
                (=> v0x7f01a3fda890_0
                    (and v0x7f01a3fd9f10_0 E0x7f01a3fda950 v0x7f01a3fda750_0))
                (=> v0x7f01a3fda890_0 E0x7f01a3fda950)
                (=> v0x7f01a3fdad90_0
                    (and v0x7f01a3fd9f10_0
                         E0x7f01a3fdae50
                         (not v0x7f01a3fda750_0)))
                (=> v0x7f01a3fdad90_0 E0x7f01a3fdae50)
                (=> v0x7f01a3fdb850_0
                    (and v0x7f01a3fdad90_0 E0x7f01a3fdb910 v0x7f01a3fdb710_0))
                (=> v0x7f01a3fdb850_0 E0x7f01a3fdb910)
                (=> v0x7f01a3fdbad0_0 a!5)
                a!6
                v0x7f01a3fdbad0_0
                v0x7f01a3fdca50_0
                (<= v0x7f01a3fd8050_0 v0x7f01a3fd9fd0_0)
                (>= v0x7f01a3fd8050_0 v0x7f01a3fd9fd0_0)
                (<= v0x7f01a3fd8150_0 v0x7f01a3fdbb90_0)
                (>= v0x7f01a3fd8150_0 v0x7f01a3fdbb90_0)
                (<= v0x7f01a3fd5010_0 v0x7f01a3fdbc50_0)
                (>= v0x7f01a3fd5010_0 v0x7f01a3fdbc50_0)
                (= v0x7f01a3fd87d0_0 (= v0x7f01a3fd8710_0 0.0))
                (= v0x7f01a3fd8c10_0 (< v0x7f01a3fd7f90_0 2.0))
                (= v0x7f01a3fd8dd0_0 (ite v0x7f01a3fd8c10_0 1.0 0.0))
                (= v0x7f01a3fd8f10_0 (+ v0x7f01a3fd8dd0_0 v0x7f01a3fd7f90_0))
                (= v0x7f01a3fd9890_0 (= v0x7f01a3fd97d0_0 0.0))
                (= v0x7f01a3fd9c90_0 (= v0x7f01a3fd7e10_0 0.0))
                (= v0x7f01a3fd9dd0_0 (ite v0x7f01a3fd9c90_0 1.0 0.0))
                (= v0x7f01a3fda750_0 (= v0x7f01a3fd8090_0 0.0))
                (= v0x7f01a3fdab10_0 (> v0x7f01a3fd9110_0 1.0))
                (= v0x7f01a3fdac50_0
                   (ite v0x7f01a3fdab10_0 1.0 v0x7f01a3fd8090_0))
                (= v0x7f01a3fdb050_0 (> v0x7f01a3fd9110_0 0.0))
                (= v0x7f01a3fdb190_0 (+ v0x7f01a3fd9110_0 (- 1.0)))
                (= v0x7f01a3fdb350_0
                   (ite v0x7f01a3fdb050_0 v0x7f01a3fdb190_0 v0x7f01a3fd9110_0))
                (= v0x7f01a3fdb490_0 (= v0x7f01a3fd9fd0_0 0.0))
                (= v0x7f01a3fdb5d0_0 (= v0x7f01a3fdb350_0 0.0))
                (= v0x7f01a3fdb710_0 (and v0x7f01a3fdb490_0 v0x7f01a3fdb5d0_0))
                (= v0x7f01a3fdc7d0_0 (not (= v0x7f01a3fdbb90_0 0.0)))
                (= v0x7f01a3fdc910_0 (= v0x7f01a3fdbc50_0 0.0))
                (= v0x7f01a3fdca50_0 (or v0x7f01a3fdc910_0 v0x7f01a3fdc7d0_0)))))
  (=> F0x7f01a3fdd6d0 a!7))))
(assert (=> F0x7f01a3fdd6d0 F0x7f01a3fdd790))
(assert (let ((a!1 (=> v0x7f01a3fd9050_0
               (or (and v0x7f01a3fd8910_0
                        E0x7f01a3fd91d0
                        (<= v0x7f01a3fd9110_0 v0x7f01a3fd8f10_0)
                        (>= v0x7f01a3fd9110_0 v0x7f01a3fd8f10_0))
                   (and v0x7f01a3fd8650_0
                        E0x7f01a3fd9390
                        v0x7f01a3fd87d0_0
                        (<= v0x7f01a3fd9110_0 v0x7f01a3fd7f90_0)
                        (>= v0x7f01a3fd9110_0 v0x7f01a3fd7f90_0)))))
      (a!2 (=> v0x7f01a3fd9050_0
               (or (and E0x7f01a3fd91d0 (not E0x7f01a3fd9390))
                   (and E0x7f01a3fd9390 (not E0x7f01a3fd91d0)))))
      (a!3 (=> v0x7f01a3fd9f10_0
               (or (and v0x7f01a3fd99d0_0
                        E0x7f01a3fda090
                        (<= v0x7f01a3fd9fd0_0 v0x7f01a3fd9dd0_0)
                        (>= v0x7f01a3fd9fd0_0 v0x7f01a3fd9dd0_0))
                   (and v0x7f01a3fd9050_0
                        E0x7f01a3fda250
                        v0x7f01a3fd9890_0
                        (<= v0x7f01a3fd9fd0_0 v0x7f01a3fd7e10_0)
                        (>= v0x7f01a3fd9fd0_0 v0x7f01a3fd7e10_0)))))
      (a!4 (=> v0x7f01a3fd9f10_0
               (or (and E0x7f01a3fda090 (not E0x7f01a3fda250))
                   (and E0x7f01a3fda250 (not E0x7f01a3fda090)))))
      (a!5 (or (and v0x7f01a3fda890_0
                    E0x7f01a3fdbd10
                    (<= v0x7f01a3fdbb90_0 v0x7f01a3fd9110_0)
                    (>= v0x7f01a3fdbb90_0 v0x7f01a3fd9110_0)
                    (<= v0x7f01a3fdbc50_0 v0x7f01a3fdac50_0)
                    (>= v0x7f01a3fdbc50_0 v0x7f01a3fdac50_0))
               (and v0x7f01a3fdb850_0
                    E0x7f01a3fdbfd0
                    (and (<= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0)
                         (>= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0))
                    (<= v0x7f01a3fdbc50_0 v0x7f01a3fd8090_0)
                    (>= v0x7f01a3fdbc50_0 v0x7f01a3fd8090_0))
               (and v0x7f01a3fdad90_0
                    E0x7f01a3fdc290
                    (not v0x7f01a3fdb710_0)
                    (and (<= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0)
                         (>= v0x7f01a3fdbb90_0 v0x7f01a3fdb350_0))
                    (<= v0x7f01a3fdbc50_0 0.0)
                    (>= v0x7f01a3fdbc50_0 0.0))))
      (a!6 (=> v0x7f01a3fdbad0_0
               (or (and E0x7f01a3fdbd10
                        (not E0x7f01a3fdbfd0)
                        (not E0x7f01a3fdc290))
                   (and E0x7f01a3fdbfd0
                        (not E0x7f01a3fdbd10)
                        (not E0x7f01a3fdc290))
                   (and E0x7f01a3fdc290
                        (not E0x7f01a3fdbd10)
                        (not E0x7f01a3fdbfd0))))))
(let ((a!7 (and (=> v0x7f01a3fd8910_0
                    (and v0x7f01a3fd8650_0
                         E0x7f01a3fd89d0
                         (not v0x7f01a3fd87d0_0)))
                (=> v0x7f01a3fd8910_0 E0x7f01a3fd89d0)
                a!1
                a!2
                (=> v0x7f01a3fd99d0_0
                    (and v0x7f01a3fd9050_0
                         E0x7f01a3fd9a90
                         (not v0x7f01a3fd9890_0)))
                (=> v0x7f01a3fd99d0_0 E0x7f01a3fd9a90)
                a!3
                a!4
                (=> v0x7f01a3fda890_0
                    (and v0x7f01a3fd9f10_0 E0x7f01a3fda950 v0x7f01a3fda750_0))
                (=> v0x7f01a3fda890_0 E0x7f01a3fda950)
                (=> v0x7f01a3fdad90_0
                    (and v0x7f01a3fd9f10_0
                         E0x7f01a3fdae50
                         (not v0x7f01a3fda750_0)))
                (=> v0x7f01a3fdad90_0 E0x7f01a3fdae50)
                (=> v0x7f01a3fdb850_0
                    (and v0x7f01a3fdad90_0 E0x7f01a3fdb910 v0x7f01a3fdb710_0))
                (=> v0x7f01a3fdb850_0 E0x7f01a3fdb910)
                (=> v0x7f01a3fdbad0_0 a!5)
                a!6
                v0x7f01a3fdbad0_0
                (not v0x7f01a3fdca50_0)
                (= v0x7f01a3fd87d0_0 (= v0x7f01a3fd8710_0 0.0))
                (= v0x7f01a3fd8c10_0 (< v0x7f01a3fd7f90_0 2.0))
                (= v0x7f01a3fd8dd0_0 (ite v0x7f01a3fd8c10_0 1.0 0.0))
                (= v0x7f01a3fd8f10_0 (+ v0x7f01a3fd8dd0_0 v0x7f01a3fd7f90_0))
                (= v0x7f01a3fd9890_0 (= v0x7f01a3fd97d0_0 0.0))
                (= v0x7f01a3fd9c90_0 (= v0x7f01a3fd7e10_0 0.0))
                (= v0x7f01a3fd9dd0_0 (ite v0x7f01a3fd9c90_0 1.0 0.0))
                (= v0x7f01a3fda750_0 (= v0x7f01a3fd8090_0 0.0))
                (= v0x7f01a3fdab10_0 (> v0x7f01a3fd9110_0 1.0))
                (= v0x7f01a3fdac50_0
                   (ite v0x7f01a3fdab10_0 1.0 v0x7f01a3fd8090_0))
                (= v0x7f01a3fdb050_0 (> v0x7f01a3fd9110_0 0.0))
                (= v0x7f01a3fdb190_0 (+ v0x7f01a3fd9110_0 (- 1.0)))
                (= v0x7f01a3fdb350_0
                   (ite v0x7f01a3fdb050_0 v0x7f01a3fdb190_0 v0x7f01a3fd9110_0))
                (= v0x7f01a3fdb490_0 (= v0x7f01a3fd9fd0_0 0.0))
                (= v0x7f01a3fdb5d0_0 (= v0x7f01a3fdb350_0 0.0))
                (= v0x7f01a3fdb710_0 (and v0x7f01a3fdb490_0 v0x7f01a3fdb5d0_0))
                (= v0x7f01a3fdc7d0_0 (not (= v0x7f01a3fdbb90_0 0.0)))
                (= v0x7f01a3fdc910_0 (= v0x7f01a3fdbc50_0 0.0))
                (= v0x7f01a3fdca50_0 (or v0x7f01a3fdc910_0 v0x7f01a3fdc7d0_0)))))
  (=> F0x7f01a3fdd850 a!7))))
(assert (=> F0x7f01a3fdd850 F0x7f01a3fdd790))
(assert (=> F0x7f01a3fdd990 (or F0x7f01a3fdd690 F0x7f01a3fdd6d0)))
(assert (=> F0x7f01a3fdd950 F0x7f01a3fdd850))
(assert (=> pre!entry!0 (=> F0x7f01a3fdd5d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f01a3fdd790 (>= v0x7f01a3fd8090_0 0.0))))
(assert (let ((a!1 (=> F0x7f01a3fdd790
               (or (<= v0x7f01a3fd8090_0 0.0) (not (<= v0x7f01a3fd7f90_0 1.0))))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f01a3fdd990 (>= v0x7f01a3fd5010_0 0.0))))
(assert (let ((a!1 (=> F0x7f01a3fdd990
               (or (<= v0x7f01a3fd5010_0 0.0) (not (<= v0x7f01a3fd8150_0 1.0))))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i35.i.i!0 (=> F0x7f01a3fdd950 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i35.i.i!0) (not lemma!bb1.i.i35.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f01a3fdd5d0)
; (error: F0x7f01a3fdd950)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i35.i.i!0)
