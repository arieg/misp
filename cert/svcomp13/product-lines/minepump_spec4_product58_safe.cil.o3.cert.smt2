(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7f34c5b20b50 () Bool)
(declare-fun F0x7f34c5b20c50 () Bool)
(declare-fun post!bb1.i.i34.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i34.i.i!0 () Bool)
(declare-fun v0x7f34c5b1fc10_0 () Bool)
(declare-fun v0x7f34c5b1fad0_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun v0x7f34c5b1c750_0 () Real)
(declare-fun v0x7f34c5b1dfd0_0 () Real)
(declare-fun v0x7f34c5b1bd50_0 () Real)
(declare-fun v0x7f34c5b1fd50_0 () Bool)
(declare-fun v0x7f34c5b1e190_0 () Real)
(declare-fun E0x7f34c5b1f310 () Bool)
(declare-fun v0x7f34c5b1cc10_0 () Bool)
(declare-fun v0x7f34c5b1de90_0 () Bool)
(declare-fun v0x7f34c5b1ad90_0 () Real)
(declare-fun v0x7f34c5b1ed10_0 () Real)
(declare-fun E0x7f34c5b1f090 () Bool)
(declare-fun v0x7f34c5b1ec50_0 () Real)
(declare-fun E0x7f34c5b1f510 () Bool)
(declare-fun v0x7f34c5b1bb90_0 () Bool)
(declare-fun E0x7f34c5b1edd0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun E0x7f34c5b1e9d0 () Bool)
(declare-fun v0x7f34c5b1e910_0 () Bool)
(declare-fun E0x7f34c5b1e4d0 () Bool)
(declare-fun E0x7f34c5b1dc90 () Bool)
(declare-fun v0x7f34c5b1d6d0_0 () Bool)
(declare-fun v0x7f34c5b1dbd0_0 () Bool)
(declare-fun v0x7f34c5b1da90_0 () Bool)
(declare-fun v0x7f34c5b1b690_0 () Real)
(declare-fun v0x7f34c5b1d810_0 () Bool)
(declare-fun v0x7f34c5b1af10_0 () Real)
(declare-fun E0x7f34c5b1d1d0 () Bool)
(declare-fun v0x7f34c5b1e7d0_0 () Real)
(declare-fun v0x7f34c5b1c810_0 () Bool)
(declare-fun v0x7f34c5b1e2d0_0 () Bool)
(declare-fun v0x7f34c5b1b010_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7f34c5b1c090_0 () Real)
(declare-fun E0x7f34c5b1d010 () Bool)
(declare-fun v0x7f34c5b1be90_0 () Real)
(declare-fun E0x7f34c5b1c150 () Bool)
(declare-fun E0x7f34c5b1d8d0 () Bool)
(declare-fun E0x7f34c5b1ca10 () Bool)
(declare-fun v0x7f34c5b1b750_0 () Bool)
(declare-fun E0x7f34c5b1c310 () Bool)
(declare-fun E0x7f34c5b1b950 () Bool)
(declare-fun v0x7f34c5b1b5d0_0 () Bool)
(declare-fun v0x7f34c5b1b890_0 () Bool)
(declare-fun v0x7f34c5b1e410_0 () Bool)
(declare-fun v0x7f34c5b1e690_0 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun F0x7f34c5b208d0 () Bool)
(declare-fun v0x7f34c5b19010_0 () Real)
(declare-fun F0x7f34c5b20a90 () Bool)
(declare-fun v0x7f34c5b1b0d0_0 () Real)
(declare-fun v0x7f34c5b1ce90_0 () Bool)
(declare-fun v0x7f34c5b1bfd0_0 () Bool)
(declare-fun F0x7f34c5b20c90 () Bool)
(declare-fun v0x7f34c5b1afd0_0 () Real)
(declare-fun v0x7f34c5b1cf50_0 () Real)
(declare-fun v0x7f34c5b19110_0 () Bool)
(declare-fun F0x7f34c5b209d0 () Bool)
(declare-fun v0x7f34c5b1c950_0 () Bool)
(declare-fun v0x7f34c5b1eb90_0 () Bool)
(declare-fun v0x7f34c5b1cd50_0 () Real)
(declare-fun F0x7f34c5b20990 () Bool)

(assert (=> F0x7f34c5b20990
    (and v0x7f34c5b19110_0
         (<= v0x7f34c5b1afd0_0 0.0)
         (>= v0x7f34c5b1afd0_0 0.0)
         (<= v0x7f34c5b1b0d0_0 0.0)
         (>= v0x7f34c5b1b0d0_0 0.0)
         (<= v0x7f34c5b19010_0 1.0)
         (>= v0x7f34c5b19010_0 1.0))))
(assert (=> F0x7f34c5b20990 F0x7f34c5b208d0))
(assert (let ((a!1 (=> v0x7f34c5b1bfd0_0
               (or (and v0x7f34c5b1b890_0
                        E0x7f34c5b1c150
                        (<= v0x7f34c5b1c090_0 v0x7f34c5b1be90_0)
                        (>= v0x7f34c5b1c090_0 v0x7f34c5b1be90_0))
                   (and v0x7f34c5b1b5d0_0
                        E0x7f34c5b1c310
                        v0x7f34c5b1b750_0
                        (<= v0x7f34c5b1c090_0 v0x7f34c5b1b010_0)
                        (>= v0x7f34c5b1c090_0 v0x7f34c5b1b010_0)))))
      (a!2 (=> v0x7f34c5b1bfd0_0
               (or (and E0x7f34c5b1c150 (not E0x7f34c5b1c310))
                   (and E0x7f34c5b1c310 (not E0x7f34c5b1c150)))))
      (a!3 (=> v0x7f34c5b1ce90_0
               (or (and v0x7f34c5b1c950_0
                        E0x7f34c5b1d010
                        (<= v0x7f34c5b1cf50_0 v0x7f34c5b1cd50_0)
                        (>= v0x7f34c5b1cf50_0 v0x7f34c5b1cd50_0))
                   (and v0x7f34c5b1bfd0_0
                        E0x7f34c5b1d1d0
                        v0x7f34c5b1c810_0
                        (<= v0x7f34c5b1cf50_0 v0x7f34c5b1af10_0)
                        (>= v0x7f34c5b1cf50_0 v0x7f34c5b1af10_0)))))
      (a!4 (=> v0x7f34c5b1ce90_0
               (or (and E0x7f34c5b1d010 (not E0x7f34c5b1d1d0))
                   (and E0x7f34c5b1d1d0 (not E0x7f34c5b1d010)))))
      (a!5 (or (and v0x7f34c5b1e410_0
                    E0x7f34c5b1edd0
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0))
                    (<= v0x7f34c5b1ed10_0 v0x7f34c5b1e7d0_0)
                    (>= v0x7f34c5b1ed10_0 v0x7f34c5b1e7d0_0))
               (and v0x7f34c5b1d810_0
                    E0x7f34c5b1f090
                    (not v0x7f34c5b1da90_0)
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0))
                    (and (<= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)
                         (>= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)))
               (and v0x7f34c5b1e910_0
                    E0x7f34c5b1f310
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0))
                    (and (<= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)
                         (>= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)))
               (and v0x7f34c5b1dbd0_0
                    E0x7f34c5b1f510
                    (not v0x7f34c5b1e2d0_0)
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0))
                    (<= v0x7f34c5b1ed10_0 0.0)
                    (>= v0x7f34c5b1ed10_0 0.0))))
      (a!6 (=> v0x7f34c5b1eb90_0
               (or (and E0x7f34c5b1edd0
                        (not E0x7f34c5b1f090)
                        (not E0x7f34c5b1f310)
                        (not E0x7f34c5b1f510))
                   (and E0x7f34c5b1f090
                        (not E0x7f34c5b1edd0)
                        (not E0x7f34c5b1f310)
                        (not E0x7f34c5b1f510))
                   (and E0x7f34c5b1f310
                        (not E0x7f34c5b1edd0)
                        (not E0x7f34c5b1f090)
                        (not E0x7f34c5b1f510))
                   (and E0x7f34c5b1f510
                        (not E0x7f34c5b1edd0)
                        (not E0x7f34c5b1f090)
                        (not E0x7f34c5b1f310))))))
(let ((a!7 (and (=> v0x7f34c5b1b890_0
                    (and v0x7f34c5b1b5d0_0
                         E0x7f34c5b1b950
                         (not v0x7f34c5b1b750_0)))
                (=> v0x7f34c5b1b890_0 E0x7f34c5b1b950)
                a!1
                a!2
                (=> v0x7f34c5b1c950_0
                    (and v0x7f34c5b1bfd0_0
                         E0x7f34c5b1ca10
                         (not v0x7f34c5b1c810_0)))
                (=> v0x7f34c5b1c950_0 E0x7f34c5b1ca10)
                a!3
                a!4
                (=> v0x7f34c5b1d810_0
                    (and v0x7f34c5b1ce90_0 E0x7f34c5b1d8d0 v0x7f34c5b1d6d0_0))
                (=> v0x7f34c5b1d810_0 E0x7f34c5b1d8d0)
                (=> v0x7f34c5b1dbd0_0
                    (and v0x7f34c5b1ce90_0
                         E0x7f34c5b1dc90
                         (not v0x7f34c5b1d6d0_0)))
                (=> v0x7f34c5b1dbd0_0 E0x7f34c5b1dc90)
                (=> v0x7f34c5b1e410_0
                    (and v0x7f34c5b1d810_0 E0x7f34c5b1e4d0 v0x7f34c5b1da90_0))
                (=> v0x7f34c5b1e410_0 E0x7f34c5b1e4d0)
                (=> v0x7f34c5b1e910_0
                    (and v0x7f34c5b1dbd0_0 E0x7f34c5b1e9d0 v0x7f34c5b1e2d0_0))
                (=> v0x7f34c5b1e910_0 E0x7f34c5b1e9d0)
                (=> v0x7f34c5b1eb90_0 a!5)
                a!6
                v0x7f34c5b1eb90_0
                v0x7f34c5b1fd50_0
                (<= v0x7f34c5b1afd0_0 v0x7f34c5b1ed10_0)
                (>= v0x7f34c5b1afd0_0 v0x7f34c5b1ed10_0)
                (<= v0x7f34c5b1b0d0_0 v0x7f34c5b1cf50_0)
                (>= v0x7f34c5b1b0d0_0 v0x7f34c5b1cf50_0)
                (<= v0x7f34c5b19010_0 v0x7f34c5b1ec50_0)
                (>= v0x7f34c5b19010_0 v0x7f34c5b1ec50_0)
                (= v0x7f34c5b1b750_0 (= v0x7f34c5b1b690_0 0.0))
                (= v0x7f34c5b1bb90_0 (< v0x7f34c5b1b010_0 2.0))
                (= v0x7f34c5b1bd50_0 (ite v0x7f34c5b1bb90_0 1.0 0.0))
                (= v0x7f34c5b1be90_0 (+ v0x7f34c5b1bd50_0 v0x7f34c5b1b010_0))
                (= v0x7f34c5b1c810_0 (= v0x7f34c5b1c750_0 0.0))
                (= v0x7f34c5b1cc10_0 (= v0x7f34c5b1af10_0 0.0))
                (= v0x7f34c5b1cd50_0 (ite v0x7f34c5b1cc10_0 1.0 0.0))
                (= v0x7f34c5b1d6d0_0 (= v0x7f34c5b1ad90_0 0.0))
                (= v0x7f34c5b1da90_0 (> v0x7f34c5b1c090_0 1.0))
                (= v0x7f34c5b1de90_0 (> v0x7f34c5b1c090_0 0.0))
                (= v0x7f34c5b1dfd0_0 (+ v0x7f34c5b1c090_0 (- 1.0)))
                (= v0x7f34c5b1e190_0
                   (ite v0x7f34c5b1de90_0 v0x7f34c5b1dfd0_0 v0x7f34c5b1c090_0))
                (= v0x7f34c5b1e2d0_0 (= v0x7f34c5b1e190_0 0.0))
                (= v0x7f34c5b1e690_0 (= v0x7f34c5b1cf50_0 0.0))
                (= v0x7f34c5b1e7d0_0
                   (ite v0x7f34c5b1e690_0 1.0 v0x7f34c5b1ad90_0))
                (= v0x7f34c5b1fad0_0 (not (= v0x7f34c5b1ec50_0 0.0)))
                (= v0x7f34c5b1fc10_0 (= v0x7f34c5b1ed10_0 0.0))
                (= v0x7f34c5b1fd50_0 (or v0x7f34c5b1fc10_0 v0x7f34c5b1fad0_0)))))
  (=> F0x7f34c5b209d0 a!7))))
(assert (=> F0x7f34c5b209d0 F0x7f34c5b20a90))
(assert (let ((a!1 (=> v0x7f34c5b1bfd0_0
               (or (and v0x7f34c5b1b890_0
                        E0x7f34c5b1c150
                        (<= v0x7f34c5b1c090_0 v0x7f34c5b1be90_0)
                        (>= v0x7f34c5b1c090_0 v0x7f34c5b1be90_0))
                   (and v0x7f34c5b1b5d0_0
                        E0x7f34c5b1c310
                        v0x7f34c5b1b750_0
                        (<= v0x7f34c5b1c090_0 v0x7f34c5b1b010_0)
                        (>= v0x7f34c5b1c090_0 v0x7f34c5b1b010_0)))))
      (a!2 (=> v0x7f34c5b1bfd0_0
               (or (and E0x7f34c5b1c150 (not E0x7f34c5b1c310))
                   (and E0x7f34c5b1c310 (not E0x7f34c5b1c150)))))
      (a!3 (=> v0x7f34c5b1ce90_0
               (or (and v0x7f34c5b1c950_0
                        E0x7f34c5b1d010
                        (<= v0x7f34c5b1cf50_0 v0x7f34c5b1cd50_0)
                        (>= v0x7f34c5b1cf50_0 v0x7f34c5b1cd50_0))
                   (and v0x7f34c5b1bfd0_0
                        E0x7f34c5b1d1d0
                        v0x7f34c5b1c810_0
                        (<= v0x7f34c5b1cf50_0 v0x7f34c5b1af10_0)
                        (>= v0x7f34c5b1cf50_0 v0x7f34c5b1af10_0)))))
      (a!4 (=> v0x7f34c5b1ce90_0
               (or (and E0x7f34c5b1d010 (not E0x7f34c5b1d1d0))
                   (and E0x7f34c5b1d1d0 (not E0x7f34c5b1d010)))))
      (a!5 (or (and v0x7f34c5b1e410_0
                    E0x7f34c5b1edd0
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0))
                    (<= v0x7f34c5b1ed10_0 v0x7f34c5b1e7d0_0)
                    (>= v0x7f34c5b1ed10_0 v0x7f34c5b1e7d0_0))
               (and v0x7f34c5b1d810_0
                    E0x7f34c5b1f090
                    (not v0x7f34c5b1da90_0)
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1c090_0))
                    (and (<= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)
                         (>= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)))
               (and v0x7f34c5b1e910_0
                    E0x7f34c5b1f310
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0))
                    (and (<= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)
                         (>= v0x7f34c5b1ed10_0 v0x7f34c5b1ad90_0)))
               (and v0x7f34c5b1dbd0_0
                    E0x7f34c5b1f510
                    (not v0x7f34c5b1e2d0_0)
                    (and (<= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0)
                         (>= v0x7f34c5b1ec50_0 v0x7f34c5b1e190_0))
                    (<= v0x7f34c5b1ed10_0 0.0)
                    (>= v0x7f34c5b1ed10_0 0.0))))
      (a!6 (=> v0x7f34c5b1eb90_0
               (or (and E0x7f34c5b1edd0
                        (not E0x7f34c5b1f090)
                        (not E0x7f34c5b1f310)
                        (not E0x7f34c5b1f510))
                   (and E0x7f34c5b1f090
                        (not E0x7f34c5b1edd0)
                        (not E0x7f34c5b1f310)
                        (not E0x7f34c5b1f510))
                   (and E0x7f34c5b1f310
                        (not E0x7f34c5b1edd0)
                        (not E0x7f34c5b1f090)
                        (not E0x7f34c5b1f510))
                   (and E0x7f34c5b1f510
                        (not E0x7f34c5b1edd0)
                        (not E0x7f34c5b1f090)
                        (not E0x7f34c5b1f310))))))
(let ((a!7 (and (=> v0x7f34c5b1b890_0
                    (and v0x7f34c5b1b5d0_0
                         E0x7f34c5b1b950
                         (not v0x7f34c5b1b750_0)))
                (=> v0x7f34c5b1b890_0 E0x7f34c5b1b950)
                a!1
                a!2
                (=> v0x7f34c5b1c950_0
                    (and v0x7f34c5b1bfd0_0
                         E0x7f34c5b1ca10
                         (not v0x7f34c5b1c810_0)))
                (=> v0x7f34c5b1c950_0 E0x7f34c5b1ca10)
                a!3
                a!4
                (=> v0x7f34c5b1d810_0
                    (and v0x7f34c5b1ce90_0 E0x7f34c5b1d8d0 v0x7f34c5b1d6d0_0))
                (=> v0x7f34c5b1d810_0 E0x7f34c5b1d8d0)
                (=> v0x7f34c5b1dbd0_0
                    (and v0x7f34c5b1ce90_0
                         E0x7f34c5b1dc90
                         (not v0x7f34c5b1d6d0_0)))
                (=> v0x7f34c5b1dbd0_0 E0x7f34c5b1dc90)
                (=> v0x7f34c5b1e410_0
                    (and v0x7f34c5b1d810_0 E0x7f34c5b1e4d0 v0x7f34c5b1da90_0))
                (=> v0x7f34c5b1e410_0 E0x7f34c5b1e4d0)
                (=> v0x7f34c5b1e910_0
                    (and v0x7f34c5b1dbd0_0 E0x7f34c5b1e9d0 v0x7f34c5b1e2d0_0))
                (=> v0x7f34c5b1e910_0 E0x7f34c5b1e9d0)
                (=> v0x7f34c5b1eb90_0 a!5)
                a!6
                v0x7f34c5b1eb90_0
                (not v0x7f34c5b1fd50_0)
                (= v0x7f34c5b1b750_0 (= v0x7f34c5b1b690_0 0.0))
                (= v0x7f34c5b1bb90_0 (< v0x7f34c5b1b010_0 2.0))
                (= v0x7f34c5b1bd50_0 (ite v0x7f34c5b1bb90_0 1.0 0.0))
                (= v0x7f34c5b1be90_0 (+ v0x7f34c5b1bd50_0 v0x7f34c5b1b010_0))
                (= v0x7f34c5b1c810_0 (= v0x7f34c5b1c750_0 0.0))
                (= v0x7f34c5b1cc10_0 (= v0x7f34c5b1af10_0 0.0))
                (= v0x7f34c5b1cd50_0 (ite v0x7f34c5b1cc10_0 1.0 0.0))
                (= v0x7f34c5b1d6d0_0 (= v0x7f34c5b1ad90_0 0.0))
                (= v0x7f34c5b1da90_0 (> v0x7f34c5b1c090_0 1.0))
                (= v0x7f34c5b1de90_0 (> v0x7f34c5b1c090_0 0.0))
                (= v0x7f34c5b1dfd0_0 (+ v0x7f34c5b1c090_0 (- 1.0)))
                (= v0x7f34c5b1e190_0
                   (ite v0x7f34c5b1de90_0 v0x7f34c5b1dfd0_0 v0x7f34c5b1c090_0))
                (= v0x7f34c5b1e2d0_0 (= v0x7f34c5b1e190_0 0.0))
                (= v0x7f34c5b1e690_0 (= v0x7f34c5b1cf50_0 0.0))
                (= v0x7f34c5b1e7d0_0
                   (ite v0x7f34c5b1e690_0 1.0 v0x7f34c5b1ad90_0))
                (= v0x7f34c5b1fad0_0 (not (= v0x7f34c5b1ec50_0 0.0)))
                (= v0x7f34c5b1fc10_0 (= v0x7f34c5b1ed10_0 0.0))
                (= v0x7f34c5b1fd50_0 (or v0x7f34c5b1fc10_0 v0x7f34c5b1fad0_0)))))
  (=> F0x7f34c5b20b50 a!7))))
(assert (=> F0x7f34c5b20b50 F0x7f34c5b20a90))
(assert (=> F0x7f34c5b20c90 (or F0x7f34c5b20990 F0x7f34c5b209d0)))
(assert (=> F0x7f34c5b20c50 F0x7f34c5b20b50))
(assert (=> pre!entry!0 (=> F0x7f34c5b208d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7f34c5b20a90 (>= v0x7f34c5b1ad90_0 0.0))))
(assert (let ((a!1 (=> F0x7f34c5b20a90
               (or (not (<= v0x7f34c5b1b010_0 1.0)) (<= v0x7f34c5b1ad90_0 0.0)))))
  (=> pre!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i!0 (=> F0x7f34c5b20c90 (>= v0x7f34c5b1afd0_0 0.0))))
(assert (let ((a!1 (=> F0x7f34c5b20c90
               (or (not (<= v0x7f34c5b19010_0 1.0)) (<= v0x7f34c5b1afd0_0 0.0)))))
  (= lemma!bb1.i.i!1 a!1)))
(assert (= lemma!bb1.i.i34.i.i!0 (=> F0x7f34c5b20c50 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb1.i.i34.i.i!0) (not lemma!bb1.i.i34.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7f34c5b208d0)
; (error: F0x7f34c5b20c50)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb1.i.i34.i.i!0)
