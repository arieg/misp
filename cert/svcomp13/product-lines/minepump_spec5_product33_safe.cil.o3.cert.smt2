(declare-fun lemma!bb2.i.i16.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!entry!0 () Bool)
(declare-fun F0x7fabc86f9890 () Bool)
(declare-fun F0x7fabc86f9790 () Bool)
(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun F0x7fabc86f9510 () Bool)
(declare-fun v0x7fabc86f87d0_0 () Bool)
(declare-fun v0x7fabc86f7390_0 () Real)
(declare-fun v0x7fabc86f7250_0 () Bool)
(declare-fun v0x7fabc86f5f10_0 () Real)
(declare-fun v0x7fabc86f5850_0 () Real)
(declare-fun v0x7fabc86f8a50_0 () Bool)
(declare-fun v0x7fabc86f7550_0 () Real)
(declare-fun E0x7fabc86f7e50 () Bool)
(declare-fun E0x7fabc86f8090 () Bool)
(declare-fun v0x7fabc86f7a10_0 () Real)
(declare-fun E0x7fabc86f7b90 () Bool)
(declare-fun v0x7fabc86f7950_0 () Bool)
(declare-fun E0x7fabc86f7750 () Bool)
(declare-fun v0x7fabc86f7690_0 () Bool)
(declare-fun v0x7fabc86f8690_0 () Bool)
(declare-fun E0x7fabc86f6c90 () Bool)
(declare-fun v0x7fabc86f6bd0_0 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun v0x7fabc86f5190_0 () Real)
(declare-fun v0x7fabc86f8550_0 () Bool)
(declare-fun E0x7fabc86f64d0 () Bool)
(declare-fun v0x7fabc86f6050_0 () Real)
(declare-fun v0x7fabc86f6f90_0 () Bool)
(declare-fun v0x7fabc86f6190_0 () Bool)
(declare-fun F0x7fabc86f9850 () Bool)
(declare-fun E0x7fabc86f5b10 () Bool)
(declare-fun v0x7fabc86f5790_0 () Bool)
(declare-fun v0x7fabc86f7ad0_0 () Real)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun v0x7fabc86f6a90_0 () Bool)
(declare-fun E0x7fabc86f7050 () Bool)
(declare-fun v0x7fabc86f5310_0 () Real)
(declare-fun v0x7fabc86f8910_0 () Bool)
(declare-fun v0x7fabc86f5a50_0 () Bool)
(declare-fun F0x7fabc86f9690 () Bool)
(declare-fun E0x7fabc86f6310 () Bool)
(declare-fun v0x7fabc86f5910_0 () Bool)
(declare-fun v0x7fabc86f4010_0 () Real)
(declare-fun F0x7fabc86f95d0 () Bool)
(declare-fun v0x7fabc86f53d0_0 () Real)
(declare-fun v0x7fabc86f5d50_0 () Bool)
(declare-fun v0x7fabc86f4110_0 () Bool)
(declare-fun v0x7fabc86f6e50_0 () Bool)
(declare-fun v0x7fabc86f6250_0 () Real)
(declare-fun F0x7fabc86f9750 () Bool)

(assert (=> F0x7fabc86f9750
    (and v0x7fabc86f4110_0
         (<= v0x7fabc86f53d0_0 1.0)
         (>= v0x7fabc86f53d0_0 1.0)
         (<= v0x7fabc86f4010_0 0.0)
         (>= v0x7fabc86f4010_0 0.0))))
(assert (=> F0x7fabc86f9750 F0x7fabc86f9690))
(assert (let ((a!1 (=> v0x7fabc86f6190_0
               (or (and v0x7fabc86f5a50_0
                        E0x7fabc86f6310
                        (<= v0x7fabc86f6250_0 v0x7fabc86f6050_0)
                        (>= v0x7fabc86f6250_0 v0x7fabc86f6050_0))
                   (and v0x7fabc86f5790_0
                        E0x7fabc86f64d0
                        v0x7fabc86f5910_0
                        (<= v0x7fabc86f6250_0 v0x7fabc86f5190_0)
                        (>= v0x7fabc86f6250_0 v0x7fabc86f5190_0)))))
      (a!2 (=> v0x7fabc86f6190_0
               (or (and E0x7fabc86f6310 (not E0x7fabc86f64d0))
                   (and E0x7fabc86f64d0 (not E0x7fabc86f6310)))))
      (a!3 (or (and v0x7fabc86f7690_0
                    E0x7fabc86f7b90
                    (and (<= v0x7fabc86f7a10_0 v0x7fabc86f6250_0)
                         (>= v0x7fabc86f7a10_0 v0x7fabc86f6250_0))
                    (and (<= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)
                         (>= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)))
               (and v0x7fabc86f6bd0_0
                    E0x7fabc86f7e50
                    v0x7fabc86f6e50_0
                    (and (<= v0x7fabc86f7a10_0 v0x7fabc86f6250_0)
                         (>= v0x7fabc86f7a10_0 v0x7fabc86f6250_0))
                    (<= v0x7fabc86f7ad0_0 1.0)
                    (>= v0x7fabc86f7ad0_0 1.0))
               (and v0x7fabc86f6f90_0
                    E0x7fabc86f8090
                    (<= v0x7fabc86f7a10_0 v0x7fabc86f7550_0)
                    (>= v0x7fabc86f7a10_0 v0x7fabc86f7550_0)
                    (and (<= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)
                         (>= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)))))
      (a!4 (=> v0x7fabc86f7950_0
               (or (and E0x7fabc86f7b90
                        (not E0x7fabc86f7e50)
                        (not E0x7fabc86f8090))
                   (and E0x7fabc86f7e50
                        (not E0x7fabc86f7b90)
                        (not E0x7fabc86f8090))
                   (and E0x7fabc86f8090
                        (not E0x7fabc86f7b90)
                        (not E0x7fabc86f7e50))))))
(let ((a!5 (and (=> v0x7fabc86f5a50_0
                    (and v0x7fabc86f5790_0
                         E0x7fabc86f5b10
                         (not v0x7fabc86f5910_0)))
                (=> v0x7fabc86f5a50_0 E0x7fabc86f5b10)
                a!1
                a!2
                (=> v0x7fabc86f6bd0_0
                    (and v0x7fabc86f6190_0 E0x7fabc86f6c90 v0x7fabc86f6a90_0))
                (=> v0x7fabc86f6bd0_0 E0x7fabc86f6c90)
                (=> v0x7fabc86f6f90_0
                    (and v0x7fabc86f6190_0
                         E0x7fabc86f7050
                         (not v0x7fabc86f6a90_0)))
                (=> v0x7fabc86f6f90_0 E0x7fabc86f7050)
                (=> v0x7fabc86f7690_0
                    (and v0x7fabc86f6bd0_0
                         E0x7fabc86f7750
                         (not v0x7fabc86f6e50_0)))
                (=> v0x7fabc86f7690_0 E0x7fabc86f7750)
                (=> v0x7fabc86f7950_0 a!3)
                a!4
                v0x7fabc86f7950_0
                (not v0x7fabc86f8a50_0)
                (<= v0x7fabc86f53d0_0 v0x7fabc86f7a10_0)
                (>= v0x7fabc86f53d0_0 v0x7fabc86f7a10_0)
                (<= v0x7fabc86f4010_0 v0x7fabc86f7ad0_0)
                (>= v0x7fabc86f4010_0 v0x7fabc86f7ad0_0)
                (= v0x7fabc86f5910_0 (= v0x7fabc86f5850_0 0.0))
                (= v0x7fabc86f5d50_0 (< v0x7fabc86f5190_0 2.0))
                (= v0x7fabc86f5f10_0 (ite v0x7fabc86f5d50_0 1.0 0.0))
                (= v0x7fabc86f6050_0 (+ v0x7fabc86f5f10_0 v0x7fabc86f5190_0))
                (= v0x7fabc86f6a90_0 (= v0x7fabc86f5310_0 0.0))
                (= v0x7fabc86f6e50_0 (> v0x7fabc86f6250_0 1.0))
                (= v0x7fabc86f7250_0 (> v0x7fabc86f6250_0 0.0))
                (= v0x7fabc86f7390_0 (+ v0x7fabc86f6250_0 (- 1.0)))
                (= v0x7fabc86f7550_0
                   (ite v0x7fabc86f7250_0 v0x7fabc86f7390_0 v0x7fabc86f6250_0))
                (= v0x7fabc86f8550_0 (= v0x7fabc86f7a10_0 2.0))
                (= v0x7fabc86f8690_0 (= v0x7fabc86f7ad0_0 0.0))
                (= v0x7fabc86f87d0_0 (or v0x7fabc86f8690_0 v0x7fabc86f8550_0))
                (= v0x7fabc86f8910_0 (xor v0x7fabc86f87d0_0 true))
                (= v0x7fabc86f8a50_0 (and v0x7fabc86f6a90_0 v0x7fabc86f8910_0)))))
  (=> F0x7fabc86f95d0 a!5))))
(assert (=> F0x7fabc86f95d0 F0x7fabc86f9510))
(assert (let ((a!1 (=> v0x7fabc86f6190_0
               (or (and v0x7fabc86f5a50_0
                        E0x7fabc86f6310
                        (<= v0x7fabc86f6250_0 v0x7fabc86f6050_0)
                        (>= v0x7fabc86f6250_0 v0x7fabc86f6050_0))
                   (and v0x7fabc86f5790_0
                        E0x7fabc86f64d0
                        v0x7fabc86f5910_0
                        (<= v0x7fabc86f6250_0 v0x7fabc86f5190_0)
                        (>= v0x7fabc86f6250_0 v0x7fabc86f5190_0)))))
      (a!2 (=> v0x7fabc86f6190_0
               (or (and E0x7fabc86f6310 (not E0x7fabc86f64d0))
                   (and E0x7fabc86f64d0 (not E0x7fabc86f6310)))))
      (a!3 (or (and v0x7fabc86f7690_0
                    E0x7fabc86f7b90
                    (and (<= v0x7fabc86f7a10_0 v0x7fabc86f6250_0)
                         (>= v0x7fabc86f7a10_0 v0x7fabc86f6250_0))
                    (and (<= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)
                         (>= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)))
               (and v0x7fabc86f6bd0_0
                    E0x7fabc86f7e50
                    v0x7fabc86f6e50_0
                    (and (<= v0x7fabc86f7a10_0 v0x7fabc86f6250_0)
                         (>= v0x7fabc86f7a10_0 v0x7fabc86f6250_0))
                    (<= v0x7fabc86f7ad0_0 1.0)
                    (>= v0x7fabc86f7ad0_0 1.0))
               (and v0x7fabc86f6f90_0
                    E0x7fabc86f8090
                    (<= v0x7fabc86f7a10_0 v0x7fabc86f7550_0)
                    (>= v0x7fabc86f7a10_0 v0x7fabc86f7550_0)
                    (and (<= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)
                         (>= v0x7fabc86f7ad0_0 v0x7fabc86f5310_0)))))
      (a!4 (=> v0x7fabc86f7950_0
               (or (and E0x7fabc86f7b90
                        (not E0x7fabc86f7e50)
                        (not E0x7fabc86f8090))
                   (and E0x7fabc86f7e50
                        (not E0x7fabc86f7b90)
                        (not E0x7fabc86f8090))
                   (and E0x7fabc86f8090
                        (not E0x7fabc86f7b90)
                        (not E0x7fabc86f7e50))))))
(let ((a!5 (and (=> v0x7fabc86f5a50_0
                    (and v0x7fabc86f5790_0
                         E0x7fabc86f5b10
                         (not v0x7fabc86f5910_0)))
                (=> v0x7fabc86f5a50_0 E0x7fabc86f5b10)
                a!1
                a!2
                (=> v0x7fabc86f6bd0_0
                    (and v0x7fabc86f6190_0 E0x7fabc86f6c90 v0x7fabc86f6a90_0))
                (=> v0x7fabc86f6bd0_0 E0x7fabc86f6c90)
                (=> v0x7fabc86f6f90_0
                    (and v0x7fabc86f6190_0
                         E0x7fabc86f7050
                         (not v0x7fabc86f6a90_0)))
                (=> v0x7fabc86f6f90_0 E0x7fabc86f7050)
                (=> v0x7fabc86f7690_0
                    (and v0x7fabc86f6bd0_0
                         E0x7fabc86f7750
                         (not v0x7fabc86f6e50_0)))
                (=> v0x7fabc86f7690_0 E0x7fabc86f7750)
                (=> v0x7fabc86f7950_0 a!3)
                a!4
                v0x7fabc86f7950_0
                v0x7fabc86f8a50_0
                (= v0x7fabc86f5910_0 (= v0x7fabc86f5850_0 0.0))
                (= v0x7fabc86f5d50_0 (< v0x7fabc86f5190_0 2.0))
                (= v0x7fabc86f5f10_0 (ite v0x7fabc86f5d50_0 1.0 0.0))
                (= v0x7fabc86f6050_0 (+ v0x7fabc86f5f10_0 v0x7fabc86f5190_0))
                (= v0x7fabc86f6a90_0 (= v0x7fabc86f5310_0 0.0))
                (= v0x7fabc86f6e50_0 (> v0x7fabc86f6250_0 1.0))
                (= v0x7fabc86f7250_0 (> v0x7fabc86f6250_0 0.0))
                (= v0x7fabc86f7390_0 (+ v0x7fabc86f6250_0 (- 1.0)))
                (= v0x7fabc86f7550_0
                   (ite v0x7fabc86f7250_0 v0x7fabc86f7390_0 v0x7fabc86f6250_0))
                (= v0x7fabc86f8550_0 (= v0x7fabc86f7a10_0 2.0))
                (= v0x7fabc86f8690_0 (= v0x7fabc86f7ad0_0 0.0))
                (= v0x7fabc86f87d0_0 (or v0x7fabc86f8690_0 v0x7fabc86f8550_0))
                (= v0x7fabc86f8910_0 (xor v0x7fabc86f87d0_0 true))
                (= v0x7fabc86f8a50_0 (and v0x7fabc86f6a90_0 v0x7fabc86f8910_0)))))
  (=> F0x7fabc86f9790 a!5))))
(assert (=> F0x7fabc86f9790 F0x7fabc86f9510))
(assert (=> F0x7fabc86f9890 (or F0x7fabc86f9750 F0x7fabc86f95d0)))
(assert (=> F0x7fabc86f9850 F0x7fabc86f9790))
(assert (=> pre!entry!0 (=> F0x7fabc86f9690 true)))
(assert (let ((a!1 (not (or (not (>= v0x7fabc86f5310_0 0.0))
                    (not (<= v0x7fabc86f5190_0 1.0))
                    (not (<= v0x7fabc86f5310_0 0.0))
                    (not (>= v0x7fabc86f5190_0 1.0))))))
  (=> pre!bb1.i.i!0 (=> F0x7fabc86f9510 (or (>= v0x7fabc86f5310_0 1.0) a!1)))))
(assert (let ((a!1 (not (or (not (>= v0x7fabc86f4010_0 0.0))
                    (not (<= v0x7fabc86f53d0_0 1.0))
                    (not (<= v0x7fabc86f4010_0 0.0))
                    (not (>= v0x7fabc86f53d0_0 1.0))))))
  (= lemma!bb1.i.i!0 (=> F0x7fabc86f9890 (or (>= v0x7fabc86f4010_0 1.0) a!1)))))
(assert (= lemma!bb2.i.i16.i.i!0 (=> F0x7fabc86f9850 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb2.i.i16.i.i!0) (not lemma!bb2.i.i16.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0)
; (init: F0x7fabc86f9690)
; (error: F0x7fabc86f9850)
; (post-assumptions: post!bb1.i.i!0 post!bb2.i.i16.i.i!0)
