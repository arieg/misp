(declare-fun post!bb1.i.i!1 () Bool)
(declare-fun lemma!bb1.i.i!1 () Bool)
(declare-fun post!bb1.i.i!0 () Bool)
(declare-fun lemma!bb1.i.i!0 () Bool)
(declare-fun pre!bb1.i.i!0 () Bool)
(declare-fun F0x7fd88e2b6410 () Bool)
(declare-fun v0x7fd88e2b5710_0 () Bool)
(declare-fun v0x7fd88e2b5490_0 () Bool)
(declare-fun v0x7fd88e2b5350_0 () Bool)
(declare-fun v0x7fd88e2b2d10_0 () Real)
(declare-fun post!bb2.i.i16.i.i!0 () Bool)
(declare-fun v0x7fd88e2b2090_0 () Real)
(declare-fun v0x7fd88e2b1c10_0 () Real)
(declare-fun v0x7fd88e2b3f90_0 () Bool)
(declare-fun v0x7fd88e2b1150_0 () Real)
(declare-fun v0x7fd88e2b5850_0 () Bool)
(declare-fun pre!bb1.i.i!1 () Bool)
(declare-fun E0x7fd88e2b4f10 () Bool)
(declare-fun v0x7fd88e2b2bd0_0 () Bool)
(declare-fun E0x7fd88e2b4b50 () Bool)
(declare-fun v0x7fd88e2b45d0_0 () Bool)
(declare-fun F0x7fd88e2b63d0 () Bool)
(declare-fun E0x7fd88e2b47d0 () Bool)
(declare-fun v0x7fd88e2b4710_0 () Bool)
(declare-fun E0x7fd88e2b4410 () Bool)
(declare-fun v0x7fd88e2b4350_0 () Bool)
(declare-fun E0x7fd88e2b36d0 () Bool)
(declare-fun v0x7fd88e2b2ed0_0 () Real)
(declare-fun E0x7fd88e2b3950 () Bool)
(declare-fun E0x7fd88e2b3310 () Bool)
(declare-fun v0x7fd88e2b27d0_0 () Bool)
(declare-fun E0x7fd88e2b2450 () Bool)
(declare-fun v0x7fd88e2b0190_0 () Real)
(declare-fun v0x7fd88e2b3190_0 () Real)
(declare-fun v0x7fd88e2b2150_0 () Bool)
(declare-fun v0x7fd88e2b49d0_0 () Bool)
(declare-fun F0x7fd88e2b6550 () Bool)
(declare-fun v0x7fd88e2b3250_0 () Real)
(declare-fun v0x7fd88e2b4a90_0 () Real)
(declare-fun E0x7fd88e2b2350 () Bool)
(declare-fun v0x7fd88e2b0a90_0 () Real)
(declare-fun pre!entry!0 () Bool)
(declare-fun v0x7fd88e2b2290_0 () Bool)
(declare-fun v0x7fd88e2b0f90_0 () Bool)
(declare-fun F0x7fd88e2b6610 () Bool)
(declare-fun v0x7fd88e2b1cd0_0 () Bool)
(declare-fun E0x7fd88e2b1ed0 () Bool)
(declare-fun v0x7fd88e2b1e10_0 () Bool)
(declare-fun v0x7fd88e2b0410_0 () Real)
(declare-fun E0x7fd88e2b29d0 () Bool)
(declare-fun v0x7fd88e2b2910_0 () Bool)
(declare-fun E0x7fd88e2b1710 () Bool)
(declare-fun v0x7fd88e2b1490_0 () Real)
(declare-fun v0x7fd88e2b1290_0 () Real)
(declare-fun E0x7fd88e2b1550 () Bool)
(declare-fun v0x7fd88e2b13d0_0 () Bool)
(declare-fun E0x7fd88e2b0d50 () Bool)
(declare-fun v0x7fd88e2b3010_0 () Bool)
(declare-fun v0x7fd88e2b09d0_0 () Bool)
(declare-fun v0x7fd88e2b55d0_0 () Bool)
(declare-fun v0x7fd88e2b0c90_0 () Bool)
(declare-fun v0x7fd88e2b0b50_0 () Bool)
(declare-fun F0x7fd88e2b66d0 () Bool)
(declare-fun v0x7fd88e2b40d0_0 () Bool)
(declare-fun F0x7fd88e2b64d0 () Bool)
(declare-fun v0x7fd88e2b04d0_0 () Real)
(declare-fun v0x7fd88e2b0310_0 () Real)
(declare-fun v0x7fd88e2b03d0_0 () Real)
(declare-fun v0x7fd88e2b4210_0 () Bool)
(declare-fun E0x7fd88e2b4d10 () Bool)
(declare-fun v0x7fd88e2af010_0 () Real)
(declare-fun v0x7fd88e2b30d0_0 () Real)
(declare-fun lemma!bb2.i.i16.i.i!0 () Bool)
(declare-fun v0x7fd88e2af110_0 () Bool)
(declare-fun F0x7fd88e2b67d0 () Bool)

(assert (=> F0x7fd88e2b67d0
    (and v0x7fd88e2af110_0
         (<= v0x7fd88e2b03d0_0 1.0)
         (>= v0x7fd88e2b03d0_0 1.0)
         (<= v0x7fd88e2b04d0_0 0.0)
         (>= v0x7fd88e2b04d0_0 0.0)
         (<= v0x7fd88e2af010_0 1.0)
         (>= v0x7fd88e2af010_0 1.0))))
(assert (=> F0x7fd88e2b67d0 F0x7fd88e2b64d0))
(assert (let ((a!1 (=> v0x7fd88e2b13d0_0
               (or (and v0x7fd88e2b0c90_0
                        E0x7fd88e2b1550
                        (<= v0x7fd88e2b1490_0 v0x7fd88e2b1290_0)
                        (>= v0x7fd88e2b1490_0 v0x7fd88e2b1290_0))
                   (and v0x7fd88e2b09d0_0
                        E0x7fd88e2b1710
                        v0x7fd88e2b0b50_0
                        (<= v0x7fd88e2b1490_0 v0x7fd88e2b0410_0)
                        (>= v0x7fd88e2b1490_0 v0x7fd88e2b0410_0)))))
      (a!2 (=> v0x7fd88e2b13d0_0
               (or (and E0x7fd88e2b1550 (not E0x7fd88e2b1710))
                   (and E0x7fd88e2b1710 (not E0x7fd88e2b1550)))))
      (a!3 (=> v0x7fd88e2b2290_0
               (or (and v0x7fd88e2b1e10_0 E0x7fd88e2b2350 v0x7fd88e2b2150_0)
                   (and v0x7fd88e2b13d0_0
                        E0x7fd88e2b2450
                        (not v0x7fd88e2b1cd0_0)))))
      (a!4 (=> v0x7fd88e2b2290_0
               (or (and E0x7fd88e2b2350 (not E0x7fd88e2b2450))
                   (and E0x7fd88e2b2450 (not E0x7fd88e2b2350)))))
      (a!5 (or (and v0x7fd88e2b2910_0
                    E0x7fd88e2b3310
                    (and (<= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0)
                         (>= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0))
                    (and (<= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0)
                         (>= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0))
                    (<= v0x7fd88e2b3250_0 v0x7fd88e2b2ed0_0)
                    (>= v0x7fd88e2b3250_0 v0x7fd88e2b2ed0_0))
               (and v0x7fd88e2b2290_0
                    E0x7fd88e2b36d0
                    v0x7fd88e2b27d0_0
                    (and (<= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0)
                         (>= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0))
                    (and (<= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0)
                         (>= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0))
                    (and (<= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)
                         (>= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)))
               (and v0x7fd88e2b1e10_0
                    E0x7fd88e2b3950
                    (not v0x7fd88e2b2150_0)
                    (<= v0x7fd88e2b30d0_0 0.0)
                    (>= v0x7fd88e2b30d0_0 0.0)
                    (<= v0x7fd88e2b3190_0 0.0)
                    (>= v0x7fd88e2b3190_0 0.0)
                    (and (<= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)
                         (>= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)))))
      (a!6 (=> v0x7fd88e2b3010_0
               (or (and E0x7fd88e2b3310
                        (not E0x7fd88e2b36d0)
                        (not E0x7fd88e2b3950))
                   (and E0x7fd88e2b36d0
                        (not E0x7fd88e2b3310)
                        (not E0x7fd88e2b3950))
                   (and E0x7fd88e2b3950
                        (not E0x7fd88e2b3310)
                        (not E0x7fd88e2b36d0)))))
      (a!7 (or (and v0x7fd88e2b4710_0
                    E0x7fd88e2b4b50
                    (and (<= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)
                         (>= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)))
               (and v0x7fd88e2b4350_0
                    E0x7fd88e2b4d10
                    v0x7fd88e2b45d0_0
                    (<= v0x7fd88e2b4a90_0 1.0)
                    (>= v0x7fd88e2b4a90_0 1.0))
               (and v0x7fd88e2b3010_0
                    E0x7fd88e2b4f10
                    (not v0x7fd88e2b4210_0)
                    (and (<= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)
                         (>= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)))))
      (a!8 (=> v0x7fd88e2b49d0_0
               (or (and E0x7fd88e2b4b50
                        (not E0x7fd88e2b4d10)
                        (not E0x7fd88e2b4f10))
                   (and E0x7fd88e2b4d10
                        (not E0x7fd88e2b4b50)
                        (not E0x7fd88e2b4f10))
                   (and E0x7fd88e2b4f10
                        (not E0x7fd88e2b4b50)
                        (not E0x7fd88e2b4d10))))))
(let ((a!9 (and (=> v0x7fd88e2b0c90_0
                    (and v0x7fd88e2b09d0_0
                         E0x7fd88e2b0d50
                         (not v0x7fd88e2b0b50_0)))
                (=> v0x7fd88e2b0c90_0 E0x7fd88e2b0d50)
                a!1
                a!2
                (=> v0x7fd88e2b1e10_0
                    (and v0x7fd88e2b13d0_0 E0x7fd88e2b1ed0 v0x7fd88e2b1cd0_0))
                (=> v0x7fd88e2b1e10_0 E0x7fd88e2b1ed0)
                a!3
                a!4
                (=> v0x7fd88e2b2910_0
                    (and v0x7fd88e2b2290_0
                         E0x7fd88e2b29d0
                         (not v0x7fd88e2b27d0_0)))
                (=> v0x7fd88e2b2910_0 E0x7fd88e2b29d0)
                (=> v0x7fd88e2b3010_0 a!5)
                a!6
                (=> v0x7fd88e2b4350_0
                    (and v0x7fd88e2b3010_0 E0x7fd88e2b4410 v0x7fd88e2b4210_0))
                (=> v0x7fd88e2b4350_0 E0x7fd88e2b4410)
                (=> v0x7fd88e2b4710_0
                    (and v0x7fd88e2b4350_0
                         E0x7fd88e2b47d0
                         (not v0x7fd88e2b45d0_0)))
                (=> v0x7fd88e2b4710_0 E0x7fd88e2b47d0)
                (=> v0x7fd88e2b49d0_0 a!7)
                a!8
                v0x7fd88e2b49d0_0
                (not v0x7fd88e2b5850_0)
                (<= v0x7fd88e2b03d0_0 v0x7fd88e2b3190_0)
                (>= v0x7fd88e2b03d0_0 v0x7fd88e2b3190_0)
                (<= v0x7fd88e2b04d0_0 v0x7fd88e2b4a90_0)
                (>= v0x7fd88e2b04d0_0 v0x7fd88e2b4a90_0)
                (<= v0x7fd88e2af010_0 v0x7fd88e2b3250_0)
                (>= v0x7fd88e2af010_0 v0x7fd88e2b3250_0)
                (= v0x7fd88e2b0b50_0 (= v0x7fd88e2b0a90_0 0.0))
                (= v0x7fd88e2b0f90_0 (< v0x7fd88e2b0410_0 2.0))
                (= v0x7fd88e2b1150_0 (ite v0x7fd88e2b0f90_0 1.0 0.0))
                (= v0x7fd88e2b1290_0 (+ v0x7fd88e2b1150_0 v0x7fd88e2b0410_0))
                (= v0x7fd88e2b1cd0_0 (= v0x7fd88e2b1c10_0 0.0))
                (= v0x7fd88e2b2150_0 (= v0x7fd88e2b2090_0 0.0))
                (= v0x7fd88e2b27d0_0 (= v0x7fd88e2b0310_0 0.0))
                (= v0x7fd88e2b2bd0_0 (> v0x7fd88e2b1490_0 0.0))
                (= v0x7fd88e2b2d10_0 (+ v0x7fd88e2b1490_0 (- 1.0)))
                (= v0x7fd88e2b2ed0_0
                   (ite v0x7fd88e2b2bd0_0 v0x7fd88e2b2d10_0 v0x7fd88e2b1490_0))
                (= v0x7fd88e2b3f90_0 (not (= v0x7fd88e2b3190_0 0.0)))
                (= v0x7fd88e2b40d0_0 (= v0x7fd88e2b30d0_0 0.0))
                (= v0x7fd88e2b4210_0 (and v0x7fd88e2b3f90_0 v0x7fd88e2b40d0_0))
                (= v0x7fd88e2b45d0_0 (> v0x7fd88e2b3250_0 1.0))
                (= v0x7fd88e2b5350_0 (= v0x7fd88e2b3250_0 2.0))
                (= v0x7fd88e2b5490_0 (= v0x7fd88e2b4a90_0 0.0))
                (= v0x7fd88e2b55d0_0 (or v0x7fd88e2b5490_0 v0x7fd88e2b5350_0))
                (= v0x7fd88e2b5710_0 (xor v0x7fd88e2b55d0_0 true))
                (= v0x7fd88e2b5850_0 (and v0x7fd88e2b40d0_0 v0x7fd88e2b5710_0)))))
  (=> F0x7fd88e2b66d0 a!9))))
(assert (=> F0x7fd88e2b66d0 F0x7fd88e2b6610))
(assert (let ((a!1 (=> v0x7fd88e2b13d0_0
               (or (and v0x7fd88e2b0c90_0
                        E0x7fd88e2b1550
                        (<= v0x7fd88e2b1490_0 v0x7fd88e2b1290_0)
                        (>= v0x7fd88e2b1490_0 v0x7fd88e2b1290_0))
                   (and v0x7fd88e2b09d0_0
                        E0x7fd88e2b1710
                        v0x7fd88e2b0b50_0
                        (<= v0x7fd88e2b1490_0 v0x7fd88e2b0410_0)
                        (>= v0x7fd88e2b1490_0 v0x7fd88e2b0410_0)))))
      (a!2 (=> v0x7fd88e2b13d0_0
               (or (and E0x7fd88e2b1550 (not E0x7fd88e2b1710))
                   (and E0x7fd88e2b1710 (not E0x7fd88e2b1550)))))
      (a!3 (=> v0x7fd88e2b2290_0
               (or (and v0x7fd88e2b1e10_0 E0x7fd88e2b2350 v0x7fd88e2b2150_0)
                   (and v0x7fd88e2b13d0_0
                        E0x7fd88e2b2450
                        (not v0x7fd88e2b1cd0_0)))))
      (a!4 (=> v0x7fd88e2b2290_0
               (or (and E0x7fd88e2b2350 (not E0x7fd88e2b2450))
                   (and E0x7fd88e2b2450 (not E0x7fd88e2b2350)))))
      (a!5 (or (and v0x7fd88e2b2910_0
                    E0x7fd88e2b3310
                    (and (<= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0)
                         (>= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0))
                    (and (<= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0)
                         (>= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0))
                    (<= v0x7fd88e2b3250_0 v0x7fd88e2b2ed0_0)
                    (>= v0x7fd88e2b3250_0 v0x7fd88e2b2ed0_0))
               (and v0x7fd88e2b2290_0
                    E0x7fd88e2b36d0
                    v0x7fd88e2b27d0_0
                    (and (<= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0)
                         (>= v0x7fd88e2b30d0_0 v0x7fd88e2b0310_0))
                    (and (<= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0)
                         (>= v0x7fd88e2b3190_0 v0x7fd88e2b0190_0))
                    (and (<= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)
                         (>= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)))
               (and v0x7fd88e2b1e10_0
                    E0x7fd88e2b3950
                    (not v0x7fd88e2b2150_0)
                    (<= v0x7fd88e2b30d0_0 0.0)
                    (>= v0x7fd88e2b30d0_0 0.0)
                    (<= v0x7fd88e2b3190_0 0.0)
                    (>= v0x7fd88e2b3190_0 0.0)
                    (and (<= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)
                         (>= v0x7fd88e2b3250_0 v0x7fd88e2b1490_0)))))
      (a!6 (=> v0x7fd88e2b3010_0
               (or (and E0x7fd88e2b3310
                        (not E0x7fd88e2b36d0)
                        (not E0x7fd88e2b3950))
                   (and E0x7fd88e2b36d0
                        (not E0x7fd88e2b3310)
                        (not E0x7fd88e2b3950))
                   (and E0x7fd88e2b3950
                        (not E0x7fd88e2b3310)
                        (not E0x7fd88e2b36d0)))))
      (a!7 (or (and v0x7fd88e2b4710_0
                    E0x7fd88e2b4b50
                    (and (<= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)
                         (>= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)))
               (and v0x7fd88e2b4350_0
                    E0x7fd88e2b4d10
                    v0x7fd88e2b45d0_0
                    (<= v0x7fd88e2b4a90_0 1.0)
                    (>= v0x7fd88e2b4a90_0 1.0))
               (and v0x7fd88e2b3010_0
                    E0x7fd88e2b4f10
                    (not v0x7fd88e2b4210_0)
                    (and (<= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)
                         (>= v0x7fd88e2b4a90_0 v0x7fd88e2b30d0_0)))))
      (a!8 (=> v0x7fd88e2b49d0_0
               (or (and E0x7fd88e2b4b50
                        (not E0x7fd88e2b4d10)
                        (not E0x7fd88e2b4f10))
                   (and E0x7fd88e2b4d10
                        (not E0x7fd88e2b4b50)
                        (not E0x7fd88e2b4f10))
                   (and E0x7fd88e2b4f10
                        (not E0x7fd88e2b4b50)
                        (not E0x7fd88e2b4d10))))))
(let ((a!9 (and (=> v0x7fd88e2b0c90_0
                    (and v0x7fd88e2b09d0_0
                         E0x7fd88e2b0d50
                         (not v0x7fd88e2b0b50_0)))
                (=> v0x7fd88e2b0c90_0 E0x7fd88e2b0d50)
                a!1
                a!2
                (=> v0x7fd88e2b1e10_0
                    (and v0x7fd88e2b13d0_0 E0x7fd88e2b1ed0 v0x7fd88e2b1cd0_0))
                (=> v0x7fd88e2b1e10_0 E0x7fd88e2b1ed0)
                a!3
                a!4
                (=> v0x7fd88e2b2910_0
                    (and v0x7fd88e2b2290_0
                         E0x7fd88e2b29d0
                         (not v0x7fd88e2b27d0_0)))
                (=> v0x7fd88e2b2910_0 E0x7fd88e2b29d0)
                (=> v0x7fd88e2b3010_0 a!5)
                a!6
                (=> v0x7fd88e2b4350_0
                    (and v0x7fd88e2b3010_0 E0x7fd88e2b4410 v0x7fd88e2b4210_0))
                (=> v0x7fd88e2b4350_0 E0x7fd88e2b4410)
                (=> v0x7fd88e2b4710_0
                    (and v0x7fd88e2b4350_0
                         E0x7fd88e2b47d0
                         (not v0x7fd88e2b45d0_0)))
                (=> v0x7fd88e2b4710_0 E0x7fd88e2b47d0)
                (=> v0x7fd88e2b49d0_0 a!7)
                a!8
                v0x7fd88e2b49d0_0
                v0x7fd88e2b5850_0
                (= v0x7fd88e2b0b50_0 (= v0x7fd88e2b0a90_0 0.0))
                (= v0x7fd88e2b0f90_0 (< v0x7fd88e2b0410_0 2.0))
                (= v0x7fd88e2b1150_0 (ite v0x7fd88e2b0f90_0 1.0 0.0))
                (= v0x7fd88e2b1290_0 (+ v0x7fd88e2b1150_0 v0x7fd88e2b0410_0))
                (= v0x7fd88e2b1cd0_0 (= v0x7fd88e2b1c10_0 0.0))
                (= v0x7fd88e2b2150_0 (= v0x7fd88e2b2090_0 0.0))
                (= v0x7fd88e2b27d0_0 (= v0x7fd88e2b0310_0 0.0))
                (= v0x7fd88e2b2bd0_0 (> v0x7fd88e2b1490_0 0.0))
                (= v0x7fd88e2b2d10_0 (+ v0x7fd88e2b1490_0 (- 1.0)))
                (= v0x7fd88e2b2ed0_0
                   (ite v0x7fd88e2b2bd0_0 v0x7fd88e2b2d10_0 v0x7fd88e2b1490_0))
                (= v0x7fd88e2b3f90_0 (not (= v0x7fd88e2b3190_0 0.0)))
                (= v0x7fd88e2b40d0_0 (= v0x7fd88e2b30d0_0 0.0))
                (= v0x7fd88e2b4210_0 (and v0x7fd88e2b3f90_0 v0x7fd88e2b40d0_0))
                (= v0x7fd88e2b45d0_0 (> v0x7fd88e2b3250_0 1.0))
                (= v0x7fd88e2b5350_0 (= v0x7fd88e2b3250_0 2.0))
                (= v0x7fd88e2b5490_0 (= v0x7fd88e2b4a90_0 0.0))
                (= v0x7fd88e2b55d0_0 (or v0x7fd88e2b5490_0 v0x7fd88e2b5350_0))
                (= v0x7fd88e2b5710_0 (xor v0x7fd88e2b55d0_0 true))
                (= v0x7fd88e2b5850_0 (and v0x7fd88e2b40d0_0 v0x7fd88e2b5710_0)))))
  (=> F0x7fd88e2b6550 a!9))))
(assert (=> F0x7fd88e2b6550 F0x7fd88e2b6610))
(assert (=> F0x7fd88e2b63d0 (or F0x7fd88e2b67d0 F0x7fd88e2b66d0)))
(assert (=> F0x7fd88e2b6410 F0x7fd88e2b6550))
(assert (=> pre!entry!0 (=> F0x7fd88e2b64d0 true)))
(assert (=> pre!bb1.i.i!0 (=> F0x7fd88e2b6610 (>= v0x7fd88e2b0190_0 0.0))))
(assert (let ((a!1 (not (or (not (>= v0x7fd88e2b0410_0 1.0))
                    (not (<= v0x7fd88e2b0190_0 1.0))
                    (not (<= v0x7fd88e2b0310_0 0.0))
                    (not (<= v0x7fd88e2b0410_0 1.0))
                    (not (>= v0x7fd88e2b0190_0 1.0))
                    (not (>= v0x7fd88e2b0310_0 0.0))))))
(let ((a!2 (=> F0x7fd88e2b6610
               (or a!1
                   (<= v0x7fd88e2b0190_0 0.0)
                   (not (<= v0x7fd88e2b0310_0 0.0))))))
  (=> pre!bb1.i.i!1 a!2))))
(assert (= lemma!bb1.i.i!0 (=> F0x7fd88e2b63d0 (>= v0x7fd88e2b03d0_0 0.0))))
(assert (let ((a!1 (not (or (not (>= v0x7fd88e2af010_0 1.0))
                    (not (<= v0x7fd88e2b03d0_0 1.0))
                    (not (<= v0x7fd88e2b04d0_0 0.0))
                    (not (<= v0x7fd88e2af010_0 1.0))
                    (not (>= v0x7fd88e2b03d0_0 1.0))
                    (not (>= v0x7fd88e2b04d0_0 0.0))))))
(let ((a!2 (=> F0x7fd88e2b63d0
               (or a!1
                   (<= v0x7fd88e2b03d0_0 0.0)
                   (not (<= v0x7fd88e2b04d0_0 0.0))))))
  (= lemma!bb1.i.i!1 a!2))))
(assert (= lemma!bb2.i.i16.i.i!0 (=> F0x7fd88e2b6410 false)))
(assert (or (and (not post!bb1.i.i!0) (not lemma!bb1.i.i!0))
    (and (not post!bb1.i.i!1) (not lemma!bb1.i.i!1))
    (and (not post!bb2.i.i16.i.i!0) (not lemma!bb2.i.i16.i.i!0))))
(check-sat pre!entry!0 pre!bb1.i.i!0 pre!bb1.i.i!1)
; (init: F0x7fd88e2b64d0)
; (error: F0x7fd88e2b6410)
; (post-assumptions: post!bb1.i.i!0 post!bb1.i.i!1 post!bb2.i.i16.i.i!0)
