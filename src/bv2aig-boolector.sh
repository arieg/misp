#!/bin/bash

MISP_ROOT=~/MyRoot/School/research/misp/misp

[ $# -ne 1 ] && echo "Usage: bv2cnf-boolector <file.smt2>" && exit -1
[ ! -f $MISP_ROOT/tools/boolector/boolector ] && echo "Build the tools in ../tools first" && exit -1

b=`basename $1 .smt2`
echo "Bit-blasting $1..."
$MISP_ROOT/tools/boolector/boolector $1 -rwl0 -de -o $b.btor
$MISP_ROOT/tools/boolector/synthebtor -m $b.btor $b.aig 
rm -f $b.btor
echo "AIGER is in $b.aig"

