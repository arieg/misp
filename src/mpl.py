#!/usr/bin/python
#
# An implementation of the MISPER PipeLine (MPL) described in TACAS'14 
# submission. Uses: z3pdr.py, latobv.py, extract_inv.py, pick_inv.py, z3 and 
# ufo.
#
# (c) Anton Belov and Arie Gurfinkel, 2013

from __future__ import print_function
import z3pdr
import latobv
import extract_inv
import pick_inv
import z3
import sys
import os
import tempfile
import re
import subprocess

class Status:
    '''Safety status'''
    def __init__(self, r):
        self.r = r

    def __eq__(self, other):
        return isinstance(other, Status) and self.r == other.r

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        if self.r == 0: return "UNKNOWN"
        if self.r == -1: return "SAFE"
        if self.r == 1: return "UNSAFE"
#
safe = unsat = Status(-1)
unsafe = sat = Status(1)
unknown = Status(0)

class MPL(object):
    '''Implementation of MISPER PipeLine (MPL) for bit-presice safety'''
    def __init__(self):
        self.verbosity = 1
        self.width = 32
        self.keep_files = False
        self.bc_file = None
        self.tools_dir = None
        self.ufo_path = None
        self.z3_path = None
        self.horn_la_file = None
        self.horn_bv_file = None
        self.assinvar_la_file = None
        self.assinvar_bv_file = None
        self.cert_la_file = None
        self.cert_bv_file = None
        self.cert_mis_bv_file = None
        self.invar_mis_bv_file = None

    def _setup_tools(self):
        '''Checks that all the required tools are present, and sets up the paths'''
        self.ufo_path = self.tools_dir + "ufo"
        print(self.ufo_path)
        if not os.path.exists(self.ufo_path) or not os.access(self.ufo_path, os.X_OK):
            raise ValueError("ufo executable is not found at %s" % self.ufo_path)
        self.z3_path = self.tools_dir + "z3"
        if not os.path.exists(self.z3_path) or not os.access(self.z3_path, os.X_OK):
            raise ValueError("z3 executable is not found at %s" % self.z3_path)

    def _create_files(self):
        '''Creates all the temp files required for the execution of the pipeline'''
        pref = os.path.basename(self.bc_file) + "."
        self.horn_la_file = tempfile.mkstemp(".horn.la", prefix=pref, text=True)[1]
        self.horn_bv_file = tempfile.mkstemp(".horn.bv" + str(self.width) + ".smt2", prefix=pref, text = True)[1]
        self.assinvar_la_file = tempfile.mkstemp(".assinvar.la.smt2", prefix=pref, text = True)[1]
        self.assinvar_bv_file = tempfile.mkstemp(".assinvar.bv" + str(self.width) + ".smt2", prefix=pref, text = True)[1]
        self.cert_la_file = tempfile.mkstemp(".cert.la.smt2", prefix=pref, text = True)[1]
        self.cert_bv_file = tempfile.mkstemp(".cert.bv" + str(self.width) + ".smt2", prefix=pref, text = True)[1]
        self.cert_mis_bv_file = tempfile.mkstemp(".cert.mis.bv" + str(self.width) + ".smt2", prefix=pref, text = True)[1]
        self.invar_mis_bv_file = tempfile.mkstemp(".invar.mis.bv" + str(self.width) + ".smt2", prefix=pref, text = True)[1]
        if self.verbosity >= 2:
            print("[mpl] working files: ")
            for n in sorted(self.__dict__.keys()):
                if n.endswith("_file"):
                    print("[mpl]   %s = %s" % (n, self.__dict__[n]))

    def _remove_files(self):
        '''Cleans up after _create_files()'''
        if self.horn_la_file is not None:
            os.remove(self.horn_la_file) 
            os.remove(self.horn_bv_file)
            os.remove(self.assinvar_la_file)
            os.remove(self.assinvar_bv_file)
            os.remove(self.cert_la_file)
            os.remove(self.cert_bv_file)
            os.remove(self.cert_mis_bv_file)
            os.remove(self.invar_mis_bv_file)

    def _run_command(self, command):
        '''A little helper to run commands and pipe their output'''
        if self.verbosity >= 2: 
            print("[mpl] executing ", command)
        p = subprocess.Popen(command.split(),
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
        return iter(p.stdout.readline, '')

    def _run_ufo(self, in_bc, out_horn_la=None, out_cert=None, out_inv=None):
        '''Params:
            in_bc            the input bitcode file
            out_horn_la      passed as -muz-output
            out_cert         passed as -muz-cert + -muz-cert-inline=true
            out_inv          passed as -muz-invar-cert + -muz-invar-cert-with-assump
           Returns: safe/unsafe/unknown
        '''
        cmd = self.ufo_path + " -muz -muz-solve"
        if out_horn_la is not None:
            cmd += " -muz-output=" + out_horn_la
        if out_cert is not None:
            cmd += " -muz-cert=" + out_cert + " -muz-cert-inline=true"
        if out_inv is not None:
            cmd += " -muz-invar-cert=" + out_inv + " -muz-invar-cert-with-assump"
        cmd += " " + in_bc
        res = unknown
        for line in self._run_command(cmd):
            if self.verbosity >= 3: print(line)
            if line.startswith("SAFE"): res = safe
        if self.verbosity >= 2: print("[mpl] ufo returned %r" % res)
        return res
    
    def _run_latobv(self, in_la, out_bv, width=32, horn=False, break_and=False):
        '''Params:
            in_la    the input LA formula
            out_bv   the output BV formula
            width    the bit-width for conversion
            horn     if True, pass --horn option (for Horn systems)
            break_and if True, pass --break-and option (for invariants)
        '''
        cmd = "latobv.py -o " + out_bv + " -w " + str(width)
        if horn: cmd += " --horn"
        if break_and: cmd += " --break-and"
        cmd += " " + in_la
        if self.verbosity >= 2: print("[mpl] running latobv: " + cmd)
        latobv.main(cmd.split())
        if self.verbosity >= 2: print("[mpl] finished latobv")
        
    def _run_z3(self, in_f):
        '''Runs z3 on a given formula; returns sat/unsat/unknown'''
        # TODO: make this more roust to get errors ...
        cmd = "z3 " + in_f
        res = unknown
        for line in self._run_command(cmd):
            if self.verbosity >= 3: print(line)
            if line.startswith("sat"): res = sat
            if line.startswith("unsat"): res = unsat
        if self.verbosity >= 2: print("[mpl] z3 returned %r" % res)
        return res

    def _run_extract_inv(self, in_cert, width=32, bv_cert=True, out_cert=None):
        '''Params:
            in_cert    input certificate (LA or BV)
            width      bit width
            bv_cert    if True the certificate is BV, add -s option
            out_cert   output file for MIS certificate
           Returns: safe (if cert is safe) or unknown
        '''
        ie = extract_inv.InvExtractor(self.tools_dir,
                                      self.tools_dir,
                                      self.tools_dir + "abc",
                                      self.tools_dir + "muser-2")
        ie.verbose = self.verbosity
        ie.width = width
        ie.skip = bv_cert
        ie.inv_file_name = out_cert
        res = ie.run(in_cert)
        if self.verbosity >= 2: print("[mpl] extract_inv returned %r" % res)
        return safe if res else unknown

    def _run_pick_inv(self, in_cert_mis, in_ass_inv, out_inv_mis):
        '''Params:
            in_cert_mis   the input BV invariant (from extract_inv)
            in_ass_inv    the input inv with assumptions (from ufo, after latobv)
            out_inv_mis   the output invariant
        '''
        cmd = "pick_inv.py -o " + out_inv_mis + " " + in_ass_inv + " " + in_cert_mis
        if self.verbosity >= 2: print("[mpl] running pick_inv: " + cmd)
        pick_inv.main(cmd.split())
        if self.verbosity >= 2: print("[mpl] finished pick_inv")
                
    def _run_z3pdr(self, in_horn, in_inv = None, out_inv = None, out_cert = None):
        '''Params:
            in_horn    the input Horn SMT system
            in_inv     if not None, passed as --invs
            out_inv    if not None, passed as --out-inv; --out-inv_asmp is set
            out_cert   if not None, passed as --out-cert
            Returns: safe/unsafe/unknown
        '''
        cmd = "z3pdr.py"
        if in_inv is not None:
            cmd += " --invs " + in_inv
        if out_inv is not None:
            cmd += " --out-inv-asmp --out-inv " + out_inv
        if out_cert is not None:
            cmd += " --out-cert " + out_cert
        cmd += " " + in_horn
        if self.verbosity >= 2: print("[mpl] running Z3/PDR: " + cmd)
        res = z3pdr.main(cmd.split())
        if self.verbosity >= 2: print("[mpl] Z3/PDR returned %r" % res)
        res = safe if res == z3.unsat else (unsafe if res == z3.sat else unknown)
        return res 

    #
    # main flow ...
    #
    def run(self):
        '''Top-level entry point to run the framework. Returns mpl.safe or mpl.unknown'''
        try:
            self._setup_tools()
            self._create_files()
        
            if self.verbosity >= 1: print("[mpl] running ufo")
            res = self._run_ufo(self.bc_file, out_horn_la=self.horn_la_file, out_cert=self.cert_la_file, out_inv=self.assinvar_la_file)
            if (res != safe):
                if self.verbosity >= 1: print("[mpl] couldn't show safety of bitcode with ufo; giving up")
                return unknown
            
            if self.verbosity >= 1: print("[mpl] converting LA certificate to BV")
            self._run_latobv(self.cert_la_file, self.cert_bv_file, self.width)

            if self.verbosity >=1: print("[mpl] testing BV certificate")
            res = self._run_z3(self.cert_bv_file)
            if (res == unsat):
                if self.verbosity >=1: print("[mpl] BV certificate is unsat, done")
                return safe

            if self.verbosity >= 1: print("[mpl] extracting invariants")
            res = self._run_extract_inv(self.cert_bv_file, width=self.width, bv_cert=True, out_cert=self.cert_mis_bv_file)
            if (res == safe):
                if self.verbosity >=1: print("[mpl] extracted invariant is safe, done")
                return safe

            if self.verbosity >= 1: print("[mpl] converting LA invariants to BV")
            self._run_latobv(self.assinvar_la_file, self.assinvar_bv_file, self.width, break_and=True)

            if self.verbosity >= 1: print("[mpl] generating BV invariants")
            self._run_pick_inv(self.cert_mis_bv_file, self.assinvar_bv_file, self.invar_mis_bv_file)
            
            if self.verbosity >= 1: print("[mpl] converting Horn system to BV")
            self._run_latobv(self.horn_la_file, self.horn_bv_file, self.width, horn=True)
            
            if self.verbosity >= 1: print("[mpl] running Z3/PDR on BV system with BV invariants")
            res = self._run_z3pdr(self.horn_bv_file, in_inv=self.invar_mis_bv_file)
            if self.verbosity >= 1: print("[mpl] safety %s proved, done" % ("" if res == safe else "not"))
            
            return safe if res == safe else unknown
        
        finally:        
            if (not self.keep_files): self._remove_files()
    
        
# Entry point for external callers
def main(argv):
    def parseArgs(argv):
        import argparse as a
        p = a.ArgumentParser(description="Safety prover for programs in LLVM bitcode; \
                                          exit code: 1 = safe, 2 = unknown",
                             formatter_class=a.ArgumentDefaultsHelpFormatter)
        p.add_argument ("in_file", type=str, 
                        help="bitcode file")
        p.add_argument ("-v", "--verb", type=int, 
                        help="verbosity level (0,1,2)", default=1)
        p.add_argument ("-w", "--width", type=int,
                        help="bit-width for bit-precise reasoning", 
                        default=32)
        p.add_argument("-k", "--keep-files", 
                       help="keep all temporary files", 
                       action='store_true', default=False)
        p.add_argument ("--tools-dir", type=str, 
                        help="the path to the tools directory (default is the \
                        script path + /tools), which contains \
                        all the required tools (see readme)",
                        default=os.path.abspath(os.path.dirname(__file__)) + "/tools/")
        return p.parse_args (argv)
    # go ...
    args = parseArgs(sys.argv[1:])
    res = unknown
    try:
        mpl = MPL() # args.mroot + "/tools/boolector",
        mpl.verbosity = args.verb
        mpl.width = args.width
        mpl.keep_files = args.keep_files
        mpl.bc_file = args.in_file
        mpl.tools_dir = args.tools_dir
        res = mpl.run()
    except IOError as e:
        print("[mpl] ERROR: %r on %s" % (e, e.filename))
    except Exception as e:
        if (e.message): print("[mpl] ERROR: " + e.message)
        else: print("[mpl] ERROR: %r " % e)
    finally:
        if args.verb >= 1: print("[mpl] exiting with %r" % res)
        return 1 if res == safe else 2
        
if __name__ == "__main__":
    sys.exit(main(sys.argv))
    
