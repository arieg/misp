#!/usr/bin/python
#
# A prototype tool/module for conversion of BV formulas to AIG circuits
#
# (c) Anton Belov, 2013

from __future__ import print_function
import sys
import os
import subprocess 
import tempfile
import shutil
import shlex
import re

class Bv2aig:
    '''Aggregates all the functionality related to the conversion
    
    The pipeline is as follows:
        BV -> [boolector] -> BTOR -> [synthebtor] -> AIG1 -> [abc] -> AIG
        [boolector] stage can be done with or without optimizations
        [abc] stage is optional, and the ABC pipeline is configurable
    
    Configuration parameters (members):
        self.verbose = 1            # verbosity level
        self.store_inter = False    # store intermediate files
        self.btor_rwl = 3           # rewrite level in boolector
        self.abc = False            # use ABC stage
        self.abc_pipeline = "dc2 -v; dch; dc2 -v"
    
    If store_inter = True, the BTOR and AIG1 are stored into files in the same
    place where the input BV file is with appropriate names
    
    '''
    def __init__(self, aiger_root, btor_root, abc_path):
        '''The aiger_root and btor_root are directories; abc_path is an executable'''
        self.aigtoaig_path = aiger_root + "/aigtoaig"
        if not os.path.exists(self.aigtoaig_path):
            raise ValueError("aigtoaig executable is not found at %s" % self.aigtocnf_path)
        self.boolector_path = btor_root + "/boolector"
        if not os.path.exists(self.boolector_path):
            raise ValueError("boolector executable is not found at %s" % self.boolector_path)
        self.synthebtor_path = btor_root + "/synthebtor"
        if not os.path.exists(self.synthebtor_path):
            raise ValueError("synthebtor executable is not found at %s" % self.synthebtor_path)
        self.abc_path = abc_path
        if not os.path.exists(self.abc_path):
            raise ValueError("abc executable is not found at %s" % self.abc_path)
        self.verbose = 1            # verbosity level
        self.store_inter = False    # store intermediate files
        self.btor_rwl = 3           # rewrite level for boolector
        self.abc = False            # use ABC stage
        self.abc_pipeline = "dc2 -v; dch; dc2 -v"
        # file names
        self.bv_file_name = ""
        self.btor_file_name = ""
        self.aig1_file_name = ""
        self.aig_file_name = ""
        # to keep the process handle for return codes
        self.p = None
        
    def _init_file_names(self, bv_file_name, aig_file_name = ""):
        self.bv_file_name = bv_file_name
        if bv_file_name.endswith(".smt2"): bv_file_name = bv_file_name[:-5]
        if self.store_inter:
            self.btor_file_name = bv_file_name + ".btor"
            self.aig1_file_name = bv_file_name + ".1.aig"
        else:
            self.btor_file_name = tempfile.mkstemp(".btor", text = True)[1]
            self.aig1_file_name = tempfile.mkstemp(".1.aig", text = True)[1]
        self.aig_file_name = (bv_file_name + ".aig") if not aig_file_name else aig_file_name 
        if (self.verbose >= 2):
            print("[bv2aig] file names:")
            print("[bv2aig]   bv(input):", self.bv_file_name)
            print("[bv2aig]   btor:", self.btor_file_name)
            print("[bv2aig]   aig1:", self.aig1_file_name)
            print("[bv2aig]   aig(output):", self.aig_file_name)
    
    def _run_command(self, command):
        '''A little helper to run commands and pipe their output'''
        if self.verbose >= 2: print("[bv2aig] executing ", command)
        self.p = subprocess.Popen(shlex.split(command),
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
        return iter(self.p.stdout.readline, '')
    
    def _get_exit_code(self):
        '''Waits for the last command executed with _run_command to finish and
           returns the exit code'''
        self.p.wait()
        return self.p.returncode
    
    def _run_btor(self):
        '''Runs boolector and synthebtor'''
        cmd1 = (self.boolector_path + " " + self.bv_file_name + 
                " -rwl" + str(self.btor_rwl) + " -de -o " + self.btor_file_name)
        for line in self._run_command(cmd1):
            line = line.strip()
            if self.verbose >= 2: print("[bv2aig] [boolector]", line)
        if self._get_exit_code() != 0: 
            raise RuntimeError("boolector exited with non-0 status, probably an error in bitverctor formula") 
        cmd2 = (self.synthebtor_path + " -m -rwl" + str(self.btor_rwl) + " " +
                self.btor_file_name + " " + self.aig1_file_name)
        for line in self._run_command(cmd2):
            line = line.strip()
            if self.verbose >= 2: print("[bv2aig] [synthebtor]", line)
        if self._get_exit_code() != 0: 
            raise RuntimeError("synthebtor exited with non-0 status, probably an error in bitverctor formula") 
        if self.verbose >= 1: print("[bv2aig] finished boolector stage")

    def _run_abc(self):
        '''Runs ABC; if pipeline contains GIA commands, need to fix the signal names'''
        if not self.abc:
            shutil.copyfile(self.aig1_file_name, self.aig_file_name)
            return
        aig_file_name = tempfile.mkstemp(".aig", text = True)[1]
        cmd1 = (self.abc_path + " -c 'print_stats; " + self.abc_pipeline +
                "; print_stats; write_aiger -s " + aig_file_name + "' " + 
                self.aig1_file_name)
        for line in self._run_command(cmd1):
            line = line.strip()
            if self.verbose >= 2: print("[bv2aig] [abc]", line)
        if self.abc_pipeline.find("&") != -1:
            # need to fix the names of the signals the names of the signals
            sigs = []
            for line in self._run_command(self.aigtoaig_path + " -a " + self.aig1_file_name):
                if line.startswith("i"): sigs.append(line.strip())
            aag_file_name = tempfile.mkstemp(".aag", text = True)[1]
            aag_f = open(aag_file_name, "w")
            for line in self._run_command(self.aigtoaig_path + " -a " + aig_file_name):
                if line.startswith("i"): break
                print(line,file=aag_f,end='')
            for s in sigs: print(s,file=aag_f)
            aag_f.close()
            for line in self._run_command(self.aigtoaig_path + " " + aag_file_name + " " + self.aig_file_name):
                pass
        else:
            shutil.copyfile(aig_file_name, self.aig_file_name)
        if self.verbose >= 1: print("[bv2aig] finished ABC stage")
        
    def get_selectors(self, file_name):
        '''For a given file, returns a map of matched pre/post selectors (key=pre)
        and a list of unmatched selectors'''
        pres = set()
        posts = set()
        for line in open(file_name):
            for w in line.strip().split():
                m = re.match("(pre|post)(![^!]+![0-9]+).*", w)
                if m:
                    if m.group(1) == "pre": pres.add(m.group(1)+m.group(2))
                    else: posts.add(m.group(1)+m.group(2))
        matched = {}
        for pre in pres:
            cand = pre.replace("pre!", "post!")
            if cand in posts: matched[pre] = cand
        unmatched = [x for x in pres if not matched.has_key(x)]
        unmatched += [x for x in posts if not matched.has_key(x.replace("post!", "pre!"))]
        return (matched, unmatched)
        
    def convert(self, bv_file_name, aig_file_name = ""):
        '''Main external entry point'''
        self._init_file_names(bv_file_name, aig_file_name)
        bv_sels = self.get_selectors(self.bv_file_name)
        if self.verbose >= 1: print("[bv2aig] %d lemmas in the BV file" % 
                                    len(bv_sels[0].keys()))
        if len(bv_sels[1]) > 2:
            print("[bv2aig] WARNING: more than 2 unmatched assumptions in BV file: %r" % 
                  bv_sels[1])
        self._run_btor()
        self._run_abc()
        if self.verbose >= 1: print("[bv2aig] wrote AIG to", self.aig_file_name)
        aig_sels = self.get_selectors(self.aig_file_name)
        if len(aig_sels[1]) > 1:
            print("[bv2aig] WARNING: %d unmatched assumptions in AIG file: %r" % 
                  (len(aig_sels[1]), aig_sels[1]))
        
        
# Entry point for external callers
if __name__ == "__main__":
    def parseArgs(argv):
        import argparse as a
        p = a.ArgumentParser(description="BV to AIG converter",
                             formatter_class=a.ArgumentDefaultsHelpFormatter)
        p.add_argument ("bv_file", type=str, 
                        help="bitvector formula in SMT2 format")
        p.add_argument ("-o", "--outfile", type=str,
                        help="file to write the resulting AIG; if none given\
                              then the output is written to input.aig",
                        default="")
        p.add_argument ("-b", "--btorrwl", type=int,
                        help="rewrite level in boolector (0-3)", 
                        default=3)
        p.add_argument ("-a", "--abc",
                        help="use ABC stage", 
                        action='store_true', default=False)
        p.add_argument ("--abccmd", type=str, 
                        help="the pipeline of ABC commands to use (enclosed in quotes);\
                              add -v and set --verb 2 to see what ABC is doing", 
                        default='dc2 -v; dch; dc2 -v')
        p.add_argument ("-v", "--verb", type=int, 
                        help="verbosity level (0,1,2)", default=1)
        p.add_argument ("-s", "--storeint",
                        help="store intermediate files with alongside the input", 
                        action='store_true', default=False)
        p.add_argument ("--aroot", type=str, 
                        help="the path to aiger package root", 
                        default="../tools/aiger")
        p.add_argument ("--broot", type=str, 
                        help="the path to boolector package root", 
                        default="../tools/boolector")
        p.add_argument ("--abcpath", type=str, 
                        help="the path to ABC executable", 
                        default="../tools/abc/abc")
        
        return p.parse_args (argv)
    # go ...
    args = parseArgs(sys.argv[1:])
    try:
        b2a = Bv2aig(args.aroot, args.broot, args.abcpath)
        b2a.btor_rwl = args.btorrwl
        b2a.abc = args.abc
        b2a.abc_pipeline = args.abccmd
        b2a.verbose = args.verb
        b2a.store_inter = args.storeint
        b2a.convert(args.bv_file, args.outfile)
    except IOError as e:
        print("[bv2aig] ERROR: %r on %s" % (e, e.filename))
    except Exception as e:
        if (e.message): print("[bv2aig] ERROR: " + e.message)
        else: print("[bv2aig] ERROR: %r " % e)
        
