import z3
import sys

def parseArgs (argv):
    import argparse as a
    p = a.ArgumentParser (description='Generate MSI problem')

    p.add_argument ('--inv', help='Invariant to use', required=True,
                    type=str, metavar='FILE', default=None)
    p.add_argument ('file', metavar='FILE', help='SMT2-LIB file' )
    p.add_argument ('-o', metavar='FILE', help='Output file', required=True)

    args = p.parse_args (argv)
    return args

def main (argv):
    args = parseArgs (argv [1:])

    ctx = z3.Context ()
    fp = z3.Fixedpoint (ctx=ctx)
    fp.set (engine='pdr')
    fp.set (slice=False)
    fp.set (inline_linear = False)
    fp.set (inline_eager = False)

    print 'Parsing...'
    query = fp.parse_file (args.file)

    lemmas = [ z3.parse_smt2_file (args.inv, sorts={}, decls={}, ctx=ctx) ]
    if z3.is_and (lemmas [0]):
        lemmas = lemmas[0].children ()

    print 'Found {} lemmas'.format (len (lemmas))

    m = z3.AstMap (ctx=ctx)

    for l in lemmas:
        pred = l.arg (0)
        lemma = l.arg (1)

        if pred not in m:
            m [pred] = lemma
        else:
            m [pred] = z3.And (m [pred], lemma)

    for pred in m.keys ():
        rule = z3.Implies (m[pred], pred)
        vars = pred.children ()
        if len (vars) > 0:
            rule = z3.ForAll (vars, rule)
        # Add rule lemmas -> head
        fp.add_rule (rule)

    print 'Writing', args.o
    with open (args.o, 'w') as out:
        out.write (fp.sexpr ())
        out.write ('\n')
        out.write ('(query ')
        out.write (query [0].sexpr ())
        out.write (' )')
    return 0

if __name__ == '__main__':
    sys.exit (main (sys.argv))
