#!/usr/bin/env python

import z3
import z3core
import sys
import stats

from z3_utils import *
import hc_problem as hcp

verbose=False

def fp_add_cover (fp, pred, lemma, level=-1):
    # no trivial lemmas
    if z3.is_true (lemma): return

    assert (z3.is_app (pred))
    sub = []
    for i in range (0, pred.num_args ()):
        arg = pred.arg (i)
        sub.append ((arg, 
                     z3.Var (i, arg.decl ().range ())))

    tlemma = z3.substitute (lemma, sub)
    if verbose:
        print "Lemma for ", pred.decl (), ": ", tlemma
    fp.add_cover (level, pred.decl (), tlemma)

def fp_get_cover_delta (fp, pred, level=-1):
    sub = []
    for i in range (0, pred.num_args ()):
        sub.append (pred.arg (i))

    lemma = fp.get_cover_delta (level, pred.decl ())
    if z3core.Z3_get_bool_value (fp.ctx.ctx, lemma.as_ast ()) != z3.unsat:
        lemma = z3.substitute_vars (lemma, *sub)
    return lemma


def parseArgs (argv):
    import argparse as a
    p = a.ArgumentParser (description='Z3 PDR Frontend')
    
    p.add_argument ('file', metavar='BENCHMARK', help='Benchmark file')
    p.add_argument ('--pp', 
                    help='Enable default pre-processing', 
                    action='store_true', default=False)
    p.add_argument ('--validate', help='Enable validation',
                    action='store_true', default=False)
    p.add_argument ('--trace', help='Trace levels to enable', 
                   default='')
    p.add_argument ('--answer', help='Print answer', action='store_true',
                    default=False)
    p.add_argument ('--invs', help='Additional invariants', default=None)
    p.add_argument ('--verbose', help='Be verbose', 
                    default=False, action='store_true')
    p.add_argument ('--out-cert', dest='out_cert', 
                    help='Location for certificate', default=None)
    p.add_argument ('--out-inv', dest='out_inv', 
                    help='Location for invariants', default=None)
    p.add_argument ('--out-inv-asmp', dest='out_inv_asmp', 
                    default=False, action='store_true', 
                    help='Store invariants with assumptions')
    pars = p.parse_args (argv)
    global verbose
    verbose = pars.verbose
    return pars

def stat (key, val): stats.put (key, val)

def output_certificate (fp, query, args):
    if args.out_cert is None: return

    p = hcp.hc_from_fp (fp, query)
    
    smt = z3.Solver (solver=None, ctx=fp.ctx)
    smt2 = z3.Solver (solver=None, ctx=fp.ctx)

    ### edges / rules
    for pt in p.pts:
        for r in pt.get_rules ():
            if r.src is None: continue
            body = []
            body.extend (r.tail)
            body.extend (r.state_eq)
            smt.add (z3.Implies (r.get_tag (), z3.And (*body)))
            smt.add (z3.Implies (r.get_tag (), 
                                 r.get_src_pt ().get_tag ()))

    ### edges to nodes
    for pt in p.pts:
        if pt == p.get_root () : continue
        pred_tags = [r.get_tag () for r in pt.get_rules ()]
        smt.add (z3.Implies (pt.get_tag2 (), z3.Or (*pred_tags)))

    ### pre lemmas
    preAsmps = []
    for pt in p.pts:
        if pt == p.get_root (): continue
        
        asmpPrefix = "pre!" + str (pt.head) + "!"
        
        lemmas = [fp_get_cover_delta (fp, pt.pre_inst ())]
        if z3.is_and (lemmas[0]): lemmas = lemmas[0].children ()
        
        for i in range (0, len (lemmas)):
            lemma = lemmas[i]
            asmpName = asmpPrefix + str(i)
            asmp = z3.Bool (asmpName, smt.ctx)
            preAsmps.append (asmp)
            smt.add (z3.Implies (asmp, z3.Implies (pt.get_tag (), lemma)))

            inv = z3.Implies (pt.pre_inst (), lemma)
            if args.out_inv_asmp: inv = z3.Implies (asmp, inv)
            smt2.add (inv)
            
    ### post lemmas
    postAsmps = []
    postLemmas = []
    for pt in p.pts:
        asmpPrefix = 'post!' + str (pt.head) + '!'
        lemmas = [fp_get_cover_delta (fp, pt.post_inst ())]
        if z3.is_and (lemmas[0]): lemmas = lemmas[0].children ()
        
        tag = pt.get_tag2 ()
        for i in range (0, len(lemmas)):
            lemma = lemmas [i]
            asmpName = asmpPrefix + str(i)
            asmp = z3.Bool (asmpName, smt.ctx)
            postAsmps.append (asmp)
            postLemmas.append (z3.And (z3.Not (asmp), tag, z3.Not (lemma)))
    smt.add (z3.Or (*postLemmas))
        

    ## write out the certificate
    f = open (args.out_cert, 'w')
    solver_to_smtlib (smt, f)
    f.write ('(check-sat ')
    for a in preAsmps: 
        f.write (str (a))
        f.write (' ')
    f.write (')\n')

    f.write ('; (init: ')
    f.write (str (p.get_root ().get_tag ()))
    f.write (')\n')
    f.write ('; (error: ')
    f.write (str (p.get_query ().get_tag2 ()))
    f.write (')\n')
    

    f.write (";(post-assumptions: ")
    for a in postAsmps: 
        f.write (str (a))
        f.write (' ')
    f.write (')\n')
    f.close ()
    f = None

    ## write out the invariants
    if args.out_inv is not None:
        f = open (args.out_inv, 'w')
        solver_to_smtlib (smt2, f)
        f.close ()

def main (argv):
    args = parseArgs (argv[1:])
    stat ('Result', 'UNKNOWN')
    ctx = z3.Context ()
    fp = z3.Fixedpoint (ctx=ctx)
    fp.set (engine='pdr')
    if not args.pp:
        print 'No pre-processing'
        fp.set (slice=False)
        fp.set (inline_linear=False)
        fp.set (inline_eager=False)

    fp.set (validate_result=args.validate)
    
    with stats.timer ('Parse'):
        q = fp.parse_file (args.file)

    if len(args.trace) > 0:
        print 'Enable trace: ',
        for t in args.trace.split (','):
            print t,
            z3.enable_trace (t)
        print 
        stats.put ('Trace', args.trace)

    if args.invs :
        lemmas = z3.parse_smt2_file (args.invs, sorts={}, decls={}, ctx=ctx)
        if z3.is_and (lemmas):
            lemmas = lemmas.children ()
        for l in lemmas:
            if verbose: print l
            fp_add_cover (fp, l.arg(0), l.arg(1))

    #print fp
    with stats.timer ('Query'):
        res = fp.query (q[0])

    if res == z3.sat: stat ('Result', 'CEX')
    elif res == z3.unsat: stat ('Result', 'SAFE')

    if args.answer:
        print 'The answer is:'
        print fp.get_answer ()

    if res == z3.unsat:
        output_certificate (fp, q, args)
        
    return res

if __name__ == '__main__':
    try:
        res = main (sys.argv)
    finally:
        stats.brunch_print ()
    sys.exit (res)

